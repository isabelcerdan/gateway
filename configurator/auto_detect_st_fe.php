<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');
include_once ('control/get_network_protector_status.php');

if($netEnabled == 1){
    $netProEnabled = 0;
}else {
    $netProEnabled = 1;
}

$stringResponse = '';
$alertString = ''; //alert-danger

$feedDetect =  $_GET['feedDetect'];
$feedDetect = filter_var($feedDetect, FILTER_SANITIZE_STRING);

$stringDetect =  $_GET['stringDetect'];
$stringDetect= filter_var($stringDetect, FILTER_SANITIZE_STRING);

$stringFirst=  $_GET['stringFirst'];
$stringFirst = filter_var($stringFirst, FILTER_SANITIZE_STRING);


if($stringDetect == 'no') {
    $stringResponse = ' No change detected';
    $alertString =  'alert-warning';
}
if($stringDetect == 'yes') {
    $stringResponse = '<strong>Success!</strong> String Detection has been started';
    $alertString = 'alert-success';
}
if($feedDetect == 'no') {
    $stringResponse = ' No change detected';
    $alertString =  'alert-warning';
}
if($feedDetect == 'yes') {
    $stringResponse = '<strong>Success!</strong> Feed Detection has been started';
    $alertString = 'alert-success';
}
if($stringFirst == 'no') {
    $stringResponse = '<strong>Failure</strong> You cannot find Feeds until you detect Strings';
    $alertString =  'alert-danger';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Auto Detect Strings, Feeds, Inverters</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>

    <script>

        function auto_feed_status(){
            $.ajax({
                url: "control/get_feed_detect_status.php",
                cache: false,
                success: function(data){
                    $("#getFeedDetectStatus").html(data);

                    if (data.indexOf("start") >= 0) {
                        $("#feedButton").html('Cancel Detect Feeds');
                        $("#feedDetectURL").attr("href", "control/form_auto_detect_st_fe.php?feed=yes&feedCommand=cancel");
                    }else {
                        $("#feedButton").html('Detect Feeds All Strings');
                        $("#feedDetectURL").attr("href", "control/form_auto_detect_st_fe.php?feed=yes&feedCommand=start");
                    }
                }
            });
        }

        $(document).ready(function(){
            auto_feed_status();
        });

        //Refresh auto_load() function after 10000 milliseconds
        setInterval(auto_feed_status,2000);


        $(document).ready(function(){
            $('.loading').hide();

            $(".loading").click(function(){
                $('.loading').hide();
            });


            $('#network_option').click(function(){
                    var r = confirm("Are you sure you want to change Network Settings?");
                    if (r == true) {
                        open('network_settings.php', '_self',);
                    }
                });


            auto_scan_string();
            auto_network_protector_status();



            $('.detectString').on('click', function(e) {


                $.ajax({
                    type: "POST",
                    url: 'control/form_auto_detect_st_fe.php?string=yes&stringDetect=1',
                    //data: formDetails.serialize(),
                    success: function (data) {
                        console.log('success:');

                    },
                    error: function(jqXHR, text, error){

                        console.log('error:');

                    }
                });


            });





            function auto_inverter_status(){
                $.ajax({
                    url: "control/inverter_status.php",
                    cache: false,
                    success: function(data){
                        $("#inverter_status_div").html(data);
                    }
                });
            }




            function auto_scan_string(){
                $.ajax({
                    url: "control/get_string_detect_status.php",
                    cache: false,
                    success: function(data){
                        $("#string_scan_div").html(data);
                    }
                });
            }

            function auto_network_protector_status(){
                $.ajax({
                    url: "control/get_network_protector_status.php?display=yes",
                    cache: false,
                    success: function(data){
                        $("#network-protector-status").html(data);
                    }
                });
            }


            //Refresh auto_load() function after 10000 milliseconds
            setInterval(auto_inverter_status,20000);
            function auto_scan_string(){
                $.ajax({
                    url: "control/get_string_detect_status.php",
                    cache: false,
                    success: function(data){
                        $("#string_scan_div").html(data);
                    }
                });
            }

            function auto_network_protector_status(){
                $.ajax({
                    url: "control/get_network_protector_status.php?display=yes",
                    cache: false,
                    success: function(data){
                        $("#network-protector-status").html(data);
                    }
                });
            }


            //Refresh auto_load() function after 10000 milliseconds


            setInterval(auto_scan_string,2000);
            setInterval(auto_network_protector_status,20000);
        });
    </script>
</head
<body>
<div class="loading">Loading&#8230;</div>
<div class="container">
    
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-8"><h1 style="padding-left: 30px;">Auto Detect Strings, Feeds, Inverters</h1></div>
                    <div class="col-md-4">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>



                    <div style="padding-left: 30px; padding-right: 30px;">

                        <?php
                        if( ( $stringDetect != '') OR ($stringFirst== 0)) {
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert <?php echo $alertString; ?>" ?>
                                        <?php echo $stringResponse; ?></php>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Auto Detect Strings</h3></div>
                                    <div class="panel-body">

                                        <p></p>
                                        <p>  </p>
                                        <p class="text-center"><button class="detectString btn btn-warning">Detect Strings</button></a>
                                        </p>
                                        <div id="string_scan_div"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">



                                    <div class="panel-heading"><h3 class="text-center">Auto Detect Feeds</h3></div>
                                    <div class="panel-body">


                                        <!--
                                         <p class="text-center"><a id='feedDetectURL' href="control/form_auto_detect_st_fe.php?feed=yes&feedCommand=start"><button type="button" class="btn btn-warning"><span id="feedButton">Detect Feeds All Strings</span></button></a> -->
                                        <p class="text-center">
                                            <a  href="fd_by_string.php"> <button type="button" class="btn btn-warning"><span id="feedStringButton">Pick Strings for Feeds Detection </span></button></a>
                                        </p>
                                        <div id="getFeedDetectStatus"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Router Info</h3></div>
                                    <div class="panel-body">


                                       <p class="text-center"><a href="router_info.php"><button type="button" class="btn btn-warning">Router Options</button></a></p>
                                        <div class="text-center"> </div>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Enable  Network Protector</h3></div>
                                    <div class="panel-body">
                                        <form class="form-horizontal row-border" action="control/form_network_protector_enable.php" method="post" id="brainboxesForm">
                                            <input type="hidden" name="network_protector_enabled" value="<?php echo $netProEnabled; ?>">

                                        <p class="text-center"><button  id ='network-protector' type="submit"  class="btn btn-primary" ><?php echo $netProEnabled == 1 ? 'Enable' : 'Disable'; ?> Network Protector</button></p>
                                            </form>
                                         <div class=" text-center" id="network-protector-status"></div>
                                        <p><br>
                                            <a href="net_protect_users.php">Manage Network Protection Users</a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">PSA Options</h3></div>
                                    <div class="panel-body">


                                        <p class="text-center"><a href="psa-edit.php"><button type="button" class="btn btn-warning">PSA Options</button></a></p>
                                        <div class="text-center"> </div>

                                    </div>
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Network Settings</h3></div>
                                    <div class="panel-body">


                                        <p class="text-center"><button type="button" id="network_option" class="btn btn-danger">Network Options</button></p>
                                        <div class="text-center"> </div>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">

                            </div>

                        </div>

                    </div>
            </div>
        </div>
    </div>
    <?php    include_once ('footer.php'); ?>
</div>


</div>
</body>
</html>
