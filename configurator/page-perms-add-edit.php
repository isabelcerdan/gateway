<?php
    //include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Meter Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }


    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js"></script>

    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

include_once('control/get_brainboxes.php'); // Get default data
include_once ('functions/get_readable_privileges.php');


$page_perm_id= mysqli_real_escape_string($conn, $_REQUEST['page_perm_id']);
$page_perm_id = filter_var($page_perm_id, FILTER_SANITIZE_STRING);
//include_once ('functions/page_permissions_check.php');

$errors = mysqli_real_escape_string($conn, $_GET['errors']);
$errors = filter_var($errors, FILTER_SANITIZE_STRING);

if (strpos($errors, 'no_affected_rows') !== false) {
    $changes = "No Changes Made";
}

if($page_perm_id !='') {
 include_once('functions/page_permissions_check.php'); // Get default data
}
else{
    $pageURL = '';
}
// set Privileges human readable
$human_privileges = get_readable_privileges($access_level);
?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Set Page Access Level</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_page_perms_add_edit.php" method="post" id="permForm">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">
                        <input type="hidden" name="page_perm_id" value="<?php echo $page_perm_id; ?>">


                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">
                                <div class="form-group">

                                    <div class="col-md-12 " >
                                        <div class="formTextSpacing text-center">
                                            <h2> Add/Edit <?php echo $pageURL; ?> Page </h2>
                                            <?php
                                            // Errors
                                            if ($changes != '') {
                                                echo"<p style='color: red;font-weight: bold'>$changes </p>";
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                            </div>


                                <!-- Beginning of tab 1 -->

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Page Name:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="pageURL" id="pageURL" class="form-control" value="<?php echo $pageURL; ?>" required>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Page URL" data-content="Copy the URL of the page that you want to set page permission for (include the initial / "  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Copy the URL of the page that you want to set page permission for (include the initial / Example: <strong>/power_regulator.php</strong>)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Page Access Level:</label>
                                            <div class="col-md-3">
                                                <select name="access_level" class="form-control" required>
                                                    <?php
                                                    if($access_level !='') {
                                                        echo "<option value='$access_level' >$human_privileges - Current</option>";
                                                    }else {
                                                        echo "<option value='' >Select Privileges</option>";
                                                    }
                                                    ?>
                                                    <option value='4'>Monitor</option>
                                                    <option value='3'>Installer</option>
                                                    <option value='2'>Manager</option>
                                                    <option value='1'>Administrator</option>
                                                </select>
                                                
                                          
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Meter Role" data-content="Provide the role that the meter will play in this installation. Examples:"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>When setting Access Level remember that any higher level user has access to lover level pages. Example: If you set page access to an Installer a Manager will also have access to that page.
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Site (GUI or Config): </label>
                                            <div class="col-md-3">
                                                <select name="site" class="form-control" required>
                                                    <?php
                                                    if($site !='') {
                                                        echo "<option value='$site' >$site</option>";
                                                    }else {
                                                        echo "<option value='' >Select Role</option>";
                                                    }
                                                    ?>
                                                    <option value='config' >Configurator Access</option>
                                                    <option value='gui' >GUI Access</option>
                                                    <option value='both' >GUI & Configurator Access</option>
                                                </select>

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Meter Class"
                                                        data-content=" "  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>Specify it you want this rule to apply to the GUI or the Configurator.
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Notes:</label>
                                            <div class="col-md-8">

                                                <input  type="textarea" name="notes" id="notes" class="form-control" value="<?php echo $notes; ?>">
                                            </div>
                                            <div class="col-md-1" >

                                            </div>
                                        </div>


                                    </div>




                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>

                    </form>


        </div>
                    <?php    include_once ('footer.php'); ?>

                </div>
    <!-- Row end -->

</div>
</body>
</html>