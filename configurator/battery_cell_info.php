<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Alarm Data</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->


    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.4/dt-1.10.15/b-1.3.1/cr-1.3.3/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    -->


    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>



</head
<body>
<?php
include_once ('functions/mysql_connect.php');

?>

<script type="text/javascript" class="init">


    $(document).ready(function (){



    });


</script>
<div class="container">
    
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Battery Cell Info</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                
                <div class="formBoxSection">

                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">

                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">

                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <?php include_once ('control/get_battery_cell.php'); ?>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>