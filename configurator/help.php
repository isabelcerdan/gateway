<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Help</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        button, input[type="button"] {

            text-shadow: none !important;
        }

        #stopSystem {
            display: none;
        }

        .smallerText {
            font-size: xx-small;
        }

        #Update_Success {
            display: none;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>
</head
<body>
<?php
include_once('functions/mysql_connect.php');
?>
<div class="container">
    <div class="apparentBrand">Apparent igOS Gateway</div>
    <?php
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Help</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>

                <div style="padding: 30px;">

                    <!-- uncomment when we have a good system for displaying active configuration isssues

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><h3 class="text-center">Active Isuses</h3></div>
                                                    <div class="panel-body">
                                                        <p><strong>Problem:</strong></p>



                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                    -->

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading"><h3 class="text-center">Configuration Help</h3></div>
                                <div class="formBoxSection">

                                    <h3 id="brainbox">Brainbox Problems</h3>

                                    <ul>
                                        <li><strong>Brainbox</strong></li>
                                        <ul>
                                            <li>No brainbox</li>
                                            <li>IP address that cannot be reached</li>
                                        </ul>
                                        <li><strong>Brainbox Unreachable</strong></li>
                                        <ul>
                                            <li>Check brainbox jumpers</li>
                                            <ul>
                                                <li>Open the brainbox cover and place jumpers on HD+ and HD-. Disconnect
                                                    all other jumpers.
                                                </li>
                                            </ul>
                                            <li>Check IP address in browser</li>
                                        </ul>
                                        <li><strong>Brainbox comm issues</strong></li>
                                        <ul>
                                            <li>Is brainbox configured correctly?</li>
                                            <ul>
                                                <li>Serial Port</li>
                                                <li>38400 baud rate (Don't use 38400 drop down option; must choose
                                                    "Other" in drop down and type 38400 for bug workaround)
                                                </li>
                                                <li>Data Bits: 8</li>
                                                <li>Parity: None</li>
                                                <li>Stop Bits: 1</li>
                                                <li>Flow Control: None</li>
                                                <li>Enable FIFO: checked</li>
                                                <li>Duplex Mode: ES485 half-duplex autogating mode</li>
                                                <li>Protocol type: raw tcp</li>
                                                <li>Local tcp port: 9001</li>
                                                <li>Connection timeout: 0</li>
                                                <li>Serial Tuning: not checked</li>
                                                <li>Port status: should show "Connected to #10.252.0.2:35156":</li>
                                                <li> Place the jumpers on HD+ and HD-. Disconnect all of the other jumpers.
                                                    <br>
                                                    <img src="images/brainbox_jumpers.png" class="border border-dark">
                                                </li>
                                            </ul>
                                        </ul>
                                    </ul>
                                    <strong></strong>

                                    <h3 id="meter-veris">Veris Meter</h3>
                                    <ul>
                                        <li><strong>Can't talk to veris:</strong></li>
                                        <ul>
                                            <li>Check baud rate 38400 bps (consistent with brainbox)</li>
                                            <li>Check modbus channel (usually 1)</li>
                                        </ul>
                                        <li><strong>Inconsistent meter polarity with gateway:</strong></li>
                                        <ul>
                                            <li>Check gateway perspective: expected if production otherwise CT
                                                orientation
                                            </li>
                                            <li>Check RS-485 wiring</li>
                                            <li>Check polarity consistency across all phases</li>
                                        </ul>
                                        <li><strong>Wrong meter reading magnitude</strong></li>
                                        <ul>
                                            <li>CT sizing setting should be 0.33V sizing</li>
                                        </ul>
                                        <li><strong>Wrong system configuration (who is master, gateway or veris? make
                                                    sure seeing is consistent)</strong></li>
                                        <ul>
                                            <li>3L-1n Wye Three Phase with neutral</li>
                                            <li>3L Delta Three Phase no neutral</li>
                                            <li>2L-1n Single Split Phase: A & B Neutral</li>
                                            <li>2L Single Phase: A & B no neutral</li>
                                        </ul>
                                        <li><strong><a href='/docs/e51cx_full_i0d2.pdf' target='_blank'>Veris Meter
                                                                                                        Documentation</a></strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('footer.php'); ?>
    </div>
</div>
</body>
</html>