<?php
include_once ('functions/mysql_connect.php');
include_once('control/get_power_factor_control.php');
//print_r($FEED_NAME_DB);

$changed_id = mysqli_real_escape_string($conn, $_REQUEST['changed_id']);
$changed_id = filter_var($changed_id, FILTER_SANITIZE_STRING);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Adaptive Power Factor Settings</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }
        #powerFactorUpdated {
            text-align: center;
            color: green;
            font-size: large;
            visibility: hidden;
        }


    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js?<?php echo rand(); ?>"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        var changeID = <?php echo $changed_id; ?>;
        $(document).ready(function(){

            if(changeID == 1) {
                $("#powerFactorUpdated").css('visibility', 'visible').delay(5000).fadeOut();
            }
            
        });

    </script>



</head>
<body>

<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Set Adaptive Power Factor</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>



                    <div style="padding: 30px;">


                        <div class="row">
                            <div class="col-md-1"></div>

                            <div class="col-md-10">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Adaptive Power Factor</h3></div>
                                    <div class="panel-body">
                                        <div id="powerFactorUpdated">Power Factor Updated</div>
                                        <div class="panel-body">
                                            <form id="1powerFactorSetting" action="control/form_adaptive_power_factor.php" method="post">

                                                <h3> Enable Adaptive Power Factor:</h3>
                                                <label>Enabled:</label>
                                                <input type="checkbox" name="enabled" value="1" <?php if($enabled== 1) {echo 'checked';} ?>>
                                                <hr>
                                                <p>
                                                    <label>PF Target A:</label>

                                                    <input type="number" name="pf_target_A"  min="0.707" max="1" step="0.001" value="<?php echo $pf_target_A; ?>">
                                                </p>
                                                <p>
                                                    <label>Lagging:</label>
                                                    <input type="radio" name="pf_target_A_leading" value="1" <?php if($pf_target_A_leading== 1) {echo 'checked';} ?>> True
                                                    <input type="radio"  name="pf_target_A_leading" value="0" <?php if($pf_target_A_leading== 0) {echo 'checked';} ?>>False<br>
                                                </p>
                                                <p>
                                                    <label>Enabled:</label>
                                                    <input type="checkbox" name="pf_enable_A" value="1" <?php if($pf_enable_A== 1) {echo 'checked';} ?>>

                                                </p>
                                                <hr>
                                                <label>PF Target B:</label>

                                                <input type="number" name="pf_target_B"  min="0.707" max="1" step="0.001" value="<?php echo $pf_target_B; ?>">
                                                </p>
                                                <p>
                                                    <label>Lagging:</label>
                                                    <input type="radio" name="pf_target_B_leading" value="1" <?php if($pf_target_B_leading == 1) {echo 'checked';} ?>> True
                                                    <input type="radio"  name="pf_target_B_leading" value="0" <?php if($pf_target_B_leading == 0) {echo 'checked';} ?>>False<br>
                                                </p>
                                                <p>
                                                    <label>Enabled:</label>
                                                    <input type="checkbox" name="pf_enable_B" value="1" <?php if($pf_enable_B== 1) {echo 'checked';} ?>>
                                                </p>

                                                <hr>
                                                <label>PF Target C:</label>

                                                <input type="number" name="pf_target_C"  min="0.707" max="1" step="0.001" value="<?php echo $pf_target_C; ?>">
                                                </p>
                                                <p>
                                                    <label>Lagging:</label>
                                                    <input type="radio" name="pf_target_C_leading" value="1" <?php if($pf_target_C_leading == 1) {echo 'checked';} ?>> True
                                                    <input type="radio"  name="pf_target_C_leading" value="0" <?php if($pf_target_C_leading == 0) {echo 'checked';} ?>>False<br>
                                                </p>
                                                <p>
                                                    <label>Enabled:</label>
                                                    <input type="checkbox" name="pf_enable_C" value="1" <?php if($pf_enable_C== 1) {echo 'checked';} ?> >
                                                </p>
                                                <hr>
                                                <button id="set_adaptive_power_factor" class="btn btn-danger"  type="submit">Set Adaptive Power Factor</button>
                                            </form>
                                            <hr>

                            



                                        </div>
                                        

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>



                        </div>
                    </div>
          
            </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>