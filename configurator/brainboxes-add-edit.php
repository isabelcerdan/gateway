<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Brainboxes Device Configure</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/brainboxes-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>

    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

$target_interface = mysqli_real_escape_string($conn, $_REQUEST['target_interface']);
$target_interface = filter_var($target_interface, FILTER_SANITIZE_STRING);

$brainbox_id = mysqli_real_escape_string($conn, $_REQUEST['brainbox_id']);
$brainbox_id = filter_var($brainbox_id, FILTER_SANITIZE_STRING);

$model = mysqli_real_escape_string($conn, $_REQUEST['model']);
$model = filter_var($model, FILTER_SANITIZE_STRING);

$dual_status = mysqli_real_escape_string($conn, $_REQUEST['dual_status']);
$dual_status = filter_var($dual_status, FILTER_SANITIZE_STRING);



include_once ('control/get_brainboxes.php');
include_once ('control/get_meter_roles.php');


if ($model == "ES-413")
{
    // Clear id for second 413 //
    if($dual_status ==1) {
        $brainbox_id = '';
    }

    $dual_status++;

}
echo $dual_status;


?>


<div class="container">
  
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Brainboxes Device Configure</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_bainboxes_add_edit.php" method="post" id="brainboxesForm">
                        <input type="hidden" name="IOFlex_id" value="<?php echo $IOFlex_id; ?>">
                        <input type="hidden" name="dual_status" value="<?php echo $dual_status; ?>">
                        <input type="hidden" name="brainbox_id" value="<?php echo $brainbox_id; ?>">
                        <input type='hidden' name='model_number' value='ES-420'>


                        
                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">
                                <div class="form-group">

                                    <div class="col-md-12 " >
                                        <div class="formTextSpacing text-center">
                                            <h2> Add/Edit <?php echo $nickname . " ID: " . $target_interface; ?> Brainboxes</h2>
                                            <?php

                                            // Errors
                                            if ($changes != '') {
                                                echo"<p style='color: red;font-weight: bold'>$changes </p>";
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="formBoxSectionWhite">


                                <div class="formBoxSectionWhite">
                                    <div class="form-group">

                                        <label class="col-md-3 control-label">Target Interface:</label>
                                        <div class="col-md-3">
                                            <select name="target_interface" id="target_interface" class="form-control" required>

                                                <?php

                                                    // if nothing is set
                                                    if($target_interface == '') {
                                                        echo "<option value=''>Select</option>";
                                                    }
                                                    for($i =0; $i < count($meter_role_id);$i++ ) {
                                                        // display current setting
                                                       if($target_interface ==  $target_role_interface[$i]){
                                                           echo "<option value='$target_interface'>$target_interface </option>";
                                                        }
                                                        // Loop through list
                                                        echo "<option value='$target_role_interface[$i]'>$target_role_interface[$i]</option>";
                                                }

                                                ?>

                                            </select>

                                        </div>
                                        <div class="col-md-6 " >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="Target Interface" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong> Enter in the Target Interface
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">IP Address:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="IP_Address" id="IP_Address" class="form-control" value="<?php echo $IP_Address; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="IP Address" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>Please enter an IP Address.<br> Example 192.164.23.127

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Mac Address:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="mac_address" id="mac_address" class="form-control" value="<?php echo $mac_address; ?>" >

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Mac Address" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>Please enter in the devices Mac Address.<br> Example 00:0a:95:9d:68:16

                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Baud:</label>
                                        <div class="col-md-3">

                                            <select name="baud" class="form-control" required>
                                                <?php

                                                $selected_baud = 38400;
                                                $selected_text = "Default";
                                                if ($baud != '') {
                                                    $selected_baud = $baud;
                                                    $selected_text = "Current";
                                                }

                                                $bauds = array(300, 600, 1200, 2400, 4800, 9600, 14400, 28800, 38400, 57600, 115200);
                                                for ($i=0; $i < count($bauds); $i++) {
                                                    if ($bauds[$i] == $selected_baud)
                                                        echo "<option value='$bauds[$i]' selected>$bauds[$i] - $selected_text</option>";
                                                    else
                                                        echo "<option value='$bauds[$i]'>$bauds[$i]</option>";
                                                }
                                                ?>
                                            </select>


                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">
                                                <a  data-toggle="popover" title="Baud" data-content="The baud rate is the rate at which information is transferred in a communication channel. In the serial port context, 9600 baud means that the serial port is capable of transferring a maximum of 9600 bits per second."  >
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">tcp port:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="tcp_port" id="tcp_port" class="form-control" value="<?php  echo ($dual_status > 1) ? ''   : $tcp_port;?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="tcp_port" data-content=" "  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>Please enter an tcp port.<br>

                                            </div>
                                        </div>
                                    </div>

                                </div>
<!--
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">tcp_port_serial_1</label>
                                        <div class="col-md-3">
                                            <input type="text" name="tcp_port_serial_1" id="tcp_port_serial_1" class="form-control" value="<?php echo $tcp_port_serial_1; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="IP Address" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>This option is rare <br>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">tcp_port_serial_2</label>
                                        <div class="col-md-3">
                                            <input type="text" name="tcp_port_serial_2" id="tcp_port_serial_2" class="form-control" value="<?php echo $tcp_port_serial_2; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="IP Address" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>This option is rare <br>

                                            </div>
                                        </div>
                                    </div>

                                </div>
-->
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Apparent Part Number:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="apparent_part" id="apparent_part" class="form-control" value="<?php echo $apparent_part; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="Mac Address" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>Please enter an Apparent Part Number.<br>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                </div>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>