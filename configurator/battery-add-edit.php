<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Battery Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }


    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/battery_model-add-edit.js"></script>
    <script>
        $(document).ready(function(){

        });

    </script>
    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

include_once('control/get_model.php'); // Get default data
include_once('control/get_power_regulator.php');
include_once ('control/get_meter.php');

include_once ('functions/mysql_connect.php');

$battery_id = mysqli_real_escape_string($conn, $_REQUEST['battery_id']);
$battery_id = filter_var($battery_id, FILTER_SANITIZE_STRING);

$errors = mysqli_real_escape_string($conn, $_GET['errors']);
$errors = filter_var($errors, FILTER_SANITIZE_STRING);

if (strpos($errors, 'no_affected_rows') !== false) {
    $changes = "No Changes Made";
}

if($battery_id !='') {
    include_once ('control/get_battery_int.php'); // Get default data
}

?>


<div class="container">
    <div class="apparentBrand">Apparent igOS Gateway</div>
    <?php
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-8"><h1 style="padding-left: 30px;"> Add/Edit <?php echo $battery_name; ?> Battery </h1>

                        <?php
                        // Errors
                        if ($changes != '') {
                            echo"<p style='color: red;font-weight: bold'>$changes </p>";
                        }

                        ?>
                    </div>
                    <div class="col-md-64">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection">
                    <form class="form-horizontal row-border" action="/control/form_battery_add_edit.php" method="post" id="batteryConfigForm">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">
                        <input type="hidden" name="battery_id" value="<?php echo $battery_id; ?>">

                        
                                <!-- Beginning of tab 1 -->

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Battery Name:</label>
                                            <div class="col-md-3">
                                                <select name="model_id"  id="model_id" class="form-control" required >
                                                    < <?php
                                                    if($battery_name != ''){
                                                        echo "<option value='$battery_model_id'>$battery_name - Current</option>";
                                                    }else {
                                                        echo "<option value=''>Select</option>";
                                                    }

                                                    for ($i = 0; $i < count($model_id);$i++ ) {
                                                    echo "  <option value='$model_id[$i]'> $manufacturer[$i] - $model[$i]</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Meter Name" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">IP Address</label>
                                            <div class="col-md-3">



                                                    <input type="text"  name="ip_address" class="form-control"value="<?php echo $battery_ip_address; ?>" >

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="IP address" data-content="Provide the role that the meter will play in this installation. Examples:"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Please enter an IP Address. <br>Example 192.164.23.127
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label">MAC Address:</label>
                                            <div class="col-md-3">
                                                <input type="text"  name="mac_address" class="form-control"  value="<?php echo $battery_mac_address; ?>" >
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Meter Class"
                                                        data-content="Each type of meter monitors different types of power variables. For the meter to work correct the system must know what type of meter you are installing.  "  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>

                                                    </a>
                                                    <strong>Note:</strong>Please enter in the devices Mac Address.<br> Example 00:0a:95:9d:68:16
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="formBoxSectionWhite">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Power Regulator:</label>
                                                <div class="col-md-3">
                                                    <select name="power_regulator"  id="power_regulator" class="form-control" >

                                                    <?php


                                                    for($i =0; $i < count($power_regulator_idArray); $i++){
                                                        if($power_regulator_id != '') {
                                                            if ($battery_power_regulator_id == $power_regulator_idArray[$i]) {

                                                            echo "<option value='$battery_power_regulator_id'>$power_regulator_id - Current</option>";
                                                            }
                                                        }else {
                                                            echo "<option value=''>Select</option>";
                                                        }

                                                        echo "<option value='$power_regulator_idArray[$i]'>$pr_name_Array[$i] Current</option>";
                                                    }



                                                        //<

                                                        ?>


                                                    </select>

                                                </div>
                                                <div class="col-md-6" >
                                                    <div class="formTextSpacing">
                                                        <a  data-toggle="popover" title="Modbus Device Number" data-content="M"  ">
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                        </a>
                                                    </div>
                                                </div>


                                    </div>




                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Battery Meter:</label>
                                                <div class="col-md-3">
                                                    <select name="battery_meter_id"  id="battery_meter_id" class="form-control" >

                                                        <?php
                                                        if($meter_id == '') {
                                                            echo "<option value='' selected>Select</option>";
                                                        }

                                                        for($i =0; $i < count($meter_id_DB); $i++){
                                                            if($meter_id != '') {
                                                                if ($meter_id == $meter_id_DB[$i]) {

                                                                    echo "<option value='$meter_id' selected>$meter_role_DB[$i] - Current</option>";
                                                                }
                                                            }

                                                            echo "<option value='$meter_id_DB[$i]'>$meter_role_DB[$i]</option>";
                                                        }




                                                        ?>


                                                    </select>

                                                </div>
                                                <div class="col-md-6" >
                                                    <div class="formTextSpacing">
                                                        <a  data-toggle="popover" title="Modbus Device Number" data-content="M"  ">
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                        </a>
                                                    </div>
                                                </div>










                            </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>

                </div>
            </div>
        </div>
            <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>