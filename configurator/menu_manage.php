<?php
include_once ('functions/session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Manage Menus</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function (){

            // Store old value of access level //
            var olddAccessLevel = '';
            $(".change_access_level").click('focus', function () {
                olddAccessLevel = $("[name='"+this.id+"']").val();
                console.log("old " + olddAccessLevel);

            });

            // Change access level //
            $(".change_access_level").on('change', function() {
                var newAccessLevel = $("[name='"+this.id+"']").val();

                if (confirm('Are you sure you want to change access level to: '+newAccessLevel  )) {

                    $.ajax({
                        type: "POST",
                        url: '/control/form_menu_update.php?menu_id='+this.id+ ' &access_level='  +newAccessLevel,
                        success: function (data) {
                            console.log('success:');
                        },
                        error: function(jqXHR, text, error){
                            console.log('error:');
                        }
                    });
                    return false;

                }else {
                    // after cancel send old access level //
                    $(this).val(olddAccessLevel);
                }


            });

        });
    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var

include_once ('control/get_menu.php');



?>
<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Manage Menus</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">




                    <div class="formBoxSection form-horizontal row-border">

                        <div class="formBoxSection">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: smaller">
                                        <th class='text-center'>Menu ID</th>
                                        <th class='text-center'>Name</th>
                                        <th class='text-center'>Site</th>
                                        <th class='text-center'>URL</th>
                                        <th class='text-center'>Access Level( 1 = highest level)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for($i =0; $i < count($menu_id);$i++) {
                                         ?>
                                        <tr>
                                            <td class='text-center'><?php echo $menu_id[$i] ; ?></td>
                                            <td class='text-center'><?php echo $menu_name[$i]; ?></td>
                                            <td class='text-center'><?php echo $menu_site[$i] ; ?></td>
                                            <td class='text-center'><?php echo $menu_url[$i] ; ?></td>
                                            <td class='text-center'>
                                                <select id="access_level-<?php echo $menu_id[$i] ; ?>" name="access_level-<?php echo $menu_id[$i] ; ?>" class="change_access_level">
                                                    <option value="<?php echo $menu_access_level[$i]; ?>"><?php echo $menu_access_level[$i]; ?></option>
                                                    <option value="0">Hide All</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </tbody>
                            </table>



                        </div>
                    </div>

                </div>


                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>