<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

    $subscribers_id = mysqli_real_escape_string($conn, $_GET['subscribers_id']);
    $subscribers_id = filter_var($subscribers_id, FILTER_SANITIZE_STRING);

include_once('control/get_alarm_subscribers.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Alarm Notifications Add/Edit Users</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>



<div class="container">
 
    <?php

    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-7"><h1 style="padding-left: 30px;">Alarm Notifications Add/Edit Users</h1></div>
                    <div class="col-md-5">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>


                <div class="panel-body">
                    <form class="form-horizontal row-border" action="/control/form_alarm_notifications-users.php" method="post">
                        <?php
                            echo "<input type='hidden' name='subscribers_id' value='$subscribers_id'>";
                        ?>

                        <div class="formBoxSection ">
                            <div class="form-group  ">
                                <label class="col-md-3 control-label">Person to receive Alert:</label>
                                <div class="col-md-3 ">
                                    <input type="text"  name="subscribers_name"  id='subscribers_name'  class="form-control"value="<?php echo $subscribers_name; ?>" >
                                </div>
                                <div class="col-md-6" >
                                  <div class="formTextSpacing">
    
                                  </div>
                                </div>
                            </div>


                                <div class="form-group  ">
                                    <label class="col-md-3 control-label">Email Addreses:</label>
                                    <div class="col-md-3 ">
                                        <input name="email" type="email" id="email" class="form-control" placeholder="Email address" required value="<?php echo $email_address; ?>">
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                            </div>
    
                            <button type="submit" class="btn btn-primary" >Add User</button>

                    </form>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>