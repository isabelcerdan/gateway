<?php
include_once ('functions/mysql_connect.php');
include_once('control/form_inverter_display.php'); // Get default data

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Set Fixed Power Factor</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }




        /**
        * Featherlight Loader
        *
        * Copyright 2015, WP Site Care http://www.wpsitecare.com
        * MIT Licensed.
        */
        @-webkit-keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .featherlight-loading .featherlight-content {
            -webkit-animation: featherlightLoader 1s infinite linear;
        animation: featherlightLoader 1s infinite linear;
        background: transparent;
        border: 8px solid #8f8f8f;
        border-left-color: #fff;
        border-radius: 80px;
        width: 80px;
        height: 80px;
        min-width: 0;
        }

        .featherlight-loading .featherlight-content > * {
            display: none !important;
        }

        .featherlight-loading .featherlight-close,
        .featherlight-loading .featherlight-inner {
            display: none;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js?<?php echo rand(); ?>"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/js.cookie.js"></script>
    <script type="text/javascript" src="js/inverter-power-factor.js?<?php echo rand(); ?>"></script>
</head>
<body>

<div class="container">
    
    <?php
    // Header
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Set Fixed Power Factor<h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>
                <div class="panel-body">
                    <form method='post' action=" <?php echo $_SERVER['PHP_SELF']; ?> ">
                        <div style="margin: 5px; float: right" class="text-right"><label>Search:<input type="search" name="search" class="" placeholder="" ></label></div>
                    </form>
                    <div id="getCheckedinfo">Inverters Selected:<span id="invertersSelectedNum"></span> </div>

                    <form  method='post' action=" <?php echo $_SERVER['PHP_SELF']; ?> ">
                        <div style="margin: 5px;float: left" class="text-left"><label>Show # of items:
                                <select id="itemsperpage" name="itemsperpage" >
                                    <option value='4'" <?php if($limit == 4) {echo 'selected';} ?> >4</option>
                                    <option value='8'" <?php if($limit == 8) {echo 'selected';} ?> >8</option>
                                    <option value='16'" <?php if($limit == 16) {echo 'selected';} ?> >16</option>
                                    <option value='32'" <?php if($limit == 32) {echo 'selected';} ?> >32</option>
                                    <option value='64'" <?php if($limit == 64) {echo 'selected';} ?> >64</option>
                                    <option value='128'" <?php if($limit == 138) {echo 'selected';} ?> >128</option>
                                    <option value='256'" <?php if($limit == 138) {echo 'selected';} ?> >256</option>
                                    <option value='<?php echo $total; ?>'" <?php if($limit == $total) {echo 'selected';} ?>><?php echo $total; ?> - All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                    <div style="clear:both"> </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <p style="margin-top: 7px;"><input type="checkbox" id="select_all"/><span > Select All Visible</span></p>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-4  text-right">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-4  text-right">

                                <div style="margin: -10px;" class="text-left"><label>Select Group:</label>
                                    <select id="powerFactorGroup" name="powerFactorGroup" >
                                        <option value='No_Group'">No Group</option>
                                        <?php
                                        for($p = 0; $p < count($FEED_NAME_DB); $p++) {
                                            echo "<option value='$FEED_NAME_DB[$p]'>Feed: $FEED_NAME_DB[$p]</option>";
                                        }
                                        ?>
                                        <option value='All'" >All Inverters</option>

                                    </select>

                                </div>

                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-4  text-right">
                            <button id="set_power_factor" class="btn btn-danger"  >Set Power Factor</button>

                        </div>
                    </div>

                    <div class="formBoxSection ">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                                <tr >
                                    <th></th>
                                    <th class="text-center">Serial #<a style="float: right" href="set_inverter_power_factor.php?orderby=serialNumber&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?> "><div class="sortInvertersBoth"></div></a> </th>
                                    <th class="text-center">IP Address <a style="float: right" href="set_inverter_power_factor.php?orderby=IP_Address&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">MAC Address <a style="float: right" href="set_inverter_power_factor.php?orderby=mac_address&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Version<a style="float: right" href="set_inverter_power_factor.php?orderby=version&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Feed Name<a style="float: right" href="set_inverter_power_factor.php?orderby=FEED_NAME&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">SeT Power Factor<a style="float: right" href="set_inverter_power_factor.php?orderby=pf&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Leading<a style="float: right" href="set_inverter_power_factor.php?orderby=pf_c_lead&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">PF Enabled<a style="float: right" href="set_inverter_power_factor.php?orderby=pf_c_lead&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>

                                </tr>
                                <?php
                                //print_r($serialNumber);
                                // $rowId = 0;
                                for ($i = 0; $i < count($mac_address);$i++) {
                                    //$rowId++;
                                    ?>
                                    <input type="hidden" name="serialNumber[]" value="<?php echo $serialNumber[$i]; ?>">
                                    <tr >
                                        <td><input class="selectCheckBox checkbox"  id="select-<?php echo $inverters_id[$i];?>" type="checkbox" name="select-me" value="<?php echo $mac_address[$i]; ?>"></td>
                                        <td><?php echo $serialNumber[$i]; ?> </td>
                                        <td><?php echo $IP_Address[$i]; ?></td>
                                        <td><?php echo $mac_address[$i]; ?></td>
                                        <td><?php echo $version[$i]; ?></td>

                                        <td  id="feed-<?php echo  $inverters_id[$i]; ?>" class="inputForms">
                                            <?php echo    $FEED_NAME_inverter[$i]; ?>
                                        </td>
                                        <td >
                                            <?php echo $pf[$i]; ?>
                                        </td>
                                        <td >
                                            <?php echo $pf_c_lead[$i]; ?>
                                        </td>
                                        <td>
                                            <?php echo $pf_enable[$i]; ?>
                                        </td>

                                    </tr>
                                 
                                    <?php
                                    // End of loop //
                                }
                                ?>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <div class="text-center">
        <ul class="pagination">
            <?php
            if($num_page>1)
            {
                pagination($page,$num_page,$limit,$orderby);
            }
            ?>
        </ul>
    </div>
</body>
</html>