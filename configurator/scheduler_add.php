<?php
include_once ('functions/session.php');
//https://www.malot.fr/bootstrap-datetimepicker/demo.php

$task_id = mysqli_real_escape_string($conn, $_REQUEST['task_id']);
//$task_id= filter_var($task_id, FILTER_SANITIZE_STRING);

if($task_id != '') {
    include_once ('control/get_scheduler.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Scheduler add/edit</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.malot.fr" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->


    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.4/dt-1.10.15/b-1.3.1/cr-1.3.3/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    -->


    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>



</head
<body>
<?php
include_once ('functions/mysql_connect.php');

?>

<script type="text/javascript" class="init">


    $(document).ready(function (){

        $(".form_datetime").datetimepicker({
            format: "yyyy-mm-dd hh:ii",
            autoclose: true,
            todayBtn: true,
            minuteStep: 15
        });


        $('.icon-arrow-right').addClass('glyphicon-arrow-right');
        $('.icon-arrow-left').addClass('glyphicon-arrow-left');

        <?php
        // Get database info
        $ids = explode('-', $task_id);
        if($ids[1] == '') {
            echo '$("#recurringOptions").hide();';
        }
        if($unitDB != 'week'){
        echo '$("#weekOptions").hide();';
        }
        if($endsDB == 'on'){
            echo '$("#endsOn").show();';
        }elseif ($endsDB == 'after'){
            echo '$("#endsAfter").show();';
        }else {
            echo '$("#endsOn").hide();';
            echo '$("#endsAfter").hide();';
        }
        ?>



        $("#recurring").click(function() {
            var checked = $(this).is(':checked');
            if (checked) {
                $("#recurringOptions").show();
            } else {
                $("#recurringOptions").hide();
            }
        });

        $("#unit").change(function () {
            var unit = this.value;
            if(unit == 'week') {
                $("#weekOptions").show();
            }else {
                $("#weekOptions").hide();
            }
        });
        $("#ends").change(function () {
            var ends = this.value;
            if(ends == 'on') {
                $("#endsOn").show();
            }else {
                $("#endsOn").hide();
            }
            if(ends == 'after') {
                $("#endsAfter").show();
            }else {
                $("#endsAfter").hide();
            }

        });

    });


</script>
<div class="container">
    
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Scheduler</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                
                <div class="formBoxSection">
                    <form action="/control/form_scheduler_add_edit.php"  method="post">
                        <input type="hidden" name="task_id" value="<?php echo $task_id; ?>">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Task Name:</label>
                                <div class="col-sm-10">
                                    <input id='taskName' name='taskName' size="20" type="text" value="<?php echo $nameDB; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label"> Start Cmd:</label>
                                <div class="col-sm-10">
                                    <input id='start_cmd' name='start_cmd' size="50" type="text" value="<?php echo $start_cmdDB; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Start Date:</label>
                                <div class="col-sm-10">
                                    <div class="input-append date form_datetime" data-date="">
                                        <input id='startDate' name='startDate' size="20" type="text" value="<?php echo $start_dateDB; ?>" required> Select Date
                                        <span class="add-on"><i class="glyphicon  icon-th"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label"> Enabled:</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="enabled"  id="enabled" value="1" <?php if(($enabledDB == '1')) {echo 'checked';}  ?>>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label"> Recurring:</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="recurring"  id="recurring" value="yes" <?php if(($ids[1] != '')) {echo 'checked';}  ?>>
                                </div>
                            </div>
                        <div id="recurringOptions">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Repeat Every:</label>
                                <div class="col-sm-10">
                                    <input id='period' name='period' size="5" type="text"  value="<?php if($periodDB < 1){echo 1;}else echo $periodDB; ?> ">
                                    <span > <strong style="padding-right: 5px;padding-left: 10px;"> </strong>
                                        <select name="unit" id="unit">
                                            <?php
                                            // Get database info
                                            if($task_id != '') {
                                                echo "<option value='$unitDB' >$unitDB</option>";
                                            }
                                            ?>
                                        <option value="minute">Minute</option>
                                        <option value="hour">Hour</option>
                                        <option value="day">Day</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                        <option value="year">Year</option>
                                    </select>
                                        </span>
                                </div>




                            </div>


                        <div id="weekOptions">
                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="mo" value="1" <?php if($moDB == 1) {echo 'checked';}  ?>> Monday </option>
                                    <input type="checkbox" name="tu" value="1" <?php if($tuDB == 1) {echo 'checked';}  ?>> Tuesday </option>
                                    <input type="checkbox" name="we" value="1" <?php if($weDB == 1) {echo 'checked';}  ?>> Wednesday </option>
                                    <input type="checkbox" name="th" value="1" <?php if($thDB == 1) {echo 'checked';}  ?> > Thursday </option>
                                    <input type="checkbox" name="fr" value="1" <?php if($frDB == 1) {echo 'checked';}  ?> > Friday </option>
                                    <input type="checkbox" name="sa" value="1" <?php if($saDB == 1) {echo 'checked';}  ?> > Saturday </option>
                                    <input type="checkbox" name="su" value="1" <?php if($suDB == 1) {echo 'checked';}  ?> > Sunday </option>

                                </div>
                            </div>
                        </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Ends:</label>
                                    <div class="col-sm-10">
                                        <select name="ends" id="ends">
                                            <?php
                                            // Get database info
                                            if($task_id != '') {
                                                echo "<option value='$endsDB' >$endsDB</option>";
                                            }
                                            ?>
                                            <option value="never">never</option>
                                            <option value="on">on</option>
                                            <option value="after">after </option>
                                        </select>
                                    </div>
                                </div>
                            <div id="endsOn">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Ends On</label>
                                <div class="col-sm-10">
                                    <div class="input-append date form_datetime" data-date="">
                                        <input id='endsOn' name='endsOn' size="20" type="text"  value="<?php echo $ends_onDB; ?>" > Select Date
                                        <span class="add-on"><i class="glyphicon icon-th"></i></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div id="endsAfter">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label"> After:</label>
                                    <div class="col-sm-10">
                                        <input id='ends_after' name='ends_after' size="20" type="text" value="<?php echo $ends_afterDB; ?>" >  Occurrences
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>







                        </div>
                    </div>
                    </form>
                </div>



            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>





</div>
</body>
</html>