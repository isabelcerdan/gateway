<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 5/16/2016
 * Time: 1:25 PM
 */
$home = '';
$feed = '';
$meter = '';

if (strpos($_SERVER['PHP_SELF'], 'feed') !== false) {
    $feed_menu = 'active';
}
elseif ((strpos($_SERVER['PHP_SELF'], 'system') !== false)) {
    $system_menu  = 'active';
}
elseif ((strpos($_SERVER['PHP_SELF'], 'meter') !== false)) {
    $meter_menu  = 'active';
}
elseif ((strpos($_SERVER['PHP_SELF'], 'brainboxes') !== false)) {
    $brainboxes_menu  = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'inverters.php') !== false) {
    $inverter_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'users') !== false) {
    $users_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'energyreview') !== false) {
    $energyreview_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'main') !== false) {
    $home_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'bucket') !== false) {
    $bucket_menu = 'active';
}

elseif (strpos($_SERVER['PHP_SELF'], 'auto_detect_st_fe') !== false) {
    $auto_detect_st_fe_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'power_factor') !== false) {
    $pf_menu_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'alarm') !== false) {
    $alarms_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'scheduler') !== false) {
    $scheduler_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'battery') !== false) {
    $battery_menu = 'active';
}
elseif (strpos($_SERVER['PHP_SELF'], 'help') !== false) {
    $help_menu = 'active';
}
else {
    $home_menu = 'active';
}
if($stringSight != 1) {
?>

<ul class="nav nav-tabs">
    <li class="<?php echo $home_menu; ?>"><a href="main.php">Home</a></li>
    <li class="<?php echo $inverter_menu; ?>"><a href="inverters.php">Inverters</a></li>
    <li class="<?php echo $meter_menu; ?>"><a href="meter.php">Meters</a></li>
    <li class="<?php echo $feed_menu; ?>"><a href="power_regulator.php">Power Regulators</a></li>
    <li class="<?php echo $brainboxes_menu; ?>"><a href="brainboxes.php">Brainboxes</a></li>
    <li class="<?php echo $auto_detect_st_fe_menu; ?>"><a href="auto_detect_st_fe.php">Detect</a></li>
    <li class="<?php echo $energyreview_menu; ?>"><a href="energyreview.php">ER</a></li>
    <li class="<?php echo $bucket_menu; ?>"><a href="bucket.php">Bucket</a></li>
    <li class="<?php echo $users_menu; ?>"><a href="users.php">Users</a></li>
    <li class="<?php echo $system_menu; ?>"><a href="system.php">Site</a></li>
    <li class="<?php echo $alarms_menu; ?>"><a href="alarms.php">Alarms</a></li>
    <li class="<?php echo $pf_menu_menu; ?>"><a href="power_factor.php">PF</a></li>
    <li class="<?php echo $scheduler_menu; ?>"><a href="scheduler.php">Scheduler</a></li>
    <li class="<?php echo $battery_menu; ?>"><a href="battery.php">Battery</a></li>
    <li class="<?php echo $help_menu; ?>"><a href="help.php">Help</a></li>
</ul>
    <?php
}
?>