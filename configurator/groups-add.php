<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Groups</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }

        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/system-add-edit.js"></script>


    <script src="js/jquery-ui-1.11.4.min.js"></script>



</head
<body>
<?php
include_once('functions/mysql_connect.php');
// Init meter var
$group_id  = mysqli_real_escape_string($conn, $_GET['group_id']);
$group_id = filter_var($group_id, FILTER_SANITIZE_STRING);


include_once('control/get_groups.php'); // Get default data


?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Group Settings</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_groups_inverters.php" method="post" id="systemForm">


                        <div class="formBoxSection">

                            <div class="formBoxSection">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Group:</label>
                                    <div class="col-md-3">
                                        <select name="group_id"  id="group_id" class="form-control" required >
                                            < <?php
                                            if($group_id != ''){
                                                for($i =0;$i <  count($group_id);$i++ )
                                                {
                                                    echo "<option value='$group_id[$i]'>$group_id[$i] - $alias[$i]</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Log level" data-content="log-level">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Select the group you want to edit
                                        </div>
                                    </div>
                                </div>



                            </div>





                            <div class="formBoxSection">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Enter Serial Numbers<br> IP or Mac Addresses</label>

                                    <div class="col-md-3">
                                        <textarea class="form-control" rows="6" id="inverters" name="inverters"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Note: </strong> Select the group you want to edit
                                    </div>
                                </div>


                                </div>


                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>


            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>