<?php
include_once ('functions/session_end.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>


<div class="container">

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <a href="/index.php"  class="btn btn-default btn-sm buttonAlignRight" role="button">
                        <span class="glyphicon glyphicon-log-in"></span> Log in
                    </a>


                </div>

                <div class="panel-body">



                    <div class="formBoxSection">
                       <h2 class="text-center"> You are now logged out of the system.</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerBottomPage">
            <?php    include_once ('footer.php'); ?>
        </div>
    </div>
    <!-- Row end -->

</div>
</body>
</html>