<?php
include_once ('functions/session.php');

$error= mysqli_real_escape_string($conn, $_REQUEST['error']);
$error = filter_var($error, FILTER_SANITIZE_STRING);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Battery</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>

    <script type="text/javascript">
        $( document ).ready( function () {
            $(document).on("click", "a.delete-battery", function() {
                if (confirm('Are you sure  you want to delete ' + this.id + ' Battery?' )) {
                    $(location).attr('href', '/control/form_battery_add_edit.php?battery_id=' + this.id + '&delete_battery=yes');
                }
            });
        } );
    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$meter_id = '';
include_once ('control/get_battery_int.php'); // Get default data
//include_once ('control/get_brainboxes.php'); // Get default data


?>
<div class="container">
   
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Setup Battery</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

            <?php if ($error == 'no_feed'): ?>

                <div style="padding-left: 30px; padding-right: 30px;">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <strong>Error: </strong> You have to create a feed before you can create a battery
                            </div>
                        </div>

                    </div>
                </div>
            <?php endif; ?>
                <div class="panel-body">

                    <?php

                                for($i = 0;$i <  count($battery_id); $i++) {
                                echo '<h3 style="margin-left: 40px"><a href="battery_model_' . $battery_model_id[$i] . '.php"> ' . $battery_name[$i]. ' </a><br></h3> ';
                                }

                    ?>

                    <div class="formBoxSection">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="font-size: smaller">
                                    <th></th>
                                    <th></th>
                                    <th class='text-center'>Battery ID</th>
                                    <th class='text-center'>Battery Name</th>
                                    <th class='text-center'>Model ID</th>
                                    <th class='text-center'>ID Address</th>
                                    <th class='text-center'>MAC Address</th>
                                    <th class='text-center'>Power Regulator</th>
                                </tr>
                                </thead>
                                <tbody id="auto_brainbox_status_div">
                                <?php
                                for($i = 0;$i <  count($battery_id); $i++) {
                                    echo "<tr>
                                            <td class='text-center'><a href='battery-add-edit.php?battery_id=$battery_id[$i]'> <button class=\"btn btn-warning\" >Edit</button></a></td>
                                            <td class='text-center'><a class='delete-battery' id='$battery_id[$i]' > <button class=\"btn btn-danger\" >Delete</button></a></td>
                                            <td class='text-center'>$battery_id[$i]</td>
                                            <td class='text-center'><a href='battery_model_" .$battery_model_id[$i] .".php'> $battery_name[$i]</a></td>
                                            <td class='text-center'>$battery_model_id[$i]</td>
                                            <td class='text-center'>$battery_ip_address[$i]</td>
                                            <td class='text-center'>$battery_mac_address[$i]</td>
                                            <td class='text-center'>$power_regulator_id[$i]</td>
                       
                                    ";
                                    echo "</tr>";
                                }

                                ?>

                                </tbody>
                            </table>
                        </div>


                    </div>




                    <div class="formBoxSection">

                        <a href='battery-add-edit.php'> <button class="btn btn-primary" >New Battery</button></a>
                  


                    </div>

                </div>
            </div>


            </div>
        </div>
    <?php    include_once ('footer.php'); ?>
</div>
    <!-- Row end -->

</div>
</body>
</html>