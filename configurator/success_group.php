<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php
include_once('functions/mysql_connect.php');
$group_id = mysqli_real_escape_string($conn, $_GET['group_id']);
$group_id= filter_var($group_id, FILTER_SANITIZE_STRING);



$badInverters = unserialize($_GET['badInverters']);
$notInGatInverters = unserialize($_GET['notInGatInverters']);
$goodInverters = unserialize($_GET['goodInverters']);
// $badInverters= mysqli_real_escape_string($conn, );
//$badInverters = filter_var($badInverters, FILTER_SANITIZE_STRING);


?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review Meter</h3>
                    <div class="text-right">
                        <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border">
                        <input type="hidden" name="gateway_id" value="<?php echo $group_id; ?>">

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Add to Group:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $group_id; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Inverters Added:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">

                                        <?php
                                        foreach ($goodInverters as $goodInverter) {
                                            echo filter_var($goodInverter, FILTER_SANITIZE_STRING)  ." <br>";
                                        }

                                        ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Inverters Not in Gateway: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php
                                        foreach ($notInGatInverters as $NotInInverter) {
                                            echo filter_var($NotInInverter, FILTER_SANITIZE_STRING) ." <br>";
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Bad Inverters Serial Numbers: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php
                                        foreach ($badInverters as $badInverters) {
                                            echo filter_var(mysqli_real_escape_string($conn, $badInverters), FILTER_SANITIZE_STRING)." <br>";
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

















                        </div>





                    <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                </div>
            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>
