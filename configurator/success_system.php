<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php
include_once('functions/mysql_connect.php');
$gateway_id = mysqli_real_escape_string($conn, $_GET['gateway_id']);
$gateway_id = filter_var($gateway_id, FILTER_SANITIZE_STRING);

$region= mysqli_real_escape_string($conn, $_GET['region']);
$region = filter_var($region, FILTER_SANITIZE_STRING);

if($region == ''){
    $region = 'No Change';
}

include_once('control/get_system.php');
/*
$result = $conn->query("SELECT * FROM gateway WHERE gateway_name = '$gateway_name_raw'");


while($row = $result->fetch_assoc()) {
    $gateway_name = $row['gateway_name'];
    $total_output_limit =  $row['total_output_limit'];
    $address =  $row['address'];
    $city =  $row['city'];
    $state=  $row['state'];
    $postcode=  $row['postcode'];
}

$conn->close();
*/
?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review Meter</h3>
                    <div class="text-right">
                        <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_name; ?>">

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gateway Name:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $gateway_name; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Total Output Limit:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $total_output_limit; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $address; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">City: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $city; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">State: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $state; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postcode: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $postcode; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Log Level: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $log_level; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Perspective: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $gateway_perspective; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Operations Mode: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $operation_mode; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Region: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $region; ?>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                    <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                </div>
            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>
