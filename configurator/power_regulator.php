<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Power Regulator</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>

    <?php
    include_once('functions/mysql_connect.php');
    include_once('control/get_system.php');
    include_once('control/get_feed_list.php'); // Get default data
    include_once('control/get_meter.php'); // Get default data



    $sql = "SELECT ramp_rate_knee, ramp_rate_slope, violation_response_factor FROM gateway";
    $result = mysqli_query($conn, $sql);
    $row = $result->fetch_assoc();
    $ramp_rate_knee = $row['ramp_rate_knee'];
    $ramp_rate_slope = $row['ramp_rate_slope'];
    $violation_response_factor = $row['violation_response_factor'];


    // lets not allow user to set meter feed limits on solar meter for now
    unset($meters['solar']);

    $stringResponse = '<strong>Failure</strong> You must first add a <strong>PCC</strong> Meter before you can create a Power Regulator';
    $alertString =  'alert-danger';

    ?>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#prError').hide();
            var yesMeter = false;
            <?php

            for($i=0;$i < count($meter_id_DB); $i++){
                if(strtolower($meter_role_DB[$i])  == 'pcc'){
                    echo "yesMeter = true; ";
                    break;
                }
            }

            ?>


            $('[data-toggle="popover"]').popover();

            $(document).on("click", "a.delete-feed", function () {
                if (confirm('Are you sure you want to delete power regulator ' + this.id + '?')) {
                    $(location).attr('href', '/control/form_power_regulator.php?id=' + this.id + '&delete_feed=yes');
                }
            });
            $('#new-power-regulator-button').on("click", function (ev) {
                if(yesMeter == true){
                    $(location).attr('href', '/feed-add-edit.php');
                    ev.preventDefault();
                }else{
                    $('#prError').show();
                    // alert('You msut first add a Meter before you can create a Power Regulator');
                }

            });

            $('#power-shaping-btn').on('click', function (ev) {
                $('#span-ramp-rate-knee').toggle();
                $('#span-ramp-rate-slope').toggle();
                $('#span-violation-response').toggle();
                $('#input-ramp-rate-knee').toggle();
                $('#input-ramp-rate-slope').toggle();
                $('#input-violation-response').toggle();

                if ($.trim($('#power-shaping-btn').text()) != "Submit") {
                    $('#power-shaping-btn').text("Submit");
                } else {
                    var slope = parseFloat($('#input-ramp-rate-slope').val());
                    var knee = parseFloat($('#input-ramp-rate-knee').val());
                    var violation_response = parseFloat($('#input-violation-response').val());
                    var data = {slope: slope, knee: knee, violation_response: violation_response};
                    $.post('/control/power-shaping-edit.php', data, function(data, textStatus) {
                        $('#span-ramp-rate-knee').text(data.knee);
                        $('#span-ramp-rate-slope').text(data.slope);
                        $('#span-violation-response').text(data.violation_response);
                    }, 'json');
                    $('#power-shaping-btn').text("Edit");

                }
            });
        });
    </script>

    <style>
        .smallerText {
            font-size: xx-small;
        }

        .popover.right {
            margin-left: 70px !important;
            margin-top: 15px;
            width: 300px;
        }

        .table {
            margin-bottom: 0px;
        }
    </style>


</head>
<body>
<div class="container">
    <?php
    // Header
    include_once ('header.php');

    include_once('menu.php'); ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-md-6"><h1 style="padding-left: 30px;">Power Regulators</h1></div>
                        <div class="col-md-6">
                            <div class="text-right" style="margin: 20px;">
                                <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                    <span class="glyphicon glyphicon-log-out"></span> Log out
                                </a>
                            </div>
                        </div>
                    </div>
                    <style>
                        .power-shaping-control {
                            width: 50px;
                        }
                    </style>
                    <div style="padding: 30px;">

                        <?php
                        //if( ( $stringDetect != '') OR ($stringFirst== 0)) {
                        ?>
                        <div id="prError">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert <?php echo $alertString; ?>" ?>
                                        <?php echo $stringResponse; ?></php>
                                    </div>
                                </div>
                            </div>
                            <?php
                            // }
                            ?>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="formBoxSection">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr style="font-size:smaller">
                                                <th class="col-md-2"></th>
                                                <th class="col-md-2">
                                                    <span style="float: left; padding-right: 5px">Inverter Ramp Rate Slope</span>
                                                    <a data-toggle="popover" title="Ramp Rate Slope"
                                                       data-content="Determines how quickly the inverter ramp rate is adjusted as we approach the target. Counter-intuitively, a lower slope value will ramp faster, at the expense of potentially overshooting the target and causing oscillations and instability."
                                                    ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                    </a>
                                                </th>
                                                <th class="col-md-2">
                                                    <span style="float: left; padding-right: 5px">Inverter Ramp Rate Knee</span>
                                                    <a data-toggle="popover" title="Ramp Rate Knee"
                                                       data-content="Determines when the algorithm starts to creep towards its target at its slowest ramp rate of 0.1%/s. A higher knee value means the algorithm will begin creeping earlier, making it take longer to reach its target but potentially preventing overshoot."
                                                    ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                    </a>
                                                </th>
                                                <th class="col-md-2">
                                                    <span style="float: left; padding-right: 5px">Violation Response Factor</span>
                                                    <a data-toggle="popover" title="Violation Response Factor"
                                                       data-content="Determines how aggressively the algorithm responds to a violation of the threshold. A violation response factor > 1.0 will respond more aggressively (greater curtailment of solar power). A violation response factor < 1.0 will respond less aggressively."
                                                    ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                    </a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <button type="submit" class="btn btn-warning"
                                                            id="power-shaping-btn">
                                                        Edit
                                                    </button>
                                                </td>
                                                <td>
                                                    <input class="form-control power-shaping-control" id="input-ramp-rate-slope" style="display:none;" value="<?php echo $ramp_rate_slope ?>"/>
                                                    <span id="span-ramp-rate-slope"><?php echo $ramp_rate_slope ?></span>
                                                </td>
                                                <td>
                                                    <input class="form-control power-shaping-control" id="input-ramp-rate-knee" style="display:none;" value="<?php echo $ramp_rate_knee ?>"/>
                                                    <span id="span-ramp-rate-knee"><?php echo $ramp_rate_knee ?></span>
                                                </td>
                                                <td>
                                                    <input class="form-control power-shaping-control" id="input-violation-response" style="display:none;" value="<?php echo $violation_response_factor ?>"/>
                                                    <span id="span-violation-response"><?php echo $violation_response_factor ?></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <img width=100% src="/images/inverter-ramp-rate.jpg"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="formBoxSection">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr style="font-size: smaller">
                                            <th></th>
                                            <th>Name</th>
                                            <th>Feed</th>
                                            <th>Has Solar</th>
                                            <th>Has Storage</th>
                                            <th>Enabled</th>
                                        </tr>
                                        </thead>
                                        <tbody id="auto_brainbox_status_div">
                                        <?php
                                        for ($i = 0; $i < count($power_regulator_name); $i++) {
                                            echo "<tr>
                                            <td class='text-left'><a href='feed-add-edit.php?id=$power_regulator_id[$i]'> <button class=\"btn btn-warning\" >Edit</button></a>
                                            <a class='delete-feed' id='$power_regulator_id[$i]' > <button class=\"btn btn-danger\" >Delete</button></a></td>
                                            <td class='text-left'>$power_regulator_name[$i]</td>
                                            <td class='text-left'>$feed_name[$i]</td>
                                            <td class='text-left'>$has_solar[$i]</td>
                                            <td class='text-left'>$has_storage[$i]</td>
                                             <td class='text-left'>$daemon_control_pr_enabled[$i]</td>
                                          </tr>";
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form class="form-horizontal row-border" action="feed-add-edit.php" method="post">
                                <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">
                                <div class="formBoxSection">
                                    <div class="form-group">
                                        <div class="col-md-12" style="padding-top: 10px;" text-align="center" >
                                            <button id="new-power-regulator-button" type="button" class="btn btn-primary">New Power Regulator</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once('footer.php'); ?>
        </div>
    </div>
</body>
</html>