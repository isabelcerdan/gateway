<?php
include_once ('functions/mysql_connect.php');
include_once ('functions/session.php');
//

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inverter List</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }




        /**
        * Featherlight Loader
        *
        * Copyright 2015, WP Site Care http://www.wpsitecare.com
        * MIT Licensed.
        */
        @-webkit-keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .featherlight-loading .featherlight-content {
            -webkit-animation: featherlightLoader 1s infinite linear;
            animation: featherlightLoader 1s infinite linear;
            background: transparent;
            border: 8px solid #8f8f8f;
            border-left-color: #fff;
            border-radius: 80px;
            width: 80px;
            height: 80px;
            min-width: 0;
        }

        .featherlight-loading .featherlight-content > * {
            display: none !important;
        }

        .featherlight-loading .featherlight-close,
        .featherlight-loading .featherlight-inner {
            display: none;
        }
        #inverterdata {
            font-size: 12px;
        }

        .infoBoxSection {
            border: 1px solid #a1a1a1;
            padding: 8px;
            width: 98%;
            border-radius: 15px;
            margin: 0px;
        }
        .centered-text {
            text-align:center
        }
    </style>

    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->


    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/js.cookie.js"></script>


    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="js/buttons.html5.min.js"></script>





    <script type="text/javascript" src="js/string-sight-saved.js?<?php echo rand(); ?>">" class="init"></script>


</head>
<body>
<div class="container">
    <div class="apparentBrand">Apparent igOS Gateway</div>
    <?php
    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Inverters</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <table id='controlButtons' class="table" width="100%">

                    <tr>
                        <td width="50%"><div style="margin-left: 25px; margin-top: 10px; ">Inverters:<span id="inverter_count_div"></span> </div></td>
                        <td width="30%"></td>
                        <td width="10%"></td>
                        <td width="10%"></td>
                        <td width="10%"></td>
                        <td width="10%"></td>
                        <td width="10%"><button id="delete-inverters" type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal">Delete Inverters</button></td>

                    </tr>

                </table>
                <div class="panel-body">

                    <div style="clear:both"> </div>
                    <div class="text-right" ><a href="stringSight.php" >Back to StringSight Main Page</a></div>

                    <div class="formBoxSection">

                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">

                                <table id="inverterdata" class="display" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" name="select_all" value="1" id="inverterdata-select-all-Saved"></th>
                                        <th>Group</th>
                                        <th>Serial</th>
                                        <th>Mac</th>
                                        <th>Version</th>
                                        <th>String ID</th>
                                        <th>String Pos</th>


                                    </tr>
                                    </thead>

                                </table


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <div class="text-center">
        <ul class="pagination">
            <?php
            if($num_page>1)
            {
                pagination($page,$num_page,$limit,$orderby);
            }
            ?>
        </ul>
    </div>
</body>
</html>