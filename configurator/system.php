<?php
include_once('functions/session.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Global System Settings</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }

        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/system-add-edit.js"></script>


    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function () {
            /*
            $("#inverter_fallback_power_setting").change(function(){
               var powerSettings = $("#inverter_fallback_power_setting").val();
                alert('Are you really sure that you want to change the default power settings on the inverters to: ' + powerSettings + ' ?');
            });
*/
            $("#operation_mode").change(function(){
                var mode = $("#operation_mode").val();
                var r = confirm('Are you really sure that you want to change the the Gateway to : ' + mode + ' Mode? ');
                if (r == true) {

                }
            });

            $("#ntp_url_sg424").change(function(){
                var timeServer = $("#ntp_url_sg424").val();
                var r = confirm('Are you really sure that you want to change the NTP Server to : ' + timeServer + ' ? ');
                if (r == true) {

                }
            });

            $('[data-toggle="popover"]').popover();
        });



    </script>

</head
<body>
<?php
include_once('functions/mysql_connect.php');
// Init meter var

include_once('control/get_system.php'); // Get default data
include_once('control/get_universal_aif.php');

$ntp = mysqli_real_escape_string($conn, $_GET['ntp']);
$ntp= filter_var($ntp, FILTER_SANITIZE_STRING);

if($ntp == 'no'){
    $ntp_Errror = true;
}

?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Site Settings</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_system.php" method="post" id="systemForm">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">

                        <div class="formBoxSection">
                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Gateway Name:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="gateway_name" id="gateway_name" class="form-control"
                                               value="<?php echo $gateway_name; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the Gateway a unique name
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Log Level:</label>
                                    <div class="col-md-3">
                                        <select id="log-level" name="log_level" class="form-control">
                                            <option value="DEBUG" <?php if ($log_level == "DEBUG") echo "selected"; ?>>
                                                DEBUG
                                            </option>
                                            <option value="INFO" <?php if ($log_level == "INFO") echo "selected"; ?>>
                                                INFO
                                            </option>
                                            <option value="WARN" <?php if ($log_level == "WARN") echo "selected"; ?>>
                                                WARN
                                            </option>
                                            <option value="ERROR" <?php if ($log_level == "ERROR") echo "selected"; ?>>
                                                ERROR
                                            </option>
                                            <option value="CRITICAL" <?php if ($log_level == "CRITICAL") echo "selected"; ?>>
                                                CRITICAL
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Log level" data-content="log-level">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please select a log-level: "regat" is needed to take
                                                                                                                effect.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Perspective:</label>
                                    <div class="col-md-3">
                                        <select id="gateway-perspective" name="gateway_perspective" class="form-control">
                                            <option value="production" <?php if ($gateway_perspective == "production") echo "selected"; ?>>
                                                Production
                                            </option>
                                            <option value="consumption" <?php if ($gateway_perspective == "consumption") echo "selected"; ?>>
                                                Consumption
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Gateway Perspective"
                                               data-content='This setting does not change the behavior of power algorithms
                                               but rather changes the rendered polarity of certain meter readings in the
                                               GUI. Meter feed limits and curtail targets will automatically be inverted
                                               to follow changes to gateway perspective.'>
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note:</strong> "Production" is +export/-import; "Consumption" is +import/-export.
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Operations Mode:</label>
                                    <div class="col-md-3">
                                        <select name="operation_mode" id="operation_mode" class="form-control">
                                            <option value="power_control_mode" <?php if ($operation_mode == "power_control_mode") echo "selected"; ?>>
                                                Active Control Mode
                                            </option>
                                            <option value="observation_mode" <?php if ($operation_mode == "observation_mode") echo "selected"; ?>>
                                                Observation Mode
                                            </option>
                                        </select>



                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Operations Mode"
                                               data-content="operation_mode">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong>Operations mode tells the Gateway to either be in Full Control Mode (NXO), or Observer Mode that takes the inverters out of Gateway control and the inverters make as much power as they can without any Gateway interference.
                                        </div>
                                    </div>
                                </div>

                            </div>


                            

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Region Name:</label>
                                    <div class="col-md-3">
                                        <select name="region_name" id="region_name" class="form-control">
                                            <option value="CA120" <?php if ($region_nameDB == "CA120") echo "selected"; ?>>
                                                CA120 :28 (1.00,0,0)
                                            </option>
                                            <option value="HI120" <?php if ($region_nameDB == "HI120") echo "selected"; ?>>
                                                HI120 :12 (0.95,0,1)
                                            </option>
                                            <option value="IEEE" <?php if ($region_nameDB == "IEEE") echo "selected"; ?>>
                                                IEEE :10 (1.00,0,0)
                                            </option>
                                            <option value="NA120" <?php if ($region_nameDB == "NA120") echo "selected"; ?>>
                                                NA120 :10 (1.00,0,0)
                                            </option>
                                            <option value="MFG120" <?php if ($region_nameDB == "MFG120") echo "selected"; ?>>
                                                MFG120  :50 (1.00,0,0)
                                            </option>
                                            <option value="BATT_NA120" <?php if ($region_nameDB == "BATT_NA120") echo "selected"; ?>>
                                                BATT NA120 :22 (1.00,0,0)
                                            </option>
                                            <option value="BATT_HI120" <?php if ($region_nameDB == "BATT_HI120") echo "selected"; ?>>
                                                BATT HI120  :23 (0.95,0,1)
                                            </option>
                                            <option value="BATT_CA120" <?php if ($region_nameDB == "BATT_CA120") echo "selected"; ?>>
                                                BATT CA120 :24 (1.00,0,0)
                                            </option>
                                        </select>



                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Operations Mode"
                                               data-content="operation_mode">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Region name :SW_PROFILE_VALUE  (Pf,PF Current Leading,PF enabled)
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="address" id="address" class="form-control"
                                               value="<?php echo $address; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Address" data-content="Address">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the Address of the Site
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">City:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="city" id="city" class="form-control"
                                               value="<?php echo $city; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="City" data-content="City">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the city of the Site
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">State:</label>
                                    <div class="col-md-3">
                                        <select name="state" id="state" class="form-control">
                                            <?php
                                            if ($state != '') {
                                                echo "<option value='$state'>$state - Current</option>";
                                            }
                                            ?>
                                            <option value="">Select a State</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                            <option value="NOT USA">Not USA</option>
                                        </select>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="State" data-content="State">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the state of the Site
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Postcode:</label>
                                    <div class="col-md-3">
                                        <input type="text" name="postcode" id="postcode" class="form-control"
                                               value="<?php echo $postcode; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Post Code" data-content="Post Code">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the postcode of the Site
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>


            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>