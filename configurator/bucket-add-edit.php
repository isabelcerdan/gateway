<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Energy Bucket Add / Edit</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/brainboxes-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>

    <script>


        $( document ).ready( function () {

            $( "#bucket" ).validate( {
                rules: {

                    Bucket_Coefficient:{
                        required: true,
                        range:[0,0]
                    }
                },
                messages: {

                    gateway_name: "Please enter a Gateway Name",

                },
                errorElement: "em",
                errorPlacement: function ( error, element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".col-md-3" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".col-md-3" ).addClass( "has-success" ).removeClass( "has-error" );
                }
            } );


        } );

    </script>
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

$feed = mysqli_real_escape_string($conn, $_REQUEST['feed']);
$feed = filter_var($feed, FILTER_SANITIZE_STRING);
$feed_display = $feed;


include_once ('control/get_gatewayMeter.php');


?>


<div class="container">

    <?php
    include_once ('header.php');

    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Bucket Feed Configuration</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form name='bucket' id='bucket' class="form-horizontal row-border" action="/control/form_bucket.php" method="post" >
                        <input type="hidden" name="feed" value="<?php echo $feed_display; ?>">


                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Feed:</label>
                                        <div class="col-md-3 formTextSpacing"><?php echo $feed_display; ?> </div>
                                        <div class="col-md-6 " ></div>
                                    </div>
                                </div>


  <!--                              <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bucket Coefficient:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="Bucket_Coefficient" id="Bucket_Coefficient" class="form-control" value="<?php echo $Bucket_Coefficient; ?>" >
                                        </div>
                                        <div class="col-md-6 " >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
-->
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bucket Size:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="Bucket_Size" id="Bucket_Size" class="form-control" value="<?php echo $Bucket_Size; ?>" >
                                        </div>
                                        <div class="col-md-6 " >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Bucket Period:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="Bucket_Period" id="Bucket_Period" class="form-control" value="<?php echo $Bucket_Period; ?>" >
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="" data-content=""  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bucket Enabled:</label>
                                        <div class="col-md-3">
                                            <select name="Bucket_Enabled" class="form-control" required>
                                                <?php
                                                if($Bucket_Enabled != ''){
                                                    echo "<option value='$Bucket_Enabled'>$Bucket_Enabled - Current</option>";
                                                }
                                                ?>
                                                <option value='1'>Enable</option>
                                                <option value='0'>Disable</option>
                                            </select>

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>

                                            </div>
                                        </div>
                                    </div>

                                </div>
<!--

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Reinitialize_Bucket:</label>
                                            <div class="col-md-3">
                                                <select name="Reinitialize_Bucket" class="form-control" required>
                                                    <option value='0'>No</option>
                                                    <option value='1'>Yes</option>
                                                </select>

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="" data-content="."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
-->




                                <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>