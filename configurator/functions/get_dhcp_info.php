<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 5/31/2017
 * Time: 10:46 AM
 */

function getDHCPInfo($getHeader='no', $file = "/etc/dnsmasq.d/dnsmasq-gateway.conf")
{

    $myfile = fopen($file, "r");
    $dhcpFileInfo = fread($myfile, filesize($file));
    fclose($myfile);
    $dhcpFileLine = explode("=",$dhcpFileInfo);
    if($getHeader != 'yes'){
        $dhcpFileLine[1];
        $dhcpFileLineContents = explode(",",$dhcpFileLine[1]);
        return $dhcpFileLineContents;   
    }else {
        return str_replace('dhcp-range', '', $dhcpFileLine[0]);;
        }
    
}
?>