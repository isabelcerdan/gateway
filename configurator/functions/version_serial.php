<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 3/24/2017
 * Time: 11:35 AM
 */
include_once('mysql_connect.php');
$serialNumber = '';
$gateway_base_sn = '';
$result = $conn->query("SELECT * FROM energy_report_config");
if (mysqli_num_rows($result) > 0) {
    while ($row = $result->fetch_assoc()) {
        $gateway_base_sn = $row['gateway_base_sn'];
    }
}
if($gateway_base_sn != '' ) {
    $serialNumber = 'Serial Number: ' . $gateway_base_sn;
}
$version = filter_var(file_get_contents("/usr/share/apparent/VERSION"), FILTER_SANITIZE_STRING);
echo "<span class='colorDarkGray smallerText' >Software Version: $version $serialNumber</span>";

?>