<?php
/**
 * Created by PhpStorm.
 * User: Patrice
 * Date: 3/3/2017
 * Time: 2:52 PM
 *
 * Configure brainbox by sending an HTTP GET request to its web interface.
 * Set timeout to 5 seconds.
 *
 * http://10.252.2.2//port.cgi?portmode=1&port=1&bh=&bdl=&baud=38400
 *     &databits=8&parity=1&stopbits=1&fifo=1&duplex=2&protocol=0&tcpport=9001
 *     &timeout=0&trel=1&rip1=192&rip2=168&rip3=0&rip4=0&rtcpport=9001&btnApply=Apply+Changes
 */

function brainbox_config(&$config) {
    $error = null;

    // if baud is not set, use default value of 38400
    if ($config['baud'] == '')
        $config['baud'] = 38400;

    // auto-configure serial port 1 on device
    $url = "http://" .  $config['ip_address'];
    $url = $url . "/port.cgi?portmode=1&port=1&bh=&bdl=&baud=" . $config['baud'];
    $url = $url . "&databits=8&parity=1&stopbits=1&fifo=1&duplex=2&protocol=0&tcpport=" . $config['tcp_port'];
    $url = $url . "&timeout=0&trel=1&rip1=192&rip2=168&rip3=0&rip4=0&rtcpport=" . $config['tcp_port'];
    $url = $url . "&btnApply=Apply+Changes";
    $context = stream_context_create(array('http'=>
        array('timeout' => 5)
    ));

    if (file_get_contents($url, false, $context) === FALSE)
        $error = $config['mac_address'];

    return $error;
}

?>