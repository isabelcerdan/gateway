<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/19/2016
 * Time: 2:45 PM
 */
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/17/2016
 * Time: 4:49 PM
 * .htaccess file for restricting IP address
 * sudo vi default-ssl.conf
 *
 *   <Directory /usr/share/apparent/www/gui>
SSLOptions +StdEnvVars
AllowOverride All
</Directory>
 * sudo service apache2 restart
 */

$safe = true;

include_once ('mysql_connect.php');

// Get white list of safe IP addresses
$result = $conn->query("SELECT * FROM ip_address_white");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $IP_Address_White[] = $row['IP_Address'];
    }
}

for($i = 0;$i < count($IP_Address_White); $i++ ){
    //
    if(($IP_Address_White[$i] ==  $_SERVER['REMOTE_ADDR'])) {
        //echo "<br> you are good; " . $_SERVER['REMOTE_ADDR'];
        $safe = true;
        break;
    }
}

if($safe == true) {
    //echo '<br>safe';
    //header('Location: /index.php?errors=passwordproblems' );
}else {
    $_SESSION["login"] = "logged_no";
    //echo '<br>unsafe';
    header('Location: /index.php?errors=location' );
    exit();
}

?>