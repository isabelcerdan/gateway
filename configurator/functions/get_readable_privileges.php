<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 5/16/2016
 * Time: 3:58 PM
 */

function get_readable_privileges($privilegesDB) {
    $privilegesDB = filter_var($privilegesDB, FILTER_SANITIZE_STRING);
    switch ($privilegesDB) {
        case "1":
            $human_privileges = 'Administrator';
            break;
        case "2":
            $human_privileges = 'Manager';
            break;
        case "3":
            $human_privileges = 'Installer';
            break;
        case "4":
            $human_privileges = 'Monitor';
            break;
        default:
            $human_privileges = '';
    }
return $human_privileges;
}
?>