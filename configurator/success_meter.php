<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php
include_once ('functions/mysql_connect.php');
$meter_id = mysqli_real_escape_string($conn, $_GET['meter_id']);
$meter_id = filter_var($meter_id, FILTER_SANITIZE_STRING);

include_once ('control/get_meter.php');

?>


<div class="container">
    <?php
    // Menu Link //
    // Header
    include_once ('header.php');
    
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review Meter</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meter Name:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                          <?php echo $meter_name; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Meter Type</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $meter_type; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meter Role:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                            <?php echo $meter_role; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meter Class:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                            <?php echo $meter_class; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Modbus Device Number:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $modbusdevicenumber; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Interface Address:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $address; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Baud :</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $baud ; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Network Comm Type: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $network_comm_type; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Meter Delta: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $meter_delta; ?>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing"></div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">CT Orientation Reversed: </label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $ct_orientation_reversed; ?>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing"></div>
                                </div>
                            </div>
                        </div>
                        </div>


                        <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                    </div>
                </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>

</div>
    <!-- Row end -->

</div>
</body>
</html>
