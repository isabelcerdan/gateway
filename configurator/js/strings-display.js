/**
 * Created by AdamCadieux on 7/26/2016.
 */


$(document).ready(function() {

    var StringCheckedBoxes =[];
    var stringCookie = null;
    var stringsSelected = '';



    $('#scan-alert').click(function(){

        $('#scan-alert').hide();
    });

    function unique(list) {

        list.sort()
        var result = [];
        $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        // remove empty places
        result = result.filter(v=>v!='');
        return result;
    }

    $('.run_fd_now').on('click', function() {

        if($(this).attr('id') == 'run_later') {
            alert("Make sure that you remember to turn on the Feed Detection Audit.");
        }
        // Clear out checkboxes, and cookies //
        tempStringId = StringCheckedBoxes;
        StringCheckedBoxes = [];
        // Cookies.set('stringCookie',StringCheckedBoxes);
        Cookies.set('stringCookie', '');
        $('.selectCheckBox').attr('checked', false);
        $('#select_all').attr('checked', false);
        $("#stringsSelectedNum").html('');
        $.featherlight({iframe: '/feed_progress.php?stringId='+tempStringId+'&enabled='+this.id, iframeMaxWidth: '100%', iframeWidth: 800,iframeHeight: 800});

    });

    //
    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change

        if($("#select_all").is(':checked')) {
            $.each($('.selectCheckBox'), function(index, value) {
                serialID=this.id.split('--');
                StringCheckedBoxes.push(serialID[1]);
            });

            stringCookie = Cookies.get('stringCookie');
            if ( stringCookie != null ) {
                var array = stringCookie.replace(/[\[\]'\"]+/g,'').split(',');
                StringCheckedBoxes = unique(StringCheckedBoxes.concat(array));
                //console.log('43: ' +StringCheckedBoxes )
            }else {
                // console.log('IS  0: ' +StringCheckedBoxes )
            }
            Cookies.set('stringCookie',StringCheckedBoxes);

            // Cookies.set('stringCookie', StringCheckedBoxes);

            $(".checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status

        }

        else {

            //StringCheckedBoxes.length = '';
            StringCheckedBoxes = [];
            Cookies.set('stringCookie', '');
            console.log('delete:' +StringCheckedBoxes );

            $(".checkbox").prop('checked', false); //change all ".checkbox" checked status
        }

        $("#stringsSelectedNum").html(StringCheckedBoxes.length);

        $("#stringsEstimatedTime").html(StringCheckedBoxes.length * 4.5 + ' minutes');

    });

    //".checkbox" change
    $('.checkbox').change(function(){

        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all").prop('checked', false); //change "select all" checked status to false

        }
        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all").prop('checked', true);

        }
    });

    


    //manually clear storage //
    $('#itemsperpage').on('change', function() {
        this.form.submit();
    });
    // keep array up to date when page reloads


    if(Cookies.get('stringCookie') == undefined) {
        Cookies.set('stringCookie', '');
        console.log('cookies: undefined' );
    }else {
        console.log('cookies: defined' );
    }

    if ( stringCookie == null ) {
        console.log('no stringCookie');
    }
   stringCookie = Cookies.get('stringCookie');
    if ( stringCookie != null ) {
        // StringCheckedBoxes = unique(StringCheckedBoxes.concat(JSON.stringify(stringCookie) ));
        var array = stringCookie.replace(/[\[\]'\"]+/g,'').split(',');

        StringCheckedBoxes = unique(StringCheckedBoxes.concat(array));

    }



    $("#stringsSelectedNum").html(StringCheckedBoxes.length);
    $("#stringsEstimatedTime").html(StringCheckedBoxes.length * 4.5 + ' minutes');
    for(var i = 0; i < StringCheckedBoxes.length; i++) {
        $( "#select--" + StringCheckedBoxes[i]).prop('checked', true);
    }
    // Alert to show what items are in checked list //
    $('#getCheckedinfo').click(function(){
        arrayView = '';
        macList ='';
        for(var i = 0; i < StringCheckedBoxes.length; i++) {
            arrayView = arrayView + '\n' + StringCheckedBoxes[i];
            if($('#select-'+i).val() === undefined) {
                console.log('undefined');
            }else {
                macList = macList+ '\n' + $('#select-'+i).val();
            }
        }

    });
    $('.selectCheckBox').change(function(){
        serialID=this.id.split('--');

        //mac = $('#'+this.id).val();
        serialNumber = serialID[1];

        if ($("#select--"+serialID[1]).is(":checked"))
        {
            // Add items to the checked list
            if(StringCheckedBoxes.indexOf(serialID[1]) == -1) {
                StringCheckedBoxes.push(serialNumber);
            }
        }else {
            // Remove items from the checked list
            var index = StringCheckedBoxes.indexOf(serialNumber);
            if (index != -1)
                StringCheckedBoxes.splice(index, 1);
        }
        // print out the number of items checked //
        $("#stringsSelectedNum").html(StringCheckedBoxes.length);
        $("#stringsEstimatedTime").html(StringCheckedBoxes.length * 4.5 + ' minutes');
        // Store inverter array in local memory//

        Cookies.set('stringCookie', StringCheckedBoxes);
        console.log('String:' + StringCheckedBoxes)

    });
    $('.inputForms').on('change', function() {
        formIdSplit=this.id.split('--');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        $.ajax({
            type: "POST",
            url: '/control/form_inverters_update.php?inverters_id=' +StringCheckedBoxes,
            data: formDetails.serialize(),
            success: function (data) {
                console.log('success:' + formDetails.serialize());
                // clean up //
                for(var i = 0; i < StringCheckedBoxes.length; i++) {
                    $( "#select--" + StringCheckedBoxes[i]).prop('checked', false);
                }
                StringCheckedBoxes = '';
                //localStorage.setItem("checkboxList", '');
                window.location.reload(true);
            },
            error: function(jqXHR, text, error){
                console.log('error:');
            }
        });
        return false;

    });

    $('.delete').on('click', function() {
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);

        var r = confirm("Are you sure you want to delete these inverters?");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: '/control/form_inverters_update.php?delete=yes&inverters_id='  +StringCheckedBoxes,
                data: formDetails.serialize(),
                success: function (data) {
                    console.log('success:' + formDetails.serialize());
                    // clean up //
                    for(var i = 0; i < StringCheckedBoxes.length; i++) {
                        $( "#select-" + StringCheckedBoxes[i]).prop('checked', false);
                    }
                    StringCheckedBoxes = '';
                    //localStorage.setItem("checkboxList", '');
                    window.location.reload(true);
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                }
            });
            return false;
        }

        /*


         */
    });
});

