/**
 * Created by AdamCadieux on 5/18/2016.
 */
$(document).ready(function () {

    var FeedInputVal;


    //update all fields when one changes //

    $(".inputUpdate").change(function () {
        console.log('inputUpdate Called');
    });

    $("#select-feed-name").change(function() {
        if (String(this.value) == "Aggregate") {
            $(".feed-settings").show();
        } else {
            $(".feed-settings").hide();
            $("#feed-settings-" + this.value).show();
        }
    });

    $("#select-feed-name").change();


    function direction(value) {
        var perspective = $("#gateway-perspective").val();
        if ((perspective == "consumption" && value >= 0) || (perspective == "production" && value < 0)) {
            return "import";
        } else {
            return "export";
        }
    }

    var curtialLow = -100;
    var curtialHigh = 100;

    if(gatewayPerspective == 'production'){
        var curtialLow = -100;
        var curtialHigh = 0;

    }else{
        var curtialLow = 0;
        var curtialHigh = 100;
    }
/*
    $("#feedForm").validate({
        rules: {
            SYSTEM_SIZE: {
                required: true,
                min: 0.25
            }
        },
        messages: {
            CURTAIL_TARGET: {
                required: "Please enter your Curtail Target",
                range: "Please enter values between the range of -100 to 100"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-3").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-3").addClass("has-success").removeClass("has-error");
        }
    });
*/
    $.validator.addMethod('integer', function (value, element, param) {
        return (value != 0) && (value == parseInt(value, 10));
    }, 'Please enter a non zero integer value!');

    var curtialLow = -100;
    var curtialHigh = 100;

    if(gatewayPerspective == 'production'){
        var curtialLow = 0;
        var curtialHigh = 100;
    }else{
        var curtialLow = -100;
        var curtialHigh = 0;
    }

    


});