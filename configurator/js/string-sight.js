/**
 * Created by AdamCadieux on 12/7/2017.
 */

var InvertersCheckedBoxes =[];
var inverterCookie = null;
var pageLength = 50;
var StringDetectCounter = 0;
var myVar;

$(document).ready(function (){

    auto_inverter_count();
    setInterval(auto_inverter_count,10000);

    function auto_inverter_count(){
        $.ajax({
            url: "control/get_inverters_count.php",
            cache: false,
            success: function(data){
                $("#inverter_count_div").html(data);
            }
        });
    }

    inverterCookie = Cookies.get('inverterCookie');
    if ( inverterCookie != null ) {
        // InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(JSON.stringify(inverterCookie) ));
        var array = inverterCookie.replace(/[\[\]'\"]+/g,'').split(',');
        InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));

    }

    $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);





    $("#inverterdata-select-all").change(function(){  //"select all" change

        $("input[name='id[]']:checked").each( function () {
            // InvertersCheckedBoxes.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxes.push($(this).val());
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        });
        $("input[name='id[]']:not(:checked)").each( function () {
            // InvertersCheckedBoxes.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxes.push($(this).val());
            inverterCookie = Cookies.get('inverterCookie');
            InvertersCheckedBoxes.length = '';
            InvertersCheckedBoxes = [];
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        });


    });

    setInterval( function () {
        table.ajax.reload( null, false );
    }, 5000 );

    $('#inverterdata').on( 'load', 'tr', function () {
        alert( 'Row index: '+table.row( this ).index() );
    } );

    var table =    $('#inverterdata').DataTable( {
        "pageLength": pageLength,
        'info': true,
        "lengthMenu": [ 10, 25, 50, 75, 100, 200, 500 ],
        "ajax": "/control/inverters_json.php",
        "columns": [
            { 'data': 'inverters_id'},
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { 'data': 'inverters_id',

                "render": function ( data, type, row ) {
                    return '<span class="glyphicon glyphicon-list-alt inverterQuery" id="query-'  + data +' "></span>';
                }
            },

            { 'data': 'serialNumber'},
            { 'data': 'mac_address'},
            { 'data': 'IP_Address'},
            { 'data': 'version'},
            { 'data': 'stringId'},
            { 'data': 'stringPosition'},
            //  { 'data': 'prodPartNo'},
            // { 'data': 'comm','className': "comm"},
            { 'data': 'comm',
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        if(data =='DC_FULL_PWR' ){
                            return ' <span style="color:GREEN" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'"  ></span>';
                        }
                        if(data =='DC_NIGHT' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='DC_OTHER' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='DC_RAMP' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='UNDER_PERF' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='BOOT_LOADER' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='BOOT_UPDATER' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NEEDS_ATTENTION' ){
                            return ' <span style="color: orange" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NO_COMM' ){
                            return ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='LEGACY' ){
                            return ' <span style="color: blue" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='MGI_220' ){
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='UNKNOWN' ){
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        else {
                            return ' <span style="color: gray" class=" glyphicon glyphicon-one-fine-dot"  title="Not Mapped: ' + data +'" ></span>';
                        }

                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }
            }, { 'data': 'watts'},
            { 'data': 'das_boot'},
            { 'data': 'inverters_id'

                ,
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        return '<button id="delete-'  + data +' " type="button" class="delete">Delete</button>';
                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }

            }

        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                check = '';

                if($.inArray(data, InvertersCheckedBoxes) > -1){
                    check = 'checked';
                }

                return '<input '+check+' class="inverterCheckbox" type="checkbox" name="id[]" id="select-'+data+'"  value="' + $('<div/>').text(data).html() + '">';
            }
        }],
        'order': [[1, 'asc']]
    } );

    // Handle click on "Select all" control
    $('#inverterdata-select-all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#inverterdata tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#inverterdata-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    // Add Remove items from inverter list //
    $('#inverterdata tbody').on( 'click', '.inverterCheckbox', function () {
        console.log($(this).is(':checked') + ' ' + $(this).val() );
        var removeMe = $(this).val();
        if($(this).is(':checked')  == true )
        {
            //console.log ('true:' + $(this).is(':checked') + ' ' + $(this).val() )
            InvertersCheckedBoxes.push($(this).val());
            console.log('Add: '+InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        }else {

            InvertersCheckedBoxes = jQuery.grep(InvertersCheckedBoxes, function(value) {
                return value != removeMe ;
            });
            console.log('subtract: ' +InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes );
            removeMe = '';
        }

    } );



    $('#inverterdata').on('submit', function(e){
        var form = this;

        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
            // If checkbox doesn't exist in DOM
            if(!$.contains(document, this)){
                // If checkbox is checked
                if(this.checked){
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                    );
                }
            }
        });

    });

    //$('.inverterQuery tbody').on('click', function() {
    $('#inverterdata tbody').on( 'click', '.inverterQuery', function () {


        formIdSplit=this.id.split('-');
        var  inverters_id = formIdSplit[1];

        $.featherlight({
            iframe: '/inverter_query.php?inverters_id='+inverters_id, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800,afterClose : function(event){
                //console.log("after close" +this); // this contains all related elements
                //alert(this.$content.hasClass('true')); // alert class of content
                window.location.reload();
            }
        });


    });

    $('#health-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/health_scan.php', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){
                    //console.log("after close" +this); // this contains all related elements
                    //alert(this.$content.hasClass('true')); // alert class of content
                    window.location.reload();
                }
            });
        }
    });

    $('.detectString').on('click', function(e) {
        var StringDetectCounter = 0;
        $("#getSerailResults").html('<div class="text-warning font-weight-bold">Started String Detect</div>');

        //var myVar = setInterval(stringDetect, 1000);

        var counter = 0;

        var looper = setInterval(function(){
            counter++;
            console.log("Counter is: " + counter);
            stringDetect();
            $("#getSerailResults").html('<div class="text-warning font-weight-bold">Running String Detect</div>');
            if (counter >= 10)
            {
                clearInterval(looper);
                console.log("Finished String Detect!!!" );
                $("#getSerailResults").html('<div class="text-warning font-weight-bold ">Finished String Detect</div>');
            }

        }, 5000);

        auto_get_health();
    });



    function stringDetect() {
        StringDetectCounter = StringDetectCounter +1;
        console.log("running String detect" + StringDetectCounter);

        $.ajax({
            url: "control/form_string_detect.php?string=yes&stringDetect=1&stringSight=yes",
            cache: false,
            success: function(data) {
                // $("#getSerailResults").html('<div class="text-success font-weight-bold">Finished String Detect</div>');
                // $("#getSerailResults").html('<div class="text-warning font-weight-bold">Running String Detect</div>');
            }
        });


    }

    function auto_get_health(){
        console.log('running health scan');
        $.ajax({
            url: "control/set_inverter_health.php",
            cache: false,
            success: function(data){

            }
        });
    }


    $('#serial-scan').on('click', function() {
        //  console.log('well it was clicked...')
        $("#getSerailResults").html('<div class="text-warning font-weight-bold">Started Serial Number Scan</div>');
        $.ajax({
            url: "control/set_serial_scan.php",
            cache: false,
            success: function(data){
                console.log(data);
                $("#getSerailResults").html('<div class="text-success font-weight-bold">Finished Serial Number Scan</div>');
            }
        });

    });




    $('#upgrade-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        //https://github.com/noelboss/featherlight/#installation

        // Clear out checkboxes, and cookies //
        tempSInvertersId = InvertersCheckedBoxes;
        InvertersCheckedBoxes = [];
        Cookies.set('inverterCookie', '');
        $('.selectCheckBox').attr('checked', false);
        $("#select_all").prop('checked', false);
        //console.log('IDs: ' +tempSInvertersId )
        $("#invertersSelectedNum").html('');
        $.featherlight({iframe: '/upload_upgrade.php?inverters_id='+tempSInvertersId, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800});
    });


    $('#inverterdata tbody').on( 'click', '.delete', function () {

        $('.loading-overlay').show();
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);

        var r = confirm("Are you sure you want to delete these inverters?");
        if (r == true) {

            $.ajax({
                type: "POST",
                url: '/control/form_inverters_update.php?delete=yes&inverters_id='  +formID,
                data: formDetails.serialize(),
                success: function (data) {
                    $('.loading-overlay').hide();
                    console.log('success:' + formDetails.serialize());

                    //localStorage.setItem("checkboxList", '');
                    window.location.reload(true);
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                    $('.loading-overlay').hide();
                }
            });
            return false;
        }

    });

    $('.deleteAll').on('click', function() {
        $('.loading-overlay').show();

        var r = confirm("Are you sure you want to delete ALL inverters? That means all inverters in the Database");
        if (r == true) {

            $.ajax({
                type: "POST",
                url: '/control/form_inverters_delete_all.php?deleteAll=yes',
                success: function (data) {
                    $('.loading-overlay').hide();

                    //InvertersCheckedBoxes = '';
                    InvertersCheckedBoxes = [];
                    //localStorage.setItem("checkboxList", '');
                    window.location.reload(true);
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                    $('.loading-overlay').hide();
                }
            });
            return false;
        }
    });

    $('.save').on('click', function() {
        $('.loading-overlay').show();

        var r = confirm("Are you sure you want to Save and Delete these inverters?");
        if (r == true) {
            $("#getSerailResults").html('<div class="text-success font-weight-bold">Saving Inverters</div>');
            $.ajax({
                url: "/control/form_copy_inverters.php",
                cache: false,
                success: function(data){
                    var res = data.split(",");

                    if(res.length >1){
                        alert('Looks like you have already saved some of these Inverters. Please delete the duplicates before saving (they should all be checked)')
                    }
                    console.log(res[0] + " |" + res[1] );
                    for(i=0; i <res.length; i++) {
                        InvertersCheckedBoxes.push(res[i]);
                        console.log('Add: '+InvertersCheckedBoxes );
                        Cookies.set('inverterCookie',InvertersCheckedBoxes);
                        $( "#select-"+res[i]).prop('checked', true);
                    }

                }
            });


        }

    });
    $('#inverterdata tbody').on( 'change', '.feed_change', function () {
        //$('.selectpicker .feed_change').on('click', function() {
        var FEED_NAME =   table.$("[name='FEED_NAME']").val();
        console.log('inverter ID:'+InvertersCheckedBoxes + ' FEED_NAME:' + FEED_NAME);
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        $.ajax({
            type: "POST",
            url: '/control/form_inverters_update.php?inverters_id=' +InvertersCheckedBoxes +'&FEED_NAME=' + FEED_NAME,
            success: function (data) {
                console.log('success:' + formDetails.serialize());
                // clean up //
                for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
                    $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', false);
                }
                InvertersCheckedBoxes = [];
                //localStorage.setItem("checkboxList", '');
                window.location.reload(true);
            },
            error: function(jqXHR, text, error){
                console.log('error:');
            }
        });
        return false;

    });

    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
        $( ".inverterCheckbox #select-" + InvertersCheckedBoxes[i]).prop('checked', true);
    }

});


function unique(list) {

    list.sort()
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    // remove empty places
    result = result.filter(v=>v!='');
    return result;
}
