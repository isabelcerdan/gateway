/**
 * Created by AdamCadieux on 7/26/2016.
 */

$(document).ready(function() {

    var InvertersCheckedBoxes =[];
    var inverterCookie = null;
    var invertersSelected = '';

    function unique(list) {

        list.sort()
        var result = [];
        $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        // remove empty places
        result = result.filter(v=>v!='');
        return result;
    }

    $('#upgrade-inverters').on('click', function() {
          //  console.log('well it was clicked...')
        //https://github.com/noelboss/featherlight/#installation

        // Clear out checkboxes, and cookies //
        tempSInvertersId = InvertersCheckedBoxes;
       InvertersCheckedBoxes = [];
        Cookies.set('inverterCookie', '');
        $('.selectCheckBox').attr('checked', false);
        $("#select_all").prop('checked', false);
        //console.log('IDs: ' +tempSInvertersId )
        $("#invertersSelectedNum").html('');
        $.featherlight({iframe: '/upload_upgrade.php?inverters_id='+tempSInvertersId, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800});
    });

    $('#health-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/health_scan.php', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){
                    //console.log("after close" +this); // this contains all related elements
                    //alert(this.$content.hasClass('true')); // alert class of content
                    window.location.reload();
                }
            });
        }
    });

    $('#inverters-start-db-con').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/control/form_start_db_con_audit.php?run_db_const_auditor=YES', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){
                    //console.log("after close" +this); // this contains all related elements
                    //alert(this.$content.hasClass('true')); // alert class of content
                    window.location.reload();
                }
            });
        }
    });


    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change

        if($("#select_all").is(':checked')) {
            $.each($('.selectCheckBox'), function(index, value) {
                serialID=this.id.split('-');
                InvertersCheckedBoxes.push(serialID[1]);
            });

            inverterCookie = Cookies.get('inverterCookie');
            if ( inverterCookie != null ) {
                var array = inverterCookie.replace(/[\[\]'\"]+/g,'').split(',');
                InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));
               //console.log('43: ' +InvertersCheckedBoxes )
            }else {
               // console.log('IS  0: ' +InvertersCheckedBoxes )
            }
            Cookies.set('inverterCookie',InvertersCheckedBoxes);

           // Cookies.set('inverterCookie', InvertersCheckedBoxes);

            $(".checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
           // console.log('checked:' +InvertersCheckedBoxes );
        }

        else {

            InvertersCheckedBoxes.length = '';
            InvertersCheckedBoxes = [];
            Cookies.set('inverterCookie', '');
            //console.log('69 invertersSelectedNum:' +InvertersCheckedBoxes );

            $(".checkbox").prop('checked', false); //change all ".checkbox" checked status
        }
        //console.log('73 invertersSelectedNum:' +InvertersCheckedBoxes.length );
        $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);

    });

    //".checkbox" change
    $('.checkbox').change(function(){

        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all").prop('checked', false); //change "select all" checked status to false

        }
        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all").prop('checked', true);

        }
    });

    $('#upgrade-cancel').on('click', function() {

        if (confirm("Cancel Upgrade") == true) {
            $.ajax({
                type: "POST",
                url: 'control/upgrade/upload_cancel.php?cancel_upgrade=yes',
                success: function (data) {
                    console.log('cancel:' );
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                }
            });
            return false;
        }


    });


    //manually clear storage //
    $('#itemsperpage').on('change', function() {
        this.form.submit();
    });
    // keep array up to date when page reloads

    inverterCookie = Cookies.get('inverterCookie');
    if ( inverterCookie != null ) {
       // InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(JSON.stringify(inverterCookie) ));
        var array = inverterCookie.replace(/[\[\]'\"]+/g,'').split(',');

        InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));

    }

    console.log('128 invertersSelectedNum:' +InvertersCheckedBoxes );
    $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);
    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
        $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', true);
    }
    // Alert to show what items are in checked list //

    $('#getCheckedinfo').click(function(){
        arrayView = '';
        macList ='';
        for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
            arrayView = arrayView + '\n' + InvertersCheckedBoxes[i];
            if($('#select-'+i).val() === undefined) {
                console.log('undefined');
            }else {
                macList = macList+ '\n' + $('#select-'+i).val();
            }
        }

    });
    $('.selectCheckBox').change(function(){
        serialID=this.id.split('-');
        //mac = $('#'+this.id).val();
        serialNumber = serialID[1];
        if ($("#select-"+serialID[1]).is(":checked"))
        {
            // Add items to the checked list
            if(InvertersCheckedBoxes.indexOf(serialID[1]) == -1)  {

               InvertersCheckedBoxes.push(serialNumber);
            }
        }else {
            // Remove items from the checked list
            var index = InvertersCheckedBoxes.indexOf(serialNumber);
            if (index != -1)
                InvertersCheckedBoxes.splice(index, 1);
        }
        // print out the number of items checked //
        console.log('166 invertersSelectedNum:' +InvertersCheckedBoxes );
        $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);
        // Store inverter array in local memory//

        Cookies.set('inverterCookie', InvertersCheckedBoxes);

    });
    $('.inputForms').on('change', function() {
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        $.ajax({
            type: "POST",
            url: '/control/form_inverters_update.php?inverters_id=' +InvertersCheckedBoxes,
            data: formDetails.serialize(),
            success: function (data) {
                console.log('success:' + formDetails.serialize());
                // clean up //
                for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
                    $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', false);
                }
                InvertersCheckedBoxes = [];
                //localStorage.setItem("checkboxList", '');
                window.location.reload(true);
            },
            error: function(jqXHR, text, error){
                console.log('error:');
            }
        });
        return false;

    });

    $('.loading').hide();


    
    $('.detectString').on('click', function(e) {
       // alert('Finding Strings... ');
  /*
        $.ajax({
            type: "POST",
            url: 'control/form_auto_detect_st_fe.php?string=yes&stringDetect=1',
            //data: formDetails.serialize(),
            success: function (data) {
                console.log('success:');
     
            },
            error: function(jqXHR, text, error){
            
                console.log('error:');

            }
        });
*/
        $.featherlight({
            iframe: 'control/form_string_detect.php?string=yes&stringDetect=1', iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800,afterClose : function(event){
                window.location.reload();
            }
        });

    });







    $('.delete').on('click', function() {
        $('.loading-overlay').show();
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        var formDetails = $('#'+formID);

        var r = confirm("Are you sure you want to delete these inverters?");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: '/control/form_inverters_update.php?delete=yes&inverters_id='  +InvertersCheckedBoxes,
                data: formDetails.serialize(),
                success: function (data) {
                    $('.loading-overlay').hide();
                    console.log('success:' + formDetails.serialize());
                    // clean up //
                    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
                        $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', false);
                    }
                    //InvertersCheckedBoxes = '';
                    InvertersCheckedBoxes = [];
                    //localStorage.setItem("checkboxList", '');
                    window.location.reload(true);
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                    $('.loading-overlay').hide();
                }
            });
            return false;
        }
      
        /*
        
        
    */
    });
});

