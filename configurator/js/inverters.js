/**
 * Created by AdamCadieux on 12/7/2017.
 */

var InvertersCheckedBoxes =[];
var details = [];
var detailsCookie = null;
var inverterCookie = null;
var pageLength = 50;




$(document).ready(function (){

/*
    $.ajax({
        url: '/control/inverters_json.php?dataType=inverters',
        type: 'GET',
        dataType: "json",
        success: displayAll
    });

    function displayAll(data){
        //alert(JSON.stringify(data));
        Cookies.set('testCookie',JSON.stringify(data));
    }
*/

   // Cookies.set('testCookie', JSON.stringify(jsonData));

    inverterCookie = Cookies.get('inverterCookie');
    if ( inverterCookie != null ) {
        // InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(JSON.stringify(inverterCookie) ));
        var array = inverterCookie.replace(/[\[\]'\"]+/g,'').split(',');
        InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));

    }

    $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);




    $("#inverterdata-select-all").change(function(){  //"select all" change

        $("input[name='id[]']:checked").each( function () {
            // InvertersCheckedBoxes.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxes.push($(this).val());
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        });
        $("input[name='id[]']:not(:checked)").each( function () {
            // InvertersCheckedBoxes.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxes.push($(this).val());
            inverterCookie = Cookies.get('inverterCookie');
            InvertersCheckedBoxes.length = '';
            InvertersCheckedBoxes = [];
        });


    });

    setInterval( function () {
        table.ajax.reload(function () {

           // test = Cookies.get('testCookie');
           // alert(test);
            details  = Cookies.get('detailsCookie');
            console.log(details);
            var tr = $('#'+details).closest('tr');
            var row = table.row( tr );
            row.child( format(row.data()) ).show();
            tr.addClass('shown');

            row.child( format(row.data()) ).show();
           // alert('fired off');
        }, false);
    }, 5000 );


    function format ( d ) {
        // `d` is the original data object for the row
        return '<table id="cellDetails" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;float: none; margin: 0 auto;" class="table-bordered text-center">' +

            '<tr style="background-color: #f2f2f2;"><td class="textBold">sw_profile</td><td class="textBold">prodPartNo</td><td class="textBold">output_status</td><td class="textBold">Gateway</td><td class="textBold">status</td><td class="textBold">connect</td><td class="textBold">upgrade_status</td><td class="textBold">upgrade_progress</td><td class="textBold">das_boot</td></tr>' +
            '<tr><td>'+d.sw_profile+'</td><td>'+d.prodPartNo+'</td><td>'+d.output_status+'</td><td>'+d.controlledByGateway+'</td><td>'+d.status+'</td><td>'+d.connect+'</td><td>'+d.upgrade_status+'</td><td>'+d.upgrade_progress+'</td><td>'+d.das_boot+'</td><tr>'+

            '<tr style="background-color: #f2f2f2;"><td class="textBold">resets</td><td class="textBold">uptime</td><td class="textBold">curtail</td><td class="textBold">regulate</td><td class="textBold">ka_gone</td><td class="textBold">ka_back</td><td class="textBold">ka_late</td><td class="textBold">seq_num_repeat</td><td class="textBold">seq_num_oos</td></tr>' +
            '<tr><td>'+d.resets+'</td><td>'+d.uptime+'</td><td>'+d.curtail+'</td><td>'+d.regulate+'</td><td>'+d.ka_gone+'</td><td>'+d.ka_back+'</td><td>'+d.ka_late+'</td><td>'+d.seq_num_repeat+'</td><td>'+d.seq_num_oos+'</td><tr>'+

            '<tr style="background-color: #f2f2f2;"><td class="textBold">up_link</td><td class="textBold">dn_link</td><td class="textBold">up_nthresh</td><td class="textBold">dn_nthresh</td><td class="textBold">up_notice</td><td class="textBold">dn_notice</td><td class="textBold">sis_last_resp</td><td class="textBold">sis_1st_resp</td><td class="textBold">aess_group_id </td></tr>' +
            '<tr><td>'+d.up_link+'</td><td>'+d.dn_link+'</td><td>'+d.up_nthresh+'</td><td>'+d.dn_nthresh+'</td><td>'+d.up_notice+'</td><td>'+d.dn_notice+'</td><td>'+d.sis_last_resp+'</td><td>'+d.sis_1st_resp+'</td><td>'+d.aess_group_id +'</td><tr>'+

            '</table>';
    }


    var table =    $('#inverterdata').DataTable( {
        "pageLength":  50,

        "aLengthMenu": [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "ajax": "/control/inverters_json.php?dataType=inverters",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                'searchable': false,
                "defaultContent": ''
            },
            { 'data': 'inverters_id'},
            { 'data': 'inverters_id',

            "render": function ( data, type, row ) {
                            return '<span class="glyphicon glyphicon-list-alt inverterQuery" id="query-'  + data +' "></span>';
                }
                },
            { 'data': 'serialNumber'},
            { 'data': 'mac_address'},
            { 'data': 'IP_Address'},
            { 'data': 'version'},
            { 'data': 'stringId'},
            { 'data': 'stringPosition'},
            //  { 'data': 'prodPartNo'},
            // { 'data': 'comm','className': "comm"},
            { 'data': 'comm',
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        if(data =='SG424_APP' ){
                            return ' <span style="color:GREEN" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'"  ></span>';
                        }
                        if(data =='BOOT_LOADER' ){
                            return ' <span style="color: orange" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='BOOT_UPDATER' ){
                            return ' <span style="color: orange" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NEEDS_ATTENTION' ){
                            return ' <span style="color: purple" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='UNDER_PERF' ){
                            return ' <span style="color: blue" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NO_COMM' ){
                            return ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='LEGACY' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='MGI_220' ){
                            return ' <span style="color: brown" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='MUNKNOWN' ){
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        else {
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot"  title="Not Mapped: ' + data +'" ></span>';
                        }

                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }
            }, { 'data': 'watts'},
            { 'data': 'battery'
                ,
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        if(data == 1){
                            return 'B'
                        }else return 'S'
                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }}
            ,
            { 'data': 'group_id',
                "render": function ( data, type, row ) {
                    if( data == null){
                        return '';
                    }else {
                        return 'Grp-' + data ;
                    }

                }},
            { 'data': 'FEED_NAME',
                "render": function ( data, type, row ) {
                    if( data == null){
                        return 'N/A';
                    }else {
                        return  data ;
                    }
                }
                /*
                ,
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        return '<select name="FEED_NAME" id="id-controlledByGateway" class="selectpicker feed_change"><option value=' + data +' $selected>' + data +'</option><option value="A">A</option><option value="B">B</option><option value="C">C</option></select>';
                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }
                */

            }

        ],
        'columnDefs': [{
            'targets': 1,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                check = '';

                if($.inArray(data, InvertersCheckedBoxes) > -1){
                    check = 'checked';
                }

                return '<input '+check+' class="inverterCheckbox" type="checkbox" name="id[]" id="select-'+data+'"  value="' + $('<div/>').text(data).html() + '">';
            }
        }],
        'order': [[1, 'asc']],
        rowId: function(a) {
            return 'id_' + a.inverters_id;
        }
    } );


    // Add event listener for opening and closing details
    $('#inverterdata tbody').on('click', 'td.details-control', function () {
        var rowIDCurrent = $(this).parent().attr('id');
        console.log( rowIDCurrent);
        var tr = $('#'+rowIDCurrent).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            Cookies.set('detailsCookie','');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
           //details  = Cookies.get('detailsCookie');
            Cookies.set('detailsCookie',rowIDCurrent);
        }
        
    } );


    // Handle click on "Select all" control
    $('#inverterdata-select-all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#inverterdata tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#inverterdata-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    // Add Remove items from inverter list //
    $('#inverterdata tbody').on( 'click', '.inverterCheckbox', function () {
        console.log($(this).is(':checked') + ' ' + $(this).val() );
        var removeMe = $(this).val();
        if($(this).is(':checked')  == true )
        {
            //console.log ('true:' + $(this).is(':checked') + ' ' + $(this).val() )
            InvertersCheckedBoxes.push($(this).val());
            console.log('Add: '+InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        }else {

            InvertersCheckedBoxes = jQuery.grep(InvertersCheckedBoxes, function(value) {
                return value != removeMe ;
            });
            console.log('subtract: ' +InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes );
            removeMe = '';
        }

    } );

    $('#inverterdata tbody').on( 'click', '.inverterCheckbox', function () {
        console.log($(this).is(':checked') + ' ' + $(this).val() );
        var removeMe = $(this).val();
        if($(this).is(':checked')  == true )
        {
            //console.log ('true:' + $(this).is(':checked') + ' ' + $(this).val() )
            InvertersCheckedBoxes.push($(this).val());
            console.log('Add: '+InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes);
        }else {

            InvertersCheckedBoxes = jQuery.grep(InvertersCheckedBoxes, function(value) {
                return value != removeMe ;
            });
            console.log('subtract: ' +InvertersCheckedBoxes );
            Cookies.set('inverterCookie',InvertersCheckedBoxes );
            removeMe = '';
        }

    } );

    $('#inverterdata').on('submit', function(e){
        var form = this;

        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
            // If checkbox doesn't exist in DOM
            if(!$.contains(document, this)){
                // If checkbox is checked
                if(this.checked){
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                    );
                }
            }
        });

    });

    //$('.inverterQuery tbody').on('click', function() {
    $('#inverterdata tbody').on( 'click', '.inverterQuery', function () {


        formIdSplit=this.id.split('-');
        var  inverters_id = formIdSplit[1];

        $.featherlight({
            iframe: '/inverter_query.php?inverters_id='+inverters_id, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800,afterClose : function(event){
            }
        });


    });

    $('#health-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/health_scan.php', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){

                }
            });
        }
    });

    $('#serial-scan').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/serial_scan.php', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){

                }
            });
        }
    });


    
    
    $('#inverters-start-db-con').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/control/form_start_db_con_audit.php?run_db_const_auditor=YES', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){
                }
            });
        }
    });

    $('#upgrade-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        //https://github.com/noelboss/featherlight/#installation

        // Clear out checkboxes, and cookies //
        tempSInvertersId = InvertersCheckedBoxes;
        InvertersCheckedBoxes = [];
        Cookies.set('inverterCookie', '');
        $('.selectCheckBox').attr('checked', false);
        $("#select_all").prop('checked', false);
        //console.log('IDs: ' +tempSInvertersId )
        $("#invertersSelectedNum").html('');
        $.featherlight({iframe: '/upload_upgrade.php?inverters_id='+tempSInvertersId, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800});
    });

    $('.delete').on('click', function() {
        $('.loading-overlay').show();
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        var formDetails = $('#'+formID);

        var r = confirm("Are you sure you want to delete these inverters?");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: '/control/form_inverters_update.php?delete=yes&inverters_id='  +InvertersCheckedBoxes,
                data: formDetails.serialize(),
                success: function (data) {
                    $('.loading-overlay').hide();
                    console.log('success:' + formDetails.serialize());
                    // clean up //
                    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
                        $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', false);
                    }
                    //InvertersCheckedBoxes = '';
                    InvertersCheckedBoxes = [];
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                    $('.loading-overlay').hide();
                }
            });
            return false;
        }

    });
    $('#inverterdata tbody').on( 'change', '.feed_change', function () {

/*
        Feed Change

        //$('.selectpicker .feed_change').on('click', function() {
        var FEED_NAME =   table.$("[name='FEED_NAME']").val();
        alert('Feed Name:' + FEED_NAME );
        console.log('inverter ID:'+InvertersCheckedBoxes + ' FEED_NAME:' + FEED_NAME);
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        $.ajax({
            type: "POST",
            url: '/control/form_inverters_update.php?inverters_id=' +InvertersCheckedBoxes +'&FEED_NAME=' + FEED_NAME,
            success: function (data) {
                console.log('success:' + formDetails.serialize());
                // clean up //
                for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
                    $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', false);
                }
                InvertersCheckedBoxes = [];
            },
            error: function(jqXHR, text, error){
                console.log('error:');
            }
        });
        return false;
*/
    });

    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
        $( ".inverterCheckbox #select-" + InvertersCheckedBoxes[i]).prop('checked', true);
    }

});


function unique(list) {

    list.sort()
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    // remove empty places
    result = result.filter(v=>v!='');
    return result;
}
