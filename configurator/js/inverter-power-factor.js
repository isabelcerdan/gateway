/**
 * Created by AdamCadieux on 7/26/2016.
 */

$(document).ready(function() {

    var InvertersCheckedBoxes =[];
    var inverterPFCookie = null;
    var invertersSelected = '';

    function unique(list) {

        list.sort()
        var result = [];
        $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        // remove empty places
        result = result.filter(v=>v!='');
        return result;
    }



    $('#set_power_factor').on('click', function() {

        // get all the inputs into an array.
        //var email = $("#myField").val();

      //  var text = $('#set_power_factor_form').find('input[name="#powerFactorGroup"]').val();
        tempSInvertersId = InvertersCheckedBoxes;
        InvertersCheckedBoxes = [];
        Cookies.set('inverterPFCookie', '');
        $('.selectCheckBox').attr('checked', false);
        $("#select_all").prop('checked', false);
        //console.log('IDs: ' +tempSInvertersId )
        $("#invertersSelectedNum").html('');

        var group =   $("#powerFactorGroup").val();


    $.featherlight({iframe: '/power_factor_update.php?inverters_id='+tempSInvertersId+"&group=" + group, iframeMaxWidth: '100%', iframeWidth: 800,
      iframeHeight: 800});


    });

    $('#set_adaptive_power_factor').on('click', function() {

        // get all the inputs into an array.
        //var email = $("#myField").val();

        //  var text = $('#set_power_factor_form').find('input[name="#powerFactorGroup"]').val();
        var adaptive=   jQuery('input[name="adaptive"]').val();
        var lagging=   jQuery('input[name="lagging"]:checked').val();
        var powerFactor=   jQuery('input[name="powerFactor"]').val();

        if(adaptive =='Yes' ) {
            $.featherlight({
                iframe: '/control/set_power_factor.php?adaptive=' + adaptive + "&lagging=" + lagging + '&powerFactor=' + powerFactor,
                iframeMaxWidth: '100%',
                iframeWidth: 800,
                iframeHeight: 800
            });
        }else {
            alert('Looks like there was an error, please go back and try again');
        }

    });





    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change

        if($("#select_all").is(':checked')) {
            $.each($('.selectCheckBox'), function(index, value) {
                serialID=this.id.split('-');
                InvertersCheckedBoxes.push(serialID[1]);
            });

            inverterPFCookie = Cookies.get('inverterPFCookie');
            if ( inverterPFCookie != null ) {
                var array = inverterPFCookie.replace(/[\[\]'\"]+/g,'').split(',');
                InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));
               //console.log('43: ' +InvertersCheckedBoxes )
            }else {
               // console.log('IS  0: ' +InvertersCheckedBoxes )
            }
            Cookies.set('inverterPFCookie',InvertersCheckedBoxes);

           // Cookies.set('inverterPFCookie', InvertersCheckedBoxes);

            $(".checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
           // console.log('checked:' +InvertersCheckedBoxes );
        }

        else {

            InvertersCheckedBoxes.length = '';
            InvertersCheckedBoxes = [];
            Cookies.set('inverterPFCookie', '');
            //console.log('69 invertersSelectedNum:' +InvertersCheckedBoxes );

            $(".checkbox").prop('checked', false); //change all ".checkbox" checked status
        }
        //console.log('73 invertersSelectedNum:' +InvertersCheckedBoxes.length );
        $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);

    });

    //".checkbox" change
    $('.checkbox').change(function(){

        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all").prop('checked', false); //change "select all" checked status to false

        }
        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all").prop('checked', true);

        }
    });




    //manually clear storage //
    $('#itemsperpage').on('change', function() {
        this.form.submit();
    });
    // keep array up to date when page reloads

    inverterPFCookie = Cookies.get('inverterPFCookie');
    if ( inverterPFCookie != null ) {
       // InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(JSON.stringify(inverterPFCookie) ));
        var array = inverterPFCookie.replace(/[\[\]'\"]+/g,'').split(',');

        InvertersCheckedBoxes = unique(InvertersCheckedBoxes.concat(array));

    }

    console.log('128 invertersSelectedNum:' +InvertersCheckedBoxes );
    $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);
    for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
        $( "#select-" + InvertersCheckedBoxes[i]).prop('checked', true);
    }
    // Alert to show what items are in checked list //

    $('#getCheckedinfo').click(function(){
        arrayView = '';
        macList ='';
        for(var i = 0; i < InvertersCheckedBoxes.length; i++) {
            arrayView = arrayView + '\n' + InvertersCheckedBoxes[i];
            if($('#select-'+i).val() === undefined) {
                console.log('undefined');
            }else {
                macList = macList+ '\n' + $('#select-'+i).val();
            }
        }

    });
    $('.selectCheckBox').change(function(){
        serialID=this.id.split('-');
        //mac = $('#'+this.id).val();
        serialNumber = serialID[1];
        if ($("#select-"+serialID[1]).is(":checked"))
        {
            // Add items to the checked list
            if(InvertersCheckedBoxes.indexOf(serialID[1]) == -1)  {

               InvertersCheckedBoxes.push(serialNumber);
            }
        }else {
            // Remove items from the checked list
            var index = InvertersCheckedBoxes.indexOf(serialNumber);
            if (index != -1)
                InvertersCheckedBoxes.splice(index, 1);
        }
        // print out the number of items checked //
        console.log('166 invertersSelectedNum:' +InvertersCheckedBoxes );
        $("#invertersSelectedNum").html(InvertersCheckedBoxes.length);
        // Store inverter array in local memory//

        Cookies.set('inverterPFCookie', InvertersCheckedBoxes);

    });



});

