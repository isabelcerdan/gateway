/**
 * Created by AdamCadieux on 5/18/2016.
 */
$( document ).ready( function () {
    $( "#systemForm" ).validate( {
        rules: {

            gateway_name:"required",
            log_level:"required",
            total_output_limit:"required",
            address:"required",
            city:"required",
            state:"required",
            postcode:"required"
        },
        messages: {
            gateway_name: "Please enter a Gateway Name",
            log_level: "Please enter the Log Level",
            total_output_limit: "Please enter the Total Output Limit",
            address: "Please enter the Address",
            city: "Please enter the City",
            state: "Please enter the State",
            postcode: "Please enter the Postcode"
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-md-3" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".col-md-3" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    } );


} );