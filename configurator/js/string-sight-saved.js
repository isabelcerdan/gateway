/**
 * Created by AdamCadieux on 12/7/2017.
 */

var InvertersCheckedBoxesSaved =[];
var inverterCookieSaved = null;
var pageLength = 50;
var StringDetectCounterSaved = 0;
var myVar;

$(document).ready(function (){

    auto_inverter_count();
    setInterval(auto_inverter_count,10000);

    function auto_inverter_count(){
        $.ajax({
            url: "control/get_inverters_count.php?dataType=stringSave",
            cache: false,
            success: function(data){
                $("#inverter_count_div").html(data);
            }
        });
    }

    inverterCookieSaved = Cookies.get('inverterCookieSaved');
    if ( inverterCookieSaved != null ) {
        // InvertersCheckedBoxesSaved = unique(InvertersCheckedBoxesSaved.concat(JSON.stringify(inverterCookieSaved) ));
        var array = inverterCookieSaved.replace(/[\[\]'\"]+/g,'').split(',');
        InvertersCheckedBoxesSaved = unique(InvertersCheckedBoxesSaved.concat(array));

    }

    $("#invertersSelectedNum").html(InvertersCheckedBoxesSaved.length);

    for(var i = 0; i < InvertersCheckedBoxesSaved.length; i++) {
        $( ".inverterCheckboxSaved #select-" + InvertersCheckedBoxesSaved[i]).prop('checked', true);
    }



    $("#inverterdata-select-all-Saved").change(function(){  //"select all" change

        $("input[name='id[]']:checked").each( function () {
            // InvertersCheckedBoxesSaved.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxesSaved.push($(this).val());
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved);
    
        });
        $("input[name='id[]']:not(:checked)").each( function () {
            // InvertersCheckedBoxesSaved.push($(this).val());
            // If checkbox doesn't exist in DOM
            InvertersCheckedBoxesSaved.push($(this).val());
            inverterCookieSaved = Cookies.get('inverterCookieSaved');
            InvertersCheckedBoxesSaved.length = '';
            InvertersCheckedBoxesSaved = [];
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved);

        });


    });

    setInterval( function () {
        table.ajax.reload( null, false );
    }, 5000 );

    $('#inverterdata').on( 'load', 'tr', function () {
        alert( 'Row index: '+table.row( this ).index() );
    } );

    var table =    $('#inverterdata').DataTable( {
        "pageLength": pageLength,
        'info': true,
        "ajax": "/control/inverters_json.php?dataType=stringSave",
        dom: 'Bfrtip',
        buttons: [
            'csv'
        ],
        "columns": [

            {  'data': 'inverters_id'},
            { 'data': 'groupId'},
            { 'data': 'serialNumber'},
            { 'data': 'mac_address'},
            { 'data': 'version'},
            { 'data': 'stringId'},
            { 'data': 'stringPosition'}


        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                check = '';

                if($.inArray(data, InvertersCheckedBoxesSaved) > -1){
                    check = 'checked';
                }

                return '<input '+check+' class="inverterCheckboxSaved" type="checkbox" name="id[]" id="select-'+data+'"  value="' + $('<div/>').text(data).html() + '">';
            }
        }],
        'order': [[1, 'asc']]
    } );

    // Handle click on "Select all" control
    $('#inverterdata-select-all-Saved').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#inverterdata tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#inverterdata-select-all-Saved').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    // Add Remove items from inverter list //
    $('#inverterdata tbody').on( 'click', '.inverterCheckboxSaved', function () {
        console.log($(this).is(':checked') + ' ' + $(this).val() );
        var removeMe = $(this).val();
        if($(this).is(':checked')  == true )
        {
            //console.log ('true:' + $(this).is(':checked') + ' ' + $(this).val() )
            InvertersCheckedBoxesSaved.push($(this).val());
            console.log('Add: '+InvertersCheckedBoxesSaved );
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved);
        }else {

            InvertersCheckedBoxesSaved = jQuery.grep(InvertersCheckedBoxesSaved, function(value) {
                return value != removeMe ;
            });
            console.log('subtract: ' +InvertersCheckedBoxesSaved );
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved );
            removeMe = '';
        }

    } );

    $('#inverterdata tbody').on( 'click', '.inverterCheckboxSaved', function () {
        console.log($(this).is(':checked') + ' ' + $(this).val() );
        var removeMe = $(this).val();
        if($(this).is(':checked')  == true )
        {
            //console.log ('true:' + $(this).is(':checked') + ' ' + $(this).val() )
            InvertersCheckedBoxesSaved.push($(this).val());
            console.log('Add: '+InvertersCheckedBoxesSaved );
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved);
        }else {

            InvertersCheckedBoxesSaved = jQuery.grep(InvertersCheckedBoxesSaved, function(value) {
                return value != removeMe ;
            });
            console.log('subtract: ' +InvertersCheckedBoxesSaved );
            Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved );
            removeMe = '';
        }

    } );

    $('#inverterdata').on('submit', function(e){
        var form = this;

        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
            // If checkbox doesn't exist in DOM
            if(!$.contains(document, this)){
                // If checkbox is checked
                if(this.checked){
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                    );
                }
            }
        });

    });

    //$('.inverterQuery tbody').on('click', function() {
    $('#inverterdata tbody').on( 'click', '.inverterQuery', function () {


        formIdSplit=this.id.split('-');
        var  inverters_id = formIdSplit[1];

        $.featherlight({
            iframe: '/inverter_query.php?inverters_id='+inverters_id, iframeMaxWidth: '100%', iframeWidth: 800,
            iframeHeight: 800,afterClose : function(event){
                //console.log("after close" +this); // this contains all related elements
                //alert(this.$content.hasClass('true')); // alert class of content
                window.location.reload();
            }
        });


    });
    

    $('.delete').on('click', function() {
        $('.loading-overlay').show();
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        var formDetails = $('#'+formID);

        var r = confirm("Are you sure you want to delete these inverters?");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: '/control/form_inverters_update.php?delete=yes&dataType=stringSave&inverters_id='  +InvertersCheckedBoxesSaved,
                data: formDetails.serialize(),
                success: function (data) {
                    $('.loading-overlay').hide();
                    console.log('success:' + formDetails.serialize());
                    // clean up //
                    for(var i = 0; i < InvertersCheckedBoxesSaved.length; i++) {
                        $( "#select-" + InvertersCheckedBoxesSaved[i]).prop('checked', false);
                    }
                    //InvertersCheckedBoxesSaved = '';
                    InvertersCheckedBoxesSaved = [];
                    Cookies.set('inverterCookieSaved',InvertersCheckedBoxesSaved );
                    //localStorage.setItem("checkboxList", '');
                    //window.location.reload(true);
                },
                error: function(jqXHR, text, error){
                    console.log('error:');
                    $('.loading-overlay').hide();
                }
            });
            window.location.reload();
            return false;
        }

    });
    $('#inverterdata tbody').on( 'change', '.feed_change', function () {
        //$('.selectpicker .feed_change').on('click', function() {
        var FEED_NAME =   table.$("[name='FEED_NAME']").val();
        console.log('inverter ID:'+InvertersCheckedBoxesSaved + ' FEED_NAME:' + FEED_NAME);
        formIdSplit=this.id.split('-');
        // Getting the form ID
        var  formID = formIdSplit[1];
        var formDetails = $('#'+formID);
        $.ajax({
            type: "POST",
            url: '/control/form_inverters_update.php?inverters_id=' +InvertersCheckedBoxesSaved +'&FEED_NAME=' + FEED_NAME,
            success: function (data) {
                console.log('success:' + formDetails.serialize());
                // clean up //
                for(var i = 0; i < InvertersCheckedBoxesSaved.length; i++) {
                    $( "#select-" + InvertersCheckedBoxesSaved[i]).prop('checked', false);
                }
                InvertersCheckedBoxesSaved = [];
                //localStorage.setItem("checkboxList", '');
                //window.location.reload(true);
            },
            error: function(jqXHR, text, error){
                console.log('error:');
            }
        });
        return false;

    });


});


function unique(list) {

    list.sort()
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    // remove empty places
    result = result.filter(v=>v!='');
    return result;
}
