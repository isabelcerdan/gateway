/**
 * Created by AdamCadieux on 5/18/2016.
 */

$( document ).ready( function () {

    $.validator.addMethod( "ipv4", function( value, element ) {
        return this.optional( element ) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test( value );
    }, "Please enter a valid IP v4 address." );

    $.validator.addMethod( "macAddress", function( value, element ) {
        return this.optional( element ) || /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/i.test( value );
    }, "Please enter a valid mac Address address." );

    $( "#brainboxesForm" ).validate( {
        rules: {

            target_interface:"required",
            nickname:"required",
            IP_Address: {
                "required": true,
                ipv4: true
            },
            mac_address:
            {
                "required": true,
                macAddress: true
            },
            baud:"required",
            model_number:"required"

        },
        messages: {

            target_interface: "Please enter the Target Interface",
            nickname: "Please enter a nickname for the Brainboxes",
            IP_Address: {
                required: "Please enter the IP Address",
                ipv4: "Please make sure to format the IP address correctly. Example: XXX.XXX.XXX.XXX"
            },
            mac_address: {
                required: "Please enter the Mac Address",
                mac_address: "Please make sure to format the Mav address correctly. Example: XX:XX:XX:XX:XX:XX"
            },
            baud: "Please enter the baud rate",
            model_number: "Please enter the model number "


        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-md-3" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".col-md-3" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    } );


} );