<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Global System Settings</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>



    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
        $(function() {
            $( "#tabs" ).tabs();
        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var

include_once ('control/get_aq_sync.php'); // Get default data

include_once ('control/get_groups.php');
?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Sync with Energy Review</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>


                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_sync_er.php" method="post"  id="systemForm">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">

                    <div class="formBoxSection">
                        <div> <strong>Base Serial Number: </strong><?php echo $gateway_base_sn; ?> </div>
                        <div class="formBoxSection">

                            <div class="form-group">
                                <label class="col-md-3 control-label">URL of Acquisition Server:</label>
                                <div class="col-md-3">
                                    <input type="text" name="URL_AQ" id="URL_AQ" class="form-control" value="<?php echo $URL_AQ; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please enter the url for the Gateway to Register itself<br>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">URL for Meter(to send reports):</label>
                                <div class="col-md-3">
                                    <input type="text" name="URL_AQ_Meter" id="URL_AQ_Meter" class="form-control" value="<?php echo $URL_AQ_Meter; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Acquisition Server URL" data-content="Acquisition Server URL" >
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please enter the url that the meters will use to send reports
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                        <div> <strong>Meter Reporting: </strong></div>
                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Transmit Frequency:</label>
                                <div class="col-md-3">
                                    <input type="text" name="transmit_frequency" id="transmit_frequency" class="form-control" value="<?php if ($transmit_frequency == '') {echo "60";} else echo $transmit_frequency; ?>" placeholder="Enter number of Seconds">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Transmit Frequency" data-content="How often to send energy reports"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please give the transmit frequency seconds<br> (5 min = 300 sec, 10 min = 600 sec, 15 min = 900 sec)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Timeout:</label>
                                <div class="col-md-3">
                                    <input type="text" name="timeout" id="timeout" class="form-control" value="<?php if ($timeout == '') {echo "3";} else echo $timeout ?>" placeholder="Enter number of Seconds">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Timeout" data-content="Timeout in seconds"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong>  Please set time in seconds<br> (5 min = 300 sec, 10 min = 600 sec, 15 min = 900 sec)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Retries:</label>
                                <div class="col-md-3">
                                    <input step="1" min="1" max="100" type="number" name="retries" id="retries" class="form-control" value="<?php if($retries == '' OR $retries == 0){echo 1;} else echo $retries; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Retries" data-content="Number of attempts to resend energy report"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please give this as a Number (INT)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Enable Meter Reporting to ER:</label>
                                <div class="col-md-3">

                                    <div class="checkbox">
                                        <label><input type="checkbox" value="1" <?php if($enabled ==1){echo 'checked';}  ?> name="enabled">Enable</label>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">


                                    </div>
                                </div>
                            </div>


                        </div>

                        <strong>Inverter Reporting: </strong>
                        <div class="formBoxSection">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Transmit Frequency to ER Server:</label>
                                <div class="col-md-3">
                                    <input type="text" name="inverter_er_rate" id="inverter_er_rate" class="form-control" value="<?php if ($inverter_er_rate == '') {echo "900";} else echo $inverter_er_rate; ?>" placeholder="Enter number of Seconds">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="inverter_er_rate" data-content="How often to send energy reports"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please give the transmit frequency seconds<br> (5 min = 300 sec, 10 min = 600 sec, 15 min = 900 sec) <br> Default is 15 min (900 sec)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">URL for Inverter:</label>
                                <div class="col-md-3">
                                    <input type="text" name="inverter_er_url" id="inverter_er_url" class="form-control" value="<?php echo $inverter_er_url; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Acquisition Server URL" data-content="Acquisition Server URL" >
                                            <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please enter the url that the inverters will use to send reports
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Port to Send Inverter Reports:</label>
                                <div class="col-md-3">
                                    <input type="text" name="inverter_er_port" id="inverter_er_port" class="form-control" value="<?php if ($inverter_er_port == '') {echo "80";} else echo $inverter_er_port; ?>" placeholder="Port Number">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Transmit Frequency" data-content="How often to send energy reports"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Most likely the port will be 80, 8180, 8081, 443
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Alarm Transmit Frequency:</label>
                                <div class="col-md-3">
                                    <input type="text" name="alarms_trans_frequency" id="alarms_trans_frequency" class="form-control" value="<?php  echo $alarms_trans_frequency; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Transmit Frequency" data-content="How often to send energy reports"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please give the transmit frequency seconds
                                        (5 min = 300 sec, 10 min = 600 sec, 15 min = 900 sec)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Number of Inverter Reports per batch:</label>
                                <div class="col-md-3">
                                    <input type="text" name="reports_per_batch" id="reports_per_batch" class="form-control" value="<?php echo $reports_per_batch ; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
                                        <a  data-toggle="popover" title="Transmit Frequency" data-content="How often to send energy reports"  ">
                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                        </a>
                                        <strong>Note: </strong> Please give the transmit frequency seconds
                                        (5 min = 300 sec, 10 min = 600 sec, 15 min = 900 sec)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Enable Inverter Reporting to ER:</label>
                                <div class="col-md-3">

                                    <div class="checkbox">
                                        <label><input type="checkbox" value="1" <?php if($erptd==1){echo 'checked';}  ?> name="erptd">Enable</label>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                        <strong>Note: </strong>Enable this to start the Gateway sending inverter reports to Energy Review directly
                                        
                                    </div>
                                </div>
                            </div>

                    </div>

                        <strong>Inverter Groups: </strong>
                        <div class="formBoxSection">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Where to Send reports from Inverter Groups:</label>
                                <div class="col-md-6">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Group ID</th>
                                            <th>Group Name</th>
                                            <th>Inverters</th>
                                            <th>Send to ER</th>
                                            <th>Send to Gateway</th>


                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php

                                        for($i = 0; $i < count($group_id); $i++) {
                                            $sendERChecked = '';
                                            $noERCchecked  = '';
                                            if($er_server_http[$i] == 1){
                                                $sendERChecked = 'selected';
                                            }else $noERCchecked = 'selected';

                                            $sendGATChecked = '';
                                            $noGATCchecked  = '';
                                            if($er_gtw_tlv[$i] == 1){
                                                $sendGATChecked = 'selected';
                                            }else $noGATCchecked = 'selected';
                                            echo "<tr>";
                                            echo "<td>" . $group_id[$i] . "</td>";
                                            echo "<td>" . $alias[$i] . "</td>";
                                            echo "<td>" . $group_count[$i] . "</td>";
                                            echo "<td>
                                                <select name='reportToER[]'  class='form-control'>
                                                <option value='$group_id[$i]-1' $sendERChecked >Yes </option>
                                                <option  value='$group_id[$i]-0' $noERCchecked >No</option>";

                                            echo "</select></label></td>";
                                            echo "<td>
                                                <select name='reportToGAT[]'  class='form-control'>
                                                <option value='$group_id[$i]-1' $sendGATChecked >Yes </option>
                                                <option  value='$group_id[$i]-0' $noGATCchecked>No</option>";

                                            echo "</select></label></td>";



                                        }

                                        ?>


                                        </tr>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>

                </div>


                </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>

</div>
    <!-- Row end -->

</div>
</body>
</html>