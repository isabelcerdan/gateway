<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

$errors = filter_var($_GET['errors'], FILTER_SANITIZE_STRING);


$netprotect_email_id= mysqli_real_escape_string($conn, $_GET['netprotect_email_id']);
$netprotect_email_id = filter_var($netprotect_email_id, FILTER_VALIDATE_INT);

include_once ('control/get_network_protector.php'); // Get default data

if($netprotect_email_id !='' ) {

    include_once ('control/get_network_protector_users.php'); // Get default data
}



// Form validation without JS //

if ($errors != ''){
    $regFail = true;
}

if (strpos($errors, 'problem') !== false) {
    $problem = true;
}

if (strpos($errors, 'input_nameError') !== false) {
    $input_nameError = true;

}
if (strpos($errors, 'recipient_name') !== false) {
    $recipient_nameError = true;

}

if (strpos($errors, 'email') !== false) {
    $emailErrror = true;

}

if (strpos($errors, 'msg_body') !== false) {
    $msg_bodyErrror = true;

}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>



<div class="container">

    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Register</h3>

                </div>

                <?php if($already_in_DB == true ) {echo "<h3 class='text-center text-danger'>Your email is already in the system!</h3>";} ;?>
                <?php if($problem == true ) {echo "<h3 class='text-center text-danger'>Looks like there was a problem registering. <b>Please contact support</b></h3>";} ;?>


                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_net_protect_email.php" method="post">
                        <?php
                            echo "<input type='hidden' name='netprotect_email_id' value='$netprotect_email_id'>";
                        ?>

                        <div class="formBoxSection ">
                            <div class="form-group <?php if($input_nameError == true ) {echo " has-error inputError";} ;?>  ">
                                <label class="col-md-3 control-label">Input Name:</label>
                                <div class="col-md-3 ">
                                    <select name="input_name" class="form-control" required>
                                        <?php
                                        if(!empty($input_name)) {
                                            echo "<option value='$input_name' >$input_name</option>";
                                        }else {
                                            echo "<option value='' >Select Device</option>";
                                        }
                                        ?>
                                        <option value='<?php echo $di0_input_name; ?>'><?php echo $di0_input_name; ?> - 0</option>
                                        <option value='<?php echo $di1_input_name; ?>'><?php echo $di1_input_name; ?> - 1</option>
                                        <option value='<?php echo $di2_input_name; ?>'><?php echo $di2_input_name; ?> - 2</option>
                                        <option value='<?php echo $di3_input_name; ?>'><?php echo $di3_input_name; ?> - 3</option>
                                    </select>
                                </div>
                                <div class="col-md-6" >
                                  <div class="formTextSpacing">
    
                                  </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($recipient_nameError== true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Name:</label>
                                <div class="col-md-3">
                                    <input type="text" name="recipient_name" class="form-control" placeholder="Last Name" required value="<?php echo $recipient_name; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($emailErrror == true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Email:</label>
                                <div class="col-md-3">
                                    <input name="email" type="email" id="email_address" class="form-control" placeholder="Email address" required value="<?php echo $email_address; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?php if($msg_bodyErrror == true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Message:</label>
                                <div class="col-md-3">
                                    <textarea  name="msg_body" class="form-control" required ><?php echo $msg_body; ?></textarea>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary" >Register</button>

                    </form>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>