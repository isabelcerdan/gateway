<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Configure Brainboxes</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/brainboxes-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script type="text/javascript">
        $( document ).ready( function () {

           // $('.delete-brain').click('change', function() {
               // alert('id:' + this.id);
               // this.form.submit();
            //});

            $(document).on("click", "a.delete-brain", function() {
                if (confirm('Are you sure  you want to delete ' + this.id + ' Brainbox?' )) {
                    $(location).attr('href', '/control/form_bainboxes_add_edit.php?brainbox_id=' + this.id + '&delete_brain=yes');
                }
            });

            $("[data-hide]").on("click", function(){
                $(this).closest("." + $(this).attr("data-hide")).hide();
            });

            $(document).on("click", "#auto-config-bbox", function() {
                $('#auto-config-bbox').addClass('disabled');
                $('#ajax-loader').show();
                $.ajax({
                    type: "POST",
                    url: "/control/auto_config_brainboxes.php",
                    success: function(data){
                        var jsonData = JSON.parse(data);
                        var message = jsonData.results.message;

                        if (jsonData.results.errors != '') {
                            message += " " + jsonData.results.errors;

                            $('#brainbox-alert').removeClass('alert-success');
                            $('#brainbox-alert').addClass('alert-danger');
                        }
                        else {
                            $('#brainbox-alert').removeClass('alert-danger');
                            $('#brainbox-alert').addClass('alert-success');
                        }

                        $('#brainbox-alert-text').html(message);
                        $('#brainbox-alert').show();
                        $('#auto-config-bbox').removeClass('disabled');
                        $('#ajax-loader').hide();
                    },
                    error: function (response) {
                        alert(response);
                        $('#auto-config-bbox').removeClass('disabled');
                        $('#ajax-loader').hide();
                    }
                });
            });
        } );


    </script>
</head
<body>
<?php
include_once ('functions/mysql_connect.php');

include_once ('control/get_brainboxes.php'); // Get default data


?>
<div class="container">
   
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Configure Brainboxes</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">

                    <div class="alert alert-dismissable" id="brainbox-alert" hidden="hidden">
                        <button type="button" class="close" data-hide="alert">&times;</button>
                        <p id="brainbox-alert-text"></p>
                    </div>

                    <div style="margin-left: 30px;">
                        <button class="btn btn-warning" id="auto-config-bbox">Auto-Configure Brainboxes</button>
                        <img src="/images/ajax-loader.gif" id="ajax-loader" style="display:none"/>
                    </div>
                    <div class="formBoxSection">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="font-size: smaller">
                                    <th></th>
                                    <th></th>
                                    <th>Target Interface</th>
                                    <th>IP Address</th>
                                    <th>Mac Address</th>
                                    <th>Baud</th>
                                    <th>TCP Port</th>
                                    <th>PID</th>


                                </tr>
                                </thead>
                                <tbody id="auto_brainbox_status_div">
                                <?php
                                for($i = 0;$i <  count($target_interfaceDB); $i++) {
                                    echo "<tr>
                                            <td class='text-center'><a href='brainboxes-add-edit.php?brainbox_id=$brainbox_idDB[$i]' target='_top'> <button class=\"btn btn-warning\" >Edit</button></a></td>
                                            <td class='text-center'><a class='delete-brain' id='$brainbox_idDB[$i]' > <button class=\"btn btn-danger\" >Delete</button></a></td>
                                            <td class='text-center'>$target_interfaceDB[$i]</td>
                                         <td class='text-center'><a href='http://$IP_AddressDB[$i]' target='_blank'>$IP_AddressDB[$i]</a></td>
                                         <td class='text-center'>$mac_addressDB[$i]</td>
                                         <td class='text-center'>$baudDB[$i]</td>
                                         <td class='text-center'>$tcp_portDB[$i]</td>
                                         <td class='text-center'>$pidDB[$i]</td>
                                    ";
                                    echo "</tr>";
                                }

                                ?>
                               
                                </tbody>
                            </table>
                        </div>


                    </div>


                   

                    <div class="formBoxSection">
                        <a href='brainboxes-add-edit.php?model=ES-420'> <button class="btn btn-primary" >New 420</button></a>
               



                    </div>


                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>

</div>
</body>
</html>