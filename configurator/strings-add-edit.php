<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Strings Configure</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>

    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

$stringId = mysqli_real_escape_string($conn, $_POST['stringId']);
$stringId= filter_var($stringId, FILTER_SANITIZE_STRING);
if ($stringId != '') {
    include_once('control/get_strings.php'); // Get default data
}



?>



<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Strings</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_string_add_edit.php" method="post" id="meterForm">
                        <input type="hidden" name="stringId" value="<?php echo $stringId ?>">
                       


                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">
                                <div class="form-group">

                                    <div class="col-md-12 " >
                                        <div class="formTextSpacing text-center">
                                            <h2> Add/Edit <?php echo $string_name . " ID: " . $stringId; ?> Strings</h2>
                                            <?php
                                            // Errors
                                            if ($changes != '') {
                                                echo"<p style='color: red;font-weight: bold'>$changes </p>";
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="formBoxSectionWhite">
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">String Name:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="string_name" id="string_name" class="form-control" value="<?php echo $string_name; ?>" >
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Meter Name" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Optional: Give each meter a unique name
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Location:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="location" id="location" class="form-control" value="<?php echo $location; ?>">
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Meter Name" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Give each meter a unique name
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Notes:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="notes" id="notes" class="form-control" value="<?php echo $notes; ?>" >

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Meter Class"
                                                        data-content="Each type of meter monitors different types of power variables. For the meter to work correct the system must know what type of meter you are installing.  "  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Feed Name:</label>
                                            <div class="col-md-3">

                                                <select name="FEED_NAME">
                                                    <?php
                                                    $letters = range('A', 'F');

                                                    foreach ($letters as $one) {
                                                        $selected = '';
                                                        if( $one == "$FEED_NAME_inverter[$i]" ) {
                                                            $selected = 'selected';
                                                        }

                                                        for ($j = 0; $j < count($FEED_NAME_Array);$j++ ) {



                                                            if($one == $FEED_NAME_Array[$j] ) {
                                                                echo "  <br> <option value='$one' $selected>$one - Live</option>";
                                                                $one = null;
                                                                break;
                                                            }
                                                        }
                                                        if($one != null )
                                                            echo "<option value='$one' $selected>$one</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Modbus Device Number" data-content="Modbus is a serial communications protocol that meter use to communicate with the Gateway"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                </div>
<!--
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Buildout:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="buildout" id="buildout" class="form-control" value="<?php echo $buildout; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">
                                                <a  data-toggle="popover" title="Meter Class"
                                                    data-content="Each type of meter monitors different types of power variables. For the meter to work correct the system must know what type of meter you are installing.  "  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
-->
                                </div>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>