<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Minion Auditor</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/brainboxes-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>

    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

$minion_auditor_id = mysqli_real_escape_string($conn, $_REQUEST['minion_auditor_id']);
$minion_auditor_id = filter_var($minion_auditor_id, FILTER_SANITIZE_STRING);



include_once ('control/get_minion_auditor.php');


?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Minion Auditor Configure</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_minion_add_edit.php" method="post" id="brainboxesForm">
                        <input type="hidden" name="minion_auditor_id" value="<?php echo $minion_auditor_id; ?>">

                        
                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name of Minion Auditor:</label>
                                        <div class="col-md-3 formTextSpacing"><?php echo $title; ?> </div>
                                        <div class="col-md-6 " ></div>
                                    </div>
                                </div>
                                

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Run Now:</label>
                                        <div class="col-md-3">
                                            <select name="run_now" class="form-control" required>
                                                <?php
                                                if($run_now != ''){
                                                    echo "<option value='$run_now'>$run_now - Current</option>";
                                                }
                                                ?>
                                                <option value='YES'>YES</option>
                                                <option value='NO'>NO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 " >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="Target Interface" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Frequency:</label>
                                            <div class="col-md-3">


                                                <select name="frequency" class="form-control" required>
                                                    <?php
                                                    if($frequency != ''){
                                                        echo "<option value='$frequency'>$frequency - Current</option>";
                                                    }
                                                    ?>
                                                    <option value=''>Select</option>
                                                    <option value='never'>never</option>
                                                    <option value='minute'>minute</option>
                                                    <option value='hourly'>hourly</option>
                                                    <option value='daily'>daily</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Device Nickname" data-content=""  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">time_value:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="time_value" id="time_value" class="form-control" value="<?php echo $time_value; ?>" >

                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="IP Address" data-content=""  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email:</label>
                                            <div class="col-md-3">

                                                <input
                                                       placeholder="Email address"  name="email" id="email" class="form-control" value="<?php echo $email; ?>" required />
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Mac Address" data-content="."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> If entering more than one email address please seperate them with a comma (,)

                                                </div>
                                            </div>
                                        </div>

                                    </div>





                                <button type="submit" class="btn btn-primary" >Submit</button>
                                <a href="auditor.php" id="cancel" name="cancel" class="btn btn-default">Cancel</a>

                    </form>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>