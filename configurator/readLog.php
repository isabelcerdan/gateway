<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/7/2016
 * Time: 4:34 PM
 *
 * 2016-09-07 16:01:52,806 Monitor [INFO]: ***MONITOR DAEMON STARTED WITH NEW LOG AT 04:01:52 (UTC = 1473289312)***
 */

// Open the file
//$filename = '/usr/share/apparent/www/log.txt';
//$filename = '/var/log/apparent/fake.log';
$filename = '/var/log/apparent/gateway.log';
//$filename = 'fakeloca..txt';
echo "filename:" . $filename . "<br>";
$data = file_get_contents($filename);

if( file_exists($filename) ) {

 $logText =  tailCustom($filename, $lines = 5);

  $logArray = explode("\n",$logText);


    foreach($logArray as $f){
        echo $f."<br />";
    }


  //  print_r (explode("\n",$array));
/*
    $array = file($filename);

    $file = array_reverse($array);
    foreach($file as $lineOfLog){
        echo  filter_var($lineOfLog, FILTER_SANITIZE_STRING) ."<br />";
    }






    $file=$filename;
    $linecount = 0;
    $handle = fopen($file, "r");
    while(!feof($handle)){
        $line = fgetss($handle);
        $linecount++;
    }

    fclose($handle);

    echo "<br>line Count: " . $linecount . "<br>";


    if ($data != '') {
        echo 'data<br>';
    } else {
        echo 'no joy<br>';
    }

    $fp = @fopen($filename, 'r');
    echo "<br>feof: " . feof($fp) . "<br>";

// Add each line to an array
    if ($fp) {
        $array = explode("\n", fread($fp, filesize($filename)));
        print_r($array);
    } else {
        echo "problem";
    }
    echo "<br>";

    $file = fopen($filename, "r");

    while (!feof($file)) {
        echo fgets($file) . "<br />";
    }

    fclose($file);
    */
}else {
    echo "File not accessible ";
}


function tailCustom($filepath, $lines = 1, $adaptive = true) {
    // Open file
    $f = @fopen($filepath, "rb");
    if ($f === false) return false;
    // Sets buffer size
    if (!$adaptive) $buffer = 4096;
    else $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));
    // Jump to last character
    fseek($f, -1, SEEK_END);
    // Read it and adjust line number if necessary
    // (Otherwise the result would be wrong if file doesn't end with a blank line)
    if (fread($f, 1) != "\n") $lines -= 1;

    // Start reading
    $output = '';
    $chunk = '';
    // While we would like more
    while (ftell($f) > 0 && $lines >= 0) {
        // Figure out how far back we should jump
        $seek = min(ftell($f), $buffer);
        // Do the jump (backwards, relative to where we are)
        fseek($f, -$seek, SEEK_CUR);
        // Read a chunk and prepend it to our output
        $output = ($chunk = fread($f, $seek)) . $output;
        // Jump back to where we started reading
        fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
        // Decrease our line counter
        $lines -= substr_count($chunk, "\n");
    }
    // While we have too many lines
    // (Because of buffer size we might have read too many)
    while ($lines++ < 0) {
        // Find first newline and remove all text before that
        $output = substr($output, strpos($output, "\n") + 1);
    }
    // Close file and return
    fclose($f);
    return trim($output);
}
?>