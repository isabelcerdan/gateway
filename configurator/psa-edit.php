<?php
include_once ('functions/session.php');

$status = mysqli_real_escape_string($conn, $_GET['status']);
$status= filter_var($status, FILTER_SANITIZE_STRING);

if($status == 'success') {

    $stringResponse = '<strong>Success!</strong> The PSA settings have been updated';
    $alertString = 'alert-success';
}
if($status == 'error') {
    $stringResponse = '<strong>Failure</strong> Hmm... something went wrong';
    $alertString =  'alert-danger';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <<title>PSA Settings</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }
        .textFormMargin {
            margin-top: 8px;;
        }

        .has-error{ color: red};
        .has-success {  color: green}
        #understand-error { color: red};

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>



    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
        $(function() {
            $( "#tabs" ).tabs();
        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');


include_once ('control/get_psa.php'); // Get default data


?>
<div class="container">

    <?php
    // Header
  include_once ('header.php');

    // Menu Link //
  include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">PSA Settings</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <?php
                if( ( $status != '')) {
                    ?>
                    <div style="padding-left: 30px; padding-right: 30px;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert <?php echo $alertString; ?>" ?>
                                    <?php echo $stringResponse; ?></php>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_psa_edit.php" method="post"  id="psaForm">



                            <div class="formBoxSection">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">PSA Run State:</label>
                                    <div class="col-md-3">
                                        <select name="run_mode"  id="run_mode" class="form-control" required >
                                            < <?php
                                            if($run_mode != ''){
                                                echo "<option value='$run_mode'>$run_mode - Current</option>";
                                            }else {
                                                echo "<option value=''>Select</option>";
                                            }
                                            ?>
                                            <option value='idle'>Idle</option>
                                            <option value='unicast'>Unicast</option>
                                            <option value='multicast_all'>Multicast All</option>
                                            <option value='multicast_hos'>Multicast HOS</option>

                                        </select>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                            <a  data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </a>
                                            <strong>FYI: </strong> HOS = Head of String<br>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Audit delay</label>
                                    <div class="col-md-3">

                                        <div class="checkbox">
                                            <input class="form-control"  type="text" value="<?php echo $inter_loop_delay; ?>"  name="inter_loop_delay" id="inter_loop_delay">
                                        </div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                            <a  data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </a>
                                            <strong>Note: </strong> Time in seconds between system audit iterations. <br>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Uni-cast Query Delay</label>
                                    <div class="col-md-3">

                                        <div class="checkbox">
                                            <input class="form-control"  type="text" value="<?php echo $inter_query_delay; ?>"  name="inter_query_delay" id="inter_query_delay">
                                        </div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                            <a  data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </a>
                                            <strong>Note: </strong> Milliseconds between inverter uni-cast queries<br>

                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Enable PSA</label>
                                    <div class="col-md-3">

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="1" <?php if($enabled ==1){echo 'checked';}  ?> name="enabled">Enable</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                            <a  data-toggle="popover" title="Gateway Name" data-content="Unique name for gateway  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </a>
                                            <strong>Note: </strong> Start/Stop PSA <br>

                                        </div>
                                    </div>
                                </div>
                                -->
                            </div>
                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Started:</label>
                                    <div class="col-md-3">
                                       <div class="textFormMargin"><?php echo $started; ?></div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Run State:</label>
                                    <div class="col-md-3">
                                        <div class="textFormMargin"><?php echo $run_now; ?></div>                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                           </div>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Run Start Time:</label>
                                    <div class="col-md-3">
                                        <div class="textFormMargin"><?php echo $run_start_time; ?></div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Run End Time:</label>
                                    <div class="col-md-3">
                                        <div class="textFormMargin"><?php echo $run_end_time; ?></div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Updated:</label>
                                    <div class="col-md-3">
                                        <div class="textFormMargin"><?php echo date("Y-m-d H:i:s", substr($updated, 0, 10)); ?></div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">PID:</label>
                                    <div class="col-md-3">
                                        <div class="textFormMargin"><?php echo $pid ?></div>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                            </div>


                        <button type="submit" class="btn btn-primary" >Submit</button>
                        </div>


                    </form>

               


            </div>
        </div>
    </div>
    <?php    include_once ('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>