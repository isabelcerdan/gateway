<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php
include_once ('functions/mysql_connect.php');
$stringIdRaw = mysqli_real_escape_string($conn, $_GET['stringId']);
$stringIdRaw = filter_var($stringIdRaw, FILTER_SANITIZE_STRING);

include_once ('control/get_strings.php');

$result = $conn->query("SELECT * FROM strings WHERE stringId = '$stringIdRaw'");


while($row = $result->fetch_assoc()) {
     $stringId =  $row['stringId'];
     $string_name =  $row['string_name'];
     $group_id =  $row['group_id'];
     $location =  $row['location'];
    $notes =  $row['notes'];
     $photo =  $row['photo'];
     $FEED_NAME =  $row['FEED_NAME'];
     $buildout =  $row['buildout'];
    $string_offset =  $row['string_offset'];
}

$conn->close();

?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review String</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">


                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">String ID:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $stringId; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">String Name:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                          <?php echo $string_name; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Location:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                            <?php echo $location; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Notes:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                            <?php echo $notes; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Feed Name:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $FEED_NAME; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        </div>


                        <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                    </div>
                </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>

</div>
    <!-- Row end -->

</div>
</body>
</html>
