<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Router Data</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
  <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->


    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

<!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.4/dt-1.10.15/b-1.3.1/cr-1.3.3/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
-->


    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>


</head
<body>
<?php
include_once ('functions/mysql_connect.php');
include_once ('functions/get_dhcp_info.php');
include_once('control/get_inverters.php');

$dhcpFileLineContents = getDHCPInfo();
$IpParts = explode(".",$dhcpFileLineContents[1]);

?>

<script type="text/javascript" class="init">


    $(document).ready(function (){
        var table = $('#router').DataTable({
            'ajax': {
                'url':  "/control/getLeases.php"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            }],
            'order': [[1, 'asc']]
        });

        setInterval( function () {
            table.ajax.reload();
        }, 30000 );

        // Handle click on "Select all" control
        $('#router-select-all').on('click', function(){
            // Get all rows with search applied
            var rows = table.rows({ 'search': 'applied' }).nodes();
            // Check/uncheck checkboxes for all rows in the table
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#router tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is not checked
            if(!this.checked){
                var el = $('#router-select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if(el && el.checked && ('indeterminate' in el)){
                    // Set visual state of "Select all" control
                    // as 'indeterminate'
                    el.indeterminate = true;
                }
            }
        });

        // Handle form submission event
        $('#frm-router').on('submit', function(e){
            var form = this;

            // Iterate over all checkboxes in the table
            table.$('input[type="checkbox"]').each(function(){
                // If checkbox doesn't exist in DOM
                if(!$.contains(document, this)){
                    // If checkbox is checked
                    if(this.checked){
                        // Create a hidden element
                        $(form).append(
                            $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', this.name)
                                .val(this.value)
                        );
                    }
                }
            });

        });

    });

</script>
<div class="container">


    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Router Info</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>


                <div class="formBoxSection">

                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <H2 class="text-center">IP Range</H2>
                        <div class="text-center">
                            <p style="margin: 5px">Set the number of IP address for this Site</p>
                            <form name="numOfIP" id="numOfIP" action="dnsmasq-results.php" style="margin: 5px" method="post">
                                <input type="hidden" name='numOfIP' value="yes">
                                <select name="userIPSelect">
                                    <?php
                                    $currentNumIP = $IpParts[2] * 256;
                                    for($i =1; $i <= 50; $i++) {
                                        $numIPAddress = $i * 256;

                                        // Make sure that the user doesn't set the number of IP address too low //
                                        // The number has to be over one and a half of the total number of inverters in the database
                                        if ($numIPAddress  > count($inverters_id)+ count($inverters_id)/2) {
                                            if ($currentNumIP == $numIPAddress)
                                                echo "<option value='$i' selected>$numIPAddress Num of IPs</option>";
                                            else{
                                                echo "<option value='$i'>Set Num IPs:  $numIPAddress </option>";
                                            }
                                        }
                                    }
                                    ?>


                                    <
                                </select>
                                <input class="btn btn-primary" type="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
                </div>

                <div class="formBoxSection">

                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <H2 class="text-center">Router Table</H2>
                            <form name="rm-router" id="rm-router" action="dnsmasq-results.php" style="margin: 5px">
                            <table id="router" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" name="select_all" value="1" id="router-select-all"></th>
                                    <th>Mac</th>
                                    <th>IP</th>
                                    <th>Name</th>
                                    <th>Mac</th>
                                    <th>Lease Expiration</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Mac</th>
                                    <th>IP</th>
                                    <th>Name</th>
                                    <th>Mac</th>
                                    <th>Lease Start</th>

                                </tr>
                                </tfoot>
                            </table><p>
                                <lable>Router Command:</lable>
                                <select name="command">
                                    <option value="ping">ping</option>
                                    <option value="deletelease">Delete Lease</option>
                                    <option value="restart">Restart DHCP Server</option>
                                </select>
                                </p>
                                <p class="form-group">
                                    <button type="submit" class="btn btn-primary" id="routerFormSubmit">Submit</button>
                                </p>

                                </p>

                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>