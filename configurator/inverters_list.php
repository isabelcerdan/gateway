<?php
include_once ('functions/mysql_connect.php');
include_once('control/form_inverter_display.php'); // Get default data
$letters = range('A', 'G');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inverter List</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }




        /**
        * Featherlight Loader
        *
        * Copyright 2015, WP Site Care http://www.wpsitecare.com
        * MIT Licensed.
        */
        @-webkit-keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .featherlight-loading .featherlight-content {
            -webkit-animation: featherlightLoader 1s infinite linear;
        animation: featherlightLoader 1s infinite linear;
        background: transparent;
        border: 8px solid #8f8f8f;
        border-left-color: #fff;
        border-radius: 80px;
        width: 80px;
        height: 80px;
        min-width: 0;
        }

        .featherlight-loading .featherlight-content > * {
            display: none !important;
        }

        .featherlight-loading .featherlight-close,
        .featherlight-loading .featherlight-inner {
            display: none;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js?<?php echo rand(); ?>"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/js.cookie.js"></script>
    <script type="text/javascript" src="js/inverter-display.js?<?php echo rand(); ?>"></script>
</head>
<body>

<div class="container">
    <div class="apparentBrand">Apparent igOS Gateway</div>
    <?php
    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Inverters</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>
                <div class="panel-body">
                    <form method='post' action=" <?php echo $_SERVER['PHP_SELF']; ?> ">
                        <div style="margin: 5px; float: right" class="text-right"><label>Search:<input type="search" name="search" class="" placeholder="" ></label></div>
                    </form>
                    <div id="getCheckedinfo">Inverters Selected:<span id="invertersSelectedNum"></span> </div>

                    <form  method='post' action=" <?php echo $_SERVER['PHP_SELF']; ?> ">
                        <div style="margin: 5px;float: left" class="text-left"><label>Show # of items:
                                <select id="itemsperpage" name="itemsperpage" >
                                    <option value='4'" <?php if($limit == 4) {echo 'selected';} ?> >4</option>
                                    <option value='8'" <?php if($limit == 8) {echo 'selected';} ?> >8</option>
                                    <option value='16'" <?php if($limit == 16) {echo 'selected';} ?> >16</option>
                                    <option value='32'" <?php if($limit == 32) {echo 'selected';} ?> >32</option>
                                    <option value='64'" <?php if($limit == 64) {echo 'selected';} ?> >64</option>
                                    <option value='128'" <?php if($limit == 138) {echo 'selected';} ?> >128</option>
                                    <option value='256'" <?php if($limit == 138) {echo 'selected';} ?> >256</option>
                                    <option value='<?php echo $total; ?>'" <?php if($limit == $total) {echo 'selected';} ?>><?php echo $total; ?> - All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                    <div style="clear:both"> </div>
                    <table class="table" width="100%">

                        <tbody>
                        <tr>
                            <td width="40%"><input type="checkbox" id="select_all"/><span st> Select All</span></td>
                            <td width="10%"><button  type="button" class="btn btn-outline-secondary detectString" data-toggle="modal" data-target="#myModal">Detect Strings</button></td>
                            <td width="10%"><button id="inverters-start-db-con" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#myModal">Get Serial#</button></td>
                            <td width="10%"><button id="health-inverters" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#myModal">Health Scan</button></td>
                            <td width="10%"><button id="upgrade-inverters" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#myModal">Upgrade Inverters</button></td>
                            <td width="10%"><button id="delete-inverters" type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal">Delete Inverters</button></td>
                        </tr>

                        </tbody>
                    </table>
                    
                    <div class="formBoxSection ">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                                <tr >
                                    <th></th>
                                    <th class="text-center">Serial #<a style="float: right" href="inverters_list.php?orderby=serialNumber&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?> "><div class="sortInvertersBoth"></div></a> </th>
                                    <th class="text-center">IP Address <a style="float: right" href="inverters_list.php?orderby=IP_Address&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">MAC Address <a style="float: right" href="inverters_list.php?orderby=mac_address&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Version<a style="float: right" href="inverters_list.php?orderby=version&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">String<br>Id<a style="float: right" href="inverters_list.php?orderby=stringId&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">String<br>Pos<a style="float: right" href="inverters_list.php?orderby=stringPosition&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Upgrade<a style="float: right" href="inverters_list.php?orderby=upgrade_status&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Comm<a style="float: right" href="inverters_list.php?orderby=comm&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Watts<a style="float: right" href="inverters_list.php?orderby=watts&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">Feed Name<a style="float: right" href="inverters_list.php?orderby=FEED_NAME&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>

                                </tr>
                                <?php
                                //print_r($serialNumber);
                                // $rowId = 0;
                                for ($i = 0; $i < count($mac_address);$i++) {
                                    //$rowId++;
                                    ?><form class='forms' id="<?php echo $inverters_id[$i]; ?>" name="<?php echo $inverters_id[$i]; ?>" action="/control/form_inverters_update.php" method="post" >
                                    <input type="hidden" name="serialNumber" value="<?php echo $serialNumber[$i]; ?>">
                                    <input type="hidden" name="mac_address" value="<?php echo $mac_address[$i]; ?>">
                                    <tr >
                                        <td><input class="selectCheckBox checkbox"  id="select-<?php echo $inverters_id[$i];?>" type="checkbox" name="select-me" value="<?php echo $mac_address[$i]; ?>"></td>
                                        <td><?php echo $serialNumber[$i]; ?> </td>
                                        <td><?php echo $IP_Address[$i]; ?></td>
                                        <td><?php echo $mac_address[$i]; ?></td>
                                        <td><?php echo $version[$i]; ?></td>
                                        <td><?php echo $stringId[$i]; ?></td>
                                        <td><?php echo $stringPosition[$i]; ?></td>
                                        <td><?php echo $upgrade_status[$i]; ?></td>
                                        <td>
                                            <?php
                                            if($comm[$i] =='1' ){
                                                echo ' <span style="color: green" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='2' ){
                                                echo ' <span style="color: #ADFF2F" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='3' ){
                                                echo ' <span style="color: #ADFF2F" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='4' ){
                                                echo ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='5' ){
                                                echo ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='6' ){
                                                echo ' <span style="color: orange" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='7' ){
                                                echo ' <span style="color: red" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='8' ){
                                                echo ' <span style="color: blue" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='9' ){
                                                echo ' <span style="color: black" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            if($comm[$i] =='10' ){
                                                echo ' <span style="color: black" class= " glyphicon glyphicon-one-fine-dot"></span>';
                                            }
                                            ?>

                                        </td>
                                        <td><?php echo $watts[$i]; ?></td>

                                        <td  id="feed-<?php echo  $inverters_id[$i]; ?>" class="inputForms">
                                            <select name="FEED_NAME" id="id-controlledByGateway" class="selectpicker">
                                                <option value='' $selected>Select</option>
                                                <?php
                                                foreach ($letters as $one) {
                                                    $selected = '';
                                                    if( $one == "$FEED_NAME_inverter[$i]" ) {
                                                        $selected = 'selected';
                                                    }
                                                    for ($j = 0; $j < count($FEED_NAME_Array);$j++ ) {
                                                        if($one == $FEED_NAME_Array[$j] ) {
                                                            echo "  <br> <option value='$one' $selected>$one - Live</option>";
                                                            $one = null;
                                                            break;
                                                        }
                                                    }
                                                    if($one != null )
                                                        echo "<option value='$one' $selected>$one</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>

                                    </tr>
                                    </form>
                                    <?php
                                    // End of loop //
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <div class="text-center">
        <ul class="pagination">
            <?php
            if($num_page>1)
            {
                pagination($page,$num_page,$limit,$orderby);
            }
            ?>
        </ul>
    </div>
</body>
</html>