<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>IO Device Configure</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>

    
</head
<body>

<?php
include_once ('functions/mysql_connect.php');

$IOFlex_id = mysqli_real_escape_string($conn, $_POST['IOFlex_id']);
 $IOFlex_id= filter_var($IOFlex_id, FILTER_SANITIZE_STRING);
if ($IOFlex_id != '') {
    include_once('control/get_io.php'); // Get default data
}

?>



<div class="container">
    <?php
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">IO Devices</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_IO_add_edit.php" method="post" id="meterForm">
                        <input type="hidden" name="IOFlex_id" value="<?php echo $IOFlex_id; ?>">
                       


                        <div class="formBoxSection">



                            <div class="formBoxSectionWhite">
                                <div class="form-group">

                                    <div class="col-md-12 " >
                                        <div class="formTextSpacing text-center">
                                            <h2> Add/Edit <?php echo $apparent_part . " ID: " . $IOFlex_id; ?> IO Devices</h2>
                                            <?php
                                            // Errors
                                            if ($changes != '') {
                                                echo"<p style='color: red;font-weight: bold'>$changes </p>";
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="formBoxSectionWhite">

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Apparent Part Number:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="apparent_part" id="apparent_part" class="form-control" value="<?php echo $apparent_part; ?>">
                                        </div>
                                        <div class="col-md-6 " >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="Apparent Part Number" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong> Enter in the Apparent Part Number
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">IO Device Name:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="IOdevice_name" id="IOdevice_name" class="form-control" value="<?php echo $IOdevice_name; ?>" >
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="IO Device Name" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Optional: Give each meter a unique name
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Mac Address:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="mac_address" id="mac_address" class="form-control" value="<?php echo $mac_address; ?>" >

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Mac Address" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong>Please enter in the devices Mac Address.<br> Example 00:0a:95:9d:68:16

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Device Description:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="device_desc" id="device_desc" class="form-control" value="<?php echo $device_desc; ?>" >
                                        </div>
                                        <div class="col-md-6" >
                                            <div class="formTextSpacing">

                                                <a  data-toggle="popover" title="Mac Address" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                </a>
                                                <strong>Note:</strong>Please give a brief description of the device

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                   
<!--

-->
                                </div>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>