<?php
include_once ('functions/session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Manage Network Protector Users</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
        $(function() {
            $( "#tabs" ).tabs();
        });

        $(document).on("click", "a.delete-user", function() {
            if (confirm('Are you sure  you want to delete this user?' + this.id )) {
                $(location).attr('href', '/control/form_net_protect_email.php?netprotect_email_id=' + this.id + '&delete_user=yes');
            }
        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var

include_once ('control/get_network_protector_users.php'); // Get default data


?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Manage Network Protector Users</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">




                    <div class="formBoxSection form-horizontal row-border">

                        <div class="formBoxSection">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: smaller">
                                        <th>ID</th>
                                        <th>input_name</th>
                                        <th>Eamil</th>
                                        <th>Message</th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                    <tbody >
                                    <?php
                                    
                                        for($i =0; $i < count($netprotect_email_id);$i++) {
                                       
                                        ?>
                                        <tr>
                                            <td class='text-center'><?php echo $netprotect_email_id[$i]; ?></td>
                                            <td class='text-left'><?php echo $input_name[$i] ; ?></td>
                                            <td class='text-left'><?php echo $email_address[$i] ; ?></td>
                                            <td class='text-left'><?php echo $msg_body[$i] ; ?></td>

                                            <td class='text-right'><a href='net_protect_user_add-edit.php?netprotect_email_id=<?php echo $netprotect_email_id[$i]; ?>'> <button class="btn btn-warning" >Edit</button></a>

                                            </td>
                                            <td class='text-tight'><a class='delete-user' id='<?php echo $netprotect_email_id[$i]; ?>' > <button class="btn btn-danger" >Delete</button></a></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </tbody>
                            </table>



                        </div>
                    </div>


                    <a class="btn btn-primary" href="/net_protect_user_add-edit.php" role="button">Add New Users</a>

                </div>


                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>