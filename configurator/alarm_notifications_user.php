<?php
include_once ('functions/session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Alarm Notifications Users</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
        $(function() {
            $( "#tabs" ).tabs();
        });

        $(document).on("click", "a.delete-subscribers", function() {
            if (confirm('Are you sure  you want to delete this User? ' + this.id )) {
                $(location).attr('href', '/control/form_alarm_notifications-users.php?subscribers_id=' + this.id + '&delete_subscriber=yes');
            }
        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var

//include_once ('control/get_alarm_names.php'); // Get default data
include_once('control/get_alarm_subscribers.php');
include_once ('functions/get_readable_privileges.php');
// set Privileges human readable


?>
<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Alarm Notifications Users</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">


                    <div class="formBoxSection form-horizontal row-border">

                        <div class="formBoxSection">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: smaller">
                                        <th class='text-left'>ID</th>
                                        <th class='text-left'>Name</th>
                                        <th>Email</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody >

                                    <?php

                                        for($i =0; $i < count($subscribers_id_DB);$i++) {
                                        ?>
                                        <tr>
                                            <td class='text-left'><?php echo $subscribers_id_DB[$i] ; ?></td>
                                            <td class='text-left'><?php echo $subscribers_name_DB[$i] ; ?></td>
                                            <td class='text-left'><?php echo $email_address_DB[$i]; ?></td>
                                   
                                            <td class='text-right'><a href='alarm_notifications-user-add-edit.php?subscribers_id=<?php echo $subscribers_id_DB[$i]; ?>'> <button class="btn btn-warning" >Edit</button></a>
                                            <td class='text-center'><a class='delete-subscribers' id='<?php echo $subscribers_id_DB[$i]; ?>' > <button class="btn btn-danger" >Delete</button></a></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </tbody>
                            </table>



                        </div>
                    </div>

                        <table id='controlButtons' class="table" width="100%">

                            <tr>
                                <td width="15%" class="text-left"><a class="btn btn-primary" href="/alarm_notifications-user-add-edit.php" role="button">Add New User</a></td>
                                <td width="25%" class="text-left"></td>
                                <td width="60%" "></td>

                            </tr>

                        </table>

                </div>


                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>