<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Inverter Feed</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }

        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        .popover {
            top: -20px !important;
            left: 35px !important;
        }

        .topCalBoxes {
            margin-top: 10px;
        }

        .boldText {
            font-weight: bold;
        }

        .calc-target {
            padding-left: 20px;
        }

        .rendered-target {
            padding-top: 13px;
        }

        .gateway-perspective {
            padding-top: 37px;
        }

        p {
            margin-top: 5px;
        }

        .input-feed {
            width: 120px;
            padding-right: 3px;
        }
        .errorTxt{
            color:red;
            text-align: center;
            min-height: 20px;
            display: none;
        }
        input.error {
            border:1px dotted red;
        }
    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>
    <script>
        var gatewayPerspective  = '<?php echo $gateway_perspective; ?>';
        $(document).ready(function () {

            $('[data-toggle="popover"]').popover();


            

            $.validator.addMethod('integer', function (value, element, param) {
                return (value != 0) && (value == parseInt(value, 10));
            }, 'Please enter a non zero integer value!');

            $('#feed-inverter-count').on("change", function (ev) {
                var feed_size = parseFloat($(this).val()) * 0.282;
                $("#feed-size").text(feed_size.toFixed(3) + " kW");
                updateTarget();
            });

            $('#curtail_target_input').on("change", function (ev) {
                updateTarget();
            });

            $('.input-limit').on("change", function (ev) {
                updateTarget();
            });

            function updateTarget() {
                var perspective = $('#gateway-perspective').val();
                var feed_size = parseFloat($("#feed-inverter-count").val() * 0.282);
                var target_coeff = parseFloat($('#curtail_target_input').val());
                var target_offset = (target_coeff / 100.0) * feed_size;
                $('.input-limit-cell').each(function(index){
                    var limit = parseFloat($(this).find('.input-limit').val());
                    if (!isNaN(limit)) {
                        var target = limit + target_offset;
                        var direction = ((perspective == 'consumption' && target >= 0) ||
                        (perspective == 'production'  && target <= 0)) ?
                            "import" : "export";
                        $(this).find('.calc-target').text(target.toFixed(1) + " kW " + direction);
                    } else {
                        $(this).find('.calc-target').text("");
                    }
                });
            }

            updateTarget();
        });

    </script>
    <script type="text/javascript" src="js/feed-add-edit.js?<?php echo rand(); ?>"></script>


</head
<body>
<?php
include_once('functions/mysql_connect.php');

$FEED_NAME_form = mysqli_real_escape_string($conn, $_REQUEST['FEED_NAME']);
$FEED_NAME_form = filter_var($FEED_NAME_form, FILTER_SANITIZE_STRING);

$FEED_NAME = $FEED_NAME_form;

$errors = mysqli_real_escape_string($conn, $_GET['errors']);
$errors = filter_var($errors, FILTER_SANITIZE_STRING);

$setToDefault = mysqli_real_escape_string($conn, $_GET['setToDefault']);
$setToDefault = filter_var($setToDefault, FILTER_SANITIZE_STRING);

if (strpos($errors, 'no_affected_rows') !== false) {
    $changes = "No Changes Made";
}

include_once('functions/feed_names.php'); // Get default data

// if there isn't a feed in the DB, make sure that we are not displaying anything from the form //
$FEED_NAME = feed_names($FEED_NAME_form);
include_once('control/get_power_regulator.php'); // Get default data or current data for Feed
include_once('control/get_meter.php');

// lets not allow user to set meter feed limits on solar meter for now
unset($meters['solar']);

include_once('control/get_meter_feed.php');
include_once('control/get_battery_int.php');

?>



<div class="container">

    <?php
    // Header
    include_once('header.php');
    include_once('menu.php');

    $direction = array('consumption' => '+import/-export', 'production' => '+export/-import');
    ?>
    <input hidden id="gateway-perspective" name="gateway-perspective" value="<?php echo $gateway_perspective; ?>"/>
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-5"><h1 style="padding-left: 30px;">Inverter Feed</h1></div>
                    <div class="col-md-5 gateway-perspective"><?php echo "<strong>" . ucfirst($gateway_perspective) . "</strong> (" . $direction[$gateway_perspective] . ")"; ?></div>
                    <div class="col-md-2">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="formBoxSection">
                        <form class="form-horizontal row-border" action="/control/form_aggregate_feed.php" method="post"
                              id="inverter-feed-form">
                            <div class="errorTxt"></div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Feed<br/>Name</th>
                                    <th>Feed<br/>Size</th>
                                    <th>Curtail<br/>Target</th>
                                    <th>Select<br/>Battery</th>
                                    <th>Meter<br/>Feed</th>
                                    <?php foreach ($meter_roles as $role): ?>
                                        <th><?php echo strtoupper($role) ?><br/> Limit</th>
                                    <?php endforeach ?>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td scope="row" rowspan="5">Aggregate</td>
                                    <td rowspan="5">
                                        <div class="input-group" style="width:170px">
                                            <input class="form-control text-right" id="feed-inverter-count"
                                                   name="feed_inverter_count" type="text"
                                                   value="<?php echo $FEED_SIZE / 0.282; ?>">
                                            <div class="input-group-addon">SG424s</div>
                                        </div>
                                        <span id="feed-size" style="padding-left:18%"><?php echo $FEED_SIZE ?> kW</span>
                                       
                                    </td>

                                    <td scope="row" rowspan="5">
                                        <div class="input-group input-feed">
                                            <input name="curtail_target_input" id="curtail_target_input" class="form-control text-right"
                                                   type="text"
                                                   value="<?php echo $CURTAIL_TARGET_coeff; ?>"/>
                                            <div class="input-group-addon" >%</div>
                                        </div>

                                    </td>
                                    <td scope="row" rowspan="5"><select name="ess_unit_id" class="form-control">
                                            <?php
                                            for ($i = 0; $i < count($battery_id); $i++) {
                                               if ($ess_unit_id_set_feed == $battery_id[$i]) {
                                                    echo "<option value='$battery_id[$i]'> $battery_name[$i] - Current</option>";
                                                }
                                            }
                                            ?>
                                            <option value=''>none</option>
                                            <?php
                                            for ($i = 0; $i < count($battery_id); $i++) {
                                                echo "<option value='$battery_id[$i]'> $battery_name[$i] $battery_model_id[$i] </option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>
                                <?php
                                foreach ($feed_names as $feed_name) {
                                    echo "<tr>";
                                    echo "<td>" . ($feed_name == 'Delta' ? "&Delta;" : $feed_name);
                                    echo "<div class='rendered-target'>Target</div></td>";
                                    foreach ($meter_feeds[$feed_name] as $i => $meter_feed) {
                                        if ($meter_feed['delta'] == 1 && $feed_name != 'Delta') {
                                            echo "<td class='input-limit'><div style=\"width: 80px; text-align: right;\"></div></td>";
                                        } else {
                                            $name = "meter_feeds[" . $meter_order[$i] . "][" . $feed_name . "]";
                                            $value = $meter_feed ? $meter_feed['threshold'] : null;
                                            $id = "input-limit-" . $meter_order[$i] . "-" . $feed_name;
                                            echo "<td class='input-limit-cell'>";
                                            echo "<div class='input-group input-feed'>";
                                            echo "<input class='form-control text-right input-limit' name='$name' id='$id' type='text' value='$value' />";
                                            echo "<div class='input-group-addon'>kW</div>";
                                            echo "</div>";
                                            echo "<span class='calc-target text-center' id='$calc_target_id'></span></td>";
                                        }
                                    }
                                    echo "</tr>";
                                }
                                ?>
                                </tbody>
                            </table>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>

                <?php include_once('footer.php'); ?>
            </div>
            <!-- Row end -->

        </div>
</body>
</html>