<?php
// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');

///////////////////////////////////////////////////////////
// Get Strings
//////////////////////////////////////////////////////////

$stringIds = mysqli_real_escape_string($conn, $_REQUEST['stringId']);
$stringIds = filter_var($stringIds, FILTER_SANITIZE_STRING);
 $stringIds = str_replace("-",":",$stringIds);
//echo "<br>";
$enabled = mysqli_real_escape_string($conn, $_REQUEST['enabled']);
 $enabled = filter_var($enabled, FILTER_SANITIZE_STRING);
//echo "<br>";
$scan = mysqli_real_escape_string($conn, $_REQUEST['scan']);
 $scan= filter_var($scan, FILTER_SANITIZE_STRING);

$enabledDetect = '';

//Check to see if the process is still running

$result = $conn->query("SELECT command FROM feed_discovery_control limit 1  ");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $command = $row['command'];
    }
}else {
    $sqlInit = "INSERT INTO feed_discovery_control (command)  VALUES('no_command') ";
    if (!mysqli_query($conn,$sqlInit)) {
      // echo "ok 1";
    }
}



// Check to see if process is still running
if(($command == 'no_command')OR ($command == '') OR ($command == 'cancel') OR ($command == NULL)) {
 // echo "ok 2";
}else {
    header('Location: /feed_progress.php?error=Process_still_running');
    exit();
}

if($enabled == 'run_now') {
    $enabledDetect = 1;
    echo "<br>run_now";
    $sql = "UPDATE feed_discovery_control set command  = 'start' ";
    if (!mysqli_query($conn,$sql)) {
        //echo "<br>mysql problem";
    }
}

if($enabled == 'run_later') {
    $enabledDetect = 0;
}

if($scan = 'yes') {
    // To clear table to start new scan
    $sql = "DELETE FROM fd_by_string";
    if (mysqli_query($conn,$sql)) {
       //  echo 'success';
    }
    else {
        echo("Error description: " . mysqli_error($conn));
    }
}

        // Update the fd_by_stringtable with the inverters that are set for the upgrade
        if($stringIds != '') {
            $stringIdArray  = explode(",", $stringIds);

           // $sql = "INSERT INTO fd_by_string set upgrade_status ='upgrade_waiting' WHERE inverters_id IN ($inverterArray)";
            for ($i =0; $i < count($stringIdArray);$i++) {

             $stringInfo  = explode("_",$stringIdArray[$i]);

                //  Make sure that there isn't a string ID of zero
                if($stringInfo[0] != '0'){
                    $sql = ("INSERT INTO fd_by_string (stringId,enabled,feed_current,status)  values('$stringInfo[0]','$enabledDetect','$stringInfo[1]','waiting' )");
                    if (mysqli_query($conn, $sql)) {
                        echo "updated";
                    }
                    else {
                        echo("Error description: " . mysqli_error($conn));
                    }
                }

            }

            if($enabled == 'run_now') {
                $sql = "UPDATE feed_discovery_control set command  = 'start' ";
                if (!mysqli_query($conn,$sql)) {
                    //echo "worked";
                }
            }

            header('Location: /feed_progress.php');
            exit();

            }
        else {
            header('Location: /feed_progress.php?error=no_strings');
            exit();
           // echo 'no Strings';

}

