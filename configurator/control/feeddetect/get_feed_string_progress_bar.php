
<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/5/2016
 * Time: 3:34 PM

*/
// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');

$results = $conn->query("SELECT * FROM fd_by_string;");

if(mysqli_num_rows($results)>0) {

    while ($row = $results->fetch_assoc()) {
        $stringId[] = $row['stringId'];
        $status[] = $row['status'];
    }
}

$numberOfString = count($stringId);
$finishedCounter = array_count_values($status);
$finishedUpgrading = $finishedCounter['success'];
$totalFinsihed =  ($finishedUpgrading / $numberOfString) *100;


?>

<div class="progress" style="height: 30px; margin-bottom: 5px">
    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $totalFinsihed ?>%">
        <div style="margin-top: 5px; font-size: large"> <?php echo number_format($totalFinsihed,0) ?>% </div>
    </div>
</div>

<span style="font-size: xx-small; margin-bottom: 1px"><b>Total Strings:</b> <?php echo $numberOfString . " <b> Strings Scanned :</b>" . $finishedUpgrading; ?></span>