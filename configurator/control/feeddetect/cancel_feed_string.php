<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/5/2016
 * Time: 5:05 PM
 */

// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');

$cancel_feed_detect = mysqli_real_escape_string($conn, $_REQUEST['cancel_feed_detect']);
$cancel_feed_detect = filter_var($cancel_feed_detect, FILTER_SANITIZE_STRING);

if($cancel_feed_detect == 'yes') {
    $sql = "DELETE FROM fd_by_string";
    if (mysqli_query($conn,$sql)) {
        // echo 'success';
    }

    $sql = "UPDATE feed_discovery_control set command  = 'cancel' ";
    if (!mysqli_query($conn,$sql)) {
        //echo "worked";
    }

}