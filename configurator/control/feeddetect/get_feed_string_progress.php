<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/17/2016
 * Time: 4:49 PM
 */

// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');


 //   $result = $conn->query("SELECT * FROM inverters WHERE upgrade_status IN ('upgrade_waiting','in_queue','in_progress','updater_in_progress','loader_in_progress','complete_success','complete_failed')");

$result = $conn->query("SELECT * FROM fd_by_string");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $stringId[] = $row['stringId'];
        $enabled[] = $row['enabled'];
        $feed_current[] = $row['feed_current'];
        $feed_discovered[] = $row['feed_discovered'];
        $feed_command[] = $row['feed_command'];
        $status[] = $row['status'];
    }
}
/*
$numberOfInverters = count($inverters_id);

$stringId = array_count_values($stringId);
$finishedUpgrading = $finishedCounter['complete_success'];

$totalFinsihed =  ($finishedUpgrading / $numberOfInverters) *100;
*/
for ($i = 0; $i < count($stringId);$i++) {
    $upgrade_progress = '';
    //$rowId++;
    if($status[$i] == 'success' ){
        $upgrade_progress = '100';
    }elseif($status[$i] == 'STBY measure' ) {
        $upgrade_progress = '35';
    }elseif($status[$i] == 'ACT measure' ) {
        $upgrade_progress = '75';
    }
    else{
        $upgrade_progress = '0';
    }
    ?>
    <tr >
        <td><?php echo $stringId[$i]; ?></td>
        <td><?php echo $feed_current[$i]; ?></td>
        <td><?php echo $feed_discovered[$i]; ?></td>
        <td><?php echo $feed_command[$i]; ?></td>
        <td><?php echo $status[$i]; ?></td>

        <td>
            <div class="progress">
                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $upgrade_progress; ?>%">
                    <?php echo number_format($upgrade_progress,0) ; ?>% Complete
                </div>
            </div>

        </td>

    </tr>

    <?php
    // End of loop //
}

//$conn->close();

?>