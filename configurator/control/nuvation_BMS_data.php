<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 1/18/2018
 * Time: 4:26 PM
 * http://10.252.2.1/api.jsonp?callback=&{%22method%22:%22readRegisters%22,%22params%22:[4129,151553,151569,4128,65536,65537,147457,147473,90112,90113,90114,90115,28674,28676,28672,28675,28673,139265,139281,139313,139329,28690,28692,28688,28691,28689,143361,143377,143425,143441,131072,131077,4592,200705],%22id%22:0}&_=1515461203
IP_Address VARCHAR(20),
time_last_reading  timestamp,
stack_voltage float,
state_of_charge float,
stack_current float,
depth_of_discharge float,
avg_cell_voltage float,
max_cell_voltage float,
min_cell_voltage float,
avg_temp  float,
max_temp  float,
min_temp  float
 */



// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$result = $conn->query("select id from ess_units");
while($row = $result->fetch_assoc()) {
    $battery_id = $row['id'];
}

$unixTime =time();
$battery_IP = "http://10.252.2.1";
////echo($t . "<br>");

$json = file_get_contents( $battery_IP .'/api.jsonp?callback=&{%22method%22:%22readRegisters%22,%22params%22:[4129,151553,151569,4128,65536,65537,147457,147473,90112,90113,90114,90115,28674,28676,28672,28675,28673,139265,139281,139313,139329,28690,28692,28688,28691,28689,143361,143377,143425,143441,131072,131077,4592,200705],%22id%22:0}&_=' . $unixTime);

//

$json = str_replace('JSONP_Callback','',$json);
$json = str_replace("('",'',$json);
$json = str_replace("')",'',$json);
$json = str_replace(";",'',$json);


//print_r($json);

$obj = json_decode($json, true);
//$result = json_decode($data, true);

$stack_voltage = mysqli_real_escape_string($conn, $obj['result'][0]);
$stack_voltage = filter_var($stack_voltage, FILTER_SANITIZE_STRING);
//echo "stack_voltage: ". $stack_voltage;
//echo "<br>";
$stack_voltage = $stack_voltage / 1000; // to match Nuv system

$state_of_charge = mysqli_real_escape_string($conn, $obj['result'][8]);
$state_of_charge = filter_var($state_of_charge, FILTER_SANITIZE_STRING);
//echo "state_of_charge ?: ". $state_of_charge ;
//echo "<br>";

$stack_current = mysqli_real_escape_string($conn, $obj['result'][3]);
$stack_current = filter_var($stack_current, FILTER_SANITIZE_STRING);
//echo "stack_current: ". $stack_current;
//echo "<br>";
$stack_current = $stack_current / 1000; // to match Nuv system

$depth_of_discharge = mysqli_real_escape_string($conn, $obj['result'][9]);
$depth_of_discharge = filter_var($depth_of_discharge, FILTER_SANITIZE_STRING);
//echo "depth_of_discharge: ". $depth_of_discharge;
//echo "<br>";

$avg_cell_voltage = mysqli_real_escape_string($conn, $obj['result'][13]);
$avg_cell_voltage = filter_var($avg_cell_voltage, FILTER_SANITIZE_STRING);
//echo "avg_cell_voltage: ". $avg_cell_voltage;
//echo "<br>";

$max_cell_voltage = mysqli_real_escape_string($conn, $obj['result'][12]);
$max_cell_voltage= filter_var($max_cell_voltage, FILTER_SANITIZE_STRING);
//echo "max_cell_voltage: ". $max_cell_voltage ;
//echo "<br>";

$min_cell_voltage  = mysqli_real_escape_string($conn, $obj['result'][14]);
$min_cell_voltage = filter_var($min_cell_voltage , FILTER_SANITIZE_STRING);
//echo "min_cell_voltage: ". $min_cell_voltage ;
//echo "<br>";

$avg_temp  = mysqli_real_escape_string($conn, $obj['result'][22]);
$avg_temp = filter_var($avg_temp, FILTER_SANITIZE_STRING);
//echo "avg_temp?: ". $avg_temp;
//echo "<br>";

$max_temp  = mysqli_real_escape_string($conn, $obj['result'][21]);
$max_temp= filter_var($max_temp, FILTER_SANITIZE_STRING);
////echo "max_temp: ". $max_temp;
////echo "<br>";

$min_temp  = mysqli_real_escape_string($conn, $obj['result'][23]);
$min_temp= filter_var($min_temp, FILTER_SANITIZE_STRING);
///echo "min_temp?: ". $min_temp;
//echo "<br>";



// Messy need to refactor ... //

if($battery_id < 1) {
//echo "<p>Inserting</p>";
//    $sql = "INSERT INTO nuvation_bms (stack_voltage,state_of_charge,stack_current,depth_of_discharge,avg_cell_voltage,max_cell_voltage,min_cell_voltage,avg_temp,max_temp,min_temp)
//            values('$stack_voltage', '$state_of_charge','$stack_current','$depth_of_discharge','$avg_cell_voltage','$max_cell_voltage','$min_cell_voltage','$avg_temp','$max_temp','$min_temp');";
    $sql = "INSERT INTO ess_master_info (battery_stack_voltage,battery_stack_soc,battery_stack_current,battery_max_cell_voltage,battery_min_cell_voltage,battery_max_cell_temperature,battery_min_cell_tempreature) 
            values('$stack_voltage', '$state_of_charge','$stack_current','$max_cell_voltage','$min_cell_voltage','$max_temp','$min_temp');";
    if (!mysqli_query($conn,$sql)) {
       // header('Location: /net_protect_users.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
       die('<br>Error: ' . mysqli_error($conn));
    }
}else {
    //echo "<p>Updating</p>";
//   $sql = "UPDATE nuvation_bms  set stack_voltage = '$stack_voltage',state_of_charge = '$state_of_charge',stack_current= '$stack_current',depth_of_discharge = '$depth_of_discharge'
//  ,avg_cell_voltage = '$avg_cell_voltage',max_cell_voltage = '$max_cell_voltage', min_cell_voltage= '$min_cell_voltage', avg_temp= '$avg_temp', max_temp= '$max_temp', min_temp= '$min_temp' WHERE battery_id = '$battery_id' ";
   $sql = "UPDATE ess_master_info set battery_stack_voltage = '$stack_voltage',battery_stack_soc = '$state_of_charge',battery_stack_current= '$stack_current',
           battery_max_cell_voltage = '$max_cell_voltage', battery_min_cell_voltage= '$min_cell_voltage', battery_max_cell_temperature= '$max_temp', battery_min_cell_tempreature= '$min_temp'";

   if (!mysqli_query($conn,$sql)) {
    //    header('Location: /net_protect_users.php?minion_auditor_id=' . $minion_auditor_id);
       die('<br>Error: ' . mysqli_error($conn));
   }

}


