<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 5/8/2019
 * Time: 5:42 PM
 * +-------------------+------------------+------+-----+---------+----------------+
| Field             | Type             | Null | Key | Default | Extra          |
+-------------------+------------------+------+-----+---------+----------------+
| meter_id          | int(11) unsigned | NO   | PRI | NULL    | auto_increment |
| meter_function    | int(5)           | NO   |     | NULL    |                |
| meter_name        | varchar(15)      | NO   |     | NULL    |                |
| meter_description | varchar(255)     | YES  |     | NULL    |                |
| target_interface  | varchar(100)     | YES  |     | NULL    |                |
+-------------------+------------------+------+-----+---------+----------------+
 *
 */
session_start();
//include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

if($meter_id != '') {
    $result = $conn->query("SELECT * FROM meter_roles WHERE meter_id = '$meter_id'");
    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $meter_role_id = $row['meter_id'];
            $meter_role_function = $row['meter_function'];
            //Uncomment out when name problem is resolved in Python code //
            //$meter_role_name = $row['meter_name']; 
            $meter_role_name = str_replace("dev/","",$row['target_interface']);
            $meter_role_description = $row['meter_description'];
            $target_role_interface = $row['target_interface'];
        }
    }
}else {
    $result = $conn->query("SELECT * FROM meter_roles");
    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $meter_role_id[] = $row['meter_id'];
            $meter_role_function[] = $row['meter_function'];
            //Uncomment out when name problem is resolved in Python code //
            //$meter_role_name[] = $row['meter_name'];
            $meter_role_name[] = str_replace("/dev/","",$row['target_interface']);
            $meter_role_description[] = $row['meter_description'];
            $target_role_interface[] = $row['target_interface'];
        }
    }
}



//$conn->close();

?>