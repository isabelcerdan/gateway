<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$feed = mysqli_real_escape_string($conn, $_POST['feed']);
 $feed = filter_var($feed, FILTER_SANITIZE_STRING);

//$Bucket_Coefficient = mysqli_real_escape_string($conn, $_POST['Bucket_Coefficient']);
//$Bucket_Coefficient = filter_var($Bucket_Coefficient, FILTER_SANITIZE_STRING);

$Bucket_Size = mysqli_real_escape_string($conn, $_POST['Bucket_Size']);
$Bucket_Size= filter_var($Bucket_Size, FILTER_SANITIZE_STRING);

$Bucket_Period = mysqli_real_escape_string($conn, $_POST['Bucket_Period']);
$Bucket_Period = filter_var($Bucket_Period, FILTER_SANITIZE_STRING);

$Bucket_Enabled = mysqli_real_escape_string($conn, $_POST['Bucket_Enabled']);
$Bucket_Enabled = filter_var($Bucket_Enabled, FILTER_SANITIZE_STRING);


//$Reinitialize_Bucket = mysqli_real_escape_string($conn, $_POST['Reinitialize_Bucket']);
//$Reinitialize_Bucket = filter_var($Reinitialize_Bucket, FILTER_SANITIZE_STRING);


$sql = "UPDATE power_regulators SET bucket_enabled ='$Bucket_Enabled',bucket_size  = '$Bucket_Size',bucket_period = '$Bucket_Period'  WHERE feed_name = '$feed'";
/*
if($Reinitialize_Bucket != '') {
    echo "trying energy";
    //Reinitialize_Bucket = '$Reinitialize_Bucket'
    $sql2 = "UPDATE energy_bucket_config SET reinitialize_bucket ='$Reinitialize_Bucket'";
    if (!mysqli_query($conn,$sql2)) {
        //header('Location: /bucket.php?update=error'  );
        die('<br>Error: ' . mysqli_error($conn));
    }
}
*/

if (!mysqli_query($conn,$sql)) {
    header('Location: /bucket.php?update=error'  );
    //ie('<br>Error: ' . mysqli_error($conn));
}
else {

    mysqli_close($con);
    header('Location: /bucket.php?update=success');

}

?>