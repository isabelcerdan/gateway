<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *+-----------------------------+-------------+------+-----+---------+-------+
| Field                       | Type        | Null | Key | Default | Extra |
+-----------------------------+-------------+------+-----+---------+-------+
| enabled                     | tinyint(1)  | NO   |     | 1       |       |
| started                     | tinyint(1)  | NO   |     | 0       |       |
| wait_b4_start               | int(11)     | NO   |     | 60      |       |
| pf_command_freq             | int(11)     | NO   |     | 20      |       |
| num_readings_for_pf_upd     | int(11)     | NO   |     | 10      |       |
| pf_regional_default         | float       | NO   |     | 0.95    |       |
| pf_regional_default_leading | tinyint(1)  | NO   |     | 0       |       |
| pf_target_A                 | float       | NO   |     | 0.95    |       |
| pf_target_A_leading         | tinyint(1)  | YES  |     | NULL    |       |
| pf_target_B                 | float       | NO   |     | 0.95    |       |
| pf_target_B_leading         | tinyint(1)  | YES  |     | NULL    |       |
| pf_target_C                 | float       | NO   |     | 0.95    |       |
| pf_target_C_leading         | tinyint(1)  | YES  |     | NULL    |       |
| pf_enable_A                 | tinyint(1)  | NO   |     | 1       |       |
| pf_enable_B                 | tinyint(1)  | NO   |     | 1       |       |
| pf_enable_C                 | tinyint(1)  | NO   |     | 1       |       |
| pf_log_A                    | tinyint(1)  | NO   |     | 0       |       |
| pf_log_B                    | tinyint(1)  | NO   |     | 0       |       |
| pf_log_C                    | tinyint(1)  | NO   |     | 0       |       |
| pf_data_log_freq            | int(11)     | NO   |     | 5       |       |
| pf_data_log_A               | tinyint(1)  | NO   |     | 0       |       |
| pf_data_log_B               | tinyint(1)  | NO   |     | 0       |       |
| pf_data_log_C               | tinyint(1)  | NO   |     | 0       |       |
| lia                         | varchar(5)  | NO   |     | NO      |       |
| interval_freq               | int(11)     | NO   |     | 5       |       |
| interval_samples            | int(11)     | NO   |     | 5       |       |
| interval_table_max_size     | int(11)     | NO   |     | 50      |       |
| updated                     | tinyint(1)  | NO   |     | 0       |       |
| pid                         | varchar(10) | NO   |     | 0       |       |
+-----------------------------+-------------+------+-----+---------+-------+
 * */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$results = $conn->query("SELECT *  FROM power_factor_control");

if(mysqli_num_rows($results)>0) {

    while($row = $results->fetch_assoc()) {
        $enabled = $row['enabled'];

        $pf_target_A = $row['pf_target_A'];
        $pf_target_A_leading = $row['pf_target_A_leading'];
        $pf_enable_A= $row['pf_enable_A'];

        $pf_target_B = $row['pf_target_B'];
        $pf_target_B_leading = $row['pf_target_B_leading'];
        $pf_enable_B= $row['pf_enable_B'];

        $pf_target_C = $row['pf_target_C'];
        $pf_target_C_leading = $row['pf_target_C_leading'];
        $pf_enable_C= $row['pf_enable_C'];
    }
}


?>