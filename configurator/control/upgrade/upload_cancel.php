<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/5/2016
 * Time: 5:05 PM
 */
// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');

$cancel_upgrade = mysqli_real_escape_string($conn, $_REQUEST['cancel_upgrade']);
$cancel_upgrade = filter_var($cancel_upgrade, FILTER_SANITIZE_STRING);

if($cancel_upgrade == 'yes') {
    $sql = "UPDATE inverter_upgrade set upgrade_command='cancel_upgrade' ";

    if (mysqli_query($conn, $sql)) {
        //echo "Record updated successfully";
    } else {
        //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) . 'multi:' . $multi);
        echo "Error updating record: " . mysqli_error($conn);
    }
/*
 *  This is is being handled by the monitor in python
    $sql_inverters = "UPDATE inverters set upgrade_status='no_action', upgrade_progress = '0' ";

    if (mysqli_query($conn, $sql_inverters)) {
        //echo "Record updated successfully";
    } else {
        //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) . 'multi:' . $multi);
        echo "Error updating record: " . mysqli_error($conn);
    }
*/

}