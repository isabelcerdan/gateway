
<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/5/2016
 * Time: 3:34 PM

*/
include_once('../../functions/mysql_connect.php');




$result = $conn->query("SELECT * FROM inverters WHERE upgrade_status IN ('start_upgrade','upgrade_in_progress','cancel_upgrade','upgrade_waiting','complete_success','in_progress','in_queue')");


//WHERE upgrade_status IN ('start_upgrade','upgrade_in_progress','cancel_upgrade')
if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $inverters_id[] = $row['inverters_id'];
        $upgrade_status[] =  $row['upgrade_status'];
        $upgrade_progress[] =  $row['upgrade_progress'];
    }
}

$numberOfInverters = count($inverters_id);

$finishedCounter = array_count_values($upgrade_status);
$finishedUpgrading = $finishedCounter['complete_success'];

 $totalFinsihed =  ($finishedUpgrading / $numberOfInverters) *100;


?>

<div class="progress" style="height: 30px; margin-bottom: 5px">
    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $totalFinsihed ?>%">
        <div style="margin-top: 5px; font-size: large"> <?php echo number_format($totalFinsihed,0) ?>% </div>
    </div>
</div>

<span style="font-size: xx-small; margin-bottom: 1px"><b>Total Inverters:</b> <?php echo $numberOfInverters . " <b>Upgraded Inverters:</b>" . $finishedUpgrading; ?></span>