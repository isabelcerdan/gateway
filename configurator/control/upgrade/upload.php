<?php

// Access restriction
include_once ('../../functions/restrict_privilage_access.php');

include_once('../../functions/mysql_connect.php');

///////////////////////////////////////////////////////////
// See if there are already items in the inverter_upgrade table
//////////////////////////////////////////////////////////
include_once('get_upgrade_info.php'); // Get default data

// init variables
$upgrade_command ='start_upgrade';
$software_directory = $software_directory_DB; // "/usr/share/apparent/upload";
$update_id = 1;

define("UPLOAD_DIR", $software_directory);

$inverters_id = mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
$inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);


////////////////////////////////////////////////////////////
// File Upload
////////////////////////////////////////////////////////
if (!empty($_FILES["myFile"])) {
    $myFile = $_FILES["myFile"];

    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        echo "<p>An error occurred.</p>";
        exit;
    }

    // ensure a safe filename
    $upgradeFileName = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

    // don't overwrite an existing file
        /*
            $i = 0;
            $parts = pathinfo($upgradeFileName);
            while (file_exists(UPLOAD_DIR . $upgradeFileName)) {
                $i++;
                $upgradeFileName = $parts["filename"] . "-" . $i . "." . $parts["extension"];
            }
        */

    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . '/'. $upgradeFileName);
//////////////////////////////////////////////////////////////

    // if problem with file upload
    if (!$success) {

        echo "<p>Unable to save file.</p>";
        exit;
    }
    else {
    // update the file name and set the upgrade command in the inverter_upgrade table

        if($upgrade_id_DB != '') {
            $sql = "UPDATE inverter_upgrade set file_name ='$upgradeFileName', upgrade_command='$upgrade_command' ";

            if (mysqli_query($conn, $sql)) {
                //echo "Record updated successfully";
            } else {
                //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) . 'multi:' . $multi);
                echo "Error updating record: " . mysqli_error($conn);
            }
        
        }else {
            $sql = "INSERT INTO inverter_upgrade (file_name,upgrade_id,software_directory,upgrade_command) VALUE('$upgradeFileName','$update_id','$software_directory','$upgrade_command')";

            if (mysqli_query($conn, $sql)) {
                echo "File inserted successfully";

            } else {
                //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) . 'multi:' . $multi);
                echo "Error updating record: " . mysqli_error($conn);
            }
        }

        // Update the inverter table with the inverters that are set for the upgrade
        if($inverters_id != ''){
            $inverters = explode(",",$inverters_id);
            foreach($inverters as $key => $inverter) {
                $inverterArray[$key] = "'$inverter'";
            }

            $inverterArray = join(', ',  $inverterArray);

            $sql = "UPDATE inverters set upgrade_status ='upgrade_waiting' WHERE inverters_id IN ($inverterArray)";

            if (mysqli_query($conn, $sql)) {
                 echo "Record updated successfully";
                header('Location: /update_progress.php');
               // header('Location: /inverters_list.php?rows=' .  mysqli_affected_rows($conn));
                // header('Location: /inverters_list.php?rows=' .  $multi);
            } else {
                //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) . 'multi:' . $multi);
                 echo "Error updating record: " . mysqli_error($conn);
            }

            mysqli_close($conn);
            //exit();
            }
        else {
            //echo 'no inverters';
        }
    }

//mysqli_close($conn);
    // set proper permissions on the new file
    chmod(UPLOAD_DIR .'/' . $upgradeFileName, 0644);
}

