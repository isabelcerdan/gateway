<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/17/2016
 * Time: 4:49 PM
 */


include_once('../../functions/mysql_connect.php');


 //   $result = $conn->query("SELECT * FROM inverters WHERE upgrade_status IN ('upgrade_waiting','in_queue','in_progress','updater_in_progress','loader_in_progress','complete_success','complete_failed')");

$result = $conn->query("SELECT * FROM inverters WHERE upgrade_status like '%upgrade_waiting%' or upgrade_status like '%in_queue%' or upgrade_status like '%in_progress%' or upgrade_status like '%updater_in_progress%' or upgrade_status like '%loader_in_progress%' or upgrade_status like '%complete_success%' or upgrade_status like '%complete_failed%' ");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $inverters_id[] = $row['inverters_id'];
        $serialNumber[] = $row['serialNumber'];
        $group_id[] = $row['group_id'];
        $mac_address[] =  $row['mac_address'];
        $IP_Address[] =  $row['IP_Address'];
        $version[] =  $row['version'];
        $lable[] =  $row['lable'];
        $stringId = $row['stringId'];
        $stringPosition[] =  $row['stringPosition'];
        $export_status[] =  $row['export_status'];
        $prodPartNo[] =  $row['prodPartNo'];
        $output_status[] =  $row['output_status'];
        $controlledByGateway[] =  $row['controlledByGateway'];
        $FEED_NAME_inverter[] =  $row['FEED_NAME'];
        $status[] =  $row['status'];
        $upgrade_status[] =  $row['upgrade_status'];
        $upgrade_progress[] =  $row['upgrade_progress'];
    }
}

$numberOfInverters = count($inverters_id);

$finishedCounter = array_count_values($upgrade_status);
$finishedUpgrading = $finishedCounter['complete_success'];

$totalFinsihed =  ($finishedUpgrading / $numberOfInverters) *100;

for ($i = 0; $i < count($mac_address);$i++) {
    //$rowId++;
    ?>
    <tr >
        <td><?php echo $serialNumber[$i]; ?> </td>
        <td><?php echo $IP_Address[$i]; ?></td>
        <td><?php echo $mac_address[$i]; ?></td>
        <td><?php echo $upgrade_status[$i]; ?></td>

        <td>
            <div class="progress">
                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $upgrade_progress[$i]; ?>%">
                    <?php echo number_format($upgrade_progress[$i],0) ; ?>% Complete
                </div>
            </div>

        </td>

    </tr>

    <?php
    // End of loop //
}

//$conn->close();

?>