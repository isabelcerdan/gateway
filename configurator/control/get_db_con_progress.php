<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/17/2016
 * Time: 4:49 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');


include_once ('../functions/mysql_connect.php');


 //   $result = $conn->query("SELECT * FROM inverters WHERE upgrade_status IN ('upgrade_waiting','in_queue','in_progress','updater_in_progress','loader_in_progress','complete_success','complete_failed')");

$result = $conn->query("select title, runs, active, run_now, progress from minion_auditor WHERE title = 'db_and_inverter_consistency'");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $title = $row['title'];
        $runs = $row['runs'];
       // $suspended = $row['suspended'];
        $active = $row['active'];
        $run_now = $row['run_now'];
        $progress = $row['progress'];
    }
}


?>
    <tr >
        <td><?php echo $title; ?></td>
        <td><?php echo $runs; ?></td>
        <td><?php echo $active; ?></td>
        <td><?php echo $run_now; ?></td>

        <td>
            <div class="progress">
                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo number_format($progress,0) ; ?>%">
                    <?php echo number_format($progress,0) ; ?>% Complete
                </div>
            </div>

        </td>

    </tr>
