<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$meters = [];

if($meter_id == '') {
    //echo "No id";
    $result = $conn->query("SELECT * FROM energy_meter");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $meter_id_DB[]=  $row['meter_id'];
            $meter_type_DB[] =  $row['meter_type'];
            $meter_name_DB[] =  $row['meter_name'];
            $group_id_DB[] =  $row['group_id'];
            $mac_address_DB[] =  $row['mac_address'];
            $meter_role_DB[] =  $row['meter_role'];
            $meter_class_DB[] =  $row['meter_class'];
            $modbusdevicenumber_DB[] =  $row['modbusdevicenumber'];
            $address_DB[] =  $row['address'];
            $baud_DB[] =  $row['baud'];
            $network_comm_type_DB[]=  $row['network_comm_type'];
            $meter_delta_DB[] = $row['delta'];
            $online_DB[] =  $row['online'];
            $ct_orientation_reversed_DB[] = $row['ct_orientation_reversed'];
            $meters[$row['meter_role']] = $row;
        }
    }
}

else {
   // echo "yes id";
    $result = $conn->query("SELECT *  FROM energy_meter WHERE meter_id = '$meter_id'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $meter_id =  $row['meter_id'];
            $meter_name =  $row['meter_name'];
            $meter_type =  $row['meter_type'];
            $group_id =  $row['group_id'];
            $mac_address =  $row['mac_address'];
            $meter_role =  $row['meter_role'];
            $meter_class =  $row['meter_class'];
            $modbusdevicenumber =  $row['modbusdevicenumber'];
            $address =  $row['address'];
            $baud =  $row['baud'];
            $network_comm_type=  $row['network_comm_type'];
            $meter_delta = $row['delta'];
            $ct_orientation_reversed = $row['ct_orientation_reversed'];
            $online =  $row['online'];

            $meters[$row['meter_role']] = $row;
        }
    }
}

$meter_roles = [];
foreach (['pcc', 'pcc2'] as $role) {
    if (array_key_exists($role, $meters)) {
        array_push($meter_roles, $role);
        if ($role == 'pcc2') {
            array_push($meter_roles, 'pcc_combo');
        }
    }
}

   // $conn->close();

?>