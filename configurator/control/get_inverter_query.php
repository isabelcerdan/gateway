<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/25/2017
 * Time: 12:31 PM


GOT list of TLV (len 29) and list of tlv values (length 29)
TLV 1003: QUERY_CTAG = 12345678
TLV 1005: PROD_PART_NO = ATIRA5486
TLV 1004: VERSION = 313svn1125
TLV 1006: SERIAL_NO = 171509000003
TLV 1007: MAC_ADDRESS = E0:1F:0A:01:09:14
TLV 1008: PCC = 0
TLV 1009: FEED = 0
TLV 1011: BOOT_VERSION = 40
TLV 1010: RUNNING_MODE = 1
TLV 1012: XET_INFO = SG424 DEVELOP R313svn1125 ATIRA5486 1 28 10
TLV 1013: CATCH_UP_REPORTS_ACTIVE = 0
TLV 1014: CATCH_UP_REPORTS_BOTH = 0
TLV 1015: CATCH_UP_REPORTS_INFO = 0
TLV 1016: CATCH_UP_REPORTS_EPBB = 0
TLV 1020: NUMBER_OF_RESETS = 384
TLV 1023: INVERTER_SW_PROFILE = 10
TLV 1024: INVERTER_EEPROM_INV_CTRL = 0x80
TLV 1028: UPTIME_SECS = 330988
TLV 1027: KEEP_ALIVE_TIMEOUT = 3000
TLV 1025: INVERTER_MSM_SM_STATE = 0x08
TLV 1026: INVERTER_DC_SM_STATE = 0x0a
TLV 1032: ERC = 0x00
TLV 1034: PH_ANGLE = 0
TLV 1038: PF = 0.950
TLV 1039: PF_LEAD = 0
TLV 1040: PF_ENA = 1
TLV 1043: DC_WATTS = 252
TLV 1044: DC_VOLTS = 30.00
TLV 1045: DC_AMPS = 8.39
DONE
 */
include_once ('../functions/session.php');

if($IP_Address != '') {
    // old command
   // $results = exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py query $IP_Address", $outcome, $status3);

    $results = exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py psa decode $IP_Address", $outcome, $status3);

    ?>
<table style="width:80%">
    <?php

    for ($i=0;$i <count( $outcome);$i++){
        $color = '';
        if($i % 2 == 0){
            $color = '#F5F5F5';
        }
        else{
            $color = '';
        }
        echo "<tr bgcolor='$color'>";
        $pieces = explode("=", $outcome[$i]);
        echo "<td>" .$pieces[0] ."</td>";
        echo "<td >" .$pieces[1] ."</td>";
        echo "</tr>";
    }
}
?>
</table>
