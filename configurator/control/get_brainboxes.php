<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');
include_once ($_SERVER['DOCUMENT_ROOT'] . '/functions/mysql_connect.php');

if(!isset($brainbox_id) || $brainbox_id == '') {
  // echo "No id";
    $result = $conn->query("SELECT *  FROM brainboxes order by brainbox_id");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $brainbox_idDB[] = $row['brainbox_id'];
            $target_interfaceDB[] = $row['target_interface'];
            $nicknameDB[]  = $row['nickname'];
            $model_numberDB[]  = $row['model_number'];
            $apparent_partDB[]  = $row['apparent_part'];
            $IP_AddressDB[] = $row['IP_Address'];
            $mac_addressDB[] = $row['mac_address'];
            $baudDB[] = $row['baud'];
            $tcp_portDB[] = $row['tcp_port'];
            $tcp_port_serial_1DB[] = $row['tcp_port_serial_1'];
            $tcp_port_serial_2DB[] = $row['tcp_port_serial_2'];
            $enabledDB[] = $row['enabled'];
            $tunnel_openDB[] = $row['tunnel_open'];
            $daemon_startedDB[] = $row['started'];
            $updatedDB[] = $row['updated'];
            $pidDB[] = $row['pid'];
            $update_timeDB[] = $row['update_time'];
        }
    }
}

else {

  //echo "yes id";
    $result = $conn->query("SELECT *  FROM brainboxes WHERE brainbox_id = '$brainbox_id'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $target_interface=  $row['target_interface'];
            $nickname=  $row['nickname'];
            $model_number=  $row['model_number'];
            $apparent_part  =  $row['apparent_part'];
            $IP_Address  =  $row['IP_Address'];
            $mac_address  =  $row['mac_address'];
            $baud  =  $row['baud'];
            $tcp_port  =  $row['tcp_port'];
            $tcp_port_serial_1  =  $row['tcp_port_serial_1'];
            $tcp_port_serial_2  =  $row['tcp_port_serial_2'];
        }
    }

}
   // $conn->close();



?>
