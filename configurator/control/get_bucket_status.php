<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 12:05 PM
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$getHTMLData = $_REQUEST['data'];

$result = $conn->query("select * from energy_bucket_config");


while($row = $result->fetch_assoc()) {
    $reinitialize_bucket = $row['reinitialize_bucket'];
}

if($getHTMLData == 'yes') {
    if($reinitialize_bucket  == '1'){
        echo 'Reinitialize pending: True';
    }elseif($reinitialize_bucket == '0'){
        echo 'Reinitialize pending: False';
    }else {
        echo "Not set";
    }
}