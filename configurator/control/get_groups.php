<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *ALTER TABLE groups ADD fallback INT;
ALTER TABLE groups ADD ntp_url_sg424 INT;
ALTER TABLE groups ADD forced_upgrade INT;
ALTER TABLE groups ADD irradiance INT;
ALTER TABLE groups ADD gateway_control INT;
ALTER TABLE groups ADD AIF_id INT;
 * */
include_once ('../functions/session.php');

$group_id = filter_var($group_id, FILTER_SANITIZE_STRING);

include_once ('../functions/mysql_connect.php');

if($group_id == '') {
   //echo "No id";
    $result = $conn->query("SELECT *  FROM groups");

    if(mysqli_num_rows($result)>0) {
        while($row = $result->fetch_assoc()) {
            $group_id[] =  $row['group_id'];
            $alias[] =  $row['alias'];
            $location[] =  $row['location'];
            $notes[] =  $row['notes'];
            $fallback[] =  $row['fallback'];
            $ntp_url_sg424[] =  $row['ntp_url_sg424'];
            $forced_upgrade[] =  $row['forced_upgrade'];
            $irradiance[] =  $row['irradiance'];
            $gateway_control[] =  $row['gateway_control'];
            $sleep[] =  $row['sleep'];
            $AIF_id[] =  $row['AIF_id'];
            $er_server_http[] =  $row['er_server_http'];
            $er_gtw_tlv[] =  $row['er_gtw_tlv'];
        }
    }
}else {
   //echo "yes id: $group_id";

    $result = $conn->query("SELECT * FROM groups WHERE group_id = $group_id");

    if(mysqli_num_rows($result)>0) {
        while($row = $result->fetch_assoc()) {
            $group_idDB =  $row['group_id'];
            $aliasDB =  $row['alias'];
            $locationDB =  $row['location'];
            $notesDB =  $row['notes'];
            $fallbackDB =  $row['fallback'];
            $ntp_url_sg424DB =  $row['ntp_url_sg424'];
            $forced_upgradeDB =  $row['forced_upgrade'];
            $irradianceDB =  $row['irradiance'];
            $gateway_controlDB =  $row['gateway_control'];
            $sleepDB =  $row['sleep'];
            $AIF_idDB =  $row['AIF_id'];
            $er_server_httpDB =  $row['er_server_http'];
            $er_gtw_tlvDB =  $row['er_gtw_tlv'];
        }
    }

}

$resultGroups = $conn->query("select count(*) AS group_count, group_id AS group_ids ,(SELECT alias FROM groups WHERE  inverters.group_id = groups.group_id) as group_name from inverters group by group_id;");

if(mysqli_num_rows($resultGroups)>0) {
    while ($row = $resultGroups->fetch_assoc()) {
        $group_count[] = $row['group_count'];
        if($row['group_ids'] == '') {
            $group_ids[] = 'N/A';
        }else {
            $group_ids[] =$row['group_ids'];
        }

        if($row['group_name'] == '') {
            $group_name[] = 'N/A';
        }else {
            $group_name[] =$row['group_name'];
        }

    }

}

?>