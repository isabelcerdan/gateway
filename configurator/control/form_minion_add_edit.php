<?php
include_once ('../functions/mysql_connect.php');

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');


//print_r($target_interfaceDB[]);
//echo $target_interfaceDB[0];


$error = '';
$minion_auditor_id= mysqli_real_escape_string($conn, $_REQUEST['minion_auditor_id']);
 $minion_auditor_id = filter_var($minion_auditor_id, FILTER_SANITIZE_STRING);

$suspended = mysqli_real_escape_string($conn, $_REQUEST['suspended']);
$suspended = filter_var($suspended, FILTER_SANITIZE_STRING);

$frequency = mysqli_real_escape_string($conn, $_REQUEST['frequency']);
$frequency = filter_var($frequency, FILTER_SANITIZE_STRING);

$time_value = mysqli_real_escape_string($conn, $_POST['time_value']);
$time_value = filter_var($time_value, FILTER_SANITIZE_STRING);

$email = mysqli_real_escape_string($conn, $_POST['email']);
$email = filter_var($email, FILTER_SANITIZE_STRING);

// split email
$emailArray = preg_split("/[\s,]+/", $email);

//Clean out non-email characters
for($i = 0; $i < count($emailArray); $i++ ) {
    $emailArray[$i] = filter_var( $emailArray[$i], FILTER_SANITIZE_EMAIL);
}

// Split it cleanly
$emailData = implode("," , $emailArray);

$run_now = mysqli_real_escape_string($conn, $_POST['run_now']);
$run_now = filter_var($run_now, FILTER_SANITIZE_STRING);


// basic error checking //

/*
if($target_interface == '') {
      $error = $error . " target_interface";
}

if($nickname == '') {
     $error = $error . " nickname";
}
if($IP_Address == '') {
     $error = $error . " IP_Address";
}

if($mac_address == '') {
      $error = $error . " mac_address";
}
if($baud == '') {
     $error = $error . " baud";
}
if( ($tcp_port == '') || (tcp_port_serial_1 == '') || (tcp_port_serial_2 == '') ){
      $error = $error . " tcp_port";
}

if ($error != '') {
   // echo "FAILURE!!! ";
    header('Location: /brainboxes-add-edit.php?brainboxes=' .$target_interface .'&dual_status=' . $dual_status . '&errors=' . $error .  " problem");
    exit();
}
*/

$sql = "UPDATE minion_auditor set frequency = '$frequency',time_value = '$time_value',email = '$emailData',run_now = '$run_now'  WHERE id = '$minion_auditor_id' ";


if (!mysqli_query($conn,$sql)) {
    header('Location: /minion_auditor-add-edit.php?minion_auditor_id=' . $minion_auditor_id);
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    //if($conn->insert_id != 0){
    //    $gateway_id = $conn->insert_id;
    //}

    header('Location: /auditor.php?update=success&minion_auditor_id=' . $minion_auditor_id);

}

?>