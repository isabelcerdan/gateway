<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */

if($power_regulator_id_form != '') {
    $result = $conn->query("SELECT mf.power_regulator_id, mf.meter_role, mf.feed_name, mf.threshold, mf.target_offset, 
pr.id, pr.battery_strategy ,pr.feed_name  AS pr_feed_name, pr.has_solar, pr.name, pr.enabled, pr.bucket_size,pr.bucket_period, pr.bucket_enabled, pr.solar_curtailment_window
FROM meter_feeds mf join power_regulators pr ON pr.id = mf.power_regulator_id WHERE power_regulator_id= '$power_regulator_id_form' order by mf.meter_role, mf.feed_name");

        while($row = $result->fetch_assoc()) {
           $power_regulator_idDB[] =  $row['power_regulator_id'];
           $meter_roledDB[] =  $row['meter_role'];
           $feed_nameDB[] =  $row['feed_name'];
           $thresholdDB[] =  $row['threshold'];
           $target_offsetDB[] =  $row['target_offset'];
            $idDB[] =  $row['id'];
            $pr_feed_nameDB[] =  $row['pr_feed_name'];
            $has_solarDB[] = $row['has_solar'];
            $nameDB[] = $row['name'];
            $enabledDB[] = $row['enabled'];
            $bucket_sizeDB[] = $row['bucket_size'];
            $bucket_periodDB[] =$row['bucket_period'];
            $bucket_enabledDB[] =$row['bucket_enabled'];
            $solar_curtailment_windowDB[] =$row['solar_curtailment_window'];
            $battery_strategy_DB[] = $row['battery_strategy'];
        }
}

// add meter feeds to meters
foreach ($meters as $role => $meter) {
    $result = $conn->query("SELECT * FROM meter_feeds WHERE meter_role = '" . $meter['meter_role'] . "'");
    $meters[$role]['meter_feeds'] = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = $result->fetch_assoc()) {
            $meters[$role]['meter_feeds'][$row['feed_name']] = $row;
        }
    }
}

// add pcc combo to $meters if pcc2 exists
if (array_key_exists('pcc2', $meters)) {
    $result = $conn->query("SELECT *,em.delta FROM meter_feeds mf LEFT JOIN energy_meter em ON em.meter_role = mf.meter_role WHERE mf.meter_role = 'pcc_combo'");
    $meters['pcc_combo'] = array();
    if (mysqli_num_rows($result) > 0) {
        $meters['pcc_combo']['meter_feeds'] = array();
        while ($row = $result->fetch_assoc()) {
            $meters['pcc_combo']['meter_feeds'][$row['feed_name']] = $row;
        }
    }
    $meters['pcc_combo']['delta'] = ($meters['pcc']['delta'] || $meters['pcc2']['delta']) ? "1" : "0";
}

$meter_order = array('pcc', 'pcc2', 'pcc_combo');
$feed_names = array('A', 'B', 'C', 'Delta');

// reorder by feed for table rendering
$meter_feeds = array();
foreach ($feed_names as $feed_name) {
    $meter_feeds[$feed_name] = array();
    foreach ($meter_order as $meter_name) {
        if (array_key_exists($meter_name, $meters)) {


                $meter_feed = $meters[$meter_name]['meter_feeds'][$feed_name];
                $meter_feed['delta'] = $meters[$meter_name]['delta'] ? $meters[$meter_name]['delta'] : 0;
                $meter_feed['threshold'] = $meter_feed['threshold'] ? number_format($meter_feed['threshold'], 3, '.', '') : null;
                if ($gateway_perspective == "production" and $meter_feed['threshold'])
                    $meter_feed['threshold'] = -$meter_feed['threshold'];
                array_push($meter_feeds[$feed_name], $meter_feed);


        }
    }
}

?>