<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
    $stringId = mysqli_real_escape_string($conn, $_POST['stringId']);
   $stringId = filter_var($stringId, FILTER_SANITIZE_STRING);

    $string_name = mysqli_real_escape_string($conn, $_POST['string_name']);
    $string_name = filter_var($string_name, FILTER_SANITIZE_STRING);

    $location = mysqli_real_escape_string($conn, $_POST['location']);
    $location = filter_var($location, FILTER_SANITIZE_STRING);

    $notes = mysqli_real_escape_string($conn, $_POST['notes']);
    $notes = filter_var($notes, FILTER_SANITIZE_STRING);

    $FEED_NAME = mysqli_real_escape_string($conn, $_POST['FEED_NAME']);
    $FEED_NAME = filter_var($FEED_NAME, FILTER_SANITIZE_STRING);

    $buildout = mysqli_real_escape_string($conn, $_POST['buildout']);
    $buildout = filter_var($buildout, FILTER_SANITIZE_STRING);


$sql = "INSERT INTO strings (stringId,string_name,location,notes,FEED_NAME,buildout)
  values('$stringId', '$string_name', '$location','$notes','$FEED_NAME','$buildout')
    ON DUPLICATE KEY UPDATE
    stringId = '$stringId', string_name = '$string_name', location = '$location', notes = '$notes', FEED_NAME = '$FEED_NAME', buildout = '$buildout'
";


if (!mysqli_query($conn,$sql)) {
    header('Location: /strings-add-edit.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
    die('<br>Error: ' . mysqli_error($conn));
}
else {

    header('Location: /success_string.php?stringId=' . $conn->insert_id);
   // mysqli_close($con);

}

/*

  ON DUPLICATE KEY UPDATE
  stringId = '$stringId,string_name = '$string_name',location = '$location',notes = '$notes',FEED_NAME = '$FEED_NAME',
  buildout = '$buildout'

if(($stringId =='') OR ($stringId == null)) {
$stmt = $conn->prepare("INSERT INTO strings (string_name,location,notes,FEED_NAME,buildout)  VALUES (?,?,?,?,?)");
//   $stmt = $conn->prepare("INSERT INTO energy_meter (meter_name,meter_role,modbusdevicenumber,meter_class,address,baud,network_comm_type,feed_of_phase_A,feed_of_phase_B,feed_of_phase_C)  VALUES (?,?,?,?,?,?,?,?,?,?)");
$stmt->bind_param("sssss", $string_name,$location,$notes,$FEED_NAME,$buildout);
    $stringId = $stmt->insert_id;
}else {

    $stmt = $conn->prepare("UPDATE  strings  set string_name=?,location=?,notes=?, FEED_NAME=?,buildout=? WHERE stringId=?");
    $stmt->bind_param("ssssss", $string_name,$location,$notes,$FEED_NAME,$buildout,$stringId );

}
$stmt->execute();
//echo "<br>ID: " . $id = $stmt->insert_id;

if($stringId != '') {
    $id = $stmt->insert_id;
}else {
    $id = $stringId;
}

if($stmt->affected_rows > 0) {

    header('Location: /success_string.php?stringId=' . $id);
    $stmt->close();
    $conn->close();
}
else {
    //printf("Error: %s.\n", mysqli_stmt_error($stmt));
    $stmt->close();
    $conn->close();
    header('Location: /strings-add-edit.php?errors=' . $error = $error .  " problem" );
}

*/


?>