<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 11/2/2017
 * Time: 1:00 PM
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$inverters_id = mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
$inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);

$powerFactor =  mysqli_real_escape_string($conn, $_GET['powerFactor']);

$powerFactor = filter_var($powerFactor, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

$group =  mysqli_real_escape_string($conn, $_GET['group']);
$group = filter_var($group, FILTER_SANITIZE_STRING);

$enabled =  mysqli_real_escape_string($conn, $_GET['enabled']);
$enabled = filter_var($enabled, FILTER_SANITIZE_NUMBER_INT);


//lagging
$lagging =  mysqli_real_escape_string($conn, $_GET['lagging']);
$lagging = filter_var($lagging, FILTER_SANITIZE_NUMBER_INT);

$adaptive =  mysqli_real_escape_string($conn, $_GET['adaptive']);
$adaptive = filter_var($adaptive, FILTER_SANITIZE_STRING);



if($inverters_id != '') {
 //echo "inverters";
    $inverters = explode(",",$inverters_id);
    foreach($inverters as $key => $inverter) {
        $inverterArray[$key] = "'$inverter'";
    }
    $inverterArray = join(', ',  $inverterArray);

    $result = $conn->query("SELECT mac_address,serialNumber,IP_Address  FROM inverters WHERE inverters_id IN ($inverterArray)");

    if(mysqli_num_rows($result)>0) {

        while ($row = $result->fetch_assoc()) {
            $IP_Address[] =  $row['IP_Address'];
        }
    }

}
if($adaptive == 'Yes' ) {

    $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" 239.192.0.0');

    $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled 239.192.0.0"); // pf_ena 1
}
elseif($group != '' ) {

    if($group == 'All' ) {
        //Make sure that phase angle is set to zero (0)
        $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" 239.192.0.0');

        //Set Power Factor by Feed //
        $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled 239.192.0.0");
    }elseif ($group == 'A' ) {
        $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" 239.192.1.1');

        //Set Power Factor by Feed //
        $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled  239.192.1.1");
    }
    elseif ($group == 'B' ) {
        $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" 239.192.1.2');

        //Set Power Factor by Feed //
        $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled 239.192.1.2");
    }
    elseif ($group == 'C' ) {
        $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" 239.192.1.4');

        //Set Power Factor by Feed //
        $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled 239.192.1.4");
    }else {
        $resultsPowerFactor[] = "no changes at this time";
    }


}elseif(!empty($IP_Address)) {

    for ($i = 0;$i< count($IP_Address); $i++ ){
        //Make sure that phase angle is set to zero (0)
       $resultsPhaseAngle =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py cli "ph 0" ' . $IP_Address[$i]);

        $resultsPowerFactor[] =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py set_aif_immed PWR_FACTOR $powerFactor PWR_FACTOR_LEADING $lagging PWR_FACTOR_ENABLE $enabled $IP_Address[$i]");
    }
}else {
    $resultsPowerFactor[] =  "no changes at this time";
}



?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="../css/style.css" rel="stylesheet">
<body>


<div class="container">
    <h2>Power Factor - Updating</h2>


    <div class="panel panel-default">

        <div class="panel-body">

            <h3>Results
            </h3>
            <p>
                <?php
                for($i =0;$i < count($resultsPowerFactor);$i++) {
                    echo "<p>" . filter_var($resultsPowerFactor[$i], FILTER_SANITIZE_STRING) . "<p>" ;
                }
                ?>


            </p>

        </div>
    </div>
</div>


</body>
</html>
