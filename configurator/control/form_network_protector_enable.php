<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

include_once('../functions/session.php');
include_once('../functions/mysql_connect.php');

$error = '';

$network_protector_enabled = mysqli_real_escape_string($conn, $_POST['network_protector_enabled']);
$network_protector_enabled = filter_var($network_protector_enabled, FILTER_SANITIZE_NUMBER_INT);

if (($network_protector_enabled == 1) OR ($network_protector_enabled == 0)) {

    $sql = "UPDATE daemon_control SET enabled  = '$network_protector_enabled' WHERE name = 'icpcond'";
    if (!mysqli_query($conn, $sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }

    $sql2 = "UPDATE daemon_control SET enabled  = '$network_protector_enabled' WHERE name = 'nplogd'";
    if (!mysqli_query($conn, $sql2)) {
        die('<br>Error: ' . mysqli_error($conn));
    }

    $sql3 = "UPDATE daemon_control SET enabled  = '$network_protector_enabled' WHERE name = 'notifyd'";
    if (!mysqli_query($conn, $sql3)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    // clear protection_alert table if disabled //
    if ($network_protector_enabled == 0) {
        // delete alart
        $sql4 = "DELETE FROM protection_alert;";
        if (!mysqli_query($conn, $sql4)) {
            die('<br>Error: ' . mysqli_error($conn));
        }
    }
    // mysqli_close($con);
    header('Location: /auto_detect_st_fe.php?net=success');
} else {
    header('Location: /auto_detect_st_fe.php?net=fail');
}


?>