<?php




/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */


// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
$meter_id = mysqli_real_escape_string($conn, $_REQUEST['meter_id']);
$meter_id = filter_var($meter_id, FILTER_SANITIZE_STRING);

$delete_meter = mysqli_real_escape_string($conn, $_REQUEST['delete_meter']);
$delete_meter = filter_var($delete_meter, FILTER_SANITIZE_STRING);

// Delete before ///
if(($delete_meter == 'yes') AND ($meter_id != '')){
    // sql to delete a record
    $sql = "SELECT meter_role FROM energy_meter WHERE meter_id=$meter_id";
    $result = mysqli_query($conn,$sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_row($result);
        $meter_role = $row[0];
    } else {
        header('Location: /meter.php?error=0');
        die('<br>Error: ' . mysqli_error($conn));
    }

    $sql = "DELETE FROM energy_meter WHERE meter_id=$meter_id";
    if (!mysqli_query($conn,$sql)) {
        header('Location: /meter.php?error=0');
        die('<br>Error: ' . mysqli_error($conn));
    }

    $sql = "DELETE FROM meter_feeds WHERE meter_role=$meter_role";
    if ($meter_role == 'pcc2') {
        $sql .= " OR meter_role='pcc_combo'";
    }
    if (!mysqli_query($conn,$sql)) {
        header('Location: /meter.php?error=0');
        die('<br>Error: ' . mysqli_error($conn));
    }

    header('Location: /meter.php');
    die('exit');

}
$meter_type  = mysqli_real_escape_string($conn, $_POST['meter_type']);
$meter_type= filter_var($meter_type, FILTER_SANITIZE_STRING);
//$meter_name = mysqli_real_escape_string($conn, $_POST['meter_name']);
//$meter_name = filter_var($meter_name, FILTER_SANITIZE_STRING);

$meter_role  = mysqli_real_escape_string($conn, $_POST['meter_role']);
$meter_role = filter_var($meter_role, FILTER_SANITIZE_STRING);


//Give meter name to same name as Role. We may be able to delete this field, but the code should be audited first //
$meter_name = $meter_role;


// if a non drop down item exists //
if($meter_role =='Add-New') {
    $meter_role  = mysqli_real_escape_string($conn, $_POST['meter_role_manual']);
    $meter_role = filter_var($meter_role, FILTER_SANITIZE_STRING);

}

//$meter_class  = mysqli_real_escape_string($conn, $_POST['meter_class']);
//$meter_class = filter_var($meter_class, FILTER_SANITIZE_EMAIL);
$meter_class = 48;

// if a non drop down item exists //
//if($meter_class =='Add-New') {
//    $meter_class  = mysqli_real_escape_string($conn, $_POST['meter_class_manual']);
//    $meter_class = filter_var($meter_class, FILTER_SANITIZE_STRING);
//}

$modbusdevicenumber = mysqli_real_escape_string($conn, $_POST['modbusdevicenumber']);
$modbusdevicenumber= filter_var($modbusdevicenumber, FILTER_SANITIZE_STRING);

$interfaceSelect  = mysqli_real_escape_string($conn, $_POST['interfaceSelect']);
$interfaceSelect = filter_var($interfaceSelect, FILTER_SANITIZE_STRING);

$interfaceManual  = mysqli_real_escape_string($conn, $_POST['interfaceManual']);
$interfaceManual = filter_var($interfaceManual, FILTER_SANITIZE_STRING);

$meter_delta = false;
if (array_key_exists('meter_delta', $_POST)) {
    $meter_delta = $_POST['meter_delta'] == "on";
}

$ct_orientation_reversed = false;
if (array_key_exists('ct_orientation_reversed', $_POST)) {
    $ct_orientation_reversed = $_POST['ct_orientation_reversed'] == "on";
}


// if user enters a manual interface instead of picking one from the dropdown  //
if($interfaceSelect == 'manual_interface'){
    $address =  $interfaceManual;
}else {
    $address =  $interfaceSelect;
}

$baud  = mysqli_real_escape_string($conn, $_POST['baud']);
$baud = filter_var($baud, FILTER_SANITIZE_STRING);

$network_comm_type   = mysqli_real_escape_string($conn, $_POST['network_comm_type']);
$network_comm_type = filter_var($network_comm_type, FILTER_SANITIZE_STRING);

$feed_of_phase_A   = mysqli_real_escape_string($conn, $_POST['feed_of_phase_A']);
$feed_of_phase_A = filter_var($feed_of_phase_A, FILTER_SANITIZE_STRING);

$feed_of_phase_B   = mysqli_real_escape_string($conn, $_POST['feed_of_phase_B']);
$feed_of_phase_B = filter_var($feed_of_phase_B, FILTER_SANITIZE_STRING);

$feed_of_phase_C   = mysqli_real_escape_string($conn, $_POST['feed_of_phase_C']);
$feed_of_phase_C = filter_var($feed_of_phase_C, FILTER_SANITIZE_STRING);

// basic error checking //
//if($meter_name == '') {
//    echo   $error = $error . " meter_name";
//}
if($meter_role == '') {
    echo  $error = $error . " meter_role";
}
if($meter_class == '') {
    echo  $error = $error . " meter_class";
}
if($address == '') {
    echo  $error = $error . " interface";
}
if ($error != '') {
    header('Location: /meter-add-edit.php?errors=' . $error .  " problem");
}


$sql = "INSERT INTO energy_meter (meter_id,meter_name,meter_role,modbusdevicenumber,meter_class,address,baud,network_comm_type,feed_of_phase_A,feed_of_phase_B,feed_of_phase_C,delta,ct_orientation_reversed,meter_type)
  values('$meter_id', '$meter_name','$meter_role','$modbusdevicenumber','$meter_class','$address', '$baud', '$network_comm_type','$feed_of_phase_A','$feed_of_phase_B','$feed_of_phase_C','$meter_delta','$ct_orientation_reversed','$meter_type')
 ON DUPLICATE KEY UPDATE
  meter_id ='$meter_id', meter_name='$meter_name',meter_role='$meter_role',modbusdevicenumber='$modbusdevicenumber',meter_class='$meter_class',address='$address',baud= '$baud', network_comm_type='$network_comm_type',feed_of_phase_A='$feed_of_phase_A',feed_of_phase_B='$feed_of_phase_B',feed_of_phase_C='$feed_of_phase_C',delta='$meter_delta',ct_orientation_reversed='$ct_orientation_reversed',meter_type='$meter_type'
  
";
if (!mysqli_query($conn,$sql)) {
    //header('Location: /meter-add-edit.php?errors=' . $error .  " problem");
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if ($conn->insert_id != 0) {
        $meter_id= $conn->insert_id;
    }
}
header('Location: /success_meter.php?meter_id=' .$meter_id);
/*
if($meter_id =='') {
// prepare and bind
//$stmt = $conn->prepare("INSERT INTO energy_meter (meter_name,meter_role,meter_class,address,baud,network_comm_type,feed_of_phase_A,feed_of_phase_B,feed_of_phase_C)  VALUES (?,?,?,?,?,?,?,?,?,?)");
    $stmt = $conn->prepare("INSERT INTO energy_meter (meter_name,meter_role,modbusdevicenumber,meter_class,address,baud,network_comm_type,feed_of_phase_A,feed_of_phase_B,feed_of_phase_C,delta,ct_orientation_reversed)  VALUES (?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param("sssisissssiis", $meter_name,$meter_role,$modbusdevicenumber,$meter_class,$address, $baud, $network_comm_type,$feed_of_phase_A,$feed_of_phase_B,$feed_of_phase_C,$meter_delta,$ct_orientation_reversed);
}else{
//echo "Meter ID $meter_id";

  $stmt = $conn->prepare("UPDATE energy_meter set meter_name=?,meter_role=?,modbusdevicenumber=?, meter_class=?,address=?,baud=?,network_comm_type=?,feed_of_phase_A=?,feed_of_phase_B=?,feed_of_phase_C=?,delta=?,ct_orientation_reversed=? WHERE meter_id=?");
   $stmt->bind_param("sssisissssiis", $meter_name,$meter_role,$modbusdevicenumber,$meter_class,$address, $baud, $network_comm_type,$feed_of_phase_A,$feed_of_phase_B,$feed_of_phase_C, $meter_delta, $ct_orientation_reversed, $meter_id);

  if ($stmt->errno) {
      // echo "FAILURE!!! " . $stmt->error;
    header('Location: /meter-add-edit.php?errors=' .  $error .  " problem " . $stmt->error);
  }
   else echo "Updated {$stmt->affected_rows} rows";
}

$stmt->execute();
$id = $stmt->insert_id;
if (($id == 0) || ($id == '')) {
    $id =  $meter_id;
}

if($stmt->affected_rows > 0) {
    $stmt->close();
    $conn->close();
    header('Location: /success_meter.php?meter_id=' .$id);
}
else {
   printf("Error: %s.\n", mysqli_stmt_error($stmt) . mysqli_error($conn));
   // exit();
    $stmt->close();
    //$conn->close();
    header('Location: /meter-add-edit.php?errors=' . $error = $error .  " no_affected_rows " .  mysqli_error($conn));
}

*/
?>