<?php
include_once ('../functions/mysql_connect.php');

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

$error = '';
$run_mode= mysqli_real_escape_string($conn, $_REQUEST['run_mode']);
$run_mode = filter_var($run_mode, FILTER_SANITIZE_STRING);

$enabled = mysqli_real_escape_string($conn, $_REQUEST['enabled']);
$enabled = filter_var($enabled, FILTER_SANITIZE_STRING);

$inter_loop_delay = mysqli_real_escape_string($conn, $_REQUEST['inter_loop_delay']);
$inter_loop_delay = filter_var($inter_loop_delay, FILTER_SANITIZE_STRING);

$inter_query_delay = mysqli_real_escape_string($conn, $_REQUEST['inter_query_delay']);
$inter_query_delay = filter_var($inter_query_delay, FILTER_SANITIZE_STRING);

$sql = "UPDATE psa_control set run_mode = '$run_mode', inter_loop_delay='$inter_loop_delay', inter_query_delay = '$inter_query_delay' ";


if (!mysqli_query($conn,$sql)) {
    header('Location: /psa-edit.php?status=error');
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    header('Location: /psa-edit.php?status=success');
}

?>