<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/25/2017
 * Time: 12:31 PM
 */

include_once ('../functions/session.php');

// Access restriction
//include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');
$startStop=  mysqli_real_escape_string($conn, $_GET['startStop']);
$startStop = filter_var($startStop, FILTER_VALIDATE_INT);

$activePower = mysqli_real_escape_string($conn, $_REQUEST['activePower']);
$activePower = filter_var($activePower, FILTER_SANITIZE_STRING);

$reactivePower = mysqli_real_escape_string($conn, $_REQUEST['reactivePower']);
$reactivePower = filter_var($reactivePower, FILTER_SANITIZE_STRING);

$powerRiseRime = mysqli_real_escape_string($conn, $_REQUEST['powerRiseRime']);
$powerRiseRime = filter_var($powerRiseRime, FILTER_SANITIZE_STRING);

$powerDecreaseTime = mysqli_real_escape_string($conn, $_REQUEST['powerDecreaseTime']);
$powerDecreaseTime= filter_var($powerDecreaseTime, FILTER_SANITIZE_STRING);

//
$level = mysqli_real_escape_string($conn, $_REQUEST['level']);
$level  = filter_var($level , FILTER_SANITIZE_STRING);

$command =='';

if(is_int($startStop) ) {
    if ($startStop == 1){
        $command = 'run';
        echo $command;
    }
    if ($startStop ==0){
        $command = 'stop';
        echo $command;
    }
}
elseif($activePower =='yes' ) {
    if (is_numeric($level)) {
        $command = "active-power $level";
        echo $command;
    }
    else {
        echo "Not numeric";
    }
}
elseif($reactivePower =='yes' ) {
        if (is_numeric ($level)) {
            $command = "reactive-power $level";
            echo $command;
        }else {
            //echo "Not numeric";
        }
}elseif($powerRiseRime =='yes' ) {
    if (is_numeric($level)) {
        $command = "power-rise-time $level";
        echo $command;
    }
    else {
        echo "Not numeric";
    }
}elseif($powerDecreaseTime =='yes' ) {
    if (is_numeric($level)) {
        $command = "power-decrease-time $level";
        echo $command;
    }
    else {
        echo "Not numeric";
    }
}else {
    echo "exit";
    exit();
}

$results =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/EssCLI.py --".$command,$outcome3, $status3);


