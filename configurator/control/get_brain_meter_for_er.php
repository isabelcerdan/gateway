<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/20/2018
 * Time: 12:43 PM
 */
include_once ('../functions/mysql_connect.php');
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

$result = $conn->query("SELECT 
brainboxes.brainbox_id, brainboxes.nickname AS B_nickname,  brainboxes.IP_Address AS B_IP_Address,   brainboxes.target_interface AS B_target_interface, brainboxes.ER_Connect AS B_ER_Connect,

energy_meter.meter_id AS meter_id, energy_meter.meter_name AS M_meter_name ,  energy_meter.address AS M_target_interface, 
energy_meter.meter_class AS M_meter_class, energy_meter.modbusdevicenumber AS M_modbusdevicenumber, energy_meter.ER_Connect AS M_ER_Connect

FROM brainboxes
LEFT OUTER JOIN energy_meter
ON energy_meter.address=brainboxes.target_interface order by brainbox_id");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $brainbox_id[] = $row['brainbox_id'];
        $B_nickname[] = $row['B_nickname'];
        $B_IP_Address[] = $row['B_IP_Address'];
        $B_target_interface[] = $row['B_target_interface'];
        $meter_id[] = $row['meter_id'];
        $M_meter_name[] = $row['M_meter_name'];
        $M_target_interface[] = $row['M_target_interface'];
        $M_meter_class[] = $row['M_meter_class'];
        $M_modbusdevicenumber[] = $row['M_modbusdevicenumber'];
        $B_ER_Connect[] = $row['B_ER_Connect'];
        $M_ER_Connect[] = $row['M_ER_Connect'];
    }
}