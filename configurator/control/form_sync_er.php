<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';

    $URL_AQ = mysqli_real_escape_string($conn, $_POST['URL_AQ']);
    $URL_AQ = filter_var($URL_AQ, FILTER_SANITIZE_URL);

    $URL_AQ_Meter = mysqli_real_escape_string($conn, $_POST['URL_AQ_Meter']);
    $URL_AQ_Meter = filter_var($URL_AQ_Meter, FILTER_SANITIZE_URL);

    $transmit_frequency = mysqli_real_escape_string($conn, $_POST['transmit_frequency']);
    $transmit_frequency = filter_var($transmit_frequency, FILTER_VALIDATE_INT);

    $timeout = mysqli_real_escape_string($conn, $_POST['timeout']);
    $timeout = filter_var($timeout, FILTER_VALIDATE_INT);

    $retries = mysqli_real_escape_string($conn, $_POST['retries']);
    $retries = filter_var($retries, FILTER_VALIDATE_INT);

    $enabled = mysqli_real_escape_string($conn, $_POST['enabled']);
    $enabled = filter_var($enabled, FILTER_VALIDATE_INT);

    $inverter_er_rate = mysqli_real_escape_string($conn, $_POST['inverter_er_rate']);
    $inverter_er_rate = filter_var($inverter_er_rate, FILTER_VALIDATE_INT);

    $inverter_er_url = mysqli_real_escape_string($conn, $_POST['inverter_er_url']);
    $inverter_er_url = filter_var($inverter_er_url, FILTER_SANITIZE_URL);

    $inverter_er_port = mysqli_real_escape_string($conn, $_POST['inverter_er_port']);
    $inverter_er_port = filter_var($inverter_er_port, FILTER_VALIDATE_INT);

    $alarms_trans_frequency = mysqli_real_escape_string($conn, $_POST['alarms_trans_frequency']);
    $alarms_trans_frequency = filter_var($alarms_trans_frequency, FILTER_VALIDATE_INT);


    $reports_per_batch = mysqli_real_escape_string($conn, $_POST['reports_per_batch']);
    $reports_per_batch = filter_var($reports_per_batch, FILTER_VALIDATE_INT);

    $erptd= mysqli_real_escape_string($conn, $_POST['erptd']);
    $erptd= filter_var($erptd, FILTER_VALIDATE_INT);


    $reportToERArray =$_POST['reportToER'];
    foreach ($reportToERArray as $toER) {
        //$toER = mysqli_real_escape_string($conn, $toER);
        //$toER = filter_var($toER, FILTER_VALIDATE_INT);
        $stringSplit = explode("-", $toER);
        updateERGroups($conn,filter_var($stringSplit[0], FILTER_VALIDATE_INT),filter_var($stringSplit[1], FILTER_VALIDATE_INT));
    }

    $reportToGATArray =$_POST['reportToGAT'];
    foreach ($reportToGATArray as $toGAT) {
        //$toER = mysqli_real_escape_string($conn, $toER);
        //$toER = filter_var($toER, FILTER_VALIDATE_INT);
        $stringSplit = explode("-", $toGAT);
        updateGATGroups($conn,filter_var($stringSplit[0], FILTER_VALIDATE_INT),filter_var($stringSplit[1], FILTER_VALIDATE_INT));
    }

    function updateERGroups($conn,$group_id,$er_server_http ) {
       // echo $group_id;
        $sql = " UPDATE groups SET er_server_http='$er_server_http' WHERE group_id = '$group_id' ";

        if (!mysqli_query($conn,$sql)) {
            //pass
        }
        $sql = "UPDATE inverters SET group_audit_required = 1 WHERE group_id ='$group_id'";
        if (!mysqli_query($conn,$sql)) {
            //pass
        }else {
            //echo "updated";
        }

    }

    function updateGATGroups($conn,$group_id, $er_gtw_tlv ) {
        $sql = " UPDATE groups SET er_gtw_tlv='$er_gtw_tlv' WHERE group_id = '$group_id' ";

        if (!mysqli_query($conn,$sql)) {
            //pass
        }
        $sql = "UPDATE inverters SET group_audit_required = 1 WHERE group_id ='$group_id'";
        if (!mysqli_query($conn,$sql)) {
//pass
        }
    }

    // set enable to zero if not equal to 1
    if($enabled !=1) {
        $enabled = 0;
    }

    if($URL_AQ == '') {
        $error = $error . " URL";
    }

$result = $conn->query("SELECT * FROM energy_report_config");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $URL_AQDB = $row['URL_AQ'];
    }
}

// Start Daemon //

    if($erptd  !=1) {
        $erptd = 0;
    }
    $sql = "UPDATE daemon_control SET enabled = '$erptd' where name = 'erptd'";
    if (!mysqli_query($conn,$sql)) {
       // die('<br>Error: ' . mysqli_error($conn));
    }



if(!$URL_AQDB ) {
        $sql = "INSERT INTO energy_report_config (URL_AQ,transmit_frequency,timeout,retries,enabled,URL_AQ_Meter,inverter_er_rate,inverter_er_url,inverter_er_port,alarms_trans_frequency,reports_per_batch)
      values('$URL_AQ','$transmit_frequency','$timeout','$retries','$enabled','$URL_AQ_Meter', '$inverter_er_rate', '$inverter_er_url','$inverter_er_port','$alarms_trans_frequency','$reports_per_batch')
    
    ";
}else {
    $sql = "UPDATE energy_report_config SET URL_AQ = '$URL_AQ', transmit_frequency = '$transmit_frequency', timeout = '$timeout',
retries = '$retries',enabled = '$enabled', URL_AQ_Meter = '$URL_AQ_Meter', inverter_er_rate = '$inverter_er_rate',inverter_er_url = '$inverter_er_url', inverter_er_port = '$inverter_er_port',alarms_trans_frequency='$alarms_trans_frequency',
reports_per_batch = '$reports_per_batch'
    
    ";

    
}

if (!mysqli_query($conn,$sql)) {
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if($conn->insert_id != 0){
        $gateway_id = $conn->insert_id;
    }

      //mysqli_close($con);
    header('Location: /success_aq_sync.php');

}






?>