<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 10/30/2017
 * Time: 2:22 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');


include_once ('../functions/mysql_connect.php');

$error = '';

$run_db_const_auditor= mysqli_real_escape_string($conn, $_GET['run_db_const_auditor']);
$run_db_const_auditor = filter_var($run_db_const_auditor, FILTER_SANITIZE_STRING);

if($run_db_const_auditor == 'YES'){

    $sql = "UPDATE auditor_control set operating = 'YES' ";

    if (!mysqli_query($conn,$sql)) {
        //die('<br>Error: ' . mysqli_error($conn));
       // header('Location: /inverters_list.php?auditor=fail');
    }


    $sql_minion = "update minion_auditor set  run_now = 'YES' WHERE title = 'db_and_inverter_consistency'";

    if (!mysqli_query($conn,$sql_minion)) {
        //die('<br>Error: ' . mysqli_error($conn));
        header('Location: /inverters.php?auditor=fail');
    }
    $results =  exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py  status ");
    header('Location: /update_db_con_audit.php');

}else {
    header('Location: /inverters.php?auditor=fail');
}








?>