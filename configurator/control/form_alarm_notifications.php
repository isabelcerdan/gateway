<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$subscriptions_id= mysqli_real_escape_string($conn, $_REQUEST['subscriptions_id']);
$subscriptions_id = filter_var($subscriptions_id, FILTER_SANITIZE_STRING);

$subscriber_id= mysqli_real_escape_string($conn, $_REQUEST['subscriber_id']);
$subscriber_id = filter_var($subscriber_id, FILTER_SANITIZE_STRING);

$alarm_id = mysqli_real_escape_string($conn, $_REQUEST['alarm_id']);
$alarm_id = filter_var($alarm_id, FILTER_SANITIZE_STRING);

$enabled = mysqli_real_escape_string($conn, $_REQUEST['enabled']);
$enabled= filter_var($enabled, FILTER_SANITIZE_STRING);


// Delete before ///
if(($delete_subscriptions == 'yes') AND ($subscriptions_id != '')){

        // sql to delete a record
        $sql = "DELETE FROM alarm_subscriptions WHERE id='$subscriptions_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: /alarm_notifications.php');
            die('exit');
        }
        else {
            header('Location: /alarm_notifications.php?error=1');
            die('<br>Error: ' . mysqli_error($conn));
        }
}


//remove id field if we change table to AUTO_INCREMENT
if( $subscriptions_id== '') {
    $sql = "insert into alarm_subscriptions (subscriber_id,alarm_id,enabled) VALUES('$subscriber_id','$alarm_id','$enabled')";
}
else {
    $sql = "UPDATE alarm_subscriptions set
subscriber_id = '$subscriber_id',alarm_id ='$alarm_id', enabled = '$enabled' where subscriptions_id = '$subscriptions_id'
";

}


if (!mysqli_query($conn,$sql)) {
    header('Location: /alarm_notifications.php?' . mysqli_error($conn));
  // die('<br>Error: ' . mysqli_error($conn));
}
else {
    if($conn->insert_id != 0){
        $battery_id = $conn->insert_id;
    }
   // mysqli_close($con);

    header('Location: /alarm_notifications.php?subscriptions_id=' . $battery_id );

}

?>