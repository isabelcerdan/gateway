<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
$update = false;
$emailInDB = false;

   $user_id = mysqli_real_escape_string($conn, $_REQUEST['user_id']);
   $user_id  = filter_var($user_id , FILTER_SANITIZE_STRING);

    if(is_numeric($user_id) == true) {
        $update = true;
    }

    $delete_user = mysqli_real_escape_string($conn, $_REQUEST['delete_user']);
    $delete_user = filter_var($delete_user, FILTER_SANITIZE_STRING);

    if(($delete_user == 'yes') AND ($update == true)) {
        // sql to delete a record
        $sql = "DELETE FROM  users where user_id = '$user_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: /users.php?errors=user_deleted_success');
            die('exit');
        }
        else {
            header('Location: /users.php?errors=user_deleted_fail');
            die('<br>Error: ' . mysqli_error($conn));
        }

    }

    $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
    $first_name = filter_var($first_name, FILTER_SANITIZE_STRING);

    $last_name  = mysqli_real_escape_string($conn, $_POST['last_name']);
    $last_name = filter_var($last_name, FILTER_SANITIZE_STRING);

    $email  = mysqli_real_escape_string($conn, $_POST['email']);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $password = filter_var($password, FILTER_SANITIZE_STRING);

    $password_comf  = mysqli_real_escape_string($conn, $_POST['password_comf']);
    $password_comf = filter_var($password_comf, FILTER_SANITIZE_STRING);

    $role  = mysqli_real_escape_string($conn, $_POST['role']);
    $role = filter_var($role, FILTER_SANITIZE_STRING);

    $registration_code  = mysqli_real_escape_string($conn, $_POST['registration_code']);
    $registration_code = filter_var($registration_code, FILTER_SANITIZE_STRING);

    $privileges  = mysqli_real_escape_string($conn, $_POST['privileges']);
    $privileges = filter_var($privileges, FILTER_SANITIZE_STRING);

    $company   = mysqli_real_escape_string($conn, $_POST['company']);
    $company = filter_var($company, FILTER_SANITIZE_STRING);

    $phone   = mysqli_real_escape_string($conn, $_POST['phone']);
    $phone = filter_var($phone, FILTER_SANITIZE_STRING);

    if($first_name == '') {
        $error = $error . " first_name";
    }

    if($last_name  == '') {
    $error = $error . " last_name";
    }

    // confirm email is correct format
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = $error . " email";
    }

    if($registration_code != 'apparent4you') {
        $error = $error . " registration_code";
    }

    if($company == '') {
        $error = $error . " company";
    }
    if($role == '') {
        $error = $error . " role";
    }

    if($phone == '') {
        $error = $error . " phone";
    }

    if($privileges == '') {
        $error = $error . " privileges";
    }

    // See if email is already in DB
    $result = $conn->query("SELECT email  FROM users where email = '$email'");
    while($row = $result->fetch_assoc()) {
        $emailDB = $row['email'];

        if ($emailDB == $email ) {
            $emailInDB = true;
        }
    }

    // If email is already in DB and not an updated give error
    if (($emailInDB == true) AND ($update == false) ) {
            $error = $error . " already_in_DB " .  __LINE__;
        }

    // make sure this isn't an update
    if($update == false){
        if (($password != $password_comf) && ($password > 6)) {
            $error = $error . " passwordsDoNotmatch";
        }
        if ( $emailInDB == true){
            $error = $error . " already_in_DB "  .  __LINE__;
        }
    }else {
        // if the user is resetting a password make  sure it is formatted correctly
        if($password !='') {
            if (($password != $password_comf) && ($password > 6)) {
                $error = $error . " passwordsDoNotmatch";
            }
        }

    }

if ($error != ''){
    header('Location: /register.php?errors=' . $error);
    exit();
}
// check to see if this is an update or not
if($update == false) {

    //create password salt for new users
    $password_salt = randomString(4);

    $password_hash    = hash('sha256', $password . $password_salt);

// prepare and bind
    $stmt = $conn->prepare("INSERT INTO users (first_name,last_name,email,phone,company,password_hash,password_salt,role,privileges)  VALUES (?,?,?,?,?,?,?,?,?)");

    $stmt->bind_param("sssssssss", $first_name, $last_name, $email, $phone,$company, $password_hash,$password_salt,$role,$privileges);

$first_name_db = $first_name;
$last_name_db = $last_name;
$email_db = $email;
$phone_db = $phone;
$company_db = $company;
$password_hash_db =$password_hash;
$password_salt_db = $password_salt;
$role_db = $role;
$privileges_db = $privileges;

}else {
    echo "trying to update ";
    $result = $conn->query("SELECT password_salt,password_hash, email  FROM users where user_id = '$user_id'");
    while($row = $result->fetch_assoc()) {
        $password_salt = $row['password_salt'];
        $password_hashDB = $row['password_hash'];
        $emailUseridDB = $row['email'];
    }
    // if not password use old password//
    if($password==''){
        $password_hash = $password_hashDB;
    }else {
        //create new password//
        $password_hash    = hash('sha256', $password . $password_salt);
    }

    if( ($emailInDB == true) AND ($emailUseridDB != $email)) {
        $error = $error . " already_in_DB";
        header('Location: /register.php?errors=' . $error);
        exit();
    }
    //echo "$emailUseridDB  | $email";

    //exit();
    // prepare and bind
    $stmt = $conn->prepare("UPDATE users SET first_name=?,last_name=?,email=?,phone=?,company=?,role=?,privileges=?,password_hash=? WHERE user_id=?");

    $stmt->bind_param("ssssssssi", $first_name, $last_name,$email,$phone,$company,$role,$privileges,$password_hash, $user_id);
}

$stmt->execute();

$stmt->affected_rows;

$stmt->close();
// $conn->close();
header('Location: /thanks_for_registering.php');

// Get Random string //
function randomString($length = 6) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}


/*
 *
 * if($user_id  != '') {
    $stmt = $conn->prepare("UPDATE users SET first_name = ?,
   last_name = ?,
   email = ?,
   phone = ?,
   company = ?,
   password_hash = ?,
   WHERE filmID = ?");
    $stmt->bind_param('sssdii',
        $_POST['filmName'],
        $_POST['filmDescription'],
        $_POST['filmImage'],
        $_POST['filmPrice'],
        $_POST['filmReview'],
        $_POST['filmID']);
    $stmt->execute();
    $stmt->close();
}
$sql = "INSERT INTO users (first_name,last_name,email,phone,company,password_hash,password_salt) 
      values('$first_name','$last_name', '$email','$phone','$company','$password_hash','$password_salt')
 
  ";
// set parameters and execute
$first_name_db = $first_name;
$last_name_db = $last_name;
$email_db = $email;
$phone_db = $phone;
$company_db = $company;
$password_hash_db =$password_hash;
$password_salt_db = $password_salt;
$role_db = $role;
$privileges_db = $privileges;
if (!mysqli_query($conn,$stmt)) {
    die('Error: ' . mysqli_error($conn));
}
else {
    $stmt->close();
    $conn->close();
    header('Location: /thanks_for_registering.php');
}
echo "1 record added";

mysqli_close($con);
*/



//ON DUPLICATE KEY UPDATE name="A"
/*
 * $sql = ("EXPLAIN `gateway_config`");
while($row = mysql_fetch_assoc($sql)) {
    if ($row['Field'] == 'timestamp') {
        echo $row['Default'];
    }
}$sql = mysql_query("EXPLAIN `mytable`");
while($row = mysql_fetch_assoc($sql)) {
    if ($row['Field'] == 'timestamp') {
        echo $row['Default'];
    }
}

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if ($row['Field'] == 'SYSTEM_SIZE') {
            $SYSTEM_SIZE =  $row['Default'];
        }
        if ($row['Field'] == 'MINIMUM_IMPORT') {
            $MINIMUM_IMPORT =  $row['Default'];
        }
        if ($row['Field'] == 'CURTAIL_TARGET') {
            $CURTAIL_TARGET =  $row['Default'];
        }
    }
} else {
    echo "0 results";
}
*/
//$sql = "SELECT id, firstname, lastname FROM MyGuests";



?>