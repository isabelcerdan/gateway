<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction


include_once('../functions/session.php');
include_once('../functions/restrict_privilage_access.php');

include_once('../functions/mysql_connect.php');
include_once('get_system.php');

$power_regulator_id = mysqli_real_escape_string($conn, $_REQUEST['id']);
$power_regulator_id = filter_var($power_regulator_id, FILTER_SANITIZE_STRING);
if ($power_regulator_id == '') {
    $power_regulator_id = 'NULL';
}

$delete_feed = mysqli_real_escape_string($conn, $_REQUEST['delete_feed']);
$delete_feed = filter_var($delete_feed, FILTER_SANITIZE_STRING);

// Delete before ///
if (($delete_feed == 'yes') AND ($power_regulator_id != '')) {
    // sql to delete a record
    $sql = "DELETE FROM power_regulators WHERE id='$power_regulator_id'";
    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php?error=DB' . mysqli_error($conn));
        die('<br>Error: ' . mysqli_error($conn));
    }

    $sql = "DELETE FROM meter_feeds WHERE power_regulator_id = '$power_regulator_id'";
    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php?error=DB' . mysqli_error($conn));
        die('<br>Error: ' . mysqli_error($conn));
    }

    header('Location: /power_regulator.php');
    die('exit');
}
$power_regulator_name = mysqli_real_escape_string($conn, $_POST['power_regulator_name']);
$power_regulator_name = filter_var($power_regulator_name, FILTER_SANITIZE_STRING);
//echo "power_regulator_name $power_regulator_name<br> ";

$select_meter_role = mysqli_real_escape_string($conn, $_REQUEST['select_meter_role']);
$select_meter_role = filter_var($select_meter_role, FILTER_SANITIZE_STRING);
//echo "select_meter_role: $select_meter_role<br> ";

$select_feed_name = mysqli_real_escape_string($conn, $_REQUEST['select_feed_name']);
$select_feed_name= filter_var($select_feed_name, FILTER_SANITIZE_STRING);
//echo "select-feed-name: $select_feed_name<br> ";

$pcc_meter =mysqli_real_escape_string($conn, $_REQUEST['pcc_meter']);
$pcc_meter = filter_var($pcc_meter, FILTER_SANITIZE_STRING);

$pcc2_meter =mysqli_real_escape_string($conn, $_REQUEST['pcc2_meter']);
$pcc2_meter= filter_var($pcc2_meter, FILTER_SANITIZE_STRING);

$pcc_combo_meter =mysqli_real_escape_string($conn, $_REQUEST['pcc_combo_meter']);
$pcc_combo_meter= filter_var($pcc_combo_meter, FILTER_SANITIZE_STRING);

$meter_feeds = mysqli_real_escape_string($conn, $_POST['meter_feeds']);
$meter_feeds= filter_var($meter_feeds, FILTER_SANITIZE_STRING);

$meter_feed_targets = mysqli_real_escape_string($conn, $_POST['meter_feed_targets']);
$meter_feed_targets= filter_var($meter_feed_targets, FILTER_SANITIZE_STRING);

$has_solar = key_exists('has_solar', $_POST) ? 1 : 0;
$has_storage = key_exists('has_storage', $_POST) ? 1 : 0;

$step_size = mysqli_real_escape_string($conn, $_POST['step_size']);
$step_size= filter_var($step_size, FILTER_SANITIZE_STRING);

$battery_strategy= mysqli_real_escape_string($conn, $_POST['battery_strategy']);
$battery_strategy= filter_var($battery_strategy, FILTER_SANITIZE_STRING);



$updatcontrol = $_POST['updatecontrol'];
$charge_priority = $_POST['charge_priority'];
$discharge_priority = $_POST['discharge_priority'];


$sql = "INSERT INTO power_regulators (id, name, feed_name, has_solar, has_storage,step_size,battery_strategy) 
  VALUES($power_regulator_id, '$power_regulator_name', '$select_feed_name', '$has_solar', '$has_storage','$step_size', '$battery_strategy')
  ON DUPLICATE KEY UPDATE
    name = '$power_regulator_name', feed_name = '$select_feed_name', has_solar = '$has_solar', has_storage ='$has_storage',step_size='$step_size', battery_strategy='$battery_strategy'";

if (!mysqli_query($conn, $sql)) {
    header('Location: /power_regulator.php?error=no_feed');
    //die('Error: ' . mysqli_error($conn));

} else {
    if(($power_regulator_id == '') OR ($power_regulator_id == 0)){
        $power_regulator_id= $conn->insert_id;
    }
    //echo $power_regulator_id ;

    // mysqli_close($con);
    //header('Location: /power_regulator.php');
}


foreach($updatcontrol AS $battery_id ) {

    $battery_info = explode("-", $battery_id);

    if($battery_info[1] == 'add'){
        addPoweregulatorToEssUnitis($conn,$power_regulator_id,$battery_info[0]);
    }
    if($battery_info[1] == 'remove'){
        removePoweregulatorToEssUnitis($conn,$battery_info[0]);
    }
}

foreach($charge_priority AS $charge_info ) {
    $charge_info_array = explode("-", $charge_info);
    addChargePriorityToEssUnitis($conn,$charge_info_array[0],$charge_info_array[1]);
}
foreach($discharge_priority AS $discharge_info ) {
    $discharge_info_array = explode("-", $discharge_info);
    addDisChargePriorityToEssUnitis($conn,$discharge_info_array[0],$discharge_info_array[1]);
}

function addChargePriorityToEssUnitis($conn,$battery_id,$charge ){
    $charge = filter_var($charge, FILTER_SANITIZE_NUMBER_INT);
   $battery_id = filter_var($battery_id, FILTER_SANITIZE_NUMBER_INT);
    $sql = "c '$charge' where id = '$battery_id '";

    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php');
        //die('Error: ' . mysqli_error($conn));
    }
}
function addDisChargePriorityToEssUnitis($conn,$battery_id,$charge ){
    $charge = filter_var($charge, FILTER_SANITIZE_NUMBER_INT);
    $battery_id = filter_var($battery_id, FILTER_SANITIZE_NUMBER_INT);
    $sql = "UPDATE ess_units set discharge_priority = '$charge' where id = '$battery_id '";

    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php');
        //die('Error: ' . mysqli_error($conn));
    }
}
function addPoweregulatorToEssUnitis($conn,$power_regulator_id,$battery_id ){
    $battery_id  = filter_var($battery_id , FILTER_SANITIZE_NUMBER_INT);
    $sql = "UPDATE ess_units set power_regulator_id = '$power_regulator_id' where id = '$battery_id '";

    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php?error=no_feed');
        //die('Error: ' . mysqli_error($conn));

    }
}
function removePoweregulatorToEssUnitis($conn,$battery_id ){
    $battery_id  = filter_var($battery_id , FILTER_SANITIZE_NUMBER_INT);
    $sql = "UPDATE ess_units set power_regulator_id = '' where id = '$battery_id '";

    if (!mysqli_query($conn, $sql)) {
        header('Location: /power_regulator.php?error=no_feed');
        //die('Error: ' . mysqli_error($conn));
    }
}

/// A  OR ($select_feed_name =='B') OR ($select_feed_name =='C')
//if(($select_feed_name =='A')){
if ((strpos($select_feed_name, 'A') !== false) OR ($select_feed_name =='Delta')) {

   $targets_pcc_A= mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_A']);

    if((($pcc_meter == 'pcc') OR ( $select_meter_role == "pcc_combo")) AND ($targets_pcc_A != '')) {
        $targets_pcc_A = filter_var($targets_pcc_A, FILTER_SANITIZE_STRING);
        echo "targets_pcc_A: $targets_pcc_A<br> ";
        $limit_pcc_A= mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_A']);
        $limit_pcc_A= filter_var($limit_pcc_A, FILTER_SANITIZE_STRING);
        echo "limit_pcc_A: $limit_pcc_A<br> ";
        addToPowerRegulator($conn,$targets_pcc_A,$limit_pcc_A,$power_regulator_id,'pcc','A');
    }

    $targets_pcc2_A = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc2_A']);
    if((($pcc2_meter == 'pcc2') OR ( $select_meter_role == "pcc_combo")) AND ($targets_pcc2_A != '')) {
        $targets_pcc2_A = filter_var($targets_pcc2_A, FILTER_SANITIZE_STRING);
        echo "targets_pcc2_A: $targets_pcc2_A<br> ";
        $limit_pcc2_A = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc2_A']);
        $limit_pcc2_A = filter_var($limit_pcc2_A, FILTER_SANITIZE_STRING);
        echo "limit_pcc2_A: $limit_pcc2_A<br> ";
        addToPowerRegulator($conn,$targets_pcc2_A,$limit_pcc2_A,$power_regulator_id,'pcc2','A');

    }

    $targets_pcc_combo_A = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_combo_A']);
    if(( $pcc_combo_meter == "pcc_combo") AND ($targets_pcc_combo_A != '') ) {
        $targets_pcc_combo_A = filter_var($targets_pcc_combo_A, FILTER_SANITIZE_STRING);
        echo "targets_pcc_combo_A $targets_pcc_combo_A<br> ";
        $limit_pcc_combo_A = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_combo_A']);
        $limit_pcc_combo_A = filter_var($limit_pcc_combo_A, FILTER_SANITIZE_STRING);
        echo "limit_pcc_combo_A: $limit_pcc_combo_A<br> ";
        addToPowerRegulator($conn,$targets_pcc_combo_A,$limit_pcc_combo_A,$power_regulator_id,'pcc_combo','A');
    }
}
else {
    // echo " No feed A $select_feed_name <br> ";
}


/// B OR ($select_feed_name =='C')
if ((strpos($select_feed_name, 'B') !== false) OR ($select_feed_name =='Delta')) {

    $targets_pcc_B = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_B']);
    if((($pcc_meter == 'pcc') OR ( $select_meter_role == "pcc_combo")) AND ($targets_pcc_B != '')) {
        $targets_pcc_B = filter_var($targets_pcc_B, FILTER_SANITIZE_STRING);
        echo "targets_pcc_B: $targets_pcc_B<br> ";
        $limit_pcc_B = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_B']);
        $limit_pcc_B = filter_var($limit_pcc_B, FILTER_SANITIZE_STRING);
        echo "limit_pcc_B: $limit_pcc_B<br> ";
        addToPowerRegulator($conn,$targets_pcc_B,$limit_pcc_B,$power_regulator_id,'pcc','B');
    }

    $targets_pcc2_B = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc2_B']);
    if((($pcc2_meter == 'pcc2') OR ( $select_meter_role == "pcc_combo"))  AND ($targets_pcc2_B != '')) {
        $targets_pcc2_B = filter_var($targets_pcc2_B, FILTER_SANITIZE_STRING);
        echo "targets_pcc2_B: $targets_pcc2_B<br> ";
        $limit_pcc2_B= mysqli_real_escape_string($conn, $_REQUEST['limit_pcc2_B']);
        $limit_pcc2_B= filter_var($limit_pcc2_B, FILTER_SANITIZE_STRING);
        echo "limit_pcc2_B: $limit_pcc2_B<br> ";
        addToPowerRegulator($conn,$targets_pcc2_B,$limit_pcc2_B,$power_regulator_id,'pcc2','B');
    }

    $targets_pcc_combo_B = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_combo_B']);
    if(( $pcc_combo_meter == "pcc_combo") AND ($targets_pcc_combo_B != '')) {
        $targets_pcc_combo_B = filter_var($targets_pcc_combo_B, FILTER_SANITIZE_STRING);
        echo "targets_pcc_combo_B $targets_pcc_combo_B<br> ";
        $limit_pcc_combo_B = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_combo_B']);
        $limit_pcc_combo_B = filter_var($limit_pcc_combo_B, FILTER_SANITIZE_STRING);
        echo "limit_pcc_combo_B: $limit_pcc_combo_B<br> ";
        addToPowerRegulator($conn,$targets_pcc_combo_B,$limit_pcc_combo_B ,$power_regulator_id,'pcc_combo','B');
    }
}
else {
    // echo " No feed B $select_feed_name <br> ";
}


/// C
if ((strpos($select_feed_name, 'C') !== false) OR ($select_feed_name =='Delta'))  {

    $targets_pcc_C = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_C']);
    if((($pcc_meter == 'pcc') OR ( $select_meter_role == "pcc_combo")) AND ( $targets_pcc_C != '')) {
        $targets_pcc_C = filter_var($targets_pcc_C, FILTER_SANITIZE_STRING);
        echo "targets_pcc_C: $targets_pcc_C<br> ";
        $limit_pcc_C = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_C']);
        $limit_pcc_C = filter_var($limit_pcc_C, FILTER_SANITIZE_STRING);
        echo "limit_pcc_C: $limit_pcc_C<br> ";
        addToPowerRegulator($conn,$targets_pcc_C,$limit_pcc_C,$power_regulator_id,'pcc','C');
    }

    $targets_pcc2_C = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc2_C']);
    if((($pcc2_meter  == 'pcc2') OR ( $select_meter_role == "pcc_combo"))  AND ( $targets_pcc2_C != '')) {
        $targets_pcc2_C = filter_var($targets_pcc2_C, FILTER_SANITIZE_STRING);
        echo "targets_pcc2_C: $targets_pcc2_C<br> ";
        $limit_pcc2_C = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc2_C']);
        $limit_pcc2_C = filter_var($limit_pcc2_C, FILTER_SANITIZE_STRING);
        echo "limit_pcc2_C: $limit_pcc2_C<br> ";
        addToPowerRegulator($conn,$targets_pcc2_C,$limit_pcc2_C,$power_regulator_id,'pcc2','C');
    }
    $targets_pcc_combo_C = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_combo_C']);
    if(( $pcc_combo_meter == "pcc_combo") AND ( $targets_pcc_combo_C != '')) {
        $targets_pcc_combo_C = filter_var($targets_pcc_combo_C, FILTER_SANITIZE_STRING);
        echo "targets_pcc_combo_B $targets_pcc_combo_C<br> ";
        $limit_pcc_combo_C = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_combo_C']);
        $limit_pcc_combo_C = filter_var($limit_pcc_combo_C, FILTER_SANITIZE_STRING);
        echo "limit_pcc_combo_C: $limit_pcc_combo_C<br> ";
        addToPowerRegulator($conn,$targets_pcc_combo_C,$limit_pcc_combo_C,$power_regulator_id,'pcc_combo','C');
    }
}else {
    // echo " No feed C $select_feed_name <br> ";
}


// Delta
if($select_feed_name =='Delta') {
    if(($pcc_meter == 'pcc') OR ( $select_meter_role == "pcc_combo")) {
        $targets_pcc_Delta = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_Delta']);
        $targets_pcc_Delta = filter_var($targets_pcc_Delta, FILTER_SANITIZE_STRING);
        echo "targets_pcc_Delta: $targets_pcc_Delta<br> ";
        $limit_pcc_Delta = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_Delta']);
        $limit_pcc_Delta = filter_var($limit_pcc_Delta, FILTER_SANITIZE_STRING);
        echo "limit_pcc_Delta: $limit_pcc_Delta<br> ";
        addToPowerRegulator($conn,$targets_pcc_Delta,$limit_pcc_Delta,$power_regulator_id,'pcc','Delta');
    }
    if(($pcc2_meter == 'pcc2') OR ( $select_meter_role == "pcc_combo")) {
        $targets_pcc2_Delta = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc2_Delta']);
        $targets_pcc2_Delta = filter_var($targets_pcc2_Delta, FILTER_SANITIZE_STRING);
        echo "targets_pcc2_Delta: $targets_pcc2_Delta<br> ";
        $limit_pcc2_Delta = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc2_Delta']);
        $limit_pcc2_Delta = filter_var($limit_pcc2_Delta, FILTER_SANITIZE_STRING);
        echo "limit_pcc2_Delta: $limit_pcc2_Delta<br> ";
        addToPowerRegulator($conn,$targets_pcc2_Delta,$limit_pcc2_Delta,$power_regulator_id,'pcc2','Delta');
    }
    if(( $$pcc_combo_meter == "pcc_combo")) {
        $targets_pcc_combo_Delta = mysqli_real_escape_string($conn, $_REQUEST['targets_pcc_combo_Delta']);
        $targets_pcc_combo_Delta = filter_var($targets_pcc_combo_Delta, FILTER_SANITIZE_STRING);
        echo "targets_pcc_combo_Delta $targets_pcc_combo_Delta<br> ";
        $limit_pcc_combo_Delta = mysqli_real_escape_string($conn, $_REQUEST['limit_pcc_combo_Delta']);
        $limit_pcc_combo_Delta = filter_var($limit_pcc_combo_Delta, FILTER_SANITIZE_STRING);
        echo "limit_pcc_combo_Delta: $limit_pcc_combo_Delta<br> ";
        addToPowerRegulator($conn,$targets_pcc_combo_Delta,$limit_pcc_combo_Delta,$power_regulator_id,'pcc_combo','Delta');
    }
}else {
    // echo " No feed Delta $select_feed_name <br> ";
}


function addToPowerRegulator($conn,$target,$threshold,$power_regulator_id,$meter,$feed){

    //echo "<br> target = $target , threshold = $threshold <br>";
    $target_offset = $threshold - $target;

    $target_offset = $threshold - $target;
    $sql = "INSERT INTO meter_feeds (power_regulator_id, meter_role, feed_name, threshold,target, target_offset) 
                VALUES ('$power_regulator_id', '$meter', '$feed', '$threshold','$target', '$target_offset')
                ON DUPLICATE KEY UPDATE threshold = '$threshold', target_offset = '$target_offset'";
    if (!mysqli_query($conn, $sql)) {

    }

}

header('Location: /power_regulator.php');




/*
foreach ($meter_feed_targets as $meter => $targets) {
    foreach ($targets as $feed => $target) {
        $threshold = $meter_feeds[$meter][$feed];
        $target= $meter_feed_targets[$meter][$feed];
        if (ss && strlen($threshold) == 0) {
            $sql = "DELETE FROM meter_feeds";
            $sql .= " WHERE power_regulator_id = '$power_regulator_id' AND meter_role = '$meter' AND feed_name = '$feed'";
            if (!mysqli_query($conn, $sql)) {
                header('Location: /power_regulator.php?error=delete_error');
                die('Error: ' . mysqli_error($conn));
            }
        } else {
            $target_offset = $threshold - $target;
            $sql = "INSERT INTO meter_feeds (power_regulator_id, meter_role, feed_name, threshold, target_offset)
                VALUES ('$power_regulator_id', '$meter', '$feed', '$threshold', '$target_offset')
                ON DUPLICATE KEY UPDATE threshold = '$threshold', target_offset = '$target_offset'";
            if (!mysqli_query($conn, $sql)) {
                header('Location: /power_regulator.php?error=insert_error');
                die('Error: ' . mysqli_error($conn));
            }
        }
    }
}
*/


//
?>

