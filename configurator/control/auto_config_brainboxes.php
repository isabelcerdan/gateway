<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../control/get_brainboxes.php');
include_once ('../functions/brainbox_config.php');

$port = 9000;
$count = count($target_interfaceDB);
$errors = array();

// iterate through brainboxes returned by get_brainboxes.php
for($i = 0; $i < $count; $i++) {

    // if port number is not set, find unused port
    if ($tcp_portDB[$i] == '') {
        while (in_array($port, $tcp_portDB))
            $port += 1;
        $tcp_portDB[$i] = $port;    // assign unused port
        $port += 1;                 // bump for next time around
    }

    $config = array(
        'mac_address' => $mac_addressDB[$i],
        'ip_address' => $IP_AddressDB[$i],
        'tcp_port' => $tcp_portDB[$i],
        'baud' => $baudDB[$i]
    );

    $err = brainbox_config($config);

    $sql = "UPDATE brainboxes SET tcp_port = '" . $config['tcp_port'] . "', baud = '" . $config['baud'] . "'";
    $sql = $sql . " WHERE IP_Address = '" .$config['ip_address'] . "' AND mac_address = '" . $config['mac_address'] . "'";
    if (!mysqli_query($conn, $sql))
        $err = $config['mac_address'];

    if ($err)
        array_push($errors, $err);
}

if (count($errors) > 0) {
    $message = "Failed to auto-configure device(s):";
} else {
    $success = $count - count($errors);
    $message = "Successfully auto-configured " . $success . "/" . $count . " device(s).";
}

$results = array("results" =>
    array("message" => $message, "errors" => implode(',', $errors))
);

echo json_encode($results);

?>
