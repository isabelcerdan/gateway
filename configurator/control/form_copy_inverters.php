<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/17/2016
 * Time: 4:49 PM
 */

//include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$resultGetGroup = $conn->query("select MAX(groupId)AS MaxGroupID from saved_inverters ");
$MaxGroupID = 0;
if(mysqli_num_rows($resultGetGroup)>0) {

    while ($row = $resultGetGroup->fetch_assoc()) {
         $MaxGroupID = $row['MaxGroupID'];
    }
}

if($MaxGroupID == 0){
    $groupId = 1;
}else {
    $groupId = $MaxGroupID+ 1;
}
//groupId
    $result = $conn->query("SELECT * FROM inverters");

$values = '';
 //serialNumber, mac_address,stringId, stringPosition
if(mysqli_num_rows($result)>0) {

    $count = 0;
    $comma = '';
    while($row = $result->fetch_assoc()) {
        $inverters_id[] = $row['inverters_id'];
        $serialNumber[] = $row['serialNumber'];
        $group_id[] = $row['group_id'];
        $mac_address[] =  $row['mac_address'];
        $IP_Address[] =  $row['IP_Address'];
        $version[] =  $row['version'];
        $lable[] =  $row['lable'];
        $stringId = $row['stringId'];
        $stringPosition[] =  $row['stringPosition'];
        $export_status[] =  $row['export_status'];
        $prodPartNo[] =  $row['prodPartNo'];
        $output_status[] =  $row['output_status'];
        $controlledByGateway[] =  $row['controlledByGateway'];
        $FEED_NAME_inverter[] =  $row['FEED_NAME'];
        $status[] =  $row['status'];
        $upgrade_status[] =  $row['upgrade_status'];

        if($count != 0)  {
            $comma = ',';
        }
        $count++;
        $values = $values . "$comma('" . $row['serialNumber'] .  "','" . $row['mac_address'] . "','" . $row['stringId'] . "','" . $row['stringPosition'] . "','" . $groupId . "', '" . $row['version']  . "')";
    }
}

//echo $values;


$sql = "INSERT INTO saved_inverters (serialNumber, mac_address,stringId, stringPosition,groupId,version) VALUE  $values";


if (!mysqli_query($conn,$sql)) {

    // see if Duplicate inverters are being rejected by 
    $resultGetDuplicate= $conn->query("SELECT inverters_id, serialNumber
FROM inverters
WHERE `serialNumber` IN (select serialNumber from saved_inverters WHERE serialNumber  IN (" . implode(", ",$serialNumber) . "))");

    if(mysqli_num_rows($resultGetDuplicate)>0) {

        while ($row = $resultGetDuplicate->fetch_assoc()) {
           echo $inverters_idDuplicate = $row['inverters_id'] . ',';
        }
    }

exit();
    //die('<br>Error: ' . mysqli_error($conn));



}else{
    
        //echo "running delete all";
        include_once ('/usr/share/apparent/www/configurator/functions/deleteLeases.php');

        $result = $conn->query("SELECT mac_address  FROM inverters ");

        if(mysqli_num_rows($result)>0) {

            while ($row = $result->fetch_assoc()) {
                $mac_addresses[] = $row['mac_address'];
            }
        }

        $sql = " DELETE FROM inverters ";
        if (mysqli_query($conn,$sql)) {

            deleteLease($mac_addresses);

            //header('Location: /inverters_list.php?delete=success');
            die('Successfully deleted');
        }
        else {
            //header('Location: /inverters_list.php??delete=fail');
            die('<br>Error: ' . mysqli_error($conn));
        }


    echo "Success";
    exit();
}

//$conn->close();
/*
 * insert into saved_inverters (serialNumber) VALUES ('
 *INSERT INTO saved_inverters (serialNumber) VALUE ('171509000003'),('171509000004');
 *
 */
?>