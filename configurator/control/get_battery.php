<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM


CESS_40_UNIT_WORK_STATE = "unit_work_state"
CESS_40_UNIT_CTRL_MODE = "unit_ctrl_mode"
CESS_40_UNIT_WORK_MODE = "unit_work_mode"
 * active_power_cmd_setting
 * active_power_cmd_setting
 */


include_once ('../functions/mysql_connect.php');

    $result = $conn->query("SELECT *  FROM ess_master_info ");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {

            $unit_ctrl_mode =  $row['unit_ctrl_mode'];
            $unit_work_state =  $row['unit_work_state'];
            $reconnect_state = $row['reconnect_state'];
            $battery_string_running = $row['battery_string_running'];
            $battery_string_enabled = $row['battery_string_enabled'];
            $battery_string_isolated = $row['battery_string_isolated'];
            $battery_stack_voltage = $row['battery_stack_voltage'];
            $battery_stack_current = $row['battery_stack_current'];
            $battery_stack_power  = $row['battery_stack_power'];
            $battery_stack_soc = number_format($row['battery_stack_soc'],2);
            $battery_stack_soh = number_format($row['battery_stack_soh'],2);
            $battery_max_cell_voltage = $row['battery_max_cell_voltage'];
            $battery_min_cell_voltage = $row['battery_min_cell_voltage'];
            $battery_max_cell_temperature  = $row['battery_max_cell_temperature'];
            $battery_min_cell_tempreature  = $row['battery_min_cell_tempreature'];
            $dc_voltage  = $row['dc_voltage'];
            $dc_current = $row['dc_current'];
            $dc_power = $row['dc_power'];
            $p_value = $row['p_value'];
            $q_value = $row['q_value'];
            $s_value = $row['s_value'];
            $ia_value  = $row['ia_value'];
            $ib_value = $row['ib_value'];
            $ic_value = $row['ic_value'];
            $uab_value = $row['uab_value'];
            $ubc_value = $row['ubc_value'];
            $uac_value = $row['uac_value'];
            $ua_value = $row['ua_value'];
            $ub_value  = $row['ub_value'];
            $uc_value  = $row['uc_value'];
            $frequency =  $row['frequency'];
            $power_factor  =  $row['power_factor'];
            $unit_work_mode =  $row['unit_work_mode'];
            $work_state_control_cmd  =  $row['work_state_control_cmd'];
            $active_power_cmd =  $row['active_power_cmd'];
            $reactive_power_cmd =  $row['reactive_power_cmd'];
            $voltage_control_cmd = $row['voltage_control_cmd'];
            $frequency_control_cmd = $row['frequency_control_cmd'];
            $timestamp = $row['timestamp'];
            $apparent_power = $row['apparent_power'];
            $true_power = $row['rue_power'];
        }
    }

$resultAlarm = $conn->query("SELECT * FROM ess_units");

if(mysqli_num_rows($resultAlarm)>0) {

    while($row = $resultAlarm->fetch_assoc()) {

        $soc_charge_limit =  $row['soc_charge_limit'];
        $soc_charge_limit_response =  $row['soc_charge_limit_response'];
        $soc_discharge_limit  =  $row['soc_discharge_limit'];
        $soc_discharge_limit_response  =  $row['soc_discharge_limit_response'];
        $battery_max_cell_voltage_limit =  $row['battery_max_cell_voltage_limit'];
        $battery_max_cell_voltage_limit_response =  $row['battery_max_cell_voltage_limit_response'];
        $battery_min_cell_voltage_limit =  $row['battery_min_cell_voltage_limit'];
        $battery_min_cell_voltage_limit_response =  $row['battery_min_cell_voltage_limit_response'];

    }
}



$resultController = $conn->query("SELECT * FROM cess_400_controller_control");

if(mysqli_num_rows($resultController)>0) {

    while($row = $resultController->fetch_assoc()) {
        $cess_controller_control_state =  $row['enabled'];
    }
}
?>