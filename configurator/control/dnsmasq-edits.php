<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('/usr/share/apparent/www/configurator/functions/restrict_privilage_access.php');
include_once ('/usr/share/apparent/www/configurator/functions/deleteLeases.php');

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/7/2017
 * Time: 4:21 PM
 * errors
 * ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */


$numOfIP = mysqli_real_escape_string($conn, $_POST['numOfIP']);
$numOfIP = filter_var($numOfIP, FILTER_SANITIZE_STRING);

$userIPSelect= mysqli_real_escape_string($conn, $_POST['userIPSelect']);
$userIPSelect = filter_var($userIPSelect, FILTER_VALIDATE_INT);

$id = filter_var($_SERVER['QUERY_STRING'], FILTER_SANITIZE_STRING);
$id = str_replace('%3A', ':',$id);

$command = filter_var($_REQUEST['command'], FILTER_SANITIZE_STRING);
//echo $command . "<br>";
//Just get IP address
preg_match_all('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $id, $ip_matches);


//preg_match_all('/^(?:[0-9a-fA-F]{2}[:;.]?){6}$/', $id, $macResults);
preg_match_all("/(?:[0-9a-fA-F]{2}[:]?){6}/", $id, $macResults);
// print_r($macResults);

include_once ( $_SERVER['DOCUMENT_ROOT'] . '/functions/mysql_connect.php');
include_once ( $_SERVER['DOCUMENT_ROOT'] .  '/functions/get_dhcp_info.php');

$dhcpFileLineContents = getDHCPInfo();
$getHeader = getDHCPInfo('yes');

if($numOfIP  == 'yes'){
    echo"<h2>IP Address Changes <h2></h2>";
    $stop = exec("sudo /etc/init.d/dnsmasq stop", $outcome2, $status2);

    echo "<small>" . $outcome2[0] . "</small>";
    echo "<br>";

    $secondBlock = explode(".",$dhcpFileLineContents[1]);
    $thirdBlock = explode("\n",$dhcpFileLineContents[2]);

    if( filter_var($userIPSelect, FILTER_VALIDATE_INT) ){
        $NewIPs = $userIPSelect;
    }else {
        $NewIPs = $secondBlock[2];
    }
    echo "<p style='margin: 40px;'><strong>The new number of IP address is: " . $NewIPs * 256 . " </strong></p>";

    $myfile = fopen("/etc/dnsmasq.d/dnsmasq-gateway.conf", "w");
    $txt = "dhcp-range=$dhcpFileLineContents[0],$secondBlock[0].$secondBlock[1].$NewIPs.$secondBlock[3],$dhcpFileLineContents[2]";

    fwrite($myfile, $getHeader . $txt);
    fclose($myfile);
    $start = exec("sudo /etc/init.d/dnsmasq start", $outcome3, $status3);

    echo "<small>" . $outcome3[0] . "</small>";

}



if($command == 'ping') {
    echo"<h2>Ping Results <h2></h2>";
    for($i=0; $i < count($ip_matches[0]);$i++ ) {
        pingAddress($ip_matches[0][$i]) ;
    }
}

if($command == 'restart') {
    echo"<h2>Restarting DHCP <h2></h2>";
    $stop = exec("sudo /etc/init.d/dnsmasq stop", $outcome2, $status2);

    echo "<small>" . $outcome2[0] . "</small>";
    echo "<p style='margin: 40px;'><strong>Restarting...</p>";
    $start = exec("sudo /etc/init.d/dnsmasq start", $outcome3, $status3);

    echo "<small>" . $outcome3[0] . "</small>";
}

if($command == 'deletelease') {

    deleteLease($macResults[0]);
//echo 'ok';
    /*
    echo"<h2>Remove Device from DHCP Lease File<h2></h2>";
    $stop = exec("sudo /etc/init.d/dnsmasq stop", $outcome2, $status2);

    echo "<small>" . $outcome2[0] . "</small>";
    echo "<br>";
    $lineList = array();
    for($i=0; $i < count($macResults[0]);$i++ ) {
        echo $rawLease = shell_exec("grep -rnw '/var/lib/misc/dnsmasq.leases' -e '" . $macResults[0][$i] . "'");
        $rawLeaseLine = explode(":",$rawLease);
        if($rawLease >0) {
            echo  "<p style='margin: 40px;'>Removing Device: " .  $macResults[0][$i] . "</p><br>" ;
            try {
                var_dump(shell_exec("sed -i '" . $rawLeaseLine[0] . "d'  /var/lib/misc/dnsmasq.leases 2>&1"));
            }catch(Exception $e) {
                echo 'Message: ' .$e->getMessage();
            }
        }else {
            echo  "<p style='margin: 40px;'>No Devices Removed </p><br>" ;
        }


    }

    $start = exec("sudo /etc/init.d/dnsmasq start", $outcome3, $status3);
    echo "<small>" . $outcome3[0] . "</small>";
*/
}




function pingAddress($ip) {
    $pingresult = exec("ping -c 1 $ip", $outcome, $status);
    if (0 == $status) {
        $status = "$outcome[7]   Active<br> ";
    } else {
        $status = "Not Responding";
    }
    echo "<div style='font-size: 12px;'>IP $ip, status:  <strong>". $status . "</strong></div>";
}


echo "<br>";

