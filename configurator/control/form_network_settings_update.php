<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$iface = mysqli_real_escape_string($conn, $_POST['iface']);
$iface = filter_var($iface, FILTER_SANITIZE_STRING);

$inet = mysqli_real_escape_string($conn, $_POST['inet']);
$inet = filter_var($inet, FILTER_SANITIZE_STRING);

$address = mysqli_real_escape_string($conn, $_POST['address']);
$address = filter_var($address, FILTER_VALIDATE_IP);

$netmask = mysqli_real_escape_string($conn, $_POST['netmask']);
$netmask = filter_var($netmask, FILTER_VALIDATE_IP);

//$broadcast = mysqli_real_escape_string($conn, $_POST['broadcast']);
//$broadcast = filter_var($broadcast, FILTER_VALIDATE_IP);

$gateway = mysqli_real_escape_string($conn, $_POST['gateway']);
$gateway = filter_var($gateway, FILTER_VALIDATE_IP);


// A static IP address needs all of the below info to work //
if($inet == 'static') {
        // OR ($broadcast == '')
    if(($address == '') OR ($netmask == '')) {
        header('Location: /network_settings.php?status=error');
        exit();
    }
}

echo $sql = "UPDATE network_config SET
  inet = '$inet', address = '$address', netmask = '$netmask',  gateway = '$gateway' WHERE iface = 'eth0'";

//broadcast = '$broadcast',
if (!mysqli_query($conn,$sql)) {
    header('Location: /network_settings.php?status=error');
    die('<br>Error: ' . mysqli_error($conn));
}
else {

    $start = exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python /usr/share/apparent/python/inet/manage_inet.py --sync-from=db", $outcome3, $status3);
    ///usr/share/apparent/python/inet/manage_inet.py --sync-from=db
    header('Location: /network_settings.php?status=success&restart=' . $outcome3[0]);

}

?>(
