<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Grab default values for both examples and for default in form fields
 *
 *
 * */

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');
include_once ('get_system.php');

/*
 * See if there is any live data and replace defaults with live data
 *
 * */


if($setToDefault != 'yes') {
    $result = $conn->query("SELECT * FROM power_regulators WHERE id = '$power_regulator_id' ");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $power_regulator_id = $row['id'];
            $power_regulator_name = $row['name'];
            $feed_name = $row['feed_name'];
            $has_solar = $row['has_solar'];
            $has_storage = $row['has_storage'];
            $enabled = $row['enabled'];
            $step_size = $row['step_size'];
            $battery_strategy = $row['battery_strategy'];
        }
    }
}

if($power_regulator_id != '') {
    $result = $conn->query("SELECT * FROM power_regulators WHERE id = '$power_regulator_id' ");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
           // $power_regulator_idDB = $row['id'];
            $power_regulator_nameDB = $row['name'];
            $feed_name_DB = $row['feed_name'];
            $has_solarDB = $row['has_solar'];
            $has_storageDB = $row['has_storage'];
            $enabledDB = $row['enabled'];
            $step_sizeDB = $row['step_size'];
            $battery_strategyDB = $row['battery_strategy'];
        }
    }
}else{
    $result = $conn->query("SELECT * FROM power_regulators ");
    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $power_regulator_idArray[] = $row['id'];
            $pr_name_Array[] = $row['name'];
            $pr_feed_Array[] = $row['feed_name'];
            $has_solarArray[] = $row['has_solar'];
            $has_storagerray[] = $row['has_storage'];
            $enabledArray[] = $row['enabled'];
            $step_sizeArray[]= $row['step_size'];
            $battery_strategyArray[] = $row['battery_strategy'];
        }
    }
}
// Get number of inverters by feed

$Inverters_per_feed = $conn->query(" select count(*) AS count_of_feed from inverters where FEED_NAME = '$FEED_NAME' group by FEED_NAME ");

if(mysqli_num_rows($Inverters_per_feed)>0) {
    while($row = $Inverters_per_feed->fetch_assoc()) {
        $count_of_feed = $row['count_of_feed'];
    }
}

//$conn->close();


?>