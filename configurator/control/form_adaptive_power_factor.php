<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once('../functions/session.php');
include_once('../functions/restrict_privilage_access.php');

include_once('../functions/mysql_connect.php');


$error = '';

$enabled = mysqli_real_escape_string($conn, $_POST['enabled']);
echo $enabled = filter_var($enabled, FILTER_SANITIZE_STRING);

if ($enabled != 1) {
    $enabled = 0;
}

$pf_target_A = mysqli_real_escape_string($conn, $_POST['pf_target_A']);
$pf_target_A = filter_var($pf_target_A, FILTER_SANITIZE_STRING);

$pf_target_A_leading = mysqli_real_escape_string($conn, $_POST['pf_target_A_leading']);
$pf_target_A_leading = filter_var($pf_target_A_leading, FILTER_SANITIZE_STRING);;
$pf_enable_A = mysqli_real_escape_string($conn, $_POST['pf_enable_A']);
$pf_enable_A = filter_var($pf_enable_A, FILTER_SANITIZE_STRING);

if ($pf_enable_A != 1) {
    $pf_enable_A = 0;
}


$pf_target_B = mysqli_real_escape_string($conn, $_POST['pf_target_B']);
$pf_target_B = filter_var($pf_target_B, FILTER_SANITIZE_STRING);

$pf_target_B_leading = mysqli_real_escape_string($conn, $_POST['pf_target_B_leading']);
$pf_target_B_leading = filter_var($pf_target_B_leading, FILTER_SANITIZE_STRING);

$pf_enable_B = mysqli_real_escape_string($conn, $_POST['pf_enable_B']);
$pf_enable_B = filter_var($pf_enable_B, FILTER_SANITIZE_STRING);

if ($pf_enable_B != 1) {
    $pf_enable_B = 0;
}

$pf_target_C = mysqli_real_escape_string($conn, $_POST['pf_target_C']);
$pf_target_C = filter_var($pf_target_C, FILTER_SANITIZE_STRING);

$pf_target_C_leading = mysqli_real_escape_string($conn, $_POST['pf_target_C_leading']);
$pf_target_C_leading = filter_var($pf_target_C_leading, FILTER_SANITIZE_STRING);

$pf_enable_C = mysqli_real_escape_string($conn, $_POST['pf_enable_C']);
$pf_enable_C = filter_var($pf_enable_C, FILTER_SANITIZE_STRING);

if ($pf_enable_C != 1) {
    $pf_enable_C = 0;
}

$sql = "UPDATE power_factor_control set pf_target_A = '$pf_target_A',pf_target_A_leading = '$pf_target_A_leading',
 pf_enable_A = '$pf_enable_A', pf_target_B = '$pf_target_B',pf_target_B_leading = '$pf_target_B_leading',
 pf_enable_B = '$pf_enable_B', pf_target_C = '$pf_target_C',pf_target_C_leading = '$pf_target_C_leading',
 pf_enable_C = '$pf_enable_C' ";


if (!mysqli_query($conn, $sql)) {
    header('Location: /set_adaptive_power_factor.php?changed_id=problems');
    die('<br>Error: ' . mysqli_error($conn));
} else {
    if ($conn->insert_id != 0) {
        $changed_id = mysqli_affected_rows($conn);
    }
    $changed_id = mysqli_affected_rows($conn);
    header('Location: /set_adaptive_power_factor.php?changed_id=' . $changed_id);
}

$sql = "UPDATE daemon_control SET enabled = '$enabled' WHERE name = 'pfcd'";

if (!mysqli_query($conn, $sql)) {
    header('Location: /set_adaptive_power_factor.php?changed_id=problems');
    die('<br>Error: ' . mysqli_error($conn));
} else {
    if ($conn->insert_id != 0) {
        $changed_id = mysqli_affected_rows($conn);
    }
    $changed_id = mysqli_affected_rows($conn);
    header('Location: /set_adaptive_power_factor.php?changed_id=' . $changed_id);
}

?>