<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
    $IOFlex_id = mysqli_real_escape_string($conn, $_POST['IOFlex_id']);
    $IOFlex_id = filter_var($IOFlex_id, FILTER_SANITIZE_STRING);

    $apparent_part = mysqli_real_escape_string($conn, $_POST['apparent_part']);
    $apparent_part = filter_var($apparent_part, FILTER_SANITIZE_STRING);

    $IOdevice_name = mysqli_real_escape_string($conn, $_POST['IOdevice_name']);
    $IOdevice_name= filter_var($IOdevice_name, FILTER_SANITIZE_STRING);

    $mac_address = mysqli_real_escape_string($conn, $_POST['mac_address']);
    $mac_address = filter_var($mac_address, FILTER_SANITIZE_STRING);

    $device_desc = mysqli_real_escape_string($conn, $_POST['device_desc']);
    $device_desc = filter_var($device_desc, FILTER_SANITIZE_STRING);


$sql = "INSERT INTO IO_Flex (IOFlex_id,apparent_part,mac_address,device_desc, device_name)
  value('$IOFlex_id', '$apparent_part','$mac_address','$device_desc','$IOdevice_name')
  ON DUPLICATE KEY UPDATE
  IOFlex_id = '$IOFlex_id',apparent_part = '$apparent_part',device_desc = '$device_desc', device_name = '$IOdevice_name'
";

if (!mysqli_query($conn,$sql)) {
    header('Location: /success_io.php?errors=' . $error = $error );
    die('<br>Error: ' . mysqli_error($conn));
}
else {

   // mysqli_close($con);
    header('Location: /success_io.php?IOFlex_id=' .$conn->insert_id);

}


?>