<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */
include_once ('../functions/session.php');
/*
 * Get all feeds to edit
 *
 *mysql> desc minion_auditor;
+------------------------+--------------+------+-----+---------+----------------+
| Field                  | Type         | Null | Key | Default | Extra          |
+------------------------+--------------+------+-----+---------+----------------+
| id                     | int(11)      | NO   | PRI | NULL    | auto_increment |
| title                  | varchar(55)  | YES  |     | NULL    |                |
| runs                   | int(11)      | YES  |     | NULL    |                |
| suspended              | varchar(7)   | YES  |     | NULL    |                |
| next_launch_time       | datetime     | YES  |     | NULL    |                |
| completion_time        | datetime     | YES  |     | NULL    |                |
| results_path_file_name | varchar(50)  | YES  |     | NULL    |                |
| frequency              | varchar(12)  | YES  |     | NULL    |                |
| time_value             | int(11)      | YES  |     | NULL    |                |
| active                 | varchar(5)   | YES  |     | NULL    |                |
| email                  | varchar(150) | YES  |     | NULL    |                |
+------------------------+--------------+------+-----+---------+----------------+
 * */

include_once ('../functions/mysql_connect.php');



if($minion_auditor_id == '') {
 // echo "No id";
    $result = $conn->query("SELECT *  FROM minion_auditor ");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $minion_auditor_idDB[] = $row['id'];
            $titleDB[] = $row['title'];
            $runsDB[]  = $row['runs'];
            //$suspendedDB[]  = $row['suspended'];
            $next_launch_timetDB[]  = $row['next_launch_time'];
            $completion_timeDB[] = $row['completion_time'];
            $results_path_file_nameDB[] = $row['results_path_file_name'];
            $frequencyDB[] = $row['frequency'];
            $time_valueDB[] = $row['time_value'];
            $activeDB[] = $row['active'];
            $emailDB[] = $row['email'];
            $run_nowDB[] = $row['run_now'];

        }
    }
}

else {
   // echo "yes id: $minion_auditor_id";

    $result = $conn->query("SELECT *  FROM minion_auditor WHERE id = '$minion_auditor_id'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $minion_auditor_id = $row['id'];
            $title = $row['title'];
            $runs  = $row['runs'];
            //$suspended  = $row['suspended'];
            $next_launch_timet  = $row['next_launch_time'];
            $completion_time = $row['completion_time'];
            $results_path_file_name = $row['results_path_file_name'];
            $frequency = $row['frequency'];
            $time_value = $row['time_value'];
            $active = $row['active'];
            $email= $row['email'];
            $run_now= $row['run_now'];
        }
    }

}
   // $conn->close();

?>