<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/17/2018
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

// get list of inverters in DB
$result = $conn->query("SELECT serialNumber FROM inverters");

if(mysqli_num_rows($result)>0) {
    while($row = $result->fetch_assoc()) {
        $serialNumber[] = $row['serialNumber'];
        $mac_address[] = $row['mac_address'];
        $IP_Address[] = $row['IP_Address'];
    }
}

$error = '';
$group_id= mysqli_real_escape_string($conn, $_REQUEST['group_id']);
$group_id = filter_var($group_id, FILTER_SANITIZE_STRING);

$inverters  = mysqli_real_escape_string($conn, $_POST['inverters']);
$inverters = filter_var($inverters, FILTER_SANITIZE_STRING);

$string =str_replace('\r\n',',',$inverters);
$string =str_replace(' ','',$string);
//$string =str_replace('.','',$string);
$string =str_replace('<br>','',$string);
$string =str_replace('-','',$string);

$array=explode(',', $string);

// Find out what type of data is coming in by testing first item in array //



$badInverters = array();
$goodInverters = array();
$notInGatInverters = array();




for($i = 0;$i < count($array);$i++ ) {


    if((inverterStringTest($array[$i]) == false) AND (!filter_var($array[$i], FILTER_VALIDATE_IP) AND (!filter_var($array[$i], FILTER_VALIDATE_MAC))) ) {
        $badInverters[] = $array[$i];
        echo "Bad:  $array[$i]<br>";
    }
    elseif((!in_array($array[$i], $serialNumber)) AND (in_array($array[$i], $mac_address)) AND (in_array($array[$i], $IP_Address)) ) {
        $notInGatInverter[] = $array[$i];
        echo "Not in DB:  $array[$i]<br>";
    }
    else{
        $goodInverters[] = $array[$i];
        echo "Good:  $array[$i]<br>";
    }
}
$goodInverters = array_unique($goodInverters);
$goodInvertersOutput =$goodInverters;
$notInGatInverter = array_unique($notInGatInverter);
$badInverters = array_unique($badInverters);

$goodInverters = implode("','", $goodInverters);

$sql = "UPDATE inverters SET group_id ='$group_id', group_audit_required = 1 WHERE serialNumber IN ('".$goodInverters."') OR mac_address IN ('".$goodInverters."') OR IP_Address IN ('".$goodInverters."')";
if (!mysqli_query($conn,$sql)) {
    //header('Location: /meter-add-edit.php?errors=' . $error .  " problem");
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if ($conn->insert_id != 0) {
     echo    $id= $conn->insert_id;
    }
}


function inverterStringTest($inverterSN) {

    if ( filter_var($inverterSN, FILTER_VALIDATE_INT) === false ) {
        return false;
    }
    elseif(strlen($inverterSN) != 12){
        return false;
    }
    else{
        return true;
    }
}

header('Location: /success_group.php?group_id=' . $group_id .'&badInverters=' .serialize($badInverters) . '&notInGatInverters=' . serialize($notInGatInverter) . '&goodInverters=' . serialize($goodInvertersOutput) );

?>