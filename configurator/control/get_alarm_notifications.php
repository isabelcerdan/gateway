<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');



if($alarm_subscriptions_id  == '') {
    //echo "No id";
    $result = $conn->query("SELECT alarm_subscribers.id AS user_id, alarm_subscribers.name AS user_name, alarm_subscribers.email_address, alarm_subscribers.created_at AS user_c_date, subscriptions_id,subscriber_id, alarm_id, alarms.name AS alarm_name, enabled, alarm_subscriptions.created_at AS sub_c_date
FROM alarm_subscribers
INNER JOIN alarm_subscriptions ON alarm_subscribers.id=alarm_subscriptions.subscriber_id
INNER JOIN alarms ON alarm_id=alarms.id");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $user_id_DB[]=  $row['user_id'];
            $user_name_DB[]=  $row['user_name'];
            $email_address_DB[]=  $row['email_address'];
            $user_c_date_DB[]=  $row['user_c_date'];
            $subscriber_id_DB[]=  $row['subscriber_id'];
            $subscriptions_id_DB[]=  $row['subscriptions_id'];
            $alarm_id_DB[]=  $row['alarm_id'];
            $alarm_name_DB[]=  $row['alarm_name'];
            $enabled_DB[]=  $row['enabled'];
            $sub_c_date_DB[]=  $row['sub_c_date'];

        }
    }
}

else {

  // echo "yes id";
    $result = $conn->query("SELECT alarm_subscribers.id AS user_id, alarm_subscribers.name AS user_name, alarm_subscribers.email_address, alarm_subscribers.created_at AS user_c_date, subscriptions_id,subscriber_id, alarm_id, alarms.name AS alarm_name, enabled, alarm_subscriptions.created_at AS sub_c_date
FROM alarm_subscribers
INNER JOIN alarm_subscriptions ON alarm_subscribers.id=alarm_subscriptions.subscriber_id
INNER JOIN alarms ON alarm_id=alarms.id
WHERE subscriptions_id = '$alarm_subscriptions_id'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
           $alarm_user_id=  $row['user_id'];
            $alarm_user_name=  $row['user_name'];
            $alarm_email_address=  $row['email_address'];
            $user_c_date=  $row['user_c_date'];
            $subscriber_id=  $row['subscriber_id'];
            $subscriptions_id=  $row['subscriptions_id'];
            $alarm_id=  $row['alarm_id'];
            $alarm_name=  $row['alarm_name'];
            $enabled=  $row['enabled'];
            $sub_c_date=  $row['sub_c_date'];
        }
    }
}
// $conn->close();

?>