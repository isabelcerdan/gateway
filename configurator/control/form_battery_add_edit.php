<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');



$battery_id= mysqli_real_escape_string($conn, $_REQUEST['battery_id']);
$battery_id = filter_var($battery_id, FILTER_SANITIZE_STRING);

$model_id = mysqli_real_escape_string($conn, $_REQUEST['model_id']);
$model_id = filter_var($model_id, FILTER_SANITIZE_STRING);
include_once ('get_model.php'); // Get default data

$name = $manufacturer . '-' .$model;

$ip_address = mysqli_real_escape_string($conn, $_REQUEST['ip_address']);
$ip_address= filter_var($ip_address, FILTER_SANITIZE_STRING);

$mac_address = mysqli_real_escape_string($conn, $_REQUEST['mac_address']);
$mac_address = filter_var($mac_address, FILTER_SANITIZE_STRING);

$power_regulator = mysqli_real_escape_string($conn, $_REQUEST['power_regulator']);
$power_regulator = filter_var($power_regulator, FILTER_SANITIZE_STRING);

$delete_battery = mysqli_real_escape_string($conn, $_REQUEST['delete_battery']);
$delete_battery = filter_var($delete_battery, FILTER_SANITIZE_STRING);

$battery_meter_id = mysqli_real_escape_string($conn, $_REQUEST['battery_meter_id']);
$battery_meter_id= filter_var($battery_meter_id, FILTER_SANITIZE_STRING);

$resultFeeds = $conn->query("select * from power_regulators;");

$feedStatus = false;
if(mysqli_num_rows($resultFeeds)>0) {
    $feedStatus = true;
}

if($feedStatus === false){
    echo"no_feed";
    //header('Location: /battery.php?error=no_feed');
    exit();
}

// Delete before ///
if(($delete_battery == 'yes') AND ($battery_id != '')){

        // sql to delete a record
        $sql = "DELETE FROM ess_units WHERE id='$battery_id'";
        if (!mysqli_query($conn, $sql)) {
            header('Location: /battery.php?error=0');
            die('<br>Error: ' . mysqli_error($conn));
        }
        $sql = "DELETE FROM ess_master_info WHERE ess_id='$battery_id'";
        if (mysqli_query($conn, $sql)) {
            header('Location: /battery.php');
            die('exit');
        }
        else {
            header('Location: /battery.php?error=0');
            die('<br>Error: ' . mysqli_error($conn));
        }
}

// basic error checking //


if($model_id == '') {
      $error .= " model_id";
}

if($ip_address == '') {
     $error .= " ip_address";
}
if($mac_address == '') {
     $error .= " mac_address";
}

if ($error != '') {
    header('Location: /battery.php?error=' . $error .  " problem");
    exit();
}

//remove id field if we change table to AUTO_INCREMENT
if( $battery_id == '') {
    $sql = "INSERT INTO ess_units (enabled, name, model_id, ip_address, mac_address, power_regulator_id,meter_id) VALUES('1','$name','$model_id','$ip_address','$mac_address','$power_regulator','$battery_meter_id')";
    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    else {
        if($conn->insert_id != 0){
            $battery_id = $conn->insert_id;
        }
    }
    $sql = "INSERT IGNORE INTO ess_master_info (ess_id) VALUES ('$battery_id')";
}
else {
    $sql = "UPDATE ess_units SET name = '$name', model_id = '$model_id',ip_address='$ip_address', mac_address = '$mac_address', power_regulator_id = '$power_regulator', meter_id = '$battery_meter_id' where id = '$battery_id'";
}
if (!mysqli_query($conn, $sql)) {
    //die('<br>Error: ' . mysqli_error($conn));
}
header('Location: /battery.php?battery_id=' . $battery_id );

?>