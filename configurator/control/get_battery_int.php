<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


if($battery_id == '') {
   // echo "No id";
    $result = $conn->query("SELECT * FROM ess_units");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $battery_id[]=  $row['id'];
            $battery_name[]=  $row['name'];
            $battery_model_id[]=  $row['model_id'];
            $battery_ip_address[]=  $row['ip_address'];
            $battery_mac_address[]=  $row['mac_address'];
            $battery_mode[]=  $row['mode'];
            $battery_power_regulator_id[]=  $row['power_regulator_id'];
            $charge_priority[]=  $row['charge_priority'];
            $discharge_priority[]=  $row['discharge_priority'];
            $meter_id[]=  $row['meter_id'];
        }
    }
}

else {
  // echo "yes id";
    $result = $conn->query("SELECT * FROM ess_units WHERE id = '$battery_id'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $battery_id=  $row['id'];
            $battery_name=  $row['name'];
            $battery_model_id=  $row['model_id'];
            $battery_ip_address=  $row['ip_address'];
            $battery_mac_address=  $row['mac_address'];
            $battery_mode=  $row['mode'];
            $power_regulator_id=  $row['power_regulator_id'];
            $charge_priority=  $row['charge_priority'];
            $discharge_priority=  $row['discharge_priority'];
            $meter_id =  $row['meter_id'];
        }
    }
}



   // $conn->close();

?>