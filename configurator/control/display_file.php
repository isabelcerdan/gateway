<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/14/2016
 * Time: 1:23 PM
 */

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$minion_auditor_id = mysqli_real_escape_string($conn, $_REQUEST['minion_auditor_id']);
$minion_auditor_id = filter_var($minion_auditor_id, FILTER_SANITIZE_URL);


include_once('get_minion_auditor.php'); // Get default data

echo "<strong>FILE:</strong><br> " . $results_path_file_name . "<br><br>";

echo "<strong>Content:</strong><br> ";
echo filter_var(file_get_contents($results_path_file_name, FILTER_SANITIZE_STRING))
?>