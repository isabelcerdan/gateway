<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
$netprotect_email_id = 0;

   $input_name = mysqli_real_escape_string($conn, $_REQUEST['input_name']);
   $input_name = filter_var($input_name, FILTER_SANITIZE_STRING);

    $recipient_name = mysqli_real_escape_string($conn, $_POST['recipient_name']);
   $recipient_name = filter_var($recipient_name, FILTER_SANITIZE_STRING);

    $email = mysqli_real_escape_string($conn, $_POST['email']);
   $email = filter_var($email, FILTER_SANITIZE_STRING);

    $msg_body  = mysqli_real_escape_string($conn, $_POST['msg_body']);
    $msg_body = filter_var($msg_body, FILTER_SANITIZE_STRING);

    $netprotect_email_id  = mysqli_real_escape_string($conn, $_REQUEST['netprotect_email_id']);
    $netprotect_email_id = filter_var($netprotect_email_id, FILTER_VALIDATE_INT);

    $delete_user = mysqli_real_escape_string($conn, $_REQUEST['delete_user']);
   $delete_user = filter_var($delete_user, FILTER_SANITIZE_STRING);

    if(($delete_user == 'yes') AND ($netprotect_email_id > 0)) {
        // sql to delete a record
        $sql = "DELETE FROM  netprotect_email where netprotect_email_id = '$netprotect_email_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: /net_protect_users.php?errors=user_deleted_success');
            die('exit');
        }
        else {
            header('Location: /net_protect_users.php?errors=user_deleted_fail');
            die('<br>Error: ' . mysqli_error($conn));
        }

    }

    if($input_name== '') {
        $error = $error . " input_nameError";
    }

    if($recipient_name  == '') {
    $error = $error . " recipient_name";
    }

    // confirm email is correct format
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = $error . " email";
    }

if($msg_body  == '') {
    $error = $error . " msg_body";
}

if ($error != ''){
    header('Location: /net_protect_users.php?errors=' . $error);
    exit();
}
// check to see if this is an update or not
if($netprotect_email_id == 0) {

    $sql = "INSERT INTO netprotect_email (input_name,recipient_name,email_address,msg_body)
  values('$input_name', '$recipient_name', '$email','$msg_body');
";
    if (!mysqli_query($conn,$sql)) {
        header('Location: /net_protect_users.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
        die('<br>Error: ' . mysqli_error($conn));
    }

}else {

   $sql = "UPDATE netprotect_email set input_name = '$input_name',recipient_name = '$recipient_name',email_address= '$email',msg_body = '$msg_body' WHERE netprotect_email_id = '$netprotect_email_id' ";


    if (!mysqli_query($conn,$sql)) {
        header('Location: /net_protect_users.php?minion_auditor_id=' . $minion_auditor_id);
        die('<br>Error: ' . mysqli_error($conn));
    }
}
header('Location: /net_protect_users.php');
?>