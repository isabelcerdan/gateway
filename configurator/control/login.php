<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */


include_once ('../functions/mysql_connect.php');
// clear error var
$error = '';

$email  = mysqli_real_escape_string($conn, $_POST['email']);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$password = mysqli_real_escape_string($conn, $_POST['password']);
$password = filter_var($password, FILTER_SANITIZE_STRING);

// Get user info by email
$result = $conn->query("SELECT * FROM users where email = '$email'");

while($row = $result->fetch_assoc()) {
        $emailDB = $row['email'];
        $password_salt  = $row['password_salt'];
        $password_hash_DB  = $row['password_hash'];
        $role  = $row['role'];
        $privileges =  $row['privileges'];
}
/*
if(($role != 'config') OR ($role != 'both')) {
    header('Location: /index.php?errors=passwordproblems role');
}
*/

if ($role == 'gui') {
    header('Location: /index.php?errors=passwordproblems role');
    exit();
} elseif($role == 'config'){
    // OK
}
elseif($role == 'both'){
    // OK
}else {
    header('Location: /index.php?errors=passwordproblems role');
    exit();
}
// create hash from user input
$password_hash   = hash('sha256', $password . $password_salt);

// check user hash from db hash
if($password_hash_DB == $password_hash ) {
    $_SESSION["login"] = "logged_yes";
    $_SESSION["privileges"] = $privileges;
  header('Location: /main.php' . $error);
}else {
    $_SESSION["login"] = "logged_no";
   header('Location: /index.php?errors=passwordproblems');
}

//mysqli_close($con);



?>