<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');
include_once ('get_brainboxes.php'); // Get default data
include_once ('../functions/brainbox_config.php');

$target_interface = mysqli_real_escape_string($conn, $_REQUEST['target_interface']);
$target_interface = filter_var($target_interface, FILTER_SANITIZE_STRING);

//$nickname = mysqli_real_escape_string($conn, $_POST['nickname']);
//$nickname = filter_var($nickname, FILTER_SANITIZE_STRING);

/////////////////////////////////////////////////
// Nickname is now the same as target_interface
/////////////////////////////////////////////////

if($target_interface == '/dev/pcc'){
    $nickname = 'PCC';
}elseif($target_interface == '/dev/pcc2'){
    $nickname = 'PCC2';
}elseif($target_interface == '/dev/solar'){
    $nickname = 'Solar';
}elseif($target_interface == '/dev/battery'){
    $nickname = 'Battery';
}else{
    $nickname = '';
}

$delete_brain = mysqli_real_escape_string($conn, $_REQUEST['delete_brain']);
$delete_brain = filter_var($delete_brain, FILTER_SANITIZE_STRING);

$brainbox_id = mysqli_real_escape_string($conn, $_REQUEST['brainbox_id']);
$brainbox_id = filter_var($brainbox_id, FILTER_SANITIZE_NUMBER_INT);

// Delete before ///
if(($delete_brain == 'yes') AND ($brainbox_id != '')){

        // sql to delete a record
        $sql = "DELETE FROM brainboxes WHERE brainbox_id='$brainbox_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: /brainboxes.php');
            die('exit');
        }
        else {
            header('Location: /brainboxes.php?error=0');
            die('<br>Error: ' . mysqli_error($conn));
        }
}

$dual_status = mysqli_real_escape_string($conn, $_POST['dual_status']);
$dual_status = filter_var($dual_status, FILTER_SANITIZE_STRING);

$model_number = mysqli_real_escape_string($conn, $_POST['model_number']);
$model_number = filter_var($model_number, FILTER_SANITIZE_STRING);

$apparent_part = mysqli_real_escape_string($conn, $_POST['apparent_part']);
$apparent_part = filter_var($apparent_part, FILTER_SANITIZE_STRING);

$IP_Address = mysqli_real_escape_string($conn, $_POST['IP_Address']);
$IP_Address = filter_var($IP_Address, FILTER_SANITIZE_STRING);

$mac_address = mysqli_real_escape_string($conn, $_POST['mac_address']);
$mac_address = filter_var($mac_address, FILTER_SANITIZE_STRING);

$baud = mysqli_real_escape_string($conn, $_POST['baud']);
$baud = filter_var($baud, FILTER_SANITIZE_STRING);

$tcp_port = mysqli_real_escape_string($conn, $_POST['tcp_port']);
$tcp_port = filter_var($tcp_port, FILTER_SANITIZE_STRING);
$tcp_port = filter_var($tcp_port, FILTER_SANITIZE_STRING);

$tcp_port_serial_1 = mysqli_real_escape_string($conn, $_POST['tcp_port_serial_1']);
$tcp_port_serial_1 = filter_var($tcp_port_serial_1, FILTER_SANITIZE_STRING);

$tcp_port_serial_2 = mysqli_real_escape_string($conn, $_POST['tcp_port_serial_2']);
$tcp_port_serial_2 = filter_var($tcp_port_serial_2, FILTER_SANITIZE_STRING);

// basic error checking //

// Check to make sure that target_interface isn't repeated //

    for($i = 0;$i<count($target_interfaceDB); $i++ ) {
        if($target_interfaceDB[$i] == $target_interface ){
          $error .= " target_interface_duplicate";
        }
    }


if($target_interface == '') {
      $error .= " target_interface";
}


if($IP_Address == '') {
     $error .= " IP_Address";
}

if($mac_address == '') {
      $error .= " mac_address";
}
if($baud == '') {
     $error .= " baud";
}
if( ($tcp_port == '')){
      $error .= " tcp_port";
}

if ($error != '') {
    header('Location: /brainboxes-add-edit.php?brainboxes=' .$target_interface .'&dual_status=' . $dual_status . '&errors=' . $error .  " problem");
    exit();
}

$config = array(
    'mac_address' => $mac_address,
    'ip_address' => $IP_Address,
    'tcp_port' => $tcp_port,
    'baud' => $baud
);
$auto_config_error = brainbox_config($config);

$sql = "INSERT INTO brainboxes (brainbox_id, target_interface,nickname,model_number,apparent_part,IP_Address,mac_address,baud,tcp_port,tcp_port_serial_1,tcp_port_serial_2)
  values('$brainbox_id', '$target_interface','$nickname','$model_number','$apparent_part','$IP_Address','$mac_address','$baud','$tcp_port','$tcp_port_serial_1','$tcp_port_serial_2')
  ON DUPLICATE KEY UPDATE
  brainbox_id = '$brainbox_id', target_interface = '$target_interface',nickname = '$nickname',model_number = '$model_number',apparent_part = '$apparent_part',IP_Address = '$IP_Address',
  mac_address = '$mac_address',baud = '$baud',tcp_port = '$tcp_port', tcp_port_serial_1 = '$tcp_port_serial_1',tcp_port_serial_2 = '$tcp_port_serial_2'
";


if (!mysqli_query($conn,$sql)) {
    header('Location: /brainboxes-add-edit.php?brainboxes=' .$target_interface);
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if($conn->insert_id != 0){
        $brainbox_id = $conn->insert_id;
    }
   // mysqli_close($con);

    if ($auto_config_error) {
        $error .= "&error=auto-config";
    }

    if($dual_status == 1) {
        header('Location: /brainboxes-add-edit.php?brainbox_id=' . $brainbox_id .'&dual_status=' . $dual_status .'&model=' . $model_number . $error);
        exit();
    }
    header('Location: /success_brainboxes.php?brainbox_id=' . $brainbox_id . $error);

}

?>