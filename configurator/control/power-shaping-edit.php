<?php
/**
 * Created by IntelliJ IDEA.
 * User: Patrice
 * Date: 2/9/2018
 * Time: 9:32 PM
 */

include_once ('../functions/mysql_connect.php');

$slope = floatval($_POST['slope']);
$slope = filter_var($slope, FILTER_SANITIZE_STRING);

$knee = floatval($_POST['knee']);
$knee = filter_var($knee, FILTER_SANITIZE_STRING);

$violation_response = floatval($_POST['violation_response']);
$violation_response = filter_var($violation_response, FILTER_SANITIZE_STRING);

$sql = "UPDATE gateway SET ramp_rate_slope = " . $slope . ", ramp_rate_knee = " . $knee;
$sql .= ", violation_response_factor = " . $violation_response;
$result = mysqli_query($conn, $sql);

$data = array('slope' => $slope, 'knee' => $knee, 'violation_response' => $violation_response);
echo json_encode($data);