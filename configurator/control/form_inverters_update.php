
<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
//include_once ('../functions/session.php');
//nclude_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$tableName= 'inverters';

    $mac_address = mysqli_real_escape_string($conn, $_POST['mac_address']);
    $mac_address = filter_var($mac_address, FILTER_SANITIZE_STRING);

    $serialNumber = mysqli_real_escape_string($conn, $_POST['serialNumber']);
    $serialNumber = filter_var($serialNumber, FILTER_SANITIZE_STRING);

    $FEED_NAME = mysqli_real_escape_string($conn, $_REQUEST['FEED_NAME']);
    $FEED_NAME = filter_var($FEED_NAME, FILTER_SANITIZE_STRING);

    $controlledByGateway = mysqli_real_escape_string($conn, $_POST['controlledByGateway']);
    $controlledByGateway = filter_var($controlledByGateway, FILTER_SANITIZE_STRING);

    $multi = mysqli_real_escape_string($conn, $_REQUEST['multi']);
    $multi = filter_var($multi, FILTER_SANITIZE_STRING);

    $inverters_id = mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
    $inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);

    $dataType = mysqli_real_escape_string($conn, $_REQUEST['dataType']);
    $dataType = filter_var($dataType, FILTER_SANITIZE_STRING);

    $deleteAll = mysqli_real_escape_string($conn, $_REQUEST['deleteAll']);
    $deleteAll = filter_var($deleteAll , FILTER_SANITIZE_STRING);

    $delete = mysqli_real_escape_string($conn, $_REQUEST['delete']);
    $delete = filter_var($delete, FILTER_SANITIZE_STRING);



        $inverters = explode(",",$inverters_id);
        foreach($inverters as $key => $inverter) {
            $inverterArray[$key] = "'$inverter'";
        }
        $inverterArray = join(', ',  $inverterArray);

    if($dataType == 'stringSave'){
        $tableName = 'saved_inverters';
    }

if ($delete  == 'yes'){
        include_once ('/usr/share/apparent/www/configurator/functions/deleteLeases.php');

        $result = $conn->query("SELECT mac_address  FROM inverters WHERE inverters_id IN ($inverterArray)");

        if(mysqli_num_rows($result)>0) {

            while ($row = $result->fetch_assoc()) {
                $mac_addresses[] = $row['mac_address'];
            }

        }


    $sql = " DELETE FROM $tableName  WHERE inverters_id IN ($inverterArray)";
    if (mysqli_query($conn,$sql)) {

    }
    else {
        header('Location: /inverters_list.php??delete=fail');
        die('<br>Error: ' . mysqli_error($conn));
    }

    deleteLease($mac_addresses);
    header('Location: /inverters_list.php?delete=success');
    die('exit');

}

if($FEED_NAME != '' ) {

    if($inverters_id != ''){
        $where = "WHERE inverters_id IN ($inverterArray)";
    }else {
        $where = "WHERE mac_address='$mac_address'";
    }
    
    $sql = "UPDATE inverters set FEED_NAME='$FEED_NAME', controlledByGateway='YES'" . $where;

    $result = $conn->query("SELECT * FROM inverters " . $where);
    while ($row = $result->fetch_assoc()) {
        $IP_Address[] = $row['IP_Address'];
        $FEED_NAME_Current[] = $row['FEED_NAME'];
    }

    for ($i = 0; $i < count($IP_Address); $i++) {
        if ($FEED_NAME_Current[$i] != $FEED_NAME) {
            $feedOutput = $IP_Address[$i] . " " . $FEED_NAME_Current[$i];
            exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py  set_aif_settings feed $FEED_NAME " . $IP_Address[$i] . "", $outcome, $status);
        }
    }
}
if (mysqli_query($conn, $sql)) {
   //header('Location: /inverters_list.php?rows=' .  mysqli_affected_rows($conn) );
} else {
    //header('Location: /inverters_list.php?errors=' .  $error .  " problem " . mysqli_error($conn) );
}

exit();

?>