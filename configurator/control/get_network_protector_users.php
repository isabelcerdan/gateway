<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 8/31/2017
 * Time: 2:01 PM
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

if($netprotect_email_id == '') {

    $result = $conn->query("select * from netprotect_email");

    $num_rows = mysqli_num_rows($result);

    if($num_rows  > 0) {

        while($row = $result->fetch_assoc()) {
            $netprotect_email_id[] =  $row['netprotect_email_id'];
            $input_name[] =  $row['input_name'];
            $recipient_name[] =  $row['recipient_name'];
            $email_address[] =  $row['email_address'];
            $msg_body[] =  $row['msg_body'];
        }
    }
} else {

    $result = $conn->query("select * from netprotect_email WHERE netprotect_email_id = $netprotect_email_id");

    $num_rows = mysqli_num_rows($result);

    if($num_rows  > 0) {

        while($row = $result->fetch_assoc()) {
          $input_name =  $row['input_name'];
          $recipient_name =  $row['recipient_name'];
          $email_address =  $row['email_address'];
          $msg_body =  $row['msg_body'];
        }
    }
}

