<?php
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

// Access restriction
include_once ('../functions/restrict_privilage_access.php');

$error = '';
$page_perm_id = mysqli_real_escape_string($conn, $_REQUEST['page_perm_id']);
$page_perm_id = filter_var($page_perm_id, FILTER_SANITIZE_STRING);

$delete_page = mysqli_real_escape_string($conn, $_REQUEST['delete_page']);
$delete_page= filter_var($delete_page, FILTER_SANITIZE_STRING);

$pageURL= mysqli_real_escape_string($conn, $_POST['pageURL']);
$pageURL = filter_var($pageURL, FILTER_SANITIZE_STRING);

// Add slash if not in variable
if (substr($pageURL, 0, 1) === '/') {
   // do nothing
}else {
    $pageURL = "/" . $pageURL;
}

$access_level = mysqli_real_escape_string($conn, $_POST['access_level']);
$access_level = filter_var($access_level, FILTER_SANITIZE_STRING);
$site = mysqli_real_escape_string($conn, $_POST['site']);
$site = filter_var($site, FILTER_SANITIZE_STRING);
$notes = mysqli_real_escape_string($conn, $_POST['notes']);
$notes = filter_var($notes, FILTER_SANITIZE_STRING);

// Delete before ///
if(($delete_page == 'yes') AND ($page_perm_id != '')){

        // sql to delete a record
        $sql = "DELETE FROM page_permissions WHERE page_perm_id='$page_perm_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: /page_permissions.php');
            die('exit');
        }
        else {
            header('Location: /page_permissions.php?error=0');
            die('<br>Error: ' . mysqli_error($conn));
        }

}

// basic error checking //

if($pageURL == '') {
      $error = $error . " pageURL";
}
if($access_level == '') {
     $error = $error . " access_level";
}
if($site == '') {
     $error = $error . " site";
}
if ($error != '') {
   // echo "FAILURE!!! ";
    header('Location: /page_permissions.php?brainboxes=' .$target_interface .'&dual_status=' . $dual_status . '&errors=' . $error .  " problem");
    exit();
}

if($page_perm_id =='') {
// prepare and bind
//$stmt = $conn->prepare("INSERT INTO energy_meter (meter_name,meter_role,meter_class,address,baud,network_comm_type,feed_of_phase_A,feed_of_phase_B,feed_of_phase_C)  VALUES (?,?,?,?,?,?,?,?,?,?)");
    $stmt = $conn->prepare("INSERT INTO page_permissions (pageURL,access_level,site, notes)  VALUES (?,?,?,?)");
    $stmt->bind_param("ssss",$pageURL,$access_level,$site, $notes );
}else{
//echo "Meter ID $meter_id";

    $stmt = $conn->prepare("UPDATE page_permissions set pageURL=?,access_level=?,site=?,notes=? WHERE page_perm_id=?");
    $stmt->bind_param("sssss", $pageURL,$access_level,$site,$notes, $page_perm_id );

    if ($stmt->errno) {
        // echo "FAILURE!!! " . $stmt->error;
        header('Location: /page_permissions.php?errors=' .  $error .  " problem " . $stmt->error);
    }
    else echo "Updated {$stmt->affected_rows} rows";
}

$stmt->execute();
$id = $stmt->insert_id;
if (($id == 0) || ($id == '')) {
    $id =  $meter_id;
}

if($stmt->affected_rows > 0) {
    $stmt->close();
    $conn->close();
    header('Location: /page_permissions.php');
}
else {
    printf("Error: %s.\n", mysqli_stmt_error($stmt) . mysqli_error($conn));
    // exit();
    $stmt->close();
    //$conn->close();
    header('Location: /page_permissions.php?errors=' . $error = $error .  " no_affected_rows " .  mysqli_error($conn));
}

?>