<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 11/14/2016
 * Time: 11:57 AM
 *
 *
http://52.8.194.162:8180/AcquiSuite/servlet/gatewayload.php?Reactive_Energy_Quadrant_4=15.971&SerialNumber=Gateway01&Reactive_Energy_Quadrant_1=2.762&Reactive_Energy_Quadrant_2=25.878&
Reactive_Energy_Quadrant_3=656.147&modbusdevicenumber=1&Total_Apparent_Power_Present_Demand=1.263&Total_Real_Power_Present_Demand
=-1.23&Apparent_Energy_Net=-1297.93&Total_Reactive_Power_Present_Demand=-0.32&Accumulated_Real_Energy_Net=-1042.48
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$brainbox_id = mysqli_real_escape_string($conn, $_REQUEST['brainbox_id']);
$brainbox_id = filter_var($brainbox_id, FILTER_SANITIZE_STRING);

$meter = mysqli_real_escape_string($conn, $_REQUEST['meter']);
$meter = filter_var($meter, FILTER_SANITIZE_STRING);

$meter_id = mysqli_real_escape_string($conn, $_REQUEST['meter_id']);
$meter_id = filter_var($meter_id, FILTER_SANITIZE_STRING);

$meter_class = mysqli_real_escape_string($conn, $_REQUEST['meter_class']);
$deviceclass = filter_var($meter_class, FILTER_SANITIZE_STRING);

$modbusdevicenumber = mysqli_real_escape_string($conn, $_REQUEST['modbusdevicenumber']);
$modbusdevicenumber = filter_var($modbusdevicenumber, FILTER_SANITIZE_STRING);

$B_ER_Connect = mysqli_real_escape_string($conn, $_REQUEST['B_ER_Connect']);
$B_ER_Connect = filter_var($B_ER_Connect, FILTER_SANITIZE_STRING);



//$url = 'http://acq.lc:8180/AcquiSuite/servlet/gatewaysync.php';

$result = $conn->query("SELECT * FROM energy_report_config");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $url = $row['URL_AQ'];
        $SerialNumber = $row['gateway_base_sn'];
    }
}




$saltLarge = '3dlmgrf$cd4';

//$SerialNumber = 'GATE10000019';

// If not a meter make sure that device number and modbus are set to zero 0//
if($meter == 'no' ){
    $deviceclass = '0';
    $modbusdevicenumber = '0';
}else {
    //if a meter, make sure that the brainbox is already registered, if not reject
    if($B_ER_Connect !=1) {
        header('Location: /energyreview_met-brain.php?error=no_brain');
        exit();
    }
}
//echo "brainbox_id: $brainbox_id <br>";
//echo "meter: $meter <br>";
//echo "deviceclass: $deviceclass <br>";
//echo "modbusdevicenumber: $modbusdevicenumber <br>";

//$brainbox_id ='2';

//$SerialNumber = $SerialNumber .'-' .$brainbox_id;

$SerialNumber = preg_replace('/[^a-zA-Z0-9-_]/', '', $SerialNumber);
// clean out any brainbox info//
$serialArray = explode("-",$SerialNumber);
$SerialNumber = $serialArray[0]; 
$modbusdevicenumber = preg_replace('/[^a-zA-Z0-9-_]/', '', $modbusdevicenumber);
$deviceclass = preg_replace('/[^a-zA-Z0-9-_]/', '', $deviceclass);
$SHA = sha1($SerialNumber .  $modbusdevicenumber . $saltLarge . $deviceclass);

$data = array('SerialNumber' => $SerialNumber, 'modbusdevicenumber' => $modbusdevicenumber, 'SHA' => $SHA, 'deviceclass' =>$deviceclass, 'brainbox_id' => $brainbox_id);


// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);

//echo $result;
if ($result === FALSE) { /* Handle error */ }

// Clean $result:
$result = preg_replace('/[^a-zA-Z0-9-_:]/', '', $result);

if (strpos($result, 'Success:') !== false) {
    $serial = explode("Success:",$result);
    $serialArray = explode("-",$serial);

    $sql = "UPDATE energy_report_config SET gateway_base_sn = '$serial[1]' ";
        if (!mysqli_query($conn,$sql)) {
            die('<br>Error: ' . mysqli_error($conn));
        }
    if($meter == 'no' ){
        echo "brian yes __ $brainbox_id ___";
        $sql2 = "UPDATE brainboxes SET ER_Connect = '1' WHERE brainbox_id = '$brainbox_id'";
        if (!mysqli_query($conn,$sql2)) {
            die('<br>Error: ' . mysqli_error($conn));
        }
    }

    if($meter == 'yes' ){
        echo "meter yes ___ $meter_id ___";
        $sql2 = "UPDATE energy_meter SET ER_Connect = '1' WHERE meter_id = '$meter_id'";
        if (!mysqli_query($conn,$sql2)) {
            die('<br>Error: ' . mysqli_error($conn));
        }
    }
    header('Location: /energyreview_met-brain.php?Success');
    exit();
}
header('Location: /energyreview_met-brain.php?error=Failure ' . $result);

var_dump($result);


/*
 * 
$url = 'http://52.8.194.162:8180/AcquiSuite/servlet/gatewayload.php?';
$data = array('SerialNumber' => 'Gateway01', 'Reactive_Energy_Quadrant_1' => '2.762');

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) {  }

var_dump($result);
*/