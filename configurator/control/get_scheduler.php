<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');

$task_id = filter_var($task_id, FILTER_SANITIZE_STRING);
$ids = explode('-', $task_id);
include_once ('../functions/mysql_connect.php');

if($task_id == '') {
 // echo "No id";
    $result = $conn->query("select tasks.id AS task_id, recurrences.id AS recurrence_id, name,start_cmd, start_date, enabled, period, unit, mo,tu,we,th,fr,sa,su, ends, ends_on, ends_after 
from tasks left outer join recurrences  ON  recurrences.id = tasks.recurrence_id;");

    if(mysqli_num_rows($result)>0) {
        while($row = $result->fetch_assoc()) {
            $task_id[] =  $row['task_id'];
            $recurrence_id[] =  $row['recurrence_id'];
            $name[] =  $row['name'];
            $start_cmd[] =  $row['start_cmd'];
            $start_date[] =  $row['start_date'];
            $enabled[] =  $row['enabled'];
            $period[] =  $row['period'];
            $unit[] =  $row['unit'];
            $mo[] =  $row['mo'];
            $tu[] =  $row['tu'];
            $we[] =  $row['we'];
            $th[] =  $row['th'];
            $fr[] =  $row['fr'];
            $sa[] =  $row['sa'];
            $su[] =  $row['su'];
            $ends[] =  $row['ends'];
            $ends_on[] =  $row['ends_on'];
            $ends_after[] =  $row['ends_after'];
        }
    }
}else {


    $result = $conn->query("select tasks.id AS task_id, recurrences.id AS recurrence_id, name,start_cmd, start_date, enabled, period, unit, mo,tu,we,th,fr,sa,su, ends, ends_on, ends_after 
from tasks left outer join recurrences  ON  recurrences.id = tasks.recurrence_id WHERE tasks.id = $ids[0]");

    if(mysqli_num_rows($result)>0) {
        while($row = $result->fetch_assoc()) {
            $task_idDB =  $row['task_id'];
            $recurrence_idDB =  $row['recurrence_id'];
            $nameDB =  $row['name'];
            $start_cmdDB =  $row['start_cmd'];
            $start_dateDB =  $row['start_date'];
            $enabledDB =  $row['enabled'];
            $periodDB =  $row['period'];
            $unitDB =  $row['unit'];
            $moDB =  $row['mo'];
            $tuDB =  $row['tu'];
            $weDB =  $row['we'];
            $thDB =  $row['th'];
            $frDB =  $row['fr'];
            $saDB =  $row['sa'];
            $suDB =  $row['su'];
            $endsDB =  $row['ends'];
            $ends_onDB =  $row['ends_on'];
            $ends_afterDB =  $row['ends_after'];
        }
    }

}
 
?>