<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/26/2016
 * Time: 3:56 PM
 */

include_once ('../functions/session.php');
$page = mysqli_real_escape_string($conn, $_REQUEST['p']);
$page = filter_var($page, FILTER_SANITIZE_NUMBER_INT);
$search = mysqli_real_escape_string($conn, $_REQUEST['search']);
$search = filter_var($search, FILTER_SANITIZE_STRING);
$orderby = mysqli_real_escape_string($conn, $_REQUEST['orderby']);
$orderby = filter_var($orderby, FILTER_SANITIZE_STRING);
$sortDirection = mysqli_real_escape_string($conn, $_REQUEST['sortDirection']);
$sortDirection = filter_var($sortDirection, FILTER_SANITIZE_STRING);
$itemsperpage = mysqli_real_escape_string($conn, $_REQUEST['itemsperpage']);
$itemsperpage = filter_var($itemsperpage, FILTER_SANITIZE_NUMBER_INT);
$sortDirectionDefault = 'DESC';
if (is_numeric($itemsperpage))   {
    $limit = $itemsperpage;
}
else {
    $limit=32;
}
if($page=='')
{
    $page=1;
    $start=0;
}
else
{
    $start=$limit*($page-1);
}
// check search value exist
if( !empty($search) ) {
    // reset page numbers //
    $page=1;
    $start=0;
    $where .=" WHERE ";
    $where .=" ( mac_address LIKE '".$search."%' ";
    $where .=" OR serialNumber LIKE '".$search."%' ";
    $where .=" OR IP_Address LIKE '".$search."%' ";
    $where .=" OR controlledByGateway LIKE '".$search."%' ";
    $where .=" OR FEED_NAME LIKE '".$search."%' )";
}
if( !empty($orderby) ) {
    //$page=1;
    //$start=0;
    $order = "ORDER BY " . $orderby;
    if($sortDirection == 'DESC') {
        $order = $order . " " . $sortDirection;
        $sortDirectionDefault = 'ASC';
    }
    if($sortDirection == 'ASC')  {
        $order = $order . " " . $sortDirection;
        $sortDirectionDefault = 'DESC';
    }
}
$result = $conn->query("SELECT * FROM inverters $where  $order limit $start, $limit");
//$query=mysql_query("select * from table1 limit $start, $limit") or die(mysql_error());
$tot = $conn->query("SELECT * FROM inverters");
$total=mysqli_num_rows($tot);
$num_page=ceil($total/$limit);
while($row = $result->fetch_assoc()) {
    $inverters_id[] = $row['inverters_id'];
    $serialNumber[] = $row['serialNumber'];
    $group_id[] = $row['group_id'];
    $mac_address[] =  $row['mac_address'];
    $IP_Address[] =  $row['IP_Address'];
    $version[] =  $row['version'];
    $label[] =  $row['label'];
    $stringId[] = $row['stringId'];
    $stringPosition[] =  $row['stringPosition'];
    $export_status[] =  $row['export_status'];
    $prodPartNo[] =  $row['prodPartNo'];
    $output_status[] =  $row['output_status'];
    $inverter_lock =$row['inverter_lock'];
    $status[] =  $row['status'];
    $resets[] =$row['resets'];
    $comm[] =  $row['comm'];
    $pf[] = $row['pf'];
    $pf_c_lead[] =$row['pf_c_lead'];
    $pf_enable[] =$row['pf_enable'];
    $controlledByGateway[] =  $row['controlledByGateway'];
    $FEED_NAME_inverter[] =  $row['FEED_NAME'];
    $status[] =  $row['status'];
    $upgrade_status[] =  $row['upgrade_status'];
    $watts[] =  $row['watts'];
}

?>