<?php




/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/17/2018
 * Time: 2:40 PM
 */


// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';
$group_id= mysqli_real_escape_string($conn, $_REQUEST['group_id']);
$group_id = filter_var($group_id, FILTER_SANITIZE_STRING);

$delete= mysqli_real_escape_string($conn, $_REQUEST['delete']);
$delete = filter_var($delete, FILTER_SANITIZE_STRING);

// Delete before ///
if (($delete== 'yes') AND ($group_id != '')) {
    // sql to delete a record
    $sql = "DELETE FROM groups WHERE group_id ='$group_id'";
    if (!mysqli_query($conn, $sql)) {
        header('Location: /groups.php?error=DB' . mysqli_error($conn));
        die('<br>Error: ' . mysqli_error($conn));
    }
    
    header('Location: /groups.php?deleted');
    die('exit');
}

$alias  = mysqli_real_escape_string($conn, $_POST['alias']);
$alias = filter_var($alias, FILTER_SANITIZE_STRING);

$location  = mysqli_real_escape_string($conn, $_POST['location']);
$location = filter_var($location, FILTER_SANITIZE_STRING);


$fallback = mysqli_real_escape_string($conn, $_POST['fallback']);
$fallback = filter_var($fallback, FILTER_SANITIZE_STRING);

$ntp_url_sg424 = mysqli_real_escape_string($conn, $_POST['ntp_url_sg424']);
$ntp_url_sg424= filter_var($ntp_url_sg424, FILTER_SANITIZE_STRING);

$forced_upgrade = mysqli_real_escape_string($conn, $_POST['forced_upgrade']);
$forced_upgrade= filter_var($forced_upgrade, FILTER_SANITIZE_STRING);

$irradiance= mysqli_real_escape_string($conn, $_POST['irradiance']);
$irradiance= filter_var($irradiance, FILTER_SANITIZE_STRING);


$gateway_control = mysqli_real_escape_string($conn, $_POST['gateway_control']);
$gateway_control = filter_var($gateway_control, FILTER_SANITIZE_STRING);

$sleep  = mysqli_real_escape_string($conn, $_POST['sleep']);
$sleep = filter_var($sleep, FILTER_SANITIZE_STRING);

$notes  = mysqli_real_escape_string($conn, $_POST['notes']);
$notes = filter_var($notes, FILTER_SANITIZE_STRING);


$er_server_http = mysqli_real_escape_string($conn, $_POST['er_server_http']);
$er_server_http = filter_var($er_server_http, FILTER_SANITIZE_STRING);

$er_gtw_tlv = mysqli_real_escape_string($conn, $_POST['er_gtw_tlv']);
$er_gtw_tlv = filter_var($er_gtw_tlv, FILTER_SANITIZE_STRING);


$sql = "INSERT INTO groups (group_id,alias,location,notes,fallback,ntp_url_sg424,forced_upgrade,irradiance, gateway_control,er_server_http,er_gtw_tlv,sleep)
  values('$group_id', '$alias','$location','$notes', '$fallback','$ntp_url_sg424','$forced_upgrade', '$irradiance', '$gateway_control', '$er_server_http','$er_gtw_tlv','$sleep')
 ON DUPLICATE KEY UPDATE
  group_id ='$group_id', alias='$alias',location='$location',notes='$notes',fallback='$fallback',ntp_url_sg424='$ntp_url_sg424',forced_upgrade ='$forced_upgrade',irradiance= '$irradiance', gateway_control='$gateway_control',
  er_server_http='$er_server_http',er_gtw_tlv='$er_gtw_tlv',sleep='$sleep'
  
";

if (!mysqli_query($conn,$sql)) {
    //header('Location: /meter-add-edit.php?errors=' . $error .  " problem");
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if ($conn->insert_id != 0) {
        $group_id= $conn->insert_id;
    }

    $sqlUpdate = "UPDATE inverters SET group_audit_required = 1 WHERE group_id ='$group_id'";
    if (!mysqli_query($conn,$sqlUpdate)) {
        //header('Location: /meter-add-edit.php?errors=' . $error .  " problem");
        // die('<br>Error: ' . mysqli_error($conn));
    }
}
header('Location: /groups.php?group_id=' .$group_id);

?>