<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM


CESS_40_UNIT_WORK_STATE = "unit_work_state"
CESS_40_UNIT_CTRL_MODE = "unit_ctrl_mode"
CESS_40_UNIT_WORK_MODE = "unit_work_mode"
 * active_power_cmd_setting
 * active_power_cmd_setting
 */

include_once ('../functions/mysql_connect.php');

$resultMonitor = $conn->query("SELECT enabled FROM daemon_control where name = 'esscontrollerd'");

if(mysqli_num_rows($resultMonitor)>0) {

    while($row = $resultMonitor->fetch_assoc()) {
        $cess_controller_control_state =  $row['enabled'];
    }
}

$result = $conn->query(" SELECT * FROM meter_readings WHERE meter_role = 'battery' AND data_type = 1");

if(mysqli_num_rows($resultMonitor)>0) {
    while ($row = $result->fetch_assoc()) {
        $Export_Battery_1_Delta = number_format((float)$row['Total_Net_Instantaneous_Real_Power_FP'], 3, '.', '');
    }
}
$resultNXO = $conn->query(" SELECT * FROM nxo_status ORDER BY Update_Time LIMIT 1");

if(mysqli_num_rows($resultMonitor)>0) {
    while ($row = $resultNXO->fetch_assoc()) {
        $Export_PCC_1_Delta = number_format((float)$row['Export_PCC_1_Delta'], 3, '.', '');
        $Export_PCC_2_Delta = number_format((float)$row['Export_PCC_2_Delta'], 3, '.', '');
        $Export_PCC_Combo_Delta = number_format((float)$row['Export_PCC_Combo_Delta'], 3, '.', '');

        $Curtail_PCC_Combo_A = $row['Curtail_PCC_Combo_A'];
        $Export_PCC_Combo_A = number_format((float)$row['Export_PCC_Combo_A'], 3, '.', '');
        $PCC_Combo_A_Threshold = $row['PCC_Combo_A_Threshold'];

        $Curtail_PCC_Combo_B = $row['Curtail_PCC_Combo_B'];
        $Export_PCC_Combo_B = number_format((float)$row['Export_PCC_Combo_B'], 3, '.', '');
        $PCC_Combo_B_Threshold = $row['PCC_Combo_B_Threshold'];

        $Curtail_PCC_Combo_C = $row['Curtail_PCC_Combo_C'];
        $Export_PCC_Combo_C = number_format((float)$row['Export_PCC_Combo_C'], 3, '.', '');
        $PCC_Combo_C_Threshold = $row['PCC_Combo_C_Threshold'];
    }
}

$resultAlarm = $conn->query("SELECT enabled,soc_charge_limit,soc_discharge_limit  FROM ess_units");

if(mysqli_num_rows($resultAlarm)>0) {

    while($row = $resultAlarm->fetch_assoc()) {
        $soc_charge_limit =  $row['soc_charge_limit'];
        $soc_discharge_limit =  $row['soc_discharge_limit'];
    }
}
$soc_charge_limit = $soc_charge_limit * 100;
$soc_discharge_limit = $soc_discharge_limit * 100;

    $sql = $conn->query("SELECT *  FROM ess_master_info ");
    $results = array();

    while($row = $sql->fetch_assoc())
    {
        $results[] = array(

            'unit_ctrl_mode' =>  $row['unit_ctrl_mode'],
            'unit_work_state' =>  $row['unit_work_state'],
            'unit_work_mode' =>$row['unit_work_mode'],
            'reconnect_state' => $row['reconnect_state'],
            'battery_string_running' => $row['battery_string_running'],
            'battery_string_enabled' => $row['battery_string_enabled'],
            'battery_string_isolated' => $row['battery_string_isolated'],
            'battery_stack_voltage' => $row['battery_stack_voltage'],
            'battery_stack_current' => $row['battery_stack_current'],
            'battery_stack_power' => $row['battery_stack_power'],
            'battery_stack_soc' => number_format($row['battery_stack_soc'],2),
            'battery_stack_soh' => number_format($row['battery_stack_soh'],2),
            'battery_max_cell_voltage' => $row['battery_max_cell_voltage'],
            'battery_min_cell_voltage' => $row['battery_min_cell_voltage'],
            'battery_max_cell_temperature' => $row['battery_max_cell_temperature'],
            'battery_min_cell_tempreature' => $row['battery_min_cell_tempreature'],
            'dc_voltage' => $row['dc_voltage'],
            'dc_current' => $row['dc_current'],
            'dc_power' => $row['dc_power'],
            'p_value' => $row['p_value'],
            'q_value' => $row['q_value'],
            's_value' => $row['s_value'],
            'ia_value' => $row['ia_value'],
            'ib_value' => $row['ib_value'],
            'ic_value' => $row['ic_value'],
            'uab_value' => $row['uab_value'],
            'ubc_value' => $row['ubc_value'],
            'uac_value' => $row['uac_value'],
            'ua_value' => $row['ua_value'],
            'ub_value' => $row['ub_value'],
            'uc_value' => $row['uc_value'],
            'frequency' =>  $row['frequency'],
            'power_factor' =>  $row['power_factor'],
            'work_state_control_cmd' =>  $row['work_state_control_cmd'],
            'active_power_cmd' =>  $row['active_power_cmd'],
            'reactive_power_cmd' =>  $row['reactive_power_cmd'],
            'voltage_control_cmd' => $row['voltage_control_cmd'],
            'frequency_control_cmd' =>$row['frequency_control_cmd'],
            'timestamp' => $row['timestamp'],
            'soc_charge_limit' => $soc_charge_limit,
            'soc_discharge_limit' => $soc_discharge_limit,
            'cess_monitor_control_state' => $cess_controller_control_state,
            'apparent_power' => $apparent_power,
            'true_power'=> $true_power,
            ''
        );
    }
echo $json = json_encode($results);

?>