<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once('../functions/session.php');
include_once('../functions/mysql_connect.php');


if ($feed == '') {
    //echo "No id";
    $result = $conn->query("SELECT *  FROM power_regulators ");

    if (mysqli_num_rows($result) > 0) {

        while ($row = $result->fetch_assoc()) {
            $feed_DB[] = $row['feed_name'];
            $enabled_DB[] = $row['enabled'];
            $Bucket_Size_DB[] = $row['bucket_size'];
            $Bucket_Period_DB[] = $row['bucket_period'];
            $Bucket_Enabled_DB[] = $row['bucket_enabled'];
            $Bucket_Fullness_DB[] = $row['bucket_fullness'];
            $Bucket_Coefficient_DB[] = $row['bucket_coefficient'];
            $Reinitialize_Bucket_DB[] = $row['reinitialize_bucket'];
        }
    }

} else {
    $resultFeed = $conn->query("SELECT *  FROM power_regulators WHERE feed_name = '$feed'");
    // Turn into join //
    if (mysqli_num_rows($resultFeed) > 0) {

        while ($row = $resultFeed->fetch_assoc()) {
            $enabled = $row['enabled'];
            $Bucket_Size = $row['bucket_size'];
            $Bucket_Period = $row['bucket_period'];
            $Bucket_Enabled = $row['bucket_enabled'];
            $Bucket_Fullness[] = $row['bucket_fullness'];
            $Bucket_Coefficient[] = $row['bucket_coefficient'];
            $Reinitialize_Bucket[] = $row['reinitialize_bucket'];
        }
    }
}
// $conn->close();

?>