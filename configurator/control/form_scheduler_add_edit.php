<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once('../functions/session.php');
include_once('../functions/restrict_privilage_access.php');

include_once('../functions/mysql_connect.php');

$error = '';
$task_id = mysqli_real_escape_string($conn, $_REQUEST['task_id']);
$task_id = filter_var($task_id, FILTER_SANITIZE_STRING);

$delete_task = mysqli_real_escape_string($conn, $_REQUEST['delete_task']);
$delete_task = filter_var($delete_task, FILTER_SANITIZE_STRING);

$ids = explode('-', $task_id);

$recurrence_id = $ids[1];

/////////////////////////////////////////////////
// Delete whole task from main scheduler page ///
/////////////////////////////////////////////////
if (($delete_task == 'yes') AND ($task_id != '')) {

    //$ids[0] = task ID
    if ($ids[0] != '') {
        $sql = "DELETE FROM tasks WHERE id='$ids[0]'";
        if (mysqli_query($conn, $sql)) {

        } else {
            header('Location: /scheduler.php?failure');
            die('<br>Error: ' . mysqli_error($conn));
        }

        if ($ids[1] != '') {
            $sql2 = "DELETE FROM recurrences WHERE id='$ids[1]'";
            if (mysqli_query($conn, $sql2)) {

            } else {
                header('Location: /scheduler.php?failure');
                die('<br>Error: ' . mysqli_error($conn));
            }
        }

        header('Location: /scheduler.php?deleted');
        die('exit');
    }

}

$taskName = mysqli_real_escape_string($conn, $_POST['taskName']);
$taskName = filter_var($taskName, FILTER_SANITIZE_STRING);

$start_cmd = $_POST['start_cmd'];
$start_cmd = filter_var($start_cmd, FILTER_SANITIZE_STRING);

$startDate = mysqli_real_escape_string($conn, $_POST['startDate']);
$startDate = filter_var($startDate, FILTER_SANITIZE_STRING);

$datestartDate = date_create($startDate);
$datestartDateClean = date_format($datestartDate, 'Y-m-d H:i');

if ($endDate != "") {
    $dateendDate = date_create($endDate);
    echo $dateendDateClean = date_format($dateendDate, 'Y-m-d H:i');
} else {
    $dateendDateClean = 'NULL';
}

$recurring = mysqli_real_escape_string($conn, $_POST['recurring']);
$recurring = filter_var($recurring, FILTER_SANITIZE_STRING);

$enabled = mysqli_real_escape_string($conn, $_POST['enabled']);
$enabled = filter_var($enabled, FILTER_SANITIZE_STRING);

if ($enabled != 1) {
    $enabled = 0;
}

$period = mysqli_real_escape_string($conn, $_POST['period']);
$period = filter_var($period, FILTER_SANITIZE_STRING);

// period can't be less than 1 or it causes errors //
if ($period < 1) {
    $period = 1;
}

$unit = mysqli_real_escape_string($conn, $_POST['unit']);
$unit = filter_var($unit, FILTER_SANITIZE_STRING);


$mo = mysqli_real_escape_string($conn, $_POST['mo']);
$mo = filter_var($mo, FILTER_SANITIZE_STRING);

$tu = mysqli_real_escape_string($conn, $_POST['tu']);
$tu = filter_var($tu, FILTER_SANITIZE_STRING);

$we = mysqli_real_escape_string($conn, $_POST['we']);
$we = filter_var($we, FILTER_SANITIZE_STRING);

$th = mysqli_real_escape_string($conn, $_POST['th']);
$th = filter_var($th, FILTER_SANITIZE_STRING);

$fr = mysqli_real_escape_string($conn, $_POST['fr']);
$fr = filter_var($fr, FILTER_SANITIZE_STRING);

$sa = mysqli_real_escape_string($conn, $_POST['sa']);
$sa = filter_var($sa, FILTER_SANITIZE_STRING);

$su = mysqli_real_escape_string($conn, $_POST['su']);
$su = filter_var($su, FILTER_SANITIZE_STRING);

$ends = mysqli_real_escape_string($conn, $_POST['ends']);
$ends = filter_var($ends, FILTER_SANITIZE_STRING);

$endsOn = mysqli_real_escape_string($conn, $_POST['endsOn']);
$endsOn = filter_var($endsOn, FILTER_SANITIZE_STRING);
$date = date_create($endsOn);
$endsOnClean = date_format($date, 'Y-m-d H:i');

$ends_after = mysqli_real_escape_string($conn, $_POST['ends_after']);
$ends_after = filter_var($ends_after, FILTER_SANITIZE_STRING);


////////////////////////////////////
// if task if recurring task update
//////////////////////////////////////

if ($recurring == 'yes') {

    if ($recurrence_id == '') {
        $sql = "INSERT INTO recurrences (period,unit,mo,tu,we,th,fr,sa,su,ends,ends_on,ends_after)
  values('$period','$unit', '$mo','$tu', '$we','$th','$fr','$sa','$su','$ends','$endsOnClean','$ends_after');";

        if (!mysqli_query($conn, $sql)) {
            die('<br>Error: ' . mysqli_error($conn));
        }
        echo "ID: " . $recurrence_id = $conn->insert_id;
    } else {

        $sql = "UPDATE recurrences set period= '$period',unit= '$unit',mo='$mo',tu='$tu',we='$we',th='$th',fr='$fr',sa='$sa',su='$su',
ends='$ends',ends_on='$endsOnClean',ends_after='$ends_after' WHERE id= '$recurrence_id' ";

        if (!mysqli_query($conn, $sql)) {

            die('<br>Error: ' . mysqli_error($conn));
        }
    }

} elseif (($recurring != 'yes') AND ($recurrence_id != '')) {
    echo "delete";
    $sql2 = "DELETE FROM recurrences WHERE id='$ids[1]'";
    if (mysqli_query($conn, $sql2)) {
        // error //
    }
    $recurrence_id == '';

}
/////////////////////////////////////////////////////////////////
// cleaning recurrence_id from tasks if recurrence is disabled //
/////////////////////////////////////////////////////////////////

if (($recurring != 'yes') AND ($ids[0] != '')) {
    $recurrence_id = '';
}

///////////////////////////////////
// Enter new task or update task //
///////////////////////////////
if ($ids[0] == '') {

    $sql = "INSERT INTO tasks (enabled,name,start_cmd,start_date,recurrence_id)
  values('$enabled','$taskName','$start_cmd','$datestartDateClean', '$recurrence_id');
";
} else {

    $sql = "UPDATE tasks set enabled = '$enabled', name='$taskName', start_cmd='$start_cmd',start_date ='$datestartDateClean', recurrence_id = '$recurrence_id'  WHERE ID = '$ids[0]' ";
}

if (!mysqli_query($conn, $sql)) {

    die('<br>Error: ' . mysqli_error($conn));
} else {
    header('Location: /scheduler.php?success');
}


?>