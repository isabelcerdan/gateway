<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/21/2017
 * Time: 5:26 PM
 */
include_once ('../functions/mysql_connect.php');
include_once ('get_battery.php');
?>
<script type="text/javascript">

    $(document).ready(function (){

        $('.sendCommand').on('click', function() {
            console.log('click me');
            var r = confirm("Turning On or Off the battery can take several minutes. Do you want to continue?");
            if (r == true) {
                var command = '';
                if (this.id == 'stop') {
                    command = 0;
                }
                if (this.id == 'start') {
                    command = 1;
                }
                $.ajax({
                    type: "POST",
                    url: 'control/set_battery_run_stop.php?startStop=' + command,
                    success: function (data) {
                        console.log('data:' + data);
                    },
                    error: function (jqXHR, text, error) {
                        console.log('error:');
                    }
                });
                return false;
            }
        });
        $('.setACTIVE_POWER').on('click', function() {
            value = $("#ACTIVE_POWER").val();
            //console.log('ACTIVE_POWER:'+ value );

            $.ajax({
                type: "POST",
                url: 'control/set_battery_run_stop.php?activePower=yes&level='+value,
                success: function (data) {
                    console.log('data:' + data);
                },
                error: function (jqXHR, text, error) {
                    console.log('error:');
                }
            });
            return false;

        });

        $('.setREACTIVE_POWER').on('click', function() {
            value = $("#REACTIVE_POWER").val();
            //console.log('ACTIVE_POWER:'+ value );
            $.ajax({
                type: "POST",
                url: 'control/set_battery_run_stop.php?reactivePower=yes&level='+value,
                success: function (data) {
                    console.log('data:' + data);
                },
                error: function (jqXHR, text, error) {
                    console.log('error:');
                }
            });
            return false;

        });

        //
    });
</script>
<div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Battery Charge State</h3></div>
                                    <div class="panel-body">
                                        <h4>Battery State:

                                            <?php
                                            // Show battery state / Charging/discharging

                                            if($active_power_cmd_setting > 0) {
                                                echo "<span class='charging'>Charging</span>";
                                            }else {
                                                echo "<span class='discharging'>Discharging</span>";
                                            }

                                            ?>

</h4>

<div class="row">
    <div class="col-md-1"></div>

    <div class="col-md-10">
        <div class="progress">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $battery_string_soc; ?>%">
                <?php echo $battery_string_soc; ?>%
            </div>
        </div>


    </div>

    <div class="col-md-1"></div>
</div>


</div>
</div>
</div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="text-center">Commands</h3></div>
            <div class="panel-body">

                <p>
                    <?php
                    if( $unit_work_state == 'Running'){
                        echo '<strong>Turn off the battery:</strong> <button class="sendCommand" id="stop" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Stop</button>';
                    }else {
                        echo '<strong>Turn on the battery:</strong> <button class="sendCommand" id="start" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Start</button>';
                    }
                    ?>
                </p>
                <p>
                    <strong>Active Power:</strong> <input type="text" id="ACTIVE_POWER" name="ACTIVE_POWER" maxlength="6" size="6"><button class="setACTIVE_POWER" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                    <br><small>(+) Set active power discharge, (-) Set active power charge</small>
                <p>
                    <strong>Reactive Power:</strong> <input type="text" id="REACTIVE_POWER" name="REACTIVE_POWER" maxlength="6" size="6"><button class="setREACTIVE_POWER" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                    <br><small>(+) Provide Reactive, (-) Absorb Reactive</small>
                </p>




            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">

            <div class="panel-heading"><h3 class="text-center">System Status</h3></div>
            <div class="panel-body">

                <p>
                    <strong> System State:</strong>
                </p>
                <p>
                    <strong>work_state:</strong> <?php echo $unit_work_state; ?>
                </p>
                <p>
                    <strong>ctrl_mode:</strong> <?php echo $unit_ctrl_mode; ?>
                </p>
                <p>
                    <strong>work_mode:</strong>  <?php echo $unit_work_mode; ?>
                </p>

                <p>
                    <strong>active_power_cmd_setting:</strong>  <?php echo $active_power_cmd_setting; ?>
                </p>

                <p>
                    <strong>reactive_power_cmd_setting:</strong>  <?php echo $reactive_power_cmd_setting; ?>
                </p>


            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="text-center">Battery String Overview</h3></div>
            <div class="panel-body">

                <?php

                
                echo  "<strong>battery_string_total_voltage:</strong> " .$battery_string_total_voltage;
                
                echo  "<strong>battery_string_total_current:</strong> " .$battery_string_total_current;
                
                echo  "<strong>battery_string_soc:</strong> " .$battery_string_soc;
                
                echo  "<strong>battery_string_soh:</strong> " .$battery_string_soh;
                

                ?>



            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">

            <div class="panel-heading"><h3 class="text-center">DC Voltage / Current</h3></div>
            <div class="panel-body">
                <?php

                echo  "<strong>dc_voltage_info:</strong> " . $dc_voltage_info;
                
                echo  "<strong>dc_current_info:</strong>" . $dc_current_info;
                
                echo  "<strong>dc_power_info:</strong>" .$dc_power_info;
                

                ?>



            </div>
        </div>
    </div>

</div>



<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="text-center">DCAC Energy</h3></div>
            <div class="panel-body">



                <?php
                echo  "<strong>dcac_ac_discharge_energy:</strong> " .$dcac_ac_discharge_energy;
                
                echo  "<strong>dcac_ac_charge_energy:</strong> " . $dcac_ac_charge_energy;
                
                ?>



            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">

            <div class="panel-heading"><h3 class="text-center">AC Output</h3></div>
            <div class="panel-body">
                <?php

                
                echo  "<strong>Phase a_amps: </strong>" .$a_amps;
                
                echo  "<strong>Phase  b_amps: </strong>" .$b_amps;
                
                echo  "<strong>Phase  c_amps:</strong> " .$c_amps;
                
                echo  "<strong>Phase  a_volts: </strong> " .$a_volts;
                
                echo  "<strong>Phase  b_volts: </strong> " .$b_volts;
                
                echo  "<strong>Phase  c_volts: </strong> " .$c_volts;
                
                echo  "<strong>true_power:</strong> " .$true_power;
                
                echo  "<strong>apparent_power:</strong> " .$apparent_power;
                ?>



            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="text-center">Frequency</h3></div>
            <div class="panel-body">



                <?php

                echo  "<strong>Frequency:</strong> " .$frequency;
                



                ?>



            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">

            <div class="panel-heading"><h3 class="text-center">Temperatures</h3></div>
            <div class="panel-body">
                <?php


                echo  "<strong>Temperature ipm_phase_a_temperature:</strong> " .$ipm_phase_a_temperature;
                
                echo  "<strong>Temperature ipm_phase_b_temperature:</strong> " .$ipm_phase_b_temperature;
                
                echo  "<strong>Temperature  ipm_phase_c_temperature: </strong> " .$ipm_phase_c_temperature;
                
                echo  "<strong>Temperature Transformer phase_b_temperature:</strong> " .$transformer_phase_b_temperature;
                ?>



            </div>
        </div>
    </div>

</div>