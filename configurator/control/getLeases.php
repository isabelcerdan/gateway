{ "data":
<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 5/12/2017
 * Time: 11:56 AM
 */
include_once ('../functions/session.php');
$filename = '/var/lib/misc/dnsmasq.leases';


if( file_exists($filename) ) {
//echo 'file access granted';
    //$myfile = fopen($filename, "r") or die("Unable to open file!");


    $lines = file($filename, FILE_IGNORE_NEW_LINES);
    $output[] = '';
    for ($i =0; $i < count($lines);$i++){
        $dataRow = explode(" ",$lines[$i]);
        $output[0] = $dataRow[1] . '-' . $dataRow[2];
        $output[1] = $dataRow[1];
        $output[2] = $dataRow[2];
        $output[3] = $dataRow[3];
        $output[4] = $dataRow[4];

        //  Infinate leases show time before 1970
        //Wed, 31 Dec 1969 16:00:00 -0800

        $dateOut = date('r', $dataRow[0]);
        if($dateOut == 'Wed, 31 Dec 1969 16:00:00 -0800'){
            $output[5] = 'Forever';
        }else {
            $output[5] = $dateOut;
        }


        $linedDATA[$i] = $output;
    }

  //  print_r($linedDATA);
   echo json_encode($linedDATA);

}else {
    echo "File not accessible ";
}
?>

}
