<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

include_once('get_system.php');
include_once('get_universal_aif.php');



// Get current values so we don't send any commands if there isn't a chance //
$inverter_fallback_power_setting_DB =  $inverter_fallback_power_setting;
$operation_mode_DB = $operation_mode;
$ntp_url_sg424_DB = $ntp_url_sg424;


$error = '';

    $gateway_id = mysqli_real_escape_string($conn, $_POST['gateway_id']);
    $gateway_id = filter_var($gateway_id, FILTER_SANITIZE_STRING);

    $gateway_name = mysqli_real_escape_string($conn, $_POST['gateway_name']);
    $gateway_name = filter_var($gateway_name, FILTER_SANITIZE_STRING);

    $total_output_limit = mysqli_real_escape_string($conn, $_POST['total_output_limit']);
    $total_output_limit= filter_var($total_output_limit, FILTER_SANITIZE_STRING);

    $address  = mysqli_real_escape_string($conn, $_POST['address']);
    $address = filter_var($address, FILTER_SANITIZE_STRING);

    $city   = mysqli_real_escape_string($conn, $_POST['city']);
    $city = filter_var($city, FILTER_SANITIZE_STRING);

    $state   = mysqli_real_escape_string($conn, $_POST['state']);
    $state = filter_var($state, FILTER_SANITIZE_STRING);

    $postcode   = mysqli_real_escape_string($conn, $_POST['postcode']);
    $postcode = filter_var($postcode, FILTER_SANITIZE_STRING);

    $log_level = mysqli_real_escape_string($conn, $_POST['log_level']);
    $log_level = filter_var($log_level, FILTER_SANITIZE_STRING);

    $gateway_perspective = mysqli_real_escape_string($conn, $_POST['gateway_perspective']);
    $gateway_perspective = filter_var($gateway_perspective, FILTER_SANITIZE_STRING);

    $inverter_fallback_power_setting = mysqli_real_escape_string($conn, $_POST['inverter_fallback_power_setting']);
    $inverter_fallback_power_setting = filter_var($inverter_fallback_power_setting, FILTER_SANITIZE_STRING);

    $operation_mode = mysqli_real_escape_string($conn, $_POST['operation_mode']);
    $operation_mode = filter_var($operation_mode, FILTER_SANITIZE_STRING);

    $ntp_url_sg424 = mysqli_real_escape_string($conn, $_POST['ntp_url_sg424']);
    $ntp_url_sg424 = filter_var($ntp_url_sg424, FILTER_SANITIZE_STRING);

    $region_name = mysqli_real_escape_string($conn, $_POST['region_name']);
    $region_name = filter_var($region_name, FILTER_SANITIZE_STRING);

if(($ntp_url_sg424_DB != $ntp_url_sg424) AND ($ntp_url_sg424 != '' ) ) {

    $ntpdateresult = exec("ntpdate -q $ntp_url_sg424", $outcome, $status);

    if (0 == $status) {
        $status = $outcome[1];
    } else {
        header('Location: /system.php?ntp=no');
       //echo $status = "Not Responding";
        exit;
    }
}


    if($gateway_name == '') {
        $error = $error . " gateway_name";
    }

    if($total_output_limit == '') {
        $error = $error . " total_output_limit";
    }
    if($address == '') {
        $error = $error . " address";
    }

    if($city == '') {
        $error = $error . " city";
    }

    if($state == '') {
        $error = $error . " state";
    }

    if( $postcode == '') {
        $error = $error . " postcode";
    }

    if( $log_level == '') {
        $error = $error . " log_level";
    }

    if( $gateway_perspective == '') {
        $error = $error . " gateway_perspective";
    }

//inverter_fallback_power_setting, '$inverter_fallback_power_setting', ,ntp_url_sg424
$sql = "INSERT INTO gateway (gateway_id,gateway_name,total_output_limit,address,city,state,postcode,log_level,perspective,operation_mode)
     values('$gateway_id','$gateway_name','$total_output_limit','$address','$city',
           '$state','$postcode','$log_level','$gateway_perspective','$operation_mode')
 ON DUPLICATE KEY UPDATE
     gateway_id = '$gateway_id', gateway_name = '$gateway_name',total_output_limit = '$total_output_limit', 
     address = '$address', city = '$city',postcode = '$postcode', log_level = '$log_level', 
     perspective = '$gateway_perspective',operation_mode = '$operation_mode'
";

// inverter_fallback_power_setting = '$inverter_fallback_power_setting',


if (!mysqli_query($conn,$sql)) {
    die('<br>Error: ' . mysqli_error($conn));
}
else {
    if($conn->insert_id != 0){
        $gateway_id = $conn->insert_id;
    }
    // Sends command to
    if ($operation_mode_DB != $operation_mode) {
        if($operation_mode =='observation_mode') {
            $resultsOperationMode =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py gom observation_mode');
        }
        if($operation_mode =='power_control_mode') {
            $resultsOperationMode =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py gom power_control_mode');
        }
        $resultsOperationMode= filter_var($resultsOperationMode, FILTER_SANITIZE_STRING);
    }


    if($inverter_fallback_power_setting_DB != $inverter_fallback_power_setting){
        $resultsFallBack =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py ifps ' . $inverter_fallback_power_setting);
        $resultsFallBack= filter_var($resultsFallBack, FILTER_SANITIZE_STRING);
    }

    // set network time server on inverters //
    if(($ntp_url_sg424_DB != $ntp_url_sg424) AND ($ntp_url_sg424 != '' ) ) {

        $resultsNTP=  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py nsu ' .$ntp_url_sg424);
    }
    
    if( ($region_nameDB != $region_name) AND ($region_name != '') AND ((strpos($region_name, '120') !== false) OR (strpos($region_name, 'IEEE') !== false))) {
        $resultsrRegion =  exec('sudo /usr/share/apparent/.py2-virtualenv/bin/python  /usr/share/apparent/python/gpts.py universal_aif ' .$region_name);
        $regionSent = $region_name;
    }

    header('Location: /success_system.php?gateway_id=' .$gateway_id . '&mode=' . $resultsOperationMode . '&fall=' .$resultsFallBack . '&NTP=' .
        $resultsNTP . "&resultsrRegion=" . $resultsrRegion ."&region=" . $regionSent);
}
?>