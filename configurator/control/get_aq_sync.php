<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Get all feeds to edit
 *
 *
 * */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$result = $conn->query("SELECT * FROM energy_report_config");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $URL_AQ = $row['URL_AQ'];
        $URL_AQ_Meter = $row['URL_AQ_Meter'];
        $transmit_frequency = $row['transmit_frequency'];
        $timeout = $row['timeout'];
        $retries = $row['retries'];
        $enabled = $row['enabled'];
        $gateway_base_sn = $row['gateway_base_sn'];
        $inverter_er_rate = $row['inverter_er_rate'];
        $inverter_er_url = $row['inverter_er_url'];
        $inverter_er_port = $row['inverter_er_port'];
        $inverter_er_user_name= $row['inverter_er_user_name'];
        $inverter_er_password= $row['inverter_er_password'];
        $alarms_trans_frequency= $row['alarms_trans_frequency'];
        $reports_per_batch = $row['reports_per_batch'];
    }
}


$result2 = $conn->query("select enabled from daemon_control where name = 'erptd'");

if(mysqli_num_rows($result2)>0) {

    while($row = $result2->fetch_assoc()) {
       $erptd= $row['enabled'];
    }
}
   // $conn->close();

?>