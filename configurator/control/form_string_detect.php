<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 *
 *         The "command" field is one of:

            no_command
            cancel
            start
            in_progress
 */

// Access restriction
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');


$error = '';

    $string = mysqli_real_escape_string($conn, $_GET['string']);
    $string= filter_var($string, FILTER_SANITIZE_STRING);

    $stringDetect = mysqli_real_escape_string($conn, $_GET['stringDetect']);
    $stringDetectEnabled = filter_var($stringDetect, FILTER_VALIDATE_INT);

$resultString = $conn->query("SELECT * FROM stringmon_control");

    while($row = $resultString->fetch_assoc()) {
     $audit_complete = $row['audit_complete'];
    }

// Update strings 
if( $string != ''  ){


    if(mysqli_num_rows($resultString)>0) {
        $sql = "UPDATE stringmon_control SET audit_status = 'start_audit',strings_detected = 0 ";

    }else {
        $sql = "INSERT INTO stringmon_control (audit_status,strings_detected)
      values('start_audit',0) ";
    }

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    else {
        if(mysqli_affected_rows($conn) == 1){
            $stringDetect = 'yes';
        }else {
            $stringDetect = 'no';
        }
    }
}

header('Location: /update_string_audit.php');
exit();

?>