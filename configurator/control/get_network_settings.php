<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM
 */

/*
 * Grab default values for both examples and for default in form fields
 *
 *
 * */

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

/*
 * See if there is any live data and replace defaults with live data
 *
 * 
 * +-----------+--------------+------+-----+---------+-------+
| Field     | Type         | Null | Key | Default | Extra |
+-----------+--------------+------+-----+---------+-------+
| iface     | varchar(64)  | NO   |     | NULL    |       |
| inet      | varchar(64)  | NO   |     | dhcp    |       |
| address   | varchar(128) | YES  |     | NULL    |       |
| mask      | varchar(128) | YES  |     | NULL    |       |
| broadcast | varchar(128) | YES  |     | NULL    |       |
+-----------+--------------+------+-----+---------+-------+
 * */

$start = exec("sudo /usr/share/apparent/.py2-virtualenv/bin/python /usr/share/apparent/python/inet/manage_inet.py --sync-from=file", $outcome3, $status3);

    $result = $conn->query("SELECT * FROM network_config  WHERE iface = 'eth0'");

    if(mysqli_num_rows($result)>0) {

        while($row = $result->fetch_assoc()) {
            $iface = $row['iface'];
            $inet = $row['inet'];
            $address = $row['address'];
            $netmask = $row['netmask'];
            //$broadcast = $row['broadcast'];
            $gateway = $row['gateway'];
        }
    }

?>