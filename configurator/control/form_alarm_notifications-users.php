<?php

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$subscribers_id= mysqli_real_escape_string($conn, $_REQUEST['subscribers_id']);
$subscribers_id= filter_var($subscribers_id, FILTER_SANITIZE_STRING);

$subscribers_name = mysqli_real_escape_string($conn, $_REQUEST['subscribers_name']);
$subscribers_name = filter_var($subscribers_name, FILTER_SANITIZE_STRING);

$delete_subscriber = mysqli_real_escape_string($conn, $_REQUEST['delete_subscriber']);
$delete_subscriber= filter_var($delete_subscriber, FILTER_SANITIZE_STRING);

$email = mysqli_real_escape_string($conn, $_REQUEST['email']);
$email= filter_var($email, FILTER_SANITIZE_EMAIL);


// Delete before ///
if(($delete_subscriber == 'yes') AND ($subscribers_id != '')){

        // sql to delete a record
        $sql = "DELETE FROM alarm_subscribers WHERE id='$subscribers_id'";

        if (mysqli_query($conn,$sql)) {
            header('Location: ../alarm_notifications_user.php');
            die('exit');
        }
        else {
            header('Location: ..alarm_notifications_user.php?error=1');
            die('<br>Error: ' . mysqli_error($conn));
        }
}

if($email == '') {

    header('Location: ../alarm_notifications_user.php?error=no_email');
    die('<br>Error:  email');
}


//remove id field if we change table to AUTO_INCREMENT
if( $subscribers_id== '') {
    // See if email is already in DB
    $result = $conn->query("SELECT email_address FROM alarm_subscribers where email_address = '$email'");
    while($row = $result->fetch_assoc()) {
        $emailDB = $row['email_address'];

        if(mysqli_num_rows($result)>0) {
            header('Location: ../alarm_notifications_user.php?error_email_in_db');
            exit();
        }
    }

    
    $sql = "insert into alarm_subscribers (name,email_address) VALUES('$subscribers_name','$email')";
}
else {
    $sql = "UPDATE alarm_subscribers set
email_address ='$email',name = '$subscribers_name' where id = '$subscribers_id'
";

}


if (!mysqli_query($conn,$sql)) {
    header('Location: ../alarm_notifications_user.php?' . mysqli_error($conn));
  // die('<br>Error: ' . mysqli_error($conn));
}
else {
    if($conn->insert_id != 0){
        $subscribers_id= $conn->insert_id;
    }
   // mysqli_close($con);

    header('Location: ../alarm_notifications_user.php?subscriptions_id=' .$subscribers_id );

}

?>