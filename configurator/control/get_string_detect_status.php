<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/19/2016
 * Time: 2:59 PM
 */

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$result = $conn->query("SELECT audit_status, strings_detected FROM stringmon_control");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        echo "<strong>Status:</strong> " . $row['audit_status'] . " <br><strong>Detected Strings:</strong>" . $row['strings_detected'];

        if($row['audit_status'] == 'start_audit') {

            echo "
              <script>
            $(document).ready(function(){ 
            console.log('start spinner');
              $('.loading').show(); 
            });
            </script>";

        }

        if(($row['audit_status'] == 'audit_failed') OR ($row['audit_status'] == 'audit_delayed' )  ) {
            echo "
              <script>
            $(document).ready(function(){ 
            console.log('close spinner: audit_failed');
              $('.loading').hide(); 
            });
            </script>";
        }

        if(($row['audit_status'] == 'audit_complete' ) AND  ($row['strings_detected'] > 0) ){
            echo "
              <script>
            $(document).ready(function(){ 
            console.log('close spinner: complete string > 0');
              $('.loading').hide(); 
            });
            </script>";
        }

        if(($row['audit_status'] == 'audit_complete' )  ){
            echo "
              <script>
            $(document).ready(function(){   
                setTimeout(function() {
                        console.log('close spinner: time');
                     $('.loading').hide(); 
                    }, 100000);
                }); 
            </script>";
        }
    }
}

//$conn->close();
//

?>


