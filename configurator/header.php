<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 5/3/2018
 * Time: 11:41 AM
 */
include_once ("functions/gateway_name.php");
?>

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div  class="gatewayName"><?php echo $gateway_display_name; ?></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="apparentBrand">Apparent igOS Gateway</div>
    </div>
</div>
