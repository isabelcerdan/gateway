<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

$errors = filter_var($_GET['errors'], FILTER_SANITIZE_STRING);

$human_privileges ='';
    $user_id = mysqli_real_escape_string($conn, $_GET['user_id']);
    $user_id = filter_var($user_id, FILTER_SANITIZE_STRING);

  include_once ('control/get_users.php'); // Get default data

include_once ('functions/get_readable_privileges.php');
// Form validation without JS //

if ($errors != ''){
    $regFail = true;
}

if (strpos($errors, 'already_in_DB') !== false) {
    $already_in_DB = true;
}

if (strpos($errors, 'problem') !== false) {
    $problem = true;
}


if (strpos($errors, 'passwordsDoNotmatch') !== false) {
    $passwordsDoNotmatch = true;

}
if (strpos($errors, 'first_name') !== false) {
    $first_nameErrror = true;

}

if (strpos($errors, 'last_name') !== false) {
    $first_nameErrror = true;

}

if (strpos($errors, 'email') !== false) {
    $emailErrror = true;

}
if (strpos($errors, 'registration_code') !== false) {
    $registration_codeErrror = true;

}
if (strpos($errors, 'company') !== false) {
    $companyErrror = true;

}
if (strpos($errors, 'phone') !== false) {
    $phoneErrror = true;

}


// set Privileges human readable
$human_privileges = get_readable_privileges($privilegesDB);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>



<div class="container">
 
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Register</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <?php if($already_in_DB == true ) {echo "<h3 class='text-center text-danger'>Your email is already in the system!</h3>";} ;?>
                <?php if($problem == true ) {echo "<h3 class='text-center text-danger'>Looks like there was a problem registering. <b>Please contact support</b></h3>";} ;?>


                <div class="panel-body">
                    <form class="form-horizontal row-border" action="/control/form_reg.php" method="post">
                        <?php
                            echo "<input type='hidden' name='user_id' value='$user_id'>";
                        ?>

                        <div class="formBoxSection ">
                            <div class="form-group <?php if($first_nameErrror == true ) {echo " has-error inputError";} ;?>  ">
                                <label class="col-md-3 control-label">First Name:</label>
                                <div class="col-md-3 ">
                                    <input type="text" name="first_name" class=" form-control " placeholder="First Name" required value="<?php echo $first_nameDB; ?>">
                                </div>
                                <div class="col-md-6" >
                                  <div class="formTextSpacing">
    
                                  </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($last_nameErrror == true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Last Name:</label>
                                <div class="col-md-3">
                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" required value="<?php echo $last_nameDB; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($emailErrror == true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Email:</label>
                                <div class="col-md-3">
                                    <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required value="<?php echo $emailDB; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($passwordsDoNotmatch == true ) {echo " has-error inputError";} ;?> ">
                                <label class="col-md-3 control-label">Password:</label>
                                <div class="col-md-3">
                                    <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password"
                                           <?php
                                           // don't require passwords if update //
                                           if($user_id == ''){
                                               echo "pattern=\".{6,25}\" required title=\"6 to 15 characters\" required";
                                           }
                                           ?>

                                           >
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing text-danger">
                                        <?php if($passwordsDoNotmatch == true ) {echo " Make sure that your passwords match, and they are at least 6 char long";} ;?>
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($passwordsDoNotmatch == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Confirm Password:</label>
                                <div class="col-md-3">
                                    <input type="password"  name="password_comf" id="inputPassword" class="form-control" placeholder="Password confirmation"
                                        <?php
                                        // don't require passwords if update //
                                        if($user_id == ''){
                                            echo "pattern=\".{6,25}\" required title=\"6 to 15 characters\" required";
                                        }
                                        ?>
                                          >
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing text-danger">
                                        <?php if($passwordsDoNotmatch == true ) {echo " Make sure that your passwords match, and they are at least 6 char long";} ;?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?php if($registration_code == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Role:</label>
                                <div class="col-md-3">
                                    <select name="role" class="form-control" required>
                                        <?php
                                        if($roleDB !='') {
                                            echo "<option value='$roleDB' >$roleDB</option>";
                                        }else {
                                            echo "<option value='' >Select Role</option>";
                                        }
                                        ?>
                                        <option value='config' >Configurator Access</option>
                                        <option value='gui' >GUI Access</option>
                                        <option value='both' >GUI & Configurator Access</option>
                                    </select>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing text-danger">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?php if($registration_code == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Privileges:</label>
                                <div class="col-md-3">
                                    <select name="privileges" class="form-control" required>
                                        <?php
                                        if($privilegesDB !='') {
                                            echo "<option value='$privilegesDB' >$human_privileges</option>";
                                        }else {
                                            echo "<option value='' >Select Privileges</option>";
                                        }
                                        ?>
                                        <option value='4'>Monitor</option>
                                        <option value='3'>Installer</option>
                                        <option value='2'>Manager</option>
                                        <option value='1'>Administrator</option>
                                    </select>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing text-danger">

                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($registration_code == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Registration Code:</label>
                                <div class="col-md-3">
                                    <input type="text" name="registration_code" class="form-control" placeholder="Registration Code" required>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing text-danger">
                                        <?php if($registration_code == true ) {echo " Make sure that your registration code is correct";} ;?>
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($companyErrror == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Company:</label>
                                <div class="col-md-3">
                                    <input type="text" name="company" class="form-control" placeholder="Your Company Name" required value="<?php echo $companyDB; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group <?php if($phoneErrror == true ) {echo " has-error inputError";} ;?>">
                                <label class="col-md-3 control-label">Phone Number:</label>
                                <div class="col-md-3">
                                    <input type='tel' name="phone" class="form-control" placeholder="999-999-9999" value="<?php echo $phoneDB; ?>">
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">
    
                                    </div>
                                </div>
                            </div>
    
    
                            </div>
    
                            <button type="submit" class="btn btn-primary" >Register</button>

                    </form>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>