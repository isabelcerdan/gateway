<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 11/206/2017
 * Time: 2:26 PM
 */
include_once('functions/mysql_connect.php');
$inverters_id = mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
$inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);

$inverters = explode(",",$inverters_id);
foreach($inverters as $key => $inverter) {
    $inverterArray[$key] = "'$inverter'";
}
$inverterArray = join(', ',  $inverterArray);


$result = $conn->query("SELECT mac_address,serialNumber,IP_Address  FROM inverters WHERE inverters_id IN ($inverterArray)");

if(mysqli_num_rows($result)>0) {

    while ($row = $result->fetch_assoc()) {
        $mac_addresses[] = $row['mac_address'];
        $serialNumber[] = $row['serialNumber'];
        $IP_Address[] =  $row['IP_Address'];
    }
}


$group=  mysqli_real_escape_string($conn, $_GET['group']);
$group = filter_var($group, FILTER_SANITIZE_STRING);

?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/jquery-ui.css" >

<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<style>

    .error {
      color:red;
    }



</style>


<script>
    $( document ).ready( function () {
       
        $( "#powerFactorSetting" ).validate( {
            rules: {
                powerFactor:{
                    required: true,
                    range:[0.707,1.00],
                    number: true
                },
                enabled:{required :true},
                lagging:{required :true}
            },
            messages: {

                PowerFactor: {
                    required: "Please enter the Power Factor",
                    range: "Please enter values between the range of 1 to 0.707"
                },
                enabled: "Please enabled or dis-enabled Power fFactor control",
                lagging:"Please select Current Leading or Not Current Leading"
        }, errorElement: "em",
            errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            }
           
        });
    });
</script>
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<body>


<div class="container">
<h2>Set Power factor</h2>

<br>


<div class="panel panel-default">

    <div class="panel-body">
        <form id="powerFactorSetting" action="control/set_power_factor.php" name="powerFactorSetting" >
            <p>
                <label>Power Factor:</label>
                <br>
                <input type="number" name="powerFactor"  min="0.707" max="1" value="0.95" >
            </p>
            <p>
                <label>Enable Power Factor Control:</label>
                <br>
                <input type="radio" name="enabled" value="1" checked>True<br>
                <input type="radio"  name="enabled" value="0" >False<br>
            </p>
            <p>
                <label>Current Leading:</label>
                <br>
                <input type="radio" name="lagging" value="1" >True<br>
                <input type="radio"  name="lagging" value="0">False<br>

            </p>


                <label>Inverters/Groups to </label>
            <p>
            <?php

                if($group != "No_Group") {
                    echo "Feed: " . $group;
                    echo "<input type='hidden' value='$group' name='group'>";
                }
                elseif(!empty($serialNumber)) {
                    echo "<input type='hidden' value='$inverters_id' name='inverters_id'>";
                    for ($i = 0;$i< count($serialNumber); $i++ ){

                        echo "Inverter SN:" . $serialNumber[$i] . ' | Mac: '.$mac_addresses[$i] . ' | IP: ' . $IP_Address[$i] . "<br>";
                    }
                }else {
                    echo "no changes at this time";
                }
            ?>
            </p>
            <button type="submit">Submit</button>
        </form>
        <?php
       // include_once('control/set_inverter_health.php'); // Get default data
        ?>



    </div>
</div>
</div>


</body>
</html>