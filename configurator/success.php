<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php

include_once ('functions/mysql_connect.php');

$result = $conn->query("SELECT * FROM power_regulators");


while($row = $result->fetch_assoc()) {
    $feed_id = $row['id'];
    $feed_name = $row['feed_name'];
    $SYSTEM_SIZE =  $row['SYSTEM_SIZE'];
    $MINIMUM_IMPORT =  $row['MINIMUM_IMPORT'];
    $CURTAIL_TARGET =  $row['CURTAIL_TARGET'];
}

$conn->close();

/*
 *
echo "<br>";
echo $SYSTEM_SIZE;

echo "<br>";
echo $MINIMUM_IMPORT;

echo "<br>";
echo $CURTAIL_TARGET;

 */

?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review Your Inputs</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">

                    <div class="formBoxSection">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Minimum Import:</label>
                            <div class="col-md-3">
                                <div class="formTextSpacing">
                                    <strong><?php echo $MINIMUM_IMPORT; ?></strong>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Curtail Target:</label>
                            <div class="col-md-3">
                                <div class="formTextSpacing">
                                    <strong><?php echo $CURTAIL_TARGET; ?></strong>
                                </div>

                            </div>
                        </div>

                    </div>

                        </div>

                        <a class="btn btn-warning" href="/power_regulator.php" role="button">Make Changes</a>
                        <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                        <a class="btn btn-primary" href="/meter.php" role="button">Add a Meter</a>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>
