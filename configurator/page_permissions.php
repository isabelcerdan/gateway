<?php
//include_once ('functions/session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Meters</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>

    <script type="text/javascript">
        $( document ).ready( function () {
            $(document).on("click", "a.delete-page", function() {
                if (confirm('Are you sure  you want to delete ' + this.id + ' Page?' )) {
                    $(location).attr('href', '/control/form_page_perms_add_edit.php?page_perm_id=' + this.id + '&delete_page=yes');
                }
            });
        } );
    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$pageURL ='';
include_once('functions/page_permissions_check.php');
include_once ('functions/get_readable_privileges.php');



?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Assign Page Access Level</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">

                    <div class="formBoxSection">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="font-size: smaller">
                                    <th></th>
                                    <th></th>
                                    <th class='text-center'>Page ID</th>
                                    <th class='text-center'>Site</th>
                                    <th class='text-center'>Page URL</th>
                                    <th class='text-center'>Access Level</th>
                                    <th class='text-center'>Page Section</th>
                                    <th class='text-center'>Notes</th>


                                </tr>
                                </thead>
                                <tbody id="auto_brainbox_status_div">
                                <?php
                                for($i = 0;$i <  count($page_perm_id); $i++) {
                                    echo "<tr>
                                            <td class='text-center'><a href='page-perms-add-edit.php?page_perm_id=$page_perm_id[$i]'> <button class=\"btn btn-warning\" >Edit</button></a></td>
                                            <td class='text-center'><a class='delete-page' id='$page_perm_id[$i]' > <button class=\"btn btn-danger\" >Delete</button></a></td>
                                            <td class='text-center'>$page_perm_id[$i]</td>
                                            <td class='text-center'>$site[$i]</td>
                                            <td class='text-center'>$pageURL[$i]</td>
                                            <td class='text-center'>" .  get_readable_privileges($access_level[$i]) .
                                            "</td>  
  
                                            <td class='text-center'>$page_section[$i]</td>
                                    
                                            <td class='text-center'>$notes[$i]</td>                      
                                    ";
                                    echo "</tr>";
                                }

                                ?>

                                </tbody>
                            </table>
                        </div>


                    </div>




                    <div class="formBoxSection">

                        <a href='page-perms-add-edit.php'> <button class="btn btn-primary" >Set Additional Page Permissions</button></a>
                  


                    </div>

                </div>
            </div>


            </div>
        </div>
    <?php    include_once ('footer.php'); ?>

</div>
    <!-- Row end -->

</div>
</body>
</html>