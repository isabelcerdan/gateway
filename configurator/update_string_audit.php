<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:14 PM
 */
include_once('functions/mysql_connect.php');



?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">

<style>
    .progress {
        margin-bottom: 0px;
    }
</style>
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="js/inverter-display.js?<?php echo rand(); ?>"></script>


<script>

    function auto_feed_status(){
        $.ajax({
            url: "control/get_string_progress.php",
            cache: false,
            success: function(data){
                $("#auto_feed_status_div").html(data);
            }
        });
    }



    $(document).ready(function(){
        //console.log('I live');
        auto_feed_status(); //Call auto_load() function when DOM is Ready
        auto_feed_status2(); //Call auto_load() function when DOM is Ready



    });

    //Refresh auto_load() function after 10000 milliseconds
    setInterval(auto_feed_status,2000);


</script>
<body>


<div class="container">
    <h2>Getting Inverter information</h2>


    <div class="panel panel-default">

        <div style="margin-right: 50px; margin-left: 50px; margin-top: 20px; margin-bottom: 10px">

                <div id="auto_feed_status2_div"></div>
                <!--
                <div class="progress" style="height: 30px">
                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $totalFinsihed ?>%">
                    <div style="margin-top: 5px; font-size: large"> <?php //echo $totalFinsihed ?>% Complete </div>
                </div>
                </div>
                -->

        </div>
        <div class="panel-body">


            <div class="table-responsive">
                <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                    <tr >
                        <th class="text-center">audit_status </th>
                        <th class="text-center">strings_detected</th>
                        <th class="text-center">timestamp</th>
                        <th class="text-center"></th>

                    </tr>
                    <tbody id="auto_feed_status_div">


                    </tbody>

                </table>
            </div>
            <div class=" text-right">
                <button id="upgrade-cancel" type="button" class="btn btn-outline-secondary"  >Cancel</button>
            </div>

        </div>
    </div>
</div>


</body>
</html>