<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:14 PM
 */
include_once('functions/mysql_connect.php');

////////////////////////////////////////////////////////////////////////
// If inverter are being upgraded forward to upgrade status page
////////////////////////////////////////////////////////////////////

$inverters_id= mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
$inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);


$result = $conn->query("SELECT * FROM inverters where inverters_id = '$inverters_id' ");



if(mysqli_num_rows($result)>0) {

    while ($row = $result->fetch_assoc()) {
        $IP_Address = $row['IP_Address'];
    }
}
//in_progress
?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
<body>
<script>
   // window.parent.location = document.referrer;

</script>

<div class="container">
    <h2>Inverter Query Info:</h2>

    <br>


    <div class="panel panel-default">

        <div class="panel-body">
                <?php
                include_once('control/get_inverter_query.php'); // Get default data
                ?>
        </div>
    </div>
</div>


</body>
</html>