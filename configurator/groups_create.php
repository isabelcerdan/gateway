<?php
include_once('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Groups</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }

        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        .popover {
            top: -30px !important;
            left: 35px !important;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/system-add-edit.js"></script>


    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script>
        $(document).ready(function () {
            /*
            $('#group_id').on('change', function() {
                var url = window.location.href;

                    url += '?group_id=2';

                window.location.href = url;
            });
*/
            $('[data-toggle="popover"]').popover();

            $("#inverter_fallback_power_setting").change(function(){
                var powerSettings = $("#inverter_fallback_power_setting").val();
                alert('Are you really sure that you want to change the default power settings on the inverters to: ' + powerSettings + ' ?');
            });

            $("#operation_mode").change(function(){
                var mode = $("#operation_mode").val();
                var r = confirm('Are you really sure that you want to change the the Gateway to : ' + mode + ' Mode? ');
                if (r == true) {

                }
            });

            $("#ntp_url_sg424").change(function(){
                var timeServer = $("#ntp_url_sg424").val();
                var r = confirm('Are you really sure that you want to change the NTP Server to : ' + timeServer + ' ? ');
                if (r == true) {

                }
            });

        });


    </script>

</head
<body>
<?php
include_once('functions/mysql_connect.php');
// Init meter var
$group_id  = mysqli_real_escape_string($conn, $_GET['group_id']);
$group_id = filter_var($group_id, FILTER_SANITIZE_STRING);


include_once('control/get_groups.php'); // Get default data


?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Group Settings</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal row-border" action="control/form_groups_add_edit.php" method="post" id="systemForm">
                            <input type="hidden" name="group_id" value="<?php echo $group_id; ?> ">

                        <div class="formBoxSection">
                                <h4 class="text-center">Group Name: <?php echo $aliasDB; ?></h4>
                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Name:</label>
                                    <div class="col-md-2">
                                        <input type="text" name="alias" id="alias" class="form-control"
                                               value="<?php echo $aliasDB; ?>">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="alias" data-content="alias">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the Alias of the group you are creating
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Location:</label>
                                    <div class="col-md-2">
                                        <input type="text" name="location" id="location" class="form-control"
                                               value="<?php echo $locationDB; ?>">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Please give the location of the group (if pertinent)
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">FallBack Power Settings:</label>
                                    <div class="col-md-2">

                                        <select name="fallback"  id="fallback" class="form-control" required >
                                            < <?php
                                            if($fallbackDB!= ''){
                                                echo "<option value='$fallbackDB'>$fallbackDB% - Current</option>";
                                            }else {
                                                echo "<option value=''>Select</option>";
                                            }

                                            for ($i = 0; $i <= 100;$i++ ) {
                                                echo "  <option value='$i'>$i%</option>";
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> This is the power setting the inverter will default to if the inverter losses connectivity with the Gateway
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Time Server(NTP):</label>
                                    <div class="col-md-2"> <?php if($ntp_Errror == true ) {echo " has-error inputError";} ;?>
                                        <input name="ntp_url_sg424" id="ntp_url_sg424" class="form-control " value="<?php echo $ntp_url_sg424DB; ?>">
                                        <?php if($ntp_Errror == true ) {echo "<h4 style='color:red;'> NTP Server Not Reachable </h4>";} ;?>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> To change the inverters time server (NTP) please enter the URL of the time server above.
                                            Example: time.google.com Do not include (http://)
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Forced Upgrade:</label>
                                    <div class="col-md-2" style="padding-top: 8px">

                                        <input name="forced_upgrade" id="forced_upgrade" value="1" type="checkbox" <?php if ($forced_upgradeDB== '1'): ?>checked<?php endif ?>>

                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Check if you want these inverters to be forced to a specific software version
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Irradiance Meters:</label>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <input name="irradiance" id="irradiance"  value="1" type="checkbox" <?php if ($irradianceDB == '1'): ?>checked<?php endif ?>>

                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Turn group of inverters into Irradiance Meters and let them make full power unrestrained
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Gateway Control:</label>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <input name="gateway_control" id="gateway_control" value="1" type="checkbox" <?php if (($gateway_controlDB== '1') OR ($gateway_controlDB== '')): ?>checked<?php endif ?>>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong> Take inverters in/out of Gateway mode
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Sleep:</label>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <input name="sleep" id="sleep" value="1" type="checkbox" <?php if  ($sleepDB== '1'): ?>checked<?php endif ?>>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong>Inverters will go into low power mode at night
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">er_server_http:</label>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <input name="er_server_http" id="er_server_http" value="1" type="checkbox" <?php if (($er_server_httpDB== '1')): ?>checked<?php endif ?>>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong>Send Reports to ER through http
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">er_gtw_tlv:</label>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <input name="er_gtw_tlv" id="er_gtw_tlv" value="1" type="checkbox" <?php if (($er_gtw_tlvDB== '1')): ?>checked<?php endif ?>>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong>Send Reports to ER through http
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label">AIF:</label>
                                    <div class="col-md-2" style="padding-top: 8px">

                                      <strong><?php echo $AIF_idDB; ?></strong> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="location" data-content="location">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>
                                            <strong>Note: </strong>TBA
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Notes:</label>
                                    <div class="col-md-8">
                                        <input type="textarea" name="notes" id="notes" class="form-control"
                                               value="<?php echo $notesDB; ?>">
                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                </div>


                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>


            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>

</div>
<!-- Row end -->

</div>
</body>
</html>