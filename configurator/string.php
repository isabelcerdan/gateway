<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Strings</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$meter_id = '';
include_once ('control/get_strings.php'); // Get default data


?>
<div class="container">

    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Pick your String to Configure </h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal row-border" action="strings-add-edit.php" method="post">
                   

                    <div class="formBoxSection">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Edit Strings:</label>
                            <div class="col-md-3">
                                <!-- if added new feed make sure to add them to the get_default.php -->
                                <select name="stringId" class="form-control">
                                    <br> <option value=''>New String</option>
                                    <?php

                                        for ($i = 0; $i < count($stringId);$i++ ) {
                                                echo "  <br> <option value='$stringId[$i] '>$stringId[$i] - $string_name[$i]  - Edit</option>";
                                        }

                                    ?>

                                </select>
                            </div>
                            <div class="col-md-6" >
                              <div class="formTextSpacing">

                              </div>
                            </div>
                        </div>



                        
                        
                    </div>

                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>