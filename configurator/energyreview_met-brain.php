<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Register Meters and Brainboxes with Energy Review</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/brainboxes-add-edit.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>


    <script type="text/javascript">
        $( document ).ready( function () {

           // $('.delete-brain').click('change', function() {
               // alert('id:' + this.id);
               // this.form.submit();
            //});

            $(document).on("click", "a.delete-brain", function() {
                if (confirm('Are you sure  you want to delete ' + this.id + ' Brainbox?' )) {
                    $(location).attr('href', '/control/form_bainboxes_add_edit.php?brainbox_id=' + this.id + '&delete_brain=yes');
                }
            });


        } );


    </script>
</head
<body>
<?php
include_once ('functions/mysql_connect.php');

$error = mysqli_real_escape_string($conn, $_REQUEST['error']);
$error = filter_var($error, FILTER_SANITIZE_STRING);

//include_once ('control/get_brainboxes.php'); // Get default data
//include_once ('control/get_meter.php'); // Get default data
include_once ('control/get_brain_meter_for_er.php');

?>
<div class="container">
 
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Register Meters and Brainboxes with Energy Review</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">
                    <?php
                        if($error == 'no_brain'){
                            echo '      
                                <div class="alert alert-danger">
                                You need to register the Brainbox before registering the Meter 
                                </div>
                            ';
                        }

                    ?>


                    <div class="formBoxSection">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="font-size: smaller">

                                    <th></th>
                                    <th></th>

                                    <th class='text-center'>BrainBox</th>
                                    <th class='text-center'>Meter</th>
                                    <th class='text-center'>Dev No.</th>
                                    <th class='text-center'>Device Name</th>
                                    <th class='text-center'>Interface</th>
                                    <th class='text-center'>Com Status</th>



                                </tr>
                                </thead>
                                <tbody id="auto_brainbox_status_div">
                                <?php
                                for($i = 0;$i <  count($brainbox_id); $i++) {
                                    echo "<tr>";
                                    if($brainbox_id[$i] != $brainbox_id[$i-1]) {
                                        echo "<td></td>
                                            <td></td>
                                           
<td class='text-left'><a href='control/aq_sync.php?brainbox_id=$brainbox_id[$i]&meter=no'> <button class=\"btn btn-warning\" >Add BrainBox $B_nickname[$i] to ER</button></a></td>
                                        <td class='text-center'></td>
                                        <td class='text-center'></td>
                                        <td class='text-center'>$B_nickname[$i]</td>
                                  
                              
                                          
                                          <td class='text-center'>$B_target_interface[$i]</td>
                                          <td class='text-center'>";
                                                    if($B_ER_Connect[$i] ==1){
                                                        echo "Paired with ER";
                                                    }else{
                                                        echo "Not Connected";
                                                    }
                                    }
                                    echo "</td>
                                        <td style='border:none'>
                                            
                                            <tr >
                                                <td ></td>
                                                <td></td>
                                                <td></td>
                                              
                                                <td class='text-left'><a href='control/aq_sync.php?brainbox_id=$brainbox_id[$i]&meter_id=$meter_id[$i]&meter=yes&meter_class=$M_meter_class[$i]&modbusdevicenumber=$M_modbusdevicenumber[$i]&B_ER_Connect=$B_ER_Connect[$i]'> <button class=\"btn btn - warning\" >Add Meter $M_meter_name[$i] to ER</button></a>
                                                </td>
                                  
                                                <td class='text-center'>$M_modbusdevicenumber[$i]</td>
                                                <td class='text-center'>$M_meter_name[$i]</td>
                                                <td class='text-center'>$M_target_interface[$i]</td>
                                                <td class='text-center'>";
                                                    if($M_ER_Connect[$i] ==1){
                                                        echo "Paired with ER";
                                                    }else{
                                                        echo "Not Connected";
                                                    }

                                    echo"</td>
                                            </tr>
                                    
                                            
                                                                            
                                        </td>
                                    
                                        ";

                                    echo "</tr>";
                                }

                                ?>
                               
                                </tbody>
                            </table>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    

</div>
</body>
</html>