<?php
include_once ('functions/session.php');

include_once ('functions/mysql_connect.php');
include_once('control/get_brainboxes.php'); // Get default data
include_once ('control/get_meter_roles.php');

$meter_id = mysqli_real_escape_string($conn, $_REQUEST['meter_id']);
$meter_id = filter_var($meter_id, FILTER_SANITIZE_STRING);

$errors = mysqli_real_escape_string($conn, $_GET['errors']);
$errors = filter_var($errors, FILTER_SANITIZE_STRING);

if (strpos($errors, 'no_affected_rows') !== false) {
    $changes = "No Changes Made";
}

if($meter_id !='') {
    include_once ('control/get_meter.php'); // Get default data
}

$letters = range('A', 'F');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Meter Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }
        .popover {
            top: -30px !important;
            left: 35px !important;
        }


    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/meter-add-edit.js"></script>
    <script>
        $(document).ready(function(){

            <?php
            ////////////////////////////////////////////////////////////////////////
            // On page load hide items that are not necessary for remote meters  //
            ///////////////////////////////////////////////////////////////////////

                if($meter_type == 'remote'){
                    echo '$(".remoteHide").hide();';
                }

            ?>

            //////////////////////////////////////////////////////////////////////
            // If user picks remote meters hide unnecessary items
            ////////////////////////////////////////////////////////////////////
            $("#meter_type").change(function(){
              meterType =  $("#meter_type").val();
                //alert('meterType: ' + meterType);
                if(meterType == 'remote') {
                    $(".remoteHide").hide();
                }else{
                    $(".remoteHide").show();
                }
            });

        });

    </script>
    
</head
<body>


<div class="container">
    
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;"> Configure Meter </h1>
                        <?php 
                        // Errors
                        if ($changes != '') {
                            echo"<p style='color: red;font-weight: bold'>$changes </p>";
                        }
                        ?>
                    </div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                    <form class="form-horizontal row-border" action="/control/form_meter_add_edit.php" method="post" id="meterForm">
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">
                        <input type="hidden" name="meter_id" value="<?php echo $meter_id; ?>">


                        <div class="formBoxSection">

                            <div class="formBoxSectionWhite">
                                <div class="form-group">

                                    <div class="col-md-12 " >
                                        <div class="formTextSpacing text-center">
                                            <h2> Add/Edit <?php echo $meter_role; ?>  Meter</h2>
                                            <?php

                                            // Errors
                                            if ($changes != '') {
                                                echo"<p style='color: red;font-weight: bold'>$changes </p>";
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>


                            </div>






                                <!--
<div id="tabs">                                <ul>
                                    <li><a href="#tabs-1">Basic Meter</a></li>
                                    <li><a href="#tabs-2">Advanced Communications</a></li>
                                    <!-- <li><a href="#tabs-3">Advanced Configuration </a></li>
                                </ul> <div id="tabs-1">
                                <!-- Beginning of tab 1 -->


                                    <!--
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Meter Name:</label>
                                            <div class="col-md-3">
                                                <input type="text" name="meter_name" id="meter_name" class="form-control" value="<?php echo $meter_name; ?>" required>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="formTextSpacing">

                                                    <a  data-toggle="popover" title="Meter Name" data-content="Give the meter a unique name, that also describes it use and location."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                    <strong>Note:</strong> Give each meter a unique name
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                            <div class="formBoxSectionWhite">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meter Type:</label>
                                    <div class="col-md-3">
                                        <select name="meter_type"  id="meter_type" class="form-control" required >
                                            < <?php
                                            if($meter_type != ''){
                                                echo "<option value='$meter_type'>$meter_type - Current</option>";
                                            }else {
                                                echo "<option value=''>Select</option>";
                                            }
                                            ?>
                                            <option value='veris_brainbox'>Veris/Brainbox</option>
                                            <option value='remote'>Remote</option>
                                            <option value='apparent'>Apparent</option>

                                        </select>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">
                                            <a  data-toggle="popover" title="Meter Role" data-content="Provide the role that the meter will play in this installation. Examples:"  ">
                                            <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Meter Role:</label>
                                            <div class="col-md-3">
                                                <select name="meter_role"  id="meter_role" class="form-control" required >

                                                    <?php
                                                    // if nothing is set
                                                    if($meter_role == '') {
                                                        echo "<option value=''>Select</option>";
                                                    }
                                                    for($i =0; $i < count($meter_role_id);$i++ ) {
                                                        // display current setting
                                                        if($meter_role_name[$i] ==  $meter_role){
                                                            echo "<option value='$meter_role_name[$i]'>$meter_role_name[$i]  - Current</option>";
                                                        }
                                                        // Loop through list
                                                        echo "<option value='$meter_role_name[$i]'>$meter_role_name[$i]</option>";
                                                    }

                                                    ?>

                                                </select>
                                                <div id="meter_role_manual" style="display:none; margin-top: 5px;">
                                                    <strong>Meter Role- Manual:</strong>
                                                    <p>Adding a meter role type is rare...</p>
                                                    <input type="text"  name="meter_role_manual" class="form-control" value="" >
                                                </div>
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Meter Role" data-content="Provide the role that the meter will play in this installation. Examples:"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="formBoxSectionWhite remoteHide">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Modbus Device Number:</label>
                                            <div class="col-md-3">

                                                <input type="text" name="modbusdevicenumber" id="modbusdevicenumber" class="form-control" value="<?php echo $modbusdevicenumber; ?>">
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Modbus Device Number" data-content="Modbus is a serial communications protocol that meter use to communicate with the Gateway"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Interface Address:</label>
                                            <div class="col-md-3">

                                                <select name="interfaceSelect" id="interfaceSelect" class="form-control">

                                                    <?php
                                                    // if nothing is set
                                                    if($meter_role == '') {
                                                        echo "<option value=''>Select</option>";
                                                    }
                                                    for($i =0; $i < count($meter_role_id);$i++ ) {
                                                        // display current setting
                                                        if($target_role_interface[$i] ==  $address){
                                                            echo "<option value='$target_role_interface[$i]'>$target_role_interface[$i]  - Current</option>";
                                                        }
                                                        // Loop through list
                                                        echo "<option value='$target_role_interface[$i]'>$target_role_interface[$i]</option>";
                                                    }

                                                    ?>
                                                    <option value='manual_interface'>Manually input interface</option>
                                                </select>

                                                <div id="interfaceManual" style="display:none; margin-top: 5px;">
                                                    <strong>Interface - Manual:</strong>

                                                    <input  type="text" name="interfaceManual" class="form-control" value="<?php if($address !='') { echo $address; };  ?>" >

                                                </div>

                                          
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Interface Address" data-content="Please specify which Interface Address the meter is using. It could be a mac address(example: 00:0a:95:9d:68:16.) or and IP address (example: 192.168.10.137)"  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Meter Delta:</label>
                                            <div class="col-md-3" style="padding-top: 8px">
                                                <input name="meter_delta" id="meter_delta" type="checkbox" <?php if ($meter_delta == '1'): ?>checked<?php endif ?>></input>
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Delta"
                                                        data-content="Meter is behind a delta-wye transformer so individual feed control is not possible."  ">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="formBoxSectionWhite  remoteHide">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">CT Orientation Reversed:</label>
                                            <div class="col-md-3" style="padding-top: 8px">
                                                <input name="ct_orientation_reversed" id="ct_orientation_reversed" type="checkbox" <?php if ($ct_orientation_reversed == '1'): ?>checked<?php endif ?>></input>
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Polarity"
                                                        data-content="This should be checked if CT orientation is producing -import/+export."">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="formBoxSectionWhite remoteHide">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Baud:</label>
                                            <div class="col-md-3">

                                                <select name="baud" class="form-control" required>
                                                    <?php
                                                    if($baud != ''){
                                                        echo "<option value='$baud'>$baud - Current</option>";
                                                    }
                                                    ?>

                                                    <option value='300'>300</option>
                                                    <option value='600'>600</option>
                                                    <option value='1200'>1200</option>
                                                    <option value='2400'>2400</option>
                                                    <option value='4800'>4800</option>
                                                    <option value='4800'>9600</option>
                                                    <option value='14400'>14400</option>
                                                    <option value='28800'>28800</option>
                                                    <option value='38400' selected>38400</option>
                                                    <option value='57600'>57600</option>
                                                    <option value='115200'>115200</option>
                                                </select>


                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Baud" data-content="The baud rate is the rate at which information is transferred in a communication channel. In the serial port context, 9600 baud means that the serial port is capable of transferring a maximum of 9600 bits per second."  >
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="formBoxSectionWhite remoteHide">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Network Comm Type:</label>
                                            <div class="col-md-3">
                                                <select name="network_comm_type" class="form-control" required>
                                                    <?php
                                                    if($network_comm_type!= ''){
                                                        echo "<option value='$network_comm_type'>$network_comm_type- Current</option>";
                                                    }
                                                    ?>
                                                    <option value='rtu'>rtu</option>
                                                    <option value='tcp'>tcp</option>


                                                </select>

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Network Comm Type" data-content=""  >
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div><!-- End of Tab 1 -->

                        <!-- <div id="tabs-2"> Beginning of tab 2
                        <h3 class="text-center rarely">These settings rarely need to be changed</h3>-->





                        <!--  </div>< End of Tab 2-->

                               <!-- <div id="tabs-3"> -->
                               <!-- Beginning of tab 3 -->
                                    <!--
                                    <h3 class="text-center rarely">These settings rarely need to be changed</h3>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Feed of Phase A:</label>
                                            <div class="col-md-3">

                                                <select name="feed_of_phase_A" class="form-control">
                                                    <?php


                                                    foreach ($letters as $one) {
                                                        $selected = '';
                                                        if( $one == 'A' ) {
                                                            $selected = 'selected';
                                                        }

                                                        for ($i = 0; $i < count($FEED_NAME);$i++ ) {



                                                            if($one == $FEED_NAME[$i] ) {
                                                                echo "  <br> <option value='$one' $selected>$one - Edit</option>";
                                                                $one = null;
                                                                break;
                                                            }
                                                        }
                                                        if($one != null )
                                                            echo "<option value='$one' $selected>$one</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Feed of Phase A" data-content="Three-phase electric power is a common method of alternating-current electric power generation, transmission, and distribution.[1] It is a type of polyphase system and is the most common method used by electrical grids worldwide to transfer power."  >
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Feed of Phase B:</label>
                                            <div class="col-md-3">

                                                <select  name="feed_of_phase_B" class="form-control">
                                                    <?php


                                                    foreach ($letters as $one) {
                                                        $selected = '';
                                                        if( $one == 'B' ) {
                                                            $selected = 'selected';
                                                        }

                                                        for ($i = 0; $i < count($FEED_NAME);$i++ ) {



                                                            if($one == $FEED_NAME[$i] ) {
                                                                echo "  <br> <option value='$one' $selected>$one - Edit</option>";
                                                                $one = null;
                                                                break;
                                                            }
                                                        }
                                                        if($one != null )
                                                            echo "<option value='$one' $selected>$one</option>";
                                                    }
                                                    ?>

                                                </select>



                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Feed of Phase B" data-content="Three-phase electric power is a common method of alternating-current electric power generation, transmission, and distribution.[1] It is a type of polyphase system and is the most common method used by electrical grids worldwide to transfer power."  >
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="formBoxSectionWhite">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Feed of Phase C:</label>
                                            <div class="col-md-3">
                                      

                                                <select  name="feed_of_phase_C" class="form-control">
                                                    <?php


                                                    foreach ($letters as $one) {
                                                        $selected = '';
                                                        if( $one == 'C' ) {
                                                            $selected = 'selected';
                                                        }

                                                        for ($i = 0; $i < count($FEED_NAME);$i++ ) {



                                                            if($one == $FEED_NAME[$i] ) {
                                                                echo "  <br> <option value='$one' $selected>$one - Edit</option>";
                                                                $one = null;
                                                                break;
                                                            }
                                                        }
                                                        if($one != null )
                                                            echo "<option value='$one' $selected>$one</option>";
                                                    }
                                                    ?>

                                                </select>

                                            </div>
                                            <div class="col-md-6" >
                                                <div class="formTextSpacing">
                                                    <a  data-toggle="popover" title="Feed of Phase C" data-content="Three-phase electric power is a common method of alternating-current electric power generation, transmission, and distribution.[1] It is a type of polyphase system and is the most common method used by electrical grids worldwide to transfer power."  >
                                                        <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                                <!-- --> <!-- End of Tab 3 -->
                                <button type="submit" class="btn btn-primary" >Submit</button>
                           <!-- </div> -->

                    </form>

                </div>
            </div>
        </div>
            <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>