<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php
include_once ('functions/mysql_connect.php');
$gateway_id = mysqli_real_escape_string($conn, $_GET['gateway_id']);
$gateway_id=  filter_var($gateway_id, FILTER_SANITIZE_STRING);

include_once ('control/get_aq_sync.php');

?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Review Sync with Energy Review</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >


                            <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">URL of Acquisition Server:</label>
                                <div class="col-md-9">
                                    <div class="formTextSpacing">
                                        <?php echo  $URL_AQ; ?>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">URL of Meter Data:</label>
                                <div class="col-md-9">
                                    <div class="formTextSpacing">
                                        <?php echo  $URL_AQ_Meter; ?>
                                    </div>

                                </div>

                            </div>
                        </div>

                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Transmit frequency:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                            <?php echo $transmit_frequency; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Timeout:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $timeout; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Retries:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $retries; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Enable Reporting to ER:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php if($enabled ==1){
                                            echo "Reporting Enabled";
                                        }else {
                                            echo "Reporting Disabled";
                                        } ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Transmit Frequency to ER Server:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $inverter_er_rate; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">URL for Inverter:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $inverter_er_url; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Port to Send Inverter Reports:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $inverter_er_port; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alarm Transmit Frequency:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $alarms_trans_frequency; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBoxSection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Number of Inverter Reports per batch:</label>
                                <div class="col-md-3">
                                    <div class="formTextSpacing">
                                        <?php echo $reports_per_batch ; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <a class="btn btn-primary" href="/main.php" role="button">Finished</a>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>
