<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Power Factor Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript">


        $(document).ready(function(){


            //console.log('I live');
            auto_inverter_status();
            auto_scan_status();
//$('#moreLog').on('click', function() {
            $('#inverters-scan').on('click', function(e) {
              //  e.preventDefault();


                $.ajax({
                    type: "POST",
                    url: '/control/inverter_scan.php?requested=1',
                    //data: formDetails.serialize(),
                    success: function (data) {
                    console.log('success:' +data);
                        $(".findInverter").text("Started Search");
                        setInterval(function(){ $(".findInverter").text(" "); }, 4000);
                      //  e.preventDefault();
                      //  e.stopImmediatePropagation();
                    },
                    error: function(jqXHR, text, error){
                        $(".findInverter").text("hmm looks like a problem");
                       console.log('error:');
                    }
                });


                //return false;
            });

        });

        function auto_inverter_status(){
            $.ajax({
                url: "control/inverter_status.php",
                cache: false,
                success: function(data){
                    $("#inverter_status_div").html(data);
                }
            });
        }

        function auto_scan_status(){
            $.ajax({
                url: "control/inverter_scan_status.php",
                cache: false,
                success: function(data){
                    $("#inverter_scan_div").html(data);
                }
            });
        }


        //Refresh auto_load() function after 10000 milliseconds
        setInterval(auto_inverter_status,20000);
        setInterval(auto_scan_status,20000);





    </script>
</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$inverters_status_id = mysqli_real_escape_string($conn, $_GET['inverters_status_id']);
$inverters_status_id = filter_var($inverters_status_id, FILTER_SANITIZE_STRING);


include_once('control/get_inverters_scan_results.php'); // Get default data




?>
<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-md-6"><h1 style="padding-left: 30px;">Power Factor Config</h1></div>
                        <div class="col-md-6">
                            <div class="text-right" style="margin: 20px;">
                                <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                    <span class="glyphicon glyphicon-log-out"></span> Log out
                                </a>
                            </div></div>
                    </div>



                    <div style="padding: 30px;">


                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Enable/Disable Power Factor Regulation</h3></div>
                                    <div class="panel-body">

                                        <p></p>
                                        <p>  </p>
                                        <p class="text-center"><a href=""><button type="button" class="btn btn-warning">Enable</button></a>
                                        </p>
                                        <p>1 active/deactivate power factor control, feed A. Required.</p>
                                        <p>2 active/deactivate power factor control, feed B. Required.</p>
                                        <p>3 active/deactivate power factor control, feed C. Required.</p>


                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Power Factor by Feed</h3></div>
                                    <div class="panel-body">
                                        <p></p>
                                        <p>  </p>
                                        <p class="text-center"><a href=""><button type="button" class="btn btn-warning">Configure Inverters</button></a>
                                        </p>
                                        <p>1 enable/disable logging of power factor data and commanded values on feed A. Optional.</p>
                                        <p>2 enable/disable logging of power factor data and commanded values on feed B. Optiona</p>
                                        <p>3 enable/disable logging of power factor data and commanded values on feed C. Optional.</p>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Data logging Options</h3></div>
                                    <div class="panel-body">


                                        <p class="text-center"><a href=""><button type="button" class="btn btn-warning">Router Options</button></a>


                                        <p>1 enable/disable logging of study data on feed A. Optional.</p>
                                        <p>4.2 enable/disable logging of study data on feed B. Optional.</p>
                                        <p>4.3 enable/disable logging of study data on feed C. Optional.</p>


                                        </div>
                                    </div>


                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Find Inverters</h3></div>
                                    <div class="panel-body">


                                            <p class="text-center"><button  id ='inverters-scan' type="submit"  class="btn btn-primary" >Find Inverters</button></p>
                                        <div class="findInverter text-center"> </div>
                                            <div id="inverter_scan_div">

                                            </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>