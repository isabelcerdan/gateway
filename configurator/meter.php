<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Meters</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>

    <script type="text/javascript">
        $( document ).ready( function () {
            $(document).on("click", "a.delete-meter", function() {
                if (confirm('Are you sure  you want to delete ' + this.id + ' Meter?' )) {
                    $(location).attr('href', '/control/form_meter_add_edit.php?meter_id=' + this.id + '&delete_meter=yes');
                }
            });
        } );
    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$meter_id = '';
include_once ('control/get_meter.php'); // Get default data
//include_once ('control/get_brainboxes.php'); // Get default data


?>
<div class="container">
  
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Configure Meters</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <div class="panel-body">

                    <div class="formBoxSection">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="font-size: smaller">
                                    <th></th>
                                    <th></th>
                                    <th>Meter ID</th>
                                    <th>Meter Name</th>
                                    <th>Meter Role</th>
                                    <th>Meter Type</th>
                                    <th>Modbus #</th>
                                    <th>Interface</th>
                                    <th>Baud</th>
                                    <th>Net Comm Type</th>
                                    <th>Meter Delta</th>
                                    <th>CT Orientation Reversed</th>


                                </tr>
                                </thead>
                                <tbody id="auto_brainbox_status_div">
                                <?php
                                for($i = 0;$i <  count($meter_id_DB); $i++) {
                                    echo "<tr>
                                            <td class='text-center'><a href='meter-add-edit.php?meter_id=$meter_id_DB[$i]'> <button class=\"btn btn-warning\" >Edit</button></a></td>
                                            <td class='text-center'><a class='delete-meter' id='$meter_id_DB[$i]' > <button class=\"btn btn-danger\" >Delete</button></a></td>
                                            <td class='text-center'>$meter_id_DB[$i]</td>
                                            <td class='text-center'>$meter_name_DB[$i]</td>
                                            <td class='text-center'>$meter_role_DB[$i]</td>
                                            <td class='text-center'>$meter_type_DB[$i]</td>
                                            <td class='text-center'>$modbusdevicenumber_DB[$i]</td>
                                            <td class='text-center'>$address_DB[$i]</td>
                                            <td class='text-center'>$baud_DB[$i]</td>
                                            <td class='text-center'>$network_comm_type_DB[$i]</td>
                                            <td class='text-center'>$meter_delta_DB[$i]</td>
                                            <td class='text-center'>$ct_orientation_reversed_DB[$i]</td>
                                    ";
                                    echo "</tr>";
                                }

                                ?>

                                </tbody>
                            </table>
                        </div>


                    </div>




                    <div class="formBoxSection">

                        <a href='meter-add-edit.php'> <button class="btn btn-primary" >New Meter</button></a>
                  


                    </div>

                </div>
            </div>


            </div>
        </div>
    <?php    include_once ('footer.php'); ?>
</div>
    <!-- Row end -->

</div>
</body>
</html>