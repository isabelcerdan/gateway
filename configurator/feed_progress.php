<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:14 PM
 */
include_once('functions/mysql_connect.php');

$stringIds = mysqli_real_escape_string($conn, $_REQUEST['stringId']);
$stringIds = filter_var($stringIds, FILTER_SANITIZE_STRING);

$enabled = mysqli_real_escape_string($conn, $_REQUEST['enabled']);
$enabled = filter_var($enabled, FILTER_SANITIZE_STRING);

$error = mysqli_real_escape_string($conn, $_REQUEST['error']);
$error = filter_var($error, FILTER_SANITIZE_STRING);
//error=Process_still_running
?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">

<style>
    .progress {
        margin-bottom: 0px;
    }


    }
    ?>
</style>
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>



<script>

    function auto_feed_status(){
        $.ajax({
            url: "control/feeddetect/get_feed_string_progress.php",
            cache: false,
            success: function(data){
                $("#auto_feed_status_div").html(data);
            }
        });
    }

    function auto_feed_status2(){
        $.ajax({
            url: "control/feeddetect/get_feed_string_progress_bar.php",
            cache: false,
            success: function(data){
                $("#auto_feed_status2_div").html(data);
            }
        });
    }


    $(document).ready(function(){
        
        //
        //console.log('I live');
        auto_feed_status(); //Call auto_load() function when DOM is Ready
        auto_feed_status2(); //Call auto_load() function when DOM is Ready

        $('#feed-detect-cancel').on('click', function() {
            console.log('cancel:' );
            if (confirm("Feed Detect Cancel. Are you Sure?") == true) {
                $.ajax({
                    type: "POST",
                    url: 'control/feeddetect/cancel_feed_string.php?cancel_feed_detect=yes',
                    success: function (data) {
                        console.log('cancel:' );
                    },
                    error: function(jqXHR, text, error){
                        console.log('error:');
                    }
                });
                return false;
            }

        });


    });

    //Refresh auto_load() function after 10000 milliseconds
    setInterval(auto_feed_status,2000);
    setInterval(auto_feed_status2,2000);



</script>
<body>


<div class="container">
    <h2>Feed Detection - Scanning by String</h2>

    <?php if($error == 'Process_still_running') { ?>
        <div class="alert alert-dismissable alert-danger" id="scan-alert" style="display: block;">
            <button type="button" class="close" data-hide="alert" id="closeAlert">×</button>
            Please wait 5 minutes to run scan, right now the old scan is just finishing up.
        </div>
    <?php } ?>
    <div class="panel panel-default">

        <div style="margin-right: 50px; margin-left: 50px; margin-top: 20px; margin-bottom: 10px">

                <div id="auto_feed_status2_div"></div>




        </div>
        <div class="panel-body">

            <a  href="/control/feeddetect/start_feed_by_string.php?stringId=<?php echo $stringIds; ?>  &enabled=<?php echo $enabled; ?>&scan=new"> <button type="button" class="btn btn-warning"><span >Start New Scan</span></button></a>
            </p>
            <div class="table-responsive">
                <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                    <tr >
                        <th class="text-center">String Id</th>
                        <th class="text-center">Current Feed </th>
                        <th class="text-center">Discovered Feed</th>
                        <th class="text-center">Command</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Progress</th>
                    </tr>
                    <tbody id="auto_feed_status_div">


                    </tbody>
                    <?php
                    //print_r($serialNumber);
                    // $rowId = 0;
                    //include_once ('control/get_upgrade_progress.php'); // Get default data
                    ?>
                </table>
            </div>
            <div class=" text-right">
                <button id="feed-detect-cancel" type="button" class="btn btn-outline-secondary"  >Cancel</button>
            </div>

        </div>
    </div>
</div>


</body>
</html>