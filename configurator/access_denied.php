<?php
//include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Access Denied</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>
<?php

$IOFlex_id= mysqli_real_escape_string($conn, $_GET['IOFlex_id']);
$IOFlex_id = filter_var($IOFlex_id, FILTER_SANITIZE_STRING);



?>


<div class="container">
    <?php
    include_once ('header.php');

    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Access Denied</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >
                        <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">


                        <div class="formBoxSection">
                            <div class="form-group">
                                <div class="col-md-12" >
                                    <div class="formTextSpacing text-center"><h2>You don't have access rights for the previous page.</h2></div>
                                </div>





                            </div>
                        </div>



                        



           
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>
