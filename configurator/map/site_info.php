<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/28/2017
 * Time: 1:51 PM
 *  site_map_id int(10) unsigned not null auto_increment primary key,
site_id varchar(40) not null,
tbl_row smallint(6) not null,
tbl_col smallint(6) not null
 */
include('../functions/mysql_connect.php');


$site_map_id = mysqli_real_escape_string($conn, @$_REQUEST['site_map_id']);
echo
$site_name = mysqli_real_escape_string($conn, @$_REQUEST['site_name']);
$site_name = filter_var($site_name, FILTER_SANITIZE_STRING);

$site_id = mysqli_real_escape_string($conn, @$_REQUEST['site_id']);
$site_id = filter_var($site_id, FILTER_SANITIZE_STRING);

$tbl_row = mysqli_real_escape_string($conn, @$_REQUEST['tbl_row']);
$tbl_row = filter_var($tbl_row, FILTER_SANITIZE_STRING);

$tbl_col = mysqli_real_escape_string($conn, @$_REQUEST['tbl_col']);
$tbl_col = filter_var($tbl_col, FILTER_SANITIZE_STRING);

$delete = mysqli_real_escape_string($conn, $_REQUEST['delete']);
$delete = filter_var($delete, FILTER_SANITIZE_STRING);


if(($site_map_id != '')) {

    if($delete == 'yes'){
        $sql = ("Delete from site_map_config  WHERE site_map_id = '$site_map_id'"); // where site_map_id='$site_map_id'
        if (!mysqli_query($conn,$sql)) {
            header('Location: map_picker.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
            die('<br>Error: ' . mysqli_error($conn));
        }
        else {
            header('Location:  map_picker.php');
        }
        exit();
    }

    $sql = ("update site_map_config set tbl_row = '$tbl_row', tbl_col = '$tbl_col',site_name = '$site_name' WHERE site_map_id = '$site_map_id'"); // where site_map_id='$site_map_id'
    if (!mysqli_query($conn,$sql)) {
        header('Location: mapBuilder.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
        die('<br>Error: ' . mysqli_error($conn));
    }
    else {
        header('Location:  mapBuilder.php?site_map_id=' . $site_map_id );
    }

}else {

    $sql = "INSERT INTO site_map_config (tbl_row,tbl_col,site_name) values( '$tbl_row','$tbl_col','$site_name')";

    if (!mysqli_query($conn,$sql)) {
        header('Location: mapBuilder.php?errors=' . $error = $error .  " problem" . mysqli_error($conn) );
        die('<br>Error: ' . mysqli_error($conn));
    }
    else {
        header('Location:  mapBuilder.php?site_map_id=' . $conn->insert_id);
    }
}

