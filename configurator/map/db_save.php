<?php
session_start();
$site_map_id = $_SESSION["site_map_id"];


// include config with database definition

include('../functions/mysql_connect.php');
// input parameter is element to delete (suppress errors by adding a @ sign)
$p = @$_REQUEST['p'];


//$site_map_id = filter_var($site_map_id, FILTER_SANITIZE_STRING);
// explode input parameters:
// 0 - $sub_id - subject id
// 1 - $tbl1   - target table index
// 2 - $row1   - target row
// 3 - $col1   - target column
// 4 - $tbl0   - source table index
// 5 - $row0   - source row
// 6 - $col0   - source column
list($sub_id, $tbl1, $row1, $col1, $tbl0, $row0, $col0) = explode('_', $p);

// discard clone id part from the sub_id
$sub_id = substr($sub_id, 0, 12);

// if source table is 0 (element is dragged from "subject" table and dropped to the "timetable") then it should be inserted to the table

// Get ID from table and make sure we don't have duplicates

//$result = $conn->query("select sub_id from inverter_map WHERE sub_id = '$sub_id '");

$inDB = $conn->query("select * from saved_inverters WHERE serialNumber= '$sub_id '");
if(mysqli_num_rows($inDB )>0) {
	while ($row = $inDB->fetch_assoc()) {
		$id = $row['serialNumber'];
		$mac_address = $row['mac_address'];
		$stringId = $row['stringId'];
		$stringPosition = $row['stringPosition'];
		$groupId = $row['groupId'];
	}
}

if (($tbl0 == 0) ) {

	// commit transaction (sqlCommit is function from config.php)
	$sql = "insert into inverter_map (sub_id, tbl_row, tbl_col,stringId,stringPosition,site_map_id) values ('$sub_id', $row1, $col1,'$stringId','$stringPosition','$site_map_id')";


	//sqlQuery("insert into inverter_map (sub_id, tbl_row, tbl_col) values ('$sub_id', $row1, $col1)");

	$sqlDelete = ("update saved_inverters set display = 'no' where serialNumber ='$sub_id'");

	if (mysqli_query($conn,$sqlDelete)) {
		//header('Location: /meter.php');
		//die('exit');
	}
}
// else, element is moved to the new location
else {
//	sqlQuery("update inverter_map set tbl_row=$row1, tbl_col=$col1 where sub_id='$sub_id' and tbl_row=$row0 and tbl_col=$col0");
	$sql = ("update inverter_map set tbl_row=$row1, tbl_col=$col1 where sub_id='$sub_id' and tbl_row=$row0 and tbl_col=$col0");

}

if (!mysqli_query($conn,$sql)) {
	die('<br>Error: ' . mysqli_error($conn));
}
// commit transaction (sqlCommit is function from config.php)
//sqlCommit();



// no cache
header('Pragma: no-cache');
// HTTP/1.1


?>