<?php

// reset record set to null ($rs is used in timetable function)
$rs = null;

// function executes SQL statement and returns result set as Array
// $key defines column number to be set as array key (needed for fetch from inverter_map  $rs[1_2) -> Array (... ) 
function sqlQuery($sql, $key = NULL) {
	include('../functions/vault.php');
	$servername = "localhost";
	$username = $vault_username;
	$password = $vault_password;
	$database = $vault_database;
	include('../functions/mysql_connect.php');
	// execute query	
	$db_result = $conn->query($sql);
	// if db_result is null then trigger error
	if (!$db_result) {
		print "SQL failed: (" . $conn->errno . ") " . $conn->error;
		exit();
	}
	// prepare result array
	$resultSet = Array();
	// if resulted array isn't true and that is in case of select statement then open loop
	// (insert / delete / update statement will return true on success) 
	if ($db_result !== true) {
		// loop through fetched rows and prepare result set
		while ($row = $db_result->fetch_row()) {
			// if array key is defined
			if ($key !== NULL) {
				$resultSet[$row[$key]] = $row;
			}
			// array key is not needed
			else {
				$resultSet[] = $row;
			}
		}
	}
	// return result set
	return $resultSet;
}


// commit transaction
function sqlCommit() {
	include('../functions/mysql_connect.php');
	// commit transaction
	if (!$conn->commit()) {
		print("Transaction commit failed\n");
		exit();
	}
	// close database connection
	$conn->close();
}



// print subjects
function invertersList($site_map_id = '') {


	include('../functions/vault.php');
	$servername = "localhost";
	$username = $vault_username;
	$password = $vault_password;
	$database = $vault_database;
	include('../functions/mysql_connect.php');
	//echo "<br>var:" . $vault_username;




	//include('../functions/mysql_connect.php');
	// returned array is compound of nested arrays
	//$subjects = sqlQuery("select sub_id, sub_name,stringId,stringPosition from inverters_mapping_raw where display = 'yes' order by stringId");
	// print_r($subjects);

	//for($i=0; $i < count($subjects);$i++ ){
//echo $subjects[sub_id][$i];
	//	echo "<td class='dark'><div id='$subjects[sub_id][$i]' class='redips-drag redips-clone $id'>$id</div><input id='b_$id' class='$id'type='button' value='' onclick='redips.report('$id')' title='Show only $id'/></td>\n";
//$result = $conn->query("SELECT sub_id, sub_name,stringId,stringPosition FROM inverters_mapping_raw where display = 'yes' order by stringId");
	$result = $conn->query("SELECT  serialNumber, groupId,stringId,stringPosition FROM saved_inverters where display = 'yes' order by stringId,stringPosition");

	if(mysqli_num_rows($result)>0) {
		while ($row = $result->fetch_assoc()) {
			$id[] = $row['serialNumber'];
			$sub_name[] = $row['groupId'];
			$stringId[] = $row['stringId'];
			$stringPosition[] = $row['stringPosition'];
		}

		$newString = '';
		for($i=0;$i < count($id);$i++ ){
			//echo $stringId[$i] . ' - ';
			if($i > 0){
				$newString =  $stringId[$i-1];
			}else {
				$newString =  $stringId[0];

			}

			if($newString != $stringId[$i]) {

				echo "<tr> ";
			}
			//echo "<tr>";

			echo "<td class='redips-mark'><div id='" .$id[$i]."' class='fontSizeChange redips-drag redips-clone $id[$i]' title='$id[$i]'>" .substr($id[$i] , -7) . "<br> $stringId[$i] <b>$stringPosition[$i]</b></div><input id='b_$id[$i]' class='$id[$i]'type='button' value='' onclick='redips.report('$id[$i]')' title='Show only $id[$i]'/> </td>\n";
			if($newString != $stringId[$i]) {
				//echo "</tr>";  "-" . $site_map_id .
			}

		}
	}

}


// create timetable row
function timetable($hour, $row,$tbl_col, $site_map_id ) {
	global $rs;

	// if $rs is null then query database (this should be executed only once - first time)
	if ($rs === null) {
		// first column of the query is used as key in returned array
		//	$rs = sqlQuery("select concat(t.tbl_row,'_',t.tbl_col) as pos, t.tbl_id, t.sub_id, s.sub_name
		//				from inverter_map  t, inverters_mapping_raw s
		//				where t.sub_id = s.sub_id", 0);

		$rs = sqlQuery("select concat(t.tbl_row,'_',t.tbl_col) as pos, t.tbl_id, t.sub_id, t.sub_id AS sub_name, t.stringId, t.stringPosition
						from inverter_map  t
						 WHERE t.site_map_id ='$site_map_id'", 0);
	}

	print '<tr >';
	print '<td class="redips-mark dark fontSizeChange rowHeight">' . $hour . '</td>';
	// column loop starts from 1 because column 0 is for hours
	for ($col = 1; $col <= $tbl_col; $col++) {
		// create table cell
		print '<td >';
		// prepare position key in the same way as the array key looks
		$pos = $row . '_' . $col;
		// if content for the current table cell exists
		if (array_key_exists($pos, $rs)) {
			// prepare elements for defined position (it can be more than one element per table cell)
			$elements = $rs[$pos];
			// id of DIV element will start with sub_id and followed with 'b' (because cloned elements on the page have 'c') and with tbl_id
			// this way content from the database will not be in collision with new content dragged from the left table and each id stays unique
			$id = $elements[2] . 'b' . $elements[1];
			$name = $elements[3];
			$stringID = $elements[4];
			$stringPosition = $elements[5];
			$class = substr($id, 0, 12); // class name is only first 2 letters from ID
			print "<div id='$id' class='fontSizeChange redips-drag $class' title='$name'>" .substr($name , -7) . "<br> $stringID <b>$stringPosition</b></div>";

		}
		// close table cell
		print '</td>';
	}
	print "</tr>\n";
}

?>