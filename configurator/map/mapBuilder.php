<?php
session_start();
// include config with database definition
include('../functions/mysql_connect.php');
$site_map_id = mysqli_real_escape_string($conn, $_GET['site_map_id']);
$site_map_id_URL = filter_var($site_map_id, FILTER_SANITIZE_STRING);

if($site_map_id_URL != '') {
	$_SESSION["site_map_id"] = $site_map_id_URL;
}
$_SESSION["site_map_id"];

include ('get_grid.php');
include ('config_mysqli.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>

		<meta name="viewport" content="width=device-width, user-scalable=no"/><!-- "position: fixed" fix for Android 2.2+ -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/style.css?<?php echo rand(); ?>" type="text/css" media="screen"/>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/redips-drag-min.js"></script>
		<script type="text/javascript" src="js/script.js?<?php echo rand(); ?>"></script>




		<script>


			$(document).ready(function(){



				var fontSize =12;
				var columns = <?php echo $tbl_col; ?>;



				 $(".changeFontSize").on('click', function() {
					 fontSize = 5;
					 $(".fontSizeChange").css({"font-size": fontSize +"px"});
				 });
				

			});
		</script>


		<style>

			.rowHeight {
				height: 50px;
				width: 50px;
			}
		</style>
	</head>
	<body>
	<h1>Site Name: <?php echo $site_name; ?> </h1>
	<a href='map_picker.php?'>Setup New site </a><br>


<div class="fontSizeChange changeFontSize"></div>
	<br>

		<div id="main_container">

			<!-- tables inside this DIV could have draggable content -->
				<div id="redips-drag">

					<div class="container-fluid">
					<!-- left container -->
						<div class="row">
							<div class="col-sm-3 col-md-3 col-lg-3">
									<div id="left">
										<table id="table1">

											<tbody>


												<tr class="body"><td class="redips-trash" title="Reset Inverter" width="100%" colspan="3">Reset Inverter</td></tr class="body">
												<?php invertersList($site_map_id_URL) ?>
											</tbody>
										</table>
									</div><!-- left container -->
							</div>
						<!-- right container -->
							<div class="col-sm-9 col-md-9 col-lg-9">
									<div id="right">
										<table id="table2">

											<tbody>
												<tr>

													<td class="redips-mark blank">

													</td>

													<?php
													for($j =1; $j <= $tbl_row ; $j++) {
														echo "<td class='redips-mark dark rowHeight fontSizeChange'>$j</td>";
													}

													?>



												</tr>
												<?php
													for($i =1; $i <= $tbl_row ; $i++) {
														timetable($i, $i,$tbl_col,$site_map_id_URL);
													}

												?>

											</tbody>
										</table>
									</div><!-- right container -->
							</div>
						</div>
				<!-- drag container -->

			<br/>
			<div id="message">Drag school subjects to the timetable (clone subjects with SHIFT key)</div>

		</div><!-- main container -->

	</body>
</html>