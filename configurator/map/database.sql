# noinspection SqlNoDataSourceInspectionForFile
/*

Darko Bunic
http://www.redips.net/
Feb, 2010.

SQL script to create tables and initially insert subjects

*/

/* create timetable
 Add site ID
Add Gateway serial
redips_timetable
 */
create table inverter_map (
    tbl_id int(10) unsigned not null auto_increment primary key,
    sub_id varchar(40) not null,
    tbl_row smallint(6) not null,
    tbl_col smallint(6) not null
);


create table site_map_config (
    site_map_id int(10) unsigned not null auto_increment primary key,
    site_id varchar(40) not null,
    tbl_row smallint(6) not null,
    tbl_col smallint(6) not null
);

/* create subjects
Add site ID
Add Gateway serial

*/
create table inverters_mapping_raw (
    sub_id varchar(40) not null primary key,
    sub_name varchar(40) not null,
    display varchar(3),
    stringId int(11),
    stringPosition int(11)
);


/* fill subjects */
insert into redips_subject values
('en','English', 'yes'),
('ma','Mathematics', 'yes'),
('ph','Physics', 'yes'),
('bi','Biology', 'yes'),
('ch','Chemistry', 'yes'),
('it','IT', 'yes'),
('ar','Arts', 'yes'),
('hi','History', 'yes'),
('et','Ethics', 'yes');

insert into redips_subject values
('a','English'),
('b','Mathematics'),
('c','Physics'),
('d','Biology'),
('e','Chemistry'),
('f','IT'),
('g','Arts'),
('h','History'),
('i','Ethics');

insert into inverters_mapping_raw  values
('170','171451000170', 'yes','1','1'),
('171','171451000171', 'yes','1','2'),
('172','171451000172', 'yes','1','3'),
('173','171451000173', 'yes','2','1'),
('174','171451000174', 'yes','2','2'),
('175','171451000175', 'yes','2','3'),
('176','171451000176', 'yes','3','1'),
('177','171451000177', 'yes','3','2'),
('178','171451000178', 'yes','3','3');




