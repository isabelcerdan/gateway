<?php
// include config with database definition
//include('../config_mysqli.php');
include('../functions/mysql_connect.php');

// input parameter is element to delete (suppress errors by adding a @ sign) 
$p = @$_REQUEST['p'];

// explode input parameteres: id, row and column
list($sub_id, $row, $col) = explode('_', $p);

// discard clone id part from the sub_id
$sub_id = substr($sub_id, 0, 3);

// if row and col are numeric then delete from database (but only one row - limit 1)
if (is_numeric($row) && is_numeric($col)) {
	// delete element from database (only one row)
	//sqlQuery("delete from inverter_map where sub_id='$sub_id' and tbl_row=$row and tbl_col=$col limit 1");
	// commit transaction (sqlCommit is function from config.php)
	//sqlCommit();
	$sqlDelete = ("update inverters_mapping_raw set display = 'yes' where sub_id='$sub_id'");

	//$sqlInsert = "insert into inverter_map (sub_id, tbl_row, tbl_col) values ('$sub_id', $row1, $col1)";

	if (mysqli_query($conn,$sqlDelete)) {
		//header('Location: /meter.php');
		//die('exit');
	}


	$sql = ("delete from inverter_map where sub_id='$sub_id' and tbl_row=$row and tbl_col=$col limit 1");

	if (mysqli_query($conn,$sql)) {
		//header('Location: /meter.php');
		//die('exit');
	}
}

// no cache
header('Pragma: no-cache');
// HTTP/1.1
header('Cache-Control: no-cache, must-revalidate');
// date in the past
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

?>