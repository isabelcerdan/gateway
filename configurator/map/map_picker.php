<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 3/7/2018
 * Time: 2:24 PM
 */



include('../functions/vault.php');
$servername = "localhost";
$username = $vault_username;
$password = $vault_password;
$database = $vault_database;
include('../functions/mysql_connect.php');

$site_map_id_get = mysqli_real_escape_string($conn, $_GET['site_map_id']);
$site_map_id_get = filter_var($site_map_id_get, FILTER_SANITIZE_STRING);

$_SESSION["site_map_id"] = $site_map_id_get;
$result = $conn->query("SELECT * FROM site_map_config");


if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $site_map_id[] = $row['site_map_id'];
        $site_name[] = $row['site_name'];
        $site_id[] = $row['site_id'];
        $tbl_row[] = $row['tbl_row'];
        $tbl_col[] = $row['tbl_col'];
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Map Picker</title>
    

</head>
<body>

<h1>Map Picker</h1>

<p> Select Map</p>
<?php

for($i = 0; $i < count($site_map_id); $i++ ){
  echo "<a href='mapBuilder.php?site_map_id=" .$site_map_id[$i] ." '>Go to: </a> |  <a href='map_picker.php?site_map_id=" .$site_map_id[$i] ."'> Edit Site: |  <a href='site_info.php?site_map_id=" .$site_map_id[$i] ."&delete=yes'> Delete Site:</a> Name: " . $site_name[$i] . " | Col: " . $tbl_col[$i] . " | Row: " . $tbl_row[$i] . "<br>";

    if($site_map_id[$i] == $site_map_id_get) {
        $tbl_row_display =  $tbl_row[$i];
        $tbl_col_display =  $tbl_col[$i];
        $site_name_display =  $site_name[$i];
    }
}


?>
<a href='map_picker.php?'>New site: </a><br>


<p>Create/update map</p>
<form action="site_info.php" method="post">
    <p>Enter size of site</p>
    <input type="hidden" name="site_map_id" value="<?php echo  $site_map_id_get; ?>"/>
    <input type="text" name="tbl_row" placeholder="20"  value="<?php echo  $tbl_row_display ; ?>"><br>
    <input type="text" name="tbl_col" placeholder="20"  value="<?php echo  $tbl_col_display; ?>"><br>
    <input type="text" name="site_name" placeholder="Enter Name"  value="<?php echo  $site_name_display; ?>"><br>
    <input type="submit" value="Submit">
</form>

</body>
</html>
