<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Energy Review</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>


</head
<body>

<div class="container">

    <?php
       include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Integrate Energy Review</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>



                    <div style="padding: 30px;">


                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Configure Connection to Energy Review</h3></div>
                                    <div class="panel-body">

                                        <p></p>
                                        <p>  </p>
                                        <p class="text-center"><a href="sync_energyreview.php"><button type="button" class="btn btn-warning">Configure Energy Review</button></a>
                                        </p>




                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Pair Brainboxes & Meters to Energy Review </h3></div>
                                    <div class="panel-body">
                                        <p></p>
                                        <p>  </p>
                                        <p class="text-center"><a href="energyreview_met-brain.php"><button type="button" class="btn btn-warning">Pair with Energy Review</button></a>
                                        </p>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
          
            </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>