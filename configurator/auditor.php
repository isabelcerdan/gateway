<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>auditor</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        button, input[type="button"] {

            text-shadow: none !important;
        }
        #stopSystem {
            display: none;
        }

        .smallerText {
            font-size: xx-small;
        }
        #Update_Success {
            display: none;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript">


        $(document).ready(function(){


            $(".startStopSystem").on('click', function() {
                
                var formDetails = $('#'+this.id);
                    $.ajax({
                        type: "POST",
                        url: 'control/form_auditor_enable.php',
                        data: formDetails.serialize(),
                        success: function (data) {
                            console.log('cancel:' );
                        },
                        error: function(jqXHR, text, error){
                            console.log('error:');
                        }
                    });
                    return false;

            });

            //Refresh auto_load() function after 10000 milliseconds
            setInterval(auto_gateway_status,2000);

            function auto_gateway_status(){

                $.ajax({
                    url: "control/get_auditor_status.php?data=yes",
                    cache: false,
                    success: function(data){

                        $("#auto_gateway_status_div").html(data);

                        // console.log(' success auto_gateway_status:' + $("#auto_gateway_status_div").html());

                        if( $("#auto_gateway_status_div").html() == 'Auditor Stopped') {
                            console.log('System Offline');

                            $('#startSystem').show();
                            $('#stopSystem').hide();
                        }
                        if( $("#auto_gateway_status_div").html() == 'Auditor Running') {
                            console.log('System Online');
                            $('#stopSystem').show();
                            $('#startSystem').hide();
                        }

                    }
                });
            }

        });

    </script>
</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$update= mysqli_real_escape_string($conn, $_GET['update']);
$update = filter_var($update, FILTER_SANITIZE_STRING);

$minion_auditor_idURL = mysqli_real_escape_string($conn, $_GET['minion_auditor_id']);
$minion_auditor_idURL = filter_var($minion_auditor_idURL, FILTER_SANITIZE_STRING);
$minion_auditor_idURL = $minion_auditor_idURL -1;
if($update == 'success') {
?>
<script type="text/javascript">


    $(document).ready(function(){

        $('#Update_Success').show();
        setTimeout(
            function()
            {
                $('#Update_Success').fadeOut(1000);
            }, 5000);
    });

</script>}

<?php
}

include_once('control/get_minion_auditor.php'); // Get default data

?>
<div class="container">


    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Manage auditors</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>


                    <div style="padding: 30px;">


                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Main Auditor Control</h3></div>
                                    <div class="panel-body">
                                        <p><strong>System State:</strong> <span id="auto_gateway_status_div"></span></p>

                                        <form class='startStopSystem' id="startSystem"  name="startStopSystem"  >
                                            <input type="hidden" name="system_suspended" value="YES">


                                            <p class="text-center"> <button type="submit" class="btn btn-warning">Start Auditor Now</button></p>
                                        </form>


                                        <form class='startStopSystem' id="stopSystem"  name="startStopSystem" >
                                            <input type="hidden" name="system_suspended" value="NO">


                                            <p class="text-center"> <button type="submit" class="btn btn-warning">Suspended Auditor Now</button></p>
                                        </form>




                                    </div>
                                </div>
                            </div>


                        </div>

                        <div id="Update_Success">
                            <div class="alert alert-success">
                                <strong>Success!</strong> <?php echo $titleDB[$minion_auditor_idURL]; ?> Changed
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Minion Auditor Control</h3></div>
                                    <div class="formBoxSection">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr style="font-size: smaller">
                                                   
                                                    <th></th>
                                                    <th>ID</th>
                                                    <th>Title</th>
                                                    <th>Runs</th>
                                                    <th>Frequency</th>
                                                    <th>Next_launch_time</th>
                                                    <th>Completion_Time</th>
                                                    <th>results_path_file_nameDB</th>
                                                    <th>Time</th>
                                                    <th>Active</th>
                                                    <th>Email</th>



                                                </tr>
                                                </thead>
                                                <tbody id="auto_brainbox_status_div">
                                                <?php
                                                for($i = 0;$i <  count($minion_auditor_idDB); $i++) {
                                                    echo "<tr>
                                            <td class='text-center'><a href='minion_auditor-add-edit.php?minion_auditor_id=$minion_auditor_idDB[$i]'> <button class=\"btn btn-warning\" >Edit</button></a></td>
               
                                            <td class='text-center'>$minion_auditor_idDB[$i]</td>
                                        <td class='text-center'>$titleDB[$i]</td>
                                        <td class='text-center'>$runsDB[$i]</td>
                                         <td class='text-center'>$frequencyDB[$i]</td>
                                         <td class='text-center'>$next_launch_timetDB[$i]</td>
                                         <td class='text-center'>$completion_timeDB[$i]</td>
                                         <td class='text-center'><a target='_blank' href='control/display_file.php?minion_auditor_id=$minion_auditor_idDB[$i]' <span class='glyphicon glyphicon-list-alt'></span></a> $results_path_file_nameDB[$i]</td>
                                         
                                         <td class='text-center'>$time_valueDB[$i]</td>
                                         <td class='text-center'>$activeDB[$i]</td>
                                         <td class='text-center'>$emailDB[$i]</td>
                              
                                
                                         
                                        
                                    ";
                                                    echo "</tr>";
                                                }

                                                ?>

                                                </tbody>
                                            </table>
                                        </div>


                                    </div>



                                </div>

                            </div>




                        </div>
                    </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>