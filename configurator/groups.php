<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');
include_once ('control/get_groups.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Groups</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

</head
<body>

<div class="container">

    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-8"><h1 style="padding-left: 30px;">Groups</h1></div>
                    <div class="col-md-4">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>



                <div style="padding-left: 30px; padding-right: 30px;">


                    <div class="row">

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Add Groups</h3></div>
                                <div class="panel-body">

                                    <p></p>
                                    <p>  </p>
                                    <p class="text-center"><a href="groups_create.php"><button type="button" class="btn btn-warning">Groups</button></a></p>
                                    <p>  </p>


                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-default">



                                <div class="panel-heading"><h3 class="text-center">Add Inverters to Groups</h3></div>
                                <div class="panel-body">

                                    <p></p>
                                    <p>  </p>
                                    <p class="text-center"><a href="groups-add.php"><button type="button" class="btn btn-warning">Inverters</button></a></p>
                                    <p>  </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Current Groups</h3></div>
                                <div class="panel-body">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Group ID</th>
                                            <th>Group Name</th>
                                            <th>Inverters</th>
                                            <th>Fallback</th>
                                            <th>NTP</th>
                                            <th>Forced Upgrade</th>
                                            <th>Irradiance</th>
                                            <th>Gateway Control</th>
                                            <th>Sleep</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php

                                        for($i = 0; $i < count($group_id); $i++) {
                                            echo "<tr>";
                                            echo "<td>" . $group_id[$i] . "</td>";
                                            echo "<td>" . $alias[$i] . "</td>";
                                            echo "<td>" . $group_count[$i] . "</td>";
                                            echo "<td>" . $fallback[$i] . "</td>";
                                            echo "<td>" . $ntp_url_sg424[$i] . "</td>";
                                            echo "<td>" . $forced_upgrade[$i] . "</td>";
                                            echo "<td>" . $irradiance[$i] . "</td>";
                                            echo "<td>" . $gateway_control[$i] . "</td>";
                                            echo "<td>" . $sleep[$i] . "</td>";
                                            echo "<td><a href=\"groups_create.php?group_id=" . $group_id[$i] . "\"><button type=\"button\" class=\"btn btn-warning\">Edit</button></a></td>";
                                            echo "<td><a href=\"control/form_groups_add_edit.php?group_id=" . $group_id[$i] . "&delete=yes\"><button type=\"button\" class=\"btn btn-danger\">Delete</button></a></td>";
                                        }

                                        ?>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>



                    </div>


                </div>
            </div>
        </div>
    </div>
    <?php    include_once ('footer.php'); ?>
</div>


</div>
</body>
</html>
