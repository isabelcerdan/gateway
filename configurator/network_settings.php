<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Network Settings</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">



    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>




</head
<body>
<?php
include_once ('functions/mysql_connect.php');
include_once ('functions/get_dhcp_info.php');
include_once('control/get_network_settings.php');

$status = mysqli_real_escape_string($conn, $_GET['status']);
$status= filter_var($status, FILTER_SANITIZE_STRING);

$restart = mysqli_real_escape_string($conn, $_GET['restart']);
$restart= filter_var($restart, FILTER_SANITIZE_STRING);



if($status == 'success') {

    $stringResponse = '<strong>Success!</strong> The network settings have been updated';
    $alertString = 'alert-success';
}
if($status == 'error') {
    $stringResponse = '<strong>Failure</strong> Hmm... something went wrong';
    $alertString =  'alert-danger';
}
?>


<script type="text/javascript" class="init">


    $(document).ready(function (){
        $('#FixedIPOptions').hide();
        $('#inet').change(function(){


                if ($('#inet').val() == 'static') {
                    var r = confirm("Are you sure this site needs a static IP Address");
                    if (r == true) {
                        $('#FixedIPOptions').show();
                    }
                } else {
                    $('#FixedIPOptions').hide();
                }

        });

        if('<?php echo $inet; ?>' == 'static'){
            $('#FixedIPOptions').show();
        }
   
        $.validator.addMethod( "ipv4", function( value, element ) {
            return this.optional( element ) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test( value );
        }, "Please enter a valid IP v4 address." );

        $.validator.addMethod( "macAddress", function( value, element ) {
            return this.optional( element ) || /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/i.test( value );
        }, "Please enter a valid mac Address address." );

        validateIPs = false;
        $( "#network_settings_update" ).validate({
            rules: {
                understand: {
                    required: true
                },
                address: {
                    required: validateIPs
                },
                netmask: {
                    required: validateIPs
                },
                //broadcast: {
                //    required: validateIPs
                //},
                gateway: {
                    required: validateIPs
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".col-md-3" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".col-md-3" ).addClass( "has-success" ).removeClass( "has-error" );
            }
        });

    });

</script>
<style>
 .has-error{ color: red};
 .has-success {  color: green}
 #understand-error { color: red};

</style>
<div class="container">


    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Network Settings</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                <?php
                if( ( $status != '')) {
                 ?>
                <div style="padding-left: 30px; padding-right: 30px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert <?php echo $alertString; ?>" ?>
                                <?php echo $stringResponse; ?></php>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>


                <div class="panel-body">
                    <form class="form-horizontal row-border" name=network_settings_update" id="network_settings_update"  action="/control/form_network_settings_update.php" method="post">

                        <div class="formBoxSection ">
                            <div class="form-group ">
                                <label class="col-md-3 control-label">iface:</label>
                                <div class="col-md-3 ">
                                    <div style="margin-top: 6px">
                                        <?php echo $iface; ?>
                                    </div>

                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-md-3 control-label">inet:</label>
                                <div class="col-md-3 ">
                                    <select name="inet" id="inet" class="form-control" required>
                                        <?php
                                        if($iface !='') {
                                            echo "<option value='$inet' >$inet</option>";
                                        }else {
                                            echo "<option value='' >Select inet</option>";
                                        }
                                        ?>
                                        <option value='dhcp' >dhcp</option>
                                        <option value='static' >static</option>
                                    </select>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
                            <div id="FixedIPOptions">
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Fixed IP Address:</label>
                                    <div class="col-md-3 ">
                                        <input type="text" name="address" id="address" class=" form-control "   value="<?php echo $address; ?>" required pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$">

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Net mask:</label>
                                    <div class="col-md-3 ">
                                        <input type="text" name="netmask" id="netmask" class=" form-control "   value="<?php echo $netmask; ?>" required pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$">

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Broadcast:</label>
                                    <div class="col-md-3 ">
                                        <input type="text" name="broadcast" id="broadcast" class=" form-control "   value="<?php echo $broadcast; ?>" required pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$">

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div-->
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Default Gateway:</label>
                                    <div class="col-md-3 ">
                                            <input type="text" name="gateway" id="gateway" class=" form-control "   value="<?php echo $gateway; ?>" required pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$">
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-md-3 control-label">Understand:</label>
                                <input type="checkbox" name="understand" id="understand" required> Yes I understand what I am doing <br>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" >Change Network Settings</button>

                    </form>
                </div>







            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>