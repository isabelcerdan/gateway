<?php


include_once('functions/session.php');


include_once('functions/mysql_connect.php');

$power_regulator_id_form = mysqli_real_escape_string($conn, $_REQUEST['id']);
$power_regulator_id_form = filter_var($power_regulator_id_form, FILTER_SANITIZE_STRING);

$errors = mysqli_real_escape_string($conn, $_GET['errors']);
$errors = filter_var($errors, FILTER_SANITIZE_STRING);

$setToDefault = mysqli_real_escape_string($conn, $_GET['setToDefault']);
$setToDefault = filter_var($setToDefault, FILTER_SANITIZE_STRING);

if (strpos($errors, 'no_affected_rows') !== false) {
    $changes = "No Changes Made";
}


include_once('functions/feed_names.php'); // Get default data
include_once('control/get_meter.php');
include_once('control/get_meter_feed.php');
include_once('control/get_system.php');
include_once ('control/get_battery_int.php');


$power_regulator_id = $power_regulator_id_form;
include_once('control/get_power_regulator.php'); // Get default data or current data for Feed


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Gateway Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 0px;
        }

        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;

            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        .popover {


        }

        .topCalBoxes {
            margin-top: 10px;
        }

        .boldText {
            font-weight: bold;
        }

        p {
            margin-top: 5px;
        }
        .error{
            text-align: center;
            color: red;
        }

    </style>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script>
        var gatewayPerspective  = '<?php echo $gateway_perspective; ?>';
        console.log('gatewayPerspective: ' + gatewayPerspective );
        $(document).ready(function () {

            var meterRoleSaved ;
            var pccArray = [];
            var feedSelectSaved;

            var targets_pcc_A_on = false;
            var limit_pcc_A_on = false;

            $(".feed_settings").show();
            $("#feed_settings_A").hide();
            $("#feed_settings_B").hide();
            $("#feed_settings_C").hide();
            $("#feed_settings_Delta").hide();
            $("#batteryGroup").hide();


            $('#has_storage').change(function() {
                if(this.checked) {
                    $("#batteryGroup").show();
                }else{
                    $("#batteryGroup").hide();
                }
            });


            $(".pcc_meter").hide();
            $(".pcc2_meter").hide();
            $(".combo_meter").hide();


            $('#pcc_meter').change(function() {
                if(this.checked) {
                    $(".pcc_meter").show();
                    pccArray.push('pcc');
                    console.log("Checked " + $('#pcc_meter').val() + ' Array: ' + pccArray.toString() );
                }else{
                    pccArray.splice( $.inArray('pcc', pccArray), 1 );
                    $(".pcc_meter").hide();
                    console.log("NOT Checked " + $('#pcc_meter').val() + ' Array: ' +pccArray.toString() );
                }
            });

            $('#pcc2_meter').change(function() {
                if(this.checked) {
                    $(".pcc2_meter").show();
                    pccArray.push('pcc2');
                    console.log("Checked " + $('#pcc2_meter').val()+ ' Array: ' + pccArray.toString() );
                }else{
                    $(".pcc2_meter").hide();
                    pccArray.splice( $.inArray('pcc2', pccArray), 1 );
                    console.log("NOT Checked " + $('#pcc2_meter').val()+ ' Array: ' + pccArray.toString() );
                }
            });

            $('#pcc_combo_meter').change(function() {
                if(this.checked) {
                    $(".combo_meter").show();
                    $(".pcc_meter").show();
                    $(".pcc2_meter").show();
                    pccArray.push('pcc_combo');
                    console.log("Checked " + $('#pcc_combo_meter').val()+ ' Array: ' + pccArray.toString() );
                }else{
                    $(".combo_meter").hide();
                    $(".pcc_meter").hide();
                    $(".pcc2_meter").hide();
                    pccArray.splice( $.inArray('pcc_combo', pccArray), 1 );
                    console.log("NOT Checked " + $('#pcc_combo_meter').val()+ ' Array: ' + pccArray.toString() );
                }
            });

            $('#checkbox1').change(function() {
                if(this.checked) {
                    var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", returnVal);
                }
                $('#textbox1').val(this.checked);
            });

            $("#select_feed_name").change(function(){
                feedSelect =  $("#select_feed_name").val();

                if(feedSelect == 'A'){
                    $("#feed_settings_A").show();
                    $("#feed_settings_B").hide();
                    $("#feed_settings_C").hide();
                    $("#feed_settings_Delta").hide();
                    validateFields(pccArray ,feedSelect);
                } else if(feedSelect == 'B'){
                    $("#feed_settings_A").hide();
                    $("#feed_settings_B").show();
                    $("#feed_settings_C").hide();
                    $("#feed_settings_Delta").hide();
                    validateFields(pccArray ,feedSelect);
                }else if(feedSelect == 'C'){
                    $("#feed_settings_A").hide();
                    $("#feed_settings_B").hide();
                    $("#feed_settings_C").show();
                    $("#feed_settings_Delta").hide();
                    validateFields(pccArray,feedSelect);
                }
                else if(feedSelect == 'Aggregate'){
                    $("#feed_settings_Delta").show();
                    $("#feed_settings_A").show();
                    $("#feed_settings_B").show();
                    $("#feed_settings_C").show();
                    validateFields(pccArray,feedSelect);

                }
                feedSelectSaved = feedSelect;

            });

            function validateFields(meterRole,feed) {

                if ( (feed == 'A')) {
                    if ((meterRole.includes('pcc'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc_A').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_A'
                        });
                        $('#targets_pcc_A').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if ((meterRole.includes('pcc2'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc2_A').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc2_A'
                        });
                        $('#targets_pcc2_A').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if (meterRole.includes('pcc_combo')) {
                        $('#limit_pcc_combo_A').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_combo_A'
                        });
                        $('#targets_pcc_combo_A').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                }
                if (feed == 'B') {
                    if ((meterRole.includes('pcc'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc_B').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_B'
                        });
                        $('#targets_pcc_B').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if ((meterRole.includes('pcc2'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc2_B').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc2_B'
                        });
                        $('#targets_pcc2_B').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if (meterRole.includes('pcc_combo')) {
                        $('#limit_pcc_combo_B').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_combo_B'
                        });
                        $('#targets_pcc_combo_B').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                }
                if (feed == 'C') {
                    if ((meterRole.includes('pcc'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc_C').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_C'
                        });
                        $('#targets_pcc_C').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if ((meterRole.includes('pcc2'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc2_C').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc2_C'
                        });
                        $('#targets_pcc2_C').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if (meterRole.includes('pcc_combo')) {
                        $('#limit_pcc_combo_C').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_combo_C'
                        });
                        $('#targets_pcc_combo_C').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                }
                if (feed== 'Aggregate'){
                    if ((meterRole.includes('pcc'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc_Delta').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_Delta'
                        });
                        $('#targets_pcc_Delta').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if ((meterRole.includes('pcc2'))  || (meterRole.includes('pcc_combo')) ) {
                        $('#limit_pcc2_Delta').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc2_Delta'
                        });
                        $('#targets_pcc2_Delta').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                    if (meterRole.includes('pcc_combo')) {
                        $('#limit_pcc_combo_Delta').rules('add', {
                            required: true,
                            number: true,
                            oneGreaterThanTwo: '#targets_pcc_combo_Delta'
                        });
                        $('#targets_pcc_combo_Delta').rules('add', {
                            required: true,
                            number: true
                        });
                    }
                }

            }

            $('[data-toggle="popover"]').popover();


            $("#feedForm").validate(
                {
                    rules: {
                        power_regulator_name: {
                            required: true

                        },
                        select_meter_role: {
                            required: true

                        },select_feed_name:{
                            required: true

                        }
                    },
                    errorElement : 'div',
                    errorLabelContainer: '.errorTxt'
                }) ;

            $.validator.addMethod("oneGreaterThanTwo", function(value, element, param) {
                console.log('v:' + value + ' e:' + $(param).val());
                if(gatewayPerspective == 'production') {
                    if( parseInt(value) >= parseInt($(param).val())) return 1;
                }else{
                    if(parseInt($(param).val()) >= parseInt(value) ) return 1;
                }
            }, "One has to be greater than two");

            <?php

            if ($has_storage == "1") {
                echo '$("#batteryGroup").show();';
            }

            // foreach ($feed_nameDB as $feedDisplay):
            for($i = 0; $i < count($power_regulator_idDB); $i++) {
                echo "console.log('Count:$i $feed_nameDB[$i] $meter_roledDB[$i]');";
                if ( ($feed_nameDB[$i] == 'A')  OR  ($pr_feed_nameDB[$i] =='Delta') ) {

                   // echo "; console.log('Loop A  $feed_nameDB[$i] | $meter_roledDB[$i]');";
                    echo '$("#feed_settings_A").show();';
                    echo "validateFields('" . $meter_roledDB[$i] ."','" . $feed_nameDB[$i]. "');";

                    if(($meter_roledDB[$i] == 'pcc')) {
                        echo "console.log('Meter Role: $meter_roledDB[$i]');";
                        echo '$(".pcc_meter").show();';
                        if($feed_nameDB[$i] == 'A') {
                            $power_target_pcc_A = getTarget( $target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_A= $thresholdDB[$i];
                        }

                    }
                    if(($meter_roledDB[$i] == 'pcc2')) {
                        echo "console.log('Meter Role: $meter_roledDB[$i]');";
                        echo '$(".pcc2_meter").show();';
                        if($feed_nameDB[$i] == 'A') {
                            $power_target_pcc2_A = getTarget( $target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc2_A= $thresholdDB[$i];
                        }
                    }
                    if( $meter_roledDB[$i]  == 'pcc_combo') {
                        echo "console.log('Meter Role: $meter_roledDB[$i]');";
                        echo '$(".combo_meter").show();';
                        if($feed_nameDB[$i] == 'A') {
                            $power_target_pcc_combo_A = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_combo_A = $thresholdDB[$i];
                        }
                    }
                }
                if (($feed_nameDB[$i] == 'B') OR  ($pr_feed_nameDB[$i] =='Delta')){
                    //echo "; console.log('Loop B  $feed_nameDB[$i] | $meter_roledDB[$i]');";
                    echo '$("#feed_settings_B").show();';
                    echo "validateFields('" . $meter_roledDB[$i] ."','" . $feed_nameDB[$i]. "');";
                    if(($meter_roledDB[$i] == 'pcc')) {
                        echo '$(".pcc_meter").show();';
                        if($feed_nameDB[$i] == 'B') {
                            $power_target_pcc_B = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_B = $thresholdDB[$i];
                        }
                    }
                    if(($meter_roledDB[$i] == 'pcc2')) {
                        echo '$(".pcc2_meter").show();';
                        if($feed_nameDB[$i] == 'B') {
                            $power_target_pcc2_B = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc2_B = $thresholdDB[$i];
                        }
                    }
                    if( $meter_roledDB[$i]  == "pcc_combo") {
                        echo '$(".combo_meter").show();';
                        if($feed_nameDB[$i] == 'B') {
                            $power_target_pcc_combo_B = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_combo_B = $thresholdDB[$i];
                        }
                    }
                }
                if (($feed_nameDB[$i] == 'C') OR  ($pr_feed_nameDB[$i] =='Delta') ) {
                    //echo "; console.log('Loop C  $feed_nameDB | $meter_roledDB[$i]');";
                    echo '$("#feed_settings_C").show();';
                    echo "validateFields('" . $meter_roledDB[$i] ."','" . $feed_nameDB[$i]. "');";
                    if(($meter_roledDB[$i] == 'pcc')) {
                        echo '$(".pcc_meter").show();';
                        if($feed_nameDB[$i] == 'C') {
                            $power_target_pcc_C = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_C = $thresholdDB[$i];
                        }
                    }
                    if(($meter_roledDB[$i] == 'pcc2')) {
                        echo '$(".pcc2_meter").show();';
                        if($feed_nameDB[$i] == 'C') {
                            $power_target_pcc2_C = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc2_C = $thresholdDB[$i];
                        }
                    }
                    if( $meter_roledDB[$i]  == "pcc_combo") {
                        echo '$(".combo_meter").show();';
                        if($feed_nameDB[$i] == 'C') {
                            $power_target_pcc_combo_C = getTarget($target_offsetDB[$i], $thresholdDB[$i]);
                            $limit_pcc_combo_C = $thresholdDB[$i];
                        }
                    }

                }
                if ($pr_feed_nameDB[$i] =='Delta') {
                    //echo "; console.log('Loop Delta  $feed_nameDB | $meter_roledDB[$i]');";
                    echo '$("#feed_settings_Delta").show();';
                    echo "validateFields('" . $meter_roledDB[$i] ."','" . $feed_nameDB[$i]. "');";
                    if(($meter_roledDB[$i] == 'pcc')) {
                        echo '$(".pcc_meter").show();';
                        $power_target_pcc_Delta = getTarget( $target_offsetDB[$i], $thresholdDB[$i]);
                        $limit_pcc_Delta= $thresholdDB[$i];
                    }
                    if(($meter_roledDB[$i] == 'pcc2')) {
                        echo '$(".pcc2_meter").show();';
                        $power_target_pcc2_Delta = getTarget( $target_offsetDB[$i], $thresholdDB[$i]);
                        $limit_pcc2_Delta= $thresholdDB[$i];
                    }
                    if( $meter_roledDB[$i]  == "pcc_combo") {
                        echo '$(".combo_meter").show();';
                        $power_target_pcc_combo_Delta = getTarget( $target_offsetDB[$i], $thresholdDB[$i]);
                        $limit_pcc_combo_Delta= $thresholdDB[$i];
                    }
                }
            }

            function getTarget($target_offset, $threshold) {
                $target =$threshold - $target_offset;
                return $target;
            }
            ?>


        });

    </script>
    <script type="text/javascript" src="js/feed-add-edit.js?<?php echo rand(); ?>"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.4.min.js"></script>


</head
<body>


<?php


?>


<div class="container">

    <?php
    include_once ('header.php');

    // Menu Link //
    include_once('menu.php'); // Get default data
    ?>
    <!-- Row start -->



    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-10"><h1 style="padding-left: 30px;">Power Regulator</h1></div>
                    <div class="col-md-2">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php" class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="formBoxSection" style="background-color: #eee">
                        <form class="form-horizontal row-border" action="/control/form_power_regulator.php" method="post"
                              id="feedForm" name="feedForm">
                            <input type="hidden" name="gateway_id" value="<?php echo $gateway_id; ?>">
                            <input type="hidden" name="id" value="<?php echo $power_regulator_idDB[0]; ?>">
                            <input hidden id="gateway-perspective" name="gateway-perspective" value="<?php echo $gateway_perspective; ?>"/>

                            <div class="formBoxSection">
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <div class="formTextSpacing text-center">
                                                <h2> Configure Power Regulator <?php echo $nameDB[0]; ?> </h2>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="col-md-4 control-label" for="power_regulator_name">Name:</label>
                                            <div class="col-md-8 topCalBoxes">
                                                <input class="form-control inputUpdate" type="text" id="power_regulator_name" name="power_regulator_name"
                                                       value="<?php echo $nameDB[0]; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="formTextSpacing">
                                                <a data-toggle="popover" title="Power Regulator Name"
                                                   data-content="Used to identify a Power Regulator.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </a>Name used to uniquely identify this Power Regulator.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php $selectedMeter = array_unique($meter_roledDB);
                                            ?>

                                            <label for="select-feed-name" class="col-md-4 control-label">Meter:</label>
                                            <div class="col-md-8 topCalBoxes">
                                                PCC <input type='checkbox' id='pcc_meter' name='pcc_meter' value='pcc'
                                                    <?php if (in_array("pcc", $selectedMeter)) { echo "checked";} ?>>
                                                PCC2 <input type='checkbox' id='pcc2_meter' name='pcc2_meter' value='pcc2'
                                                    <?php if (in_array("pcc2", $selectedMeter)) { echo "checked";} ?> >
                                                PCC Combo <input type='checkbox'  id='pcc_combo_meter'  name='pcc_combo_meter' value='pcc_combo'
                                                    <?php if (in_array("pcc_combo", $selectedMeter)) { echo "checked";} ?>>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="formTextSpacing">
                                                <a data-toggle="popover" title="Feed Name"
                                                   data-content="Select meter will be regulated">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </a>Select meters to regulate
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="formBoxSectionWhite">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="select-feed-name" class="col-md-4 control-label">Feed:</label>
                                            <div class="col-md-8 topCalBoxes">
                                                <select class="form-control" id="select_feed_name" name="select_feed_name">
                                                    <option value="" <?php if ($pr_feed_nameDB[0] == '') echo "selected='selected'"; ?>>Select a Feed</option>
                                                    <option value="A" <?php if ($pr_feed_nameDB[0] == 'A') echo "selected='selected'"; ?>>A</option>
                                                    <option value="B" <?php if ($pr_feed_nameDB[0]  == 'B') echo "selected='selected'"; ?>>B</option>
                                                    <option value="C" <?php if ($pr_feed_nameDB[0]  == 'C') echo "selected='selected'"; ?>>C</option>
                                                    <option value="Aggregate" <?php if ($pr_feed_nameDB[0] == 'Delta') echo "selected='selected'"; ?>>Aggregate</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="formTextSpacing">
                                                <a data-toggle="popover" title="Feed Name"
                                                   data-content="Select which feed will be regulated.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </a>Select which feed will be regulated.
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-offset-1 col-md-10">
                                            <table class="table" style="margin-left: 10px; margin-top: 25px">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th colspan="2" class="pcc_meter">PCC Meter</th>
                                                    <th colspan="2" class="pcc2_meter">PCC2 Meter</th>
                                                    <th colspan="2" class="combo_meter">PCC COMBO Meter</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th class="pcc_meter">Power Target
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                    <th class="pcc_meter">Power Limit
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                    <th class="pcc2_meter">Power Target
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                    <th class="pcc2_meter">Power Limit
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                    <th class="combo_meter">Power Target
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                    <th class="combo_meter">Power Limit
                                                        <a data-toggle="popover" title="Power Target"
                                                           data-content="The power target defines the level in kilowatts that the Power Regulator is targeting. When meter readings are above the target, the Power Regulator will curtail solar production and/or increase charge on available batteries. When meter readings are below the target, the regulator will ramp solar production and/or decrease charge on available batteries.">
                                                            <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                        </a>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr id='feed_settings_A' class='feed_settings'><td>A</td>
                                                    <td class='input-target-cell pcc_meter'><div class='input-group input-feed '>
                                                            <input class='form-control text-right input-target' name='targets_pcc_A' id='targets_pcc_A' type='text' value='<?php echo $power_target_pcc_A;  ?>' />
                                                            <div class='input-group-addon '>kW</div></div>
                                                    </td>
                                                    <td class='input-limit-cell pcc_meter'><div class='input-group input-feed '>
                                                            <input class='form-control text-right input-limit' name='limit_pcc_A' id='limit_pcc_A' type='text' value='<?php echo $limit_pcc_A; ?>' /><div class='input-group-addon'>kW</div></div>
                                                    </td>
                                                    <td class='input-target-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                            <input class='form-control text-right input-target' name='targets_pcc2_A' id='targets_pcc2_A' type='text' value='<?php echo $power_target_pcc2_A; ?>' /><div class='input-group-addon'>kW</div></div>
                                                    </td>
                                                    <td class='input-limit-cell pcc2_meter'><div class='input-group input-feed pcc2_meter''>
                                                        <input class='form-control text-right input-limit' name='limit_pcc2_A' id='limit_pcc2_A' type='text' value='<?php echo $limit_pcc2_A;?>' /><div class='input-group-addon'>kW</div></div>
                                        </td>
                                        <td class='input-target-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                <input class='form-control text-right input-target' name='targets_pcc_combo_A' id='targets_pcc_combo_A' type='text' value='<?php echo $power_target_pcc_combo_A;?>' /><div class='input-group-addon'>kW</div></div>
                                        </td>
                                        <td class='input-limit-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                <input class='form-control text-right input-limit' name='limit_pcc_combo_A' id='limit_pcc_combo_A'   type='text' value='<?php echo $limit_pcc_combo_A; ?>' /><div class='input-group-addon'>kW</div></div>
                                        </td>
                                        </tr>

                                        <tr id='feed_settings_B' class='feed_settings'><td>B</td>
                                            <td class='input-target-cell pcc_meter'><div class='input-group input-feed r'>
                                                    <input class='form-control text-right input-target' name='targets_pcc_B' id='targets_pcc_B' type='text' value='<?php echo $power_target_pcc_B;  ?>' />
                                                    <div class='input-group-addon'>kW</div></div></td>
                                            <td class='input-limit-cell pcc_meter'><div class='input-group input-feed '>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_B' id='limit_pcc_B' type='text' value='<?php echo $limit_pcc_B; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-target' name='targets_pcc2_B' id='targets_pcc2_B' type='text' value='<?php echo $power_target_pcc2_B; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-limit' name='limit_pcc2_B' id='limit_pcc2_B' type='text' value='<?php echo $limit_pcc2_B;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                    <input class='form-control text-right input-target' name='targets_pcc_combo_B' id='targets_pcc_combo_B' type='text' value='<?php echo $power_target_pcc_combo_B;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell combo_meter'><div class='input-group input-feed' pcc_combo>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_combo_B' id='limit_pcc_combo_B'   type='text' value='<?php echo $limit_pcc_combo_B; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                        </tr>

                                        <tr id='feed_settings_C' class='feed_settings'><td>C</td>
                                            <td class='input-target-cell pcc_meter'><div class='input-group input-feed '>
                                                    <input class='form-control text-right input-target' name='targets_pcc_C' id='targets_pcc_C' type='text' value='<?php echo $power_target_pcc_C;  ?>'' />
                                                    <div class='input-group-addon'>kW</div></div></td>
                                            <td class='input-limit-cell pcc_meter'><div class='input-group input-feed '>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_C' id='limit_pcc_C' type='text' value='<?php echo $limit_pcc_C; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-target' name='targets_pcc2_C' id='targets_pcc2_C' type='text' value='<?php echo $power_target_pcc2_C; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-limit' name='limit_pcc2_C' id='limit_pcc2_C' type='text' value='<?php echo $limit_pcc2_C;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                    <input class='form-control text-right input-target' name='targets_pcc_combo_C' id='targets_pcc_combo_C' type='text' value='<?php echo $power_target_pcc_combo_C;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_combo_C' id='limit_pcc_combo_C'   type='text' value='<?php echo $limit_pcc_combo_C; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                        </tr>
                                        <tr id='feed_settings_Delta' class='feed_settings'><td>&Delta;</td>
                                            <td class='input-target-cell pcc_meter'><div class='input-group input-feed '>
                                                    <input class='form-control text-right input-target' name='targets_pcc_Delta' id='targets_pcc_Delta' type='text' value='<?php echo $power_target_pcc_Delta;  ?>'' />
                                                    <div class='input-group-addon'>kW</div></div></td>
                                            <td class='input-limit-cell pcc_meter'><div class='input-group input-feed '>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_Delta' id='limit_pcc_Delta' type='text' value='<?php echo $limit_pcc_Delta;; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-target' name='targets_pcc2_Delta' id='targets_pcc2_Delta' type='text' value='<?php echo $power_target_pcc2_Delta; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell pcc2_meter'><div class='input-group input-feed pcc2_meter'>
                                                    <input class='form-control text-right input-limit' name='limit_pcc2_Delta' id='limit_pcc2_Delta' type='text' value='<?php echo $limit_pcc2_Delta;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-target-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                    <input class='form-control text-right input-target' name='targets_pcc_combo_Delta' id='targets_pcc_combo_Delta' type='text' value='<?php echo $power_target_pcc_combo_Delta;?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                            <td class='input-limit-cell combo_meter'><div class='input-group input-feed pcc_combo'>
                                                    <input class='form-control text-right input-limit' name='limit_pcc_combo_Delta' id='limit_pcc_combo_Delta'   type='text' value='<?php echo $limit_pcc_combo_Delta; ?>' /><div class='input-group-addon'>kW</div></div>
                                            </td>
                                        </tr>

                                        </table>
                                    </div>
                                    <div class="col-md-offset-1 col-md-10">
                                        <div class="formTextSpacing text-center">
                                            <!--<strong><?php echo(ucfirst($gateway_perspective)); ?></strong>-->
                                            <strong>Polarity</strong> (<?php echo $gateway_perspective == "consumption" ? "+import/-export" : "+export/-import"; ?>)
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="formBoxSectionWhite">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="has_solar" class="col-md-4 control-label">Has Solar:</label>
                                        <div class="col-md-8 topCalBoxes">
                                            <input type="checkbox" id="has_solar" name="has_solar"
                                                   <?php if ($has_solar == "1"): ?>checked<?php endif ?>/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Minimum Import"
                                               data-content="Checked if regulating solar production.">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>Checked if regulating solar production.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="has_storage" class="col-md-4 control-label">Has Storage:</label>
                                        <div class="col-md-8 topCalBoxes">
                                            <input type="checkbox" id="has_storage" name="has_storage"
                                                   <?php if ($has_storage == "1"): ?>checked<?php endif ?>/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="formTextSpacing">
                                            <a data-toggle="popover" title="Minimum Import"
                                               data-content="Checked if regulating power using storage capacity.">
                                                <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                            </a>Checked if using storage capacity to regulate power.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="batteryGroup">
                                <div class="formBoxSectionWhite">

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="has_storage" class="col-md-4 control-label">Step Size:</label>
                                            <div class="col-md-8 topCalBoxes">
                                                <input class="form-control inputUpdate" type="text" id="step_size" name="step_size"
                                                       value="<?php echo $step_sizeDB; ?>">

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="formTextSpacing">
                                                <a data-toggle="popover" title="Minimum Import"
                                                   data-content="Checked if regulating power using storage capacity.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </a>Please enter the step size in KW
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="has_storage" class="col-md-4 control-label">Battery Strategy:</label>
                                            <div class="col-md-8 topCalBoxes">
                                                <select id="battery_strategy" name="battery_strategy"class="form-control strategySelect">
                                                    <option value='none' <?php if($battery_strategyDB  =='') echo "selected" ?>>None</option>
                                                    <option value='setpoint' <?php if($battery_strategyDB  =='setpoint') echo "selected" ?>>Setpoint</option>
                                                    <option value='variable_charge' <?php if($battery_strategyDB  =='variable_charge') echo "selected" ?>>Variable Charge</option>
                                                    <option value='variable_discharge' <?php if($battery_strategyDB  =='variable_discharge') echo "selected" ?>>Variable Discharge</option>
                                                </select>


                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="formTextSpacing">
                                                <a data-toggle="popover" title="Battery Strategy"
                                                   data-content="Checked if regulating power using storage capacity.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </a>Please enter a battery strategy if you want to run the battery(s) unattended
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="formBoxSectionWhite" >
                                    <div class="form-group">

                                        <div class="col-md-12">

                                            <div class="col-md-11 topCalBoxes">
                                                <table class="table">
                                                    <thead>
                                                    <tr style="font-size: smaller">
                                                        <th class='text-center'>B_ID</th>
                                                        <th class='text-center'>Change Power Regular</th>
                                                        <th class='text-center'>Status</th>
                                                        <th class='text-center'>Battery Name</th>
                                                        <th class='text-center'>ID Address</th>
                                                        <th class='text-center'>Charge Priority</th>
                                                        <th class='text-center'>Discharge Priority</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="auto_brainbox_status_div">
                                                    <?php
                                                    echo "<tr>";
                                                    for($i = 0;$i <  count($battery_id); $i++) {
                                                        $status = "<option value='$battery_id[$i]-add' >Add to PR</option>";
                                                        echo "<td class='text-center'>$battery_id[$i]  </td>";
                                                        if( $power_regulator_id == $battery_power_regulator_id[$i]) {
                                                            $status = "<option  value='$battery_id[$i]-remove'>Remove </option>";
                                                            echo "<td class='text-center'><b>Regulated - $battery_power_regulator_id[$i]</b></td>";
                                                        }else {
                                                            if($battery_power_regulator_id[$i] == ''){
                                                                echo "<td class='text-center'><b>Attached - $battery_power_regulator_id[$i]</b></td>";
                                                            }elseif($battery_power_regulator_id[$i] >= 1 ) {
                                                                echo "<td class='text-center'>Attached to PR $battery_power_regulator_id[$i]</td>";
                                                            }
                                                            else {
                                                                echo "<td class='text-center'>Not Attached</td>";
                                                            }
                                                        }
                                                        echo "
                                                    
                                                   <td class='text-center'>
                                                    <select name='updatecontrol[]'  class='form-control'>
                                                    <option  value='' selected>NO Action</option>
                                                        $status;
                                                       
                                                        </select></td>
                                                    <td class='text-center'><a href='battery_model_" .$battery_model_id[$i] .".php'> $battery_name[$i]</a></td>
                                        
                                                    <td class='text-center'>$battery_ip_address[$i]</td> ";

                                                        if( $power_regulator_id == $battery_power_regulator_id[$i]) {
                                                            echo "<td class='text-center'>
                                                                        <select name='charge_priority[]'  class='form-control'>";
                                                            for ($j=1; $j<=10; $j++)
                                                            {
                                                                if($charge_priority[$i] == $j) {
                                                                    echo"<option value='$battery_id[$i]-$j' selected>$j</option>";
                                                                }else{
                                                                    echo"<option value='$battery_id[$i]-$j'>$j</option>";
                                                                }
                                                            }
                                                            echo "</select>
                                                                         </td>";
                                                        }else{
                                                            echo "<td class='text-center'>$charge_priority[$i]</td>";
                                                        }
                                                        if( $power_regulator_id == $battery_power_regulator_id[$i]) {
                                                            echo "<td class='text-center'>
                                                                        <select name='discharge_priority[]'  class='form-control'>";
                                                            for ($j=1; $j<=10; $j++)
                                                            {
                                                                if($discharge_priority[$i] == $j) {
                                                                    echo"<option value='$battery_id[$i]-$j' selected>$j</option>";
                                                                }else{
                                                                    echo"<option value='$battery_id[$i]-$j'>$j</option>";
                                                                }
                                                            }
                                                            echo "</select>
                                                                         </td>";
                                                        }else{
                                                            echo "<td class='text-center'>$discharge_priority[$i]</td>";
                                                        }echo "</tr>";
                                                    }

                                                    ?>

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('footer.php'); ?>
    </div>
    <!-- Row end -->

</div>
</body>
</html>
