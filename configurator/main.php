<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

include_once ('control/get_power_regulator.php');
include_once ('control/get_brainboxes.php');
include_once ('control/get_meter.php');
include_once ('control/get_inverters.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Gateway Config</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        p {
            line-height: normal;
            margin: 10px;
        }
        h4 {
            line-height: 150%;
        }
        button, input[type="button"] {

            text-shadow: none !important;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }


    </style>


</head>
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
<script type="text/javascript">

        

    $(document).ready(function(){

        auto_inverter_status();
        auto_gateway_status();
        setInterval(auto_inverter_status,20000);
        setInterval(auto_gateway_status,2000);


    });

    function auto_inverter_status(){
        $.ajax({
            url: "control/inverter_status.php",
            cache: false,
            success: function(data){
                $("#inverter_status_div").html(data);
            }
        });
    }

    function auto_gateway_status(){

        $.ajax({
            url: "control/get_gateway-status.php?data=yes&type=status",
            cache: false,
            success: function(data){

                $("#auto_gateway_status_div").html(data);

                // console.log(' success auto_gateway_status:' + $("#auto_gateway_status_div").html());

                if( $("#auto_gateway_status_div").html() == 'System Offline') {
                    // console.log('System Offline');

                    $('#startSystem').show();
                    $('#stopSystem').hide();
                    $('#disableEnableGateway').html('Enable Gateway Now');
                    $('input[name="system_enable"]').val('1');
                }
                if( $("#auto_gateway_status_div").html() == 'System Online') {
                    // console.log('System Online');
                    $('#stopSystem').show();
                    $('#startSystem').hide();
                    $('#disableEnableGateway').html('Disable Gateway Now');
                    $('input[name="system_enable"]').val('0');
                }


            }
        });
    }
    
    //Refresh auto_load() function after 10000 milliseconds
    setInterval(auto_inverter_status,20000);






</script>
<body>

<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">


                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-md-6"><h1 style="padding-left: 30px;">Gateway Configurator</h1></div>
                        <div class="col-md-6">
                            <div class="text-right" style="margin: 20px;">
                                <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                    <span class="glyphicon glyphicon-log-out"></span> Log out
                                </a>
                            </div></div>
                    </div>

                <div style="padding: 30px;">


                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Configuration Checklist</h3></div>
                                <div class="panel-body">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>System</th>
                                            <th>Status</th>
                                            <th>Link</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th>Feeds</th>
                                            <td>
                                                <?php
                                                if($FEED_NAME_DB[0] != '' ){
                                                    echo ' <span style="color: green" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                                }else
                                                    echo ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot"></span>';

                                                ?>
                                            </td>
                                            <td><a href="power_regulator.php">Configure</a> </td>

                                        </tr>
                                        <tr>
                                            <th>Brainboxes</th>
                                            <td>
                                                <?php
                                                if($target_interfaceDB[1] != '' ){
                                                    echo ' <span style="color: green" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                                }else
                                                    echo ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot"></span>';

                                                ?>
                                            </td>
                                            <td><a href="brainboxes.php">Configure</a> </td>

                                        </tr>
                                        <tr>
                                            <th>Meters</th>
                                            <td>
                                                <?php
                                                if($meter_id_DB[1] != '' ){
                                                    echo ' <span style="color: green" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                                }else
                                                    echo ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot"></span>';

                                                ?>
                                            </td>
                                            <td><a href="meter.php">Configure</a> </td>

                                        </tr>
                                        <tr>
                                            <th>Inverters</th>
                                            <td>
                                                <?php
                                                if($inverters_id[0] != '' ){
                                                    echo ' <span style="color: green" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                                }else
                                                    echo ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot"></span>';

                                                ?>
                                            </td>
                                            <td><a href="inverters.php">Configure</a> </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-default">

                                <div class="panel-heading"><h3 class="text-center">Gateway Status </h3></div>
                                <div class="panel-body">
                                    <p></p>
                                    <p>  </p>
                                    <p style="height: 40px"><strong>System State:</strong> <span id="auto_gateway_status_div"></span></p>
                                    </p>

                                </div>
                            </div>
                        </div>




                    </div>



                    <div class="row">

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Meters</h3></div>

                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                        <tr style="font-size: smaller">
                                            <th>Meter Status</th>

                                            <th>Meter Role</th>
                                            <th>Modbus Device #</th>
                                            <th>Interface</th>
                                        </tr>
                                        </thead>
                                        <tbody id="auto_brainbox_status_div">
                                        <?php
                                        for($i = 0;$i <  count($meter_id_DB); $i++) {
                                            echo "<tr>
                                            <td class='text-center'>";

                                            if($online_DB[$i] == 1 ){
                                                echo ' <span style="color: green" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            }else
                                                echo ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot"></span>';
                                            echo"</td>
            
                                            <td class='text-center'>$meter_role_DB[$i]</td>
                                            <td class='text-center'>$modbusdevicenumber_DB[$i]</td>
                                            <td class='text-center'>$address_DB[$i]</td>
                                            ";
                                            echo "</tr>";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <a href="help.php#meter-veris">Meter help</a>
                                    </p>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Brainboxes </h3></div>
                                <div class="panel-body">

                                    <table class="table">
                                        <thead>
                                        <tr style="font-size: smaller">

                                            <th>Target Interface</th>

                                            <th>IP Address</th>
                                            <th>Mac Address</th>

                                            <th>Enabled</th>

                                            <!-- <th>Updated</th>-->
                                            <th>PID</th>



                                        </tr>
                                        </thead>
                                        <tbody id="auto_brainbox_status_div">
                                        <?php
                                        for($i = 0;$i <  count($target_interfaceDB); $i++) {
                                            echo "<tr>
                                            <td class='text-center'>$target_interfaceDB[$i]</td>
                                            <td class='text-center'><a href='http://$IP_AddressDB[$i]' target='_blank'>$IP_AddressDB[$i]</a></td>
                                            <td class='text-center'>$mac_addressDB[$i]</td>
                                            <td class='text-center'>$enabledDB[$i]</td>
                                            <td class='text-center'>$pidDB[$i]</td> 
                                            ";
                                            echo "</tr>";
                                        }

                                        ?>

                                        </tbody>
                                    </table>
                                    <a href="help.php#brainbox">Brainbox help</a>
                                    </p>
                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h3 class="text-center">Power Regulators</h3></div>
                                <div class="panel-body">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Feed</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php

                                        for($i = 0; $i < count($FEED_NAME_DB); $i++) {
                                            echo "<tr>";
                                            echo "<td>" . $FEED_NAME_DB[$i] . "</td>";
                                            echo "</tr>";
                                        }

                                        ?>
                                        </tr>

                                        </tbody>
                                    </table>
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="panel panel-default">

                                <div class="panel-heading"><h3 class="text-center">Inverter Status  </h3></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th  class='text-center'>Feed</th>
                                                <th  class='text-center'>Inverters</th>


                                            </tr>
                                            </thead>
                                            <tbody id="inverter_status_div">

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>


                </div>
            </div>
        </div>
            <?php    include_once ('footer.php'); ?>
    </div>

</div>

</body>
</html>
