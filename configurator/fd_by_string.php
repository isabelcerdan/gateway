<?php
include_once ('functions/mysql_connect.php');
include_once('control/form_string_display.php'); // Get default data
$letters = range('A', 'G');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Feed Detection by String</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript"src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript"  src="js/featherlight.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/js.cookie.js"></script>
    <script type="text/javascript" src="js/strings-display.js?<?php echo rand(); ?>"></script>
</head>
<body>
<div class="container">
   
    <?php
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Feed Detection by String</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>
                <div class="panel-body">

                    <div id="getCheckedinfo">String Selected: <span id="stringsSelectedNum" ></span> <span id="stringsEstimatedTime" style="float: right "> </span></div>

                    <form  method='post' action=" <?php echo $_SERVER['PHP_SELF']; ?> ">
                        <div style="margin: 5px;float: left" class="text-left"><label>Show # of items:
                                <select id="itemsperpage" name="itemsperpage" >
                                    <option value='4'" <?php if($limit == 4) {echo 'selected';} ?> >4</option>
                                    <option value='8'" <?php if($limit == 8) {echo 'selected';} ?> >8</option>
                                    <option value='16'" <?php if($limit == 16) {echo 'selected';} ?> >16</option>
                                    <option value='32'" <?php if($limit == 32) {echo 'selected';} ?> >32</option>
                                    <option value='64'" <?php if($limit == 64) {echo 'selected';} ?> >64</option>
                                    <option value='128'" <?php if($limit == 138) {echo 'selected';} ?> >128</option>
                                    <option value='256'" <?php if($limit == 138) {echo 'selected';} ?> >256</option>
                                    <option value='<?php echo $total; ?>'" <?php if($limit == $total) {echo 'selected';} ?>><?php echo $total; ?> - All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                    <div style="clear:both"> </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <p style="margin-top: 7px;"><input type="checkbox" id="select_all"/><span st> Select All</span></p>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5  text-right">
                            <button id="run_now" type="button" class="btn btn-primary run_fd_now" data-toggle="modal" data-target="#myModal">Run Feed Detection Now</button>

                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3  text-right">
                            

                            <button id="run_later" type="button" class="run_fd_now btn btn-warning warning" data-toggle="modal" data-target="#myModal">Use Auditor to run Feed Detection</button>
                        </div>
                    </div>
                    <div class="formBoxSection ">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                                <tr >
                                    <th></th>




                                    <th class="text-center">StringId<a style="float: right" href="fd_by_string.php?orderby=stringId&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>
                                    <th class="text-center">String Postion<a style="float: right" href="fd_by_string.php?orderby=stringPosition&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>

              
                
                                    <th class="text-center">Current Feed Name<a style="float: right" href="fd_by_string.php?orderby=FEED_NAME&sortDirection=<?php echo $sortDirectionDefault; ?>&itemsperpage=<?php echo $limit;?>  "><div class="sortInvertersBoth"></div></a></th>

                                </tr>
                                <?php
                                //print_r($serialNumber);
                                // $rowId = 0;
                                for ($i = 0; $i < count($mac_address);$i++) {
                                    //$rowId++;
                                    ?><form class='forms' id="<?php echo $stringId[$i] .'_'. $feed_current[$i] ; ?>" name="<?php echo $inverters_id[$i]; ?>" action="/control/form_inverters_update.php" method="post" >
                                    <input type="hidden" name="stringID" value="<?php echo $stringId[$i] ; ?>">

                                    <tr >
                                        <td><input class="selectCheckBox checkbox"  id="select--<?php echo $stringId[$i] .'_'. $feed_current[$i] ;?>" type="checkbox" name="select-me" value="<?php echo $stringId[$i] ;?>"></td>




                                        <td><?php echo $stringId[$i]; ?></td>
                                        <td><?php echo $stringPosition[$i]; ?></td>

                                        <td><?php echo $feed_current[$i]; ?></td>
                 


                                    </tr>
                                    </form>
                                    <?php
                                    // End of loop //
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>
    <div class="text-center">
        <ul class="pagination">
            <?php
            if($num_page>1)
            {
                pagination($page,$num_page,$limit,$orderby);
            }
            ?>
        </ul>
    </div>
</body>
</html>