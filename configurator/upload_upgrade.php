<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:14 PM
 */
include_once('functions/mysql_connect.php');
$inverters_id = mysqli_real_escape_string($conn, $_REQUEST['inverters_id']);
$inverters_id = filter_var($inverters_id, FILTER_SANITIZE_STRING);

$inverters = explode(",",$inverters_id);
foreach($inverters as $key => $inverter) {
    $inverterArray[$key] = "'$inverter'";
}

////////////////////////////////////////////////////////////////////////
// If inverter are being upgraded forward to upgrade status page
////////////////////////////////////////////////////////////////////

include_once('control/upgrade/get_upgrade_info.php'); // Get default data

if (($upgrade_command_DB == 'start_upgrade' ) OR ($upgrade_command_DB == 'upgrade_in_progress' )  OR ($upgrade_command_DB == 'in_progress' ) ) {
    header('Location: /update_progress.php');
    exit();
}
//in_progress
?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<body>


<div class="container">
    <h2>Update Inverters - Get File</h2>

    <?php
    if ($inverters_id == ''){
        echo "<p class='alert-warning'>No Inverters selected,<br> please go back and select inverters to upgrade </p>";
    }else
    {
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <form action="control/upgrade/upload.php" method="post" enctype="multipart/form-data">
                <input type="file" name="myFile">
                <br>
                <input type="submit" value="Upload">
                <input  type="hidden" name="inverters_id" value="<?php echo $inverters_id; ?>">
            </form>



        </div>
        </div>
        <?php
    }
        ?>

</div>


</body>
</html>