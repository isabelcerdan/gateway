<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Power Factor Control</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>


</head
<body>

<div class="container">
   
    <?php
    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Power Factor</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>



                    <div style="padding: 30px;">


                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Fixed Power Factor</h3></div>
                                    <div class="panel-body">

                                        <p>Set Power Factor for individual inverters, feeds or the entire site. </p><br>
                                        <p>  </p>
                                        <p class="text-center"><a href="set_inverter_power_factor.php"><button type="button" class="btn btn-warning">Set Fixed Power Factor</button></a>
                                        </p>




                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Adaptive Power Factor </h3></div>
                                    <div class="panel-body">
                                        <p>Set Power Factor at the PCC that will be able to change and adapt to different power conditions.</p>
                                        <p>  </p>
                                        <p class="text-center"><a href="set_adaptive_power_factor.php"><button type="button" class="btn btn-warning">Set Adaptive Power Factor</button></a>
                                        </p>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
          
            </div>
            </div>
        </div>
    <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>