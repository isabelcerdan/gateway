<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

    $subscriptions_id = mysqli_real_escape_string($conn, $_GET['subscriptions_id']);
    $subscriptions_id = filter_var($subscriptions_id, FILTER_SANITIZE_STRING);
    $alarm_subscriptions_id = $subscriptions_id;

  include_once ('control/get_alarm_names.php'); // Get default data
 include_once('control/get_alarm_notifications.php');

$result_sub = $conn->query("select * from alarm_subscribers");

if(mysqli_num_rows($result_sub)>0) {

    while($row = $result_sub->fetch_assoc()) {
        $alarm_subscribers_id_DB[]=  $row['id'];
        $name_subscribers_DB[]=  $row['name'];
        $email_subscribers_DB[]=  $row['email_address'];
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Alarm Notifications</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>


</head
<body>



<div class="container">
 
    <?php

    // Header
    include_once ('header.php');
    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Alarm Notifications</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>


                <div class="panel-body">
                    <form class="form-horizontal row-border" action="/control/form_alarm_notifications.php" method="post">
                        <?php
                            echo "<input type='hidden' name='subscriptions_id' value='$subscriptions_id'>";
                        ?>

                        <div class="formBoxSection ">
                            <div class="form-group  ">
                                <label class="col-md-3 control-label">Person to receive Alert:</label>
                                <div class="col-md-3 ">
                                    <select name="subscriber_id"  id="subscriber_id" class="form-control" required >
                                        <?php
                                        if($alarm_user_id != ''){
                                            echo "<option value='$alarm_user_id'>$alarm_user_name- Current</option>";
                                        }else {
                                            echo "<option value=''>Select</option>";
                                        }
                                        ?>

                                        <?php
                                        for ($i = 0; $i < count($alarm_subscribers_id_DB);$i++ ) {
                                            echo "  <option value='$alarm_subscribers_id_DB[$i] '> $name_subscribers_DB[$i]</option>";
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="col-md-6" >
                                  <div class="formTextSpacing">
    
                                  </div>
                                </div>
                            </div>


                                <div class="form-group  ">
                                    <label class="col-md-3 control-label">Alarm:</label>
                                    <div class="col-md-3 ">
                                        <select name="alarm_id"  id="user" class="form-control" required >
                                            <?php
                                            if($alarm_id != ''){
                                                echo "<option value='$alarm_id'>$alarm_name</option>";
                                            }else {
                                                echo "<option value=''>Select</option>";
                                            }
                                            ?>
                                            
                                            <?php
                                            for ($i = 0; $i < count($alarm_id_DB);$i++ ) {
                                                echo "  <option value='$alarm_id_DB[$i] '>$alarm_id_DB[$i] - $name_DB[$i]</option>";
                                            }
                                            ?>

                                        </select>
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>

                            <div class="form-group  ">
                                <label class="col-md-3 control-label">Enabled:</label>
                                <div class="col-md-3 ">
                                    <input name="enabled" id="enabled" type="checkbox" <?php if ($enabled != '0') {echo "checked";} ?> value="1"> </input>
                                </div>
                                <div class="col-md-6" >
                                    <div class="formTextSpacing">

                                    </div>
                                </div>
                            </div>
    
                            </div>
    
                            <button type="submit" class="btn btn-primary" >Register</button>

                    </form>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>