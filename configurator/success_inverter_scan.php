<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Success</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>

    <script>
       // Refresh Page
        $(document).ready(function(){

            $('#refreshPage').click(function() {
                location.reload();
            });
        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
//$inverters_status_id = mysqli_real_escape_string($conn, $_GET['inverters_status_id']);
//$inverters_status_id=  filter_var($inverters_status_id, FILTER_SANITIZE_STRING);

include_once ('control/get_inverters_scan_results.php');

if( $last_time_scanned == '') {
    $last_time_scanned = "Hasn't been scanned yet";
}
?>


<div class="container">
    <?php
    // Header
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Scanning for Inverters</h3>
                    <div class="text-right">
                        <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                            <span class="glyphicon glyphicon-log-out"></span> Log out
                        </a>

                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal row-border" >


                            <div class="formBoxSection">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Last Time Inverters scanned:</label>
                                    <div class="col-md-3">
                                        <div class="formTextSpacing">
                                          <?php echo $last_time_scanned; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" >
                                        <div class="formTextSpacing">

                                        </div>
                                    </div>
                                </div>
                            </div>


                        <a class="btn btn-primary" href="#" role="button" id="refreshPage">Refresh Page</a>
                    </div>
                </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>

    </div>
    <!-- Row end -->

</div>
</body>
</html>
