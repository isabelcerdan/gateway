<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');
include_once ('control/get_battery.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Battery Control</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        button, input[type="button"] {

            text-shadow: none !important;
        }
        #stopSystem {
            display: none;
        }

        .smallerText {
            font-size: xx-small;
        }
        #Update_Success {
            display: none;
        }
        .progress {
            height: 60px;

        }
        .progress-bar {
            font-size: 20px !important;
            padding-top: 15px;1
        }
        .lightning {
            background-image: url("images/lightning.png");
        }
        .charging {
            color: green;
        }
        .discharging {
            color: red;
        }
        .row.form-group  {
            margin-bottom: 5px;
        }
        .hrRule {
            margin-top: 1px; margin-bottom: 5px;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>

    <script type="text/javascript">


        $(document).ready(function (){

            $('.sendCommand').on('click', function() {
                console.log('click me');
                var r = confirm("This action my take up to 15 seconds. Turning On or Off the battery can take several minutes. Do you want to continue?");
                if (r == true) {
                    var command = '';
                    if (this.id == 'stop') {
                        command = 0;
                    }
                    if (this.id == 'start') {
                        command = 1;
                    }
                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?startStop=' + command,
                        success: function (data) {
                            console.log('data:' + data);

                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    return false;
                }
            });
            $('.setACTIVE_POWER').on('click', function() {
                var r = confirm("This action my take up to 15 seconds. Are you sure you want to set Active Power?");
                if (r == true) {
                    value = $("#ACTIVE_POWER").val();
                    //console.log('ACTIVE_POWER:'+ value );

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?activePower=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                    $("#ACTIVE_POWER").val('');
                    return false;
                }
            });

            $('.setREACTIVE_POWER').on('click', function() {
                var r = confirm("This action my take up to 15 seconds. Are you sure you want to set Reactive Power?");
                if (r == true) {
                    value = $("#REACTIVE_POWER").val();
                    //console.log('ACTIVE_POWER:'+ value );
                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?reactivePower=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#REACTIVE_POWER").val('');
                return false;

            });

            $('.setPowerRiseRime').on('click', function() {
                var r = confirm("This action my take up to 15 seconds. Are you sure you want to set Set Power Rise Time?");
                if (r == true) {

                    value = $("#PowerRiseTime").val();

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?powerRiseRime=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#PowerRiseTime").val('');
                return false;

            });

            $('.setPowerDecreaseTime').on('click', function() {
                var r = confirm("This action my take up to 15 seconds. Are you sure you want to set Set Power Decrease Time? ");
                if (r == true) {
                    value = $("#PowerDecreaseTime").val();

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?powerDecreaseTime=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#PowerDecreaseTime").val('');
                return false;

            });

            $('.controller_control').on('click', function() {

                var enable = $(this).attr('id');
                //alert('!ok:'+enable);
                $.ajax({
                    type: "POST",
                    url: 'control/form_battery_enable.php?monitor_control='+ enable,
                    success: function (data) {
                        if(data == 'cess_control=1'){
                            $(".controller_control").attr("id", "0");
                            $(".controller_control").html('Stop Controller');
                        }
                        if(data == 'cess_control=0'){
                            $(".controller_control").attr("id", "1");
                            $(".controller_control").html('Start Controller');
                        }
                        //
                    },
                    error: function (jqXHR, text, error) {
                        console.log('error:');
                    }
                });
                return false;

            });



            setInterval(auto_gateway_status,2000);
            auto_gateway_status();
            function auto_gateway_status() {


                $.getJSON("control/get_battery_data.php", function(result){
                    $.each(result, function(i, field){
                        /*





                        $("#dc_voltage_info").html( field.dc_voltage_info);
                        $("#dc_current_info").html( field.dc_current_info);
                        $
                        $("#dcac_ac_charge_energy").html( field.dcac_ac_charge_energy);
                        $("#dcac_ac_discharge_energy").html( field.dcac_ac_discharge_energy);
                        

                        $("#a_amps").html( field.a_amps);
                        $("#b_amps").html( field.b_amps);
                        $("#c_amps").html( field.c_amps);
                        $("#a_volts").html( field.a_volts);
                        $("#b_volts").html( field.b_volts);
                        $("#c_volts").html( field.c_volts);

                        $("#ipm_phase_a_temperature").html( field.ipm_phase_a_temperature);
                        $("#ipm_phase_b_temperature").html( field.ipm_phase_b_temperature);
                        $("#ipm_phase_c_temperature").html( field.ipm_phase_c_temperature);
                        $("#transformer_phase_b_temperature").html( field.transformer_phase_b_temperature);
                        $("#battery_string_total_voltage").html( field.battery_string_total_voltage);
                        $("#battery_string_total_current").html( field.battery_string_total_current);
                        $(".battery_string_soc").html( field.battery_string_soc);
                        $("#battery_string_soh").html( field.battery_string_soh);
                        $("#power_decrease_time").html( field.power_decrease_time);
                        $("#power_rise_time").html( field.power_rise_time);



                        */
                        $(".active_power_cmd").html( field.active_power_cmd);
                        $("#reactive_power_cmd").html( field.reactive_power_cmd);
                        $("#unit_work_mode").html( field.unit_work_mode);
                        $(".unit_work_state").html( field.unit_work_state);
                        $("#unit_ctrl_mode").html( field.unit_ctrl_mode);
                        $("#soc_charge_limit").html( field.soc_charge_limit);
                        $("#soc_discharge_limit").html( field.soc_discharge_limit);
                        $("#power_factor").html( field.power_factor);
                        $("#reconnect_state").html( field.reconnect_state);
                        $("#work_state_control_cmd").html( field.work_state_control_cmd);
                        $("#battery_string_running").html( field.battery_string_running);
                        $("#battery_string_enabled").html( field.battery_string_enabled);
                        $("#battery_string_isolated").html( field.battery_string_isolated);
                        $("#battery_stack_voltage").html( field.battery_stack_voltage);
                        $("#battery_stack_current").html( field.battery_stack_current);
                        $("#battery_stack_power").html( field.battery_stack_power);
                        $(".battery_stack_soc").html( field.battery_stack_soc);
                        $("#battery_stack_soh").html( field.battery_stack_soh);

                        $("#battery_max_cell_voltage").html( field.battery_max_cell_voltage);
                        $("#battery_min_cell_voltage").html( field.battery_min_cell_voltage);
                        $("#dc_voltage").html( field.dc_voltage);
                        $("#dc_current").html( field.dc_current);
                        $("#dc_power").html( field.dc_power);
                        $("#voltage_control_cmd").html( field.voltage_control_cmd);
                        $("#frequency_control_cmd").html( field.frequency_control_cmd);

                        $("#true_power").html( field.true_power);
                        $("#apparent_power").html( field.apparent_power);


                        $("#frequency").html( field.frequency);
                        $("#timestamp").html( field.timestamp);
                        console.log("TimeStamp:"+ field.timestamp);


                        // Show Battery running state above progress bar //
                        if(field.active_power_cmd < 0) {
                            $("#batteryChargeState").removeClass('discharging');
                            $("#batteryChargeState").addClass('charging');
                            $("#batteryChargeState").html('Charging');
                        }else{
                            $("#batteryChargeState").removeClass('charging');
                            $("#batteryChargeState").addClass('discharging');
                            $("#batteryChargeState").html('Discharging');
                        }

                        //progress-bar progress-bar-success progress-bar-striped
                        $(".progress-bar").css({'width': field.battery_string_soc +'%'});

                        // Start Stop button updates //

                        // Show Battery running state above progress bar //
                        if(field.unit_work_state  == 'Running') {
                            $(".sendCommand").attr("id", "stop");
                          $(".sendCommand").html('Stop');
                        }else{
                            $(".sendCommand").attr("id", "start");
                            $(".sendCommand").html('Start');
                        }



                    });
                });

            }
        });

    </script>





</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var


?>
<div class="container">
    <div class="apparentBrand">Apparent igOS Gateway</div>
    <?php
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Battery Control</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>

                        </div></div>
                </div>

                    <div style="padding: 30px;">

                        <div class="row">


                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><p class="text-center">Gateway Battery Control</p></div>
                                    <div class="panel-body">

                                        <p>
                                            <strong>Gateway Control: </strong><a><button class="controller_control btn btn-outline-secondary" id="<?php if($cess_controller_control_state == 1){echo 0;}else echo 1; ?>" type="button" class="btn btn-outline-secondary"  data-target="#myModal"><?php if($cess_controller_control_state == 1){echo "Stop Controller";}else echo "Start Controller"; ?></button></a>
                                        </p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6"></div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Battery Charge State</h3></div>
                                    <div class="panel-body">
                                        <h4>Battery State:
                                            <span id='batteryChargeState'
                                            <?php

                                            if($active_power_cmd < 0) {
                                                echo "class='charging'>Charging";
                                            }else {
                                                echo "class='discharging'>Discharging";
                                            }
                                            ?>

                                            </span
                                        </h4>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-1"></div>

                                            <div class="col-md-10">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $battery_stack_soc; ?>%">
                                                        <span class='battery_stack_soc'><?php echo $battery_stack_soc; ?></span>%
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-1"></div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Commands</h3></div>
                                    <div class="panel-body">



                                        <hr class='hrRule'>
                                        <p>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-5 col-form-label">Turn on/off the battery:</label>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($unit_work_state == 'Starting') {
                                                    echo ' <a><button class="sendCommand btn btn-outline-secondary" id="stop" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Stop</button></a>';
                                                } else {
                                                    echo '<a><button class="sendCommand btn btn-outline-secondary" id="start" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Start</button></a>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        </p>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong> Active Power:</strong></strong><br><small>(+) Set active power discharge <br>(-) Set active power charge</small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="ACTIVE_POWER"
                                                       name="ACTIVE_POWER" maxlength="5"
                                                       size="4"> <button class="setACTIVE_POWER" id="start" type="button"
                                                        class="btn btn-outline-secondary">Set
                                                </button>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                            <strong>Reactive Power:</strong><br>
                                                <small>(+) Provide Reactive, <br>(-) Absorb Reactive</small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="REACTIVE_POWER" name="REACTIVE_POWER" maxlength="5" size="4"><button class="setREACTIVE_POWER" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >
<!--
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Set power rise time: </strong><br>
                                                <small> Time (in seconds) {180-540}</small>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" id="PowerRiseTime" name="PowerRiseTime" maxlength="3" size="3">
                                                <button class="setPowerRiseRime" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                                            </div>

                                        </div>

                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5 ">
                                                <strong>Set power decrease time: </strong><br>
                                                <small> Time (in seconds) 360-1080</small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="PowerDecreaseTime" name="PowerDecreaseTime" maxlength="4" size="4">
                                                <button class="setPowerDecreaseTime" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                                            </div>

                                        </div>
-->


                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">System Status</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Work State:</strong></div>
                                            <div class="col-sm-7"><span class="unit_work_state"><?php //echo $unit_work_state; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Ctrl Mode:</strong></div>
                                            <div class="col-sm-7"><span id="unit_ctrl_mode"><?php //echo $unit_ctrl_mode; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Work Mode:</strong></div>
                                            <div class="col-sm-7"><span id="unit_work_mode"><?php //echo $unit_work_mode; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Active Power Setting:</strong></div>
                                            <div class="col-sm-7"><span class="active_power_cmd"><?php //echo $active_power_cmd; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Reactive Power Setting:</strong></div>
                                            <div class="col-sm-7"><span id="reactive_power_cmd"><?php //echo $reactive_power_cmd; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Charge Limit:</strong></div>
                                            <div class="col-sm-7"><span id="soc_charge_limit"><?php //echo $soc_charge_limit; ?></span>%</div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Discharge Limit:</strong></div>
                                            <div class="col-sm-7"><span id="soc_discharge_limit"><?php //echo $soc_discharge_limit; ?></span>%</div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Power Factor:</strong></div>
                                            <div class="col-sm-7"><span id="power_factor"><?php //echo $power_factor; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Reconnect State:</strong></div>
                                            <div class="col-sm-7"><span id="reconnect_state"><?php //echo $reconnect_state; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Work State Control cmd:</strong></div>
                                            <div class="col-sm-7"><span id="work_state_control_cmd"><?php //echo $work_state_control_cmd; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                       <a href="battery_cell_info.php"> <button id="serial-scan" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#myModal">Get Battery Cell Info</button></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Battery String Overview</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery String Running:</strong></div>
                                            <div class="col-sm-7"><span id='battery_string_running'><?php //echo $battery_string_running; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery String Enabled:</strong></div>
                                            <div class="col-sm-7"><span id='battery_string_enabled'><?php //echo $battery_string_enabled; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery String Isolated:</strong></div>
                                            <div class="col-sm-7"><span class='battery_string_isolated'><?php //echo $battery_string_isolated; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Stack Voltage:</strong></div>
                                            <div class="col-sm-7"><span id='battery_stack_voltage'><?php //echo $battery_stack_voltage;?> </span></div>
                                        </div>

                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Stack Current:</strong></div>
                                            <div class="col-sm-7"><span id='battery_stack_current'><?php //echo $battery_stack_current;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Stack Power:</strong></div>
                                            <div class="col-sm-7"><span id='battery_stack_power'><?php //echo $battery_stack_power;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Stack SOC:</strong></div>
                                            <div class="col-sm-7"><span class='battery_stack_soc'><?php //echo $battery_stack_soc ;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Stack SOH:</strong></div>
                                            <div class="col-sm-7"><span id='battery_stack_soh'><?php //echo $battery_stack_soh;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>



                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">DC Voltage / Current</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Max Cell Voltage:</strong></div>
                                            <div class="col-sm-7"><span id='battery_max_cell_voltage'><?php //echo $battery_max_cell_voltage;?></span></div>
                                        </div>
                                        <hr class='hrRule'>

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Min Cell Voltage:</strong></div>
                                            <div class="col-sm-7"><span id='battery_min_cell_voltage'><?php //echo $battery_min_cell_voltage; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>DC Voltage:</strong></div>
                                            <div class="col-sm-7"><span id='dc_voltage'><?php //echo $dc_voltage;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>DC Current:</strong></div>
                                            <div class="col-sm-7"><span id='dc_current'><?php //echo $dc_current;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>DC Power:</strong></div>
                                            <div class="col-sm-7"><span id='dc_power'><?php //echo $dc_power;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Voltage Control cmd:</strong></div>
                                            <div class="col-sm-7"><span id='voltage_control_cmd'><?php //echo $voltage_control_cmd;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Frequency Control cmd :</strong></div>
                                            <div class="col-sm-7"><span id='frequency_control_cmd'><?php //echo $frequency_control_cmd;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Timestamp</strong></div>
                                            <div class="col-sm-7"><span id='timestamp'><?php echo $timestamp;?> </span></div>
                                        </div>
                                        <hr class='hrRule'>

                                    </div>
                                </div>
                            </div>

                        </div>



                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">DCAC Energy</h3></div>
                                    <div class="panel-body">

                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>P Value:</strong></div>
                                            <div class="col-sm-7"><span id='p_value'><?php //echo $p_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Q Value:</strong></div>
                                            <div class="col-sm-7"><span id='q_value'><?php //echo $q_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>S Value:</strong></div>
                                            <div class="col-sm-7"><span id='s_value'><?php //echo $s_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>IA Value:</strong></div>
                                            <div class="col-sm-7"><span id='ia_value'><?php //echo $ia_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>IB Value:</strong></div>
                                            <div class="col-sm-7"><span id='ib_value'><?php //echo $ib_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>IC Value:</strong></div>
                                            <div class="col-sm-7"><span id='ic_value'><?php //echo $ic_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UAB Value:</strong></div>
                                            <div class="col-sm-7"><span id='uab_value'><?php //echo $uab_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UBC Value:</strong></div>
                                            <div class="col-sm-7"><span id='ubc_value'><?php //echo $ubc_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UAC Value:</strong></div>
                                            <div class="col-sm-7"><span id='uac_value'><?php //echo $uac_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UA Value:</strong></div>
                                            <div class="col-sm-7"><span id='ua_value'><?php //echo $ua_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UB Value:</strong></div>
                                            <div class="col-sm-7"><span id='ub_value'><?php //echo $ub_value; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>UC Value:</strong></div>
                                            <div class="col-sm-7"><span id='uc_value'><?php //echo $uc_value; ?></span></div>
                                        </div>


                                        <hr class='hrRule'>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">AC Output</h3></div>
                                    <div class="panel-body">

                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase A amps:</strong></div>
                                            <div class="col-sm-7"><span id='a_amps'><?php //echo $a_amps; ?> </span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase B amps:</strong></div>
                                            <div class="col-sm-7"><span id='b_amps'><?php //echo $b_amps; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase C amps:</strong></div>
                                            <div class="col-sm-7"><span id='c_amps'><?php //echo $c_amps; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase A Volts:</strong></div>
                                            <div class="col-sm-7"><span id='a_volts'><?php echo $a_volts; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase B Volts:</strong></div>
                                            <div class="col-sm-7"><span id='b_volts'><?php echo $b_volts; ?></span></div>
                                        </div>

                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Phase C Volts:</strong></div>
                                            <div class="col-sm-7"><span id='c_volts'><?php echo $c_volts; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>True Power:</strong></div>
                                            <div class="col-sm-7"><span id='true_power'><?php echo $true_power; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Apparent Power:</strong></div>
                                            <div class="col-sm-7"><span id='apparent_power'><?php echo $apparent_power; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>



                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Frequency</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Frequency:</strong></div>
                                            <div class="col-sm-7"><span id='frequency'><?php //echo $frequency; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Temperatures</h3></div>
                                    <div class="panel-body">

                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery max cell temp:</strong></div>
                                            <div class="col-sm-7"><span id='battery_max_cell_temperature'><?php //echo $battery_max_cell_temperature; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery min cell temp </strong></div>
                                            <div class="col-sm-7"><span id='battery_min_cell_tempreature '><?php //echo $battery_min_cell_tempreature ; ?></span></div>
                                        </div>
                                        <hr class='hrRule'>

                                        <hr class='hrRule'>

                                    </div>
                                </div>
                            </div>

                        </div>



                    </div>
            </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>