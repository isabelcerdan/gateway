<?php
include_once ('functions/session.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Energy Bucket</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        button, input[type="button"] {

            text-shadow: none !important;
        }
        #stopSystem {
            display: none;
        }

        .smallerText {
            font-size: xx-small;
        }
        #Update_Success {
            display: none;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript">


        $(document).ready(function(){


                $(".startStopSystem").on('click', function () {
                    if (confirm("Are you sure you want to Reinitialize?") == true) {
                        var formDetails = $('#' + this.id);
                        $.ajax({
                            type: "POST",
                            url: 'control/form_bucket_enable.php',
                            data: formDetails.serialize(),
                            success: function (data) {

                            },
                            error: function (jqXHR, text, error) {

                            }
                        });
                        return false;
                    }
                });


            //Refresh auto_load() function after 10000 milliseconds
            setInterval(auto_gateway_status,2000);

            function auto_gateway_status(){

                $.ajax({
                    url: "control/get_bucket_status.php?data=yes",
                    cache: false,
                    success: function(data){

                        $("#auto_gateway_status_div").html(data);

                        // console.log(' success auto_gateway_status:' + $("#auto_gateway_status_div").html());

                        if( $("#auto_gateway_status_div").html() == 'Reinitialize pending: False') {
                            //console.log('System Offline');

                            $('#startSystem').show();
                            $('#stopSystem').hide();
                        }
                        if( $("#auto_gateway_status_div").html() == 'Reinitialize pending: True') {
                            console.log('System Online');
                            $('#stopSystem').show();
                            $('#startSystem').hide();
                        }

                    }
                });
            }

        });

    </script>
</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var
$update= mysqli_real_escape_string($conn, $_GET['update']);
$update = filter_var($update, FILTER_SANITIZE_STRING);

$minion_auditor_idURL = mysqli_real_escape_string($conn, $_GET['update']);
$minion_auditor_idURL = filter_var($minion_auditor_idURL, FILTER_SANITIZE_STRING);
$minion_auditor_idURL = $minion_auditor_idURL -1;
if($update == 'success') {
?>
<script type="text/javascript">


    $(document).ready(function(){

        $('#Update_Success').show();
        setTimeout(
            function()
            {
                $('#Update_Success').fadeOut(1000);
            }, 5000);
    });

</script>

<?php
}

include_once('control/get_gatewayMeter.php'); // Get default data

?>
<div class="container">

    <?php
    include_once ('header.php');

    
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Manage Energy Bucket</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>

                    <div style="padding: 30px;">


                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Main Energy Bucket Control</h3></div>
                                    <div class="panel-body">
                                        <p><strong>System State:</strong> <span id="auto_gateway_status_div"></span></p>

                                        <form class='startStopSystem' id="startSystem"  name="startStopSystem"  >
                                            <input type="hidden" name="reinitialize_bucket" value="1">


                                            <p class="text-center"> <button type="submit" class="btn btn-warning">Initialized   Now</button></p>
                                        </form>


                                        <form class='startStopSystem' id="stopSystem"  name="startStopSystem" >
                                            <input type="hidden" name="reinitialize_bucket" value="0">


                                            <p class="text-center"> <button type="submit" class="btn btn-warning">Reinitialize Now</button></p>
                                        </form>




                                    </div>
                                </div>
                            </div>


                        </div>

                        <div id="Update_Success">
                            <div class="alert alert-success">
                                <strong>Success!</strong> <?php echo $titleDB[$minion_auditor_idURL]; ?> Changed
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Bucket Control by Feed</h3></div>
                                    <div class="formBoxSection">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr style="font-size: smaller">

                                                    <th></th>
                                                    <th>FEED</th>
                                                    <th>Bucket Fullness</th>
                                                    <th>Bucket Coefficient</th>
                                                    <th>Bucket Size</th>
                                                    <th>Bucket Period</th>
                                                    <th>Bucket_Enabled</th>





                                                </tr>
                                                </thead>
                                                <tbody id="auto_brainbox_status_div">
                                                <?php
                                                for($i = 0;$i <  count($feed_DB); $i++) {
                                                    echo "<tr>
                                            <td class='text-center'><a href='bucket-add-edit.php?feed=$feed_DB[$i]'> <button class=\"btn btn-warning\" >Edit</button></a></td>
               
                                 
                                        <td class='text-center'>$feed_DB[$i]</td>
                                        <td class='text-center'>$Bucket_Fullness_DB[$i]</td>
                                         <td class='text-center'>$Bucket_Coefficient_DB[$i]</td>
                                         <td class='text-center'>$Bucket_Size_DB[$i]</td>
                                         <td class='text-center'>$Bucket_Period_DB[$i]</td>
                                         <td class='text-center'>$Bucket_Enabled_DB[$i]</td>
       
                                    ";
                                                    echo "</tr>";
                                                }

                                                ?>

                                                </tbody>
                                            </table>
                                        </div>


                                    </div>



                                </div>

                            </div>




                        </div>
                    </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>