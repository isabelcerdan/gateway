#!/bin/bash
#
# Shell provisioner for Ansible playbooks, based on KSid's
# windows-vagrant-ansible: https://github.com/KSid/windows-vagrant-ansible
#
# USAGE: ./self-provision.sh [apt-repo|verify|push|public|private] [drivers|firmware|watchdog|sensors|teamviewer9|gateway|mysql|apache|php|python|ruby|daemons|multicast|linsight]
#
# The first parameter specifies the type of provisioning
#
# Uncomment if behind a proxy server.
# export {http,https,ftp}_proxy='http://username:password@proxy-host:80'

PLAYBOOK_DIR=${PWD}
ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway.yml"
EXTRA_VARS="apt_repo=private"

if [ "$1" = "verify" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway_verify.yml"
    shift
elif [ "$1" = "push" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway_push.yml"
    shift
elif [ "$1" = "apt_repo" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/apt_repo.yml"
    shift
elif [ "$1" = "test" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway_test.yml"
    shift
elif [ "$1" = "clean" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway_clean.yml"
    shift
elif [ "$1" = "hostname" ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/gateway_hostname.yml"
    shift
elif [ "$1" = "string_sight" ] || [ -f /usr/share/apparent/string-sight ]; then
    ANSIBLE_PLAYBOOK="${PLAYBOOK_DIR}/string_sight.yml"
    sudo touch /usr/share/apparent/string-sight
    shift
fi

# Detect package management system.
APT=$(which apt 2>/dev/null)
VM=$(lsusb | grep VirtualBox 2>/dev/null)

# Make sure Ansible playbook exists.
if [ ! -f "$ANSIBLE_PLAYBOOK" ]; then
  echo "Cannot find Ansible playbook."
  exit 1
fi

if [ "$ANSIBLE_PLAYBOOK" = "${PLAYBOOK_DIR}/gateway.yml" ]; then
    if [ "$1" = "public" ]; then
        EXTRA_VARS="apt_repo=public"
        sudo cp ./roles/apt_depends/templates/sources.list.trusty.j2 /etc/apt/sources.list
        if [ -d "/etc/apt/sources.list.d.ignore" ]; then
            sudo mv /etc/apt/sources.list.d.ignore /etc/apt/sources.list.d
        fi
        shift
    else
        EXTRA_VARS="apt_repo=private"
        sudo cp ./roles/apt_depends/templates/sources.list.apparent.j2 /etc/apt/sources.list
        if [ -d "/etc/apt/sources.list.d" ]; then
            sudo mv /etc/apt/sources.list.d /etc/apt/sources.list.d.ignore
        fi
     fi
else
    EXTRA_VARS="apt_repo=public"
fi

# Install Ansible and its dependencies if it's not installed already.
echo "Installing Ansible dependencies and Git."
if [[ ! -z ${APT} ]]; then
sudo apt update
if [ "$EXTRA_VARS" = "apt_repo=public" ]; then
  sudo apt install -y wget git python python-dev software-properties-common
  sudo add-apt-repository -y ppa:ansible/ansible
  sudo apt update
  sudo apt install -y ansible
else
  sudo apt install -y wget git python python-dev ansible
fi
else
echo "ERROR: apt is not available!"
exit 1;
fi

VAULT_PASSWORD_PATH=vault-password-file
if [[ ! -z ${VM} ]]; then
  VAULT_PASSWORD_PATH=~/vault-password-file
  cp -f vault-password-file ${VAULT_PASSWORD_PATH}
  chmod 400 ${VAULT_PASSWORD_PATH}
fi

# Run the playbook.
echo "Running Ansible provisioner..."

TAGS=$1
TAG_OPT=""
if [ "$#" -eq 1 ]; then
  TAG_OPT="-t ${TAGS}"
fi

if [ "$EXTRA_VARS" ]; then
  EXTRA_VARS="-e ${EXTRA_VARS}"
fi

ansible-playbook --connection=local ${TAG_OPT} ${EXTRA_VARS} --vault-password-file ${VAULT_PASSWORD_PATH} "${ANSIBLE_PLAYBOOK}" -v
