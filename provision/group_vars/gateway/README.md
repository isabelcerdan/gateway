# Ansible Vault

## Introduction

Ansible vault is used to prevent the need for storing clear-text passwords in our source code repository.

### Edit Passwords in Vault File

```
$ ansible-vault edit vault.yml
```

