---

- name: install pyenv
  command: sh -c "curl https://pyenv.run | bash"
  args:
    creates: /home/gateway/.pyenv

- name: install pyenv virtualenv plugin
  git:
    repo: 'https://github.com/pyenv/pyenv-virtualenv.git'
    dest: "/home/gateway/.pyenv/plugins/pyenv-virtualenv"
    version: v1.1.3

- name: read .python-version
  set_fact:
    python_version: "{{ lookup('file', '{{ playbook_dir }}/../.python-version') }}"

- name: create ~/.pyenv/versions directory
  file:
    path: /home/gateway/.pyenv/versions
    state: directory
    mode: 0775

- name: install python {{ python_version }}
  unarchive:
    src: "{{ playbook_dir }}/roles/python/resources/python-{{ python_version }}.tar.gz"
    dest: /home/gateway/.pyenv/versions/
  args:
    creates: "/home/gateway/.pyenv/versions/{{ python_version }}"

- name: create virtualenv
  command: "/home/gateway/.pyenv/bin/pyenv virtualenv {{ python_version }} .py2-virtualenv"
  args:
    creates: "/home/gateway/.pyenv/versions/.py2-virtualenv"

- name: pip uninstall simple-db-migrate
  pip: name=simple-db-migrate state=absent

- name: "create {{ apparent_root }}/python directory"
  become: yes
  file:
    path: "{{ apparent_root }}/python"
    state: directory
    recurse: yes
    owner: root
    group: "{{ default_group }}"
    mode: 0775

- name: render requirements.txt
  become: yes
  template:
    src: requirements.txt.j2
    dest: "{{ apparent_root }}/python/requirements.txt"
    mode: 0755
    owner: root
    group: root

- name: install python dependencies in virtualenv
  pip:
    requirements: "{{ apparent_root }}/python/requirements.txt"
    virtualenv: "/home/gateway/.pyenv/versions/.py2-virtualenv"
    extra_args: "--exists-action w"

- name: overwrite .py2-virtualenv with new virtualenv
  become: yes
  synchronize:
    src: "/home/gateway/.pyenv/versions/.py2-virtualenv/"
    dest: "{{ virtualenv_path }}/"
    recursive: yes
    delete: yes

- name: configure .py2-virtualenv permissions
  become: yes
  file:
    state: directory
    path: "{{ virtualenv_path }}"
    mode: 0775
    owner: root
    group: "{{ default_group }}"

- name: add virtualenv activation to bashrc
  lineinfile:
    dest: /home/gateway/.bashrc
    line: 'source {{ virtualenv_path }}/bin/activate'
    insertafter: EOF
    state: present
