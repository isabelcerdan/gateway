#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import os

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

from lib.db import prepared_act_on_database, FETCH_ALL, EXECUTE

DB_ENGINE_MYISAM = 'MyISAM'
DB_ENGINE_INNODB = 'InnoDB'


def get_tables(**kwargs):
    engine = kwargs.get('engine', None)
    exclude_tables = kwargs.get('exclude_tables', None)
    include_tables = kwargs.get('include_tables', None)
    sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
    where_clause = "WHERE TABLE_SCHEMA = 'gateway_db'"
    if engine is not None:
        where_clause = " ".join([where_clause, "AND ENGINE='%s'" % engine])
    if exclude_tables is not None:
        exclude_tables = ["'%s'" % t for t in exclude_tables]
        where_clause = " ".join([where_clause, "AND TABLE_NAME NOT IN (%s)" % ",".join(exclude_tables)])
    if include_tables is not None:
        include_tables = ["'%s'" % t for t in include_tables]
        where_clause = " ".join([where_clause, "AND TABLE_NAME IN (%s)" % ",".join(include_tables)])

    sql = " ".join([sql, where_clause])
    results = prepared_act_on_database(FETCH_ALL, sql, [])
    return results


def migrate(new_engine):

    flip_engine = {
        DB_ENGINE_MYISAM: DB_ENGINE_INNODB,
        DB_ENGINE_INNODB: DB_ENGINE_MYISAM
    }

    # Ensure these tables are always MyISAM since they use "REPLACE INTO ..."
    # SQL statements that cause InnoDB tables to deadlock:
    # https://stackoverflow.com/questions/30814982/mysql-innodb-deadlock-problems-on-replace-into
    log_tables = [
        "pcc_readings_1",
        "pcc_readings_2",
        "solar_readings_1",
        "battery_readings_1",
        "curtail_log",
        "net_prot_log",
        "pcc_interval_averages",
        "cess_400_master_data_log",
        "energy_report_catchup",
        "pcc_readings_1_old",
        "pcc_readings_2_old",
        "solar_readings_1_old",
        "battery_readings_1_old",
        "pcc_readings_1_old_01",
        "pcc_readings_2_old_01",
        "solar_readings_1_old_01",
        "battery_readings_1_old_01"
    ]

    old_engine = flip_engine[new_engine]
    old_engine_results = get_tables(engine=old_engine, exclude_tables=log_tables)
    innodb_log_results = get_tables(engine=DB_ENGINE_INNODB, include_tables=log_tables)
    if len(old_engine_results) == 0 and len(innodb_log_results) == 0:
        print "No tables to convert."
        return

    for result in old_engine_results:
        table_name = result['TABLE_NAME']
        sql = "ALTER TABLE %s ENGINE=\'%s\'" % (table_name, new_engine)
        print sql
        prepared_act_on_database(EXECUTE, sql, [])

    for result in innodb_log_results:
        table_name = result['TABLE_NAME']
        sql = "ALTER TABLE %s ENGINE=\'%s\'" % (table_name, DB_ENGINE_MYISAM)
        print sql
        prepared_act_on_database(EXECUTE, sql, [])


def print_status(engine):
    for result in get_tables(engine=engine):
        table_name = result['TABLE_NAME']
        print "%32s: %s" % (table_name, engine)


if __name__ == '__main__':
    command = 'status'
    if len(sys.argv) > 1:
        command = sys.argv[1].lower()
    if command == 'innodb':
        migrate(DB_ENGINE_INNODB)
    elif command == 'myisam':
        migrate(DB_ENGINE_MYISAM)
    elif command == 'status':
        print_status(DB_ENGINE_MYISAM)
        print_status(DB_ENGINE_INNODB)
