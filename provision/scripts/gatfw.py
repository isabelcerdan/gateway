#!/usr/share/apparent/.py2-virtualenv/bin/python
import argparse
import iptc         # https://ldx.github.io/python-iptables/usage.html


class PortPolicy(object):
    def __init__(self, **kwargs):
        self.dport = kwargs.get('dport', None)
        self.open = kwargs.get('open', False)
        self.rule = None


class Firewall(object):
    wan_interface = 'eth0'

    def __init__(self):
        self.policies = {
            'ssh': PortPolicy(dport=22),
            'http': PortPolicy(dport=80),
            'https': PortPolicy(dport=443),
            'mysql': PortPolicy(dport=3306),
            'shared-meter': PortPolicy(dport=64437),
            'sep2': PortPolicy(dport=64438),
        }
        self.dports = [pp.dport for _, pp in self.policies.iteritems()]
        self.filter_table = iptc.Table(iptc.Table.FILTER)
        self.input_chain = iptc.Chain(self.filter_table, "INPUT")

        for rule, match in self.rules():
            try:
                dport = int(match.parameters['dport'])
                if dport in self.dports:
                    for _, pp in self.policies.iteritems():
                        if dport == pp.dport:
                            pp.open = (rule.target.name == "ACCEPT")
                            pp.rule = rule
                            break
            except:
                pass

    def rules(self):
        for rule in self.input_chain.rules:
            for match in rule.matches:
                if 'dport' in match.parameters:
                    yield (rule, match)

    def update(self, proto, rule):
        pp = self.policies[proto]
        if rule == 'open':
            if not pp.open:
                rule = iptc.Rule()
                rule.in_interface = self.wan_interface
                match = iptc.Match(rule, "tcp")
                match.state = "NEW"
                match.set_parameter("dport", pp.dport)
                target = iptc.Target(rule, name="ACCEPT")
                rule.add_match(match)
                rule.target = target
                self.input_chain.insert_rule(rule, position=self.insert_position)
        elif rule == 'close':
            if pp.open:
                iptc.delete_rule(pp.rule)

    def display(self):
        for proto, pp in self.policies.iteritems():
            print("%12s: %s" % (proto, "open" if pp.open else "closed"))


if __name__ == '__main__':
    p = argparse.ArgumentParser(description='Gateway WAN firewall.')
    p.add_argument('proto', type=str, nargs="?", help='ssh|http|https|mysql|share-meter|sep2')
    p.add_argument('rule', type=str, nargs="?", help='open|close')
    args = p.parse_args()

    fw = Firewall()
    if args.rule is None and args.proto is None:
        fw.display()
    elif args.rule is None or args.proto is None:
        p.print_help()
    else:
        fw.update(args.rule, args.proto)