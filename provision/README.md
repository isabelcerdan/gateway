# Gateway Provisioning

## Introduction

Computer provisioning is a set of actions to prepare a server with appropriate systems, data and software, and make it ready for operation.

Gateway provisioning steps have evolved from the steps outlined in **MGI Gateway Exporatory Methodology** [1] and are now captured in the [ansible](https://www.ansible.com) playbook gateway.yml.

If you don't need the latest gateway software, you can restore a gateway from ISO file. System Restore ISO files are created for every major release and made available on Synology at **\\\192.168.10.50\Gateway\System ISOs**. Instructions for restoring from ISO can be found in ./Documents/System_Restore.md. 

### Zentyal Installation and Gateway Provisioning

1. Power off at brick (not front panel).
1. **IMPORTANT: REMOVE JWD1 JUMPER (NEXT TO SATA PORTS) TO DISABLE THE WATCHDOG BEFORE PERFORMING THE STEPS BELOW (OTHERWISE THE WATCHDOG WILL REBOOT BEFORE YOUR INSTALLATION COMPLETES).**
1. Insert Zentyal 4.1 Boot USB 
    1. A USB drive labelled **Zentyal 4.1** should be available on Patrice's desk. 
    1. If you need to create a new Zentyal 4.1 USB drive, see section 6 of [1] for instructions.
1. Power up
    1. If your morex can't get past SuperMicro splash screen, check BIOS post codes in `.\Documents\motherboard\Produto28317IdArquivo3086.pdf`.  
1. Enter BIOS setup. First Setup pass
    1. `Main>`
        Note Mobo Firmware version. If Firmware Revision < 1.1a, then upgrade the firmware per Section 8 in [1].
        Afterwards rerun the Top-Level Build procedure again.
    1. `Advanced>`
        1. `Boot config`
            1. `Watchdog: enable`
                NOTE: because jumper JWD1 is pulled, OEM watchdog timer will run but will not cause reset.
            1. `Restore Power on AC Loss: Power On`
        1. `SATA config`
            1. `SATA 2 controller: disable`
        1. `PCIe/PCI/PnP`
            1. `Storage opRom: UEFI`
            1. `Video opRom: UEFI`
            1. `PCIe Slot 1 opRom: disable`
    1. `Boot Priority>`
        1. `Priority #1: UEFI USB`
        1. `Priority #2: UEFI SSD`
        1. `Priority #3: UEFI Shell`
    1. `IPMI config>`
        1. If IPMI firmware revision is < 1.77, it will be updated by provisioning.
    1. F4 to `Save & reset`
1. Enter BIOS setup. Second Setup pass
    1. `Advanced>Boot>CSM: disabled`
    1. F4 to `Save & reset` 
1. Boot from Zentyal 4.1 USB and install Zentyal.
    1. Use defaults except for parameters outlined below:
        1. Hostname: `zentyalmorex`
        1. Username: `gateway`
        1. Password: `q********u`
        1. If restoring an already provisioned system:
            1. Unmount partitions that are in use?: `Yes`
            1. Partitioning method: `Guided - use entire disk and setup LVM`
            1. Select disk to partition: `SCSI (0,0,0) (sda) - 300.1 GB ATA INTEL SSDSC2BB30`
            1. Remove existing logical volume data?: `Yes`
            1. Write changes to disks and configure LVM?: `Yes`
            1. Amount of volume group to use for guided partitioning: (should show 299.3 GB) `Continue`
            1. Write the changes to disks?: `Yes`
        1. System should reboot after Zentyal installation. Note: first boot displays blank screen for several minutes before finally launching Zentyal.  
1. If the gateway is not on the Novato office LAN, you'll need to rename the domain to `zentyal-domain.lan`:
    1. Login to Zentyal webadmin using username/password from pervious step
    1. Navigate to System > General
    1. Update Domain to `zentyal-domain.lan` under Hostname and Domain section.
    1. Click `Change`
    1. Click `Save Changes` in upper right
1. Open command line and run the following commands:
```
$ sudo apt-get install git
$ git clone https://bitbucket.org/apparentinc/gateway.git
Cloning into 'gateway'...
Username for 'https://bitbucket.org': apparent_release 
Password for 'https://apparent_release@bitbucket.org': q********u
$ cd ~/gateway/provision
$ echo 'q*******u' > vault-password-file
$ ./self-provision.sh
```
1. Update Zentyal settings:
    1. Open Zentyal dashboard.
    1. All necessary modules should have been installed as part of provisioning so continue and skip installation step.
    1. Navigate to `System > General` in left-side nav.
    1. Under `Hostname and Domain`, append unique number to end of `zentyalmorex` hostname (e.g. `zentyalmorex9`).
    1. Click `OK` in `Change hostname dialog box.
    1. Click `Save Changes
1. Power off at brick
1. Replace JWD1 jumper on pins 1-2 and power up
    1. verify machine does not automatically reboot after 5 mins
1. From command line:
    ```
    $ ./self-provision.sh verify
    ```
    Machine should reboot 1 minute after script completes.
  
#### BIOS Update
  
1. Enter BIOS by pressing `Delete` at boot and check BIOS version under `Main>`. To update BIOS to 1.1a (some boxes were at 1.0c):
    1. Short mobo pins JBT1
    1. Boot BIOS USB drive
    1. From DOS prompt (replace BIOSname.### with appropriate name and version):
    ```
    DOS> flash.BAT BIOSname.###.
    ```         
  
  
### Trouble-shooting

The self-provision.sh script takes an optional tag argument. A tag allows for running a subset of the provisioning steps. The following tags are available:

* pip
* supermicro
* teamviewer
* gateway 
* zentyal
* mysql
* apache
* php
* python
* gateway

For example, to only rerun provisioning for mysql, use the following command:
```
$ ./self-provision.sh mysql
```

### Gateway Applications
1. To access the configurator, go to: http://localhost
1. To access the gui, go to: https://localhost
1. To stop the daemons and stop monitoring daemons, use: 
```
$ nogat
```

## References:
[1] "MGI Gateway Exploratory Project Methodology", Ron Knipper, Rev2.01 `./Documents/MGI Gateway Exploratory Methodology.pdf`