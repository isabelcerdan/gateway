@echo off

echo.
if "%1"=="" goto error

SET HOUR=%TIME:~0,2%
IF "%HOUR:~0,1%" == " " SET HOUR=0%HOUR:~1,1%
set TIMESTAMP=%DATE:~-4%%DATE:~4,2%%DATE:~7,2%%HOUR%%TIME:~3,2%%TIME:~6,2%
set MIGRATION=migrations\%TIMESTAMP%_%1.migration
copy template\template.migration %MIGRATION%

echo Successfully created %MIGRATION%

exit /b 0

:error
echo USAGE:  db-migrate-create.bat new_migration

@echo on