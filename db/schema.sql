-- MySQL dump 10.13  Distrib 5.5.58, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gateway_db
-- ------------------------------------------------------
-- Server version	5.5.58-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__db_version__`
--

DROP TABLE IF EXISTS `__db_version__`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__db_version__` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sql_up` longtext,
  `sql_down` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alarms`
--

DROP TABLE IF EXISTS `alarms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarms` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `severity` varchar(32) NOT NULL DEFAULT '',
  `strategy` varchar(64) NOT NULL DEFAULT 'set_and_auto_clear',
  `raised` tinyint(1) NOT NULL DEFAULT '0',
  `er_alarm` tinyint(1) NOT NULL DEFAULT '0',
  `defined_count` smallint(5) NOT NULL DEFAULT '0',
  `defined_duration` int(11) NOT NULL DEFAULT '0',
  `current_count` smallint(11) NOT NULL DEFAULT '0',
  `current_begin` int(11) DEFAULT NULL,
  `clear_pending` tinyint(1) NOT NULL DEFAULT '0',
  `last_raised` int(11) DEFAULT NULL,
  `debounce_duration` int(11) NOT NULL DEFAULT '900',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alarms_history`
--

DROP TABLE IF EXISTS `alarms_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarms_history` (
  `time` int(11) NOT NULL,
  `alarms` varchar(512) DEFAULT NULL,
  `er_sync` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auditor_control`
--

DROP TABLE IF EXISTS `auditor_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditor_control` (
  `enabled` tinyint(1) DEFAULT '1',
  `pid` varchar(10) DEFAULT NULL,
  `active_id` int(11) DEFAULT NULL,
  `operating` varchar(5) NOT NULL DEFAULT 'YES',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `running` tinyint(1) NOT NULL DEFAULT '0',
  `updated` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brainboxes`
--

DROP TABLE IF EXISTS `brainboxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brainboxes` (
  `brainbox_id` int(11) NOT NULL AUTO_INCREMENT,
  `target_interface` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `model_number` varchar(100) DEFAULT NULL,
  `apparent_part` varchar(100) DEFAULT NULL,
  `IP_Address` varchar(16) DEFAULT NULL,
  `mac_address` varchar(18) DEFAULT NULL,
  `baud` int(11) DEFAULT '38400',
  `tcp_port` int(6) DEFAULT '9001',
  `tcp_port_serial_1` int(6) DEFAULT '9001',
  `tcp_port_serial_2` int(6) DEFAULT '9002',
  `enabled` tinyint(1) DEFAULT '1',
  `tunnel_open` tinyint(1) DEFAULT '0',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `pid` varchar(20) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ER_Connect` varchar(2) DEFAULT NULL,
  `running` tinyint(1) NOT NULL DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  PRIMARY KEY (`brainbox_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtail_log`
--

DROP TABLE IF EXISTS `curtail_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtail_log` (
  `entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(20) DEFAULT 'Unknown',
  `Curtail_Value_A` float DEFAULT NULL,
  `Curtail_Value_B` float DEFAULT NULL,
  `Curtail_Value_C` float DEFAULT NULL,
  `Curtail_Value_Delta` float DEFAULT NULL,
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `row_id` (`row_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discovery`
--

DROP TABLE IF EXISTS `discovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discovery` (
  `discovery_type` varchar(30) NOT NULL DEFAULT '',
  `requested_scan_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `inverters_discovered` int(11) DEFAULT '0',
  `requested` tinyint(1) DEFAULT '0',
  `running` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `failed` tinyint(1) DEFAULT '0',
  `pid` varchar(20) DEFAULT NULL,
  `last_time_scanned` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `new_ip_discovered` datetime DEFAULT NULL,
  PRIMARY KEY (`discovery_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `energy_bucket`
--

DROP TABLE IF EXISTS `energy_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `energy_bucket` (
  `Sample_ID` int(11) DEFAULT NULL,
  `Sample_Time` datetime DEFAULT NULL,
  `Accumulated_Real_Energy_Phase_A` float DEFAULT NULL,
  `Accumulated_Real_Energy_Phase_B` float DEFAULT NULL,
  `Accumulated_Real_Energy_Phase_C` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `energy_bucket_config`
--

DROP TABLE IF EXISTS `energy_bucket_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `energy_bucket_config` (
  `reinitialize_bucket` tinyint(1) DEFAULT '1',
  `Current_Sample` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `energy_meter`
--

DROP TABLE IF EXISTS `energy_meter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `energy_meter` (
  `meter_id` int(11) NOT NULL AUTO_INCREMENT,
  `meter_name` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `mac_address` varchar(100) DEFAULT NULL,
  `meter_role` varchar(20) DEFAULT NULL,
  `meter_class` int(11) DEFAULT NULL,
  `modbusdevicenumber` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `baud` int(11) DEFAULT NULL,
  `network_comm_type` varchar(100) DEFAULT NULL,
  `feed_of_phase_A` varchar(20) DEFAULT NULL,
  `feed_of_phase_B` varchar(20) DEFAULT NULL,
  `feed_of_phase_C` varchar(20) DEFAULT NULL,
  `ER_Connect` varchar(2) DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `online_timeout` float NOT NULL DEFAULT '2',
  `delta` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`meter_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `energy_meter_schema`
--

DROP TABLE IF EXISTS `energy_meter_schema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `energy_meter_schema` (
  `e_meter_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceclass` varchar(45) DEFAULT NULL,
  `configuration` varchar(255) DEFAULT NULL,
  `energy_metercol` blob,
  `configurationchangetime` datetime DEFAULT NULL,
  `configurationchecksum` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`e_meter_config_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `energy_report_config`
--

DROP TABLE IF EXISTS `energy_report_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `energy_report_config` (
  `URL_AQ` varchar(255) DEFAULT 'http://datain.xetenergy.com:8081/AcquiSuite/servlet/gatewaysync.php',
  `transmit_frequency` int(11) DEFAULT '3600',
  `timeout` int(11) DEFAULT '4',
  `retries` int(11) DEFAULT '3',
  `enabled` varchar(1) DEFAULT '0',
  `gateway_base_sn` varchar(50) DEFAULT NULL,
  `data_selection_tool` varchar(100) DEFAULT NULL,
  `URL_AQ_Meter` varchar(255) DEFAULT 'http://datain.xetenergy.com:8081/AcquiSuite/servlet/gatewayload.php',
  `last_sent` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `severity` varchar(32) NOT NULL,
  `time` int(11) NOT NULL,
  `process` varchar(64) NOT NULL,
  `message` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=670 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events_tags`
--

DROP TABLE IF EXISTS `events_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=768 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fd_by_string`
--

DROP TABLE IF EXISTS `fd_by_string`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fd_by_string` (
  `stringId` varchar(16) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `feed_current` varchar(3) DEFAULT NULL,
  `feed_discovered` varchar(3) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `feed_command` varchar(20) DEFAULT NULL,
  `runtime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_config`
--

DROP TABLE IF EXISTS `feed_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_config` (
  `FEED_NAME` varchar(32) NOT NULL DEFAULT '',
  `FEED_SIZE` float DEFAULT '0',
  `OVERRIDE_RAMP_RATE` float DEFAULT '3',
  `OVERRIDE_RAMP_RATE_coeff` float DEFAULT '1',
  `MINIMUM_IMPORT` float DEFAULT '-0.06912',
  `MINIMUM_IMPORT_coeff` float DEFAULT '-3',
  `CREEP_THRESHOLD` float DEFAULT '-0.18432',
  `CREEP_THRESHOLD_coeff` float DEFAULT '-10',
  `CURTAIL_TARGET` float DEFAULT '-0.1152',
  `CURTAIL_TARGET_coeff` float DEFAULT '-7',
  `EXCESS_IMPORT` float DEFAULT '-0.2304',
  `EXCESS_IMPORT_coeff` float DEFAULT '-12',
  `ENABLED` varchar(1) DEFAULT '0',
  `bucket_size` float DEFAULT '0',
  `bucket_period` int(11) DEFAULT '32',
  `bucket_enabled` varchar(1) DEFAULT '0',
  PRIMARY KEY (`FEED_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_discovery_control`
--

DROP TABLE IF EXISTS `feed_discovery_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_discovery_control` (
  `command` varchar(30) DEFAULT NULL,
  `runs` int(11) DEFAULT NULL,
  `mode` varchar(10) DEFAULT NULL,
  `number_of_strings` int(11) DEFAULT NULL,
  `current_string` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `completion_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gateway` (
  `gateway_id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(100) DEFAULT NULL,
  `total_output_limit` float DEFAULT NULL,
  `system_enable` varchar(1) DEFAULT NULL,
  `update_config` varchar(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `SerialNumber` varchar(100) DEFAULT NULL,
  `system_restart` varchar(1) DEFAULT '0',
  `siteId` int(11) DEFAULT NULL,
  `uptime` int(11) DEFAULT '0',
  `stringSight` varchar(2) DEFAULT NULL,
  `log_level` varchar(10) NOT NULL DEFAULT 'INFO',
  `backup_includes_readings` tinyint(1) DEFAULT '0',
  `ramp_rate_knee` float DEFAULT '1',
  `ramp_rate_slope` float DEFAULT '2',
  PRIMARY KEY (`gateway_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gatewayMeter`
--

DROP TABLE IF EXISTS `gatewayMeter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gatewayMeter` (
  `feed` varchar(1) DEFAULT NULL,
  `pccNetPower` float DEFAULT '0',
  `solarPower` float DEFAULT '0',
  `curtailSetPoint` float DEFAULT '0',
  `auxPower` float DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '1',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `pid` varchar(10) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `net_export` float DEFAULT '0',
  `pccNetReact` float DEFAULT '0',
  `pccNetApparent` float DEFAULT '0',
  `pccPF` float DEFAULT '0',
  `pccVab` float DEFAULT NULL,
  `pccVbc` float DEFAULT NULL,
  `pccVca` float DEFAULT NULL,
  `pccVln` float DEFAULT NULL,
  `pccCurrent` float DEFAULT NULL,
  `Bucket_Fullness` float DEFAULT '0',
  `Bucket_Coefficient` float DEFAULT '0',
  `Reinitialize_Bucket` tinyint(1) DEFAULT '0',
  `solarCurrent` float DEFAULT NULL,
  `ramp_rate` float DEFAULT NULL,
  `updated` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `icpcon_control`
--

DROP TABLE IF EXISTS `icpcon_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icpcon_control` (
  `enabled` tinyint(1) DEFAULT '0',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `pid` varchar(10) DEFAULT NULL,
  `active_id` int(11) DEFAULT NULL,
  `suspended` varchar(5) DEFAULT NULL,
  `poll_freq_ms` int(5) NOT NULL DEFAULT '500',
  `updated` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `icpcon_devices`
--

DROP TABLE IF EXISTS `icpcon_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icpcon_devices` (
  `mac_addr` varchar(18) NOT NULL,
  `ip_addr` varchar(20) DEFAULT NULL,
  `device_name` varchar(32) DEFAULT 'PET-7202',
  `modbus_id` tinyint(2) NOT NULL DEFAULT '1',
  `di0_input_name` varchar(64) DEFAULT 'D0',
  `di0_tripped_value` tinyint(1) NOT NULL DEFAULT '1',
  `di0_register` tinyint(1) DEFAULT NULL,
  `di0_volatile_register` tinyint(1) DEFAULT NULL,
  `di1_input_name` varchar(64) DEFAULT 'D1',
  `di1_tripped_value` tinyint(1) NOT NULL DEFAULT '1',
  `di1_register` tinyint(1) DEFAULT NULL,
  `di1_volatile_register` tinyint(1) DEFAULT NULL,
  `di2_input_name` varchar(64) DEFAULT 'D2',
  `di2_tripped_value` tinyint(1) NOT NULL DEFAULT '1',
  `di2_register` tinyint(1) DEFAULT NULL,
  `di2_volatile_register` tinyint(1) DEFAULT NULL,
  `di3_input_name` varchar(64) DEFAULT 'D3',
  `di3_tripped_value` tinyint(1) NOT NULL DEFAULT '1',
  `di3_register` tinyint(1) DEFAULT NULL,
  `di3_volatile_register` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mac_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inverter_discovery_temp`
--

DROP TABLE IF EXISTS `inverter_discovery_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inverter_discovery_temp` (
  `mac_address` varchar(18) NOT NULL DEFAULT '',
  `ip_address` varchar(16) DEFAULT NULL,
  `breaker_number` varchar(16) DEFAULT NULL,
  `last_update` int(11) NOT NULL,
  PRIMARY KEY (`mac_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inverter_upgrade`
--

DROP TABLE IF EXISTS `inverter_upgrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inverter_upgrade` (
  `upgrade_id` int(11) DEFAULT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `software_directory` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `pref_svn_version` varchar(50) NOT NULL DEFAULT 'svn1090',
  `bootloader_directory` varchar(150) DEFAULT NULL,
  `bootloader_file_name` varchar(50) DEFAULT NULL,
  `boot_rev` int(11) DEFAULT '40',
  `upgrade_command` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inverters`
--

DROP TABLE IF EXISTS `inverters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inverters` (
  `inverters_id` int(11) NOT NULL AUTO_INCREMENT,
  `serialNumber` varchar(100) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `mac_address` varchar(18) DEFAULT NULL,
  `IP_Address` varchar(16) NOT NULL DEFAULT '0.0.0.0',
  `version` varchar(45) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `stringId` varchar(16) NOT NULL DEFAULT '00:00:00',
  `stringPosition` int(11) NOT NULL DEFAULT '0',
  `export_status` varchar(45) DEFAULT NULL,
  `prodPartNo` varchar(45) DEFAULT NULL,
  `sw_profile` int(11) NOT NULL DEFAULT '10',
  `output_status` int(11) DEFAULT NULL,
  `controlledByGateway` varchar(10) DEFAULT NULL,
  `FEED_NAME` varchar(3) DEFAULT NULL,
  `inverter_lock` varchar(10) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `comm` varchar(20) DEFAULT NULL,
  `upgrade_status` varchar(100) DEFAULT NULL,
  `upgrade_progress` varchar(50) DEFAULT NULL,
  `das_boot` varchar(255) DEFAULT NULL,
  `resets` int(11) NOT NULL DEFAULT '0',
  `watts` int(11) NOT NULL DEFAULT '0',
  `pf` float NOT NULL DEFAULT '0.95',
  `pf_c_lead` tinyint(1) NOT NULL DEFAULT '0',
  `pf_enable` tinyint(1) NOT NULL DEFAULT '1',
  `sis_consec_nr` int(11) NOT NULL DEFAULT '0',
  `sis_last_resp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sis_1st_resp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`inverters_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ip_address_white`
--

DROP TABLE IF EXISTS `ip_address_white`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_address_white` (
  `IP_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IP_Address` varchar(20) NOT NULL,
  `notes` varchar(255) NOT NULL,
  PRIMARY KEY (`IP_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meter_feeds`
--

DROP TABLE IF EXISTS `meter_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meter_feeds` (
  `meter_name` varchar(32) NOT NULL,
  `feed_name` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `threshold` float DEFAULT NULL,
  PRIMARY KEY (`meter_name`,`feed_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `minion_auditor`
--

DROP TABLE IF EXISTS `minion_auditor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `minion_auditor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) DEFAULT NULL,
  `runs` int(11) DEFAULT NULL,
  `next_launch_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `completion_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `results_path_file_name` varchar(50) DEFAULT NULL,
  `frequency` varchar(12) DEFAULT NULL,
  `time_value` int(11) DEFAULT NULL,
  `active` varchar(5) DEFAULT NULL,
  `run_now` varchar(5) DEFAULT 'NO',
  `progress` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `net_prot_log`
--

DROP TABLE IF EXISTS `net_prot_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `net_prot_log` (
  `mac_addr` varchar(18) DEFAULT NULL,
  `di0_volatile_register` tinyint(1) DEFAULT NULL,
  `di1_volatile_register` tinyint(1) DEFAULT NULL,
  `di2_volatile_register` tinyint(1) DEFAULT NULL,
  `di3_volatile_register` tinyint(1) DEFAULT NULL,
  `entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(10) unsigned NOT NULL,
  `timestamp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `row_id` (`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `netprotect_email`
--

DROP TABLE IF EXISTS `netprotect_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `netprotect_email` (
  `input_name` varchar(64) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT 'Support',
  `email_address` varchar(255) DEFAULT 'support@apparent.com',
  `from_name` varchar(64) DEFAULT 'Apparent Gateway Control',
  `from_address` varchar(64) DEFAULT 'customer.service@apparent.com',
  `msg_body` varchar(96) NOT NULL DEFAULT 'Inverter network shut down because of a trip event on ',
  `disclaimer` varchar(96) NOT NULL DEFAULT 'The inverter network will remain shut down until the Gateway is manually restarted.',
  `num_resends` int(5) NOT NULL DEFAULT '3',
  `resend_interval` int(5) NOT NULL DEFAULT '900',
  `send_count` int(3) NOT NULL DEFAULT '0',
  `next_send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `netprotect_email_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`netprotect_email_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notify_control`
--

DROP TABLE IF EXISTS `notify_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notify_control` (
  `enabled` tinyint(1) DEFAULT '0',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `pid` varchar(10) DEFAULT NULL,
  `active_id` int(11) DEFAULT NULL,
  `suspended` varchar(5) DEFAULT NULL,
  `audit_frequency` int(3) NOT NULL DEFAULT '10',
  `updated` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nplog_control`
--

DROP TABLE IF EXISTS `nplog_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nplog_control` (
  `enabled` tinyint(1) DEFAULT '0',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `pid` varchar(10) DEFAULT NULL,
  `active_id` int(11) DEFAULT NULL,
  `suspended` varchar(5) DEFAULT NULL,
  `log_interval` int(5) DEFAULT '500',
  `log_cycle` bigint(20) DEFAULT '2592000',
  `updated` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nxo_status`
--

DROP TABLE IF EXISTS `nxo_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nxo_status` (
  `Profile` varchar(100) DEFAULT 'Default',
  `Export_PCC_1_A` float DEFAULT NULL,
  `Export_PCC_1_B` float DEFAULT NULL,
  `Export_PCC_1_C` float DEFAULT NULL,
  `Export_PCC_1_Delta` float DEFAULT NULL,
  `Export_PCC_2_A` float DEFAULT NULL,
  `Export_PCC_2_B` float DEFAULT NULL,
  `Export_PCC_2_C` float DEFAULT NULL,
  `Export_PCC_2_Delta` float DEFAULT NULL,
  `Export_PCC_Combo_A` float DEFAULT NULL,
  `Export_PCC_Combo_B` float DEFAULT NULL,
  `Export_PCC_Combo_C` float DEFAULT NULL,
  `Export_PCC_Combo_Delta` float DEFAULT NULL,
  `Export_Solar_1_A` float DEFAULT NULL,
  `Export_Solar_1_B` float DEFAULT NULL,
  `Export_Solar_1_C` float DEFAULT NULL,
  `Export_Solar_1_Delta` float DEFAULT NULL,
  `Curtail_PCC_1_A` float DEFAULT NULL,
  `Curtail_PCC_1_B` float DEFAULT NULL,
  `Curtail_PCC_1_C` float DEFAULT NULL,
  `Curtail_PCC_1_Delta` float DEFAULT NULL,
  `Curtail_PCC_2_A` float DEFAULT NULL,
  `Curtail_PCC_2_B` float DEFAULT NULL,
  `Curtail_PCC_2_C` float DEFAULT NULL,
  `Curtail_PCC_2_Delta` float DEFAULT NULL,
  `Curtail_PCC_Combo_A` float DEFAULT NULL,
  `Curtail_PCC_Combo_B` float DEFAULT NULL,
  `Curtail_PCC_Combo_C` float DEFAULT NULL,
  `Curtail_PCC_Combo_Delta` float DEFAULT NULL,
  `Curtail_Solar_1_A` float DEFAULT NULL,
  `Curtail_Solar_1_B` float DEFAULT NULL,
  `Curtail_Solar_1_C` float DEFAULT NULL,
  `Curtail_Solar_1_Delta` float DEFAULT NULL,
  `Curtail_Value_A` float DEFAULT '0',
  `Curtail_Value_B` float DEFAULT '0',
  `Curtail_Value_C` float DEFAULT '0',
  `Curtail_Value_Delta` float DEFAULT '0',
  `Update_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timestamp_pcc_1` varchar(100) DEFAULT NULL,
  `timestamp_pcc_2` varchar(100) DEFAULT NULL,
  `timestamp_solar_1` varchar(100) DEFAULT NULL,
  UNIQUE KEY `Profile` (`Profile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_permissions`
--

DROP TABLE IF EXISTS `page_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_permissions` (
  `page_perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(20) DEFAULT NULL,
  `pageURL` varchar(512) DEFAULT NULL,
  `access_level` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`page_perm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pcc_interval_averages`
--

DROP TABLE IF EXISTS `pcc_interval_averages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcc_interval_averages` (
  `row_id` int(11) NOT NULL,
  `read_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Pf_A` float NOT NULL DEFAULT '0',
  `Pf_B` float NOT NULL DEFAULT '0',
  `Pf_C` float NOT NULL DEFAULT '0',
  `kW_A` float NOT NULL DEFAULT '0',
  `kW_B` float NOT NULL DEFAULT '0',
  `kW_C` float NOT NULL DEFAULT '0',
  `kvar_A` float NOT NULL DEFAULT '0',
  `kvar_B` float NOT NULL DEFAULT '0',
  `kvar_C` float NOT NULL DEFAULT '0',
  `quad_feed_A` tinyint(1) NOT NULL DEFAULT '0',
  `quad_feed_B` tinyint(1) NOT NULL DEFAULT '0',
  `quad_feed_C` tinyint(1) NOT NULL DEFAULT '0',
  `interval_time` datetime DEFAULT NULL,
  PRIMARY KEY (`read_id`),
  UNIQUE KEY `row_id` (`row_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26292 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pcc_readings_1`
--

DROP TABLE IF EXISTS `pcc_readings_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcc_readings_1` (
  `Accumulated_Real_Energy_Net` float DEFAULT NULL,
  `Accumulated_Real_Energy_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_Export` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_1` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_2` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_3` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_4` float DEFAULT NULL,
  `Apparent_Energy_Net` float DEFAULT NULL,
  `Apparent_Energy_Import` float DEFAULT NULL,
  `Apparent_Energy_Export` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power` float DEFAULT NULL,
  `Total_Net_Power_Factor` float DEFAULT NULL,
  `Voltage_LL_Avg` float DEFAULT NULL,
  `Voltage_LN_Avg` float DEFAULT NULL,
  `Current_Avg` float DEFAULT NULL,
  `Frequency` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export` float DEFAULT NULL,
  `Pulse_Counter_1` float DEFAULT NULL,
  `Pulse_Counter_2` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q4_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export` float DEFAULT NULL,
  `Real_Power_A` float DEFAULT NULL,
  `Real_Power_B` float DEFAULT NULL,
  `Real_Power_C` float DEFAULT NULL,
  `Reactive_Power_A` float DEFAULT NULL,
  `Reactive_Power_B` float DEFAULT NULL,
  `Reactive_Power_C` float DEFAULT NULL,
  `Apparent_Power_A` float DEFAULT NULL,
  `Apparent_Power_B` float DEFAULT NULL,
  `Apparent_Power_C` float DEFAULT NULL,
  `Power_Factor_A` float DEFAULT NULL,
  `Power_Factor_B` float DEFAULT NULL,
  `Power_Factor_C` float DEFAULT NULL,
  `Voltage_AB` float DEFAULT NULL,
  `Voltage_BC` float DEFAULT NULL,
  `Voltage_AC` float DEFAULT NULL,
  `Voltage_AN` float DEFAULT NULL,
  `Voltage_BN` float DEFAULT NULL,
  `Voltage_CN` float DEFAULT NULL,
  `Current_A` float DEFAULT NULL,
  `Current_B` float DEFAULT NULL,
  `Current_C` float DEFAULT NULL,
  `Accumulated_Real_Energy_Net_FP` float DEFAULT NULL,
  `Real_Energy_Import_FP` float DEFAULT NULL,
  `Real_Energy_Export_FP` float DEFAULT NULL,
  `Reactive_Energy_Q1_FP` float DEFAULT NULL,
  `Reactive_Energy_Q2_FP` float DEFAULT NULL,
  `Reactive_Energy_Q3_FP` float DEFAULT NULL,
  `Reactive_Energy_Q4_FP` float DEFAULT NULL,
  `Apparent_Energy_Net_FP` float DEFAULT NULL,
  `Apparent_Energy_Import_FP` float DEFAULT NULL,
  `Apparent_Energy_Export_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power_FP` float DEFAULT NULL,
  `Total_Power_Factor_FP` float DEFAULT NULL,
  `Voltage_LL_Avg_FP` float DEFAULT NULL,
  `Voltage_LN_Avg_FP` float DEFAULT NULL,
  `Current_Avg_FP` float DEFAULT NULL,
  `Frequency_FP` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Pulse_Counter_1_FP` float DEFAULT NULL,
  `Pulse_Counter_2_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export_FP` float DEFAULT NULL,
  `Real_Power_A_FP` float DEFAULT NULL,
  `Real_Power_B_FP` float DEFAULT NULL,
  `Real_Power_C_FP` float DEFAULT NULL,
  `Reactive_Power_A_FP` float DEFAULT NULL,
  `Reactive_Power_B_FP` float DEFAULT NULL,
  `Reactive_Power_C_FP` float DEFAULT NULL,
  `Apparent_Power_A_FP` float DEFAULT NULL,
  `Apparent_Power_B_FP` float DEFAULT NULL,
  `Apparent_Power_C_FP` float DEFAULT NULL,
  `Power_Factor_A_FP` float DEFAULT NULL,
  `Power_Factor_B_FP` float DEFAULT NULL,
  `Power_Factor_C_FP` float DEFAULT NULL,
  `Voltage_AB_FP` float DEFAULT NULL,
  `Voltage_BC_FP` float DEFAULT NULL,
  `Voltage_AC_FP` float DEFAULT NULL,
  `Voltage_AN_FP` float DEFAULT NULL,
  `Voltage_BN_FP` float DEFAULT NULL,
  `Voltage_CN_FP` float DEFAULT NULL,
  `Current_A_FP` float DEFAULT NULL,
  `Current_B_FP` float DEFAULT NULL,
  `Current_C_FP` float DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `row_id` (`row_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER
pcc_readings_1_before_insert_trigger
BEFORE INSERT ON pcc_readings_1
FOR EACH ROW
BEGIN
IF(
SELECT ( r1.Accumulated_Real_Energy_Net_FP <> NEW.Accumulated_Real_Energy_Net_FP )
     + ( r1.Real_Energy_Import_FP <> NEW.Real_Energy_Import_FP )
     + ( r1.Real_Energy_Export_FP <> NEW.Real_Energy_Export_FP )
     + ( r1.Reactive_Energy_Q1_FP <> NEW.Reactive_Energy_Q1_FP )
     + ( r1.Reactive_Energy_Q2_FP <> NEW.Reactive_Energy_Q2_FP )
     + ( r1.Reactive_Energy_Q3_FP <> NEW.Reactive_Energy_Q3_FP )
     + ( r1.Reactive_Energy_Q4_FP <> NEW.Reactive_Energy_Q4_FP )
     + ( r1.Apparent_Energy_Net_FP <> NEW.Apparent_Energy_Net_FP )
     + ( r1.Apparent_Energy_Import_FP <> NEW.Apparent_Energy_Import_FP )
     + ( r1.Apparent_Energy_Export_FP <> NEW.Apparent_Energy_Export_FP )
     + ( r1.Total_Net_Instantaneous_Real_Power_FP <> NEW.Total_Net_Instantaneous_Real_Power_FP )
     + ( r1.Total_Net_Instantaneous_Reactive_Power_FP <> NEW.Total_Net_Instantaneous_Reactive_Power_FP )
     + ( r1.Total_Net_Instantaneous_Apparent_Power_FP <> NEW.Total_Net_Instantaneous_Apparent_Power_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Voltage_LL_Avg_FP <> NEW.Voltage_LL_Avg_FP )
     + ( r1.Voltage_LN_Avg_FP <> NEW.Voltage_LN_Avg_FP )
     + ( r1.Current_Avg_FP <> NEW.Current_Avg_FP )
     + ( r1.Frequency_FP <> NEW.Frequency_FP )
     + ( r1.Accumulated_Real_Energy_A_Import_FP <> NEW.Accumulated_Real_Energy_A_Import_FP )
     + ( r1.Accumulated_Real_Energy_B_Import_FP <> NEW.Accumulated_Real_Energy_B_Import_FP )
     + ( r1.Accumulated_Real_Energy_C_Import_FP <> NEW.Accumulated_Real_Energy_C_Import_FP )
     + ( r1.Accumulated_Real_Energy_A_Export_FP <> NEW.Accumulated_Real_Energy_A_Export_FP )
     + ( r1.Accumulated_Real_Energy_B_Export_FP <> NEW.Accumulated_Real_Energy_B_Export_FP )
     + ( r1.Accumulated_Real_Energy_C_Export_FP <> NEW.Accumulated_Real_Energy_C_Export_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_A_FP <> NEW.Accumulated_Q1_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_B_FP <> NEW.Accumulated_Q1_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_C_FP <> NEW.Accumulated_Q1_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_A_FP <> NEW.Accumulated_Q2_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_B_FP <> NEW.Accumulated_Q2_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_C_FP <> NEW.Accumulated_Q2_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_A_FP <> NEW.Accumulated_Q3_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_B_FP <> NEW.Accumulated_Q3_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_C_FP <> NEW.Accumulated_Q3_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_A_FP <> NEW.Accumulated_Q4_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_B_FP <> NEW.Accumulated_Q4_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_C_FP <> NEW.Accumulated_Q4_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Import_FP <> NEW.Accumulated_Apparent_Energy_A_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Import_FP <> NEW.Accumulated_Apparent_Energy_B_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Import_FP <> NEW.Accumulated_Apparent_Energy_C_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Export_FP <> NEW.Accumulated_Apparent_Energy_A_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Export_FP <> NEW.Accumulated_Apparent_Energy_B_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Export_FP <> NEW.Accumulated_Apparent_Energy_C_Export_FP )
     + ( r1.Real_Power_A_FP <> NEW.Real_Power_A_FP )
     + ( r1.Real_Power_B_FP <> NEW.Real_Power_B_FP )
     + ( r1.Real_Power_C_FP <> NEW.Real_Power_C_FP )
     + ( r1.Reactive_Power_A_FP <> NEW.Reactive_Power_A_FP )
     + ( r1.Reactive_Power_B_FP <> NEW.Reactive_Power_B_FP )
     + ( r1.Reactive_Power_C_FP <> NEW.Reactive_Power_C_FP )
     + ( r1.Apparent_Power_A_FP <> NEW.Apparent_Power_A_FP )
     + ( r1.Apparent_Power_B_FP <> NEW.Apparent_Power_B_FP )
     + ( r1.Apparent_Power_C_FP <> NEW.Apparent_Power_C_FP )
     + ( r1.Power_Factor_A_FP <> NEW.Power_Factor_A_FP )
     + ( r1.Power_Factor_B_FP <> NEW.Power_Factor_B_FP )
     + ( r1.Power_Factor_C_FP <> NEW.Power_Factor_C_FP )
     + ( r1.Voltage_AB_FP <> NEW.Voltage_AB_FP )
     + ( r1.Voltage_BC_FP <> NEW.Voltage_BC_FP )
     + ( r1.Voltage_AC_FP <> NEW.Voltage_AC_FP )
     + ( r1.Voltage_AN_FP <> NEW.Voltage_AN_FP )
     + ( r1.Voltage_BN_FP <> NEW.Voltage_BN_FP )
     + ( r1.Voltage_CN_FP <> NEW.Voltage_CN_FP )
     + ( r1.Current_A_FP <> NEW.Current_A_FP )
     + ( r1.Current_B_FP <> NEW.Current_B_FP )
     + ( r1.Current_C_FP <> NEW.Current_C_FP )
FROM
  pcc_readings_1 r1
ORDER BY entry_id DESC LIMIT 1
) = 0
THEN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Data appears to be duplicate';
END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER pcc_readings_1_after_insert_trigger AFTER INSERT ON pcc_readings_1
FOR EACH ROW
BEGIN
DECLARE Total_Net_Instantaneous_Real_Power_FP_2 FLOAT;
DECLARE Real_Power_A_FP_2 FLOAT;
DECLARE Real_Power_B_FP_2 FLOAT;
DECLARE Real_Power_C_FP_2 FLOAT;
DECLARE Accumulated_Real_Energy_A_Import_FP_2 FLOAT;
DECLARE Accumulated_Real_Energy_B_Import_FP_2 FLOAT;
DECLARE Accumulated_Real_Energy_C_Import_FP_2 FLOAT;
SET Total_Net_Instantaneous_Real_Power_FP_2 = (SELECT Total_Net_Instantaneous_Real_Power_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_A_FP_2 = (SELECT Real_Power_A_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_B_FP_2 = (SELECT Real_Power_B_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_C_FP_2 = (SELECT Real_Power_C_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_A_Import_FP_2 = (SELECT Accumulated_Real_Energy_A_Import_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_B_Import_FP_2 = (SELECT Accumulated_Real_Energy_B_Import_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_C_Import_FP_2 = (SELECT Accumulated_Real_Energy_C_Import_FP FROM pcc_readings_2 ORDER BY entry_id DESC LIMIT 1);
UPDATE nxo_status SET Export_PCC_1_A = NEW.Real_Power_A_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_1_B = NEW.Real_Power_B_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_1_C = NEW.Real_Power_C_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_1_Delta = NEW.Total_Net_Instantaneous_Real_Power_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_A = (NEW.Real_Power_A_FP + COALESCE(Real_Power_A_FP_2, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_B = (NEW.Real_Power_B_FP + COALESCE(Real_Power_B_FP_2, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_C = (NEW.Real_Power_C_FP + COALESCE(Real_Power_C_FP_2, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_Delta = (NEW.Total_Net_Instantaneous_Real_Power_FP + COALESCE(Total_Net_Instantaneous_Real_Power_FP_2, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET timestamp_pcc_1 = CURRENT_TIMESTAMP WHERE Profile = 'Default';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_A_FP + COALESCE(Real_Power_A_FP_2, 0)) WHERE feed = 'A';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_B_FP + COALESCE(Real_Power_B_FP_2, 0)) WHERE feed = 'B';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_C_FP + COALESCE(Real_Power_C_FP_2, 0)) WHERE feed = 'C';
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_A_Import_FP = (NEW.Accumulated_Real_Energy_A_Import_FP + COALESCE(Accumulated_Real_Energy_A_Import_FP_2, 0));
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_B_Import_FP = (NEW.Accumulated_Real_Energy_B_Import_FP + COALESCE(Accumulated_Real_Energy_B_Import_FP_2, 0));
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_C_Import_FP = (NEW.Accumulated_Real_Energy_C_Import_FP + COALESCE(Accumulated_Real_Energy_C_Import_FP_2, 0));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `pcc_readings_2`
--

DROP TABLE IF EXISTS `pcc_readings_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcc_readings_2` (
  `Accumulated_Real_Energy_Net` float DEFAULT NULL,
  `Accumulated_Real_Energy_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_Export` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_1` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_2` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_3` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_4` float DEFAULT NULL,
  `Apparent_Energy_Net` float DEFAULT NULL,
  `Apparent_Energy_Import` float DEFAULT NULL,
  `Apparent_Energy_Export` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power` float DEFAULT NULL,
  `Total_Net_Power_Factor` float DEFAULT NULL,
  `Voltage_LL_Avg` float DEFAULT NULL,
  `Voltage_LN_Avg` float DEFAULT NULL,
  `Current_Avg` float DEFAULT NULL,
  `Frequency` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export` float DEFAULT NULL,
  `Pulse_Counter_1` float DEFAULT NULL,
  `Pulse_Counter_2` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q4_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export` float DEFAULT NULL,
  `Real_Power_A` float DEFAULT NULL,
  `Real_Power_B` float DEFAULT NULL,
  `Real_Power_C` float DEFAULT NULL,
  `Reactive_Power_A` float DEFAULT NULL,
  `Reactive_Power_B` float DEFAULT NULL,
  `Reactive_Power_C` float DEFAULT NULL,
  `Apparent_Power_A` float DEFAULT NULL,
  `Apparent_Power_B` float DEFAULT NULL,
  `Apparent_Power_C` float DEFAULT NULL,
  `Power_Factor_A` float DEFAULT NULL,
  `Power_Factor_B` float DEFAULT NULL,
  `Power_Factor_C` float DEFAULT NULL,
  `Voltage_AB` float DEFAULT NULL,
  `Voltage_BC` float DEFAULT NULL,
  `Voltage_AC` float DEFAULT NULL,
  `Voltage_AN` float DEFAULT NULL,
  `Voltage_BN` float DEFAULT NULL,
  `Voltage_CN` float DEFAULT NULL,
  `Current_A` float DEFAULT NULL,
  `Current_B` float DEFAULT NULL,
  `Current_C` float DEFAULT NULL,
  `Accumulated_Real_Energy_Net_FP` float DEFAULT NULL,
  `Real_Energy_Import_FP` float DEFAULT NULL,
  `Real_Energy_Export_FP` float DEFAULT NULL,
  `Reactive_Energy_Q1_FP` float DEFAULT NULL,
  `Reactive_Energy_Q2_FP` float DEFAULT NULL,
  `Reactive_Energy_Q3_FP` float DEFAULT NULL,
  `Reactive_Energy_Q4_FP` float DEFAULT NULL,
  `Apparent_Energy_Net_FP` float DEFAULT NULL,
  `Apparent_Energy_Import_FP` float DEFAULT NULL,
  `Apparent_Energy_Export_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power_FP` float DEFAULT NULL,
  `Total_Power_Factor_FP` float DEFAULT NULL,
  `Voltage_LL_Avg_FP` float DEFAULT NULL,
  `Voltage_LN_Avg_FP` float DEFAULT NULL,
  `Current_Avg_FP` float DEFAULT NULL,
  `Frequency_FP` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Pulse_Counter_1_FP` float DEFAULT NULL,
  `Pulse_Counter_2_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export_FP` float DEFAULT NULL,
  `Real_Power_A_FP` float DEFAULT NULL,
  `Real_Power_B_FP` float DEFAULT NULL,
  `Real_Power_C_FP` float DEFAULT NULL,
  `Reactive_Power_A_FP` float DEFAULT NULL,
  `Reactive_Power_B_FP` float DEFAULT NULL,
  `Reactive_Power_C_FP` float DEFAULT NULL,
  `Apparent_Power_A_FP` float DEFAULT NULL,
  `Apparent_Power_B_FP` float DEFAULT NULL,
  `Apparent_Power_C_FP` float DEFAULT NULL,
  `Power_Factor_A_FP` float DEFAULT NULL,
  `Power_Factor_B_FP` float DEFAULT NULL,
  `Power_Factor_C_FP` float DEFAULT NULL,
  `Voltage_AB_FP` float DEFAULT NULL,
  `Voltage_BC_FP` float DEFAULT NULL,
  `Voltage_AC_FP` float DEFAULT NULL,
  `Voltage_AN_FP` float DEFAULT NULL,
  `Voltage_BN_FP` float DEFAULT NULL,
  `Voltage_CN_FP` float DEFAULT NULL,
  `Current_A_FP` float DEFAULT NULL,
  `Current_B_FP` float DEFAULT NULL,
  `Current_C_FP` float DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `row_id` (`row_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER
pcc_readings_2_before_insert_trigger
BEFORE INSERT ON pcc_readings_2
FOR EACH ROW
BEGIN
IF(
SELECT ( r1.Accumulated_Real_Energy_Net_FP <> NEW.Accumulated_Real_Energy_Net_FP )
     + ( r1.Real_Energy_Import_FP <> NEW.Real_Energy_Import_FP )
     + ( r1.Real_Energy_Export_FP <> NEW.Real_Energy_Export_FP )
     + ( r1.Reactive_Energy_Q1_FP <> NEW.Reactive_Energy_Q1_FP )
     + ( r1.Reactive_Energy_Q2_FP <> NEW.Reactive_Energy_Q2_FP )
     + ( r1.Reactive_Energy_Q3_FP <> NEW.Reactive_Energy_Q3_FP )
     + ( r1.Reactive_Energy_Q4_FP <> NEW.Reactive_Energy_Q4_FP )
     + ( r1.Apparent_Energy_Net_FP <> NEW.Apparent_Energy_Net_FP )
     + ( r1.Apparent_Energy_Import_FP <> NEW.Apparent_Energy_Import_FP )
     + ( r1.Apparent_Energy_Export_FP <> NEW.Apparent_Energy_Export_FP )
     + ( r1.Total_Net_Instantaneous_Real_Power_FP <> NEW.Total_Net_Instantaneous_Real_Power_FP )
     + ( r1.Total_Net_Instantaneous_Reactive_Power_FP <> NEW.Total_Net_Instantaneous_Reactive_Power_FP )
     + ( r1.Total_Net_Instantaneous_Apparent_Power_FP <> NEW.Total_Net_Instantaneous_Apparent_Power_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Voltage_LL_Avg_FP <> NEW.Voltage_LL_Avg_FP )
     + ( r1.Voltage_LN_Avg_FP <> NEW.Voltage_LN_Avg_FP )
     + ( r1.Current_Avg_FP <> NEW.Current_Avg_FP )
     + ( r1.Frequency_FP <> NEW.Frequency_FP )
     + ( r1.Accumulated_Real_Energy_A_Import_FP <> NEW.Accumulated_Real_Energy_A_Import_FP )
     + ( r1.Accumulated_Real_Energy_B_Import_FP <> NEW.Accumulated_Real_Energy_B_Import_FP )
     + ( r1.Accumulated_Real_Energy_C_Import_FP <> NEW.Accumulated_Real_Energy_C_Import_FP )
     + ( r1.Accumulated_Real_Energy_A_Export_FP <> NEW.Accumulated_Real_Energy_A_Export_FP )
     + ( r1.Accumulated_Real_Energy_B_Export_FP <> NEW.Accumulated_Real_Energy_B_Export_FP )
     + ( r1.Accumulated_Real_Energy_C_Export_FP <> NEW.Accumulated_Real_Energy_C_Export_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_A_FP <> NEW.Accumulated_Q1_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_B_FP <> NEW.Accumulated_Q1_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_C_FP <> NEW.Accumulated_Q1_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_A_FP <> NEW.Accumulated_Q2_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_B_FP <> NEW.Accumulated_Q2_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_C_FP <> NEW.Accumulated_Q2_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_A_FP <> NEW.Accumulated_Q3_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_B_FP <> NEW.Accumulated_Q3_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_C_FP <> NEW.Accumulated_Q3_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_A_FP <> NEW.Accumulated_Q4_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_B_FP <> NEW.Accumulated_Q4_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_C_FP <> NEW.Accumulated_Q4_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Import_FP <> NEW.Accumulated_Apparent_Energy_A_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Import_FP <> NEW.Accumulated_Apparent_Energy_B_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Import_FP <> NEW.Accumulated_Apparent_Energy_C_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Export_FP <> NEW.Accumulated_Apparent_Energy_A_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Export_FP <> NEW.Accumulated_Apparent_Energy_B_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Export_FP <> NEW.Accumulated_Apparent_Energy_C_Export_FP )
     + ( r1.Real_Power_A_FP <> NEW.Real_Power_A_FP )
     + ( r1.Real_Power_B_FP <> NEW.Real_Power_B_FP )
     + ( r1.Real_Power_C_FP <> NEW.Real_Power_C_FP )
     + ( r1.Reactive_Power_A_FP <> NEW.Reactive_Power_A_FP )
     + ( r1.Reactive_Power_B_FP <> NEW.Reactive_Power_B_FP )
     + ( r1.Reactive_Power_C_FP <> NEW.Reactive_Power_C_FP )
     + ( r1.Apparent_Power_A_FP <> NEW.Apparent_Power_A_FP )
     + ( r1.Apparent_Power_B_FP <> NEW.Apparent_Power_B_FP )
     + ( r1.Apparent_Power_C_FP <> NEW.Apparent_Power_C_FP )
     + ( r1.Power_Factor_A_FP <> NEW.Power_Factor_A_FP )
     + ( r1.Power_Factor_B_FP <> NEW.Power_Factor_B_FP )
     + ( r1.Power_Factor_C_FP <> NEW.Power_Factor_C_FP )
     + ( r1.Voltage_AB_FP <> NEW.Voltage_AB_FP )
     + ( r1.Voltage_BC_FP <> NEW.Voltage_BC_FP )
     + ( r1.Voltage_AC_FP <> NEW.Voltage_AC_FP )
     + ( r1.Voltage_AN_FP <> NEW.Voltage_AN_FP )
     + ( r1.Voltage_BN_FP <> NEW.Voltage_BN_FP )
     + ( r1.Voltage_CN_FP <> NEW.Voltage_CN_FP )
     + ( r1.Current_A_FP <> NEW.Current_A_FP )
     + ( r1.Current_B_FP <> NEW.Current_B_FP )
     + ( r1.Current_C_FP <> NEW.Current_C_FP )
FROM
  pcc_readings_2 r1
ORDER BY entry_id DESC LIMIT 1
) = 0
THEN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Data appears to be duplicate';
END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER pcc_readings_2_after_insert_trigger AFTER INSERT ON pcc_readings_2
FOR EACH ROW
BEGIN
DECLARE Total_Net_Instantaneous_Real_Power_FP_1 FLOAT;
DECLARE Real_Power_A_FP_1 FLOAT;
DECLARE Real_Power_B_FP_1 FLOAT;
DECLARE Real_Power_C_FP_1 FLOAT;
DECLARE Accumulated_Real_Energy_A_Import_FP_1 FLOAT;
DECLARE Accumulated_Real_Energy_B_Import_FP_1 FLOAT;
DECLARE Accumulated_Real_Energy_C_Import_FP_1 FLOAT;
SET Total_Net_Instantaneous_Real_Power_FP_1 = (SELECT Total_Net_Instantaneous_Real_Power_FP FROM pcc_readings_1 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_A_FP_1 = (SELECT Real_Power_A_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_B_FP_1 = (SELECT Real_Power_B_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
SET Real_Power_C_FP_1 = (SELECT Real_Power_C_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_A_Import_FP_1 = (SELECT Accumulated_Real_Energy_A_Import_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_B_Import_FP_1 = (SELECT Accumulated_Real_Energy_B_Import_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
SET Accumulated_Real_Energy_C_Import_FP_1 = (SELECT Accumulated_Real_Energy_C_Import_FP FROM pcc_readings_1 as t1 ORDER BY entry_id DESC LIMIT 1);
UPDATE nxo_status SET Export_PCC_2_A = NEW.Real_Power_A_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_2_B = NEW.Real_Power_B_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_2_C = NEW.Real_Power_C_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_2_Delta = NEW.Total_Net_Instantaneous_Real_Power_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_A = (NEW.Real_Power_A_FP + COALESCE(Real_Power_A_FP_1, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_B = (NEW.Real_Power_B_FP + COALESCE(Real_Power_B_FP_1, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_C = (NEW.Real_Power_C_FP + COALESCE(Real_Power_C_FP_1, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET Export_PCC_Combo_Delta = (NEW.Total_Net_Instantaneous_Real_Power_FP + COALESCE(Total_Net_Instantaneous_Real_Power_FP_1, 0)) WHERE Profile = 'Default';
UPDATE nxo_status SET timestamp_pcc_2 = CURRENT_TIMESTAMP WHERE Profile = 'Default';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_A_FP + COALESCE(Real_Power_A_FP_1, 0)) WHERE feed = 'A';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_B_FP + COALESCE(Real_Power_B_FP_1, 0)) WHERE feed = 'B';
UPDATE gatewayMeter SET pccNetPower = (NEW.Real_Power_C_FP + COALESCE(Real_Power_C_FP_1, 0)) WHERE feed = 'C';
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_A_Import_FP = (NEW.Accumulated_Real_Energy_A_Import_FP + COALESCE(Accumulated_Real_Energy_A_Import_FP_1, 0));
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_B_Import_FP = (NEW.Accumulated_Real_Energy_B_Import_FP + COALESCE(Accumulated_Real_Energy_B_Import_FP_1, 0));
UPDATE pcc_readings_combo SET Accumulated_Real_Energy_C_Import_FP = (NEW.Accumulated_Real_Energy_C_Import_FP + COALESCE(Accumulated_Real_Energy_C_Import_FP_1, 0));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `pcc_readings_combo`
--

DROP TABLE IF EXISTS `pcc_readings_combo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcc_readings_combo` (
  `Accumulated_Real_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import_FP` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `power_factor_control`
--

DROP TABLE IF EXISTS `power_factor_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `power_factor_control` (
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `wait_b4_start` int(11) NOT NULL DEFAULT '60',
  `pf_command_freq` int(11) NOT NULL DEFAULT '20',
  `num_readings_for_pf_upd` int(11) NOT NULL DEFAULT '10',
  `pf_regional_default` float NOT NULL DEFAULT '0.95',
  `pf_regional_default_leading` tinyint(1) NOT NULL DEFAULT '0',
  `pf_target_A` float NOT NULL DEFAULT '0.95',
  `pf_target_A_leading` tinyint(1) DEFAULT NULL,
  `pf_target_B` float NOT NULL DEFAULT '0.95',
  `pf_target_B_leading` tinyint(1) DEFAULT NULL,
  `pf_target_C` float NOT NULL DEFAULT '0.95',
  `pf_target_C_leading` tinyint(1) DEFAULT NULL,
  `pf_enable_A` tinyint(1) NOT NULL DEFAULT '1',
  `pf_enable_B` tinyint(1) NOT NULL DEFAULT '1',
  `pf_enable_C` tinyint(1) NOT NULL DEFAULT '1',
  `pf_log_A` tinyint(1) NOT NULL DEFAULT '0',
  `pf_log_B` tinyint(1) NOT NULL DEFAULT '0',
  `pf_log_C` tinyint(1) NOT NULL DEFAULT '0',
  `pf_data_log_freq` int(11) NOT NULL DEFAULT '5',
  `pf_data_log_A` tinyint(1) NOT NULL DEFAULT '0',
  `pf_data_log_B` tinyint(1) NOT NULL DEFAULT '0',
  `pf_data_log_C` tinyint(1) NOT NULL DEFAULT '0',
  `lia` varchar(5) NOT NULL DEFAULT 'NO',
  `interval_freq` int(11) NOT NULL DEFAULT '5',
  `interval_samples` int(11) NOT NULL DEFAULT '5',
  `interval_table_max_size` int(11) NOT NULL DEFAULT '50',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  `pid` varchar(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `protection_alert`
--

DROP TABLE IF EXISTS `protection_alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protection_alert` (
  `mac_addr` varchar(18) DEFAULT NULL,
  `ip_addr` varchar(20) DEFAULT NULL,
  `device_name` varchar(32) DEFAULT 'PET-7202',
  `alert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `input_name` varchar(32) NOT NULL DEFAULT 'unspecified',
  PRIMARY KEY (`input_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sis_control`
--

DROP TABLE IF EXISTS `sis_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sis_control` (
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `started` tinyint(1) NOT NULL DEFAULT '0',
  `suspend` varchar(5) NOT NULL DEFAULT 'YES',
  `reset_data` varchar(5) NOT NULL DEFAULT 'NO',
  `wait_b4_start` int(11) NOT NULL DEFAULT '180',
  `q_period` int(11) NOT NULL DEFAULT '3600',
  `rank` int(11) NOT NULL DEFAULT '0',
  `queries` int(11) NOT NULL DEFAULT '0',
  `detail_log` tinyint(1) NOT NULL DEFAULT '0',
  `info_log` tinyint(1) NOT NULL DEFAULT '0',
  `q_total` int(11) NOT NULL DEFAULT '0',
  `q_resp` int(11) NOT NULL DEFAULT '0',
  `q_no_resp` int(11) NOT NULL DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  `pid` varchar(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `solar_readings_1`
--

DROP TABLE IF EXISTS `solar_readings_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solar_readings_1` (
  `Accumulated_Real_Energy_Net` float DEFAULT NULL,
  `Accumulated_Real_Energy_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_Export` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_1` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_2` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_3` float DEFAULT NULL,
  `Reactive_Energy_Quadrant_4` float DEFAULT NULL,
  `Apparent_Energy_Net` float DEFAULT NULL,
  `Apparent_Energy_Import` float DEFAULT NULL,
  `Apparent_Energy_Export` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power` float DEFAULT NULL,
  `Total_Net_Power_Factor` float DEFAULT NULL,
  `Voltage_LL_Avg` float DEFAULT NULL,
  `Voltage_LN_Avg` float DEFAULT NULL,
  `Current_Avg` float DEFAULT NULL,
  `Frequency` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export` float DEFAULT NULL,
  `Pulse_Counter_1` float DEFAULT NULL,
  `Pulse_Counter_2` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q1_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q2_Import` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q3_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_A_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_B_Q4_Export` float DEFAULT NULL,
  `Accumulated_Reactive_Energy_C_Q4_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export` float DEFAULT NULL,
  `Real_Power_A` float DEFAULT NULL,
  `Real_Power_B` float DEFAULT NULL,
  `Real_Power_C` float DEFAULT NULL,
  `Reactive_Power_A` float DEFAULT NULL,
  `Reactive_Power_B` float DEFAULT NULL,
  `Reactive_Power_C` float DEFAULT NULL,
  `Apparent_Power_A` float DEFAULT NULL,
  `Apparent_Power_B` float DEFAULT NULL,
  `Apparent_Power_C` float DEFAULT NULL,
  `Power_Factor_A` float DEFAULT NULL,
  `Power_Factor_B` float DEFAULT NULL,
  `Power_Factor_C` float DEFAULT NULL,
  `Voltage_AB` float DEFAULT NULL,
  `Voltage_BC` float DEFAULT NULL,
  `Voltage_AC` float DEFAULT NULL,
  `Voltage_AN` float DEFAULT NULL,
  `Voltage_BN` float DEFAULT NULL,
  `Voltage_CN` float DEFAULT NULL,
  `Current_A` float DEFAULT NULL,
  `Current_B` float DEFAULT NULL,
  `Current_C` float DEFAULT NULL,
  `Accumulated_Real_Energy_Net_FP` float DEFAULT NULL,
  `Real_Energy_Import_FP` float DEFAULT NULL,
  `Real_Energy_Export_FP` float DEFAULT NULL,
  `Reactive_Energy_Q1_FP` float DEFAULT NULL,
  `Reactive_Energy_Q2_FP` float DEFAULT NULL,
  `Reactive_Energy_Q3_FP` float DEFAULT NULL,
  `Reactive_Energy_Q4_FP` float DEFAULT NULL,
  `Apparent_Energy_Net_FP` float DEFAULT NULL,
  `Apparent_Energy_Import_FP` float DEFAULT NULL,
  `Apparent_Energy_Export_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Real_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Reactive_Power_FP` float DEFAULT NULL,
  `Total_Net_Instantaneous_Apparent_Power_FP` float DEFAULT NULL,
  `Total_Power_Factor_FP` float DEFAULT NULL,
  `Voltage_LL_Avg_FP` float DEFAULT NULL,
  `Voltage_LN_Avg_FP` float DEFAULT NULL,
  `Current_Avg_FP` float DEFAULT NULL,
  `Frequency_FP` float DEFAULT NULL,
  `Total_Real_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Present_Demand_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Import_FP` float DEFAULT NULL,
  `Total_Real_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Reactive_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Total_Apparent_Power_Max_Demand_Export_FP` float DEFAULT NULL,
  `Pulse_Counter_1_FP` float DEFAULT NULL,
  `Pulse_Counter_2_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Real_Energy_C_Export_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q1_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q2_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q3_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_A_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_B_FP` float DEFAULT NULL,
  `Accumulated_Q4_Reactive_Energy_C_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Import_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_A_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_B_Export_FP` float DEFAULT NULL,
  `Accumulated_Apparent_Energy_C_Export_FP` float DEFAULT NULL,
  `Real_Power_A_FP` float DEFAULT NULL,
  `Real_Power_B_FP` float DEFAULT NULL,
  `Real_Power_C_FP` float DEFAULT NULL,
  `Reactive_Power_A_FP` float DEFAULT NULL,
  `Reactive_Power_B_FP` float DEFAULT NULL,
  `Reactive_Power_C_FP` float DEFAULT NULL,
  `Apparent_Power_A_FP` float DEFAULT NULL,
  `Apparent_Power_B_FP` float DEFAULT NULL,
  `Apparent_Power_C_FP` float DEFAULT NULL,
  `Power_Factor_A_FP` float DEFAULT NULL,
  `Power_Factor_B_FP` float DEFAULT NULL,
  `Power_Factor_C_FP` float DEFAULT NULL,
  `Voltage_AB_FP` float DEFAULT NULL,
  `Voltage_BC_FP` float DEFAULT NULL,
  `Voltage_AC_FP` float DEFAULT NULL,
  `Voltage_AN_FP` float DEFAULT NULL,
  `Voltage_BN_FP` float DEFAULT NULL,
  `Voltage_CN_FP` float DEFAULT NULL,
  `Current_A_FP` float DEFAULT NULL,
  `Current_B_FP` float DEFAULT NULL,
  `Current_C_FP` float DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `row_id` (`row_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER
solar_readings_1_before_insert_trigger
BEFORE INSERT ON solar_readings_1
FOR EACH ROW
BEGIN
IF(
SELECT ( r1.Accumulated_Real_Energy_Net_FP <> NEW.Accumulated_Real_Energy_Net_FP )
     + ( r1.Real_Energy_Import_FP <> NEW.Real_Energy_Import_FP )
     + ( r1.Real_Energy_Export_FP <> NEW.Real_Energy_Export_FP )
     + ( r1.Reactive_Energy_Q1_FP <> NEW.Reactive_Energy_Q1_FP )
     + ( r1.Reactive_Energy_Q2_FP <> NEW.Reactive_Energy_Q2_FP )
     + ( r1.Reactive_Energy_Q3_FP <> NEW.Reactive_Energy_Q3_FP )
     + ( r1.Reactive_Energy_Q4_FP <> NEW.Reactive_Energy_Q4_FP )
     + ( r1.Apparent_Energy_Net_FP <> NEW.Apparent_Energy_Net_FP )
     + ( r1.Apparent_Energy_Import_FP <> NEW.Apparent_Energy_Import_FP )
     + ( r1.Apparent_Energy_Export_FP <> NEW.Apparent_Energy_Export_FP )
     + ( r1.Total_Net_Instantaneous_Real_Power_FP <> NEW.Total_Net_Instantaneous_Real_Power_FP )
     + ( r1.Total_Net_Instantaneous_Reactive_Power_FP <> NEW.Total_Net_Instantaneous_Reactive_Power_FP )
     + ( r1.Total_Net_Instantaneous_Apparent_Power_FP <> NEW.Total_Net_Instantaneous_Apparent_Power_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Total_Power_Factor_FP <> NEW.Total_Power_Factor_FP )
     + ( r1.Voltage_LL_Avg_FP <> NEW.Voltage_LL_Avg_FP )
     + ( r1.Voltage_LN_Avg_FP <> NEW.Voltage_LN_Avg_FP )
     + ( r1.Current_Avg_FP <> NEW.Current_Avg_FP )
     + ( r1.Frequency_FP <> NEW.Frequency_FP )
     + ( r1.Accumulated_Real_Energy_A_Import_FP <> NEW.Accumulated_Real_Energy_A_Import_FP )
     + ( r1.Accumulated_Real_Energy_B_Import_FP <> NEW.Accumulated_Real_Energy_B_Import_FP )
     + ( r1.Accumulated_Real_Energy_C_Import_FP <> NEW.Accumulated_Real_Energy_C_Import_FP )
     + ( r1.Accumulated_Real_Energy_A_Export_FP <> NEW.Accumulated_Real_Energy_A_Export_FP )
     + ( r1.Accumulated_Real_Energy_B_Export_FP <> NEW.Accumulated_Real_Energy_B_Export_FP )
     + ( r1.Accumulated_Real_Energy_C_Export_FP <> NEW.Accumulated_Real_Energy_C_Export_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_A_FP <> NEW.Accumulated_Q1_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_B_FP <> NEW.Accumulated_Q1_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q1_Reactive_Energy_C_FP <> NEW.Accumulated_Q1_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_A_FP <> NEW.Accumulated_Q2_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_B_FP <> NEW.Accumulated_Q2_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q2_Reactive_Energy_C_FP <> NEW.Accumulated_Q2_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_A_FP <> NEW.Accumulated_Q3_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_B_FP <> NEW.Accumulated_Q3_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q3_Reactive_Energy_C_FP <> NEW.Accumulated_Q3_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_A_FP <> NEW.Accumulated_Q4_Reactive_Energy_A_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_B_FP <> NEW.Accumulated_Q4_Reactive_Energy_B_FP )
     + ( r1.Accumulated_Q4_Reactive_Energy_C_FP <> NEW.Accumulated_Q4_Reactive_Energy_C_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Import_FP <> NEW.Accumulated_Apparent_Energy_A_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Import_FP <> NEW.Accumulated_Apparent_Energy_B_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Import_FP <> NEW.Accumulated_Apparent_Energy_C_Import_FP )
     + ( r1.Accumulated_Apparent_Energy_A_Export_FP <> NEW.Accumulated_Apparent_Energy_A_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_B_Export_FP <> NEW.Accumulated_Apparent_Energy_B_Export_FP )
     + ( r1.Accumulated_Apparent_Energy_C_Export_FP <> NEW.Accumulated_Apparent_Energy_C_Export_FP )
     + ( r1.Real_Power_A_FP <> NEW.Real_Power_A_FP )
     + ( r1.Real_Power_B_FP <> NEW.Real_Power_B_FP )
     + ( r1.Real_Power_C_FP <> NEW.Real_Power_C_FP )
     + ( r1.Reactive_Power_A_FP <> NEW.Reactive_Power_A_FP )
     + ( r1.Reactive_Power_B_FP <> NEW.Reactive_Power_B_FP )
     + ( r1.Reactive_Power_C_FP <> NEW.Reactive_Power_C_FP )
     + ( r1.Apparent_Power_A_FP <> NEW.Apparent_Power_A_FP )
     + ( r1.Apparent_Power_B_FP <> NEW.Apparent_Power_B_FP )
     + ( r1.Apparent_Power_C_FP <> NEW.Apparent_Power_C_FP )
     + ( r1.Power_Factor_A_FP <> NEW.Power_Factor_A_FP )
     + ( r1.Power_Factor_B_FP <> NEW.Power_Factor_B_FP )
     + ( r1.Power_Factor_C_FP <> NEW.Power_Factor_C_FP )
     + ( r1.Voltage_AB_FP <> NEW.Voltage_AB_FP )
     + ( r1.Voltage_BC_FP <> NEW.Voltage_BC_FP )
     + ( r1.Voltage_AC_FP <> NEW.Voltage_AC_FP )
     + ( r1.Voltage_AN_FP <> NEW.Voltage_AN_FP )
     + ( r1.Voltage_BN_FP <> NEW.Voltage_BN_FP )
     + ( r1.Voltage_CN_FP <> NEW.Voltage_CN_FP )
     + ( r1.Current_A_FP <> NEW.Current_A_FP )
     + ( r1.Current_B_FP <> NEW.Current_B_FP )
     + ( r1.Current_C_FP <> NEW.Current_C_FP )
FROM
  solar_readings_1 r1
ORDER BY entry_id DESC LIMIT 1
) = 0
THEN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Data appears to be duplicate';
END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER solar_readings_1_after_insert_trigger AFTER INSERT ON solar_readings_1
FOR EACH ROW
BEGIN
UPDATE nxo_status SET Export_Solar_1_A = NEW.Real_Power_A_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_Solar_1_B = NEW.Real_Power_B_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_Solar_1_C = NEW.Real_Power_C_FP WHERE Profile = 'Default';
UPDATE nxo_status SET Export_Solar_1_Delta = NEW.Total_Net_Instantaneous_Real_Power_FP WHERE Profile = 'Default';
UPDATE nxo_status SET timestamp_solar_1 = CURRENT_TIMESTAMP WHERE Profile = 'Default';
UPDATE gatewayMeter SET solarPower = (NEW.Real_Power_A_FP) WHERE feed = 'A';
UPDATE gatewayMeter SET solarPower = (NEW.Real_Power_B_FP) WHERE feed = 'B';
UPDATE gatewayMeter SET solarPower = (NEW.Real_Power_C_FP) WHERE feed = 'C';
UPDATE gatewayMeter SET solarCurrent = (NEW.Current_A_FP) WHERE feed = 'A';
UPDATE gatewayMeter SET solarCurrent = (NEW.Current_B_FP) WHERE feed = 'B';
UPDATE gatewayMeter SET solarCurrent = (NEW.Current_C_FP) WHERE feed = 'C';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `stringmon_control`
--

DROP TABLE IF EXISTS `stringmon_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stringmon_control` (
  `pid` varchar(10) DEFAULT NULL,
  `audit_status` varchar(32) DEFAULT NULL,
  `strings_detected` int(7) DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `strings`
--

DROP TABLE IF EXISTS `strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strings` (
  `stringId` int(11) NOT NULL AUTO_INCREMENT,
  `string_name` varchar(45) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `FEED_NAME` varchar(3) DEFAULT NULL,
  `buildout` varchar(45) DEFAULT NULL,
  `string_offset` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`stringId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_salt` varchar(10) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `privileges` varchar(45) DEFAULT NULL,
  `last_attempt` int(11) DEFAULT NULL,
  `last_IP` varchar(45) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `failed_attempts` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 21:33:35
