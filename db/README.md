# Database Schema Changes

Database schema changes are managed using [simple-db-migrate](https://github.com/guilhermechapiewski/simple-db-migrate).

## Create a New Database Migration

### Windows

Open a DOS prompt and run the following command from the `db` directory: 
```
C:\code\gateway\db> .\db-migrate-create.bat add_some_new_column
```

### Mac OS

Install simple-db-migrate using pip and copy the simple-db-config file in place (only needs to be run once):
```
$ pip install simple-db-migrate
$ cp ~/gateway/provision/roles/mysql/templates/simple-db-migrate.conf.j2 ~/gateway/db/simple-db-migrate.conf
```

To create a new migration, run the following command from the `db` directory:
```
$ db-migrate -n add_some_new_column
```

## Edit New Database Migration

The steps above should generate a new migration in the db/migrations directory:
```
./db/migrations/20161006143641_add_some_new_column.migration
```

Open the new migration file in a text editor (you should see the following):
```
#-*- coding:utf-8 -*-
SQL_UP = u"""

"""

SQL_DOWN = u"""

"""
```

Fill in the up and down portion of the migration file with the SQL that modifies the schema in the up portion and undoes the schema change in the down portion. For example:
```
#-*- coding:utf-8 -*-
SQL_UP = u"""
ALTER TABLE feed_config ADD COLUMN some_new_column varchar(50) DEFAULT NULL AFTER `LOGGER_TAG`;
"""

SQL_DOWN = u"""
ALTER TABLE feed_config DROP COLUMN some_new_column;
"""
```

Another common example is creating a new table:
```
#-*- coding:utf-8 -*-
SQL_UP = u"""
CREATE TABLE energy_meter_schema (
  e_meter_config_id int(11) NOT NULL AUTO_INCREMENT,
  deviceclass varchar(45) DEFAULT NULL,
  configuration varchar(255) DEFAULT NULL,
  energy_metercol blob,
  configurationchangetime datetime DEFAULT NULL,
  configurationchecksum varchar(50) DEFAULT NULL,
  PRIMARY KEY (e_meter_config_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""

SQL_DOWN = u"""
DROP TABLE energy_meter_schema IF EXISTS;
"""
```

## Running New Migrations

### getgat

To run new migrations on your gateway, you can check in your code to your git branch and run `getgat`.
```
$ getgat <your-branch-name>
```

### Re-provisioning MySQL

You can also run new migrations by re-running provisioning with the mysql tag:
```
$ ./self-provision.sh mysql
``` 
This will simply run any new migrations and will not lose any existing data.

### Manually Running db-migrate

If you want to run migrations manually on your gateway, transfer the new migration file to:
```
/usr/share/apparent/db/migrations
```

Then run migrations from the db directory on your gateway. Use the --smart-migrate option to run up migrations on new migration files and down migrations on removed migration files:
```
$ cd /usr/share/apparent/db
$ db-migrate --smart-migrate

Starting DB migration on host/database "localhost/gateway_db" with user "root"...
- Current version is: 20161005174602
- Destination version is: 20161006140816

Starting migration up!
*** versions: ['20161006140816']

===== executing 20161006140816_test.migration (up) =====

Done.
```

## Other Useful Commands

To drop the database and recreate the schema from scratch:
```
$ db-migrate --drop
```

To downgrade schema to a previous version of the schema:
```
$ db-migrate -m 20161004143754
```

To see other possibilities:
```
$ db-migrate --help
```
