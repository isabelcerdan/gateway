DELETE FROM pcs_units;
ALTER TABLE pcs_units AUTO_INCREMENT = 1;
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (1, 1, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (2, 2, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (3, 3, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (4, 4, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (5, 5, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (6, 6, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (7, 7, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (8, 8, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (9, 9, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (10, 10, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (11, 11, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (12, 12, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (13, 13, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (14, 14, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (15, 15, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (16, 16, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (17, 17, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (18, 18, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (19, 19, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (20, 20, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (21, 21, 1);
INSERT INTO pcs_units (id, bmu_id_A, ess_unit_id) VALUES (22, 22, 1);

DELETE FROM aess_charger_chassis;
ALTER TABLE aess_charger_chassis AUTO_INCREMENT = 1;
INSERT INTO aess_charger_chassis (chassis_id, ip_addr, mac_addr, relay_ip_addr, relay_num) VALUES (1, '10.252.2.34', '88:c2:55:78:28:6c', '10.252.2.76', 0);
INSERT INTO aess_charger_chassis (chassis_id, ip_addr, mac_addr, relay_ip_addr, relay_num) VALUES (2, '10.252.2.32', '88:c2:55:77:f5:12', '10.252.2.76', 1);
INSERT INTO aess_charger_chassis (chassis_id, ip_addr, mac_addr, relay_ip_addr, relay_num) VALUES (3, '10.252.2.35', '68:9e:19:61:61:8b', '10.252.2.76', 2);

# charger ID (auto) | charger module number | device ID | model number | chassis ID | PCS unit ID (BMU ID)
DELETE FROM aess_charger_module;
ALTER TABLE aess_charger_module AUTO_INCREMENT = 1;
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (1, 1, 1, '73-936-0048', 1, 12);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (2, 2, 1, '73-936-0048', 1, 1);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (3, 3, 1, '73-936-0048', 1, 13);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (4, 4, 1, '73-936-0048', 1, 2);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (5, 5, 1, '73-936-0048', 1, 14);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (6, 6, 1, '73-936-0048', 1, 3);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (7, 7, 1, '73-936-0048', 1, 15);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (8, 8, 1, '73-936-0048', 1, 4);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (9, 1, 1, '73-936-0048', 2, 16);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (10, 2, 1, '73-936-0048', 2, 5);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (11, 3, 1, '73-936-0048', 2, 17);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (12, 4, 1, '73-936-0048', 2, 6);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (13, 5, 1, '73-936-0048', 2, 18);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (14, 6, 1, '73-936-0048', 2, 7);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (15, 7, 1, '73-936-0048', 2, 19);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (16, 8, 1, '73-936-0048', 2, 8);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (17, 1, 1, '73-936-0048', 3, 20);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (18, 2, 1, '73-936-0048', 3, 9);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (19, 3, 1, '73-936-0048', 3, 21);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (20, 4, 1, '73-936-0048', 3, 10);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (21, 6, 1, '73-936-0048', 3, 22);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (22, 5, 1, '73-936-0048', 3, 11);

DELETE FROM artesyn_charger_data WHERE chassis_id in (1,2,3);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (1, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (2, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (3, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (4, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (5, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (6, 1, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (1, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (2, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (3, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (4, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (5, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (6, 2, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (1, 3, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (2, 3, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (3, 3, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (4, 3, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (5, 3, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (6, 3, 56.800, 0.0);

DELETE FROM aess_serial_device_server;
ALTER TABLE aess_serial_device_server AUTO_INCREMENT = 1;
INSERT INTO aess_serial_device_server (sds_id, mac_addr, ip_addr, rack_id, ess_unit_id) VALUES (1, '00:80:d4:09:2a:ac', '10.252.2.251', 1, 1);
INSERT INTO aess_serial_device_server (sds_id, mac_addr, ip_addr, rack_id, ess_unit_id) VALUES (2, '00:80:d4:09:2a:ac', '10.252.2.252', 2, 1);

DELETE FROM aess_battery_module_unit;
ALTER TABLE aess_battery_module_unit AUTO_INCREMENT = 1;
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (1,  1,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (2,  2,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (3,  3,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (4,  4,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (5,  5,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (6,  6,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (7,  7,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (8,  8,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (9,  9,  1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (10, 10, 1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (11, 11, 1);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (12,  1,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (13,  2,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (14,  3,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (15,  4,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (16,  5,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (17,  6,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (18,  7,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (19,  8,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (20,  9,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (21, 10,  2);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (22, 11,  2);

DELETE FROM aess_bmu_data;
ALTER TABLE aess_bmu_data AUTO_INCREMENT = 1;
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (1, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (2, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (3, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (4, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (5, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (6, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (7, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (8, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (9, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (10, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (11, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (12, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (13, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (14, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (15, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (16, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (17, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (18, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (19, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (20, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (21, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (22, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);


UPDATE inverters SET pcs_unit_id = 1 WHERE serialNumber IN
    ('151819003440', '151827003810', '151834240088');
UPDATE inverters SET pcs_unit_id = 2 WHERE serialNumber IN
    ('151834240098', '151819001022', '151842240188');
UPDATE inverters SET pcs_unit_id = 3 WHERE serialNumber IN
    ('151834240079', '151819000079', '151819000527');
UPDATE inverters SET pcs_unit_id = 4 WHERE serialNumber IN
    ('151827003581', '151842240122', '151819000219');
UPDATE inverters SET pcs_unit_id = 5 WHERE serialNumber IN
    ('151834240085', '151828990092', '151827002850');
UPDATE inverters SET pcs_unit_id = 6 WHERE serialNumber IN
    ('151834240092', '151827003536', '151827003569');
UPDATE inverters SET pcs_unit_id = 7 WHERE serialNumber IN
    ('151819002532', '151827003647', '151827003568');
UPDATE inverters SET pcs_unit_id = 8 WHERE serialNumber IN
    ('151834240073', '151827002933', '151827003570');
UPDATE inverters SET pcs_unit_id = 9 WHERE serialNumber IN
    ('151834240068', '151819002565', '151819003084');
UPDATE inverters SET pcs_unit_id = 10 WHERE serialNumber IN
    ('151834240093', '151827003519', '151827002977');
UPDATE inverters SET pcs_unit_id = 11 WHERE serialNumber IN
    ('151819003011', '151834240084', '151827003548');
UPDATE inverters SET pcs_unit_id = 12 WHERE serialNumber IN
    ('151834240080', '151814000350', '151827003739');
UPDATE inverters SET pcs_unit_id = 13 WHERE serialNumber IN
    ('151834240081', '151827003775', '151814000006');
UPDATE inverters SET pcs_unit_id = 14 WHERE serialNumber IN
    ('151834240090', '151827003785', '151827003492');
UPDATE inverters SET pcs_unit_id = 15 WHERE serialNumber IN
    ('151814000696', '151827002928', '151827003550');
UPDATE inverters SET pcs_unit_id = 16 WHERE serialNumber IN
    ('151834240067', '151827002929');
UPDATE inverters SET pcs_unit_id = 17 WHERE serialNumber IN
    ('151827002826', '151827003559', '151834240087');
UPDATE inverters SET pcs_unit_id = 18 WHERE serialNumber IN
    ('151834240095', '151834240078', '151827003588');
UPDATE inverters SET pcs_unit_id = 19 WHERE serialNumber IN
    ('151834240097', '151834240082', '151827003558');
UPDATE inverters SET pcs_unit_id = 20 WHERE serialNumber IN
    ('151827003625', '151819001884', '151834240096');
UPDATE inverters SET pcs_unit_id = 21 WHERE serialNumber IN
    ('151827003741', '151828990271', '151827003546');
UPDATE inverters SET pcs_unit_id = 22 WHERE serialNumber IN
    ('151827002976', '151834240072');


#
# Patrice - 10/17/2018
# Lets spoof the BYD400 data for now so we can schedule the battery
# regulator to run with variable charge and variable discharge.
#

UPDATE ess_units SET
  soc_charge_limit = 0.95,
  soc_discharge_limit = 0.05,
  battery_max_cell_voltage_limit = 4.000,
  battery_min_cell_voltage_limit = 2.800,
  max_environment_temperature = 45.0,
  max_mosfet_temperature = 55.0;

UPDATE ess_master_info SET
  battery_stack_soc = 50.0,
  battery_min_cell_voltage = 3.3,
  battery_max_cell_voltage = 3.3,
  battery_min_cell_tempreature = 21.0,
  battery_max_cell_temperature = 15.0;


#
# PIMENTEL ESS 2  DEVICE CONFIGURATION DATA
#

DELETE FROM aess_serial_device_server WHERE sds_id >= 3;
ALTER TABLE aess_serial_device_server AUTO_INCREMENT = 3;
INSERT INTO aess_serial_device_server (sds_id, mac_addr, ip_addr, rack_id, ess_unit_id) VALUES (3, '00:80:d4:09:5d:17', '10.252.2.253', 3, 2);
INSERT INTO aess_serial_device_server (sds_id, mac_addr, ip_addr, rack_id, ess_unit_id) VALUES (4, '00:80:d4:09:5d:17', '10.252.2.254', 4, 2);

DELETE FROM aess_battery_module_unit WHERE bmu_id >= 23;
ALTER TABLE aess_battery_module_unit AUTO_INCREMENT = 23;
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (23, 1,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (24, 2,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (25, 3,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (26, 4,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (27, 5,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (28, 6,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (29, 7,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (30, 8,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (31, 9,  3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (32, 10, 3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (33, 11, 3);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (34, 1,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (35, 2,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (36, 3,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (37, 4,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (38, 5,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (39, 6,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (40, 7,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (41, 8,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (42, 9,  4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (43, 10, 4);
INSERT INTO aess_battery_module_unit (bmu_id, modbus_number, sds_id) VALUES (44, 11, 4);

DELETE FROM aess_bmu_data WHERE bmu_id >= 23;
ALTER TABLE aess_bmu_data AUTO_INCREMENT = 23;
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (23, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (24, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (25, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (26, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (27, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (28, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (29, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (30, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (31, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (32, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (33, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (34, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (35, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (36, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (37, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (38, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (39, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (40, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (41, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (42, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (43, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);
INSERT INTO aess_bmu_data (bmu_id, state, current, voltage, soc, cell_voltage_1, cell_voltage_2, cell_temperature_1, cell_temperature_2) VALUES (44, "Unrestricted", 10.0, 55.0, 45.0, 3.51, 3.52, 31.0, 32.0);

DELETE FROM aess_charger_chassis WHERE chassis_id >= 4;
ALTER TABLE aess_charger_chassis AUTO_INCREMENT = 4;
INSERT INTO aess_charger_chassis (chassis_id, ip_addr, mac_addr, relay_ip_addr, relay_num) VALUES (4, '10.252.7.115', 'f4:e1:1e:9e:66:07', '10.252.7.4', 0);
INSERT INTO aess_charger_chassis (chassis_id, ip_addr, mac_addr, relay_ip_addr, relay_num) VALUES (5, '10.252.7.116', 'f4:e1:1e:97:0d:3d', '10.252.7.4', 1);

DELETE FROM aess_charger_module WHERE charger_id >= 23;
ALTER TABLE aess_charger_module AUTO_INCREMENT = 23;
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (23, 3, 1, '73-936-0048', 4, 23);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (24, 4, 1, '73-936-0048', 4, 24);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (25, 5, 1, '73-936-0048', 4, 25);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (26, 6, 1, '73-936-0048', 4, 26);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (27, 7, 1, '73-936-0048', 4, 27);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (28, 8, 1, '73-936-0048', 4, 28);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (29, 8, 1, '73-936-0048', 5, 29);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (30, 7, 1, '73-936-0048', 5, 30);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (31, 6, 1, '73-936-0048', 5, 31);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (32, 5, 1, '73-936-0048', 5, 32);
INSERT INTO aess_charger_module (charger_id, module_num, device_id, model_num, chassis_id, pcs_unit_id) VALUES (33, 4, 1, '73-936-0048', 5, 33);

DELETE FROM pcs_units WHERE id >= 23;
ALTER TABLE pcs_units AUTO_INCREMENT = 23;
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (23, 34, 23, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (24, 35, 24, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (25, 36, 25, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (26, 37, 26, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (27, 38, 27, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (28, 39, 28, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (29, 40, 29, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (30, 41, 30, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (31, 42, 31, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (32, 43, 32, 2);
INSERT INTO pcs_units (id, bmu_id_A, bmu_id_B, ess_unit_id) VALUES (33, 44, 33, 2);

DELETE FROM artesyn_charger_data WHERE chassis_id in (4,5);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (1, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (2, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (3, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (4, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (5, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (6, 4, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (1, 5, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (2, 5, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (3, 5, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (4, 5, 56.800, 0.0);
INSERT INTO artesyn_charger_data (module_num, chassis_id, vref, oc_fault_limit_multiplier) VALUES (5, 5, 56.800, 0.0);
