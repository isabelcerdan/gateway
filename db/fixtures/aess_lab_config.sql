DELETE FROM aess_serial_device_server;
ALTER TABLE aess_serial_device_server AUTO_INCREMENT = 1;
INSERT INTO aess_serial_device_server (mac_addr, ip_addr, rack_id) VALUES ('00:80:d4:09:2a:d7', '10.252.2.251', 1);

DELETE FROM aess_battery_module_unit;
ALTER TABLE aess_battery_module_unit AUTO_INCREMENT = 1;
INSERT INTO aess_battery_module_unit (modbus_number, sds_id) VALUES (1, 1);

DELETE FROM pcs_units;
ALTER TABLE pcs_units AUTO_INCREMENT = 1;
INSERT INTO pcs_units (bmu_id_A, bmu_id_B, ess_unit_id) VALUES (1, NULL, (SELECT id FROM ess_units LIMIT 1));

DELETE FROM aess_charger_chassis;
ALTER TABLE aess_charger_chassis AUTO_INCREMENT = 1;
INSERT INTO aess_charger_chassis (ip_addr, mac_addr) VALUES ('10.252.2.29', '38:d2:69:50:1a:f4');

DELETE FROM aess_charger_module;
ALTER TABLE aess_charger_module AUTO_INCREMENT = 1;
INSERT INTO aess_charger_module (chassis_id, module_num, pcs_unit_id, model_num) VALUES (1, 2, 1, '73-936-0048');

UPDATE inverters SET pcs_unit_id = NULL WHERE battery = 1;
UPDATE inverters SET pcs_unit_id = (SELECT pcs_unit_id FROM pcs_units LIMIT 1) WHERE battery = 1;
