DELETE FROM power_regulators;
ALTER TABLE power_regulators AUTO_INCREMENT = 1;
INSERT INTO power_regulators (feed_name, has_solar, has_storage, name, enabled)
VALUES ('Aggregate', 1, 1, 'Solar/Battery', 1);

DELETE FROM ess_units;
DELETE FROM ess_master_info;
ALTER TABLE ess_units AUTO_INCREMENT = 1;

INSERT INTO ess_units (name, model_id, mode, ip_address, mac_address, enabled, charge_rate_limit, discharge_rate_limit, power_regulator_id)
VALUES ('BYD400', (SELECT id FROM ess_models WHERE model = 'CESS 400'), 'auto', '192.168.1.100', 'aa:bb:cc:00:11:22', 1, -10, 10, 1);
INSERT INTO ess_master_info (ess_id) VALUES (1);

INSERT INTO ess_units (name, model_id, mode, ip_address, mac_address, enabled, charge_rate_limit, discharge_rate_limit, power_regulator_id)
VALUES ('AESS EVE', (SELECT id FROM ess_models WHERE model = 'EVE ESS'), 'auto',  '192.168.1.100', 'aa:bb:cc:00:11:22', 1, -10, 10, 1);
INSERT INTO ess_master_info (ess_id) VALUES (2);

