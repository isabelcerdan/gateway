UPDATE __db_version__ SET sql_down =
		"CREATE TABLE icpcon_control (
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' not null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			poll_freq_ms int(5) default '500' not null,
			updated int default '0' null
		);
		INSERT icpcon_control (enabled)
				SELECT enabled FROM daemon_control WHERE name = 'icpcond';

		CREATE TABLE meter_online_control
		(
			enabled tinyint(1) default '1' not null,
			started tinyint(1) default '0' not null,
			updated int default '0' null,
			pid varchar(20) null
		);
		INSERT meter_online_control (enabled, started, updated) VALUES (1, 0, 0);

		CREATE TABLE notify_control (
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' not null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			audit_frequency int(3) default '10' not null,
			updated int default '0' null
		);
		INSERT notify_control (enabled, audit_frequency)
				SELECT enabled, 15 FROM daemon_control WHERE name = 'notifyd';

		CREATE TABLE nplog_control (
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' not null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			log_interval int(5) default '500' null,
			log_cycle bigint default '2592000' null,
			updated int default '0' null
		);
		INSERT nplog_control (enabled, log_interval)
				SELECT enabled, polling_freq_sec*1000 FROM daemon_control WHERE name = 'nplogd';

		CREATE TABLE scheduler_control
		(
			enabled tinyint(1) default '1' null,
			started tinyint(1) default '0' null,
			running tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null
		);
		INSERT INTO scheduler_control (enabled)
				SELECT enabled FROM daemon_control WHERE name = 'schedulerd';

		CREATE TABLE cess_400_alarm_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int(5) default '10' null
		);
		INSERT INTO cess_400_alarm_control (enabled, polling_freq_sec)
				SELECT enabled, polling_freq_sec FROM daemon_control WHERE name = 'essalarmd';

		CREATE TABLE cess_400_controller_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null
		);
		INSERT INTO cess_400_controller_control (enabled)
				SELECT enabled FROM daemon_control WHERE name = 'esscontrollerd';

		CREATE TABLE cess_400_data_logger_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_ms int default '5000' not null,
			log_cycle int default '2592000' not null
		);
		INSERT INTO cess_400_data_logger_control (enabled, polling_freq_ms)
				SELECT enabled, polling_freq_sec * 1000.0 FROM daemon_control WHERE name = 'essdataloggerd';

		CREATE TABLE cess_400_monitor_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int(5) default '10' null
		);
		INSERT INTO cess_400_monitor_control (enabled, polling_freq_sec)
				SELECT enabled, polling_freq_sec FROM daemon_control WHERE name = 'essmonitord';

		CREATE TABLE cess_400_sysalarms_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_ms int default '5000' not null
		);
		INSERT INTO cess_400_sysalarms_control (enabled, polling_freq_ms)
				SELECT enabled, polling_freq_sec*1000 FROM daemon_control WHERE name = 'esssysalarmd';

		CREATE TABLE eve_bmu_controller_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null
		);
		INSERT INTO eve_bmu_controller_control (enabled) VALUES (0);

		CREATE TABLE eve_bmu_data_aggregator_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '5' not null
		);
		INSERT INTO eve_bmu_data_aggregator_control (enabled) VALUES (0);

		CREATE TABLE eve_bmu_data_publisher_factory_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '1' not null
		);
		INSERT INTO eve_bmu_data_publisher_factory_control (enabled) VALUES (0);

		CREATE TABLE eve_bmu_data_subscriber_factory_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '1' not null
		);
		INSERT INTO eve_bmu_data_subscriber_factory_control (enabled) VALUES (0);

		CREATE TABLE eve_bmu_protection_subscriber_factory_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '86400' not null
		);
		INSERT INTO eve_bmu_protection_subscriber_factory_control (enabled) VALUES (0);

		CREATE TABLE eve_bmu_status_subscriber_factory_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '1' not null
		);
		INSERT INTO eve_bmu_status_subscriber_factory_control (enabled) VALUES (0);

		create table discharger_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' not null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			poll_freq_ms int(5) default '500' not null,
			updated int default '0' null
		);
		INSERT INTO discharger_control (enabled) VALUES (0);

		create table ess_limits_enforcer_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '1' not null
		);
		INSERT INTO ess_limits_enforcer_control (enabled) VALUES (0);

		CREATE TABLE aess_bmu_data_logger_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '5' not null
		);
		INSERT INTO aess_bmu_data_logger_control (enabled) VALUES (0);

		CREATE TABLE msg_broker_control
		(
			enabled tinyint(1) default '0' null,
			started tinyint(1) default '0' null,
			updated int default '0' null,
			pid varchar(10) null,
			active_id int null,
			suspended varchar(5) null,
			polling_freq_sec int default '5' not null
		);
		INSERT INTO msg_broker_control (enabled) VALUES (0);

		ALTER TABLE power_factor_control ADD COLUMN enabled tinyint(1) default '0' null;
		ALTER TABLE power_factor_control ADD COLUMN started tinyint(1) default '0' not null;
		ALTER TABLE power_factor_control ADD COLUMN pid varchar(10) null;
		ALTER TABLE power_factor_control ADD COLUMN updated int default '0' null;

		ALTER TABLE irradiance_control ADD COLUMN enabled tinyint(1) default '0' null;
		ALTER TABLE irradiance_control ADD COLUMN started tinyint(1) default '0' not null;
		ALTER TABLE irradiance_control ADD COLUMN pid varchar(10) null;
		ALTER TABLE irradiance_control ADD COLUMN updated int default '0' null;"
WHERE name = '20181105170726_daemon_control_clean.migration';