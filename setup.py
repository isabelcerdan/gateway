#!/usr/bin/env python
	
import os
import sys

#sys.path.insert(0, os.path.abspath('lib'))
__version__='1.3.0'
__author__='Apparent, Inc.'
try:
    from setuptools import setup, find_packages
except ImportError:
    print("apparent-gateway needs setuptools in order to build. Install it using"
            " your package manager (usually python-setuptools) or via pip (pip"
            " install setuptools).")
    sys.exit(1)

setup(name='apparent-gateway',
      version=__version__,
      description='Apparent, Inc. - Gateway Software',
      author=__author__,
      author_email='apparent-eng@apparent.com',
      url='http://www.apparent.com/',
      license='Proprietary work',
      install_requires=['MySQL-python==1.2.5', 'requests==2.10.0', 'pymodbus==1.2.0', 'pyserial==2.7.0'],
      packages=['python', 'python.lib', 'python.lib.gateway_constants'],
      classifiers=[
          'Development Status :: 2 - Pre-Alpha',
          'Environment :: Console',
          'Intended Audience :: Other Audience',
          'Natural Language :: English',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python :: 2.7',
          'License :: Other/Proprietary License',
          'Topic :: Software Development :: Embedded Systems'
      ],
)
