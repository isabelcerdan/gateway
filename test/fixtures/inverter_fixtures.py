"""
Create large number of inverters to test iptables.py used to generate /etc/iptables
"""

import os
import sys
import struct

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)


from lib.db import prepared_act_on_database, EXECUTE


def get_ip():
    ip = "10.252.%s.%s" % ((get_ip.value / 256) + 2, get_ip.value % 256)
    get_ip.value += 1
    return ip


get_ip.value = 0


def get_mac():
    octet0 = get_mac.value % 256
    octet1 = get_mac.value / 256
    octet2 = get_mac.value / 1024
    get_mac.value += 1
    return "e0:1f:0a:%0.2x:%0.2x:%0.2x" % (octet2, octet1, octet0)


get_mac.value = 0


def get_serial():
    serial = "1715470%0.5d" % get_serial.value
    get_serial.value += 1
    return serial


get_serial.value = 0


for i in range(100):
    sql = "insert into inverters (serialNumber, mac_address, ip_address, version) values ('%s','%s','%s','%s')" % \
          (get_serial(), get_mac(), get_ip(), "313_1436")
    print(sql)
    prepared_act_on_database(EXECUTE, sql, [])
