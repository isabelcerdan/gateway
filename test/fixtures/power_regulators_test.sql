DELETE FROM brainboxes;
ALTER TABLE brainboxes AUTO_INCREMENT = 1;
INSERT INTO brainboxes (target_interface, IP_Address, mac_address, baud, tcp_port) VALUES ('/dev/pcc', '10.252.2.10', 'aa:bb:cc:11:22:33', 38400, 9001);

DELETE FROM energy_meter;
ALTER TABLE energy_meter AUTO_INCREMENT = 1;
INSERT INTO energy_meter
    (meter_name, meter_role, meter_class, modbusdevicenumber, address, baud, network_comm_type, meter_type, enabled)
  VALUES
    ('PCC', 'pcc', '48', 1, '/dev/pcc', 38400, 'rtu', 'veris_brainbox', 1);

DELETE FROM power_regulators;
ALTER TABLE power_regulators AUTO_INCREMENT = 1;
INSERT INTO power_regulators (feed_name, has_solar, has_storage, name, enabled) VALUES ('Aggregate', 1, 1, 'Test', 1);

DELETE FROM meter_feeds;
ALTER TABLE meter_feeds AUTO_INCREMENT = 1;
INSERT INTO meter_feeds
  (power_regulator_id, meter_role, feed_name, threshold, target_offset)
  VALUES (1, 'pcc', 'Delta', 5.0, 2.0);

DELETE FROM ess_units;
ALTER TABLE ess_units AUTO_INCREMENT = 1;
INSERT INTO ess_units (name, model_id, enabled, charge_rate_limit, discharge_rate_limit, power_regulator_id) VALUES ('AESS-1', 4, 1, -100, 100, 1);
INSERT INTO ess_units (name, model_id, enabled, charge_rate_limit, discharge_rate_limit, power_regulator_id) VALUES ('BYD400-1', 1, 1, -400, 400, 1);

DELETE FROM ess_master_info;
ALTER TABLE ess_master_info AUTO_INCREMENT = 1;
INSERT INTO ess_master_info (ess_id) VALUES (1);
INSERT INTO ess_master_info (ess_id) VALUES (2);
