# Gateway Unit Tests

## Dependencies

To run unit tests, you first need to install test dependencies listed in requirements.txt.

```
$ pip install -r requirements.txt
```

## Running Unit Tests

```
$ python unitests.py
``` 
