#!/usr/share/apparent/.py2-virtualenv/bin/python
import time
from datetime import datetime, timedelta
import logging.handlers

LOGGING_FORMAT = '%(asctime)s %(name)s [%(levelname)s]: %(message)s'

def run():
    logger = logging.getLogger()
    handler = logging.handlers.RotatingFileHandler("start.log")
    handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
    logger.addHandler(handler)

    logger.info("stopping...")
    time.sleep(0.5)
    logger.info("done.")

if __name__ == '__main__':
    run()