from __future__ import print_function
import csv
import time
from optparse import OptionParser

import paramiko

host = '192.168.88.99'
username = 'localadmin'
password = 'localadmin'


def get_data(filename):
    with open(filename, "r") as f:
        for record in csv.reader(f):
            yield record


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-1", "--input1", dest="in_filename1", help="Input file 1 path")
    parser.add_option("-2", "--input2", dest="in_filename2", help="Input file 2 path")
    (options, args) = parser.parse_args()
    iter_data = iter(get_data(options.in_filename1))

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username=username, password=password, port=2112)
    transport = ssh.get_transport()
    session = transport.open_session()
    session.get_pty()
    session.invoke_shell()

    for row in iter_data:
        row = dict(zip(['load', 'state', 'time'], row))
        print(row)
        cmd = "loadctl %s -o %s" % (row['state'].lower(), row['load'])
        print(cmd)

        session.send(cmd)
        while not session.recv_ready():
            time.sleep(0.3)

        output = ""
        buf = session.recv(256)
        while buf != 0:
            buf = session.recv(256)
            output += buf
        print(output)

        time.sleep(float(row['time']))
