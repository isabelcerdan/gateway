# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from datetime import datetime
from datetime import timedelta
import sys, os
import unittest, time, re

class Test21(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.chromedr.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_21(self):
        write_to = open("outcome.txt", "w")
        driver = self.driver
        driver.get("http://localhost/")
        driver.find_element_by_id("inputEmail").click()
        driver.find_element_by_id("inputEmail").clear()
        driver.find_element_by_id("inputEmail").send_keys("apparent@apparent.com")
        driver.find_element_by_id("inputPassword").click()
        driver.find_element_by_id("inputPassword").clear()
        driver.find_element_by_id("inputPassword").send_keys("qwerty4you")
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following"
                                     "::button[1]").click()
        driver.find_element_by_link_text("Inverters").click()
        num_inverts = driver.find_element_by_id("inverterdata_info").text
        driver.find_element_by_id("inverterdata-select-all").click()
        driver.find_element_by_id("delete-inverters").click()
        self.assertRegexpMatches(self.close_alert_and_get_its_text(), r"^Are you sure you want to delete these "
                                                                      r"inverters[\s\S]$")
        self.accept_next_alert = True
        time = False
        check = False
        count = 0
        while check is not True:
            try:
                self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text,
                                         r"^[\s\S]*Showing 0 to 0 of 0 entries[\s\S]*$")
                check = True
            except:
                count += 1
        check = False
        os.system("sudo python /usr/share/apparent/python/gpts.py ip_reset 239.192.0.0")
        start = datetime.now()
        end = start + timedelta(seconds=1200)
        while (check is not True) and (time is not True):
            new_num = driver.find_element_by_id("inverterdata_info").text
            if new_num == num_inverts:
                check = True
            elif datetime.now() > end:
                time = True
        if time is True:
            write_to.write("inverters didn't return\n")
        else:
            write_to.write("inverters returned\n")
        driver.find_element_by_link_text("Detect").click()
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Auto Detect Strings'])[1]"
                                     "/following::button[1]").click()
        string = False
        str_fail = False
        start = datetime.now()
        end = start+timedelta(seconds=600)
        while(string is not True) and (str_fail is not True):
                try:
                        self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*audit_"
                                                                                                   r"complete[\s\S]*$")
                        string = True
                except AssertionError as e: self.verificationErrors.append(str(e))
                if end <= datetime.now():
                        str_fail = True
        if str_fail is True:
                write_to.write("String Detect failed\n")
        else:
                write_to.write("String Detect passed\n")
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Auto Detect Feeds'])"
                                     "[1]/following::button[1]").click()
        driver.find_element_by_id("select_all").click()
        driver.find_element_by_id("run_now").click()
        driver.switch_to_frame(driver.find_element_by_tag_name("iframe"))
        driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Strings Scanned :'])"
                                     "[1]/following::button[1]").click()
        # Warning: verifyTextPresent may require manual changes
        feed = False
        feed_fail = False
        start = datetime.now()
        end = start+timedelta(seconds=1200)
        while(feed is not True) and (feed_fail is not True):
                try:
                        self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Total "
                                                                                                   r"Strings: 3 "
                                                                                                   r"Strings Scanned "
                                                                                                   r":3[\s\S]*$")
                        feed = True
                except AssertionError as e:
                    self.verificationErrors.append(str(e))
                if end <= datetime.now():
                    feed_fail = True
        if feed_fail is True:
            write_to.write("Feed Detect failed\n")
        else:
            write_to.write("Feed Detect passed\n")
        write_to.close()
    
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
