#!/usr/bin/env python
import sys
#import NXO_export_parser_p2
import os
import subprocess
from datetime import datetime
from datetime import timedelta
from subprocess import call
from time import sleep

# To run this you will need to fill the Branch_names.txt file with the branches that you want to test The txt file
# above shouldn't have any random spaces or branches on the same line. The txt file should have one branch per line
# and no blank lines because the blank line might be taken as another branch The command line argument that you will
# use will be ./time_splice.py Branch_names "%Y-%m-%d %H:%M:%D"
# The datetime in the command line argument is the ending datetime that you want the recording to end.
# Start running the recording first and then run this program because this program will save when it was started and use
# the results starting there.

# This gets the number of branches in the text file
def get_num_branches(names):
    check = False
    line = names.readline()# reads the first line of the text file
    count = 1# counter to find number of branches, starts at 1 because the first line has already been read
    while check is False:# goes until the end of the text file is reached
        line = names.readline()
        if line == "":# checks to see if the end of the text file is reached, if it has then the while loop is signified to end
            check = True
        else:
            count += 1
    return count


# This creates the branch array and populates the branch name part of the array
def get_branches_array(names, num_branches):
    branches = [[0 for x in range(3)] for y in range(num_branches)]# a 2d array where the first dimension is a length of number of branches and the second dimension is length 3
    names.seek(0)# brings the reader to the top of the text file again
    for m in range(num_branches):
        line = names.readline().strip()
        branches[m][0] = line# adds the name of the branch to the corresponding spot in the 2d array
    return branches


# This gets the length in time of how long the program will run
def calculate_running_time(starting_date_object, ending_date_time_object):
    clock = ending_date_time_object - starting_date_object# calculates the time between the start and end using timedelta
    return clock


def print_array(branches, num_branches):
    for m in range(num_branches):
        for n in range(3):
            print(branches[m][n])
        print("\n")


# This puts the date and times into the array
def fill_array(starting_date_object, running_time, branches, num_branches):
    for m in range(num_branches):
        branches[m][1] = starting_date_object# this is when the branch starts to record
        starting_date_object = starting_date_object + running_time# calculates when the branch should end
        if m+1 == num_branches:
            starting_date_object += timedelta(seconds=1)
        branches[m][2] = starting_date_object# this is when the branch should end recording
        starting_date_object += timedelta(seconds=1)# adds a second to the starting time for the next branch so that the start and end of the two branches next to each other don't mix
    return branches


def wait_for_finish(branches, num):
    print(datetime.today().time())
    time_inbetween = branches[num][2] - branches[num][1]
    print(time_inbetween.total_seconds())
    sleep(time_inbetween.total_seconds())


def main():
    if len(sys.argv) == 3:
        switchTimes = open("switchTiming.txt", "w")
        with open(sys.argv[1], 'r') as names:
            num_branches = get_num_branches(names)
            print(num_branches)
            time_intervals = [0 for y in range(num_branches*2)]
            branchHead = [0 for y in range(num_branches)]
            branches = get_branches_array(names, num_branches)
            starting_date_object = datetime.now()
            ending_date_time_object = datetime.strptime(sys.argv[2], "%Y-%m-%d %H:%M:%S")# this makes the ending object based on the datetime and the command line argument
            running_time = calculate_running_time(starting_date_object, ending_date_time_object)# calculates the complete running time
            individual_time = (running_time / num_branches) - timedelta(seconds=1)# calculates the individual running times for each branch
            branches = fill_array(starting_date_object, individual_time, branches, num_branches)# fills the branch array up with the dates and times being used
        print_array(branches, num_branches)
        n = 0
        for m in range(num_branches):
            os.system("/usr/share/apparent/bin/getgat %s r" % branches[m][0])
            time_intervals[n] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            output = subprocess.check_output("git rev-parse HEAD", shell=True)
            branchHead[m] = output
            n += 1
            print("going into wait")
            wait_for_finish(branches, m)
            time_intervals[n] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            n += 1
        m = 0
        for n in range(num_branches):
            switchTimes.write(branches[n][0])  # writes the branch
            switchTimes.write("\n")
            switchTimes.write(time_intervals[m])  # writes the starting time for the current branch, The first one will be the universal starting time for the test run
            switchTimes.write("\n")
            m += 1
            switchTimes.write(time_intervals[m])  # writes the ending time for the current branch, The last one will be the universal ending time for the test run
            switchTimes.write("\n")
            m += 1
            switchTimes.write(branchHead[n])

        switchTimes.close()
    else:
        print("Wrong number of command line arguments")


main()
