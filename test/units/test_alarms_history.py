import os
import sys

import unittest
from alarm.alarms_history import *
from lib.db import prepared_act_on_database

class AlarmsTests(unittest.TestCase):

    def setUp(self):
        sql = "DELETE FROM alarms_history"
        prepared_act_on_database(EXECUTE, sql)

    def tearDown(self):
        pass

    def test_debounce_alarm(self):
        alarms_history = AlarmsHistory()
        Alarm.occurrence(ALARM_ID_GATEWAY_REBOOT)
        Alarm.occurrence(ALARM_ID_LOW_CPU_CORE_TEMP)
        Alarm.occurrence(ALARM_ID_LOW_CPU_TEMP)
        Alarm.occurrence(ALARM_ID_LOW_SYSTEM_TEMP)
        alarms_history.update()

        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR)
        Alarm.occurrence(ALARM_ID_AUDITOR_DAEMON_RESTARTED)
        alarms_history.update()

        Alarm.request_clear(ALARM_ID_LOW_CPU_TEMP)
        alarms_history.update()


        self.assertEquals(False, alarms_history.alarms_bitarray[ALARM_ID_AUDITOR_DAEMON_RESTARTED - 1], "Alarm is cleared after debounce period.")

if __name__ == '__main__':
    unittest.main()
