import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from lib.logging_setup import *
from event.event_constants import *
from alarm.alarms_history import *

class EventsTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_dispatch_event(self):

        logger = logging.getLogger('events_test')
        logger.error('Holy smokes Batman! We rebooted!',
                     extra={'alarm_id': ALARM_ID_GATEWAY_REBOOT, 'event_tags': [EVENT_TAG_OS]})

        alarms_history = AlarmsHistory()
        alarms_history.update()

        gateway_reboot_alarm_raised = alarms_history.alarms_bitarray[ALARM_ID_GATEWAY_REBOOT - 1]

        self.assertEqual(gateway_reboot_alarm_raised, True, 'Dispatching event successfully raises alarm.')


if __name__ == '__main__':
    unittest.main()
