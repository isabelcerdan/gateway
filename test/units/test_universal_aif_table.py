from unittest import TestCase
from gpts import GeneralPurposeTlvSendScript
from gateway_utils import update_universal_aif_table
from lib.gateway_constants.DBConstants import UNIVERSAL_AIF_POWER_FACTOR_COLUMN_NAME, \
    UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME, \
    UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME, \
    UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME
import time


valid_pfs = [0.707, 1.00, 0.95, 0.82, 0.71]
valid_c_leads = [0,1]
valid_enables = [0,1]
valid_regions = { "CA120":28, "HI120":10, "IEEE":12}


def build_input_sets():
    input_sets = []
    for region in valid_regions.keys(): # only use the string names of the regions
        for pf in valid_pfs:
            for lead in valid_c_leads:
                for enable in valid_enables:
                    input_sets.append( (region, pf, lead, enable) )
    return input_sets



class TestUniversalAifDBFunctions(TestCase):


    def prep_db(self, region, pf, c_lead, enable):
        update_universal_aif_table(region, pf, c_lead, enable)


    def test_get_regional_settings_data(self):
        verbose = True

        all_input_permutations = build_input_sets()
        for (region, pf, lead, enable) in all_input_permutations:

            # Arrange
            self.prep_db(region, pf, lead, enable)

            # Act
            row_dict = GeneralPurposeTlvSendScript.get_universal_aif_data()

            new_region = row_dict[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]
            new_pf = row_dict[UNIVERSAL_AIF_POWER_FACTOR_COLUMN_NAME]
            new_lead = row_dict[UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME]
            new_enable = row_dict[UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME]

            # Assert
            if verbose:
                print ("+----------------------------+")
                print("| %8s | %6s | %6s |" % ("Value", "Input", "Output"))
                print ("+----------------------------+")
                print("| %8s | %6s | %6s |" % ("Region", region, new_region))
                print("| %8s | %6.3f | %6.3f |" % ("PF", pf, new_pf))
                print("| %8s | %6i | %6i |" % ("Lead", lead, new_lead))
                print("| %8s | %6i | %6i |" % ("Enable", enable, new_enable))
                print ("+----------------------------+")

            assert(region == new_region)
            assert(pf == new_pf)
            assert(lead == new_lead)
            assert(enable == new_enable)
