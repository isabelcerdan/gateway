import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from dhcp_listener import DhcpListener
from dhcp.dhcp_client import DhcpClient

class DhcpListenerTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_new_lease(self):

        mac_address = 'e0:1f:0a:67:89:ab'
        ip_address = '127.0.0.1'
        argv = ['dhcp_listener.py', 'add', mac_address, ip_address]
        DhcpListener(argv)

        dhcp_client = DhcpClient(mac_address)
        record = dhcp_client.find_by_mac_address()

        self.assertEqual(ip_address, record['IP_Address'], 'New lease should be added to inverters table.')

    def test_icpcon_device(self):

        mac_address = '00:0d:e0:67:89:ba'
        ip_address = '127.0.0.1'
        argv = ['dhcp_listener.py', 'add', mac_address, ip_address]
        DhcpListener(argv)

        dhcp_client = DhcpClient(mac_address)
        record = dhcp_client.find_by_mac_address()

        self.assertEqual(ip_address, record[dhcp_client.ip_field], 'ICPCon device should be added to icpcon_devices table.')

    def test_unknown_device(self):

        mac_address = 'aa:bb:cc:00:11:22'
        ip_address = '127.0.0.1'
        argv = ['dhcp_listener.py', 'add', mac_address, ip_address]
        DhcpListener(argv)

        dhcp_client = DhcpClient(mac_address)
        record = dhcp_client.find_by_mac_address()

        self.assertEqual(record, None, 'Unknown device should return None.')

if __name__ == '__main__':
    unittest.main()
