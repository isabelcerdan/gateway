import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..','python')
sys.path.append(path)

import unittest
import time
from alarm.alarms_history import *

class AlarmsTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_set_and_clear_alarm(self):
        # record an incident for an alarm
        Alarm.occurrence(ALARM_ID_GATEWAY_REBOOT)

        # re-retrieve alarm from db
        alarm = Alarm.find(ALARM_ID_GATEWAY_REBOOT)
        self.assertEqual(True, alarm.raised, 'Set and clear alarm should have been set.')

    def test_max_count_for_duration_alarm(self):
        # record an incident for an alarm
        alarm = Alarm.find(ALARM_ID_PCC_METER_MODBUS_READ_ERROR)

        # trigger alarm by recording and incident, sleeping for duration and recording a final incident
        for i in range(alarm.defined_count):
            Alarm.occurrence(ALARM_ID_PCC_METER_MODBUS_READ_ERROR)

        # re-retrieve alarm from db
        alarm = Alarm.find(ALARM_ID_PCC_METER_MODBUS_READ_ERROR)
        self.assertEqual(True, alarm.raised, 'Max failures within duration alarm should have been set.')

    def test_max_count_for_duration_no_alarm(self):
        # record an incident for an alarm
        alarm = Alarm.find(ALARM_ID_HIGH_CPU_CORE_TEMP)
        alarm.clear()

        # trigger alarm by recording and incident, sleeping for duration and recording a final incident
        for i in range(alarm.defined_count - 1):
            alarm._occurrence()

        # re-retrieve alarm from db
        alarm = Alarm.find(ALARM_ID_HIGH_CPU_CORE_TEMP)
        self.assertEqual(False, alarm.raised, 'Max failures within duration alarm should not have been set.')

    def test_record_alarm_history(self):
        alarms_history = AlarmsHistory()

        Alarm.occurrence(ALARM_ID_GATEWAY_REBOOT)
        Alarm.occurrence(ALARM_ID_ENERGY_BUCKET_50_CAPACITY)

        alarms_history.update()
        self.assertEquals(True, alarms_history.alarms_bitarray[ALARM_ID_GATEWAY_REBOOT - 1], "Alarm occurrence should be recorded in history.")

    def test_save_alarm_history(self):
        alarms_history = AlarmsHistory()

        Alarm.occurrence(ALARM_ID_HIGH_CPU_CORE_TEMP)
        Alarm.request_clear(ALARM_ID_HIGH_CPU_CORE_TEMP)

        self.assertEquals(False, alarms_history.alarms_bitarray[ALARM_ID_HIGH_CPU_CORE_TEMP - 1], "Alarm occurrence should be recorded in history even when cleared before recorded.")
        alarms_history.update()
        self.assertEquals(True, alarms_history.alarms_bitarray[ALARM_ID_HIGH_CPU_CORE_TEMP - 1], "Alarm occurrence should be recorded in history even when cleared before recorded.")

    def test_debounce_alarm(self):
        alarms_history = AlarmsHistory()
        Alarm.occurrence(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        alarms_history.update()

        alarm = Alarm.find(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        self.assertEquals(True, alarm.raised, "Alarm is still raised after update.")

    def test_debounce_alarm(self):
        alarm = Alarm.find(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        alarm.debounce_duration = 1
        alarm.save()

        alarms_history = AlarmsHistory()
        Alarm.occurrence(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        alarms_history.update()

        alarm = Alarm.find(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        self.assertEquals(True, alarm.raised, "Alarm is initially raised.")
        alarms_history.update_interval = 1
        time.sleep(1)
        alarms_history.update()

        alarm = Alarm.find(ALARM_ID_INVERTER_DAEMON_A_RESTARTED)
        self.assertEquals(False, alarm.raised, "Alarm is cleared after debounce period.")

if __name__ == '__main__':
    unittest.main()
