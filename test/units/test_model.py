# -*- coding: utf-8 -*-
import os
import sys
import datetime

import unittest

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import ess.apparent.pcs_unit as pcs_unit
from inverter.inverter import Inverter


class ModelTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_model_find_or_create_by(self):
        inverter = Inverter.find_or_create_by(serialNumber='111111')
        inverter.destroy()

    def test_model_find_all(self):
        pcs_units = pcs_unit.PcsUnit.find_all()
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')

    def test_model_with_no_table(self):
        inverters = Inverter.find_all()
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')


if __name__ == '__main__':
    unittest.main()
