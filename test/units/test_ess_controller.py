import unittest
from ess.ess_controller import EssControllerDaemon
from EssCLI import publish_message
from EVE.MsgBroker import MsgBrokerDaemon
from daemon.daemon_monitor import DaemonMonitor


class EssControllerTest(unittest.TestCase):
    def setUp(self):
        self.ess_unit_id = 1
        self.msgbroker_monitor = DaemonMonitor(MsgBrokerDaemon, enabled=1, no_restarts=True)
        self.msgbroker_monitor.start()
        self.ess_controller_monitor = DaemonMonitor(EssControllerDaemon, enabled=1, no_restarts=True)
        self.ess_controller_monitor.start()

    def tearDown(self):
        self.ess_controller_monitor.stop()
        self.msgbroker_monitor.stop()

    def test_ess_cli(self):
        publish_message(self.ess_unit_id, "run|None")


    # def test_save(self):
    #     self.ess_controller.save(unit_work_state="Fault")
    #     sql = "SELECT * FROM ess_master_info WHERE ess_id = %s" % "%s"
    #     result = prepared_act_on_database(FETCH_ONE, sql, [self.ess_controller.ess_unit.id])
    #     if result['unit_work_state'] != "Fault":
    #         self.fail("It was bad")

if __name__ == '__main__':
    unittest.main()
