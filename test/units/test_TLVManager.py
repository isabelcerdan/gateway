from unittest import TestCase
from gpts import TLVTranslator as man
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *

from collections import namedtuple
import random as rand


"""
Input space partitions

TLV IDs
 - Empty, one item, multiple items
 - Known / unknown IDs

TLV conversions
 - string
 - capital string
 - hex number
 - floating point

DB interaction
 - update database
 - compare to database but don't update
 - don't compare, just print
 
Output types
 - excel string
 - free form text output
 - database update arrays
 
Special cases to look for
 - Runmode is parsed and stored in a nonstandard way
 - 
"""

CHECK_FOR_UPDATE = 1
CHECK_FOR_NO_UPDATE = 2
DONT_CHECK_FOR_UPDATE = 3

SAMPLE_REGIONAL_SETTINGS_TABLE = {
    UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME : "IEEE",
    UNIVERSAL_AIF_POWER_FACTOR_COLUMN_NAME : 0.95,
    UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME : 1,
    UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME : 1
}

class TestTLVManager(TestCase):


    out = None

    my_amazing_tuple_with_a_very_long_name_but_i_dont_care_because_i_will_never_use_the_name_again \
        = namedtuple("TestCase", "tlv_id tlv_value column_name preload_db_val")

    ''' Pre-test helpers '''

    def setUp(self):
        self.tlv_ids_list = []
        self.tlv_values_list = []
        rand.seed()

    def file_init(self, keyword):
        self.out = open("out/tlvmtest-%s.out" % keyword, 'w')

    def tearDown(self):
        if self.out is not None:
            self.out.close()
            self.out = None

    ''' Basic test methods '''

    SAMPLE_IP = "someIPaddress"
    SAMPLE_STRING = "stringIDhere"
    SAMPLE_STRING_POSITION = 4

    def test_single_basic_tlv(self):
        self.file_init("single")
        tlv = [SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE]
        val = [10]
        db = {INVERTERS_TABLE_RESETS_COLUMN_NAME : 9}
        regional_settings = {"pf" : 0.95}
        num = 3

        my_man = man(tlv, val, db, regional_settings, num, TestTLVManager.SAMPLE_IP,
                     TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        text_out = my_man.get_text_file_output()
        db_up_col, db_up_arg = my_man.get_db_update_info()

        self.out.write(text_out)

        assert("RESET" in text_out)
        assert("*** DB UPDATED ***" in text_out)

        assert(db_up_col == [INVERTERS_TABLE_RESETS_COLUMN_NAME])
        assert(db_up_arg == [10])

    def test_empty_tlv_lists(self):
        self.file_init("empty")
        tlvlist = []
        vallist = []
        sql = {}
        regional_settings = {"pf" : 0.95}
        num = 0

        my_man = man(tlvlist, vallist, sql, regional_settings, num, TestTLVManager.SAMPLE_IP,
                     TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        text_out = my_man.get_text_file_output()
        db_up_col, db_up_arg = my_man.get_db_update_info()

        self.out.write(text_out)
        print "Wrote to the file."

        assert(not "*** DB UPDATED ***" in text_out)
        assert(my_man.NUMBER_OF_TLV_TYPES == text_out.count("NOT_REPORTED"))

    def test_small_tlv_list(self):
        self.file_init("small-list")

        self.build_a_set_of_tlvs()
        db = {
            INVERTERS_TABLE_STATUS_COLUMN_NAME : INVERTERS_TABLE_STATUS_STRING_UNKNOWN,
            INVERTERS_TABLE_VERSION_COLUMN_NAME: "version 30",
            INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME : "A9:gG:45",
            INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME : "21009"
        }
        regional_settings = {"pf" : 0.95}


        my_man_steve = man(self.tlv_ids_list, self.tlv_values_list, db, regional_settings, 42, TestTLVManager.SAMPLE_IP,
                     TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        text = my_man_steve.get_text_file_output()
        self.out.write(text)

        update_cols, update_args = my_man_steve.get_db_update_info()

        # make sure the version is getting updated,
        # with the newest version getting written into the database
        assert(INVERTERS_TABLE_VERSION_COLUMN_NAME in update_cols)
        version_index = update_cols.index(INVERTERS_TABLE_VERSION_COLUMN_NAME)
        assert(update_args[version_index] == "version 31")

        # make sure the case in the mac address was not a problem
        assert(not INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME in update_cols)


    ''' Helpers for basic test methods '''

    def build_a_set_of_tlvs(self):
        self.add(SG424_TO_GTW_RUNMODE_U16_TLV_TYPE, RUNMODE_SG424_APPLICATION)
        self.add(SG424_TO_GTW_VERSION_STRING_TLV_TYPE, "version 31")
        self.add(SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE, "a9:GG:45")
        self.add(SG424_TO_GTW_PROD_PART_NO_STRING_TLV_TYPE, 21009)

    def add(self, tlv_id, value):
        if len(self.tlv_ids_list) != len(self.tlv_values_list):
            raise Exception("TLV list lengths did not match!!!!")

        self.tlv_ids_list.append(tlv_id)
        self.tlv_values_list.append(value)


    ''' Info for better test methods '''

    TLVData = namedtuple("TLVData", "id constant_name type col_name update_possible update_expected")

    LONG = 0
    SHORT = 1
    BOOL = 2
    FLOAT = 3
    STRING = 4

    @staticmethod
    def get_all_dica_tlvs(plan_update):
        INDEXOF_ID = 0
        INDEXOF_NAME = 1
        INDEXOF_DATATYPE = 2
        INDEXOF_INCLUDE = 3
        INDEXOF_DBCOL = 4
        INDEXOF_UPDATE = 5

        tlv_file = open("../resources/tlv.csv", 'r')
        file_contents = tlv_file.read()
        tlv_file.close()
        all_lines = file_contents.splitlines()
        testable_tlvs = []

        for i in range (1, len(all_lines)):
            single_line = all_lines[i].split(',')

            if int(single_line[INDEXOF_INCLUDE]) == 0:
                # This one is not part of dica, so let's not bother even looking at it.
                continue


            datatype = -1
            type_string = single_line[INDEXOF_DATATYPE].upper()

            if(type_string == "SHORT"):
                datatype = TestTLVManager.SHORT
            elif(type_string == "LONG"):
                datatype = TestTLVManager.LONG
            elif(type_string == "BOOL"):
                datatype = TestTLVManager.BOOL
            elif(type_string == "FLOAT"):
                datatype = TestTLVManager.FLOAT
            elif(type_string == "STRING"):
                datatype = TestTLVManager.STRING
            elif(type_string == "DEPRECATED"):
                continue
            else:
                print "ERROR parsing type of TLV ID %s : %s" \
                      % (single_line[INDEXOF_ID], single_line[INDEXOF_NAME])
                continue
            # end if/else

            new_tlv = TestTLVManager.TLVData(int(single_line[INDEXOF_ID]),  # ID number for this TLV
                                   single_line[INDEXOF_NAME],     # Constant name for this TLV
                                   datatype,                      # Datatype for this TLV
                                   single_line[INDEXOF_DBCOL],    # DB column name for this TLV
                                   (int(single_line[INDEXOF_UPDATE]) == 1),  # Whether to update DB with this TLV
                                   plan_update)  # Whether the tester should check for an update. Default is no.

            if new_tlv.id == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                # skip this one. it is a special case.
                continue

            testable_tlvs.append(new_tlv)

        # end for



        print "Added %d TLVs to be tested for." % len(testable_tlvs)

        ''' Now, testable_tlvs contains all TLVs that are used in DICA.
        It also indicates the type of each, allowing us to build appropriate values '''

        return testable_tlvs
    # end def

    @staticmethod
    def build_tlv_lists_and_db_row(updates_desired):
        """
        I took the code from the other method so that I could use this in the other test.
        :param updates_desired:
        :return:
        """
        all_types = TestTLVManager.get_all_dica_tlvs(updates_desired)

        # Initialize lists that will be populated
        tlv_ids_list = []
        tlv_values_list = []
        database = {}

        # Load up the lists with data that matches the kind of datatypes the gateway is
        # expecting from the inverter.
        for tlv_type in all_types:

            planned_response = TestTLVManager.get_value_for_this_datatype(tlv_type.type)

            # Preload the database with a datatype that will cause it to update or not,
            # whatever was specified by the test case TLV
            if tlv_type.update_possible:
                if tlv_type.update_expected:
                    database[tlv_type.col_name] = TestTLVManager.get_value_for_this_datatype(tlv_type.type, planned_response)
                else:
                    database[tlv_type.col_name] = planned_response

            tlv_ids_list.append(tlv_type.id)
            tlv_values_list.append(planned_response)
        # end for - we added each TLV type to the list

        return tlv_ids_list, tlv_values_list, database

    def test_all_dica_db_interactions(self):
        # This will guide us when we build the TLVs
        TEST_WITH_DB_UPDATES = True

        # This is my stupid loop to make sure we test everything both WITH and WITHOUT database updates.
        for i in range(0,2):

            # Get info about all the inverter -> gateway TLVs, loaded up with whether they should
            # be built in such a way that requires them to update the database
            all_types = self.get_all_dica_tlvs(TEST_WITH_DB_UPDATES)

            # Initialize lists that will be populated
            tlv_ids_list = []
            tlv_values_list = []
            database = {
                # INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME : "1.2.3.4",
                # INVERTERS_TABLE_STRING_ID_COLUMN_NAME : "SAMPLEsTRINGiD",
                # INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME: rand.randint(1,8)
            }


            print

            # Load up the lists with data that matches the kind of datatypes the gateway is
            # expecting from the inverter.
            for tlv_type in all_types:

                planned_response = self.get_value_for_this_datatype(tlv_type.type)

                # Preload the database with a datatype that will cause it to update or not,
                # whatever was specified by the test case TLV
                if tlv_type.update_possible:
                    if tlv_type.update_expected:
                        database[tlv_type.col_name] = str(self.get_value_for_this_datatype(tlv_type.type, planned_response))
                    else:
                        database[tlv_type.col_name] = str(planned_response)
                    print "TLV ID %d : %-20s %25s => %-25s " % (tlv_type.id, tlv_type.col_name, str(database[tlv_type.col_name]), str(planned_response))

                # Add the values to the list of TLVs 'received' from the inverter
                tlv_ids_list.append(tlv_type.id)
                tlv_values_list.append(planned_response)
            # end for

            print

            # Initialize the TLVManager with the values we prepared
            hey_man = man(tlv_ids_list, tlv_values_list, database, SAMPLE_REGIONAL_SETTINGS_TABLE, 25, "someIPaddress", "string1", 8)
            # Get the DB update queue for checking
            update_columns, update_arguments = hey_man.get_db_update_info()

            # Close the previous stream if it is open
            self.tearDown()
            # Print to file so I can see what is going on
            if(TEST_WITH_DB_UPDATES):
                self.file_init("with-updates")
            else:
                self.file_init("without-updates")

            self.out.write(hey_man.get_text_file_output())
            print hey_man.get_text_file_output()


            # Now we begin our assertions

            helpful_counter = 1

            # Make sure that the appropriate updates are queued for the database
            for entry in all_types:

                # Initialize an output
                if entry.update_possible and entry.update_expected:
                    warning = "DB update - YES"
                elif entry.update_possible:
                    warning = "DB update - NO"
                else:
                    warning = "(update impossible)"

                print "Starting test #%-2d on %4d - %-20s - (%s)" % (helpful_counter, entry.id, warning, entry.constant_name)
                helpful_counter += 1

                if entry.update_possible and entry.update_expected:
                    # make sure the column is in the update columns
                    assert(entry.col_name in update_columns)
                elif entry.update_possible:
                    # make sure that the column is NOT in the update columns
                    assert(entry.col_name not in update_columns)
                # end if
            # end for

            # Now toggle the instruction for updates
            TEST_WITH_DB_UPDATES = not TEST_WITH_DB_UPDATES
        # end for
        self.tearDown()

    def test_for_error_messages(self):
        ids = []
        vals = []
        db = {}
        string_id = DEFAULT_STRING_ID
        string_position = 20
        inverter_ip = ZERO_IP_ADDRESS
        my_translator = man(ids, vals, db, SAMPLE_REGIONAL_SETTINGS_TABLE, 2020, inverter_ip, string_id, string_position)

        self.file_init("string-error")

        text_out = my_translator.get_text_file_output()
        self.out.write(text_out)

        assert( text_out.count("WARNING") == 2)

    def test_new_toggle_tlvs(self):
        tlv_ids = [SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE, SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE]
        down = TestTLVManager.random_32bit_long()
        up = TestTLVManager.random_32bit_long()

        up_col = INVERTERS_TABLE_UP_LINK_TOGGLE_COUNT_COLUMN_NAME
        down_col = INVERTERS_TABLE_DOWN_LINK_TOGGLE_COUNT_COLUMN_NAME

        tlv_vals = [ down , up ]

        db_update = {
            down_col : TestTLVManager.get_value_for_this_datatype(TestTLVManager.LONG, down),
            up_col : TestTLVManager.get_value_for_this_datatype(TestTLVManager.LONG, up)
        }

        db_noupdate = {
            down_col : down,
            up_col : up
        }

        update_manager = man(tlv_ids, tlv_vals, db_update, SAMPLE_REGIONAL_SETTINGS_TABLE, 7,
                             TestTLVManager.SAMPLE_IP,
                             TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        no_update_manager = man(tlv_ids, tlv_vals, db_noupdate, SAMPLE_REGIONAL_SETTINGS_TABLE, 7,
                             TestTLVManager.SAMPLE_IP,
                             TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)


        # Print text file output so I can see what is happening on the inside

        self.file_init("new-tlvs-update")
        self.out.write("THE 2 NEW TLVS SHOULD UPDATE THE DATABASE" + LINE_TERMINATION)
        self.out.write("-----------------------------------------" + LINE_TERMINATION)
        self.out.write(update_manager.get_text_file_output())
        self.out.close()

        self.file_init("new-tlvs-noupdate")
        self.out.write("THE 2 NEW TLVS SHOULD NOT UPDATE THE DATABASE" + LINE_TERMINATION)
        self.out.write("---------------------------------------------" + LINE_TERMINATION)
        self.out.write(no_update_manager.get_text_file_output())
        self.out.close()


        # Now perform automated test to verify that the 2 new TLVs are being updated correctly

        update_cols_with_update, update_args_with_update = update_manager.get_db_update_info()
        update_cols_no_update, update_args_no_update = no_update_manager.get_db_update_info()

        assert up_col in update_cols_with_update
        assert down_col in update_cols_with_update

        assert up_col not in update_cols_no_update
        assert down_col not in update_cols_no_update

        print
        print
        print "If you see this, our new TLVs are success!"


    # Specific tests for debugging

    def test_feed_without_difference(self):
        tlv_ids = [SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE]
        tlv_values = [FEED_A_MULTICAST]

        db = {INVERTERS_TABLE_FEED_NAME_COLUMN_NAME : "A"}

        the_man = man(tlv_ids, tlv_values, db, SAMPLE_REGIONAL_SETTINGS_TABLE, 30, TestTLVManager.SAMPLE_IP,
                      TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        cols, args = the_man.get_db_update_info()

        self.file_init("feed-same")
        self.out.write("Text output for (inverter : A) (database : A)" + LINE_TERMINATION)
        self.out.write("---------------------------------------------" + LINE_TERMINATION)
        self.out.write(the_man.get_text_file_output())

        assert(INVERTERS_TABLE_FEED_NAME_COLUMN_NAME not in cols)

    def test_feed_with_difference(self):
        tlv_ids = [SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE]
        tlv_values = [FEED_A_MULTICAST]

        print tlv_ids
        print tlv_values

        db = {INVERTERS_TABLE_FEED_NAME_COLUMN_NAME : "B"}

        print db

        the_man = man(tlv_ids, tlv_values, db, SAMPLE_REGIONAL_SETTINGS_TABLE, 30, TestTLVManager.SAMPLE_IP,
                      TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)

        cols, args = the_man.get_db_update_info()

        print "Updates"
        print "--------------------------------"
        for i in range(0, len(cols)):
            print "%15%s : %s" % (cols[i], args[i])
        print "--------------------------------"

        self.file_init("feed-different")
        self.out.write("Text output for (inverter : A) (database : B)")
        self.out.write("---------------------------------------------")
        self.out.write(the_man.get_text_file_output())

        assert(INVERTERS_TABLE_FEED_NAME_COLUMN_NAME not in cols)

    def test_head_of_string(self):
        # Make sure that sending a TLV type that DICA is not meant to process,
        # doesn't break the Translator
        tlv_ids = [SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE]
        tlv_vals = ["headOfString"]

        the_man = man(tlv_ids, tlv_vals, {}, SAMPLE_REGIONAL_SETTINGS_TABLE, 7, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

        print "Error log string begins here."
        print the_man.get_error_log_string()
        print "Error log string ends here."

        assert str(SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE) in the_man.get_error_log_string()

    def test_watts_no_change(self):
        watts = INVERTERS_TABLE_WATTS_COLUMN_NAME

        tlv_ids = [SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE]
        tlv_vals_0 = [0]
        tlv_vals_1 = [1]
        db_0 = {watts : 0}
        db_1 = {watts : 1}



        man0 = man(tlv_ids, tlv_vals_0, db_0, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)
        man1 = man(tlv_ids, tlv_vals_1, db_1, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

        print bin(man0.ordered_values_plain[man.INDEXOF_DC_SM_INSTANTANEOUS_WATTS_TLV_TYPE])
        print bin(man1.ordered_values_plain[man.INDEXOF_DC_SM_INSTANTANEOUS_WATTS_TLV_TYPE])
        print bin(db_0[watts])
        print bin(db_1[watts])

        cols0, vals0 = man0.get_db_update_info()
        cols1, vals1 = man1.get_db_update_info()

        assert(watts not in cols0)
        assert(watts not in cols1)

    def test_battery_no_change(self):
        bat = INVERTERS_TABLE_BATTERY_COLUMN_NAME

        tlv_ids = [SG424_TO_GTW_BATTERY_U16_TLV_TYPE]
        vals0 = [0]
        vals1 = [1]
        db0 = { bat : 0 }
        db1 = { bat : 1 }

        man00 = man(tlv_ids, vals0, db0, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)
        man11 = man(tlv_ids, vals1, db1, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

        cols00, args00 = man00.get_db_update_info()
        cols11, args11 = man11.get_db_update_info()

        assert bat not in cols00
        assert bat not in cols11

        man01 = man(tlv_ids, vals1, db0, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)
        man10 = man(tlv_ids, vals0, db1, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

        cols01, args01 = man01.get_db_update_info()
        cols10, args10 = man10.get_db_update_info()

        assert bat in cols01
        assert bat in cols10

        vals3030 = [3030]
        db3030 = { bat : 3030 }

        man3030 = man(tlv_ids, vals3030, db3030, SAMPLE_REGIONAL_SETTINGS_TABLE, 5, self.SAMPLE_IP, self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)
        cols3030, args3030 = man3030.get_db_update_info()
        assert bat not in cols3030

    def test_number_of_resets_without_db_update(self):
        # Why is the number of resets updating when it is the same as what was in the database?
        # i found the answer ... i was comparing a string (casted from inverter TLV) with an int (loaded into the DB)

        # Arrange

        tlv_ids = [SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE]
        tlv_values = [4]
        db_row = { INVERTERS_TABLE_RESETS_COLUMN_NAME : 4 }


        # Act

        candy_man = man(tlv_ids, tlv_values, db_row, SAMPLE_REGIONAL_SETTINGS_TABLE, 10, TestTLVManager.SAMPLE_IP,
                     TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)
        update_cols, update_args = candy_man.get_db_update_info()


        # (Debug)

        print "DATABASE HELD:"
        for db_col_original in db_row:
            print "%20s : %s" % (db_col_original, db_row[db_col_original])

        print "UPDATING TO:"
        for i in range(0, len(update_cols)):
            print "%20s : %s" % (update_cols[i], update_args[i])


        # Assert

        assert(INVERTERS_TABLE_RESETS_COLUMN_NAME not in update_cols)

    def test_num_resets_with_db_update(self):
        # Arrange
        tlv_ids = [SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE]
        tlv_values = [4]
        db_row = { INVERTERS_TABLE_RESETS_COLUMN_NAME : 5 }

        # Act
        candy_man = man(tlv_ids, tlv_values, db_row, SAMPLE_REGIONAL_SETTINGS_TABLE, 10, TestTLVManager.SAMPLE_IP,
                     TestTLVManager.SAMPLE_STRING, TestTLVManager.SAMPLE_STRING_POSITION)
        update_cols, update_args = candy_man.get_db_update_info()

        # Assert
        assert(INVERTERS_TABLE_RESETS_COLUMN_NAME in update_cols)


    ''' Helpers for building fake TLVs as they might come from inverters '''

    @staticmethod
    def random_boolean(chance_of_true = 0.5):
        return rand.random() < chance_of_true

    @staticmethod
    def random_32bit_float():
        return rand.random() * 1000

    @staticmethod
    def random_32bit_long():
        return rand.randint(0,4294967295)

    @staticmethod
    def random_16bit_short():
        return rand.randint(0, 65535)

    @staticmethod
    def random_16bit_short_boolean():
        return rand.randint(0,1)

    @staticmethod
    def random_variable_length_string():
        STRING_CHARACTERS = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm12345678990.+-*/:;"
        length = rand.randint(3, 25)
        result = ''
        for i in range (0, length):
            result += STRING_CHARACTERS[rand.randint(0, len(STRING_CHARACTERS) - 1)]
        return result

    @staticmethod
    def get_value_for_this_datatype(dt, avoid = None):
        result = avoid
        counter = 0
        while(result == avoid):
            counter += 1
            if dt == TestTLVManager.LONG:
                result = TestTLVManager.random_32bit_long()
            elif dt == TestTLVManager.FLOAT:
                result = TestTLVManager.random_32bit_float()
            elif dt == TestTLVManager.SHORT:
                result = TestTLVManager.random_16bit_short()
            elif dt == TestTLVManager.BOOL:
                result = TestTLVManager.random_16bit_short_boolean()
            elif dt == TestTLVManager.STRING:
                result = TestTLVManager.random_variable_length_string()
            else:
                raise Exception("WHAT THE HEY? Use constants for your datatypes!")
        return result