import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from datetime import datetime, timedelta
from scheduler.recurrence import *


class RecurrenceTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_daily_recurrence(self):
        r = Recurrence(id=1, period=1, unit="day")

        start_date = datetime(year=2018, month=1, day=1, hour=10, minute=0)
        stop_date = datetime(year=2018, month=1, day=1, hour=11, minute=0)

        now = datetime(year=2018, month=1, day=2, hour=10, minute=0)
        recurring = r.recurring(start_date, now, stop_date)

        self.assertEquals(True, recurring, "Daily recurrence is happening now.")

    def test_weekly_recurrence(self):
        r = Recurrence(id=1, period=1, unit="week",
                       mo=1, tu=1, we=1, th=1, fr=1, sa=0, su=0)

        # 1/29/2018 is a monday
        start_date = datetime(year=2018, month=1, day=29, hour=10, minute=0)
        stop_date = datetime(year=2018, month=1, day=29, hour=11, minute=0)

        now = datetime(year=2018, month=1, day=30, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Tuesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Wednesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Thursday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Friday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Saturday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Sunday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on the following Monday.")

    def test_weekend_recurrence(self):
        r = Recurrence(id=1, period=1, unit="week",
                       mo=0, tu=0, we=0, th=0, fr=0, sa=1, su=1)

        # 1/29/2018 is a monday
        start_date = datetime(year=2018, month=1, day=29, hour=10, minute=0)
        stop_date = datetime(year=2018, month=1, day=29, hour=11, minute=0)

        now = datetime(year=2018, month=1, day=30, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Tuesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Wednesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Thursday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on Friday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Saturday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Weekly recurrence should occur on Sunday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Weekly recurrence should NOT occur on the following Monday.")

    def test_every_other_day_recurrence(self):
        r = Recurrence(id=1, period=2, unit="day")

        # 1/29/2018 is a monday
        start_date = datetime(year=2018, month=1, day=29, hour=10, minute=0)
        stop_date = datetime(year=2018, month=1, day=29, hour=11, minute=0)

        now = datetime(year=2018, month=1, day=30, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Daily recurrence should NOT occur on Tuesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Daily recurrence should occur on Wednesday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Daily recurrence should NOT occur on Thursday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Daily recurrence should occur on Friday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Daily recurrence should NOT occur on Saturday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Daily recurrence should occur on Sunday.")

        now += timedelta(days=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Daily recurrence should NOT occur on the following Monday.")

    def test_monthly_recurrence(self):
        r = Recurrence(id=1, period=1, unit="month")

        # 1/29/2018 is a monday
        start_date = datetime(year=2018, month=1, day=10, hour=10, minute=0)
        stop_date = datetime(year=2018, month=1, day=10, hour=11, minute=0)

        now = datetime(year=2018, month=2, day=10, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Monthly recurrence should occur.")

    def test_monthly_wrap_recurrence(self):
        r = Recurrence(id=1, period=2, unit="month")

        # 1/29/2018 is a monday
        start_date = datetime(year=2017, month=12, day=10, hour=10, minute=0)
        stop_date = start_date + timedelta(hours=1)

        now = datetime(year=2018, month=1, day=10, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Monthly recurrence should NOT occur.")

        now = datetime(year=2018, month=2, day=10, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Monthly recurrence should occur.")

    def test_yearly_recurrence(self):
        r = Recurrence(id=1, period=1, unit="year")

        # 1/29/2018 is a monday
        start_date = datetime(year=2018, month=1, day=10, hour=10, minute=0)
        stop_date  = datetime(year=2018, month=1, day=10, hour=11, minute=0)

        now = datetime(year=2018, month=2, day=10, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(False, recurring, "Monthly recurrence should NOT occur.")

        now = datetime(year=2019, month=1, day=10, hour=10, minute=1)
        recurring = r.recurring(start_date, now, stop_date)
        self.assertEquals(True, recurring, "Yearly recurrence should occur.")


if __name__ == '__main__':
    unittest.main()
