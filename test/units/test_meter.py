# -*- coding: utf-8 -*-
import os
import sys
import datetime

import unittest
from mock import patch, MagicMock, mock
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadRegistersResponseBase
from random import randint

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

from meter.meter import Meter
from meter.meter_daemon import MeterDaemon


class MockResponse(object):
    def __init__(self):
        pass

    def getRegister(self, index):
        return randint(0, 32768)


class MeterDaemonTests(unittest.TestCase):

    def setUp(self):
        self.meter = Meter.find_by(address='/dev/pcc')
        if not self.meter:
            self.meter = Meter(meter_name='pcc', meter_role='pcc', address='/dev/pcc')
            self.meter.save()
        self.meter.open()

    def tearDown(self):
        pass

    #@patch('meter.meter.ModbusSerialClient', autospec=True)
    @mock.patch('meter.meter.ModbusSerialClient.read_holding_registers')
    def test_read_active_registers(self, mock_read):
        mock_read.return_value = MockResponse()
        self.meter.read_all_registers()
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')


if __name__ == '__main__':
    unittest.main()
