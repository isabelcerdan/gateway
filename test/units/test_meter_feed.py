# -*- coding: utf-8 -*-
import os
import sys

import unittest
import datetime

#from random import randint

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

from power_regulator.meter_feed import MeterFeed

class MeterFeedTests(unittest.TestCase):

    def setUp(self):
        self.meter_feed = MeterFeed(
            power_regulator_id=1,
            meter_role='pcc',
            feed_name='Delta',
            energy_bucket=None,
            threshold=1.0,
            target_offset=1.0,
            Export_PCC_1_Delta=10.0,
            Export_Solar_1_Delta=10.0)
        now = datetime.datetime.now()
        timestamp = now - datetime.timedelta(seconds=10)
        for d in range(1, 10, 1):
            timestamp = timestamp + datetime.timedelta(seconds=1)
            f = float(d)
            self.meter_feed.load_reading(Export_PCC_1_Delta=f, Export_Solar_1_Delta=f, timestamp_pcc_1=timestamp)

    def tearDown(self):
        pass

    def test_min_meter_feed(self):
        min_power = self.meter_feed.get_min_power(10)
        self.assertEqual(min_power, -9.0)

    def test_max_meter_feed(self):
        max_power = self.meter_feed.get_max_power(10)
        self.assertEqual(max_power, -1.0)

    def test_avg_meter_feed(self):
        avg_power = self.meter_feed.get_avg_power(10)
        self.assertEqual(avg_power, -5.0)


if __name__ == '__main__':
    unittest.main()
