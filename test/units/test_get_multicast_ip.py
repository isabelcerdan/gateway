from unittest import TestCase
from gateway_utils import get_multicast_ip_from_feed_and_pcc as get_ip

class Test_get_multicast_ip_from_feed_and_pcc(TestCase):

    table = {
        "239.192.0.0" : ("ALL", False, False),
        "239.192.0.7" : ("ABC", False, False),
        "239.192.1.1" : ("A", True, False),
        "239.192.2.4" : ("C", False, True),
        "239.192.3.0" : ("ALL", True, True),
        "239.192.3.2" : ("B", True, True)
    }

    def test_with_full_parameters(self):
        for expected_ip, inputs in Test_get_multicast_ip_from_feed_and_pcc.table.iteritems():
            feed, pcc1, pcc2 = inputs
            actual_ip = get_ip(feed, pcc1, pcc2)
            assert(actual_ip == expected_ip)
            print "IP %s passed" % expected_ip