import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..','python')
sys.path.append(path)

import unittest
from energy_review.er_sync_daemon import *


class CatchUpReportTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_run_energy_review(self):
        instantiate_er_sync()


if __name__ == '__main__':
    unittest.main()
