# -*- coding: utf-8 -*-
import os
import sys

import unittest
from mock import patch, MagicMock, mock
from random import randint

from power_regulator.power_regulator import PowerRegulator
from power_regulator.solar_regulator import SolarRegulator
from power_regulator.meter_feed import MeterFeed

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)


class MockResponse(object):
    def __init__(self):
        pass

    def getRegister(self, index):
        return randint(0, 32768)


class SolarRegulatorTests(unittest.TestCase):

    def setUp(self):
        feed_name = 'Aggregate'
        self.power_regulator = PowerRegulator(feed_name=feed_name, enabled=1)

    def tearDown(self):
        pass

    # #@patch('meter.meter.ModbusSerialClient', autospec=True)
    # @mock.patch('meter.meter.ModbusSerialClient.read_holding_registers')
    # def test_read_active_registers(self, mock_read):
    #     mock_read.return_value = MockResponse()
    #     self.meter.read_all_registers()
    #     self.assertEqual(True, True, 'Meter daemon successfully read a register.')

    def test_regulate(self):
        self.power_regulator.regulate()


if __name__ == '__main__':
    unittest.main()
