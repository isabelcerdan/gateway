# -*- coding: utf-8 -*-
import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from mock import patch, MagicMock, mock
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadRegistersResponseBase
from lib.meter import Meter
from nxo_meter_daemon import MeterDaemon
from random import randint

class MockResponse(object):
    def __init__(self):
        pass

    def getRegister(self, index):
        return randint(0, 32768)

class MeterDaemonTests(unittest.TestCase):

    def setUp(self):
        meter = Meter.find_by_address('/dev/pcc')
        if not meter:
            meter = Meter(meter_name='pcc',meter_role='pcc',address='/dev/pcc')
            meter.save()
        self.meter_daemon = MeterDaemon(meter.address)

    def tearDown(self):
        pass

    #@patch.object(ModbusClient, 'read_holding_registers')
    @mock.patch('meter_daemon.ModbusClient.read_holding_registers')
    def test_read_active_registers(self, mock_read):
        mock_read.return_value = MockResponse()
        self.meter_daemon.read_active_registers()
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')

if __name__ == '__main__':
    unittest.main()
