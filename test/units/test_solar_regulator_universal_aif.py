import unittest
import python.power_regulator.solar_regulator as sr
import gateway_utils as gu
import lib.gateway_constants.DBConstants as db
import lib.gateway_constants.InverterConstants as inv
import tlv_utils as tu
import struct


TLV_LIST_WITHOUT_UAIF = [inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE, inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE]
TLV_LIST_WITH_UAIF = [inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE, inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE,
                        inv.GTW_TO_SG424_SW_PROFILE_U16_TLV_TYPE, inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_F32_TLV_TYPE,
                        inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_U16_TLV_TYPE,
                        inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_U16_TLV_TYPE]

def using_configured_region_name():
    sql = "UPDATE %s SET %s = \"%s\"" % \
          (db.DB_UNIVERSAL_AIF_TABLE, db.UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME, db.REGION_NAME_MFG120)
    gu.prepared_act_on_database(db.EXECUTE, sql, None)

def using_default_region_name():
    sql = "UPDATE %s SET %s = \"%s\"" % \
          (db.DB_UNIVERSAL_AIF_TABLE, db.UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME, db.REGION_NAME_UNDEFINED)
    gu.prepared_act_on_database(db.EXECUTE, sql, None)

def using_empty_db_pretest():
    sql = "delete from universal_aif;"
    gu.prepared_act_on_database(db.EXECUTE, sql, None)

def using_empty_db_posttest():
    sql = "INSERT INTO universal_aif (pf_enable) value (0);"
    gu.prepared_act_on_database(db.EXECUTE, sql, None)


def unpack_outgoing_tlv_packet(packet):
    tlv_rank = (inv.ANY_TLV_TYPE_FIELD_LENGTH + inv.ANY_TLV_DATA_LENGTH_FIELD_LENGTH + inv.MASTER_TLV_DATA_FIELD_LENGTH)
    master_tlv = packet[0:6]
    master_tlv_type, master_tlv_length, number_of_user_tlvs = struct.unpack('!HHH', master_tlv)

    tlv_ids_list = []
    tlv_values_list = []

    for i in range(number_of_user_tlvs):
        tlv_id, tlv_rank = gu.get_tlv_type(packet, tlv_rank)
        if tlv_id == inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE:
            tlv_value, tlv_rank = gu.get_tlv_data_32bit_unsigned_long(packet, tlv_rank)
            # parse as U32
        elif tlv_id == inv.GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_F32_TLV_TYPE or \
             tlv_id == inv.GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_F32_TLV_TYPE or \
             tlv_id == inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_F32_TLV_TYPE:
            tlv_value, tlv_rank = gu.get_tlv_data_32bit_float(packet, tlv_rank)
            # parse as F32
        else:
            tlv_value, tlv_rank = gu.get_tlv_data_16bit_unsigned_short(packet, tlv_rank)
            # parse as u16
        tlv_values_list.append(tlv_value)
        tlv_ids_list.append(tlv_id)

    for i in range(0, len(tlv_ids_list)):
        print "%d(%s)" % (tlv_ids_list[i], str(tlv_values_list[i]))

    return tlv_ids_list, tlv_values_list


class TestSolarRegulator(unittest.TestCase):
    """
    This test set uses complete path coverage for the 2nd half of to_tlv in
    solar_regulator.py's class SolarCommand.
    There are 4 paths:
     - 91-108 - where the ctag is not the initial startup value, so universal_aif info does
                not need to be multicasted to the inverters.
     - 91-95a-108 - where the correclation tag is the initial startup value, but there was
                no region name defined in the database
     - 91-95a-95b-108 - where the correlation tag is the initial startup value, but the
                region_name in universal_aif was the undefined region name.
     - 91-95-98-108 - where the correlation tag is the initial startup value, and the database
                had a non-default region name (indicating that it had been configured)

    For the sake of simplicity, line 91 will always be reached via the path:
     - 46-49-63-87-91
     where the solar command's attribute 'violating' is true.
    """

    def assert_matches(self, expected_list, found_list):
        for item in expected_list:
            if item not in found_list:
                self.fail("Expected %s in list but didn't find" % str(item))
        for item in found_list:
            if item not in expected_list:
                self.fail("Found unexpected %s in list" % str(item))

    def test_non_startup_ctag(self):
        # test path 91-108
        # expected TLV IDs: 102, 104
        using_configured_region_name()

        cmd = sr.SolarCommand(violating=True)
        tlv_packet = cmd.to_tlv(inv.CTAG_STARTUP_STANDBY_HEART_BEAT)

        ids, vals = unpack_outgoing_tlv_packet(tlv_packet)

        # assert inv.GTW_TO_SG424_SW_PROFILE_U16_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE not in ids
        #
        # assert inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE in ids

        self.assert_matches(TLV_LIST_WITHOUT_UAIF, ids)

    def test_no_region_in_db(self):
        # test path 91-95a-108
        # expected TLV IDs: 102, 104

        using_empty_db_pretest()

        cmd = sr.SolarCommand(violating=True)
        tlv_packet = cmd.to_tlv(inv.CTAG_INITIAL_STARTUP_VALUE)

        ids, vals = unpack_outgoing_tlv_packet(tlv_packet)
        # assert inv.GTW_TO_SG424_SW_PROFILE_U16_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE not in ids
        #
        # assert inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE in ids

        self.assert_matches(TLV_LIST_WITHOUT_UAIF, ids)


        using_empty_db_posttest()

    def test_db_region_is_default(self):
        # test path 91-95a-95b-108
        # Expected TLV IDs: 102, 104

        using_default_region_name()

        cmd = sr.SolarCommand(violating=True)
        tlv_packet = cmd.to_tlv(inv.CTAG_INITIAL_STARTUP_VALUE)

        ids, vals = unpack_outgoing_tlv_packet(tlv_packet)
        # assert inv.GTW_TO_SG424_SW_PROFILE_U16_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE not in ids
        # assert inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE in ids
        self.assert_matches(TLV_LIST_WITHOUT_UAIF, ids)

    def test_db_region_was_configured(self):
        # test path 91 - 95 - 98 - 108
        # Expected TLV IDs: 102, 104, 181, 150, 132, 133
        using_configured_region_name()

        cmd = sr.SolarCommand(violating=True)
        tlv_packet = cmd.to_tlv(inv.CTAG_INITIAL_STARTUP_VALUE)

        ids, vals = unpack_outgoing_tlv_packet(tlv_packet)

        # assert inv.GTW_TO_SG424_SW_PROFILE_U16_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE in ids
        # assert inv.GTW_TO_SG424_KEEP_ALIVE_CTAG_U32_TLV_TYPE in ids
        self.assert_matches(TLV_LIST_WITH_UAIF, ids)