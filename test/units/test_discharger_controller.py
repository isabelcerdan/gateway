# -*- coding: utf-8 -*-
import os
import sys

import unittest
from random import randint

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

from ess.apparent.discharger_controller import DischargerControllerDaemon


class DischargerControllerTests(unittest.TestCase):

    def setUp(self):
        self.controller = DischargerControllerDaemon()

    def tearDown(self):
        pass

    def test_launch_command(self):
        self.controller.launch_command('active_power', "0 aggregate 2.0")
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')


if __name__ == '__main__':
    unittest.main()
