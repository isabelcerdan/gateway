import os
import sys
import time
from collections import OrderedDict


path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from readings_logger.readings_logger import ReadingsLogger


class RecurrenceTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_readings_logger(self):

        readings_logger = ReadingsLogger(basename='pcc_readings_1',
                                         dir_path='/home/gateway/gateway/test',
                                         header=['Real Power A', 'Real Power B'],
                                         dumpPath='/home/gateway/gateway/test/test_reading.log')

        for j in range(1,10):
            for i in range(1,10):
                readings_logger.write_reading([(0.1 * i), (0.2 * j)])
                time.sleep(1)


        readings_logger = ReadingsLogger(basename='pcc_readings_2',
                                         dir_path='/home/dave/tmp',
                                         unit_of_time='minutes',
                                         dumpPath='/home/gateway/gateway/test/test_reading.log')


        for j in range(1, 10):
            for i in range(1, 13):
                od = OrderedDict()
                od["column A"] = (0.1 * i)
                od["column B"] = (0.2 * j)

                readings_logger.write_reading(od)
                time.sleep(1)


        self.assertEquals(True, True, "Csv Logger works.")

if __name__ == '__main__':
    unittest.main()
