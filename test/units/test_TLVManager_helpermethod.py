from unittest import TestCase
from gpts import TLVTranslator
from test_TLVManager import TestTLVManager as test
from lib.gateway_constants.DBConstants import INVERTERS_TABLE_BATTERY_COLUMN_NAME as battery



class TestTLVManagerHelperMethod(TestCase):

    SAMPLE_IP_ADDRESS = "192.168.10.22"
    SAMPLE_STRING = "Some String"
    SAMPLE_STRING_POSITION = 6
    SAMPLE_INVERTERS_AUDITED = 35


    def setUp(self):
        self.out = None
        self.tlv_ids = []
        self.tlv_vals = []
        self.db_row = {}

    def tearDown(self):
        if self.out != None:
            self.out.close()
            self.out = None

    def file_init(self, file_name):
        self.out = open("../out/helpertest-%s.out" % file_name, 'w')

    def set_up_response_all_update(self):
        self.tlv_ids, self.tlv_vals, self.db_row = test.build_tlv_lists_and_db_row(True)

    def set_up_response_all_noupdate(self):
        self.tlv_ids, self.tlv_vals, self.db_row = test.build_tlv_lists_and_db_row(False)

    def standard_helper_method_invocation(self):
        return TLVTranslator.verify_query_data_against_db(self.tlv_ids, self.tlv_vals, self.db_row, self.out,
                                                                 self.SAMPLE_INVERTERS_AUDITED, self.SAMPLE_IP_ADDRESS,
                                                                 self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

    def test_with_empty_parameters(self):
        tlv_ids = []
        tlv_vals = []
        database_row = {}
        self.file_init("empty")

        TLVTranslator.verify_query_data_against_db(tlv_ids, tlv_vals, database_row, self.out,
                                                          self.SAMPLE_INVERTERS_AUDITED, self.SAMPLE_IP_ADDRESS, self.SAMPLE_STRING,
                                                          self.SAMPLE_STRING_POSITION)

        print "If you're reading this, you made it through the script with no exceptions"

    def test_with_nonempty_parameters_and_empty_database(self):
        self.set_up_response_all_noupdate()
        self.db_row = {}
        self.file_init("emptyDatabase")

        # manager = self.standard_helper_method_invocation()
        #
        # print "Made it through with parameters but empty database"
        #
        #
        # print update_col
        # print update_arg
        #
        # assert( len(update_arg) == len(update_col))
        # assert( len(update_col) != 0)
        manager = TLVTranslator(self.tlv_ids, self.tlv_vals, self.db_row,
                                self.SAMPLE_INVERTERS_AUDITED, self.SAMPLE_IP_ADDRESS,
                                self.SAMPLE_STRING, self.SAMPLE_STRING_POSITION)

        update_col, update_arg = manager.get_db_update_info()

        assert( len(update_col) == len(update_arg))

        for i in range(0, len(update_col)):
            print "%15s : %s" % (update_col[i], update_arg[i])



        all_tlv_types = test.get_all_dica_tlvs(False)

        for tlv_type in all_tlv_types:
            if tlv_type.update_possible:
                assert tlv_type.col_name in update_col
            else:
                assert tlv_type.col_name not in update_col

        if battery in update_col:
            pass
            # index = update_col.index(battery)
            # del update_col[index]
            # del update_arg[index]
            # print "Removed %s from the update list." % battery

        self.out.write("Non empty TLV lists with empty database info")
        self.out.write("-------------------------------------------------------------")
        self.out.write(manager.get_text_file_output())

        manager.send_database_update(update_col, update_arg, manager.inverter_ip_address)
