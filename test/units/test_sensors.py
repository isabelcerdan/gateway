# -*- coding: utf-8 -*-
import os
import sys


path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

import unittest
from mock import patch, MagicMock
from lib.sensor import Sensor
import alarm

class SensorsTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_low_cpu_temp(self, mock_obj):
        reading = "CPU Temp         | -1.000     | degrees C  | nr    | 0.000     | 0.000     | 0.000     | 93.000    | 98.000    | 98.000"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_CPU_TEMP)
        self.assertEqual(a.raised, True, 'Low CPU temp reading triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_all_low_12V_status(self, mock_obj):
        reading = '''CPU Temp         | 48.000     | degrees C  | ok    | 0.000     | 0.000     | 0.000     | 93.000    | 98.000    | 98.000
System Temp      | 43.000     | degrees C  | ok    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000
Peripheral Temp  | 48.000     | degrees C  | ok    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000
DIMMA1 Temp      | 43.000     | degrees C  | ok    | 1.000     | 2.000     | 4.000     | 80.000    | 85.000    | 90.000
DIMMA2 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
DIMMB1 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
DIMMB2 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
FAN1             | na         |            | na    | na        | na        | na        | na        | na        | na
FAN2             | na         |            | na    | na        | na        | na        | na        | na        | na
FAN3             | na         |            | na    | na        | na        | na        | na        | na        | na
VCCP             | 0.810      | Volts      | ok    | 0.441     | 0.450     | 0.495     | 1.296     | 1.422     | 1.440
VDIMM            | 1.353      | Volts      | ok    | 1.092     | 1.119     | 1.200     | 1.641     | 1.722     | 1.749
12V              | 0.224      | Volts      | nr    | 10.144    | 10.272    | 10.784    | 12.960    | 13.280    | 13.408
5VCC             | 4.844      | Volts      | ok    | 4.246     | 4.298     | 4.480     | 5.390     | 5.546     | 5.598
3.3VCC           | 3.367      | Volts      | ok    | 2.789     | 2.823     | 2.959     | 3.554     | 3.656     | 3.690
VBAT             | 3.225      | Volts      | ok    | 2.400     | 2.490     | 2.595     | 3.495     | 3.600     | 3.690
5V Dual          | 4.865      | Volts      | ok    | 4.244     | 4.298     | 4.487     | 5.378     | 5.540     | 5.594
3.3V AUX         | 3.316      | Volts      | ok    | 2.789     | 2.823     | 2.959     | 3.554     | 3.656     | 3.690
Chassis Intru    | 0x0        | discrete   | 0x0000| na        | na        | na        | na        | na        | na
'''
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_12V_STATUS)
        self.assertEqual(a.raised, True, 'Low 12V status reading triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_chassis_intru(self, mock_obj):
        reading = "Chassis Intru    | 0x1        | discrete   | 0x0001| na        | na        | na        | na        | na        | na"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_CHASSIS_INTRU_STATUS)
        self.assertEqual(a.raised, True, 'Chassis intrusion triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_high_cpu_temp(self, mock_obj):
        reading = "CPU Temp         | 98.000     | degrees C  | nr    | 0.000     | 0.000     | 0.000     | 93.000    | 98.000    | 98.000"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_CPU_TEMP)
        self.assertEqual(a.raised, True, 'High CPU temp reading triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_low_system_temp(self, mock_obj):
        reading = "System Temp      | -8.000     | degrees C  | cr    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_SYSTEM_TEMP)
        self.assertEqual(a.raised, True, 'Low System temp triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_high_system_temp(self, mock_obj):
        reading = "System Temp      | 88.000     | degrees C  | cr    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_SYSTEM_TEMP)
        self.assertEqual(a.raised, True, 'High System temp reading triggers alarm.')



    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_low_12V_status(self, mock_obj):
        reading = "12V              | 0.224      | Volts      | nr    | 10.144    | 10.272    | 10.784    | 12.960    | 13.280    | 13.408"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_12V_STATUS)
        self.assertEqual(a.raised, True, 'Low 12V reading triggers alarm.')

    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_high_12V_status(self, mock_obj):
        reading = "12V              | 14.224      | Volts      | nc    | 10.144    | 10.272    | 10.784    | 12.960    | 13.280    | 13.408"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_12V_STATUS)
        self.assertEqual(a.raised, True, 'High 12V reading triggers alarm.')


    @patch.object(Sensor, 'read_ipmi_sensors')
    def test_fan_na_status(self, mock_obj):
        reading = "FAN1             | na         |            | na    | na        | na        | na        | na        | na        | na"
        mock_obj.return_value = reading
        Sensor.check_ipmi_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_FAN_STATUS)
        self.assertEqual(a.raised, False, 'na fan status does not trigger high fan status alarm.')
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_FAN_STATUS)
        self.assertEqual(a.raised, False, 'na fan status does not trigger low fan status alarm.')


    @patch.object(Sensor, 'read_core_sensors')
    def test_cpu_core_status(self, mock_obj):
        reading = "Core 0:       +99.0°C  (high = +98.0°C, crit = +98.0°C)"
        mock_obj.return_value = reading
        Sensor.check_core_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_CPU_CORE_TEMP)
        self.assertEqual(a.raised, False, 'High core CPU temperature reading triggers alarm.')

    @patch.object(Sensor, 'read_core_sensors')
    def test_cpu_core_status(self, mock_obj):
        reading = u'''coretemp-isa-0000
Adapter: ISA adapter
Core 0:       +99.0°C  (high = +98.0°C, crit = +98.0°C)
Core 1:       +49.0°C  (high = +98.0°C, crit = +98.0°C)
Core 2:       +48.0°C  (high = +98.0°C, crit = +98.0°C)
Core 3:        -1.0°C  (high = +98.0°C, crit = +98.0°C)        
        '''
        mock_obj.return_value = reading
        Sensor.check_core_sensors()
        a = alarm.Alarm.find(alarm.ALARM_ID_HIGH_CPU_CORE_TEMP)
        self.assertEqual(a.raised, True, 'High core CPU temperature reading triggers alarm.')
        a = alarm.Alarm.find(alarm.ALARM_ID_LOW_CPU_CORE_TEMP)
        self.assertEqual(a.raised, True, 'Low core CPU temperature reading triggers alarm.')


if __name__ == '__main__':
    unittest.main()
