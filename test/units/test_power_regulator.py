# -*- coding: utf-8 -*-
import os
import sys

import unittest

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)

from power_regulator.power_regulator import PowerRegulator


class PowerRegulatorTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_power_regulator(self):
        self.power_regulator = PowerRegulator.find_by(id=1)
        self.assertEqual(True, True, 'Meter daemon successfully read a register.')



if __name__ == '__main__':
    unittest.main()
