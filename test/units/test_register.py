import os
import sys

path = os.path.join(os.path.dirname(__file__), '..', '..','python')
sys.path.append(path)

import unittest
import struct
from meter_daemon import MeterRegisterIterator

class RegisterTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_convert_registers_to_float(self):
        #        12345678901234567890123456789012
        input = '00111110001000000000000000000000'
        #input = '01000001111101110000101000111101'

        upper = int(input[0:16], 2)
        lower = int(input[16:], 2)
        return_value = MeterRegisterIterator.convert2float(upper, lower)

        f = int(input, 2)
        float_value = struct.unpack('f', struct.pack('I', f))[0]

        #self.assertEqual(return_value, 0.15625, 'Our custom conversion to float should work.')
        self.assertEqual(float_value, 0.15625, 'Standard library conversion to float should work.')

if __name__ == '__main__':
    unittest.main()
