# -*- coding: utf-8 -*-
import os
import sys

import unittest
from mock import patch, MagicMock, mock

from power_regulator.battery_regulator import BatteryRegulator
from power_regulator.meter_feed import MeterFeed
from ess.ess_unit import EssUnit
from ess.ess_master_info import EssMasterInfo

path = os.path.join(os.path.dirname(__file__), '..', '..', 'python')
sys.path.append(path)


class BatteryRegulatorTests(unittest.TestCase):

    def setUp(self):

        self.meter_feed = MeterFeed(
            power_regulator_id=1,
            meter_role='pcc',
            feed_name='Aggregate',
            feed_size=10.0,
            energy_bucket=None
        )
        self.battery_regulator = BatteryRegulator(power_regulator_id=1)

    def tearDown(self):
        pass


    def my_active_power(self, requested_active_power):
        return float(requested_active_power)

    def test_regulate(self):
        self.meter_feed.current_limit = -5
        self.meter_feed.excess_power = 10.0

        ess_master_info = EssMasterInfo.find_by(ess_id=self.battery_regulator.ess_units[0].id)
        ess_master_info.save(unit_work_state='Running')
        self.battery_regulator.ess_units[0].save(charge_rate_limit=-15, strategy=EssUnit.BATTERY_STRATEGY_VARIABLE_CHARGE)

        ess_master_info = EssMasterInfo.find_by(ess_id=self.battery_regulator.ess_units[1].id)
        ess_master_info.save(unit_work_state='Running')
        self.battery_regulator.ess_units[1].save(charge_rate_limit=-10, strategy=EssUnit.BATTERY_STRATEGY_VARIABLE_CHARGE)

        #with mock.patch.object(EssUnit, 'set_active_power', side_effect=self.my_active_power) as m:
        with mock.patch.object(EssUnit, '_send_controller_cmd', return_value=True):
            self.battery_regulator.regulate(self.meter_feed)


if __name__ == '__main__':
    unittest.main()
