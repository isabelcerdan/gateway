<?php
include_once ('functions/session.php');

include_once ('functions/mysql_connect.php');
include_once ('functions/page_permissions_check.php');
include_once ('control/load_settings.php');
include_once ('control/get_system.php');

$showInverters = 'yes';
$showLogInfo = 'yes';
$showDownloadData = 'yes';

include_once ('control/get_page_section_perms.php');

for($i =0; $i < count($page_section_perm_id); $i++) {
    if(($section_name[$i] == 'showLogInfo') AND ($_SESSION["privileges"] >= $section_access_level[$i] ))   {
        $showLogInfo = 'no';
    }
    if(($section_name[$i] == 'showDownloadData') AND ($_SESSION["privileges"] >= $section_access_level[$i] ))   {
        $showDownloadData = 'no';
    }
    if(($section_name[$i] == 'showInverters') AND ($_SESSION["privileges"] >= $section_access_level[$i] ))   {
        $showInverters = 'no';
    }
}

$Feed_enable_all = $_GET['Feed_enable_all'];
$feed_enable_all = filter_var($feed_enable_all, FILTER_SANITIZE_NUMBER_INT);
$meter_enable_all = $_GET['Meter_enable_all'];
$meter_enable_all = filter_var($meter_enable_all, FILTER_SANITIZE_NUMBER_INT);


if($meter_enable_all == 0){
    $Meter_enable_text = "Enable All";
} elseif ($Feed_enable_all == 1) {
    $Meter_enable_text = 'Disable ALL';
}else {
    $Meter_enable_text = "Disable All";
    $meter_enable_all = 1;
}

function label_perspective($gateway_perspective) {
    echo $gateway_perspective == "production" ? "Production<br/>(+export/-import)" : "Consumption<br/>(+import/-export)";
}


?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Gateway GUI Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.css" >


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">



    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript" src="js/main.js?<?php echo rand(); ?>"></script>



    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .extraRightSpace {
            margin-right: 10px;
        }
        .formBoxSectionWhite {
            border: 1px solid #a1a1a1;
            padding: 10px;
            width: 98%;
            border-radius: 15px;
            margin: 10px;
            background-color: #fff;
        }

        p {
            line-height: normal;
            margin: 10px;
        }
        h4 {
            line-height: 150%;
        }
        button, input[type="button"] {

            text-shadow: none !important;
        }
        .lineHeight {
            height: 50px;
        }


        .smallerText {
            font-size: xx-small;
        }
        .bigButtonSize {
            width: 160px;
            margin: auto;
        }
        .popover.right {
            margin-left: 30px !important;
            margin-top: 10px;
        }

        .glyphicon.glyphicon-one-small-fine-dot {
            margin-bottom: -0.2em;
            overflow: hidden;
            margin-top: -9px;
        }
        .glyphicon.glyphicon-one-small-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }

        .glyphicon.glyphicon-one-fine-dot {
            margin-bottom: -.8em;
            overflow: hidden;
            margin-top: -15px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 3em;
        }
        .statusColor {

        }
        .rcorners1 {
            border-radius: 0px 300px 0px 0px;
            background: #5bc0de;
            padding: 0px;
            width: 50px;
            height: 50px;
            -webkit-transition: background-color 8s;
            -moz-transition: background-color 8s;
            -o-transition: background-color 8S;
            -ms-transition: background-color 8S;
            transition: background-color 8S;
        }
        .rcorners2 {
            border-radius: 300px 0px 0px 0px;
            background: #5bc0de;
            padding: 0px;
            width: 50px;
            height: 50px;
            -webkit-transition: background-color 8S;
            -moz-transition: background-color 8S;
            -o-transition: background-color 8S;
            -ms-transition: background-color 8S;
            transition: background-color 8S;
        }
        .rcorners3 {
            border-radius: 0px 0px 0px 300px;
            background: #5bc0de;
            padding: 0px;
            width: 50px;
            height: 50px;
            -webkit-transition: background-color 8S;
            -moz-transition: background-color 8S;
            -o-transition: background-color 8S;
            -ms-transition: background-color 8S;
            transition: background-color 8S;
        }
        .rcorners4 {
            border-radius: 0px 0px 300px 0px;
            background: #5bc0de;
            padding: 0px;
            width: 50px;
            height: 50px;
            -webkit-transition: background-color 8S;
            -moz-transition: background-color 8S;
            -o-transition: background-color 8S;
            -ms-transition: background-color 8S;
            transition: background-color 8S;
        }


         table  {
            border-collapse: collapse;
        }

        table.powerfactorTable, th.powerfactorTable, td.powerfactorTable {
            border: 1px solid #ddd;
        }
        .qText {
            width: 50px;
            float: left;
            text-align: center;
            margin-top: 5px;
            font-size: smaller;
        }
        .roman {
            margin-left: 3px;
        }
 
    </style>
    <script>
        var accessLevel = '<?php echo $_SESSION["privileges"]; ?>';
    </script>

    <script>

        $(document).ready(function() {

            $('[data-toggle="popover"]').popover();

        });
    </script>

</head>
<body>

<div class="container">
    
    <?php
    include_once ('header.php');
    // Menu Link //
    include_once ('menu.php'); // Get default data

    ?>
    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">


                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-md-6"><h1 style="padding-left: 30px;">Gateway GUI </h1></div>
                        <div class="col-md-6">
                            <div class="text-right" style="margin: 20px;">
                                <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                    <span class="glyphicon glyphicon-log-out"></span> Log out
                                </a>
                            </div></div>
                    </div>


                    <div style="margin-left: 30px; margin-right: 30px;">


                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Gateway Control (Start/Stop)</h3></div>
                                    <div class="panel-body">
                                        <p style="height: 40px"><strong>System State:</strong> <span id="auto_gateway_status_div"></span></p>
                                        <form id="startSystemGateway"  name="startSystemGateway" class="bigButtonSize" >
                                            <input type="hidden" id="system_enable"  name="system_enable" value="<?php echo $system_enable ?>">
                                            <input type="hidden" id="gateway_id" name="gateway_id" value="1">

                                            <p c>
                                                <button type="submit" class="btn btn-warning">
                                                    <span id="disableEnableGateway">Enable Gateway Now</span>
                                                </button>
                                            </p>

                                        </form>
                                            <p style="height: 25px"></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Power Factor</h3></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php
                                                if($FeedAEnabled == 'yes') {
                                                    echo '<p style="margin-left: 30px; "> Feed A</p>
                                                    <table class="powerfactorTable">
                                                        <tr >
                                                            <td class="powerfactorTable"><div class="rcorners2 qFeedA"><span class="roman">II</span><div class="2AText qText"></div></div></td>
                                                            <td class="powerfactorTable"><div class="rcorners1 qFeedA "><span class="roman">I</span><div class="1AText qText"></div></div> </td>
                                                        </tr>
                                                        <tr >
                                                            <td class="powerfactorTable"><div class="rcorners3 qFeedA"><span class="roman">III</span><div class="3AText qText"></div></div></td>
                                                            <td class="powerfactorTable"><div class="rcorners4 qFeedA"><span class="roman">IV</span><div class="4AText qText"></div></div></td>
                                                        </tr>
                                                    </table>';
                                                }
                                                ?>

                                               <div style="margin-left: 18px; margin-top: 5px" class="small"><strong>kvar: </strong><span id="kvar_A"></span></div>
                                            </div>

                                            <div class="col-md-4">
                                                <?php
                                                if($FeedBEnabled == 'yes') {
                                                    echo '<p style="margin-left: 30px; "> Feed B</p>
                                                    <table class="powerfactorTable">
                                                        <tr>
                                                            <td class="powerfactorTable"><div class="rcorners2 qFeedB"><span class="roman">II</span><div class="2BText qText"></div></div></td>
                                                            <td class="powerfactorTable"><div class="rcorners1 qFeedB"><span class="roman">I</span><div class="1BText qText"></div></div> </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="powerfactorTable"><div class="rcorners3 qFeedB"><span class="roman">III</span><div class="3BText qText"></div></div></td>
                                                            <td class="powerfactorTable"> <div class="rcorners4 qFeedB"><span class="roman">IV</span><div class="4BText qText"></div></div></td>
                                                        </tr>
                                                    </table>';
                                                }
                                                ?>
                                                <div style="margin-left: 18px; margin-top: 5px" class="small"><strong>kvar: </strong><span id="kvar_B"></span></div>
                                            </div>

                                            <div class="col-md-4">
                                                <?php
                                                if($FeedCEnabled == 'yes') {
                                                    echo '<p style="margin-left: 30px; "> Feed C</p>
                                                    <table class="powerfactorTable">
                                                        <tr>
                                                            <td class="powerfactorTable"><div class="rcorners2 qFeedC"><span class="roman">II</span><div class="2CText qText"></div></div></td>
                                                            <td class="powerfactorTable"><div class="rcorners1 qFeedC"><span class="roman">I</span><div class="1CText qText"></div></div> </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="powerfactorTable"><div class="rcorners3 qFeedC"><span class="roman">III</span><div class="3CqText qText"></div></div></td>
                                                            <td class="powerfactorTable"><div class="rcorners4 qFeedC"><span class="roman">IV</span><div class="4CText qText"></div></div></td>
                                                        </tr>
                                                    </table>';
                                                }
                                                ?>
                                                <div style="margin-left: 18px; margin-top: 5px" class="small"><strong>kvar: </strong><span id="kvar_C"></span></div>
                                            </div>

                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Power Regulation (Real Time)</h3>

                                        <form id="form_feed_all" name="form_feed_all" class="bigButtonSize">
                                            <input id="feed_enable_all_value" type="hidden" name="feed_enable_all_value" value="1">


                                            <p > <button type="submit" class="btn btn-warning"><span id="disableEnableAllFeeds">Enable All Regulators</span></button></p>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-11">

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr style="font-size: smaller">
                                            <th></th>

                                            <th colspan="<?php
                                            if($meterpcc2 == 'yes') {
                                                echo "3";
                                            }else echo "1";
                                            ?>" style="border: 2px solid #ddd;text-align: center"><span class="text-center">PCC Meter</span>

                                            </th>

                                            <th></th>





                                            <?php
                                            if ($meterpcc2 == 'yes') {
                                                echo"<th></th>";
                                            }
                                            if ($pcc_combo) {
                                                echo"<th></th>";
                                            }

                                            if($meterbattery == 'yes'){
                                                echo"<th></th>
                                                       <th></th>";
                                            }
                                            ?>


                                        </tr>
                                        <tr style="font-size: smaller">
                                            <th class='text-center'>Name
                                                <a  data-toggle="popover" title="Feed" data-content="This is the Feed/Phase that is connected directly to the PCC. Such as A, B or C.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </th>

                                            <th class='text-center'>Feed
                                                <a  data-toggle="popover" title="Feed" data-content="This is the Feed/Phase that is connected directly to the PCC. Such as A, B or C.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </th>

                                            <th style="border: 2px solid #ddd" class='text-center'>PCC 1 Net
                                                <a  data-toggle="popover" title="PCC 1" data-content="">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>

                                            </th>
                                            <?php
                                            if($meterpcc2 == 'yes') {
                                                ?>

                                                <th style="border: 2px solid #ddd" class='text-center'>PCC 2 Net <a data-toggle="popover"
                                                                                                title="PCC TARGET"
                                                                                                data-content="This is where we intend to hover our solar production.">
                                                        <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </th>

                                                <?php
                                                if ($pcc_combo) {
                                                    ?>
                                                <th style="border: 2px solid #ddd" class='text-center'>PCC Combo Net <a
                                                        data-toggle="popover" title="PCC Net Power"
                                                        data-content="This is where we show how much power is being imported or exported at the PCC. Exporting to the Grid is shown as a Positive Number and importing is shown as a Negative Number.">
                                                        <span class="glyphicon glyphicon-info-sign infoIcon"></span>
                                                </th>
                                                <?php
                                                }
                                                ?>
                                                <?php
                                            }
                                            ?>
                                            
                                            <th class='text-center'>Solar Meter
                                                <a  data-toggle="popover" title="Solar Power" data-content="Power Generation shows the amount of power being produced for this feed. This will be positive number when inverters are producing power, and a slight negative number at night when the inverters are not producing power.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </th>
                                            <th class='text-center'>Solar Regulation
                                                <a  data-toggle="popover" title="Curtail Set Point" data-content="Curtail Set Point shows the percentage of power that the Gateway is telling the Inverters to produce. 100% show that the inverters as much power as possible.">
                                                    <span class="glyphicon glyphicon-info-sign infoIcon" ></span>
                                            </th>
                                            <?php
                                            if($meterbattery == 'yes'){
                                                ?>
                                                <th class='text-center'>Battery Meter</th>
                                                <th class='text-center'>Battery Regulation</th>
                                            <?php
                                                }
                                                ?>
                                            
                                            <th>PID</th>

                                        </tr>
                                        </thead>
                                        <tbody id="auto_nxo_feed_status_div">
                                        <?php

                                        ?>
                                        </tbody>

                                    </table>

                                </div>

                            </div>
                            <div class="col-md-1">
                                <table class="table">
                                    <thead>
                                    <tr style="height: 35px"></tr>
                                    <tr style="font-size: smaller">

                                        <th>ON/Off</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php include_once("control/feed_button.php"); ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="text-center">NXO Details (Real Time)</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: smaller">
                                        <th></th>
                                        <?php if($metersolar == 'yes'): ?>
                                            <th colspan="1" style="border: 2px solid #ddd;text-align: center">
                                                <span class="text-center">SOLAR</span>
                                            </th>
                                        <?php endif; ?>
                                        <th colspan="2" style="border: 2px solid #ddd;text-align: center">
                                            <span class="text-center">PCC 1</span>
                                        </th>
                                        <?php
                                            if($meterpcc2 == 'yes') {
                                                echo '<th colspan="2" style="border: 2px solid #ddd;text-align: center"><span class="text-center">PCC 2</span></th>';
                                                if ($pcc_combo) {
                                                    echo '<th colspan="2" style="border: 2px solid #ddd;text-align: center"><span class="text-center">PCC COMBO</span></th>';
                                                }
                                            }
                                        ?>
                                    </tr>
                                    <tr style="font-size: smaller">
                                        <th>Feed</th>
                                        <th><?php label_perspective($gateway_perspective); ?></th>
                                        <th class='text-right'>Threshold</th>
                                        <th><?php label_perspective($gateway_perspective); ?></th>
                                        <?php if($meterpcc2 == 'yes'): ?>
                                            <th class='text-right'>Threshold</th>
                                            <th><?php label_perspective($gateway_perspective); ?></th>
                                            <?php if($pcc_combo): ?>
                                                <th class='text-right'>Threshold</th>
                                                <th><?php label_perspective($gateway_perspective); ?></th>
                                            <?php endif ?>
                                        <?php endif ?>
                                    </tr>
                                    </thead>
                                    <tbody id="auto_nxo_full_div">

                                    </tbody>
                                </table>
                            </div>


                        </div>



                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="text-center">Meter Status</h3>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: smaller">
                                        <th>Meter</th>
                                        <th>Type</th>
                                        <th>Comm</th>
                                        <th>IP Address</th>
                                        <th>Enabled</th>
                                        <th>Tunnel Open</th>
                                        <th>Started</th>
                                        <th>PID</th>
                                    </tr>
                                    </thead>
                                    <tbody id="auto_brainbox_status_div">

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-6">
                                <?php

                                if ($showInverters == 'yes'): ?>

                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Inverter Status  </h3></div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th  class='text-center'>Feed</th>
                                                    <th  class='text-center'>Inverters</th>


                                                </tr>
                                                </thead>
                                                <tbody id="inverter_status_div">

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Network Protector  </h3></div>
                                    <div class="panel-body">
                                <?php
                                // Display if Network Protector is enabled //
                                if ($netEnabled == 1): ?>



                                        <div class="table-responsive">
                                            <table class="table" class='text-center'>
                                                <thead>
                                                <tr>

                                                    <th class='text-center'>Protector</th>
                                                    <th >Status</th>


                                                </tr>
                                                </thead>
                                                <tbody id="network_protector_register_status_div">

                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>



                                                </tr>
                                                </thead>
                                                <tbody id="network_protector_status_div">

                                                </tbody>
                                            </table>
                                            <!--     <p class="text-center"><a href="control/reset_net_protector.php?delete=yes"> <button type="button" class="btn btn-warning">Reset Network Protector</button></a></p>-->



                                        </div>
                                        <form id="reset_network_protector" name="reset_network_protector" class="bigButtonSize" >
                                            <input type="hidden" id='reset_network_protector_value' name="delete" value="yes">


                                            <p class="text-center"> <button type="submit" class="btn btn-warning"><span id="reset_network_protector_button">Restart Network Protector</span></button></p>
                                        </form>


                                <?php
                                else:
                                echo "<p class='text-center'>Not Enabled</p>";

                                endif; ?>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-6">
                                <?php if ($showLogInfo == 'yes'): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Log info </h3></div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr style="font-size: smaller">
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <p class="text-center"> <button id="moreLog"type="button" class="btn btn-warning"> <span class="glyphicon glyphicon-chevron-up"></span> See More</button>   <button id="lessLog"type="button" class="btn btn-info"> <span class="glyphicon glyphicon-chevron-down"></span>  See less</button>
                                                    <a href="/control/getCSVLog.php"> <button id="lessLog"type="button" class="btn btn-success">Download CSV</button> </a>
                                                </p>
                                                <tbody id="auto_log_status_div">

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php

                                    if ($showDownloadData == 'yes'): ?>

                                    <div class="panel panel-default">

                                        <div class="panel-heading"><h3 class="text-center">Download Data</h3></div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <?php

                                                $path    = 'readings_data/';
                                                $files = array_diff(scandir($path), array('.', '..'));
                                                

                                                 foreach ($files as $file) {

                                                    if (strpos($file, '.zip') !== false) {
                                                        echo "Download data on the day of: <a href='/readings_data/$file'>" .  str_replace(".zip","",$file) . "</a><br>";
                                                    }
}
                                                ?>

                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                            </div>





                        </div>




                    </div>

                </div>
            </div>

        </div>
    </div>
        <?php    include_once ('footer.php'); ?>
</div>
</body>
</html>
