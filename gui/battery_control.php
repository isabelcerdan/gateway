<?php
include_once ('functions/session.php');
include_once ('functions/mysql_connect.php');

$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);
include_once ('control/get_battery.php');



if($model_id == 4){
    $stringsBMUs = 'bmu';
}else {
    $stringsBMUs = 'strings';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Battery Control</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        button, input[type="button"] {

            text-shadow: none !important;
        }
        #stopSystem {
            display: none;
        }

        .smallerText {
            font-size: xx-small;
        }
        #Update_Success {
            display: none;
        }
        .progress {
            height: 60px;

        }
        .progress-bar {
            font-size: 20px !important;
            padding-top: 15px;1
        }
        .green {
            color: green;
        }
        .red {
            color: red;
        }
        .charging {
            color: green;
        }
        .discharging {
            color: red;
        }
        .row.form-group  {
            margin-bottom: 5px;
        }
        .hrRule {
            margin-top: 1px; margin-bottom: 5px;
        }
    </style>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript"  src="js/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript"  src="js/featherlight.min.js" charset="utf-8"></script>

    <script type="text/javascript">


        $(document).ready(function (){

            var strategy;
            $('.string_now').on('click', function() {

                StringId = $(this).attr("id");

                $.featherlight({iframe: '/control/read_battery_string.php?ess_id=<?php echo $ess_id; ?>&stringId='+StringId+'&getStringStatus=yes', iframeMaxWidth: '100%', iframeWidth: 800,iframeHeight: 800});

            });

            $('.sendCommand').on('click', function() {
                //console.log('click me');
                var r = confirm("This action my take up to 30 seconds. Turning On or Off the battery can take several minutes. Do you want to continue?");
                if (r == true) {
                    var command = '';
                    if (this.id == 'stop') {
                        command = 0;
                    }
                    if (this.id == 'start') {
                        command = 1;
                    }
                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?ess_id=<?php echo $ess_id; ?>&startStop=' + command,
                        success: function (data) {
                            console.log('data:' + data);

                            if(data == 'no_feed'){
                                alert('You must create a feed before battery operation');
                            }

                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    return false;
                }
            });
            $('.setACTIVE_POWER').on('click', function() {
                charge = ' Charging ';
                value = $("#ACTIVE_POWER").val();
                if(value > 0){
                    charge = ' Discharging ';
                }
                var r = confirm("Are you sure you want to set Active Power to " + value + ' ' + charge + '?');
                if (r == true) {

                    //console.log('ACTIVE_POWER:'+ value );

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?ess_id=<?php echo $ess_id; ?>&activePower=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                    $("#ACTIVE_POWER").val('');
                    return false;
                }
            });

            $('.setCharge_rate_limit').on('click', function() {
                value = $("#charge_rate_limit").val();

                if(value < 0){
                    var r = confirm("Are you sure you want to set Charge Limit to:  " + value);
                    if (r == true) {

                        //alert(value);
                        $.ajax({
                            type: "POST",
                            url: 'control/form_battery_limits.php?ess_id=<?php echo $ess_id; ?>&charge_rate_limit=' + value,
                            success: function (data) {
                                console.log('data:' + data);
                            },
                            error: function (jqXHR, text, error) {
                                console.log('error:');
                            }
                        });

                    }
                }else alert('you need to enter a negative number');
                $("#charge_rate_limit").val('');
                return false;
            });

            $('.setDischarge_rate_limit').on('click', function() {
                value = $("#discharge_rate_limit").val();
                if(value > 0){
                    var r = confirm("Are you sure you want to set DischargeLimit to:  " + value);
                    if (r == true) {

                        //alert(value);
                        $.ajax({
                            type: "POST",
                            url: 'control/form_battery_limits.php?ess_id=<?php echo $ess_id; ?>&discharge_rate_limit=' + value,
                            success: function (data) {
                                console.log('data:' + data);
                            },
                            error: function (jqXHR, text, error) {
                                console.log('error:');
                            }
                        });
                        $(".discharge_rate_limit").val('');

                    }
                }else alert('you need to enter a postive number');
                $("#discharge_rate_limit").val('');
                return false;
            });

            //
            $('.setREACTIVE_POWER').on('click', function() {
                var r = confirm("This action my take up to 30 seconds. Are you sure you want to set Reactive Power?");
                if (r == true) {
                    value = $("#REACTIVE_POWER").val();
                    //console.log('ACTIVE_POWER:'+ value );
                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?ess_id=<?php echo $ess_id; ?>&reactivePower=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#REACTIVE_POWER").val('');
                return false;

            });

            $('.setPowerRiseRime').on('click', function() {
                var r = confirm("This action my take up to 30 seconds. Are you sure you want to set Set Power Rise Time?");
                if (r == true) {

                    value = $("#PowerRiseTime").val();

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?ess_id=<?php echo $ess_id; ?>&powerRiseRime=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#PowerRiseTime").val('');
                return false;

            });

            $('.setPowerDecreaseTime').on('click', function() {
                var r = confirm("This action my take up to 30 seconds. Are you sure you want to set Set Power Decrease Time? ");
                if (r == true) {
                    value = $("#PowerDecreaseTime").val();

                    $.ajax({
                        type: "POST",
                        url: 'control/set_battery_run_stop.php?ess_id=<?php echo $ess_id; ?>&powerDecreaseTime=yes&level=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                $("#PowerDecreaseTime").val('');
                return false;

            });

            $('.setSOC_charge_limit').on('click', function() {
                value = $("#SOC_charge_limit").val();
                var r = confirm("Are you sure you want to set the Max SOC Charge Limit to:  "+value);
                if (r == true) {

                    //alert(value);
                    $.ajax({
                        type: "POST",
                        url: 'control/form_battery_limits.php?ess_id=<?php echo $ess_id; ?>&SOC_charge_limit=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    $("#PowerDecreaseTime").val('');
                    return false;
                }
            });

            $('.setSOC_discharge_limit').on('click', function() {

                value = $("#SOC_discharge_limit").val();
                var r = confirm("Are you sure you want to set the Min SOC Discharge Limit to:  "+value);
                if (r == true) {
                    alert(value);
                    $.ajax({
                        type: "POST",
                        url: 'control/form_battery_limits.php?ess_id=<?php echo $ess_id; ?>&SOC_discharge_limit=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    $("#PowerDecreaseTime").val('');
                    return false;
                }
            });

            $('.setLimitSetPoint').on('click', function() {

                value = $("#LimitSetPoint").val();
                var r = confirm("Are you sure you want to set the Target Threshold to:  "+value);
                if (r == true) {
                    //alert(value);
                    $.ajax({
                        type: "POST",
                        url: 'control/form_limit_set_point.php?ess_id=<?php echo $ess_id; ?>&LimitSetPoint=' + value,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    // $("#PowerDecreaseTime").val('');
                    return false;
                }
            });

            // Get last trategy' value before change //
            $('#strategy').on('focusin', function(){
                strategy = $("#strategy").val();
            });


            $('#strategy').on('change', function() {
                strategyValue = $("#strategy").val();
                var r = confirm("Are you sure you want to change the battery strategy to: "+strategyValue);
                if (r == true) {
                    $.ajax({
                        type: "POST",
                        url: 'control/form_battery_strategy.php?ess_id=<?php echo $ess_id; ?>&strategy=' + strategyValue,
                        success: function (data) {
                            console.log('data:' + data);
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                    if(strategyValue == 'setpoint'){
                        $('#setpointView').show();
                        // $('#controls_auto').hide();
                    }
                    if(strategyValue == 'fixed_charge'){
                        $('#setFixedChargeRate').show();
                        // $('#controls_auto').hide();
                    }
                    if(strategyValue == 'fixed_discharge'){
                        $('#setFixedDischargeRate').show();
                        // $('#controls_auto').hide();
                    }
                    return false;
                }
                $("#strategy").val(strategy);
                return false;
            });


            $('.controller_control').on('click', function() {
                var enable = $(this).attr('id');
                //alert('!ok:'+enable);
                $.ajax({
                    type: "POST",
                    url: 'control/form_battery_enable.php?ess_id=<?php echo $ess_id; ?>&monitor_control='+ enable,
                    success: function (data) {
                        if(data == 'cess_control=1'){
                            $(".controller_control").attr("id", "0");
                            $(".controller_control").html('Stop Controller');
                        }
                        if(data == 'cess_control=0'){
                            $(".controller_control").attr("id", "1");
                            $(".controller_control").html('Start Controller');
                        }
                        //
                    },
                    error: function (jqXHR, text, error) {
                        console.log('error:');
                    }
                });
                return false;

            });


            $('.battery_mode_control').click(function() {
              var mode =  $('input[name=battery_mode]:checked').val();
                var r = confirm("Are you sure you want to change the battery run mode to: "+mode);
                if (r == true) {

                    $.ajax({
                        type: "POST",
                        url: 'control/form_battery_mode.php?ess_id=<?php echo $ess_id; ?>&battery_mode=' + mode,
                        success: function (data) {
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });

                }
                return false;
            });

            $('#clear-fault').click(function() {
                var r = confirm("Clear the battery \"Fault\" state? ");
                if (r == true) {
                    $.ajax({
                        type: "POST",
                        url: 'control/form_battery_clear_fault.php',
                        success: function (data) {
                        },
                        error: function (jqXHR, text, error) {
                            console.log('error:');
                        }
                    });
                }
                return false;
            });

            setInterval(auto_gateway_status,2000);
            auto_gateway_status();
            function auto_gateway_status() {


                $.getJSON("control/get_battery_command_data.php?ess_id=<?php echo $ess_id; ?>", function(result){
                    $.each(result, function(i, field) {


                        $(".active_power_cmd").html(field.active_power_cmd);
                        $(".reactive_power_cmd").html(field.reactive_power_cmd);
                        $("#unit_work_mode").html(field.unit_work_mode);
                        $(".unit_work_state").html(field.unit_work_state);
                        $("#unit_ctrl_mode").html(field.unit_ctrl_mode);
                        $(".soc_charge_limit").html(field.soc_charge_limit);
                        $(".soc_discharge_limit").html(field.soc_discharge_limit);
                        $("#power_factor").html(field.power_factor);
                        $("#reconnect_state").html(field.reconnect_state);
                        $("#work_state_control_cmd").html(field.work_state_control_cmd);
                        $("#battery_string_running").html(field.battery_string_running);
                        $("#battery_string_enabled").html(field.battery_string_enabled);
                        $("#battery_string_isolated").html(field.battery_string_isolated);
                        $("#battery_stack_voltage").html(field.battery_stack_voltage);
                        $("#battery_stack_current").html(field.battery_stack_current);
                        $("#battery_stack_power").html(field.battery_stack_power);
                        $(".battery_stack_soc").html(field.battery_stack_soc);
                        $("#battery_stack_soh").html(field.battery_stack_soh);

                        $("#battery_max_cell_voltage").html(field.battery_max_cell_voltage);
                        $("#battery_min_cell_voltage").html(field.battery_min_cell_voltage);
                        $("#dc_voltage").html(field.dc_voltage);
                        $("#dc_current").html(field.dc_current);
                        $("#dc_power").html(field.dc_power);
                        $("#voltage_control_cmd").html(field.voltage_control_cmd);
                        $("#frequency_control_cmd").html(field.frequency_control_cmd);
                        // removed decimal places //
                        $(".threshold").html(Math.round(field.threshold));
                        $(".Export_PCC_1_Delta").html(field.Export_PCC_1_Delta);
                        $(".Export_Battery_1_Delta").html(field.Export_Battery_1_Delta);

                        $(".charge_rate_limit").html(field.charge_rate_limit);
                        $(".discharge_rate_limit").html(field.discharge_rate_limit);

                        $(".strategy").html(field.strategy);

                        $(".battery_string_1_soc").html(Math.round(field.battery_string_1_soc));
                        $(".battery_string_2_soc").html(Math.round(field.battery_string_2_soc));
                        $(".battery_string_3_soc").html(Math.round(field.battery_string_3_soc));
                        $(".battery_string_4_soc").html(Math.round(field.battery_string_4_soc));
                        $(".battery_string_5_soc").html(Math.round(field.battery_string_5_soc));
                        $(".battery_string_6_soc").html(Math.round(field.battery_string_6_soc));
                        $(".battery_string_7_soc").html(Math.round(field.battery_string_7_soc));
                        $(".battery_string_8_soc").html(Math.round(field.battery_string_8_soc));


                        //rue_power'=> $true_power,
                        $('.strategySelect option[value=' + field.strategy + ']').attr('selected', 'selected');


                        $("#true_power").html(field.true_power);
                        $("#apparent_power").html(field.apparent_power);

                        $("#capability").html(field.capability);
                        $("#capability_reason").html(field.capability_reason);
                        $("#max_allowable_charge_active_power").html(field.max_allowable_charge_active_power);
                        $("#max_allowable_discharge_active_power").html(field.max_allowable_discharge_active_power);



                        $("#frequency").html(field.frequency);
                        $("#timestamp").html(field.timestamp);

                        // give you user feedback when system is starting
                        if(field.startup_delay_active == 1){
                           $("#startup_delay").html('Initializing Chargers...<br>This may take up to 30 seconds');
                        }else {
                           $("#startup_delay").html('');

                        }

                        // Show Battery running state above progress bar //
                        // console.log('active_power_cmd:' +  field.active_power_cmd);
                        if (field.active_power_cmd == 0) {
                            $(".batteryChargeState").removeClass('discharging');
                            $(".batteryChargeState").removeClass('charging');
                            $(".batteryChargeState").html('Idle');
                        }
                        else {
                            if (field.active_power_cmd < 0) {
                                $(".batteryChargeState").removeClass('discharging');
                                $(".batteryChargeState").addClass('charging');
                                $(".batteryChargeState").html('Charging');
                            } else {
                                $(".batteryChargeState").removeClass('charging');
                                $(".batteryChargeState").addClass('discharging');
                                $(".batteryChargeState").html('Discharging');
                            }
                        }

                        if (field.unit_work_state == "Stopped") {
                            $(".unit_work_state").removeClass("red");
                            $(".unit_work_state").removeClass("green");
                            $("#clear-fault").hide();
                        } else if (field.unit_work_state == "Runnning") {
                            $(".unit_work_state").removeClass("red");
                            $(".unit_work_state").addClass("green");
                            $("#clear-fault").hide();
                        } else if (field.unit_work_state == "Fault") {
                            $(".unit_work_state").addClass("red");
                            $(".unit_work_state").removeClass("green");
                            $(".unit_work_state").html(field.unit_work_state);
                            $("#clear-fault").show();
                        }

                        //progress-bar progress-bar-success progress-bar-striped
                        $(".progress-bar").css({'width': field.battery_stack_soc +'%'});

                        // Start Stop button updates //

                        // Show enabled state //
                        if(field.mode  == 'manual') {
                            $("#battery_mode_manual").prop('checked',true);
                            $('#controls_manual').show();
                            $('#controls_auto').hide();

                        }else{
                            $("#battery_mode_auto").prop('checked',true);
                           $('#controls_manual').hide();
                            $('#controls_auto').show();
                        }

                        if(field.unit_work_state  == 'Running') {
                            $(".sendCommand").attr("id", "stop");
                            $(".sendCommand").html('Stop');
                        }else{
                            $(".sendCommand").attr("id", "start");
                            $(".sendCommand").html('Start');
                        }
                       // $('input#SOC_charge_limit').val(field.soc_charge_limit);
                       // $('input#SOC_discharge_limit').val(field.soc_discharge_limit);
                       // $('input#LimitSetPoint').val(field.threshold);
                        $('#controls_manual').show();
                    });
                });

            }
            //$("#battery_mode_manual").attr('checked', true);


        });

    </script>

</head
<body>
<?php
include_once ('functions/mysql_connect.php');
// Init meter var


?>
<div class="container">

    <?php
    // Menu Link //
    include_once ('header.php');
    include_once ('menu.php'); // Get default data

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">Battery Control</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>

                        </div></div>
                </div>

                    <div style="padding: 30px;">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Battery Charge State</h3></div>
                                    <div class="panel-body">
                                        <h4>Battery State:
                                            <span class='batteryChargeState'></span>


                                        </h4>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-1"></div>

                                            <div class="col-md-10">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $battery_stack_soc; ?>%">
                                                        <span class='battery_stack_soc'><?php echo $battery_stack_soc; ?></span>%
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-1"></div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center">Commands</h3></div>
                                    <div class="panel-body">



                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Control Mode:</strong></strong>
                                            </div>

                                            <div class="col-sm-7">
                                                <input type="radio" name="battery_mode" class='battery_mode_control' value="manual" id="battery_mode_manual">  <small>Manual Control of Battery   </small><br>
                                                <input type="radio" name="battery_mode" class='battery_mode_control' value="auto" id="battery_mode_auto"> <small> Auto Control of Battery (Auto Target)   </small>

                                            </div>

                                        </div>
                                        <hr class='hrRule' >
                                    <div id="controls_manual">
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong> Active Power:</strong></strong><br><small>(+) Set active power discharge <br>(-) Set active power charge</small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="ACTIVE_POWER"
                                                       name="ACTIVE_POWER" maxlength="5"
                                                       size="4"> <button class="setACTIVE_POWER" id="start" type="button"
                                                        class="btn btn-outline-secondary">Set
                                                </button> Active: <span class="active_power_cmd"><?php //echo $active_power_cmd; ?></span> KW
                                            </div>

                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                            <strong>Reactive Power:</strong><br>
                                                <small>(+) Provide Reactive, <br>(-) Absorb Reactive</small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="REACTIVE_POWER" name="REACTIVE_POWER" maxlength="5" size="4"><button class="setREACTIVE_POWER" id="start" type="button" class="btn btn-outline-secondary">Set</button>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >
                                    </div>
                                    <div id="controls_auto">

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Auto Strategy:</strong><br>

                                            </div>
                                            <div class="col-sm-7">

                                                <select name="strategy" id="strategy" class="form-control strategySelect">
                                                    <option value='none'>None</option>
                                                    <option value='setpoint'>Setpoint</option>
                                                    <option value='variable_charge'>Variable Charge</option>
                                                    <option value='variable_discharge'>Variable Discharge</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div  id="setpointView">
                                            <hr class='hrRule' >
                                            <div class="form-group row ">

                                                <div class="col-sm-5">
                                                    <strong>Target Threshold:</strong><br>
                                                    <small>(+) Export (-) Import</small>
                                                </div>
                                                <div class="col-sm-7">

                                                    <input type="text" id="LimitSetPoint" name="LimitSetPoint" maxlength="5" size="4"><button class="setLimitSetPoint" id="start" type="button" class="btn btn-outline-secondary">Set</button> Active: <span class='threshold'> <?php echo $threshold; ?></span> KW
                                                    <br><small><i>Enter whole numbers</i></small>
                                                </div>

                                            </div>
                                        </div>



                                     </div>


                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-5 col-form-label">Turn on/off the battery:</label>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($unit_work_state == 'Starting') {
                                                    echo ' <a><button class="sendCommand btn btn-outline-secondary" id="stop" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Stop</button></a>';
                                                } else {
                                                    echo '<a><button class="sendCommand btn btn-outline-secondary" id="start" type="button" class="btn btn-outline-secondary"  data-target="#myModal">Start</button></a>';
                                                }
                                                ?>
                                                <div id="startup_delay" style="color:red; margin-top: 5px"> </div>
                                            </div>
                                        </div>
                                        <hr class='hrRule' >
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">System Status</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery SOC:</strong></div>
                                            <div class="col-sm-7"><span class='battery_stack_soc'><?php echo $battery_stack_soc; ?></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Target Threshold:</strong><br>
                                                <small>(+) Export  (-) Import</small></div>
                                            <div class="col-sm-7"><span class='threshold'><?php echo $threshold; ?></span> KW</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>PCC KW:</strong><br>
                                                <small>(+) Export  (-) Import</small></div>
                                            <div class="col-sm-7"><span class='Export_PCC_1_Delta'><?php echo $Export_PCC_1_Delta; ?></span> KW</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery KW:</strong><br><small>(+) Discharge/export<br> (-) Charge/import</small></div>
                                            <div class="col-sm-7"><span class='Export_Battery_1_Delta'><?php echo $Export_Battery_1_Delta; ?></span> KW <span class='batteryChargeState'></span></div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Active Power Setting:</strong><br><small>(+) Discharge/export<br> (-) Charge/import</small></div>
                                            <div class="col-sm-7"><span class="active_power_cmd"><?php //echo $active_power_cmd; ?></span> KW <span class='batteryChargeState'></span></div>
                                        </div>


                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Power Factor:</strong></div>
                                            <div class="col-sm-7"><span id="power_factor"><?php //echo $power_factor; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Reactive Power:</strong></div>
                                            <div class="col-sm-7"><span class="reactive_power_cmd"><?php //echo $power_factor; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Work State:</strong></div>
                                            <div class="col-sm-7">
                                                <span class="unit_work_state"><?php //echo $unit_work_state; ?></span>
                                                <span style="margin-left: 60px;">
                                                    <button style="display: none" class="btn btn-outline-secondary" id="clear-fault" type="button" data-target="#myModal">Clear</button>
                                                </span>
                                            </div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Battery Ctrl Mode:</strong></div>
                                            <div class="col-sm-7"><span id="unit_ctrl_mode"><?php //echo $unit_ctrl_mode; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Max SOC Charge Limit:</strong></div>
                                            <div class="col-sm-7"><span class="soc_charge_limit"><?php //echo $soc_charge_limit; ?></span>%</div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Min SOC Discharge Limit:</strong></div>
                                            <div class="col-sm-7"><span class="soc_discharge_limit"><?php //echo $soc_discharge_limit; ?></span>%</div>
                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Charge Rate Limit:</strong></div>
                                            <div class="col-sm-7"><span class="charge_rate_limit"><?php //echo $soc_charge_rate_limit; ?></span> KW</div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Capability:</strong></div>
                                            <div class="col-sm-7"><span id="capability"><?php //echo $soc_discharge_rate_limit; ?></span></div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Capability Reason:</strong></div>
                                            <div class="col-sm-7"><span id="capability_reason"><?php //echo $soc_discharge_rate_limit; ?></span> </div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Max Allowable Charge: </strong></div>
                                            <div class="col-sm-7"><span id="max_allowable_charge_active_power"><?php //echo $soc_discharge_rate_limit; ?></span> </div>
                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong>Max Allowable Discharge:</strong></div>
                                            <div class="col-sm-7"><span id="#max_allowable_discharge_active_power"><?php //echo $soc_discharge_rate_limit; ?></span></div>
                                        </div>
                                  
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading"><h3 class="text-center">Settings</h3></div>
                                    <div class="panel-body">
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Max SOC Charge Limit:</strong><br>
                                                <small>The level at which the battery is shutdown. </small>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" id="SOC_charge_limit" name="SOC_charge_limit" maxlength="5" size="4" value=""><button class="setSOC_charge_limit" id="start" type="button" class="btn btn-outline-secondary">Set</button> Active: <span class="soc_charge_limit"></span>%
                                                <br><small><i>Enter whole numbers</i></small>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Min SOC Discharge Limit:</strong><br>
                                                <small>The level at which the battery is shutdown. <i>Enter whole numbers</i></small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="SOC_discharge_limit" name="SOC_discharge_limit" maxlength="5" size="4"><button class="setSOC_discharge_limit" id="start" type="button" class="btn btn-outline-secondary">Set</button> Active: <span class="soc_discharge_limit"><?php //echo $soc_discharge_limit; ?></span>%
                                                <br><small><i>Enter whole numbers</i></small>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >
                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Charge Rate Limit:</strong><br>
                                                <small><i>Enter whole numbers</i></small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="charge_rate_limit" name="charge_rate_limit" maxlength="5" size="4" class=" is-invalid" ><button class="setCharge_rate_limit" id="start" type="button" class="btn btn-outline-secondary">Set</button> Active: <span class="charge_rate_limit"></span> KW
                                                <br><small><i>Enter negative numbers: Add (-) sign in front</i></small>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >

                                        <div class="form-group row">
                                            <div class="col-sm-5">
                                                <strong>Discharge Rate Limit:</strong><br>
                                                <small><i>Enter whole numbers</i></small>
                                            </div>
                                            <div class="col-sm-7">

                                                <input type="text" id="discharge_rate_limit" name="discharge_rate_limit" maxlength="5" size="4"><button class="setDischarge_rate_limit" id="start" type="button" class="btn btn-outline-secondary">Set</button> Active: <span class="discharge_rate_limit"></span> KW
                                                <br><small><i>Enter whole numbers</i></small>
                                            </div>

                                        </div>
                                        <hr class='hrRule' >
                                
                                        </div>
                                    </div>

                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default">

                                    <div class="panel-heading">
                                        <h3 class="text-center">
                                            <?php
                                            if( $stringsBMUs == 'strings') { echo"Strings";} else echo "BMUs";
                                            ?>
                                            </h3>
                                    </div>
                                    <div class="panel-body">
                                        <hr class='hrRule'>
                                        <?php
                                        if( $stringsBMUs == 'strings') {
                                        ?>
                                        <div class="form-group row">
                                            <div class="col-sm-5 "><strong><span class="string_now" id="1">Battery String 1 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_1_soc' ></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="2">Battery String 2 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_2_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="3">Battery String 3 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_3_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="4">Battery String 4 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_4_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="5">Battery String 5 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_5_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="6">Battery String 6 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_6_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="7">Battery String 7 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_7_soc'></span>%</div>
                                        </div>
                                        <hr class='hrRule'>
                                        <div class="form-group row">
                                            <div class="col-sm-5"><strong><span class="string_now" id="8">Battery String 8 SOC:</span></strong></div>
                                            <div class="col-sm-7"><span class='battery_string_8_soc'></span>%</div>
                                        </div>
                                        <?php } else {
                                        ?>
                                        <a href="battery_bmu_data.php?ess_id=<?php echo $ess_id; ?>">BMU data</a>
                                        <?php }?>
                                        <hr class='hrRule'>
                                </div>
                                </div>
                            </div>


                        </div>

                    </div>
            </div>
            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>


</div>
</body>
</html>