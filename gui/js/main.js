/**
 * Created by AdamCadieux on 11/16/2016.
 */

var rows = 10;
var meterEnabled;
var feedsEnabled;
$(document).ready(function() {

    // Disable buttons
    if(accessLevel ==4) {
        $(':button').prop('disabled', true);
    }
    
    // Get data about feeds and meter //
    $.post("control/get_meter-feed-all.php?type=both", {
    }, function(data) {

        var res = data.split("|");
        meterEnabledTemp = res[0].split(":");
        feedsEnabledTemp = res[1].split(":");
        meterEnabled = meterEnabledTemp[1];
        feedsEnabled = feedsEnabledTemp[1];
        // alert("meter:" +  meterEnabled[1] + "  feed:" + feedsEnabled[1])

        // INIT feeds set buttons correctly //
        if(feedsEnabled  == 'FeedsAllOn' ) {
            $('input[name="feed_enable_all_value"]').val('0');
            $("#disableEnableAllFeeds").html('Disable All Regulators');

        }else {
            $('input[name="feed_enable_all_value"]').val('1');
            $("#disableEnableAllFeeds").html('Enable All Regulators');
        }

        // INIT meters set buttons correctly //
        if(meterEnabled == 'meterAllOn' ) {
            $('input[name="meter_enable_all_value"]').val('0');
            $("#disableEnableAllMeters").html('Disable All Meters' );
        }else {
            $('input[name="meter_enable_all_value"]').val('1');
            $("#disableEnableAllMeters").html('Enable All Meters ');
        }



        $("#startSystemGateway").click(function(e) {

            if (confirm("Are you sure you want to change Start/Stop the Gateway") == true) {
                var system_enable = $("#system_enable").val();
                var gateway_id = $("#gateway_id").val();
                
                if (system_enable == '') {
                    console.log("system_enable blank");
                } else {
                    $.post("control/form_gateway_enable.php", {
                        system_enable: system_enable,
                        gateway_id: gateway_id
                    }, function (data) {
                        //console.log(data);


                        if (data == '0') {
                            $("#disableEnableGateway").html('Disable Gateway Now');
                            $('input[name="system_enable"]').val('0');
                        } else {
                            $("#disableEnableGateway").html('Enable Gateway Now');
                            $('input[name="system_enable"]').val('1');
                        }

                    });
                }
                return false;
            }
        });

        $("#sytemRestart").click(function(e) {
            if (confirm("Are you sure you want to Restart the Gateway") == true) {
                var system_restart = $("#system_restart").val();
                var gateway_id = $("#gateway_id").val();
                //console.log('system_restart:' + system_restart2 + " gateway_id:" + gateway_id)

                if (system_restart2 == '') {
                    //console.log("system_enable blank");
                } else {
                    $.post("control/form_gateway_restart.php", {
                        system_restart: system_restart,
                        gateway_id: gateway_id
                    }, function (data) {
                        //console.log(data);
                        //$('input[name="system_enable"]').val(data);
                        if (data == '1') {
                            $("#disableEnableGateway").html('Restarting Now:' + data);
                        }

                    });
                }
                return false;
            }
        });

        $("#form_feed_all").click(function(e) {
            var feed_enable_all = $("#feed_enable_all_value").val();
            if( ((meterEnabled == 'meterAllOn') && (feed_enable_all == 1)) || (feed_enable_all == 0)) {
                if (confirm("Are you sure you want to Change all the Feeds") == true) {


                    if (feed_enable_all == '') {
                        console.log("feed_enable_all_value blank");
                    } else {
                        $.post("control/form_feed_all.php", {
                            feed_enable_all: feed_enable_all
                        }, function (data) {
                            //console.log(data);
                            $('input[name="feed_enable_all_value"]').val(data);

                            if (data == '0') {
                                $("#disableEnableAllFeeds").html('Disable All Regulators');
                            } else {
                                $("#disableEnableAllFeeds").html('Enable All Regulators');
                            }
                        });
                    }

                }
            }else {
                alert('You must enable all Meters before you can turn ')
            }
            location.reload();
            return false;
        });


        // Meter action="/control/form_meter_all.php"
        $("#form_meter_all").click(function(e) {

            var meter_enable_all = $("#meter_enable_all_value").val();
            if ( ((feedsEnabled == 'FeedsAllOn') && (meter_enable_all == 1)) || (feedsEnabled == 'FeedsOff') ) {
                if (form_meter_all == '') {
                    console.log("form_meter_all blank");
                } else {
                    $.post("control/form_meter_all.php", {
                        meter_enable_all: meter_enable_all
                    }, function (data) {
                        //console.log(data);
                        $('input[name="meter_enable_all_value"]').val(data);

                        if (data == '0') {
                            $("#disableEnableAllMeters").html('Disable All Meters');
                        } else {
                            $("#disableEnableAllMeters").html('Enable All Meters');
                        }
                    });
                }
                location.reload();
                return false;
            }else {
                alert('You must disable All Regulators before you can turn ');
            }
            location.reload();
            return false;
        });


        $('.feed-enable').on('click', function() {

            var feedID = this.id.split('-');
            //alert("meterEnabled: " + meterEnabled + " Feed: " + feedID[1]);
            if( ((meterEnabled == 'meterAllOn') && (feedID[1] == 0)) || (feedID[1] == 1)) {
                var r = confirm("Are you sure you want to change the power regulator " + feedID[0] + " status?");
                if (r == true) {
                    $.ajax({
                        url: "control/form_feed_enable.php?id="+ feedID[0] + "&enabled="+feedID[1] ,
                        cache: false,
                        success: function(data){
                            if(feedID[1] == 1) {
                                $("#regulator-" + feedID[0]).text("Enable");
                                $('#'+feedID[0]+'-'+feedID[1]).attr('id',feedID[0]+'-0');
                                $('input[name="feed_enable_all_value"]').val('1');
                                $("#disableEnableAllFeeds").html('Enable All Regulators');
                            }else {
                                $("#regulator-" + feedID[0]).text("Disable");
                                $('#'+feedID[0]+'-'+feedID[1]).attr('id',feedID[0]+'-1');
                                $('input[name="feed_enable_all_value"]').val('0');
                                $("#disableEnableAllFeeds").html('Disable All Regulators');
                            }
                        }
                    });
                }
            }else {
                alert('You must enable all Meters before you can turn ')
            }
            location.reload();
        });


        $('.brainboxes-enable').on('click', function() {

            brainID=this.id.split('-');
            //alert("feedsEnabled: " + feedsEnabled+ " meter: " +  brainID[0]);
            if (((feedsEnabled == 'FeedsOff') && (brainID[1] == 1))|| (feedsEnabled == 'FeedsOff') ){ // FeedsAllOn
                var x = confirm("Are you sure you want to change the Meter Status?");
                if (x == true) {
                    $.ajax({
                        url: "control/form_brainboxes_enable.php?brainbox_id=" + brainID[0] + "&enabled=" + brainID[1],
                        cache: false,
                        success: function (data) {

                            if (brainID[1] == 1) {
                                $("#" + brainID[0] + "-brain").text("Enable");
                                $('#' + brainID[0] + '-' + brainID[1]).attr('id', brainID[0] + '-0');
                            } else {
                                $("#" + brainID[0] + "-brain").text("Disable");
                                $('#' + brainID[0] + '-' + brainID[1]).attr('id', brainID[0] + '-1');
                            }
                        }
                    });
                }
            }else {
                alert('You must disable All Regulators before you can turn ' );
            }
            location.reload();
        });

        // Meter action="/control/form_meter_all.php"
        $("#reset_network_protector").click(function(e) {
            var r = confirm("Are you sure you want to reset the Network Protector?");
            if (r == true) {
                $.ajax({
                    url: "control/reset_net_protector.php?delete=yes",
                    cache: false,
                    success: function (data) {
                    }
                });

                location.reload();
                return false;
            }
        });


        $('#moreLog').on('click', function() {
            rows = rows + 10;
            //alert('more logs:' + rows);

        });
        $('#lessLog').on('click', function() {
            rows = rows - 10;
            //alert('more logs:' + rows);

        });
    });
});

function auto_gateway_status(){

    $.ajax({
        url: "control/get_gateway-status.php?data=yes&type=status",
        cache: false,
        success: function(data){

            $("#auto_gateway_status_div").html(data);

            // console.log(' success auto_gateway_status:' + $("#auto_gateway_status_div").html());

            if( $("#auto_gateway_status_div").html() == 'System Offline') {
                // console.log('System Offline');

                $('#startSystem').show();
                $('#stopSystem').hide();
                $('#disableEnableGateway').html('Enable Gateway Now');
                $('input[name="system_enable"]').val('1');
            }
            if( $("#auto_gateway_status_div").html() == 'System Online') {
                // console.log('System Online');
                $('#stopSystem').show();
                $('#startSystem').hide();
                $('#disableEnableGateway').html('Disable Gateway Now');
                $('input[name="system_enable"]').val('0');
            }


        }
    });
}

function auto_gateway_restart_div(){

    $.ajax({
        url: "control/get_gateway-status.php?data=yes&type=restart",
        cache: false,
        success: function(data){

            $("#auto_gateway_restart_div").html(data);


            console.log(' success auto_gateway_restart_div:' + data);

            if( $("#auto_gateway_restart_div").html() == 'No change') {
                console.log('restart No change');
                $("#restartButton").html('Restart Gateway Now');
                //$('#startSystem').show();
                //$('#stopSystem').hide();
            }
            if( $("#auto_gateway_restart_div").html() == 'System Restarting') {
                $("#restartButton").html('System Restarting');
                console.log('System Restarting');
                //$('#stopSystem').show();
                //$('#startSystem').hide();
            }


        }
    });
}
//

function network_protector_status(){
    $.ajax({
        url: "control/get_network_protector.php",
        cache: false,
        success: function(data){
            $("#network_protector_status_div").html(data);
        }
    });
}
function network_protector_registers_status(){
    $.ajax({
        url: "control/get_network_protector_registers.php",
        cache: false,
        success: function(data){
            $("#network_protector_register_status_div").html(data);
        }
    });
}


function auto_nxo_feed_status(){

    $.ajax({
        url: "control/get_nxo_readings.php",
        cache: false,
        success: function(data){
            $("#auto_nxo_feed_status_div").html(data);
        }
    });
}

function auto_nxo_full(){

    $.ajax({
        url: "control/get_nxo_full.php",
        cache: false,
        success: function(data){
            $("#auto_nxo_full_div").html(data);
        }
    });
}
function auto_log_status(){
    $.ajax({
        url: "control/getLog.php?rows=" + rows,
        cache: false,
        success: function(data){
            $("#auto_log_status_div").html(data);
        }
    });
}

function auto_brainbox_status(){
    $.ajax({
        url: "control/get_brainboxes.php",
        cache: false,
        success: function(data){
            $("#auto_brainbox_status_div").html(data);
        }
    });
}


function auto_inverter_status(){
    $.ajax({
        url: "control/inverter_status.php",
        cache: false,
        success: function(data){
            $("#inverter_status_div").html(data);
        }
    });
}
$(document).ready(function(){
    //console.log('I live');
    auto_inverter_status();
    auto_gateway_status();
    auto_gateway_restart_div();
    auto_brainbox_status();
    auto_log_status();
    network_protector_status();
    network_protector_registers_status();
   // auto_nxo_status();
    auto_nxo_full();
    auto_nxo_feed_status();

});

//Refresh auto_load() function after 10000 milliseconds
setInterval(auto_inverter_status,20000);
setInterval(auto_gateway_status,2000);
setInterval(auto_gateway_restart_div,2000);
setInterval(auto_brainbox_status,2000);
setInterval(auto_log_status,2000);
setInterval(network_protector_status,2000);
setInterval(network_protector_registers_status,2000);
//setInterval(auto_nxo_status,2000);
setInterval(auto_nxo_full,500);
setInterval(auto_nxo_feed_status,500);

// Quandrants Graphs
$(document).ready(function() {
    auto_guandrants_graphs();

    setInterval(auto_guandrants_graphs,20000);

    //auto_gateway_status();

    function auto_guandrants_graphs(){
   
        $.ajax({
            url: "control/get_power_factor.php",
            cache: false,
            success: function(data){
                //console.log('called ajax 176');
                try {
                    var obj = $.parseJSON(data);
                    //console.log('json' + data);

                    var obj = $.parseJSON(data);
                    var quad_feed_A = Number(obj[0].quad_feed_A);
                    var row_id = Number(obj[0].row_id);

                    if((obj[0].quad_feed_A !=0 ) || (obj[0].quad_feed_A !=null) || (obj[0].quad_feed_A !='') ){
                        feedsToQuad('A',obj[0].quad_feed_A,obj[0].var_A);
                        $("#kvar_A").text(obj[0].kvar_A);
                    }
                    if((obj[0].quad_feed_B !=0 ) || (obj[0].quad_feed_B !=null)  || (obj[0].quad_feed_B !='')  ){
                        feedsToQuad('B',obj[0].quad_feed_B,obj[0].var_B);
                        $("#kvar_B").text(obj[0].kvar_B);
                    }
                    if((obj[0].quad_feed_C !=0 ) || (obj[0].quad_feed_C !=null) || (obj[0].quad_feed_V !='')  ){
                        feedsToQuad('C',obj[0].quad_feed_C,obj[0].var_B);
                        $("#kvar_C").text(obj[0].kvar_C);
                    }

                } catch(e) {
                    console.log('No json data');
                }



                function feedsToQuad(feedName,quadNum,data) {
                    var id = 'id';
                    $(".qFeed"+feedName).css("background-color", '#5bc0de');
                    $(".rcorners"+quadNum).filter(".qFeed"+feedName).css("background-color", '#afdff5');
                    $("."+quadNum+feedName+"Text").html(data);
                    setTimeout(function(){
                        $(".rcorners"+quadNum).filter(".qFeed"+feedName).css("background-color", '#428bca');
                    }, 3000);


                }

            }
        });
    }


});
