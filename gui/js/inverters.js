/**
 * Created by AdamCadieux on 12/7/2017.
 */

var InvertersCheckedBoxes =[];
var inverterCookie = null;
var pageLength = 50;

$(document).ready(function (){
    
    
    setInterval( function () {
        table.ajax.reload( null, false );
    }, 5000 );

    var table =    $('#inverterdata').DataTable( {
        "pageLength": pageLength,
        "ajax": "/control/inverters_json.php?dataType=inverters",
        "columns": [
            { 'data': 'serialNumber'},
            { 'data': 'mac_address'},
            { 'data': 'IP_Address'},
            { 'data': 'version'},
            { 'data': 'stringId'},
            { 'data': 'stringPosition'},
            //  { 'data': 'prodPartNo'},
            // { 'data': 'comm','className': "comm"},
            { 'data': 'comm',
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        if(data =='DC_FULL_PWR' ){
                            return ' <span style="color:GREEN" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'"  ></span>';
                        }
                        if(data =='DC_NIGHT' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='DC_IDLE' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='DC_OTHER' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='DC_RAMP' ){
                            return ' <span style="color: #99c199" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='UNDER_PERF' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='BOOT_LOADER' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='BOOT_UPDATER' ){
                            return ' <span style="color: yellow" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NEEDS_ATTENTION' ){
                            return ' <span style="color: orange" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='NO_COMM' ){
                            return ' <span style="color: red" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='LEGACY' ){
                            return ' <span style="color: blue" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='MGI_220' ){
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        if(data =='UNKNOWN' ){
                            return ' <span style="color: black" class=" glyphicon glyphicon-one-fine-dot" title="' + data +'" ></span>';
                        }
                        else {
                            return ' <span style="color: gray" class=" glyphicon glyphicon-one-fine-dot"  title="Not Mapped: ' + data +'" ></span>';
                        }

                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }
            }, { 'data': 'watts'},
            { 'data': 'battery'
                ,
                "render": function ( data, type, row ) {
                    if(type === 'display'){
                        if(data == 1){
                            return 'B'
                        }else return 'S'
                    }else if(type === 'sort'){
                        return data;
                    }else{
                        return data;
                    }
                }},
            { 'data': 'FEED_NAME'}

        ]

    } );
    

    $('#health-inverters').on('click', function() {
        //  console.log('well it was clicked...')
        var r = confirm("This scan may take several minutes. Do you want to continue?");
        if (r == true) {

            $.featherlight({
                iframe: '/health_scan.php', iframeMaxWidth: '100%', iframeWidth: 800,
                iframeHeight: 800,afterClose : function(event){
                    //console.log("after close" +this); // this contains all related elements
                    //alert(this.$content.hasClass('true')); // alert class of content
                    window.location.reload();
                }
            });
        }
    });

    
});


function unique(list) {

    list.sort()
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    // remove empty places
    result = result.filter(v=>v!='');
    return result;
}
