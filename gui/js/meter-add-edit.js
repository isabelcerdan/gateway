/**
 * Created by AdamCadieux on 5/18/2016.
 */
$( document ).ready( function () {
    $( "#meterForm" ).validate( {
        rules: {

            LOGGER_TAG:"required",
            modbusdevicenumber:"required",
            address:"required"
        },
        messages: {

            LOGGER_TAG: "Please enter a Logger Tag",
            modbusdevicenumber: "Please enter a Modbus Device Number",
            address:"Please enter a Interface Addressr"

        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-md-3" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".col-md-3" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    } );


} );