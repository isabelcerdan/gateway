/**
 * Created by AdamCadieux on 5/18/2016.
 */
$(document).ready(function () {

    var FeedInputVal;


    if (FeedInputVal == undefined) {
        var FeedInputVal = $("#FEED_SIZE").val();
        $(".FEED_SIZE_RESULTS").text(FeedInputVal);

    }

    $("#FEED_SIZE").blur(function () {
        FeedInputVal = $("#FEED_SIZE").val();
        $(".FEED_SIZE_RESULTS").text(FeedInputVal);
    });


    $("#MINIMUM_IMPORT").blur(function () {
        var MINIMUM_IMPORTInputVal = $("#MINIMUM_IMPORT").val();
        var MINIMUM_IMPORT_results = FeedInputVal * MINIMUM_IMPORTInputVal;
        var MIlong = String(MINIMUM_IMPORT_results);
        var MIoutput = MIlong.substr(0, 8);
        $(".FEED_SIZE_RESULTS").text(FeedInputVal);
        $("#MINIMUM_IMPORT_RESULTS").text(MIoutput);
    });

    $("#CURTAIL_TARGET_coeff").blur(function () {
        var CURTAIL_TARGETInputVal = $("#CURTAIL_TARGET_coeff").val();
        var CURTAIL_TARGET_coeff_results = FeedInputVal * CURTAIL_TARGETInputVal;
        var CTlong = String(CURTAIL_TARGET_coeff_results);
        var CToutput = CTlong.substr(0, 8);

        $(".FEED_SIZE_RESULTS").text(FeedInputVal);
        $("#CURTAIL_TARGET_RESULTS").text(CToutput);
    });

    $("#feedForm").validate({
        rules: {
            FEED_SIZE: {
                required: true,
                min: 0.25
            },
            SYSTEM_SIZE: {
                required: true,
                min: 0.25
            },
            CURTAIL_TARGET: {
                required: true,
                range: [-100, 100]
            }
        },
        messages: {
            SYSTEM_SIZE: {
                required: "Please enter your the system size",
                min: "Site size is a minimum of kW 0.25"
            },
            CURTAIL_TARGET: {
                required: "Please enter your Curtail Target",
                range: "Please enter values between the range of -100 to 100"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-3").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-md-3").addClass("has-success").removeClass("has-error");
        }
    });


});