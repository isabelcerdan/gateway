<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/25/2016
 * Time: 5:15 PM
 */
session_start();
$_SESSION["login"] = "logged_no";

// remove all session variables
session_unset();

// destroy the session
session_destroy();

?>