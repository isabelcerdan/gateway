<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/25/2016
 * Time: 5:11 PM
 */

//Make sure that user is from a good IP address
//include_once ('../control/safe_IPs.php');

session_start();

if (!isset($minPrivileges)) {
    $minPrivileges = 4;
}

if($_SESSION["privileges"] >= $minPrivileges ) {
    echo "<p>Access Denied</p>";
    //header('Location: access_denied.php?errors=access_denied');
    exit();
}else {
    //echo "OK";
}



?>