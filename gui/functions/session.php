<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/25/2016
 * Time: 5:11 PM
 */

//Make sure that user is from a good IP address
include_once('safe_IPs.php');

session_start();

//Get URL of page filter out any attaching data, and remove anything after '?' //
$pageURL = strtok(filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL), '?');

// Redirect for StringSight
$stringSight = 0;
include_once ("stringSight_status.php");
if($stringSight == '1') {
    header('Location: http://127.0.0.1/stringSight.php' );
}


// if user isn't logged in redirect them to login page //
if($_SESSION["login"] != "logged_yes"){
    header('Location: /index.php?errors=not_logged_in');
}else {

    // admin can go anywhere //
    if($_SESSION["privileges"] != 1) {
        include_once('page_permissions_check.php');

        // Make sure that the user has an equal or higher access level of the page they are on //
        if( $access_levelPage >= $_SESSION["privileges"] ) {
             //echo " OK: ";
        }else {
            //echo " problem: ";
            header('Location: access_denied.php?errors=access_denied');
        }
    }
}

?>