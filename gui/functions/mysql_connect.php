<?php

  include_once('vault.php');
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/20/2016
 * Time: 3:37 PM
 */
    $servername = "localhost";
    $username = $vault_username;
    $password = $vault_password;
    $database = $vault_database;
    // Create connection";

$conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
  // echo "Connected successfully";


?>
