
<?php
include_once ('functions/mysql_connect.php');
include_once ('functions/session.php');
//include_once('control/get_feed_list.php'); // Get default data
//
$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BMU Data</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
    <style>
        .successTable, .successTable tr{
            background-color: #33ccff !important;
        }
        .failTable, .failTable tr{
            background-color:  #cc3333 !important;
        }
        .dataTables_filter {
            float: right;
            text-align: right;
        }
        .invertersSelectedNum {
            font-weight: bold;
        }
        .glyphicon.glyphicon-one-fine-dot {

            overflow: hidden;
            margin-top: -10px;
            margin-bottom: -5px;
        }
        .glyphicon.glyphicon-one-fine-dot:before {
            content:"\25cf";
            font-size: 2em;
        }

        .progress {
            height: 20px;
            margin-bottom: 0px !important;

        }
        .progress-bar {
            font-size: 12px !important;
        }


        /**
        * Featherlight Loader
        *
        * Copyright 2015, WP Site Care http://www.wpsitecare.com
        * MIT Licensed.
        */
        @-webkit-keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes featherlightLoader {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .featherlight-loading .featherlight-content {
            -webkit-animation: featherlightLoader 1s infinite linear;
        animation: featherlightLoader 1s infinite linear;
        background: transparent;
        border: 8px solid #8f8f8f;
        border-left-color: #fff;
        border-radius: 80px;
        width: 80px;
        height: 80px;
        min-width: 0;
        }

        .featherlight-loading .featherlight-content > * {
            display: none !important;
        }

        .featherlight-loading .featherlight-close,
        .featherlight-loading .featherlight-inner {
            display: none;
        }
        #inverterdata {
            font-size: 12px;
        }
        td.details-control {
            background: url('images/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('images/details_close.png') no-repeat center center;
        }
        .textBold {
            font-weight: bold;
        }

    </style>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">

    <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="js/jquery-ui-1.11.4.min.js"></script>
    <script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/js.cookie.js"></script>

    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function() {

            var details = [];
            var detailsCookie = null;

            setInterval( function () {
                table.ajax.reload(function () {

                    // test = Cookies.get('testCookie');
                    // alert(test);
                    details  = Cookies.get('detailsCookie');
                    console.log(details);
                    var tr = $('#'+details).closest('tr');
                    var row = table.row( tr );
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');

                    row.child( format(row.data()) ).show();
                    // alert('fired off');
                }, false);
            }, 5000 );


            /* Formatting function for row details - modify as you need */
            function format ( d ) {
                // `d` is the original data object for the row

             if((d.serialNumber_1 != undefined) ){
                    var inverterData = '<tr><td colspan="2">'+d.serialNumber_1 +'</td><td>'+d.IP_Address_1+'</td><td>'+d.pcs_unit_id_1+'</td><td>'+d.status_1+'</td><td>'+d.comm_1+'</td><td>'+d.watts_1+'</td><td>'+d.curtail_1+'</td><tr>';
             }
             if(d.serialNumber_2 != undefined){
                    var inverterData =    inverterData + '<tr><td colspan="2">'+d.serialNumber_2+'</td><td>'+d.IP_Address_2+'</td><td>'+d.pcs_unit_id_2+'</td><td>'+d.status_2+'</td><td>'+d.comm_2+'</td><td>'+d.watts_2+'</td><td>'+d.curtail_2+'</td><tr>';
             }
             if(d.serialNumber_3 != undefined) {
                    var inverterData =    inverterData + '<tr><td colspan="2">'+d.serialNumber_3+'</td><td>'+d.IP_Address_3+'</td><td>'+d.pcs_unit_id_3+'</td><td>'+d.status_3+'</td><td>'+d.comm_3+'</td><td>'+d.watts_3+'</td><td>'+d.curtail_3+'</td><tr>';
             }
             if(d.serialNumber_4 != undefined) {
                 var inverterData =    inverterData + '<tr><td colspan="2">'+d.serialNumber_4+'</td><td>'+d.IP_Address_4+'</td><td>'+d.pcs_unit_id_4+'</td><td>'+d.status_4+'</td><td>'+d.comm_4+'</td><td>'+d.watts_4+'</td><td>'+d.curtail_4+'</td><tr>';
             }
             if(d.serialNumber_5 != undefined) {
                  var inverterData =    inverterData +    '<tr><td colspan="2">'+d.serialNumber_5+'</td><td>'+d.IP_Address_5+'</td><td>'+d.pcs_unit_id_5+'</td><td>'+d.status_5+'</td><td>'+d.comm_5+'</td><td>'+d.watts_5+'</td><td>'+d.curtail_5+'</td><tr>';
             }
             if(d.serialNumber_6 != undefined){
                 var inverterData =    inverterData +   '<tr><td colspan="2">'+ d.serialNumber_6+'</td><td>'+d.IP_Address_6+'</td><td>'+d.pcs_unit_id_6+'</td><td>'+d.status_6+'</td><td>'+d.comm_6+'</td><td>'+d.watts_6+'</td><td>'+d.curtail_6+'</td><tr>';
             }
             if(d.serialNumber_7 != undefined){
                    var inverterData =    inverterData +   '<tr><td colspan="2">'+d.serialNumber_7+'</td><td>'+d.IP_Address_7+'</td><td>'+d.pcs_unit_id_6+'</td><td>'+d.status_7+'</td><td>'+d.comm_7+'</td><td>'+d.watts_7+'</td><td>'+d.curtail_7+'</td><tr>';
             }
             if(d.serialNumber_8 != undefined){
                    var inverterData =    inverterData +   '<tr><td colspan="2">'+d.serialNumber_8+'</td><td>'+d.IP_Address_8+'</td><td>'+d.pcs_unit_id_8+'</td><td>'+d.status_8+'</td><td>'+d.comm_8+'</td><td>'+d.watts_8+'</td><td>'+d.curtail_8+'</td><tr>';
             }
                // need to put d.serialNumber_6 into a loop //
                return '<table id="cellDetails" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;float: none; margin: 0 auto;" class="table-bordered text-center">' +
                    '<tr style="background-color: #f2f2f2;"><td class="textBold" colspan="3">Environment Temp</td><td class="textBold">Cell Temp 1</td><td class="textBold">Cell Temp 2</td><td class="textBold">Cell Temp 3</td><td class="textBold">Cell Temp  4</td><td class="textBold">mosfet temp</td></tr>' +
                    '<tr><td  colspan="3">'+d.environment_temperature+'</td><td>'+d.cell_temperature_1+'</td><td>'+d.cell_temperature_2+'</td><td>'+d.cell_temperature_3+'</td><td>'+d.cell_temperature_4+'</td><td>'+d.mosfet_temperature+'</td><tr>'+

                    '<tr style="background-color: #f2f2f2;"><td class="textBold">Cell Volt 1</td><td class="textBold">Cell Volt 2</td><td class="textBold">Cell Volt 3</td><td class="textBold">Cell Volt 4</td><td class="textBold">Cell Volt 5</td><td class="textBold">Cell Volt 6</td><td class="textBold">Cell Volt 7</td><td class="textBold">Cell Volt 8</td></tr>' +
                    '<tr><td>'+d.cell_voltage_1+'</td><td>'+d.cell_voltage_2+'</td><td>'+d.cell_voltage_3+'</td><td>'+d.cell_voltage_4+'</td><td>'+d.cell_voltage_5+'</td><td>'+d.cell_voltage_6+'</td><td>'+d.cell_voltage_7+'</td><td>'+d.cell_voltage_8+'</td><tr>'+
                    '<tr style="background-color: #f2f2f2;"><td class="textBold">Cell Volt 9</td><td class="textBold">Cell Volt 10</td><td class="textBold">Cell Volt 11</td><td class="textBold">Cell Volt 12</td><td class="textBold">Cell Volt 13</td><td class="textBold">Cell Volt 14</td><td class="textBold">Cell Volt 15</td><td class="textBold"> Cell Volt 16</td></tr>' +
                    '<tr><td>'+d.cell_voltage_9+'</td><td>'+d.cell_voltage_10+'</td><td>'+d.cell_voltage_11+'</td><td>'+d.cell_voltage_12+'</td><td>'+d.cell_voltage_13+'</td><td>'+d.cell_voltage_14+'</td><td>'+d.cell_voltage_15+'</td><td>'+d.cell_voltage_16+'</td><tr>'+

                    '<tr style="background-color: #f2f2f2;"><td class="textBold" colspan="2">Inverters Serial #</td><td class="textBold">IP_Address</td><td class="textBold">PCS Unit </td><td class="textBold">status</td><td class="textBold">comm</td><td class="textBold">watts</td><td class="textBold">curtail</td></tr>' +
                    inverterData+

                    '<tr style="background-color: #f2f2f2;"><td class="textBold">PCS Unit ID</td><td class="textBold">BMU ID A</td><td class="textBold">BMU ID B</td><td class="textBold">Charger id</td><td class="textBold">Chassis</td><td class="textBold">Chassis IP </td><td class="textBold">State</td><td class="textBold"></td></tr>' +
                    '<tr><td>'+d.pu_id+'</td><td>'+d.bmu_id_A+'</td><td>'+d.bmu_id_B+'</td><td>'+d.charger_id+'</td><td>'+d.chassis_id+'</td><td>'+d.IP_address+'</td><td>'+d.state+'</td><td><select class="change_BMU_State" id="stateId_'+d.bmu_id_A+'"><option value="'+d.state+'">'+d.state+'</option><option value="Maintenance">Maintenance</option><option value="Unrestricted">Unrestricted</option></select></td><tr>' +
                    '</table>';
            }

           // setInterval( function () {
           //     table.ajax.reload( null, false );
           // }, 5000 );

                var table = $('#example').DataTable( {
                    "ajax": "/control/aess_bmu_data_json.php?ess_id=<?php echo $ess_id; ?>",
                    "columns": [
                        {
                            "className":      'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "bmu_id" },
                        { "data": "soc",
                            "render": function ( data, type, row ) {
                              soc =  (data *1).toFixed(0);
                                return '<div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" ria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+soc+'%">' +soc+'%</div> </div>';
                            }
                        },
                        { "data": "soh" },
                        { "data": "current" },
                        { "data": "voltage" },
                        { "data": "design_capacity" },
                        { "data": "full_charged_capacity" },
                        { "data": "state" },
                        { "data": "cycle_count" },
                        { "data": "timestamp" }

                    ],
                    "order": [[1, 'asc']],
                    rowId: function(a) {
                        return 'id_' + a.bmu_id;
                    }
                } );

                // Add event listener for opening and closing details
                $('#example tbody').on('click', 'td.details-control', function () {

                    var rowIDCurrent = $(this).parent().attr('id');
                    console.log("Row ID:" + rowIDCurrent);
                    var tr = $('#'+rowIDCurrent).closest('tr');
                    var row = table.row( tr );

                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                        Cookies.set('detailsCookie','');
                    }
                    else {
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                        //details  = Cookies.get('detailsCookie');
                        Cookies.set('detailsCookie',rowIDCurrent);
                    }
                } );

                $('#example tbody').on( 'change', '.change_BMU_State', function () {

                 var  state =  $('#' + this.id + ' :selected').text();
                    console.log('state:' + state);
                    $('.loading-overlay').show();
                    var bmu_id_from = formIdSplit=this.id.split('_')

                    var r = confirm("Are you sure you want change state to: " + state + " on BMU: "+ bmu_id_from[1]);
                    if (r == true) {
                        $.ajax({
                            type: "POST",
                            url: '/control/set_bmu.php?bmu_state='  +state + '&bmu_id=' + bmu_id_from[1],

                            success: function (data) {
                                $('.loading-overlay').hide();
                               // console.log('success:' + formDetails.serialize());
                                console.log(data);
                            },
                            error: function(jqXHR, text, error){

                            }
                        });
                        return false;
                    }



                });
            } );


    </script>



</head>
<body>
<div class="container">


    <?php
    // Header
    include_once ('header.php');

    // Menu Link //
    include_once ('menu.php'); // Get default data
    ?>
    <!-- Row start -->

            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-6"><h1 style="padding-left: 30px;">BMU Data</h1></div>
                    <div class="col-md-6">
                        <div class="text-right" style="margin: 20px;">
                            <a href="/logout.php"  class="btn btn-default btn-sm" role="button">
                                <span class="glyphicon glyphicon-log-out"></span> Log out
                            </a>
                        </div></div>
                </div>
                
                <div class="panel-body">

                    <div style="clear:both"> </div>


                    <table id="example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>bmu id</th>
                            <th>State of Charge</th>
                            <th>SOH</th>
                            <th>Current</th>
                            <th>Voltage</th>
                            <th>Design Capacity</th>
                            <th>Charged Capacity</th>
                            <th>State</th>
                            <th>Cycles</th>
                            <th>Timestamp</th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th>bmu id</th>
                            <th>State of Charge</th>
                            <th>SOH</th>
                            <th>Current</th>
                            <th>Voltage</th>
                            <th>Design Capacity</th>
                            <th>Charged Capacity</th>
                            <th>State</th>
                            <th>Cycles</th>
                            <th>Timestamp</th>
                        </tr>
                        </tfoot>
                    </table>

            </div>
        </div>
        <?php    include_once ('footer.php'); ?>
    </div>

</body>
</html>