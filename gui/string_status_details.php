<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:14 PM
 */
include_once('functions/mysql_connect.php');

$getStringStatus =  mysqli_real_escape_string($conn, $_GET['getStringStatus']);
$getStringStatus = filter_var($getStringStatus, FILTER_SANITIZE_STRING);

$stringID=  mysqli_real_escape_string($conn, $_GET['stringId']);
$stringID = filter_var($stringID, FILTER_VALIDATE_INT);
//error=Process_still_running

if($stringID != '' ) {
    $where = " where string_num = '$stringID' order by timestamp DESC Limit 1";
}else {
    $where = '';
}

$result = $conn->query("select * from cess_400_string_status $where");

while($row = $result->fetch_assoc()) {
    $string_numDB[] = $row['string_num'];
    $total_voltageDB[] = $row['total_voltage'];
    $total_currentDB[]  = $row['total_current'];
    $battery_socDB[] = $row['battery_soc'];
    $battery_sohDB[]  = $row['battery_soh'];
    $avg_cell_temptDB[]  = $row['average_cell_temperature'];
    $max_cell_v_battery_numDB[] = $row['max_cell_voltage_battery_number'];
    $max_cell_voltageDB[] = $row['max_cell_voltage'];
    $temp_max_v_cellDB[] = $row['temp_max_voltage_cell'];
    $min_cell_v_battery_numDB[] = $row['min_cell_voltage_battery_number'];
    $min_cell_voltageDB[] = $row['min_cell_voltage'];
    $timestampDB[] = $row['timestamp'];
}

?>

<!DOCTYPE html>
<html>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">

<style>
    .progress {
        margin-bottom: 0px;
    }


    }
    ?>
</style>
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>





<body>


<div class="container">
    <h2>Feed Detection - Scanning by String</h2>

    <?php if($error == 'Process_still_running') { ?>
        <div class="alert alert-dismissable alert-danger" id="scan-alert" style="display: block;">
            <button type="button" class="close" data-hide="alert" id="closeAlert">×</button>
            Please wait 5 minutes to run scan, right now the old scan is just finishing up.
        </div>
    <?php } ?>
    <div class="panel panel-default">

        <div style="margin-right: 50px; margin-left: 50px; margin-top: 20px; margin-bottom: 10px">

                <div id="auto_feed_status2_div"></div>




        </div>
        <div class="panel-body">


            </p>
            <div class="table-responsive">
                <table class="table table-striped table-condensed table-bordered text-center"  style="width: 100%">
                    <tr>
                        <th class="text-center">String Id</th>
                        <th class="text-center">Total Voltage </th>
                        <th class="text-center">Total Current</th>
                        <th class="text-center">Avg Cell Temp</th>
                        <th class="text-center">Max Cell Volt Battery Num</th>
                        <th class="text-center">Max Cell volt</th>
                        <th class="text-center">MIn Cell Volt Battery Num</th>
                        <th class="text-center">Time</th>
                    </tr>

                    <?php

                    for($i =0;$i < count($string_numDB); $i++ ) {
                        echo "<tr>";
                       echo '<td class="text-center">' . $string_numDB[$i] . "</td>";
                        echo '<td class="text-center">' . $total_voltageDB[$i]. "</td>";
                        echo '<td class="text-center">' . $total_currentDB[$i]. "</td>";
                        echo '<td class="text-center">' . $avg_cell_temptDB[$i]. "</td>";
                        echo '<td class="text-center">' . $max_cell_v_battery_numDB[$i]. "</td>";
                        echo '<td class="text-center">' . $max_cell_voltageDB[$i]. "</td>";
                        echo '<td class="text-center">' .$min_cell_v_battery_numDB[$i]. "</td>";
                        echo '<td class="text-center">' . $timestampDB[$i]. "</td>";
                        echo "</tr>";
                    }
                    //print_r($serialNumber);
                    // $rowId = 0;
                    //include_once ('control/get_upgrade_progress.php'); // Get default data
                    ?>
                </table>
            </div>


        </div>
    </div>
</div>


</body>
</html>