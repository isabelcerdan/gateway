<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 3/24/2017
 * Time: 11:33 AM
 */

include_once ('functions/version_serial.php'); // Get default data ?>
<p class="text-center small">Copyright: <?php echo date("Y");?> Apparent, Inc.  All Rights Reserved</p>