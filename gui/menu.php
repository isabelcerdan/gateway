<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 8/12/2016
 * Time: 11:09 AM
 */

$home = '';
$inverter = '';
include_once ('functions/mysql_connect.php');
include_once ('functions/page_permissions_check.php');

include_once("control/get_menu.php");

$url_page = $_SERVER['PHP_SELF'];

if($url_page == '/main.php') {
    $url_page = 'home.php'; //
}

echo '<ul class="nav nav-tabs">';
for($i = 0; $i < count($menu_id); $i++){

    if (strpos($url_page, strtolower($menu_name[$i])) !== false) {
        $active  = 'active';
    }else{
        $active  = '';
    }

    echo "<li class='$active'><a href='$menu_url[$i]'>$menu_name[$i]</a></li>";

}
?>

</ul>
