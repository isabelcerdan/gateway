<?php
// Start the session
session_start();

include_once ('functions/gateway_name.php');
//$_SESSION["login"] = "logged_yes";
//$_SESSION["login"] = "logged_no";
//header('Location: /main.php' . $error);

$errors = filter_var($_GET['errors'], FILTER_SANITIZE_STRING);

if($errors =='passwordproblems' ) {
	$login_fail = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign In for Gateway GUI</title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">



	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


</head>

<body>

<div class="container">
	<h1 style="color: lightgrey"><?php echo $gateway_display_name; ?></h1>
	<form class="form-signin " action="/control/login.php" method="post">
		<div class="form-group <?php if($login_fail == true ) {echo "has-error";} ;?>">
			<h2 class="form-signin-heading  ">User Panel Login</h2>
			<?php if($login_fail == true ) {echo "<label class='control-label' >Re-enter your email & password</label>";} ;?>
			<label for="inputEmail" class="sr-only">Email address</label>
			<input name="email" type="email" id="inputEmail" class="form-control <?php if($login_fail == true ) {echo "inputError";} ;?>  " placeholder="Email address" required autofocus>

			<label  for="inputPassword" class="sr-only" style="margin-top: 5px">Password</label>
			<input  style="margin-top: 5px" name="password"  type="password" id="inputPassword" class="form-control <?php if($login_fail == true ) {echo "inputError";} ;?>" placeholder="Password" required>
			<div class="checkbox">
				<label>
					<input type="checkbox" value="remember-me"> Remember me
				</label>
			</div>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

	</form>



</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

</body>
</html>

