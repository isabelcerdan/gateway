<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/01/2017
 * Time: 2:40 PM
 *
 *
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| enabled         | tinyint(1)  | YES  |     | 1       |       |
| started  | tinyint(1)  | YES  |     | 0       |       |
| updated         | tinyint(1)  | YES  |     | 0       |       |
| pid             | varchar(10) | YES  |     | NULL    |       |
| active_id       | int(11)     | YES  |     | NULL    |       |
| suspended       | varchar(5)  | YES  |     | NULL    |       |
| audit_frequency | int(3)      | NO   |     | 10      |       |
+-----------------+-------------+------+-----+---------+-------+
*/

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$result = $conn->query("select * from protection_alert;");

$num_rows = mysqli_num_rows($result);

echo "<tr >";
if($num_rows  > 0){
    //while($row = $result->fetch_assoc()) {

   // }
    echo "<td class='text-center'><b>Network Protector Needs Reset:</b></td>";
}
else {
    echo "<tr class='lineHeight'>";
    echo "<td class='text-center'><b>Network Protector OK </td>";
}

echo "</tr>";

?>