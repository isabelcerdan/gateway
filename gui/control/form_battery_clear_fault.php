<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');



$sql = "UPDATE ess_master_info SET unit_work_state = 'Stopped'";
if (!mysqli_query($conn,$sql)) {
    die('<br>Error: ' . mysqli_error($conn));
}

?>