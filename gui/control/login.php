<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */

//Make sure that user is from a good IP address
include_once('safe_IPs.php');

include_once ('../functions/mysql_connect.php');
// clear error var
$error = '';
$password_salt = '';
$password_hash = '';
$status  = '';
$failed_attempts = '';
$last_attempt = '';
$time = time();
//$checkTime = intval($time) + 900;
//$checkTime = intval($time);
$IP_address = mysqli_real_escape_string($conn, $_SERVER['REMOTE_ADDR']);
$IP_address = filter_var($IP_address, FILTER_SANITIZE_STRING);

$email  = mysqli_real_escape_string($conn, $_POST['email']);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);

$password = mysqli_real_escape_string($conn, $_POST['password']);
$password = filter_var($password, FILTER_SANITIZE_STRING);


$stmt = $conn->prepare("SELECT email,password_salt,password_hash,status,failed_attempts,last_attempt,role,privileges FROM users where email=?");

$stmt->bind_param('s',$email );
$stmt->execute();

$stmt->store_result();
$stmt->bind_result($emailDB,$password_saltDB,$password_hashDB,$statusDB,$failed_attemptsDB,$last_attemptDB, $roleDB, $privilegesDB);

while($stmt->fetch())
{
   $email = $emailDB;
   $password_salt = $password_saltDB;
   $password_hash_DB = $password_hashDB;
   $status  =$statusDB;
   $failed_attempts = $failed_attemptsDB;
   $last_attempt = $last_attemptDB;
     $role = $roleDB;
    $privileges = $privilegesDB;
}

if( ($role == 'gui') OR  ($role == 'both') ){
  // echo "OK";
}else {
    header('Location: /index.php?errors=wrongsite' );
    exit();
}

// Don't let the user log in after there account has been on hold until 15 minutes //
$checkTime = $last_attempt + 900;
if(($checkTime > $time) AND  ($status == 'hold')){
   //echo "Yes $checkTime +  > $time<br>";
    header('Location: /index.php?errors=lockedout' );
    exit();
}

// create hash from user input
$password_hash   = hash('sha256', $password . $password_salt);


// check user hash from db hash and account isn't on hold
if($password_hash_DB == $password_hash ) {
    $status  = '';
    $failed_attempts = '';
    $last_attempt = '';

    $sql = "UPDATE users SET failed_attempts=?,status=?,last_attempt=?  WHERE email=?";

    $stmt = $conn->prepare($sql);

    $stmt->bind_param('dsss', $failed_attempts, $status, $failed_attempts, $email );

    $stmt->execute();

    if ($stmt->errno) {
      //  echo "FAILURE!!! " . $stmt->error;
    }
   // else echo "Updated {$stmt->affected_rows} rows";


    $_SESSION["login"] = "logged_yes";
    $_SESSION["privileges"] = $privileges;
    
    header('Location: /main.php' );
}else {
    $failed_attempts++;

    $_SESSION["login"] = "logged_no";

    if($failed_attempts > 5){
        $status = 'hold';
    }

    $sql = "UPDATE users SET failed_attempts=?,status=?,last_attempt=?, last_IP=? WHERE email=?";

    $stmt = $conn->prepare($sql);


    $stmt->bind_param('dssss', $failed_attempts, $status, $time, $IP_address,$email);
    $stmt->execute();

    if ($stmt->errno) {
        //  echo "FAILURE!!! " . $stmt->error;
    }
    // else echo "Updated {$stmt->affected_rows} rows";

    header('Location: /index.php?errors=login_failed' );
}

$stmt->close();

?>