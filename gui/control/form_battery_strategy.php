<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');


$ess_unit_id = 1;

$strategy = mysqli_real_escape_string($conn, $_REQUEST['strategy']);
$strategy= filter_var($strategy, FILTER_SANITIZE_STRING);

$fixed_charge_rate= mysqli_real_escape_string($conn, $_REQUEST['fixed_charge_rate']);
$fixed_charge_rate= filter_var($fixed_charge_rate, FILTER_SANITIZE_STRING);

$fixed_discharge_rate= mysqli_real_escape_string($conn, $_REQUEST['fixed_discharge_rate']);
$fixed_discharge_rate= filter_var($fixed_discharge_rate, FILTER_SANITIZE_STRING);

//$resultAlarm = $conn->query("SELECT enabled,soc_charge_limit,soc_discharge_limit  FROM ess_units");
// make sure we only get good data
if ($strategy != '') {
    $sql = "UPDATE power_regulators pr INNER JOIN ess_units eu ON eu.power_regulator_id = pr.id SET pr.battery_strategy = '$strategy' WHERE eu.id = '$ess_unit_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    echo "updated:strategy ";
}
if ($fixed_charge_rate != '') {
    $sql = "UPDATE ess_units set fixed_charge_rate = '$fixed_charge_rate' WHERE id = '$ess_unit_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    echo "updated:fixed_charge_rate ";
}

if ($fixed_discharge_rate != '') {
    $sql = "UPDATE ess_units set fixed_discharge_rate = '$fixed_discharge_rate' WHERE id = '$ess_unit_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    echo "updated:fixed_charge_rate ";
}


?>