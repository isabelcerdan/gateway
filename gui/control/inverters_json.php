<?php
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/1/2017
 * Time: 11:30 AM
 */

$tableName= 'inverters';
//?dataType=stringSave
include_once ('../functions/mysql_connect.php');

   $dataType = mysqli_real_escape_string($conn, $_REQUEST['dataType']);
   $dataType = filter_var($dataType, FILTER_SANITIZE_STRING);

if($dataType == 'stringSave'){
    $tableName = "saved_inverters WHERE display != 'no' ";
}

$result = $conn->query("SELECT * FROM $tableName");

$rows = array();
while($r = mysqli_fetch_assoc($result)) {
    $a['data'][] = $r;
}

echo json_encode($a);
