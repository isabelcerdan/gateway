<?php
/**
 * Created by PhpStorm.
 * User: Patrice de Muizon
 * Date: 6/7/2017
 * Time: 10:38 PM
 */

include_once ('../functions/mysql_connect.php');

//header('Content-Type: application/json');

function mysql_array($result) {
    $records = array();
    if(mysqli_num_rows($result)>0) {
        while($row = $result->fetch_assoc()) {
            $record = array();
            foreach ($row as $field => $value) {
                $record[$field] = $value;
            }
            array_push($records, $record);
        }
    }
    return $records;
}

$result = $conn->query("SELECT * FROM alarms_history ORDER BY time DESC LIMIT 5000");
$alarms_history = mysql_array($result);


/*
 * Alarms history stores alarm raised flags as a character string of binary data.
 * In other words, flags are stored as an actual character string (e.g. "100101010...").
 * This greatly simplifies handling of alarms at the cost of more space.
 */

$prev_bitarray = str_replace('1', '0', $alarms_history[0]['alarms']);
$data = array('data' => array());

// loop through all relevant alarms history records
foreach ($alarms_history as $history) {

    $alarm_bitarray = $history['alarms'];
    $raised_ids = array();
    $cleared_ids = array();

    // loop through all the characters (representing bits) in the alarms array
    for ($bit = 0; $bit < strlen($alarm_bitarray); $bit++) {

        // break out of loop if bit is past the end of prev_bitarray
        if ($bit > strlen($prev_bitarray))
            break;

        // check if this alarm has changed state from previous record
        if ($alarm_bitarray[$bit] != $prev_bitarray[$bit]) {

            // if so, derive alarm id based on bit index
            $alarm_id = $bit + 1;

            // if alarm is now set
            if ($alarm_bitarray[$bit]) {
                array_push($raised_ids, $alarm_id);
            } else {
                array_push($cleared_ids, $alarm_id);
            }
        }
    }

    // if we've detected any raised or cleared alarms, render the ids in JSON data
    if (!empty($raised_ids)) {

        $sql = "SELECT name,'<div class=\"text-danger\">Alarm Raised</div>' as status,'" .
            date('Y-m-d - g:i:s A T', $history['time']) . "' as time," .
            "severity,strategy,last_raised FROM alarms WHERE id IN ('" . implode("','", $raised_ids) . "')";
        $raisedAlarms = mysql_array($conn->query($sql));
        $data['data'] = array_merge($data['data'], $raisedAlarms);
    }

    if (!empty($cleared_ids)) {
        $sql = "SELECT name,'Alarm Cleared' as status,'" .
            date('Y-m-d - g:i:s A T', $history['time']) . "' as time," .
            "severity,strategy FROM alarms where id IN ('".implode("','",$cleared_ids)."')";
        $clearedAlarms = mysql_array($conn->query($sql));
        $data['data'] = array_merge($data['data'], $clearedAlarms);
    }

    // set previous to current to process next record
    $prev_bitarray = $alarm_bitarray;
}

echo json_encode($data);


?>
