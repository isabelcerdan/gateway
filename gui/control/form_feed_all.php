<?php
include_once('../functions/session.php');
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/22/2016
 * Time: 2:11 PM
 */

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */
// Access restriction
include_once('../functions/restrict_privilage_access.php');

include_once('../functions/mysql_connect.php');

$error = '';

$feed_enable_all = mysqli_real_escape_string($conn, $_POST['feed_enable_all']);
$feed_enable_all = filter_var($feed_enable_all, FILTER_SANITIZE_NUMBER_INT);

//confirm that either a 1 or 0 is submitted and if not do nothing but redirect
if (($feed_enable_all == 0) OR ($feed_enable_all == 1)) {

    $sql = "UPDATE power_regulators set ENABLED = '$feed_enable_all'";
    if (!mysqli_query($conn, $sql)) {
        die('<br>Error: ' . mysqli_error($conn));
        //header('Location: /main.php?Feed_enable_all=error');
    } else {
        if ($conn->insert_id != 0) {
            $gateway_id = $conn->insert_id;
        }
        // toggle enable //
        if ($feed_enable_all == '1') {
            $feed_enable_all = '0';
        } else {
            $feed_enable_all = '1';
        }
        echo $feed_enable_all;
        // echo 'adam Rocks...';
        //header('Location: /main.php?Feed_enable_all=' .$feed_enable_all);

    }
} else {
    //  header('Location: /main.php?Feed_enable_all=error');
}

?>