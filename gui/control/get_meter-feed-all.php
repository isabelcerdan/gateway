<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$meters = 0;
$meterResults = '';
$feeds = 0;
$feedResults = '';

//
$type= $_REQUEST['type'];
$type = filter_var($type, FILTER_SANITIZE_STRING);


$resultMeters = $conn->query("select * from daemon_control where name like 'meterd-%';");


while($row = $resultMeters->fetch_assoc()) {
    $enabledMeterDB[] = $row['enabled'];
}

for ($i = 0; $i < count($enabledMeterDB); $i++) {

    if($enabledMeterDB[$i] == '1') {
        $meters++;
    }
}

$resultFeeds = $conn->query("select * from daemon_control where name like 'powerregulatord-%';");


while($row = $resultFeeds ->fetch_assoc()) {
    $enabledFeedDB[] = $row['enabled'];
}

for ($i = 0; $i < count($enabledFeedDB); $i++) {

    if($enabledFeedDB[$i] == '1') {
        $feeds++;
    }
}

if($feeds == count($enabledFeedDB) ) {
    $feedResults = 'FeedsAllOn';
    //echo "$feeds all feeds enabled " . count($enabledFeedDB) . "<br>";
}else {
    $feedResults = 'FeedsOff';
    //echo "NOT $feeds all feeds enabled " . count($enabledFeedDB) . "<br>";
}


if($meters == count($enabledMeterDB) ) {
    $meterResults = 'meterAllOn';
    //echo "$meters all meters enabled" . count($enabledMeterDB) . "<br>";
}else {
    $meterResults = 'meterOff';
    //echo "NOT $meters all meters enabled" . count($enabledMeterDB) . "<br>";;
}
//echo "Meters: $meters  Feeds: $feeds";

//if( )
//mysqli_close($con);

if($type == 'both') {

   echo "meters:" . $meterResults . '|feeds:' . $feedResults;

}

if($type == 'feed') {
    echo 'feeds:' . $feedResults;
}

if($type == 'meter') {
    echo "meters:" . $meterResults;
}

?>