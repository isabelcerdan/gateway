<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');


$battery_mode = mysqli_real_escape_string($conn, $_REQUEST['battery_mode']);
$battery_mode = filter_var($battery_mode, FILTER_SANITIZE_STRING);

$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);

// make sure we only get good data
if( ($battery_mode == 'manual') OR ($battery_mode == 'auto' )) {
    $sql = "UPDATE ess_units set mode = '$battery_mode' WHERE id = '$ess_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    echo "battery_mode=$battery_mode";
}
else {
   exit();
}

?>