<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');
include_once('../control/get_system.php');


$LimitSetPoint = mysqli_real_escape_string($conn, $_REQUEST['LimitSetPoint']);
$LimitSetPoint = filter_var($LimitSetPoint, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);;


//$resultAlarm = $conn->query("SELECT enabled,soc_charge_limit,soc_discharge_limit  FROM ess_units");
// make sure we only get good data
if ($LimitSetPoint != '') {
    if ($gateway_perspective == 'production')
        $LimitSetPoint = -$LimitSetPoint;

    $sql = "update meter_feeds set threshold = '$LimitSetPoint'  where meter_role='pcc' and feed_name =
'Delta'; ";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
echo "updated:LimitSetPoint";
}
else {
   exit();
}

?>