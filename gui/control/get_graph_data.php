<?php
header('Content-Type: application/json');
session_start();
/**
 * Created by IntelliJ IDEA.
 * User: Patrice
 * Date: 12/21/2017
 * Time: 6:05 PM
 */

include_once ('../functions/mysql_connect.php');

$result = $conn->query("SELECT * FROM nxo_status");

$rows = array();
while($r = mysqli_fetch_assoc($result)) {
    $a['data'][] = $r;
}

echo json_encode($a);

