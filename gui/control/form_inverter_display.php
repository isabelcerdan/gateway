<?php
include_once ('../functions/session.php');
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/26/2016
 * Time: 3:56 PM
 */

$page = mysqli_real_escape_string($conn, $_REQUEST['p']);
$page = filter_var($page, FILTER_SANITIZE_NUMBER_INT);

$search = mysqli_real_escape_string($conn, $_REQUEST['search']);
$search = filter_var($search, FILTER_SANITIZE_STRING);

$orderby = mysqli_real_escape_string($conn, $_REQUEST['orderby']);
$orderby = filter_var($orderby, FILTER_SANITIZE_STRING);

$sortDirection = mysqli_real_escape_string($conn, $_REQUEST['sortDirection']);
$sortDirection = filter_var($sortDirection, FILTER_SANITIZE_STRING);

$itemsperpage = mysqli_real_escape_string($conn, $_REQUEST['itemsperpage']);
$itemsperpage = filter_var($itemsperpage, FILTER_SANITIZE_NUMBER_INT);


$sortDirectionDefault = 'DESC';

if (is_numeric($itemsperpage))   {
    $limit = $itemsperpage;
}
else {
    $limit=16;
}

if($page=='')
{
    $page=1;
    $start=0;
}
else
{
    $start=$limit*($page-1);
}

// check search value exist


if( !empty($search) ) {
    // reset page numbers //
    $page=1;
    $start=0;

    $where .=" WHERE ";
    $where .=" ( mac_address LIKE '".$search."%' ";
    $where .=" OR serialNumber LIKE '".$search."%' ";
    $where .=" OR IP_Address LIKE '".$search."%' ";
    $where .=" OR controlledByGateway LIKE '".$search."%' ";

    $where .=" OR FEED_NAME LIKE '".$search."%' )";
}

if( !empty($orderby) ) {

    $page=1;
    $start=0;
    $order = "ORDER BY " . $orderby;
    if($sortDirection == 'DESC') {
        $order = $order . " " . $sortDirection;
        $sortDirectionDefault = 'ASC';
    }

    if($sortDirection == 'ASC')  {
        $order = $order . " " . $sortDirection;
        $sortDirectionDefault = 'DESC';
    }

}

$result = $conn->query("SELECT * FROM inverters $where  $order limit $start, $limit");

//$query=mysql_query("select * from table1 limit $start, $limit") or die(mysql_error());
$tot = $conn->query("SELECT * FROM inverters");
$total=mysqli_num_rows($tot);
$num_page=ceil($total/$limit);

while($row = $result->fetch_assoc()) {
    $serialNumber[] = $row['serialNumber'];
    $group_id[] = $row['group_id'];
    $mac_address[] =  $row['mac_address'];
    $IP_Address[] =  $row['IP_Address'];
    $version[] =  $row['version'];
    $lable[] =  $row['lable'];
    $stringId[] = $row['stringId'];
    $stringPosition[] =  $row['stringPosition'];
    $export_status[] =  $row['export_status'];
    $prodPartNo[] =  $row['prodPartNo'];
    $output_status[] =  $row['output_status'];
    $controlledByGateway[] =  $row['controlledByGateway'];
    $FEED_NAME_inverter[] =  $row['FEED_NAME'];
    $status[] =  $row['status'];
}

function pagination($page,$num_page,$limit)
{
    if($page == 1){
        $back =  1;
    }else {
        $back =  $page - 1;
    }
    if($num_page == $page) {
        $forword = $num_page;
    }else {
        $forword = $page + 1;
    }

    echo'<li class="page-item"><a class="page-link" href="inverters_list.php?p='.$back . '&itemsperpage='.$limit. '"><span aria-hidden="true">&laquo;</span></a></li>';

    for($i=1;$i<=$num_page;$i++)
    {

        if($i==$page)
        {
            echo'<li class="active"><a href="#">'.$i.'</a></li>';
        }
        else
        {
            echo'<li><a href="inverters_list.php?p='.$i.'&itemsperpage='.$limit. '">'.$i.'</a></li>';
        }
    }
    echo'<li class="page-item"><a class="page-link" href="inverters_list.php?p='.$forword. '&itemsperpage='.$limit. '"><span aria-hidden="true">&raquo;</span></a></li>';
}
?>