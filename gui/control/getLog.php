<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/7/2016
 * Time: 4:34 PM
 *
 * 2016-09-07 16:01:52,806 Monitor [INFO]: ***MONITOR DAEMON STARTED WITH NEW LOG AT 04:01:52 (UTC = 1473289312)***
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

$rows= filter_var($_GET['rows'], FILTER_SANITIZE_NUMBER_INT);

$filename = '/var/log/apparent/gateway.log';

if( file_exists($filename) ) {

    $logText =  tailCustom($filename, $rows);

    // Add comma at correct places for CSV
    $logArray = explode("\n",$logText);

    //$logArray =  preg_replace('/(,)(?!0)/' , ' ', $logArray );

    foreach($logArray as $lineOfLog){
       // $lineOfLog =   preg_replace('/,/', 'replaceMeNowProgrammer', $lineOfLog, 1); // replace first comma
        //$lineOfLog = str_ireplace(',', ' ', $lineOfLog ); // remove call commas so we don't get extra lines
        //$lineOfLog = str_ireplace("[INFO]:",",[INFO]:,",$lineOfLog);
       // $lineOfLog= str_ireplace("[CRIT]:",",[CRIT]:,",$lineOfLog);
       // $lineOfLog = str_ireplace("[ERR]:",",[ERR]:,",$lineOfLog);
       // $lineOfLog= str_ireplace("replaceMeNowProgrammer",",",$lineOfLog); // put first comma back
        echo  "<span style='font-size: smaller' >" . filter_var($lineOfLog, FILTER_SANITIZE_STRING)."</span><hr style='margin: 2px; padding 0'>";
    }

}else {
    echo "File not accessible ";
}

function tailCustom($filepath, $lines = 1, $adaptive = true) {
    // Open file
    $f = @fopen($filepath, "rb");
    if ($f === false) return false;
    // Sets buffer size
    if (!$adaptive) $buffer = 4096;
    else $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));
    // Jump to last character
    fseek($f, -1, SEEK_END);
    // Read it and adjust line number if necessary
    // (Otherwise the result would be wrong if file doesn't end with a blank line)
    if (fread($f, 1) != "\n") $lines -= 1;

    // Start reading
    $output = '';
    $chunk = '';
    // While we would like more
    while (ftell($f) > 0 && $lines >= 0) {
        // Figure out how far back we should jump
        $seek = min(ftell($f), $buffer);
        // Do the jump (backwards, relative to where we are)
        fseek($f, -$seek, SEEK_CUR);
        // Read a chunk and prepend it to our output
        $output = ($chunk = fread($f, $seek)) . $output;
        // Jump back to where we started reading
        fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
        // Decrease our line counter
        $lines -= substr_count($chunk, "\n");
    }
    // While we have too many lines
    // (Because of buffer size we might have read too many)
    while ($lines++ < 0) {
        // Find first newline and remove all text before that
        $output = substr($output, strpos($output, "\n") + 1);
    }
    // Close file and return
    fclose($f);
    return trim($output);
}
?>

