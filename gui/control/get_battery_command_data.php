<?php

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 5:34 PM


CESS_40_UNIT_WORK_STATE = "unit_work_state"
CESS_40_UNIT_CTRL_MODE = "unit_ctrl_mode"
CESS_40_UNIT_WORK_MODE = "unit_work_mode"
 * active_power_cmd_setting
 * active_power_cmd_setting
 */

include_once ('../functions/mysql_connect.php');
include_once('../control/get_system.php');


$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);

if($ess_id == ''){
    $ess_id = 1;
}

$resultBattery = $conn->query("SELECT mode,fixed_charge_rate,fixed_discharge_rate FROM ess_units WHERE id = '$ess_id'");

if(mysqli_num_rows($resultBattery)>0) {

    while($row = $resultBattery->fetch_assoc()) {
        $mode =  $row['mode']; //need
        $fixed_charge_rate =  $row['fixed_charge_rate'];
        $fixed_discharge_rate=  $row['fixed_discharge_rate'];
    }
}

$resultbattery_model = $conn->query("SELECT * FROM ess_models");

if(mysqli_num_rows($resultbattery_model )>0) {

    while($row = $resultbattery_model ->fetch_assoc()) {
        $manufacturer =  $row['manufacturer'];
        $model =  $row['model'];
        $full_capacity =  $row['full_capacity'];
        $feed_control =  $row['feed_control'];
        $num_strings =  $row['num_strings'];
    }
}


$resultNXO = $conn->query(" SELECT * FROM nxo_status ORDER BY Update_Time LIMIT 1");

function get_real_power($gateway_perspective, $row, $key) {
    $value = (float)$row[$key];
    if ($gateway_perspective == 'production')
        $value = -$value;
    return number_format($value, 3, '.', '');
}

if(mysqli_num_rows($resultNXO)>0) {
    while ($row = $resultNXO->fetch_assoc()) {
        $Export_PCC_1_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_1_Delta'); //need
        $Export_PCC_2_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_2_Delta');
        $Export_PCC_Combo_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_Delta');

        $Curtail_PCC_Combo_A = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_A');
        $Export_PCC_Combo_A = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_A');
        $PCC_Combo_A_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_A_Threshold');

        $Curtail_PCC_Combo_B = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_B');
        $Export_PCC_Combo_B = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_B');
        $PCC_Combo_B_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_B_Threshold');

        $Curtail_PCC_Combo_C = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_C');
        $Export_PCC_Combo_C = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_C');
        $PCC_Combo_C_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_C_Threshold');
    }
}

//$resultFeedSetPoint = $conn->query("SELECT threshold  FROM meter_feeds where meter_role='pcc' and feed_name ='Delta'"); // need



$sql = "SELECT enabled,soc_charge_limit,soc_discharge_limit,charge_rate_limit,discharge_rate_limit,power_regulator_id FROM ess_units WHERE id = '$ess_id'";
$resultAlarm = $conn->query($sql);

if(mysqli_num_rows($resultAlarm)>0) {

    while($row = $resultAlarm->fetch_assoc()) {
        $soc_charge_limit =  $row['soc_charge_limit']; // need
        $soc_discharge_limit =  $row['soc_discharge_limit']; // need
        $charge_rate_limit =  $row['charge_rate_limit']; // need
        $discharge_rate_limit =  $row['discharge_rate_limit']; // need
        $power_regulator_id =  $row['power_regulator_id']; // need
    }
}

$resultFeedSetPoint = $conn->query("SELECT threshold  FROM meter_feeds WHERE power_regulator_id = '$power_regulator_id'");

if(mysqli_num_rows($resultFeedSetPoint)>0) {

    while($row = $resultFeedSetPoint->fetch_assoc()) {
        $threshold =  get_real_power($gateway_perspective, $row, 'threshold');
    }
}

// getting soc from new table - temp solution
$result_avg_soc = $conn->query("SELECT battery_stack_soc FROM ess_master_info WHERE ess_id = '$ess_id'");

if(mysqli_num_rows($result_avg_soc)>0) {

    while($row = $result_avg_soc->fetch_assoc()) {
        $avg_soc =  $row['battery_stack_soc']; // need
    }
}


$soc_charge_limit = $soc_charge_limit * 100;
$soc_discharge_limit = $soc_discharge_limit * 100;

$result = $conn->query("SELECT Total_Net_Instantaneous_Real_Power_FP FROM meter_readings mr join energy_meter em on em.meter_role = mr.meter_role join ess_units eu on eu.meter_id = em.meter_id WHERE mr.data_type = 1 and eu.id = '$ess_id'");
if (mysqli_num_rows($result)>0) {
    while ($row = $result->fetch_assoc()) {
        $Export_Battery_1_Delta = get_real_power($gateway_perspective, $row, 'Total_Net_Instantaneous_Real_Power_FP'); // need
    }
}


$result = $conn->query("SELECT battery_strategy FROM power_regulators pr INNER JOIN ess_units eu ON eu.power_regulator_id = pr.id WHERE eu.id = '$ess_id'");
if (mysqli_num_rows($result)>0) {
    while ($row = $result->fetch_assoc()) {
        $battery_strategy = $row['battery_strategy'];
    }
}


$sql = $conn->query("SELECT *  FROM ess_master_info WHERE ess_id = '$ess_id'");
$results = array();

while($row = $sql->fetch_assoc())
{
    $results[] = array(

        'unit_ctrl_mode' =>  $row['unit_ctrl_mode'], // need
        'unit_work_state' =>  $row['unit_work_state'], // need
        'unit_work_mode' =>$row['unit_work_mode'],
        'reconnect_state' => $row['reconnect_state'],
        'battery_string_running' => $row['battery_string_running'],
        'battery_string_enabled' => $row['battery_string_enabled'],
        'battery_string_isolated' => $row['battery_string_isolated'],
        'battery_stack_voltage' => $row['battery_stack_voltage'],
        'battery_stack_current' => get_real_power($gateway_perspective, $row, 'battery_stack_current'),
        'battery_stack_power' => get_real_power($gateway_perspective, $row, 'battery_stack_power'),
        'battery_stack_soc' => number_format($row['battery_stack_soc'],0), // need
        'battery_stack_soh' => number_format($row['battery_stack_soh'],0),
        'battery_max_cell_voltage' => $row['battery_max_cell_voltage'],
        'battery_min_cell_voltage' => $row['battery_min_cell_voltage'],
        'battery_max_cell_temperature' => $row['battery_max_cell_temperature'],
        'battery_min_cell_tempreature' => $row['battery_min_cell_tempreature'],
        'dc_voltage' => $row['dc_voltage'],
        'dc_current' => $row['dc_current'],
        'dc_power' => $row['dc_power'],
        'p_value' => $row['p_value'],
        'q_value' => $row['q_value'],
        's_value' => $row['s_value'],
        'ia_value' => $row['ia_value'],
        'ib_value' => $row['ib_value'],
        'ic_value' => $row['ic_value'],
        'uab_value' => $row['uab_value'],
        'ubc_value' => $row['ubc_value'],
        'uac_value' => $row['uac_value'],
        'ua_value' => $row['ua_value'],
        'ub_value' => $row['ub_value'],
        'uc_value' => $row['uc_value'],
        'frequency' =>  $row['frequency'], // need
        'power_factor' =>  $row['power_factor'], // need
        'work_state_control_cmd' =>  $row['work_state_control_cmd'],
        'active_power_cmd' =>  $row['active_power_cmd'], // need
        'reactive_power_cmd' =>  $row['reactive_power_cmd'], // need
        'voltage_control_cmd' => $row['voltage_control_cmd'],
        'frequency_control_cmd' =>$row['frequency_control_cmd'],
        'timestamp' => $row['timestamp'],
        'soc_charge_limit' =>  number_format($soc_charge_limit,0), // need
        'soc_discharge_limit' => number_format($soc_discharge_limit,0), // need
        'cess_monitor_control_state' => $cess_controller_control_state,
        'apparent_power' => $apparent_power,
        'true_power'=> $true_power,
        'mode' => $mode,
        'fixed_charge_rate' => $fixed_charge_rate,
        'fixed_discharge_rate' => $fixed_discharge_rate,
        //'strategy' => $strategy,
        'strategy' => $battery_strategy,
        'threshold' => $threshold,
        'Export_PCC_1_Delta' => $Export_PCC_1_Delta,
        'Export_Battery_1_Delta'=> $Export_Battery_1_Delta,
        'charge_rate_limit' =>  $charge_rate_limit,
        'discharge_rate_limit' =>  $discharge_rate_limit,
        'battery_string_1_soc' =>$row['battery_string_1_soc'], // need
        'battery_string_2_soc' =>$row['battery_string_2_soc'],
        'battery_string_3_soc' =>$row['battery_string_3_soc'],
        'battery_string_4_soc' =>$row['battery_string_4_soc'],
        'battery_string_5_soc' =>$row['battery_string_5_soc'],
        'battery_string_6_soc' =>$row['battery_string_6_soc'],
        'battery_string_7_soc' =>$row['battery_string_7_soc'],
        'battery_string_8_soc' =>$row['battery_string_8_soc'],
        'startup_delay_active' =>$row['startup_delay_active'],
        'capability' =>$row['capability'],
        'capability_reason' =>$row['capability_reason'],
        'max_allowable_charge_active_power' =>$row['max_allowable_charge_active_power'],
        'max_allowable_discharge_active_power' =>$row['max_allowable_discharge_active_power']

    );
}
echo $json = json_encode($results);

?>