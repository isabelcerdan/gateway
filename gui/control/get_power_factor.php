<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/19/2016
 * Time: 2:45 PM
 */
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/28/2016
 * Time: 4:01 PM
 *
 * mysql> desc pcc_interval_averages;
+---------------+------------+------+-----+---------+----------------+
| Field         | Type       | Null | Key | Default | Extra          |
+---------------+------------+------+-----+---------+----------------+
| row_id        | int(11)    | NO   | UNI | NULL    |                |
| read_id       | bigint(20) | NO   | PRI | NULL    | auto_increment |
| Pf_A          | float      | NO   |     | 0       |                |
| Pf_B          | float      | NO   |     | 0       |                |
| Pf_C          | float      | NO   |     | 0       |                |
| kW_A          | float      | NO   |     | 0       |                |
| kW_B          | float      | NO   |     | 0       |                |
| kW_C          | float      | NO   |     | 0       |                |
| kvar_A        | float      | NO   |     | 0       |                |
| kvar_B        | float      | NO   |     | 0       |                |
| kvar_C        | float      | NO   |     | 0       |                |
| quad_feed_A   | tinyint(1) | NO   |     | 0       |                |
| quad_feed_B   | tinyint(1) | NO   |     | 0       |                |
| quad_feed_C   | tinyint(1) | NO   |     | 0       |                |
| interval_time | datetime   | YES  |     | NULL    |                |
+---------------+------------+------+-----+---------+----------------+
 *
 *
15 rows in set (0.00 sec)
 * SELECT * FROM pcc_interval_averages ORDER BY interval_time DESC LIMIT 1
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


  $result = $conn->query("SELECT * FROM pcc_interval_averages ORDER BY interval_time DESC LIMIT 1");

if(mysqli_num_rows($result)>0) {

    while($row = $result->fetch_assoc()) {
        $results[] = array(
            'row_id' => $row['row_id'],
            'read_id' => $row['read_id'],
            'Pf_A' => $row['Pf_A'],
            'Pf_B' => $row['Pf_B'],
            'Pf_C' => $row['Pf_C'],
            'kW_A' => $row['kW_A'],
            'kW_B' => $row['kW_B'],
            'kW_C' => $row['kW_C'],
            'kvar_A' => $row['kvar_A'],
            'kvar_B' => $row['kvar_B'],
            'kvar_C' => $row['kvar_C'],
            'var_A' => $row['var_A'],
            'var_B' => $row['var_B'],
            'var_C' => $row['var_C'],
            'quad_feed_A' => $row['quad_feed_A'],
            'quad_feed_B' => $row['quad_feed_B'],
            'quad_feed_C' => $row['quad_feed_C'],
            'interval_time' => $row['interval_time']

        );
    }
}

echo $json = json_encode($results);

//$conn->close();

?>