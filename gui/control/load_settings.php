<?php
include_once('../functions/session.php');
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 8/17/2017
 * Time: 4:19 PM
 */


include_once('../functions/mysql_connect.php');

$resultNetControl = $conn->query("SELECT enabled FROM daemon_control WHERE name LIKE 'icpcond'");
while ($row = $resultNetControl->fetch_assoc()) {
    $netEnabled = $row['enabled'];
}

$meterpcc2 = 'no';
$metersolar = 'no';

$result = $conn->query("SELECT meter_role,ct_orientation_reversed FROM energy_meter");

$meters = array();
while ($row = $result->fetch_assoc()) {
    $ct_orientation_reversed = $row['ct_orientation_reversed'];

    $meters[$row['meter_role']] = true;

    if ('pcc2' == $row['meter_role']) {
        //echo  $row['meter_role'];
        $meterpcc2 = 'yes';
    }
    if ('solar' == $row['meter_role']) {
        // echo  $row['meter_role'];
        $metersolar = 'yes';
    }
    if ('battery' == $row['meter_role']) {
        // echo  $row['meter_role'];
        $meterbattery = 'yes';
    }
}

$all_meter_roles = array("solar", "battery", "pcc", "pcc2", "pcc_combo");
$meter_roles = array();
foreach ($all_meter_roles as $role) {
    if ($meters[$role]) {
        array_push($meter_roles, $role);
        if ($role == 'pcc2')
            array_push($meter_roles, 'pcc_combo');
    }
}

$resultFeed = $conn->query("SELECT feed_name FROM power_regulators");

$inverter_feeds = array();
while ($row = $resultFeed->fetch_assoc()) {


    $inverter_feeds[$row['feed_name']] = $row;

    if ('A' == $row['feed_name']) {
        //echo  $row['meter_role'];
        $FeedAEnabled = 'yes';
    }
    if ('B' == $row['feed_name']) {
        //echo  $row['meter_role'];
        $FeedBEnabled = 'yes';
    }
    if ('C' == $row['feed_name']) {
        //echo  $row['meter_role'];
        $FeedCEnabled = 'yes';
    }
}

$resultSystemEnable = $conn->query("SELECT system_enable FROM gateway");

while ($row = $resultSystemEnable->fetch_assoc()) {

    if ($row['system_enable'] == 0) {
        $system_enable = 1;
    } else {
        $system_enable = 0;
    }
}

$result = $conn->query("SELECT threshold FROM meter_feeds WHERE meter_role = 'pcc_combo' AND threshold IS NOT NULL");
$pcc_combo = mysqli_num_rows($result) > 0;

