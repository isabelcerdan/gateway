<?php
include_once('../functions/session.php');

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/22/2016
 * Time: 2:11 PM
 */

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */
// Access restriction
include_once('../functions/restrict_privilage_access.php');

include_once('../functions/mysql_connect.php');

$error = '';
$enabled = '';

$enabled = mysqli_real_escape_string($conn, $_REQUEST['enabled']);
$enabled = filter_var($enabled, FILTER_SANITIZE_STRING);

$power_regulator_id = mysqli_real_escape_string($conn, $_REQUEST['id']);
$power_regulator_id = filter_var($power_regulator_id, FILTER_SANITIZE_STRING);

// toggle enable //
if($enabled == '1'){
    $enabled = '0';
}else {
    $enabled = '1';
}


$sql = "UPDATE power_regulators SET enabled = " . $enabled . " WHERE id = '$power_regulator_id'";
if (!mysqli_query($conn, $sql)) {
    die('<br>Error: ' . mysqli_error($conn));
} else {
    if ($conn->insert_id != 0) {
        $gateway_id = $conn->insert_id;
    }
    //mysqli_close($con);
    header('Location: /main.php?system_enable=' . $gateway_id);
}

?>