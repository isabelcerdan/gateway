<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 *
 *
+-------------------+--------------+------+-----+-------------------+-------+
|	Field	|	Type	|	Null	|	Key	|	Default	|	Extra	|
+-------------------+--------------+------+-----+-------------------+-------+
|	target_interface	|	varchar(100)	|	NO	|	PRI	|	NULL	|	|
|	nickname	|	varchar(100)	|	YES	|	|	NULL	|	|
|	model_number	|	varchar(100)	|	YES	|	|	NULL	|	|
|	apparent_part	|	varchar(100)	|	YES	|	|	NULL	|	|
|	IP_Address	|	varchar(16)	|	YES	|	|	NULL	|	|
|	mac_address	|	varchar(18)	|	YES	|	|	NULL	|	|
|	baud	|	int(11)	|	YES	|	|	38400	|	|
|	tcp_port	|	int(6)	|	YES	|	|	9001	|	|
|	tcp_port_serial_1	|	int(6)	|	YES	|	|	9001	|	|
|	tcp_port_serial_2	|	int(6)	|	YES	|	|	9002	|	|
|	enabled	|	tinyint(1)	|	YES	|	|	1	|	|
|	tunnel_open	|	tinyint(1)	|	YES	|	|	0	|	|
|	started	|	tinyint(1)	|	YES	|	|	0	|	|
|	updated	|	tinyint(1)	|	YES	|	|	0	|	|
|	pid	|	tinyint(1)	|	YES	|	|	0	|	|
|	update_time	|	timestamp	|	NO	|	|	CURRENT_TIMESTAMP	|	|
+-------------------+--------------+------+-----+-------------------+-------+

 */

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

function display_meter_role($meter_role, $meter_roleDB) {
    $role = $meter_role;
    if ($meter_role == 'pcc' && (array_key_exists('pcc2', $meter_roleDB)))
        $role = 'pcc1';
    return strtoupper($role);
}

$result = $conn->query("select brainboxes.*,energy_meter.*,brainboxes.mac_address as mac from brainboxes right join energy_meter on address = target_interface;");


while($row = $result->fetch_assoc()) {
    $brainbox_idDB[] = $row['brainbox_id'];
    $target_interfaceDB[] = $row['target_interface'];
    $nicknameDB[]  = $row['nickname'];
    $meter_roleDB[] = $row['meter_role'];
    $model_numberDB[]  = $row['model_number'];
    $apparent_partDB[]  = $row['apparent_part'];
    $IP_AddressDB[] = ($row['meter_type'] == 'remote') ? $row['address'] : $row['IP_Address'];
    $mac_addressDB[] = $row['mac'];
    $baudDB[] = $row['baud'];
    $tcp_portDB[] = $row['tcp_port'];
    $tcp_port_serial_1DB[] = $row['tcp_port_serial_1'];
    $tcp_port_serial_2DB[] = $row['tcp_port_serial_2'];
    $enabledDB[] = $row['enabled'];
    $tunnel_openDB[] = ($row['meter_type'] == 'veris_brainbox') ? $row['brainboxes.pid'] : '';
    $startedDB[] = $row['started'];
    $updatedDB[] = $row['updated'];
    $pidDB[] = $row['pid'];
    $update_timeDB[] = $row['update_time'];
    $meter_onlineDB[] = $row['online'];
    $meter_typeDB[] = $row['meter_type'];
}

//mysqli_close($con);

$meter_type_display = array(
    'remote' => 'Remote',
    'veris_brainbox' => 'Veris/Brainbox',
    'apparent' => 'Apparent'
);


for($i = 0;$i <  count($target_interfaceDB); $i++) {
    echo "<tr class='lineHeight'>";
    echo "<td class='text-left'>" . display_meter_role($meter_roleDB[$i], $meter_roleDB) . "</td>";
    echo "<td class='text-left'>" . $meter_type_display[$meter_typeDB[$i]] . "</td>";
    echo "<td class='text-left'>";
    if($meter_onlineDB[$i] == 1) {
        echo '<span style="color: green" class="glyphicon glyphicon-one-small-fine-dot"></span>';
    }else {
        echo '<span style="color: red;" class="glyphicon glyphicon-one-small-fine-dot"></span>';
    }
    echo "</td>";
    echo "<td class='text-left'>$IP_AddressDB[$i]</td>";
    echo "<td class='text-left'>$enabledDB[$i]</td>";
    echo "<td class='text-left'>$tunnel_openDB[$i]</td>";
    echo "<td class='text-left'>$startedDB[$i]</td>";
    echo "<td class='text-left'>$pidDB[$i]</td>";
    echo "</tr>";
}

?>