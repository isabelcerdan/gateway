<?php


session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 *
 *            feed: B
 * pccNetPower: 0
 * solarPower: 0
 * curtailSetPoint: 100
 * enabled: 1
 * started: 0
 * updated: 0
 * pid: NULL
 * update_time: 2016-07-29 14:20:54
 *
 */
include_once('../functions/session.php');
include_once('../functions/mysql_connect.php');
include_once('../control/get_system.php');
include_once('load_settings.php');
/*
$result = $conn->query("
SELECT pr.feed_name, pr.curtail_value, pr.ramp_rate, dc.enabled, dc.pid, emi.se
FROM power_regulators pr
INNER JOIN daemon_control dc ON dc.name = CONCAT('powerregulatord-', pr.id)
INNER JOIN ess_units eu on eu.power_regulator_id = pr.id
INNER JOIN ess_master_info emi on emi.ess_id = eu.id");

$result = $conn->query("SELECT pr.*,pr.id AS power_regulator_id, dc.enabled, dc.pid, emi.active_power_cmd
FROM power_regulators pr
LEFT JOIN daemon_control dc ON dc.name = CONCAT('powerregulatord-', pr.id)
LEFT JOIN ess_units eu on eu.power_regulator_id = pr.id
LEFT JOIN ess_master_info emi on emi.ess_id = eu.id se
");

*/


$result = $conn->query("SELECT pr.*,pr.id AS power_regulator_id, pr.name AS prname, dc.enabled, dc.pid
FROM power_regulators pr
LEFT JOIN daemon_control dc ON dc.name = CONCAT('powerregulatord-', pr.id)

");

function get_real_power($gateway_perspective, $row, $key) {
    $value = (float)$row[$key];
    if ($gateway_perspective == 'production')
        $value = -$value;
    return number_format($value, 3, '.', '');
}

while ($row = $result->fetch_assoc()) {
    $feedDB[] = $row['feed_name'];
    $prname[] = $row['prname'];
    $curtailSetPointDB[] = round($row['curtail_value']);
    $ramp_rateDB[] = $row['ramp_rate'];
    $enabledDB[] = $row['enabled'];
    $daemon_startedDB[] = $row['started'];
    $pidDB[] = $row['pid'];
    $update_timeDB[] = $row['update_time'];
    $active_power_cmdDB[] = $row['active_power_cmd'];
    $power_regulator_id[] = $row['power_regulator_id'];
    //$timePHP[] = date("Y-m-d H:i:s");
}


$resultNXO = $conn->query(" SELECT * FROM nxo_status ORDER BY Update_Time LIMIT 1");

while ($row = $resultNXO->fetch_assoc()) {
    $Export_Solar_1_Delta = get_real_power($gateway_perspective, $row, 'Export_Solar_1_Delta');
    $Export_PCC_1_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_1_Delta');
    $Export_PCC_2_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_2_Delta');
    $Export_PCC_Combo_Delta = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_Delta');
    $Export_Solar_1_Delta = get_real_power($gateway_perspective, $row, 'Export_Solar_1_Delta');

    $Curtail_PCC_Combo_A = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_A');
    $PCC_Combo_A_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_A_Threshold');
    $Export_PCC_1_[0] = get_real_power($gateway_perspective, $row, 'Export_PCC_1_A');
    $Export_PCC_2_[0] = get_real_power($gateway_perspective, $row, 'Export_PCC_2_A');
    $Export_PCC_Combo_[0] = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_A');
    $Export_Solar_1_[0] = get_real_power($gateway_perspective, $row, 'Export_Solar_1_A');

    $Curtail_PCC_Combo_B = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_B');
    $PCC_Combo_B_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_B_Threshold');
    $Export_PCC_1_[1] = get_real_power($gateway_perspective, $row, 'Export_PCC_1_B');
    $Export_PCC_2_[1] = get_real_power($gateway_perspective, $row, 'Export_PCC_2_B');
    $Export_PCC_Combo_[1] = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_B');
    $Export_Solar_1_[1] = get_real_power($gateway_perspective, $row, 'Export_Solar_1_B');

    $Curtail_PCC_Combo_C = get_real_power($gateway_perspective, $row, 'Curtail_PCC_Combo_C');
    $PCC_Combo_C_Threshold = get_real_power($gateway_perspective, $row, 'PCC_Combo_C_Threshold');
    $Export_PCC_1_[2] = get_real_power($gateway_perspective, $row, 'Export_PCC_1_C');
    $Export_PCC_2_[2] = get_real_power($gateway_perspective, $row, 'Export_PCC_2_C');
    $Export_PCC_Combo_[2] = get_real_power($gateway_perspective, $row, 'Export_PCC_Combo_C');
    $Export_Solar_1_[2] = get_real_power($gateway_perspective, $row, 'Export_Solar_1_C');
}



if (count($feedDB) == 0) {
    $feedDB = 1;
}

for ($i = 0; $i < count($feedDB); $i++) {

    $sql = "select Total_Net_Instantaneous_Real_Power_FP, Real_Power_A_FP, Real_Power_B_FP, Real_Power_C_FP,meter_role from meter_readings where meter_role IN (Select meter_name from energy_meter WHERE meter_id IN (SELECT meter_id FROM ess_units WHERE power_regulator_id = '$power_regulator_id[$i]' ) AND data_type = '1' )";
    $result = $conn->query($sql);
    // Clear battery arrays
    unset($Battery_Total_Net_Instantaneous_Real_Power_FP);
    unset($Battery_Real_Power_A_FP);
    unset($Battery_Real_Power_B_FP);
    unset($Battery_Real_Power_C_FP);
    unset($meter_role_Battery);
    if(mysqli_num_rows($result)>0) {


        while ($row = $result->fetch_assoc()) {

            $Battery_Total_Net_Instantaneous_Real_Power_FP[] = get_real_power($gateway_perspective, $row, 'Total_Net_Instantaneous_Real_Power_FP');
            $Battery_Real_Power_A_FP[] = get_real_power($gateway_perspective, $row, 'Real_Power_A_FP');
            $Battery_Real_Power_B_FP[] = get_real_power($gateway_perspective, $row, 'Real_Power_B_FP');
            $Battery_Real_Power_C_FP[] = get_real_power($gateway_perspective, $row, 'Real_Power_C_FP');
            $meter_role_Battery[] = $row['meter_role'];
        }
    }
    $sql2 = "select  emi.active_power_cmd,em.meter_id, em.meter_name from energy_meter em
INNER JOIN ess_units ess ON ess.meter_id = em.meter_id
INNER JOIN  ess_master_info emi ON ess.id = emi.ess_id  WHERE power_regulator_id = '$power_regulator_id[$i]'  GROUP BY em.meter_name order by em.meter_name";

           // WHERE meter_id IN (SELECT meter_id FROM ess_units WHERE power_regulator_id = '14') GROUP by meter_id\";
    $result2 = $conn->query($sql2);
    unset($active_power_cmd);
    if(mysqli_num_rows($result2)>0) {

        while ($row = $result2->fetch_assoc()) {
            $active_power_cmd[] = $row['active_power_cmd'];
        }
    }

    $updatedFeed = '';
    if ($updatedDB[$i] == 0) {
        $updatedFeed = 'Enable';
    } else {
        $updatedFeed = 'Disable';
    }

    echo "<tr class='lineHeight'>";

    $ramp_sign = ($ramp_rateDB[$i] >= 0) ? '+' : '';

    // If solar delta
    if ($feedDB[0] == 'Aggregate') {
        echo "<td class='text-center'>$prname[$i]</td>
        <td class='text-center'>Aggregate</td> 
       
           <td class='text-center'>$Export_PCC_1_Delta <span class='smallerText'>kW</span></td>";
        if ($meterpcc2 == 'yes') {
            echo "<td class='text-center'> $Export_PCC_2_Delta <span class='smallerText'>kW</span></td>";
            if ($pcc_combo) {
                echo "<td class='text-center'> $Export_PCC_Combo_Delta<span class='smallerText'>kW</span></td>";
            }
        }
        echo "
        <td class='text-center' >$Export_Solar_1_Delta<span class='smallerText'>kW</span></td>
        <td class='text-center'> $curtailSetPointDB[$i]<span class='smallerText'>%</span> / $ramp_sign$ramp_rateDB[$i]<span class='smallerText'>%</span></td>";
        if($meterbattery == 'yes'){
            echo "<td class='text-center' >";

            for ($j = 0; $j < count($meter_role_Battery); $j++) {
                echo "$meter_role_Battery[$j] | $Battery_Total_Net_Instantaneous_Real_Power_FP[$j]  <span class='smallerText'>kW</span><br>" ;
            }
            echo" </td>
            <td class='text-center'>";
            for ($k = 0; $k < count($meter_role_Battery); $k++) {
                echo " $active_power_cmd[$k]  <span class='smallerText'>kW</span><br>" ;
            }

            echo" </td>
     
            ";
        }
    } else {
        echo "<td class='text-center'>$prname[$i]</td>
            <td class='text-center'>$feedDB[$i]  </td> 
           <td class='text-center'>" . $Export_PCC_1_[$i]  . "<span class='smallerText'>kW</span></td>";
        if ($meterpcc2 == 'yes') {
            echo "<td class='text-center'>" . $Export_PCC_2_[$i]  ."<span class='smallerText'>kW</span></td>";
            if ($pcc_combo) {
                echo "<td class='text-center'>$active_power_cmd[$i]<span class='smallerText'>kW</span></td>";
            }
        }
        echo "
        <td class='text-center' >$Export_Solar_1_[$i]<span class='smallerText'>kW</span></td>
        <td class='text-center'>$curtailSetPointDB[$i]<span class='smallerText'>%</span> / $ramp_sign$ramp_rateDB[$i]<span class='smallerText'>%</span></td>
        ";
        if($meterbattery == 'yes'){
            echo "<td class='text-center' >";

            for ($j = 0; $j < count($meter_role_Battery); $j++) {
                echo "$meter_role_Battery[$j] | $Battery_Total_Net_Instantaneous_Real_Power_FP[$j]  <span class='smallerText'>kW</span><br>" ;
            }
            echo" </td>
            <td class='text-center'>";
            for ($k = 0; $k < count($meter_role_Battery); $k++) {
                echo " $active_power_cmd[$k]  <span class='smallerText'>kW</span><br>" ;
            }

             echo" </td>
     
            ";
        }
    }
    echo "
       
        <td class='text-center'>$pidDB[$i] </td>
      
    </tr>";
}

?>