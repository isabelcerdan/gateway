<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 7/22/2016
 * Time: 2:11 PM
 */

/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */
// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$error = '';

  $bmu_state = mysqli_real_escape_string($conn, $_REQUEST['bmu_state']);
  $bmu_state = filter_var($bmu_state, FILTER_SANITIZE_STRING);

    $bmu_id = mysqli_real_escape_string($conn, $_REQUEST['bmu_id']);
    $bmu_id = filter_var($bmu_id, FILTER_SANITIZE_STRING);

if($bmu_state == 'Maintenance') {

    $set_state_of_bmu ='Maintenance';
}
elseif($bmu_state == 'Unrestricted') {

    $set_state_of_bmu ='Unrestricted';
}
else{
    $set_state_of_bmu = '';
}
//Unrestricted

//confirm that either a 1 or 0 is submitted and if not do nothing but redirect
if(($set_state_of_bmu != '') AND ($bmu_id != '')) {
    
    $sql = "UPDATE aess_bmu_data set state = '$set_state_of_bmu' where bmu_id = '$bmu_id'";


    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
       // header('Location: /main.php?Meter_enable_all=error');
    }
    else {
        if ($conn->insert_id != 0) {
            echo $conn->insert_id;
        }
    }
        //header('Location: /main.php?Meter_enable_all=' .$meter_enable_all);

}else {
   // header('Location: /main.php?Feed_enable_all=error');
    echo "update_error";
}

?>