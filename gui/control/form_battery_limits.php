<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

include_once ('../functions/mysql_connect.php');

$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);

if($ess_id  == ''){
    $ess_id =1;
}

$SOC_charge_limit = mysqli_real_escape_string($conn, $_REQUEST['SOC_charge_limit']);
$SOC_charge_limit = filter_var($SOC_charge_limit, FILTER_SANITIZE_STRING);

$SOC_discharge_limit= mysqli_real_escape_string($conn, $_REQUEST['SOC_discharge_limit']);
$SOC_discharge_limit = filter_var($SOC_discharge_limit, FILTER_SANITIZE_STRING);

$charge_rate_limit= mysqli_real_escape_string($conn, $_REQUEST['charge_rate_limit']);
$charge_rate_limit= filter_var($charge_rate_limit, FILTER_SANITIZE_STRING);

$discharge_rate_limit= mysqli_real_escape_string($conn, $_REQUEST['discharge_rate_limit']);
$discharge_rate_limit= filter_var($discharge_rate_limit, FILTER_SANITIZE_STRING);



$SOC_charge_limit= $SOC_charge_limit *.01;
$SOC_discharge_limit = $SOC_discharge_limit *.01;

//$resultAlarm = $conn->query("SELECT enabled,soc_charge_limit,soc_discharge_limit  FROM ess_units");
// make sure we only get good data
if ($SOC_charge_limit != '') {
    $sql = "UPDATE ess_units SET soc_charge_limit = '$SOC_charge_limit' WHERE id = '$ess_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
echo "updated:soc_charge_limit ";
}elseif ($SOC_discharge_limit != '') {
    $sql = "UPDATE ess_units SET soc_discharge_limit = '$SOC_discharge_limit' WHERE id = '$ess_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
}elseif ($charge_rate_limit != '') {
    $sql = "UPDATE ess_units SET charge_rate_limit = '$charge_rate_limit' WHERE id = '$ess_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
}
elseif ($discharge_rate_limit != '') {
    $sql = "UPDATE ess_units SET discharge_rate_limit = '$discharge_rate_limit' WHERE id = '$ess_id'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
}
else {
   exit();
}

?>