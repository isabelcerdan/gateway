<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 12/13/2016
 * Time: 11:53 AM
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');


include_once ('../functions/mysql_connect.php');

$error = '';
$set_monitor_control = '';


$monitor_control = mysqli_real_escape_string($conn, $_GET['monitor_control']);
$monitor_control = filter_var($monitor_control, FILTER_SANITIZE_NUMBER_INT);


if($monitor_control == '1' ){
    $set_monitor_control = '1';
}
elseif($monitor_control == '0' ){
    $set_monitor_control = '0';
}else {
    $set_monitor_control = '';
}

if($set_monitor_control != '') {
    $sql = "UPDATE daemon_control set enabled = '$set_monitor_control' where name = 'essmonitord'";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    $sql = "UPDATE daemon_control set enabled = '$set_monitor_control' where name = 'esscontrollerd' ";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }
    $sql = "UPDATE daemon_control set enabled = '$set_monitor_control' where name = 'essalarmd' ";

    if (!mysqli_query($conn,$sql)) {
        die('<br>Error: ' . mysqli_error($conn));
    }

    echo "cess_control=$set_monitor_control";
}

?>