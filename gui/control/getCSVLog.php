<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 9/7/2016
 * Time: 4:34 PM
 *
 * 2016-09-07 16:01:52,806 Monitor [INFO]: ***MONITOR DAEMON STARTED WITH NEW LOG AT 04:01:52 (UTC = 1473289312)***
 */

// Access restriction
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

$filename = '/var/log/apparent/gateway.log';


if( file_exists($filename) ) {

    header('Content-Type: application/csv');
    header('Content-Disposition: attachement; filename="filename.csv"');

    $logArray = file_get_contents($filename);

    // Add comma at correct places for CSV
    $logArray = explode("\n",$logArray);
    //print_r($logArray);
   // echo $logArray;

    foreach($logArray as $lineOfLog){
       // $lineOfLog= filter_var($lineOfLog, FILTER_SANITIZE_STRING);
        $lineOfLog =   preg_replace('/,/', 'replaceMeNowProgrammer', $lineOfLog, 1); // replace first comma
        $lineOfLog = str_ireplace(',', ' ', $lineOfLog ); // remove call commas so we don't get extra lines
        $lineOfLog = str_ireplace("[INFO]:",",[INFO]:,",$lineOfLog);
        $lineOfLog= str_ireplace("[CRIT]:",",[CRIT]:,",$lineOfLog);
        $lineOfLog = str_ireplace("[ERR]:",",[ERR]:,",$lineOfLog);
        $lineOfLog= str_ireplace("replaceMeNowProgrammer",",",$lineOfLog); // put first comma back
        echo  $lineOfLog;
        echo "\r\n";

    }

}else {
    echo "File not accessible ";
}

?>

