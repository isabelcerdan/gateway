<?php
include_once ('../functions/session.php');
include_once ('../functions/restrict_privilage_access.php');

$tableName= 'aess_bmu_data';

include_once ('../functions/mysql_connect.php');

/////////////////////////////////////////////////
// This should be put into one big join....   //
/////////////////////////////////////////////////

$ess_id= mysqli_real_escape_string($conn, $_REQUEST['ess_id']);
$ess_id = filter_var($ess_id, FILTER_SANITIZE_STRING);

if($ess_id == '') {
    $ess_id = 1;
}

$sql = "SELECT pu.bmu_id_A,pu.bmu_id_B, pu.id AS pu_id, pu.ess_unit_id, ab.* FROM pcs_units pu, aess_bmu_data ab";
$sql .= " WHERE pu.ess_unit_id = '$ess_id' AND (ab.bmu_id = pu.bmu_id_A OR (ab.bmu_id = pu.bmu_id_B AND pu.bmu_id_B != 0))";
$result = $conn->query($sql);
$rows = array();
while($r = mysqli_fetch_assoc($result)) {
//echo $r['pu_id'];
    // removed curtail,
    $resultInverters = $conn->query("SELECT inverters_id,serialNumber,pcs_unit_id,watts,IP_Address FROM inverters WHERE pcs_unit_id = '" .$r['pu_id'] . "'");

    if(mysqli_num_rows($resultInverters)>0) {
        $counter =1;
        while ($row = $resultInverters->fetch_assoc()) {


           //array_push($r, array('serialNumber' => $row['serialNumber']));
          //  array_push($r, 'serialNumber', $row['serialNumber']);
            //$inverters_id = $row['inverters_id'];
            $r['serialNumber_' .$counter ] =$row['serialNumber'];
            //$r['inverters_id_' .$counter ] =$row['inverters_id'];
            $r['IP_Address_' .$counter ] =$row['IP_Address'];
            $r['pcs_unit_id_' .$counter ] =$row['pcs_unit_id'];
            $r['watts_' .$counter ] =$row['watts'];
            $r['curtail_' .$counter ] =$row['curtail'];
            $counter++;
        }

        $resultChargers = $conn->query("select acm.charger_id AS charger_id,acc.chassis_id AS chassis_id, acc.ip_addr AS IP_address from aess_charger_module acm, aess_charger_chassis acc where acc.chassis_id = acm.chassis_id AND pcs_unit_id ='" .$r['pu_id'] . "'");

        if(mysqli_num_rows($resultChargers)>0) {

            while ($row3 = $resultChargers->fetch_assoc()) {
                $r['charger_id'] = $row3['charger_id'];
                $r['chassis_id'] = $row3['chassis_id'];
                $r['IP_address'] = $row3['IP_address'];
            }
        }
    }
            //echo $r['pcs_unit_id'] . '<br>';
    $a['data'][] = $r;
}
//print_r($a);
echo json_encode($a);
