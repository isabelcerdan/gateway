<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 8/10/2017
 * Time: 2:42 PM
 */
include_once('../functions/session.php');
include_once('../functions/mysql_connect.php');
include_once('load_settings.php');

include_once('../control/get_system.php');

$result = $conn->query("SELECT * FROM nxo_status");
$nxo_status = $result->fetch_assoc();

$result = $conn->query("SELECT * FROM meter_feeds");

$role2name = array(
    'pcc' => 'PCC_1',
    'pcc2' => 'PCC_2',
    'pcc_combo' => 'PCC_Combo',
    'solar' => 'Solar_1');

$meter_feeds = array();
while ($row = $result->fetch_assoc()) {
    $meter_role = $row['meter_role'];
    $feed_name = $row['feed_name'];
    if (!array_key_exists($meter_role, $meter_feeds)) {
        $meter_feeds[$meter_role] = array();
    }
    if (!array_key_exists($feed_name, $meter_feeds[$meter_role])) {
        $meter_feeds[$meter_role][$feed_name] = array();
    }
    $meter_feeds[$meter_role][$feed_name] = $row;
}

// Only show PCC Combo if any PCC combo thresholds are set
$display_pcc_combo = False;
foreach ($meter_feeds['pcc_combo'] as $feed) {
    if ($feed['threshold']) {
        $display_pcc_combo = True;
        break;
    }
}

if (!$display_pcc_combo) {
    foreach ($meter_roles as $k => $v) {
        print_r($k);
        if ($v == 'pcc_combo') {
            unset($meter_roles[$k]);
            break;
        }
    }
}

$feed_names = array("A", "B", "C", "Delta");

foreach ($feed_names as $feed_name) {
    echo "<tr>\n";
    echo "\t<td><strong>$feed_name</strong></td>";
    foreach ($meter_roles as $meter_role) {
        $threshold = null;
        if (array_key_exists($meter_role, $meter_feeds) &&
            array_key_exists($feed_name, $meter_feeds[$meter_role])) {
            $threshold = $meter_feeds[$meter_role][$feed_name]['threshold'];
            if ($gateway_perspective == 'production')
                $threshold = -$threshold;
        }
        if ($meter_role != 'solar') {
            $display_threshold = $threshold ?
                number_format((float)$threshold,3) : "<span class='text-muted'>N/A</span>";
            echo "\t<td class='text-right'>" . $display_threshold . "</td>\n";
        }
        $column_name = "Export_" . $role2name[$meter_role] . "_" . $feed_name;
        $export = number_format((float)$nxo_status[$column_name], 3);
        if ($gateway_perspective == 'production')
            $export = -$export;
        $display_export = $threshold ?
            $export . "<span class='smallerText'>kW</span>" :
            "<span class='text-muted'>" . $export . "<span class='smallerText'>kW</span></span>";
        echo "\t<td>" . $display_export . "</td>\n";
    }
    echo "</tr>\n";
}
?>

