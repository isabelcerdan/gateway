<?php
session_start();
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 6/01/2017
 * Time: 2:40 PM
 *
 *
+-------------------+-------------+------+-----+----------+-------+
| Field             | Type        | Null | Key | Default  | Extra |
+-------------------+-------------+------+-----+----------+-------+
| mac_addr          | varchar(18) | NO   | PRI | NULL     |       |
| ip_addr           | varchar(20) | YES  |     | NULL     |       |
| device_name       | varchar(32) | YES  |     | PET-7202 |       |
| modbus_id         | tinyint(2)  | NO   |     | 1        |       |
| di0_tripped_value | tinyint(1)  | NO   |     | 1        |       |
| di0_register      | tinyint(1)  | YES  |     | NULL     |       |
| di1_tripped_value | tinyint(1)  | NO   |     | 1        |       |
| di1_register      | tinyint(1)  | YES  |     | NULL     |       |
| di2_tripped_value | tinyint(1)  | NO   |     | 1        |       |
| di2_register      | tinyint(1)  | YES  |     | NULL     |       |
+-------------------+-------------+------+-----+----------+-------+
*/

include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');


$result = $conn->query("select * from icpcon_devices");

$num_rows = mysqli_num_rows($result);

if($num_rows  > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<tr >";
            echo "<td  colspan'2' ><strong >" . $row['device_name'] . "</strong></td>";
        echo "</tr>";

        echo "<tr>";
            echo "<td class='text-center'>" . $row['di0_input_name'] .  "</td>";
            echo  "<td>";
            if($row['di0_register'] != $row['di0_tripped_value'] ) {
                echo ' <span style="color: green" class="glyphicon glyphicon-one-fine-dot"></span><span class="statusColor">Green</span>';
            }else {
                echo '<span style="color: red" class="glyphicon glyphicon-one-fine-dot"></span><span class="statusColor">Red</span> ';
            }
        echo "</td>";

        echo "<tr>";
        echo "<td class='text-center'>" . $row['di1_input_name'] .  "</td>";
        echo  "<td >";
        if($row['di1_register'] != $row['di1_tripped_value'] ) {
            echo ' <span style="color: green" class="glyphicon glyphicon-one-fine-dot"></span><span class="statusColor">Green</span>';
        }else {
            echo '<span style="color: red" class="glyphicon glyphicon-one-fine-dot"></span> <span class="statusColor">Red</span> ';
        }
        echo "</td>";

        echo "<tr>";
        echo "<td class='text-center'>" . $row['di2_input_name'] .  "</td>";
        echo  "<td >";
        if($row['di2_register'] != $row['di2_tripped_value'] ) {
            echo ' <span style="color: green;" class="glyphicon glyphicon-one-fine-dot"></span> <span class="statusColor">Green</span>';
        }else {
            echo '<span style="color: red;" class="glyphicon glyphicon-one-fine-dot"></span> <span class="statusColor">Red</span> ';
        }
        echo "</td>";
        
            echo "<tr>";
            echo "<td class='text-center'>" . $row['di3_input_name'] .  "</td>";
            echo  "<td >";
            if($row['di3_register'] != $row['di3_tripped_value'] ) {
                echo ' <span style="color: green;" class="glyphicon glyphicon-one-fine-dot"></span> <span class="statusColor">Green</span>';
            }else {
                echo '<span style="color: red;" class="glyphicon glyphicon-one-fine-dot"></span> <span class="statusColor">Red</span> ';
            }
            echo "</td>";
        }

}
        
?>
