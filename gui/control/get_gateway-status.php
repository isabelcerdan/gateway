<?php
/**
 * Created by PhpStorm.
 * User: AdamCadieux
 * Date: 4/22/2016
 * Time: 2:40 PM
 */
include_once ('../functions/session.php');
include_once ('../functions/mysql_connect.php');

$getHTMLData = $_REQUEST['data'];
$getHTMLData = filter_var($getHTMLData, FILTER_SANITIZE_STRING);
//
$type= $_REQUEST['type'];
$type = filter_var($type, FILTER_SANITIZE_STRING);

$result = $conn->query("select * from gateway WHERE gateway_id = 1");


while($row = $result->fetch_assoc()) {
   $gateway_id = $row['gateway_id'];
   $gateway_name  = $row['gateway_name'];
    $total_output_limit = $row['total_output_limit'];
    $system_enable= $row['system_enable'];
    $update_config= $row['update_config'];
    $address= $row['address'];
    $city= $row['city'];
    $state= $row['state'];
    //$SerialNumber= $row['SerialNumber'];
    $system_restart= $row['system_restart'];
   //$timePHP[] = date("Y-m-d H:i:s");
    }

//mysqli_close($con);

if($type == 'status') {
    if($getHTMLData == 'yes') {
        if($system_enable == 1){
            echo 'System Online';
        }elseif($system_enable == 0){
            echo 'System Offline';
        }else {
            echo "System status not updated";
        }

    }
}

if($type == 'restart') {

    if($getHTMLData == 'yes') {
        if($system_restart == 1){
            echo 'System Restarting';
        }elseif($system_restart == 0){
            echo 'No change';
        }else {
            echo "No restart info";
        }

    }
}

?>