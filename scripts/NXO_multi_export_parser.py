#!/usr/bin/env python
# encoding: utf-8
import os
import csv
from datetime import datetime, timedelta

# Data to define an export
THRESHOLD_A = -135 #-0.135  # -0.068  # 0
THRESHOLD_B = -135 #-0.135  # -0.068  # 0
THRESHOLD_C = 0  # 0.519  # 0
# How long an export event is considered a violation?
EXPORT_TIME_LIMIT = 30 # time in 100ms units
export_time_limit_sec = timedelta(seconds=3)


def get_num_branches(timing):  # This counts all of the line is the timing file and divides it by 3 because that's how many branches there are
    check = False
    #line = timing.readline()# reads the first line of the text file
    #count = 1# counter to find number of branches, starts at 1 because the first line has already been read
    count = 0
    while check is False:# goes until the end of the text file is reached
        line = timing.readline()
        if line == "":# checks to see if the end of the text file is reached, if it has then the while loop is signified to end
            check = True
        else:
            count += 1
    count = count / 4
    return count


def get_branches_array(timing, num_branches):
    branches = [0 for x in range(num_branches)]  # a 1d array that is the length of the number of branches that were tested
    timing.seek(0)  # brings the reader to the top of the text file again
    final = num_branches * 4
    line = timing.readline().strip()  # writes the first branch's name down and then starts the for loop on the next line
    branches[0] = line
    count = 0
    pos = 1  # starts at 1 because the first position was taken by the first branch name above
    for m in range(final - 1):  # one less because the first line was already read
        line = timing.readline().strip()
        if count == 3:  # this is where the branch names are suppose to be located
            branches[pos] = line  # adds the name of the branch to the array
            pos += 1
            count = 0
        else:
            count += 1
    return branches


def get_timing_array(timing, num_branches):
    changing_tm = [0 for x in range(num_branches * 2)]  # a 1d array that is the length that is one less than the number of branches because this is when a getgat is called
    timing.seek(0)  # brings the reader to the top of the text file again
    final = (num_branches * 4)
    count = 0
    pos = 0
    for m in range(final):
        line = timing.readline().strip()
        if count == 3:
            count = 0
        elif count == 1 or count == 2:
            changing_tm[pos] = line  # adds the date and time to the array
            pos += 1
            count += 1
        else:
            count += 1
    return changing_tm


def get_branch_heads(timing, num_branches):  # fills in the branch hashs into the array
    branch_heads = [0 for y in range(num_branches)]
    timing.seek(0)
    final = num_branches * 4
    count = 0
    pos = 0
    for m in range(final):
        line = timing.readline().strip()
        if count == 3:
            branch_heads[pos] = line
            pos += 1
            count = 0
        else:
            count += 1
    return branch_heads


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


os.chdir("C:/code/gateway/test/system")
with open("switchTiming.txt", 'r') as timing:
    num_branches = get_num_branches(timing)
    branches = get_branches_array(timing, num_branches)
    changing_tm = get_timing_array(timing, num_branches)
    branchHeads = get_branch_heads(timing, num_branches)
os.chdir("C:/code/gateway/scripts")

# Feed A raw and summary data storage
feed_a_export = []
feed_a_net_pos_export = [0 for x in range(num_branches)]  # put into an array for each branch, starting everything at 0
for x in range(num_branches):
    feed_a_net_pos_export[x] = 0.0
violation_count_a = 0
violation_logs_a = []
feed_a_export_list = []
feed_a_export_duration = 0
feed_a_samples = []
feed_a_log_stamp = []
feed_a_time_stamp = []

# Feed B raw and summary data storage
feed_b_export = []
feed_b_net_pos_export = [0 for x in range(num_branches)]
for x in range(num_branches):
    feed_b_net_pos_export[x] = 0.0
violation_count_b = 0
violation_logs_b = []
feed_b_export_list = []
feed_b_export_duration = 0
feed_b_samples = []
feed_b_log_stamp = []
feed_b_time_stamp = []

# Feed C raw and summary data storage
feed_c_export = []
feed_c_net_pos_export = [0 for x in range(num_branches)]
for x in range(num_branches):
    feed_c_net_pos_export[x] = 0.0
violation_count_c = 0
violation_logs_c = []
feed_c_export_list = []
feed_c_export_duration = 0
feed_c_samples = []
feed_c_log_stamp = []
feed_c_time_stamp = []

feed_time_date = False
feed_time_stamp = False
feed_time_ms = False
export_col_a = False
export_col_b = False
export_col_c = False
net_pos_col_a = False
net_pos_col_b = False
net_pos_col_c = False

markerA = 0  # Marks which position each of the arrays are in for each feed
markerB = 0
markerC = 0
# Read the .csv files and store the data in the feed lists
for log in os.listdir("data"):  # os.getcwd()):

    # log = log.replace(' ', '')
    if log.endswith(".csv"):
        with open('data/%s' % log, "rb") as csv_file:
            # These are all column indexes where the required data lives in the .csv file
            export_col_a = False
            export_col_b = False
            export_col_c = False
            net_pos_col_a = False
            net_pos_col_b = False
            net_pos_col_c = False
            feed_time_stamp = False
            feed_time_ms = False
            feed_time_date = False

            reader = csv.reader(csv_file)
            header_row_found = False
            for row in reader:
                if not header_row_found:
                    if "Store No." in str(row[0]):
                        header_row_found = True
                        for index, column in enumerate(row):
                            if "P-1-Total" == str(column):
                                export_col_b = index
                            elif "P-2-Total" == str(column):
                                export_col_a = index
                            elif "P-3-Total" == str(column):
                                export_col_c = index
                            elif "WPp-1-Total" == str(column):
                                net_pos_col_b = index
                            elif "WPp-2-Total" == str(column):
                                net_pos_col_a = index
                            elif "WPp-3-Total" == str(column):
                                net_pos_col_c = index
                            elif "Time" == str(column):
                                feed_time_stamp = index
                            elif "Millisecond" == str(column):
                                feed_time_ms = index
                            elif "Date" == str(column):
                                feed_time_date = index
                else:
                    try:
                        if is_number(export_col_a):
                            if is_number(row[export_col_a]):
                                feed_a_export.append(row[export_col_a])
                                feed_a_samples.append(row[0])
                                feed_a_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_a_time_stamp.append(dt)
                                markerA += 1
                            if is_number(net_pos_col_a):
                                pos = 0
                                for x in range(num_branches):  # goes through each branch and checks which branch it is
                                    # by the times of the branch and the time that the net_pos_col_a as found
                                    if (feed_a_time_stamp[markerA - 1] > datetime.strptime(changing_tm[pos],
                                                                                     "%Y-%m-%d %H:%M:%S")) \
                                            and (feed_a_time_stamp[markerA - 1] < datetime.strptime(changing_tm[pos+1],
                                                                                              "%Y-%m-%d %H:%M:%S")):
                                        if row[net_pos_col_a] > float(feed_a_net_pos_export[x]):
                                            feed_a_net_pos_export[x] = float(row[net_pos_col_a])
                                    pos += 2
                        if is_number(export_col_b):
                            if is_number(row[export_col_b]):
                                feed_b_export.append(row[export_col_b])
                                feed_b_samples.append(row[0])
                                feed_b_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_b_time_stamp.append(dt)
                                markerB += 1
                            if is_number(net_pos_col_b):
                                pos = 0
                                for x in range(num_branches):
                                    if (feed_b_time_stamp[markerB-1] > datetime.strptime(changing_tm[pos],
                                                                                   "%Y-%m-%d %H:%M:%S")) \
                                            and (feed_b_time_stamp[markerB-1] < datetime.strptime(changing_tm[pos+1],
                                                                                            "%Y-%m-%d %H:%M:%S")):
                                        if row[net_pos_col_b] > float(feed_b_net_pos_export[x]):
                                            feed_b_net_pos_export[x] = float(row[net_pos_col_b])
                                    pos += 2
                        if is_number(export_col_c):
                            if is_number(row[export_col_c]):
                                feed_c_export.append(row[export_col_c])
                                feed_c_samples.append(row[0])
                                feed_c_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_c_time_stamp.append(dt)
                                markerC += 1
                            if is_number(net_pos_col_c):
                                pos = 0
                                for x in range(num_branches):
                                    if (feed_c_time_stamp[markerC-1] > datetime.strptime(changing_tm[pos],
                                                                                   "%Y-%m-%d %H:%M:%S")) \
                                            and (feed_c_time_stamp[markerC-1] < datetime.strptime(changing_tm[pos+1],
                                                                                            "%Y-%m-%d %H:%M:%S")):
                                        if row[net_pos_col_c] > float(feed_c_net_pos_export[x]):
                                            feed_c_net_pos_export[x] = float(row[net_pos_col_c])
                                    pos += 2
                    except Exception as error:
                        print "Error in %s: %s" % (log, error)
                        raise

positionA = 0  # these are for the position of the arrays that each of the feeds are at to be able to start where they
# left off with the last branch
positionB = 0
positionC = 0
len_list_a = 0
len_list_b = 0
len_list_c = 0

firstStart = datetime.strptime(changing_tm[0], "%Y-%m-%d %H:%M:%S")
position = 0
check = False
while check is False:  # This gets the starting position of the array to start at the time that the time that the time_splice.py started
    if feed_a_time_stamp[position] < firstStart:
        position += 1
    else:
        check = True

print "NXO overnight Dev-wall"
print " "
print "Inverter: "
print "   rev: "

prev = position
time_pos = 0
for i in range(num_branches):
    print " "
    print "Gateway: "
    print "   branch: " + branches[i] + ':'  # prints the name of the branch before showing the results of that branch
    print "   rev: ", branchHeads[i]
    print " "

    print "From: ", changing_tm[time_pos]  # The time that the branch of results starts if it is the first branch
    time_pos += 1
    print "To:   ", changing_tm[time_pos]  # The time that the branch of results ends
    print " "

    checkForError = 0

    pos = prev

    end = datetime.strptime(changing_tm[time_pos], "%Y-%m-%d %H:%M:%S")  # changes the srt to datetime to be able to
    # compare the datetime from the results to this one
    # Use the feed lists to check for export events

    # sets all of the max export lists to 0 so that the while loops can put the highest seconds of an export in the variable
    max_export_list_a = timedelta(0)
    max_export_list_b = timedelta(0)
    max_export_list_c = timedelta(0)
    min_export_list_a = timedelta(3)
    min_export_list_b = timedelta(3)
    min_export_list_c = timedelta(3)

    while feed_a_time_stamp[pos] < end:  # This goes through until it reaches the end of the last result of the last
        # second for the current branch because the results have more line of result for each second
        export_sample = feed_a_export[pos]  # From here to the next # are the variables that need to be increased while
        # going through the results. With out a for loop I needed to do these array increases by hand
        sample = feed_a_samples[pos]
        log = feed_a_log_stamp[pos]
        timestamp = feed_a_time_stamp[pos]  # This is the end
        if float(export_sample) <= THRESHOLD_A:
            if feed_a_export_duration > 0:  # This is the duration in consecutive samples
                # We had some export but just stopped.  See if it was a violation.
                if timestamp - export_start_time > export_time_limit_sec:
                    violation_count_a += 1
                    print "A: export for %s seconds " \
                          "in %s at time %s" % ((timestamp - export_start_time).total_seconds(),
                                                  log,
                                                  timestamp)
                    if log not in violation_logs_a:
                        violation_logs_a.append(log)
                    checkForError = 1
                # print feed_a_export_duration
                height = timestamp - export_start_time
                feed_a_export_list.append(height)
                feed_a_export_duration = 0
                if height > max_export_list_a:  # finds the max time for the exports
                    max_export_list_a = height
                if height < min_export_list_a:  # finds the min time for the exports
                    min_export_list_a = height
        else:
            # We have an export event.
            if feed_a_export_duration == 0:
                export_start_time = timestamp  # If first event, record start time
            feed_a_export_duration += 1
        pos += 1
        positionA += 1
    preprev = pos  # This sets the number for the start of the next branch because the a feed will most likely alway be
    # run so it is reliable to have this feed be the one to set where the array will start at next branch
    pos = prev  # This sets the position of the array back to the beginning of the current branches results

    while feed_b_time_stamp[pos] < end:
        export_sample = feed_b_export[pos]
        sample = feed_b_samples[pos]
        log = feed_b_log_stamp[pos]
        timestamp = feed_b_time_stamp[pos]
        if float(export_sample) <= THRESHOLD_B:
            if feed_b_export_duration > 0:  # This is the duration in consecutive samples
                # We had some export but just stopped.  See if it was a violation.
                if timestamp - export_start_time > export_time_limit_sec:
                # if feed_b_export_duration > EXPORT_TIME_LIMIT:
                    violation_count_b += 1
                    print "B: export for %s seconds " \
                          "in %s at time %s" % ((timestamp - export_start_time).total_seconds(),
                                                  log,
                                                  timestamp)
                    if log not in violation_logs_b:
                        violation_logs_b.append(log)
                    checkForError = 1
                # print feed_b_export_duration
                height = timestamp - export_start_time
                feed_b_export_list.append(height)
                feed_b_export_duration = 0
                if height > max_export_list_b:
                    max_export_list_b = height
                if height < min_export_list_b:
                    min_export_list_b = height
        else:
            # We have an export event.
            if feed_b_export_duration == 0:
                export_start_time = timestamp  # If first event, record start time
            feed_b_export_duration += 1
        pos += 1
        positionB += 1
    pos = prev

    while feed_c_time_stamp[pos] < end:
        export_sample = feed_c_export[pos]
        sample = feed_c_samples[pos]
        log = feed_c_log_stamp[pos]
        timestamp = feed_c_time_stamp[pos]
        if float(export_sample) <= THRESHOLD_C:
            if feed_c_export_duration > 0:  # This is the duration in consecutive samples
                # We had some export but just stopped.  See if it was a violation.
                if timestamp - export_start_time > export_time_limit_sec:
                # if feed_c_export_duration > EXPORT_TIME_LIMIT:
                    violation_count_c += 1
                    print "C: export for %s seconds " \
                          "in %s at time %s" % ((timestamp - export_start_time).total_seconds(),
                                                  log,
                                                  timestamp)
                    if log not in violation_logs_c:
                        violation_logs_c.append(log)
                    checkForError = 1
                # print feed_b_export_duration
                height = timestamp - export_start_time
                feed_c_export_list.append(height)
                feed_c_export_duration = 0
                if height > max_export_list_c:
                    max_export_list_c = height
                if height < min_export_list_c:
                    min_export_list_c = height
        else:
            # We have an export event.
            if feed_c_export_duration == 0:
                export_start_time = timestamp  # If first event, record start time
            feed_c_export_duration += 1
        pos += 1

    if checkForError == 1:
        # Print out the summary results
        print " "

    pos = prev
    if len(feed_a_export_list) > 0:
        print "Feed A had %s export events.\n " \
              "net positive export = %s wh\n " \
              "count of 3s violations = %s\n " \
              "average duration = %s seconds\n " \
              "max duration = %s seconds\n " \
              "min duration = %s seconds\n " % ((len(feed_a_export_list)),
                                                feed_a_net_pos_export[i],
                                                violation_count_a,
                                                (sum(feed_a_export_list, timedelta(0, 0, 0)) / len(
                                                    feed_a_export_list)).total_seconds(),
                                                max_export_list_a.total_seconds(),
                                                min_export_list_a.total_seconds())
    if len(feed_b_export_list) > 0:
        print "Feed B had %s export events.\n " \
              "net positive export = %s wh\n " \
              "count of 3s violations = %s\n " \
              "average duration = %s seconds\n " \
              "max duration = %s seconds\n " \
              "min duration = %s seconds\n " % ((len(feed_b_export_list)),
                                                feed_b_net_pos_export[i],
                                                violation_count_b,
                                                (sum(feed_b_export_list, timedelta(0, 0, 0)) / len(
                                                    feed_b_export_list)).total_seconds(),
                                                max_export_list_b.total_seconds(),
                                                min_export_list_b.total_seconds())
    if len(feed_c_export_list) > 0:
        print "Feed C had %s export events.\n " \
              "net positive export = %s wh\n " \
              "count of 3s violations = %s\n " \
              "average duration = %s seconds\n " \
              "max duration = %s seconds\n " \
              "min duration = %s seconds\n " % ((len(feed_c_export_list)),
                                                feed_c_net_pos_export[i],
                                                violation_count_c,
                                                (sum(feed_c_export_list, timedelta(0, 0, 0)) / len(
                                                    feed_c_export_list)).total_seconds(),
                                                max_export_list_c.total_seconds(),
                                                min_export_list_c.total_seconds())

    print 'FEED A violating logs:'
    for log in violation_logs_a:
        print log
    if len(violation_logs_a) == 0:
        print "None"
    print 'FEED B violating logs:'
    for log in violation_logs_b:
        print log
    if len(violation_logs_b) == 0:
        print "None"
    print 'FEED C violating logs:'
    for log in violation_logs_c:
        print log
    if len(violation_logs_c) == 0:
        print "None"

    time_pos += 1
    prev = preprev + 1  # This saves where the time_stamp arrays are at to prevent overlap when going to the next
    violation_count_a = 0
    violation_count_b = 0
    violation_count_c = 0
    violation_logs_a = []
    violation_logs_b = []
    violation_logs_c = []
    feed_a_export_list = []
    feed_b_export_list = []
    feed_c_export_list = []
    # branches results

    # for a,b in zip(feed_a_export_list, feed_b_export_list):
#     print "Feed A:\t< %s seconds\t\tFeed B:\t< %s seconds" % (str(a / 10.0),
#                                                               str(b / 10.0))