#!/usr/share/apparent/.py2-virtualenv/bin/python
from optparse import OptionParser
from datetime import datetime
import os
import time
import io


def try_number(value, default=None):
    if '.' in value:
        try:
            return float(value)
        except Exception as ex:
            try:
                return int(value)
            except Exception as ex:
                if default is not None:
                    return default
                return value
    else:
        try:
            return int(value)
        except Exception as ex:
            if default is not None:
                return default
            return value


class ReadingsCSV(object):
    def __init__(self, **kwargs):
        self.filename = kwargs.get('filename', None)
        self.dirname = kwargs.get('dirname')
        self.full_path = os.path.join(self.dirname, self.filename)
        self.file = io.open(self.full_path, 'r')
        self.time_order = self.detect_time_order()
        self.header = self.read_header()
        self.fields = kwargs.get('fields', {})
        self.reading = self.get_reading()

    def detect_time_order(self):
        self.header = self.read_header()
        reading1 = self.get_reading()
        reading2 = self.get_reading()
        while reading1['Time'] == reading2['Time']:
            reading2 = self.get_reading()
        self.file.seek(0)
        if reading1['Time'] < reading2['Time']:
            return 'asc'
        else:
            return 'desc'

    def read_header(self):
        header = self.file.readline()
        return [k.strip('\"\n ') for k in header.split('","')]

    def get_reading(self):
        line = self.file.readline()
        data = [value.strip("\"\n") for value in line.split(',')]
        reading = dict(zip(self.header, data))
        for field, value in reading.iteritems():
            if field == 'Time':
                try:
                    reading[field] = datetime.strptime(value.replace('"', ''), "%Y-%m-%d %H:%M:%S")
                except ValueError as ex:
                    reading[field] = datetime.strptime(value.replace('"', ''), "%H:%M:%S")
            else:
                reading[field] = try_number(value, 0)
        return reading

    def average(self, readings):
        if len(readings) == 0:
            return None
        reading_avg = {'Time': readings[0]['Time']}
        for field, value in readings[0].iteritems():
            if isinstance(value, (float, int)):
                accum = 0
                for reading in readings:
                    accum += reading[field]
                reading_avg[field] = accum / len(readings)

        if 'Real Power, Phase A (kW)' in reading_avg and 'Total Net Instantaneous Real Power (kW)' not in reading_avg:
            reading_avg['Total Net Instantaneous Real Power (kW)'] = \
                reading_avg['Real Power, Phase A (kW)'] + \
                reading_avg['Real Power, Phase B (kW)'] + \
                reading_avg['Real Power, Phase C (kW)']

        return reading_avg

    def skip_to(self, reading_time):
        if self.time_order == 'asc':
            while self.reading is not None and self.reading['Time'] < reading_time:
                self.reading = self.get_reading()
        else:
            while self.reading is not None and self.reading['Time'] > reading_time:
                self.reading = self.get_reading()

    def read(self, reading_time=None):
        """
        Reading_time will be a date time down to the second. Most CSV files have multiple
        readings per second so this method averages those readings across one second.

        :param reading_time:
        :return: average of readings across second requested in reading_time.
        """
        try:
            if reading_time is None:
                reading_time = self.reading['Time']
            self.skip_to(reading_time)

            readings = []
            while self.reading is not None and self.reading['Time'] == reading_time:
                readings.append(self.reading)
                self.reading = self.get_reading()
            return self.average(readings)
        except Exception as ex:
            raise Exception("Unhandled exception while processing %s: %s" % (self.filename, ex))

    def time(self):
        return self.reading['Time']


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-d", "--dir", dest="dirname", help="Directory containing reading exports.")
    parser.add_option("-b", "--begin", dest="begin_time", help="Begin time.", default="00:00:00")
    parser.add_option("-e", "--end", dest="end_time", help="End time.", default="23:59:59")
    (options, args) = parser.parse_args()
    begin_time, end_time = None, None
    if options.begin_time:
        begin_time = time.strptime(options.begin_time, '%H:%M:%S')
    if options.end_time:
        end_time = time.strptime(options.end_time, '%H:%M:%S')

    readings_csv_descs = {
        'pcc_readings_1': ['Total Net Instantaneous Real Power (kW)'],
        'pcc_readings_2': ['Total Net Instantaneous Real Power (kW)'],
        'solar_readings_1': ['Total Net Instantaneous Real Power (kW)'],
        'curtail_log': ['Curtail Value Delta'],
        'net_prot_log': ['']
    }

    readings_csvs = {}
    for filename in os.listdir(options.dirname):
        for k, v in readings_csv_descs.iteritems():
            if k in filename:
                readings_csvs[k] = ReadingsCSV(filename=filename, dirname=options.dirname)

    if len(readings_csvs) == 0:
        raise ValueError("Directory \"%s\" does not contain any of the following files: %s" %
                         (options.dirname, ".csv, ".join(readings_csv_descs.keys())))


    # write output file to input directory with output file name "consolidated_readings_<date>.csv"
    out_filename = os.path.join(options.dirname, 'consolidated_readings_' + os.path.basename(options.dirname) + '.csv')
    out_file = open(out_filename, 'w')
    out_header = ["Time"]
    if 'pcc_readings_1' in readings_csvs:
        out_header.append('PCC1 Total Net Instantaneous Real Power (kW)')
    if 'pcc_readings_2' in readings_csvs:
        out_header.append('PCC2 Total Net Instantaneous Real Power (kW)')
        out_header.append('PCC Combo Total Net Instantaneous Real Power (kW)')
        out_header.append('PCC Combo Real Power A (kW)')
        out_header.append('PCC Combo Real Power B (kW)')
        out_header.append('PCC Combo Real Power C (kW)')

    if 'solar_readings_1' in readings_csvs:
        out_header.append('Solar Total Net Instantaneous Real Power (kW)')
    if 'curtail_log' in readings_csvs:
        out_header.append('Curtail Delta Value')
    out_file.write(",".join(out_header) + "\n")

    while True:
        try:
            # new dictionary to store consolidated readings
            reading = {}

            # use timestamp from PCC1 readings as a reference
            reading_time = readings_csvs['pcc_readings_1'].time()

            # get readings for this time stamp from all files
            for key, reading_csv in readings_csvs.iteritems():
                reading[key] = reading_csv.read(reading_time)

            # if we detect PCC1 and PCC2 readings, sum the two sets of readings
            if {'pcc_readings_1', 'pcc_readings_2'}.issubset(readings_csvs.keys()) and \
                    reading['pcc_readings_1'] is not None and reading['pcc_readings_2'] is not None:

                # sum the two sets of readings under pcc_readings_combo; again use PCC1 as reference for fields
                reading['pcc_readings_combo'] = {}
                for field, value in reading['pcc_readings_1'].iteritems():
                    if isinstance(value, float) and field in reading['pcc_readings_2']:
                        reading['pcc_readings_combo'][field] = value + reading['pcc_readings_2'][field]
                    else:
                        reading['pcc_readings_combo'][field] = value

            out_data = [datetime.strftime(reading['pcc_readings_1']['Time'], "%Y-%m-%d %H:%M:%S")]
            if 'pcc_readings_1' in readings_csvs:
                if 'pcc_readings_1' in reading and reading['pcc_readings_1'] is not None:
                    out_data.append(str(reading['pcc_readings_1']['Total Net Instantaneous Real Power (kW)']))
                else:
                    out_data.append('')
            if 'pcc_readings_2' in readings_csvs:
                if 'pcc_readings_2' in reading and reading['pcc_readings_2'] is not None:
                    out_data.append(str(reading['pcc_readings_2']['Total Net Instantaneous Real Power (kW)']))
                else:
                    out_data.append('')
                if 'pcc_readings_combo' in reading and reading['pcc_readings_combo'] is not None:
                    out_data.append(str(reading['pcc_readings_combo']['Total Net Instantaneous Real Power (kW)']))
                    out_data.append(str(reading['pcc_readings_combo']['Real Power, Phase A (kW)']))
                    out_data.append(str(reading['pcc_readings_combo']['Real Power, Phase B (kW)']))
                    out_data.append(str(reading['pcc_readings_combo']['Real Power, Phase C (kW)']))

                else:
                    out_data.append('')
            if 'solar_readings_1' in readings_csvs:
                if 'solar_readings_1' in reading and reading['solar_readings_1'] is not None:
                    out_data.append(str(reading['solar_readings_1']['Total Net Instantaneous Real Power (kW)']))
                else:
                    out_data.append('')
            if 'curtail_log' in readings_csvs:
                if 'curtail_log' in reading and reading['curtail_log'] is not None:
                    out_data.append(str(reading['curtail_log']['Curtail Value Delta']))
                else:
                    out_data.append('')
            out_file.write(",".join(out_data) + "\n")

        except StopIteration:
            break
