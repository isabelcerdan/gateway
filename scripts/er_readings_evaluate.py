#!/usr/share/apparent/.py2-virtualenv/bin/python
from optparse import OptionParser
from datetime import datetime, timedelta
from collections import deque
import time
import operator


def try_float(value):
    try:
        return float(value)
    except Exception as ex:
        return value


def get_time(t):
    try:
        return datetime.strptime(t, '%m/%d/%Y %H:%M')
    except Exception as ex:
        try:
            return time.strptime(t, '%H:%M:%S')
        except Exception as ex:
            return datetime.strptime(t, '%Y-%m-%d %H:%M:%S')


p = 'Total Real Power Present Demand (kW)'
e = 'Accumulated Real Energy Net (kWh)'


def get_reading(keys, line):
    row = [try_float(v.strip('\"\n ')) for v in line.split(",")]
    data = dict(zip(keys, row))
    data['Time'] = get_time(data['Time'])
    fields = ['Time', p, e]
    data = {key: data.get(key, None) for key in fields}
    return data


if __name__ == '__main__':
    readings_violating = []
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename", help="CSV reading filename.")
    parser.add_option("-b", "--begin", dest="begin_time", help="Begin time.", default=None)
    parser.add_option("-e", "--end", dest="end_time", help="End time.", default=None)
    (options, args) = parser.parse_args()

    with open(options.filename) as f:
        header = next(f)
        keys = [k.strip('\"\n ') for k in header.split('","')]
        readings_history = deque([get_reading(keys, next(f))] * 4)
        begin_time, end_time = None, None
        if options.begin_time:
            begin_time = datetime.strptime(options.begin_time, '%H:%M:%S')
        if options.end_time:
            end_time = datetime.strptime(options.end_time, '%H:%M:%S')
        
        energy_tot = 0
        num_readings = 0
        energy_avg = 1.0
        threshold = 135
        min_power = threshold
        try:
            while True:
                r = get_reading(keys, next(f))

                readings_history.appendleft(r)
                readings_history.pop()

                r0 = readings_history[0]
                r1 = readings_history[1]
                r2 = readings_history[2]

                if begin_time and end_time:
                    if not (begin_time.time() <= r['Time'].time() <= end_time.time()):
                        continue

                if r0[p] < threshold:
                    energy = r1[e] - r0[e]

                    # check for bogus readings
                    if energy > energy_avg * 1000:
                        if 1 <= (r1['Time'] - r0['Time']).total_seconds() <= 2 and \
                                        r1['Time'].minute != r0['Time'].minute:
                            power = r0[p] + r1[p]
                            if power < threshold:
                                rz = r0
                                rz[p] = rz[p] + r1[p]
                                readings_violating.append(rz)
                            else:
                                pass
                    else:
                        readings_violating.append(r0)

                energy = abs(r1[e] - r0[e])
                if energy < 1000:
                    energy_tot += energy
                    num_readings += 1
                    energy_avg = energy_tot / num_readings
        except StopIteration as ex:
            pass

    sorted_readings_violating = readings_violating #sorted(readings_violating, key=lambda k: k[p])

    print "\n".join(["time: %s power: %s kW" % (r['Time'], r[p]) for r in sorted_readings_violating])
