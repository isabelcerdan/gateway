#!/usr/bin/env python
# encoding: utf-8
import os
import csv
THRESHOLD_A = 0  # -0.068  # 0
THRESHOLD_B = 0  # -0.068  # 0
THRESHOLD_C = 0  # 0.519  # 0
EXPORT_TIME_LIMIT = 30


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

feed_a_export = []
feed_a_net_pos_export = 0
violation_count_a = 0
violation_logs_a = []
feed_b_export = []
feed_b_net_pos_export = 0
violation_count_b = 0
violation_logs_b = []
feed_c_export = []
feed_c_net_pos_export = 0
violation_count_c = 0
violation_logs_c = []

feed_a_export_list = []
feed_a_export_duration = 0
feed_a_samples = []
feed_a_log_stamp = []
feed_b_export_list = []
feed_b_export_duration = 0
feed_b_samples = []
feed_b_log_stamp = []
feed_c_export_list = []
feed_c_export_duration = 0
feed_c_samples = []
feed_c_log_stamp = []

export_col_a = False
export_col_b = False
export_col_c = False
net_pos_col_a = False
net_pos_col_b = False
net_pos_col_c = False
# for f in os.listdir("data"):
#     r = f.replace(" ", "")
#     if r != f:
#         os.rename(f, r)
for log in os.listdir("data"):  # os.getcwd()):
    # print log
    # log = log.replace(' ', '')
    if log.endswith(".csv"):
        with open('data/%s' % log, "rb") as csv_file:
            export_col_a = False
            export_col_b = False
            export_col_c = False
            net_pos_col_a = False
            net_pos_col_b = False
            net_pos_col_c = False
            reader = csv.reader(csv_file)
            header_row_found = False
            for row in reader:
                if not header_row_found:
                    if "Store No." in str(row[0]):
                        header_row_found = True
                        for index, column in enumerate(row):
                            if "P-1-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                export_col_a = index
                            elif "P-2-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                export_col_b = index
                            elif "P-3-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                export_col_c = index
                            elif "WPp-1-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                net_pos_col_a = index
                            elif "WPp-2-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                net_pos_col_b = index
                            elif "WPp-3-Total" == str(column):
                                # print "%s is column %s" % (column, index)
                                net_pos_col_c = index
                else:
                    try:
                        if is_number(export_col_a):
                            if is_number(row[export_col_a]):
                                feed_a_export.append(row[export_col_a])
                                feed_a_samples.append(row[0])
                                feed_a_log_stamp.append(log)
                            if is_number(net_pos_col_a):
                                if row[net_pos_col_a] > float(feed_a_net_pos_export):
                                    feed_a_net_pos_export = float(row[net_pos_col_a])
                        if is_number(export_col_b):
                            if is_number(row[export_col_b]):
                                feed_b_export.append(row[export_col_b])
                                feed_b_samples.append(row[0])
                                feed_b_log_stamp.append(log)
                            if is_number(net_pos_col_b):
                                if row[net_pos_col_b] > float(feed_b_net_pos_export):
                                    feed_b_net_pos_export = float(row[net_pos_col_b])
                        if is_number(export_col_c):
                            if is_number(row[export_col_c]):
                                feed_c_export.append(row[export_col_c])
                                feed_c_samples.append(row[0])
                                feed_c_log_stamp.append(log)
                            if is_number(net_pos_col_c):
                                if row[net_pos_col_c] > float(feed_c_net_pos_export):
                                    feed_c_net_pos_export = float(row[net_pos_col_c])
                    except Exception as error:
                        print "Error in %s: %s" % (log, error)

for export_sample, sample, log in zip(feed_a_export, feed_a_samples, feed_a_log_stamp):
    if float(export_sample) <= THRESHOLD_A:
        if feed_a_export_duration > 0:
            if feed_a_export_duration > EXPORT_TIME_LIMIT:
                violation_count_a += 1
                print "A: export for %s seconds " \
                      "at %s watts, " \
                      "in %s at sample %s" % (feed_a_export_duration / 10.0,
                                              export_sample,
                                              log,
                                              sample)
                if log not in violation_logs_a:
                    violation_logs_a.append(log)
            # print feed_a_export_duration
            feed_a_export_list.append(feed_a_export_duration)  # + 2)
            feed_a_export_duration = 0
    else:
        feed_a_export_duration += 1
for export_sample, sample, log in zip(feed_b_export, feed_b_samples, feed_b_log_stamp):
    if float(export_sample) <= THRESHOLD_B:
        if feed_b_export_duration > 0:
            if feed_b_export_duration > EXPORT_TIME_LIMIT:
                violation_count_b += 1
                print "B: export for %s seconds " \
                      "at  %s watts, " \
                      "in %s at sample %s" % (feed_b_export_duration / 10.0,
                                              export_sample,
                                              log,
                                              sample)
                if log not in violation_logs_b:
                    violation_logs_b.append(log)
            # print feed_b_export_duration
            feed_b_export_list.append(feed_b_export_duration)  # + 2)
            feed_b_export_duration = 0
    else:
        feed_b_export_duration += 1
for export_sample, sample, log in zip(feed_c_export, feed_c_samples, feed_c_log_stamp):
    if float(export_sample) <= THRESHOLD_C:
        if feed_c_export_duration > 0:
            if feed_c_export_duration > EXPORT_TIME_LIMIT:
                violation_count_c += 1
                print "C: export for %s seconds " \
                      "at  %s watts, " \
                      "in %s at sample %s" % (feed_c_export_duration / 10.0,
                                              export_sample,
                                              log,
                                              sample)
                if log not in violation_logs_c:
                    violation_logs_c.append(log)
            # print feed_b_export_duration
            feed_c_export_list.append(feed_c_export_duration)  # + 2)
            feed_c_export_duration = 0
    else:
        feed_c_export_duration += 1
if len(feed_a_export_list) > 0:
    print "Feed A had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "mean duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_a_export_list),
                                            feed_a_net_pos_export,
                                            violation_count_a,
                                            sum(feed_a_export_list) / (len(feed_a_export_list)*10.0),
                                            max(feed_a_export_list) / 10.0,
                                            min(feed_a_export_list) / 10.0)
if len(feed_b_export_list) > 0:
    print "Feed B had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "mean duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_b_export_list),
                                            feed_b_net_pos_export,
                                            violation_count_b,
                                            sum(feed_b_export_list) / (len(feed_b_export_list)*10.0),
                                            max(feed_b_export_list) / 10.0,
                                            min(feed_b_export_list) / 10.0)
if len(feed_c_export_list) > 0:
    print "Feed C had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "mean duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_c_export_list),
                                            feed_c_net_pos_export,
                                            violation_count_c,
                                            sum(feed_c_export_list) / (len(feed_c_export_list)*10.0),
                                            max(feed_c_export_list) / 10.0,
                                            min(feed_c_export_list) / 10.0)

print 'FEED A violating logs:'
for log in violation_logs_a:
    print log
if len(violation_logs_a) == 0:
    print "None"
print 'FEED B violating logs:'
for log in violation_logs_b:
    print log
if len(violation_logs_b) == 0:
    print "None"
print 'FEED C violating logs:'
for log in violation_logs_c:
    print log
if len(violation_logs_c) == 0:
    print "None"
# for a,b in zip(feed_a_export_list, feed_b_export_list):
#     print "Feed A:\t< %s seconds\t\tFeed B:\t< %s seconds" % (str(a / 10.0),
#                                                               str(b / 10.0))
