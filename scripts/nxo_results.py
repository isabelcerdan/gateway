from optparse import OptionParser
from datetime import datetime
from datetime import date
import os
import csv

parser = OptionParser()
parser.add_option("-d", "--data-dir", dest="data_dir", default="./data", help="Directory with CSV data of Yokogawa capture for this run.")
(options, args) = parser.parse_args()


class Reading(object):
    def __init__(self, **kwargs):
        self.time = kwargs.get('time', None)
        self.value = kwargs.get('value', None)


class Feed(object):
    def __init__(self, **kwargs):
        self.export = []
        self.threshold = kwargs.get('threshold', 0.135)
        self.violation_start = None
        self.violation_end = None


for log in os.listdir(options.data_dir):
    if log.endswith(".csv"):
        with open(os.path.join(options.data_dir, log)) as csv_file:
            feeds = {
                'A': Feed(threshold=-1500),
                'B': Feed(threshold=-1500),
                'C': Feed(threshold=-1500)
            }
            reader = csv.reader(csv_file)
            header = False
            for data in reader:
                if not header:
                    if "Store No." in str(data[0]):
                        header = data
                else:
                    reading = dict(zip(header, data))
                    try:
                        real_power = float(reading["P-1-Total"])
                        if real_power > feeds['A'].threshold:
                            dt_str = "%s %s,%s" % (reading['Date'].strip(), reading['Time'], reading['Millisecond'])
                            dt = datetime.strptime("%Y/%m/%d %H:%M:%S, %f", dt_str)
                            feeds['A'].export.append(Reading(time=dt, value=reading["P-1-Total"]))
                    except Exception as ex:
                        pass
