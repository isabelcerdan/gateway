#!/usr/bin/env python
# encoding: utf-8
import os
import csv
from datetime import datetime, timedelta
# Data to define an export
THRESHOLD_A = -135 #-0.135  # -0.068  # 0
THRESHOLD_B = -135 #-0.135  # -0.068  # 0
THRESHOLD_C = 0  # 0.519  # 0
# How long an export event is considered a violation?
EXPORT_TIME_LIMIT = 30 # time in 100ms units
export_time_limit_sec = timedelta(seconds=3)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


# Feed A raw and summary data storage
feed_a_export = []
feed_a_net_pos_export = 0
violation_count_a = 0
violation_logs_a = []
feed_a_export_list = []
feed_a_export_duration = 0
feed_a_samples = []
feed_a_log_stamp = []
feed_a_time_stamp = []

# Feed B raw and summary data storage
feed_b_export = []
feed_b_net_pos_export = 0
violation_count_b = 0
violation_logs_b = []
feed_b_export_list = []
feed_b_export_duration = 0
feed_b_samples = []
feed_b_log_stamp = []
feed_b_time_stamp = []

# Feed C raw and summary data storage
feed_c_export = []
feed_c_net_pos_export = 0
violation_count_c = 0
violation_logs_c = []
feed_c_export_list = []
feed_c_export_duration = 0
feed_c_samples = []
feed_c_log_stamp = []
feed_c_time_stamp = []

feed_time_date = False
feed_time_stamp = False
feed_time_ms = False
export_col_a = False
export_col_b = False
export_col_c = False
net_pos_col_a = False
net_pos_col_b = False
net_pos_col_c = False

# Read the .csv files and store the data in the feed lists
for log in os.listdir("data"):  # os.getcwd()):

    # log = log.replace(' ', '')
    if log.endswith(".csv"):
        with open('data/%s' % log, "rb") as csv_file:
            # These are all column indexes where the required data lives in the .csv file
            export_col_a = False
            export_col_b = False
            export_col_c = False
            net_pos_col_a = False
            net_pos_col_b = False
            net_pos_col_c = False
            feed_time_stamp = False
            feed_time_ms = False
            feed_time_date = False

            reader = csv.reader(csv_file)
            header_row_found = False
            for row in reader:
                if not header_row_found:
                    if "Store No." in str(row[0]):
                        header_row_found = True
                        for index, column in enumerate(row):
                            if "P-1-Total" == str(column):
                                export_col_b = index
                            elif "P-2-Total" == str(column):
                                export_col_a = index
                            elif "P-3-Total" == str(column):
                                export_col_c = index
                            elif "WPp-1-Total" == str(column):
                                net_pos_col_b = index
                            elif "WPp-2-Total" == str(column):
                                net_pos_col_a = index
                            elif "WPp-3-Total" == str(column):
                                net_pos_col_c = index
                            elif "Time" == str(column):
                                feed_time_stamp = index
                            elif "Millisecond" == str(column):
                                feed_time_ms = index
                            elif "Date" == str(column):
                                feed_time_date = index
                else:
                    try:
                        if is_number(export_col_a):
                            if is_number(row[export_col_a]):
                                feed_a_export.append(row[export_col_a])
                                feed_a_samples.append(row[0])
                                feed_a_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_a_time_stamp.append(dt)
                            if is_number(net_pos_col_a):
                                if row[net_pos_col_a] > float(feed_a_net_pos_export):
                                    feed_a_net_pos_export = float(row[net_pos_col_a])
                        if is_number(export_col_b):
                            if is_number(row[export_col_b]):
                                feed_b_export.append(row[export_col_b])
                                feed_b_samples.append(row[0])
                                feed_b_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_b_time_stamp.append(dt)
                            if is_number(net_pos_col_b):
                                if row[net_pos_col_b] > float(feed_b_net_pos_export):
                                    feed_b_net_pos_export = float(row[net_pos_col_b])
                        if is_number(export_col_c):
                            if is_number(row[export_col_c]):
                                feed_c_export.append(row[export_col_c])
                                feed_c_samples.append(row[0])
                                feed_c_log_stamp.append(log)
                                feedt = row[feed_time_stamp]
                                feedm = (row[feed_time_ms].lstrip()).zfill(3)
                                feeddt = row[feed_time_date].lstrip()+ " " + feedt + ":" + feedm
                                dt = datetime.strptime(feeddt,"%Y/%m/%d %H:%M:%S:%f")
                                feed_c_time_stamp.append(dt)
                            if is_number(net_pos_col_c):
                                if row[net_pos_col_c] > float(feed_c_net_pos_export):
                                    feed_c_net_pos_export = float(row[net_pos_col_c])
                    except Exception as error:
                        print "Error in %s: %s" % (log, error)
                        raise

print "From: ", feed_a_time_stamp[0].strftime("%Y-%m-%d %H:%M:%S")
print "To:   ", feed_a_time_stamp[len(feed_a_time_stamp)-1].strftime("%Y-%m-%d %H:%M:%S")
print " "

checkForError = 0
# Use the feed lists to check for export events
for export_sample, sample, log, timestamp in zip(feed_a_export, feed_a_samples, feed_a_log_stamp, feed_a_time_stamp):
    if float(export_sample) <= THRESHOLD_A:
        if feed_a_export_duration > 0:  # This is the duration in consecutive samples
            # We had some export but just stopped.  See if it was a violation.
            if timestamp - export_start_time > export_time_limit_sec:
                violation_count_a += 1
                print "A: export for %s seconds " \
                      "in %s at sample %s" % ((timestamp - export_start_time).total_seconds(),
                                              log,
                                              sample)
                if log not in violation_logs_a:
                    violation_logs_a.append(log)
                checkForError = 1
            # print feed_a_export_duration
            feed_a_export_list.append(timestamp - export_start_time)
            feed_a_export_duration = 0
    else:
        # We have an export event.
        if feed_a_export_duration == 0:
            export_start_time = timestamp  # If first event, record start time
        feed_a_export_duration += 1

for export_sample, sample, log, timestamp in zip(feed_b_export, feed_b_samples, feed_b_log_stamp, feed_b_time_stamp):
    if float(export_sample) <= THRESHOLD_B:
        if feed_b_export_duration > 0:  # This is the duration in consecutive samples
            # We had some export but just stopped.  See if it was a violation.
            if timestamp - export_start_time > export_time_limit_sec:
                # if feed_b_export_duration > EXPORT_TIME_LIMIT:
                violation_count_b += 1
                print "B: export for %s seconds " \
                      "in %s at sample %s" % ((timestamp - export_start_time).total_seconds(),
                                              log,
                                              sample)
                if log not in violation_logs_b:
                    violation_logs_b.append(log)
                checkForError = 1
            # print feed_b_export_duration
            feed_b_export_list.append(timestamp - export_start_time)
            feed_b_export_duration = 0
    else:
        # We have an export event.
        if feed_b_export_duration == 0:
            export_start_time = timestamp  # If first event, record start time
        feed_b_export_duration += 1

for export_sample, sample, log ,timestamp in zip(feed_c_export, feed_c_samples, feed_c_log_stamp, feed_c_time_stamp):
    if float(export_sample) <= THRESHOLD_C:
        if feed_c_export_duration > 0:  # This is the duration in consecutive samples
            # We had some export but just stopped.  See if it was a violation.
            if timestamp - export_start_time > export_time_limit_sec:
                # if feed_c_export_duration > EXPORT_TIME_LIMIT:
                violation_count_c += 1
                print "C: export for %s seconds " \
                      "in %s at sample %s" % ((timestamp - export_start_time).total_seconds(),
                                              log,
                                              sample)
                if log not in violation_logs_c:
                    violation_logs_c.append(log)
                checkForError = 1
            # print feed_b_export_duration
            feed_c_export_list.append(timestamp - export_start_time)
            feed_c_export_duration = 0
    else:
        # We have an export event.
        if feed_c_export_duration == 0:
            export_start_time = timestamp  # If first event, record start time
        feed_c_export_duration += 1

if checkForError == 1:
    # Print out the summary results
    print " "

if len(feed_a_export_list) > 0:
    print "Feed A had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "average duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_a_export_list),
                                            feed_a_net_pos_export,
                                            violation_count_a,
                                            (sum(feed_a_export_list, timedelta(0, 0, 0)) / len(
                                                feed_a_export_list)).total_seconds(),
                                            max(feed_a_export_list).total_seconds(),
                                            min(feed_a_export_list).total_seconds())
if len(feed_b_export_list) > 0:
    print "Feed B had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "average duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_b_export_list),
                                            feed_b_net_pos_export,
                                            violation_count_b,
                                            (sum(feed_b_export_list, timedelta(0, 0, 0)) / len(
                                                feed_b_export_list)).total_seconds(),
                                            max(feed_b_export_list).total_seconds(),
                                            min(feed_b_export_list).total_seconds())
if len(feed_c_export_list) > 0:
    print "Feed C had %s export events.\n " \
          "net positive export = %s wh\n " \
          "count of 3s violations = %s\n " \
          "average duration = %s seconds\n " \
          "max duration = %s seconds\n " \
          "min duration = %s seconds\n " % (len(feed_c_export_list),
                                            feed_c_net_pos_export,
                                            violation_count_c,
                                            (sum(feed_c_export_list, timedelta(0, 0, 0)) / len(
                                                feed_c_export_list)).total_seconds(),
                                            max(feed_c_export_list).total_seconds(),
                                            min(feed_c_export_list).total_seconds())

#print " "
print 'FEED A violating logs:'
for log in violation_logs_a:
    print log
if len(violation_logs_a) == 0:
    print "None"
print 'FEED B violating logs:'
for log in violation_logs_b:
    print log
if len(violation_logs_b) == 0:
    print "None"
print 'FEED C violating logs:'
for log in violation_logs_c:
    print log
if len(violation_logs_c) == 0:
    print "None"
# for a,b in zip(feed_a_export_list, feed_b_export_list):
#     print "Feed A:\t< %s seconds\t\tFeed B:\t< %s seconds" % (str(a / 10.0),
#                                                               str(b / 10.0))