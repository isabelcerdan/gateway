* [Gateway System Restore](#gateway-system-restore)
  * [Clean BIOS Boot Menu](#clean-bios-boot-menu)
* [System Restore Creation](#system-restore-creation)
  * [Save Image of Gateway Hard Drive](#save-image-of-gateway-hard-drive)
  * [Create ISO-Recovery-Zip ISO file](#create-iso-recovery-zip-iso-file)
  * [Create System Restore USB](#create-system-restore-usb)

## Gateway System Restore

1. Power off morex at brick
1. **IMPORTANT: REMOVE JWD1 JUMPER (NEXT TO SATA PORTS) TO DISABLE THE WATCHDOG BEFORE PERFORMING THE STEPS BELOW (OTHERWISE THE WATCHDOG WILL REBOOT BEFORE SYSTEM RESTORE IS COMPLETE).**
1. Insert System Restore USB 
    1. If you need to create a System Restore USB, see [Create System Restore USB](#create-system-restore-usb)).
1. Power up morex
1. Enter BIOS Setup
1. Boot from UEFI USB
1. F4 to Save & Exit
1. Select `Clonezilla live (Default settings, VGA 1024x768)`
1. Select SSD drive:
    1. `sda  300GB_INTEL_SSDSC2BB30_INTEL_SSDSC2BB300G4_BTWL419202CK300PGN`
1. Wait for clonezilla to complete processing
1. `y` to continue
1. `y` to continue
1. Wait for clonezilla to restore the system from image
1. Select `reboot`
    1. Note: First boot enters grub2; select ubuntu. You will then see a blank screen for several minutes before Zentyal finally launches. 
1. Update hostname in Zentyal settings:
    1. Login to Zentyal dashboard
    1. Navigate to `System > General` in left-side nav
    1. Under `Hostname and Domain`, append unique number to end of `zentyalmorex` hostname (e.g. `zentyalmorex9`)
    1. Click `Change`
    1. Click `Save Changes`
1. Setup TeamViewer9. It should already be installed but not yet configured with the apparent account.
1. Shutdown Zentyal
1. Replace JWD1 jumper to enable watchdog. 
1. Your system should be ready for gateway operation. If restoring an existing system that has already been setup for gateway operation, see next section about cleaning BIOS Boot Menu.    

### Clean BIOS Boot Menu

Restoring a system that already has zentyal/ubuntu installed may result in duplicate ubuntu entries in the BIOS boot menu:

```
        Boot Options:
           UEFI: Built-in EFI Shell
           ubuntu
           ubuntu
           Disabled
```

This can be cleaned up using the following steps:

1. Boot from Clonezilla Live USB (see step 1 of [Save Image of Gateway Hard Drive](#save-image-of-gateway-hard-drive) to create a Clonezilla Live USB)
1. Enter clonezilla command line mode
1. Install efibootmgr:
```
sudo apt-get install efibootmgr
```
1. Add to the kernel efi support
```
sudo modprobe efivars
```
1. Run `sudo efibootmgr` to check your boot entries. It will return something like this:
```
BootCurrent: 0004
Timeout: 2 seconds
BootOrder: 2001,0003,0002,0000
Boot0000* EFI: Built-in EFI Shell
Boot0001* ubuntu
Boot0002* ubuntu
Boot0003* ubuntu
```
1. Use the following to delete entry 3 and remove it from the BootOrder.
    ```
    sudo efibootmgr -b 3 -B 
    ```
1. Repeat for each duplicate that needs to be removed.

## System Restore Creation

Creating a Gateway System Restore USB from a fully provisioned Gateway System involves the following high-level steps:

1. Prepare Gateway System Image
1. Save image of Gateway System hard drive 
1. Create ISO-Recovery-Zip ISO file
1. Create System Restore USB

### Prepare Gateway System Image

1. Restore Gateway system using latest System Restore USB.
1. Upgrade the gateway to the latest stable tag:
    ```
    $ cd ~/gateway
    $ git fetch --all
    $ git checkout <latest-stable-tag>
    $ cd provision
    $ ./self-provision.sh
    ```
1. Clean the image in preparation for creating ISO:
    ```
    $ ./self-provision.sh clean
    ```

### Save Image of Gateway Hard Drive
**IMPORTANT NOTE: THIS PROCESS REQUIRES TWO USB DRIVES**

| USB Drive  | Size Requirements |Purpose | 
|---|---|---|
| 1  | >= 500MB |Clonezilla live | 
| 2  | >= 4GB | System Restore | 

1. Create Clonezilla live UEFI USB Drive
    1. Download latest stable Clonezilla live ISO from Clonezilla site (http://free.nchc.org.tw/clonezilla-live/stable/)
    1. Insert USB drive 1 (>= 500MB)
    1. In Windows Admin command line window: `diskpart`
        1. `list disk`
        1. `select disk <disknum>`
        1. `clean`
        1. `create partition primary size=500`
        1. `format fs=fat32 quick`
        1. `active`
        1. `assign`
        1. `list volume`
        1. `exit`
    1. In Magic ISO (http://www.magiciso.com/download.htm)
        1. Load the Clonezilla live ISO file.
        1. Extract (folder + green arrow button)
        1. Select the USB drive
        1. OK
        1. quit
1. Prepare USB drive 2 for saving system image. In Windows Admin command line window: `diskpart`
    1. `list disk`
    1. `select disk <disknum>`
    1. `clean`
    1. `create partition primary`
    1. `format fs=ntfs quick`
    1. `active`
    1. `assign`
    1. `list volume`
    1. `exit`    
1. Power off morex at brick
1. **IMPORTANT: REMOVE JWD1 JUMPER (NEXT TO SATA PORTS) TO DISABLE THE WATCHDOG BEFORE PERFORMING THE STEPS BELOW (OTHERWISE THE WATCHDOG WILL REBOOT BEFORE IMAGING IS COMPLETE).**
1. Insert USB drive 1 (Clonezilla live) into morex
1. Power up morex
1. Enter BIOS Setup 
1. Boot from UEFI USB
1. F4 to Save & Exit
1. Select `Clonezilla live (Default settings, VGA 1024x768)`
1. Select `English`
1. Select `Don't touch keymap`
1. Select `Start_Clonezilla Start Clonezilla`
1. Select `device-image work with disks or partitions using images`
1. Select `local_dev Use local device (E.g.: hard drive, USB drive)`
1. Insert USB drive 2 (>= 4GB) into morex
1. Write down the device path for your USB drive 2 then press Ctrl-C
    1. `/dev/sdc Ultra_Fit_ SanDisk_Ultra_Fit_4C530001270421112551-0:0 124GB`
1. Select the USB device saved from previous step: 
    1. `/dev/sdc Ultra_Fit_ SanDisk_Ultra_Fit_4C530001270421112551-0:0 124GB`
1. Select `/ Top_directory_in_the_local_device`
1. Enter to continue
1. Select `Beginner Beginner mode: Accept the default options`
1. Select `savedisk  Save_local_disk_as_an_image`
1. Input a name. Replace `img` with `zentyalmorex<morex-number>`
    1. `2016-09-12-21-zentyalmorex9`
1. Select disk to image:
    1. `[*] sda 300GB_INTEL_SSDSC2BB30_INTEL_SSDSC2BB300G4_PHWL524302NZ300PGN`
1. Select `-fsck  Interactively check and repair source file system before saving`
1. Select `Yes, check the saved image`
1. Select `Not to encrypt the image`
1. Enter to continue
1. `y` to continue
1. Wait for clonezilla to copy the hard drive image to USB drive 2
1. Enter to continue
1. Select `reboot`
1. Remove USB drive 2.
    
### Create ISO-Recovery-Zip ISO file

1. Enter BIOS Setup 
1. Boot from UEFI USB (USB drive 1 - Clonezilla Live).
1. Select `Clonezilla live (Default settings, VGA 1024x768)`
1. Select `English`
1. Select `Don't touch keymap`
1. Select `Start_Clonezilla Start Clonezilla`
1. Select `device-image work with disks or partitions using images`
1. Select `local_dev Use local device (E.g.: hard drive, USB drive)`
1. Insert USB drive 2.
1. Enter to continue
1. Write down the device path for your USB drive 2 then press Ctrl-C
    1. `/dev/sdb1 Ultra_Fit_ SanDisk_Ultra_Fit_4C530001270421112551-0:0 124GB`
1. Select the USB device saved from previous step: 
    1. `/dev/sdb1 115.7G_ntfs(In_Ultra_Fit)SanDisk_Ultra_Fit_4C530001270421112551-0:0`
1. Select `/ Top_directory_in_the_local_device`
1. Enter to continue
1. Select `Beginner Beginner mode: Accept the default options`
1. Select `recovery-iso-zip   Create_recovery_Clonezilla_live`
1. Select saved image:
    1. `2016-09-12-21-zentyalmorex9 2016-0912-2107_sda1_sda2_sda3`
1. Input `ask_user`
1. Select `Yes, check the image before restoring`
1. Select `en_US.UTF-8 English`
1. Input NONE
1. Select `iso   Create an iso file for CD/DVD use`
1. Enter to continue
1. `y` to continue
1. Enter to continue
1. Select `reboot`
1. Pull both USB drives from morex

### Create System Restore USB

1. Insert USB into Windows machine
1. Copy both image directory and .iso file from USB drive to Windows hard drive (e.g. Desktop)
    1. Image directory: `2016-09-12-21-zentyalmorex9`
    1. ISO file: `clonezilla-live-2016-09-12-21-zentyalmorex9`
1. Prepare USB drive 2 for system ISO. In Windows Admin command line window: `diskpart`
    1. `list disk`
    1. `select disk <disknum>`
    1. `clean`
    1. `create partition primary size=4000`
    1. `format fs=fat32 quick`
    1. `active`
    1. `assign`
    1. `list volume`
    1. `exit`    
1. In Magic ISO
    1. Load the zentyalmorex ISO file.
    1. Extract (folder + green arrow button)
    1. Select the USB drive
    1. OK
    1. quit
1. USB drive 2 is now ready to be used to restore a gateway system.