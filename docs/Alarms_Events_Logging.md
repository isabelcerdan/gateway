* [Alarms](##alarms)
* [Logging](##logging)
* [Events](##events)

# Gateway Alarms and Events

## Alarms

####  How do I create a new alarm?

1. Define a new alarm id in `python/alarm/alarm_constants.py`. Make sure to add the alarm at the very end of the list of 
existing alarms and give it a sequential number based on the last previously defined alarm id. **IMPORTANT: Do not change the defintion of any existing alarm ids!!** 
    ```
    ALARM_ID_AUDITOR_DAEMON_RESTARTED = 38
    ALARM_ID_PCC_METER_MODBUS_READ_ERROR = 39
    ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR = 40
    ALARM_ID_ENERGY_BUCKET_85_CAPACITY = 41
    ALARM_ID_ENERGY_BUCKET_100_CAPACITY = 42
    ALARM_ID_NEW_ALARM = 43
    ```
1. Define the alarm itself in `python/alarm/__init__.py`. Use the alarm id defined in the previous step as your first parameter. Define whether the new alarm should be transmitted to **Energy Review**.
 
    Define the alarm severity:
    1. **SEVERITY_CRITICAL**
    1. **SEVERITY_MAJOR**: 
    1. **SEVERITY_WARN**
    1. **SEVERITY_INFO**
    1. **SEVERITY_DEBUG** 
    
    Define the alarm strategy as one of the following:
    1. **Set and auto clear**:  The alarm is raised at the first occurrence and automatically cleared after a debouncing period (15m) has transpired.
    1. **Set and explicitly clear**: The alarm is raised at the first occurrence. The alarm will be cleared only after it is explicitly cleared and a debouncing period (15m) has transpired.
    1. **Maximum failures within duration**: The alarm is raised after a _defined_count_ of occurrences have occurred in less than the _defined_duration_ window (in seconds). The alarm is cleared after a debouncing period has transpired with no new occurrences of the alarm.
    1. **Minimum successes within duration**: The alarm is raised when less than a _defined_count_ of occurrences have occurred in the _defined_duration_ window (in seconds). The alarm is cleared after a debouncing period has transpired with no new occurrences of the alarm.
 
    ```
    Alarm.create(id = ALARM_ID_NEW_ALARM,
                 name = 'NEW ALARM NAME',
                 severity = SEVERITY_CRITICAL,
                 er_alarm = True,
                 strategy = STRATEGY_MINIMUM_FAILURES_WITHIN_DURATION,
                 defined_count = 20,
                 defined_duration = (5*60))
    ```
1. Register an occurrence of an alarm incident at the appropriate place in your code:

    ```
        from alarm.alarm import Alarm
        ...
        try:
            c = a + b
        except:
            Alarm.occurrence(ALARM_ID_NEW_ALARM)
    ```
    Each occurrence of an alarm will automatically be logged to `/var/log/apparent/gateway.log` as part of the alarm handling. Depending on how the alarm is defined, an occurrence of an alarm does not necessarily raise the alarm.

1. For alarms defined as **Set and Explicitly Clear**, use the `Alarm.request_clear(alarm_id)` interface to request that the alarm be cleared:

    ```
        from alarm.alarm import Alarm

        Alarm.request_clear(ALARM_ID_NEW_ALARM)
    ```
    This will create a pending alarm clear request that will cause the alarm to be cleared after the debounce period has transpired.

####  How often are alarms checked?

The gateway currently checks for new alarms every minute.
 
####  How are alarms checked?

The gateway monitor launches an alarm daemon that records any changes it detects in the overall alarm state to an `alarms_history` table.
    
####  Are alarms cleared after a gateway reboot?

All alarm records are dropped from the alarms table during gateway initialization and recreated from the definitions in `alarm\__init__.py`.

## Logging

Logging in the gateway is implemented using the `logging` package that is part of the python standard library. The gateway defines a root logger in `lib\logging_setup.py` with the following defaults which all other logger objects inherit:

1. All logging messages will be output using the following format: 
    1. `<timestamp> <logger-name> [<levelname>]: <message>`
    1. For example:
    ```
    2017-05-26 18:08:41,556 ALARM [CRITICAL]: MOTHERBOARD SENSOR - HIGH 12V STATUS: UPPER NON-RESPONSIVE VIOLATION: 14.224 Volts (limit=13.408 Volts)
    ```
1. The log file used is `/var/log/apparent/gateway.log`
1. The log level is set to **INFO**.
1. The log rotation is set to keep 10 log files with each log maxing out at 4MB (2048*2048)

#### How do I use the logger?

Typical usage of the logger is as follows:
```
from lib.logging_setup import *

logger = logging.getLogger('DHCP-EVENT')
logger.critical('DHCP is not working!!')
```

This would generate the following output in `/var/log/apparent/gateway.log`:
```
2017-05-26 19:08:41,556 DHCP-EVENT [CRITICAL]: DHCP is not working!!
```

Since the root logger is set with the appropriate formatting, we no longer need to define special methods (`log_info`, `log_critical`, etc.) throughout the gateway. 

#### What is the easiest way to log exceptions?

The `logging` package provides an `exception(...)` interface which outputs a stack trace with line numbers and a detailed description of the exception. The function must be called within an exception handler.
```
logger = logging.getLogger(__name__)
try:
    object.method_that_can_throw_exception()
except:
    logger.exception('Some method threw an exception')
```


#### How do I use different settings than the default root logger settings?

You can define your own logger that has slightly different settings by doing the following:
```
logger = logging.getLogger(__name__)
handler = logging.FileHandler('/var/log/apparent/my_log_file.log')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
```

## Events

Events can be used to record the occurrence of discrete gateway events (e.g. a gateway upgrade, inverter upgrades, feed detection runs, configurator change, etc.) that allow for forensics in troubleshooting issues out in the field. 

#### How do I create a new event?

Events are created by passing the `extra` dictionary parameter to the logging interface with at least one event tag in the list of `event_tags` :
```
logger.info('Inverters were upgraded.' extra={'event_tags': [EVENT_TAG_INVERTER_UPGRADE]})    
```
This will create a new record in both the events table and the events_tags table. Events can be tagged with multiple tags. 

Event tags are defined in `events\event_constants.py`:
```
EVENT_TAG_ALARM = "alarm"
EVENT_TAG_SENSORS = "sensors"
EVENT_TAG_DATABASE = "database"
EVENT_TAG_NETWORK = "network"
EVENT_TAG_LAN = "lan"
EVENT_TAG_WAN = "wan"
EVENT_TAG_DHCP = "dhcp"
EVENT_TAG_FILE_IO = "file_io"
EVENT_TAG_NXO = "nxo"
EVENT_TAG_OS = "os"
EVENT_TAG_METER = "meter"
EVENT_TAG_MODBUS = "modbus"
EVENT_TAG_INVERTER_UPGRADE = "inverter_upgrade"
EVENT_TAG_GATEWAY_UPGRADE = "gateway_upgrade"
EVENT_TAG_STRING_DISCOVERY = "string_discovery"
EVENT_TAG_FEED_DETECTION = "feed_detection"
EVENT_TAG_POWER_FACTOR = "power_factor"
EVENT_TAG_BATTERY = "battery"
```
#### How do I create a new event tag?
To add a new event tag, first make sure it is not already defined in `event/event_constants.py` and then add a new definition at the end of the existing tag definitions. New event tags will automatically get added to the tags table in the database.
