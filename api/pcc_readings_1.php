<?php
/**
 * Created by IntelliJ IDEA.
 * User: Patrice
 * Date: 5/16/2018
 * Time: 2:13 PM
 */

include_once('functions/mysql_connect.php');

$result = $conn->query("SELECT * FROM meter_readings WHERE meter_role = 'pcc' AND data_type = 1");
if (mysqli_num_rows($result) > 0) {
    $row = $result->fetch_assoc();
    unset($row['entry_id']);
    unset($row['row_id']);
    unset($row['timestamp_received']);
    echo json_encode($row);
}else {
    die('<br>Error: ' . mysqli_error($conn));
}

?>