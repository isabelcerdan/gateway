# Apparent Gateway Debian Software Package 

## Introduction
This document describes the organization of the `apparent-gateway` debian software package that contains all necessary software to run the apparent gateway system.

Generally speaking, the apparent gateway system makes use of an apparent private apt repository which contains the apparent-gateway software package and specific compatible versions of all dependent software packages.

## Apparent Apt Repo
All debian package dependencies and version numbers are listed in `./provision/group_vars/all/full_apt_depends.yml`. 

## Building the Debian Package
To build the apparent-gateway debian package, you need to install the following:
```
$ sudo apt update
$ sudo apt install devscripts debhelper fakeroot
```
You'll also need to copy the .py2-virtualenv directory to your git clone.
```
$ cp -R /usr/share/apparent/.py2-virtualenv gateway
```
Then run the following command from the root of the git clone:
```
$ make deb
```

## Python Dependencies
Python dependencies are handled a little differently due to the gateway daemons' reliance on more current versions of Python packages. 
All Python dependencies are listed in `./requirements.txt`: 
```
six==1.5.2
MySQL-python==1.2.5
requests==2.10.0
pyserial==2.7.0
pexpect==4.2.0
-e git+https://bitbucket.org/apparentinc/pymodbus@devel3#egg=pymodbus
```
The last line declares a dependency on the `devel3` branch of our apparent `pymodbus` git clone.

When building the apparent-gateway package, these Python dependencies are installed into a [virtualenv](https://virtualenv.pypa.io/en/stable/) that is included in the apparent-gateway debian package. When the apparent-gateway package gets installed on a gateway, the virtualenv gets copied to `/usr/share/apparent/.py2-virtualenv`. 

This approach avoids having to maintain a PyPI private repo or wrap the Python packages in debian packages (e.g. python-serial, python-pymodbus, etc.). Instead, the virtualenv is simply built and included in the apparent-gateway package. All Python invocations must first activate the virtualenv using the following commands from a bash prompt:
```
source /usr/share/apparent/.py2-virtualenv/bin/activate
```
Or from a `/bin/sh` script: 
```
. /usr/share/apparent/.py2-virtualenv/bin/activate
```

### Building the Apparent Private Apt Repository
Run the following commands to build the private apt repository:
```
$ cd ~/code/gateway/provision
$ ./self-provision.sh apt_repo
```
_NOTE: Running the `./self-provision.sh apt_repo` command from `~/code/gateway/provision` on a mounted Windows directory will generate hard link errors when attempting to build the `apparent-gateway` debian package. The workaround is to create a new gateway clone outside of the ~/code mount point._

The `./self-provision.sh apt_repo` command performs the following steps:

1. Attempts to download all debian dependencies listed in `full_apt_depends.yml` to `/var/www/debs/amd64`. 
1. Builds the python virtualenv containing all python package dependencies.
1. Build the actual `apparent-gateway` software package:
    1. Use `full_apt_depends.yml` to generate the depends part of `./packaging/debian/control`.
    1. Call `make deb` from the root of the repo to build the `apparent-gateway` software package. 
1. Copy the `apparent-gateway` debian package to `/var/www/debs/amd64`.
1. Create the Packages.gz file repository directory.

To run just the last 3 steps listed above, use the following command:
```
$ ./self-provision.sh apt_repo dpkg
```

## Upgrading Package Dependencies

Internally, a development gateway will be used to qualify new packages. To update to the latest packages on this gateway, simply run provisioning with no arguments:
```
$ ./self-provision.sh public
```

This will update all packages to the latest versions available on public repos. Any new packages will be downloaded to the development gateway box in the following directory:
```
/home/gateway/gateway/provision/debs
```

Once these new packages have been qualified through manual (or perferrably automated) testing, the packages can be pushed to our private repository on Amazon Web Services (AWS). This requires the presence of the PEM identity file for our private repository in the following location on the gateway:
```
/home/gateway/.ssh/apt_repo.pem
```

The new package dependencies can be pushed to our private debian package repository on AWS using the following commands:
```
$ cd ~/gateway
$ ./self-provision.sh push
```

This command will update our complete list of packages (full_apt_depends.yml) stored on our private repository. This file needs to be checked into our gateway repository for future upgrades to work properly.

Finally, the index on our private repository needs to be rebuilt to include the latest versions of packages that have been pushed. This can be accomplished using the following commands:
```
$ ssh -i apt_repo.pem ubuntu@52.8.130.73
$ cd ~/gateway
$ ./self-provision.sh apt-repo
```

Future provisioning can now happen using only the vetted packages available on our private package repository using the following command:
```
$ ./self-provision.sh
```

### Building the apt_repo VM
An apt_repo VM can be created for development purposes using the ./provision/Vagrantfile. The Vagrantfile requires [virtual box](https://www.virtualbox.org/wiki/Downloads) and [vagrant](https://www.vagrantup.com/downloads.html) to run. Once installed, run the following commands from terminal ([cygwin](https://cygwin.com/install.html) on windows):
```
<host>$ cd ./provision
<host>$ vagrant up
```
_NOTE: The vagrant up command attempts to mount your gateway's parent directory on to `~/code` in the VM._ 

This will create the apt_repo VM with an IP address of 192.168.33.10. You can ssh into this VM by running: 
```
<host>$ vagrant ssh
```

