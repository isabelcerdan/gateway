#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
    @module: manage_inet - Manage network interfaces

    @description: This script is used by gateway provisioning to preserve the network
        interface configuration of a gateway. It is also used by the configurator to
        make changes to network interface settings.

        Script Options:
        --sync-from=db
            This option retrieves all records from gateway_db.network_config MySQL table
            and compares it to the current /etc/network/interfaces file on the system.
            If it detects any differences it will do the following:

                1. Backup the current /etc/network/interfaces to /etc/network/interfaces.bak.
                2. Overwrite /etc/network/interfaces with contents of gateway_db.network_config.
                3. Restart all network interfaces using:
                    $ ifdown --exclude=lo -a && ifup --exclude=lo -a

            This option is intended to apply changes made by a user through the configurator.

        --sync-from=file
            This option reads the network configuration from /etc/network/interfaces on the
            file system and writes it to the gateway_db.network_config table.

            This option is intended to be used by gateway provisioning to preserve existing
            network interface settings to the database. THIS OPTION DOES NOT RESTART NETWORK
            INTERFACES.

            This option is also used by the configurator to update the database before allowing
            the user to make changes.


        This script can only handle parsing basic /etc/network/interfaces config files.
            iface eth0 inet dhcp

            iface eth1 inet static
                address 10.252.0.2
                netmask 255.255.255.0
                broadcast 10.252.255.255

        For the full syntax, see here:
        https://manpages.debian.org/stretch/ifupdown/interfaces.5.en.html


    @copyright: Apparent Inc. 2019

    @author: Patrice de Muizon

    @created: January 4, 2019
"""
import os
import sys
import argparse
from jinja2 import Environment, FileSystemLoader
import MySQLdb
import MySQLdb.cursors
import datetime
from contextlib import closing
from shutil import copyfile
import subprocess

path = os.path.join(os.path.dirname(__file__), '..')
sys.path.append(path)

from lib.gateway_constants.DBConstants import *


DUPLICATE_DATA_MESSAGE = "Data appears to be duplicate"


def prepared_act_on_database(action, command, args=[]):
    """
    Execute a query on MySQL database.

    :param action:      FETCH_ONE, FETCH_ALL, FETCH_ROW_COUNT, or EXECUTE
    :param command:     String containing SQL statement with "%s" for prepared arguments
    :param args:        Array (or tuple) of prepared arguments
    :return:            List of dictionaries containing field name and value as key value pair.
    """
    try:
        # Create a closing object for use with the database connection that
        # guarantees the connection will be opened and closed at the end of
        # the block.
        with closing(MySQLdb.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                     db=DB_NAME, cursorclass=MySQLdb.cursors.DictCursor)) as connection:

            # Using the with block on a connection will perform a database transaction
            # which will commit the changes at the end of the block or rollback the changes
            # if an exception occurs.

            with connection as cursor:
                cursor.execute(command, args)
                if action == FETCH_ONE:
                    result = cursor.fetchone()
                elif action == FETCH_ALL:
                    result = cursor.fetchall()
                elif action == FETCH_ROW_COUNT:
                    fetch_response = cursor.fetchone()
                    result = fetch_response['COUNT(*)']
                elif action == EXECUTE:
                    result = cursor.lastrowid
                else:
                    print("prepared_act_on_database failed, unknown action %s" % action)
    except MySQLdb.OperationalError as ex:
        '''
        The meter readings tables (pcc_readings_1, solar_readings_1, and pcc_readigns_2) define 
        MySQL triggers that raise an exception when duplicate data is inserted. We can safely 
        ignore the exception since it is not technically an error condition.  
        '''
        if ex.args[1] != DUPLICATE_DATA_MESSAGE:
            raise ex
    except Exception as ex:
        print("prepared_act_on_database failed: command=%s, args=%s" % (command, args))

    return result


class NetworkInterface(object):
    INET_DHCP = "dhcp"
    INET_STATIC = "static"
    CONFIG_PATH = "/etc/network/interfaces"
    WAN_IFACE = "eth0"
    LAN_IFACE = "eth1"
    
    def __init__(self, **kwargs):
        self.iface = kwargs.get('iface', self.WAN_IFACE)
        self.interface = self.iface
        self.inet = kwargs.get('inet', self.INET_DHCP)
        self.address = kwargs.get('address', None)
        self.netmask = kwargs.get('netmask', None)
        self.broadcast = kwargs.get('broadcast', None)
        self.gateway = kwargs.get('gateway', None)

    def save(self):
        sql = "INSERT INTO network_config (iface, inet, address, netmask, broadcast, gateway) VALUES (%s, %s, %s, %s, %s, %s)" \
              " ON DUPLICATE KEY UPDATE iface=%s, inet=%s, address=%s, netmask=%s, broadcast=%s, gateway=%s" % (
                  "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")
        args = [self.iface, self.inet, self.address, self.netmask, self.broadcast, self.gateway,
                self.iface, self.inet, self.address, self.netmask, self.broadcast, self.gateway]
        prepared_act_on_database(EXECUTE, sql, args)

    def __repr__(self):
        return str(self.__dict__)

    def __str__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @staticmethod
    def find_all():
        network_interfaces = {}
        sql = "SELECT * FROM network_config"
        results = prepared_act_on_database(FETCH_ALL, sql, [])
        for result in results:
            network_interfaces[result['iface']] = NetworkInterface(**result)
        return network_interfaces

    @staticmethod
    def delete_all():
        sql = "DELETE FROM network_config"
        prepared_act_on_database(EXECUTE, sql, [])


class NetworkConfig(object):
    def __init__(self):
        self.network_interfaces = {}
        self.apparent_template = False

    def sync_from_file(self):
        NetworkInterface.delete_all()
        self.network_interfaces = self.load_from_file()
        for network_interface in self.network_interfaces.values():
            network_interface.save()

    def restart(self):
        ifdown = "ifdown --exclude=lo -a"
        subprocess.call(ifdown.split())
        ifup = "ifup --exclude=lo -a"
        subprocess.call(ifup.split())

    def sync_from_db(self):
        self.network_interfaces = NetworkInterface.find_all()
        if self.network_interfaces != self.load_from_file():
            self.render_to_file()
            self.restart()

    def sorted_network_interfaces(self):
        iface_order = ['lo', 'eth0', 'eth1']
        sorted_list = []
        for iface in iface_order:
            if iface in self.network_interfaces:
                sorted_list.append(self.network_interfaces[iface])
        for iface, network_interface in self.network_interfaces.iteritems():
            if iface not in iface_order:
                sorted_list.append(network_interface)
        return sorted_list

    def _get_context(self):
        context = {
            'interfaces': self.sorted_network_interfaces(),
            'wan': self.network_interfaces[NetworkInterface.WAN_IFACE],
            'lan': self.network_interfaces[NetworkInterface.LAN_IFACE],
            'timestamp': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S%P")
        }
        return context

    def render_to_file(self):
        path = "/etc/network"
        template = "interfaces.j2"
        templates_dir = os.path.join(os.path.dirname(__file__))

        outpath = os.path.join(path, "interfaces")
        backup = os.path.join(path, "interfaces.bak")
        copyfile(outpath, backup)

        env = Environment(loader=FileSystemLoader(templates_dir))
        text = env.get_template(template).render(self._get_context())
        with open(outpath, 'w') as f:
            f.write(text)

    def load_from_file(self):
        config = {}
        network_interfaces = {}
        with open(NetworkInterface.CONFIG_PATH) as f:
            for line in f:
                line = line.strip()
                if "APPARENT" in line:
                    self.apparent_template = True
                if line.startswith("iface"):
                    if config:
                        network_interfaces[config['iface']] = NetworkInterface(**config)
                        config = {}
                    raw = line.split()
                    config['iface'], config['inet'] = raw[1], raw[3]
                elif config:
                    if line and not line.startswith("#"):
                        raw = line.split()
                        config[raw[0]] = " ".join(raw[1:])
        if config:
            network_interfaces[config['iface']] = NetworkInterface(**config)

        return network_interfaces


p = argparse.ArgumentParser(description='Manage network interfaces.')
p.add_argument('--sync-from', type=str, nargs="?", help='db|file')
args = p.parse_args()

net_config = NetworkConfig()
if args.sync_from == 'db':
    net_config.sync_from_db()
elif args.sync_from == 'file':
    net_config.sync_from_file()
else:
    inet_db = NetworkInterface.find_all()
    print "db:\n%s" % inet_db
    inet_fs = net_config.load_from_file()
    print "fs:\n%s" % inet_fs
