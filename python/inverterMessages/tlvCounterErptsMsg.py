
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterErptMsg(TlvDataObject):
    


    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE,
                               "Energy Report Counters",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",     0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "erpt_fill",   1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Rejections",  2, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ReadError",   4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "GetNoServer", 6, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DelNoServer", 8, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DelErpt",    10, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "WipeErpt",   12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "SchemaMove", 14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DoubleDisconnect",       16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "TelnetDoubleDisconnect", 18, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "HttpDoubleDisconnect",   20, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UnkHttpRequests",        22, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "UnExpAck",      24, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "IncorrectAck",  26, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "WriteErpt",     28, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "SkipWriteErpt", 30, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "WriteFailErpt", 32, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptAlarmSent", 34, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptInfoSent",  36, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptAck",       38, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptTimeout",   40, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptGtwAlarmSent", 42, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptGtwInfoSent",  44, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptGtwAck",       46, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptGtwTimeout",   48, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErptGtwReportDropped", 50, 2, "!H"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
