
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterEepromErrMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE,
                               "EEPROM Error Counters",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",           0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "eep_fill",          1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "PutConfigFailures", 2, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "GetConfigFailures", 4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrorFlags",        6, 4, "!I"))

        self.loadDataSize(10)

    def unpackDataStruct(self, data):
        self.unpackCommon(data)
        
        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True


    @staticmethod
    def getSet(val, mask):
        if (val & mask) != 0:
            return "Set"
        else:
            return "Clear"

    def dumpDecodedBuf(self):

        FLAG_GET_EE_SETTINGS_ERRORS = 0x00000001
        FLAG_GET_EE_IMMED_ERRORS = 0x00000002
        FLAG_GET_EE_VRTS_ERRORS = 0x00000004
        FLAG_GET_EE_FRTS_ERRORS = 0x00000008
        FLAG_GET_EE_VW_ERRORS = 0x00000010
        FLAG_GET_EE_VV_ERRORS = 0x00000020
        FLAG_GET_EE_FW_ERRORS = 0x00000040
        FLAG_GET_EE_VFF_ERRORS = 0x00000080
        FLAG_GET_EE_FFF_ERRORS = 0x00000100
        FLAG_NTP_SERVER_URL_GET_FAILED = 0x00000200
        FLAG_GET_PROD_NUM_STRING_FROM_EEPROM_FAILED = 0x00000400

        retStr = super(TlvCounterEepromErrMsg, self). dumpDecodedBuf()
        flags = self.getDataValByName('ErrorFlags')

        retStr += "settings bit: " + self.getSet(flags, FLAG_GET_EE_SETTINGS_ERRORS) + "\n\r"
        retStr += "immed bit: "    + self.getSet(flags, FLAG_GET_EE_IMMED_ERRORS) + "\n\r"
        retStr += "vrst bit: "     + self.getSet(flags, FLAG_GET_EE_VRTS_ERRORS) + "\n\r"
        retStr += "frts bit: "     + self.getSet(flags, FLAG_GET_EE_FRTS_ERRORS) + "\n\r"
        retStr += "vw bit: "       + self.getSet(flags, FLAG_GET_EE_VW_ERRORS) + "\n\r"
        retStr += "vv bit: "       + self.getSet(flags, FLAG_GET_EE_VV_ERRORS) + "\n\r"
        retStr += "fw bit: "       + self.getSet(flags, FLAG_GET_EE_FW_ERRORS) + "\n\r"
        retStr += "vff bit: "      + self.getSet(flags, FLAG_GET_EE_VFF_ERRORS) + "\n\r"
        retStr += "fff bit: "      + self.getSet(flags, FLAG_GET_EE_FFF_ERRORS) + "\n\r"
        retStr += "ntp bit: "      + self.getSet(flags, FLAG_NTP_SERVER_URL_GET_FAILED) + "\n\r"
        retStr += "prod_num bit: " + self.getSet(flags, FLAG_GET_PROD_NUM_STRING_FROM_EEPROM_FAILED) + "\n\r"

        return retStr


    def testMsg(self):
        msg = bytearray(self.getDataSize())
        
        return msg
