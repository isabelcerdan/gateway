
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterMiscMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE,
                               "Misc Counters",
                               ipAddr)
        
        self.loadFullDataDef(dataStruct(1, "version", 0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "misc_fill", 1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "SntpPktError", 2, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PortOpenFail", 4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UdpNotReady", 6, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UdpPutDataError", 8, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PortSwitchError", 10, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "CliCmdTooLong", 12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "NtpServerUrlUpdate", 14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "EnergyCtrlOc2Alarm", 16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "NbnsOpenError", 18, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "NbnsPktError", 20, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "NbnsPktCnt", 22, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "I2CRead", 24, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "I2CWrite", 28, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "I2CReadError", 32, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "I2CWriteError", 34, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "MacBufferSoftResets", 36, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ResetReason", 38, 2, "!H"))


    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
