
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvAifVoltWattElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_VOLT_WATT_STRUCTURED_TLV_TYPE,
                               "AIF Volt-Watt Data",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",                    0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "enable",                     1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "preDisturbanceModeEnable",   2, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "curveName",                  3, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "curveId",                    4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "voltPoint1Pct",              6, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "wattPoint1Pct",             10, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint2Pct",             14, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "wattPoint2Pct",             18, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint3Pct",             22, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "wattPoint3Pct",             26, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint4Pct",             30, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "wattPoint4Pct",             34, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "responseTimeMsec",          38, 4, "!I"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
