
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterLldpMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE,
                               "LLDP Msg Counters",
                               ipAddr)
        
        self.loadFullDataDef(dataStruct(1, "version", 0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "lldp_fill", 1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "AnnounceTxPkt", 2, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "NeighborTxPkt", 6, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "AnnounceRxPkt", 10, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "NeighborRxPkt", 14, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "InvalidMac", 18, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "InvalidSG424OuiMac", 20, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "InvalidMacTable", 22, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "MacNotInSlot1", 24, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "MacNotInNeighborPkt", 26, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "MacNotReady", 28, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "FromNetworkAtLarge", 30, 2, "!H"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
