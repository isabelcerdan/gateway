
import struct
from lib.gateway_constants.InverterConstants import *
from tlvDataObject import *


class TlvPsaStatusLatched(TlvDataObject):

    __headerLabelStr = "|     States              Status Bits         (LATCHED)    "
    __headerItemStr =  "| vr us op ad    avail    power     info    admin     oper "

    __nameStr = "PSA Status Latched"

    __version         = dataStruct(1, " version",          0,   1,  "!B")
    __usageState      = dataStruct(1, " usageState",       1,   1,  "!B")
    __opState         = dataStruct(1, " opState",          2,   1,  "!B")
    __adminState      = dataStruct(1, " adminState",       3,   1,  "!B")
    __fill            = dataStruct(1, " fill",             4,   4,  "!BBBB")
    __availStatusBits = dataStruct(1, " availStatusBits",  8,   4,  "!I")
    __powerStatusBits = dataStruct(1, " powerStatusBits", 12,   4,  "!I")
    __infoStatusBits  = dataStruct(1, " infoStatusBits",  16,   4,  "!I")
    __adminStatusBits = dataStruct(1, " adminStatusBits", 20,   4,  "!I")
    __opStatusBits    = dataStruct(1, " opStatusBits",    24,   4,  "!I")

    __elementSize = 28

    __availStatusBitsStr = ["IDLE",
                   "ACTIVE",
                   "STARTUP",
                   "NIGHT",
                   "SLEEP",
                   "GW_CTRL",
                   "GW_GONE"]

    __powerStatusBitsStr = ["MPP",
         "MAX",
         "LIMITED",
         "GW_CURT",
         "GW_REG",
         "GW_FB",
         "IMMED",
         "VW",
         "FW",
         "VAR_PRI",
         "IOUT"]

    __infoStatusBitsStr =    ["TRO"]


    __adminStatusBitsStr = ["UNLOCKED",
         "LOCKED",
         "LOCKED_LP"]


    __opStatusBitsStr = ["ENABLED",
         "DISABLED",
         "UNINIT - (TBD)",
         "DIAG_FAIL",
         "TEMP_STBY",
         "BOB",
         "DCOVP",
         "OCP",
         "TROV2",
         "NO_IGN",
         "HIGH_TEMP",
         "LOW_TEMP",
         "DCIN_P",
         "DCIN_V",
         "DCIN_I",
         "ACOV_1",
         "ACOV_2",
         "ACOV_3",
         "PLL_LOL",
         "PLL_NOLOK",
         "PLL_ISLAND",
         "BAD_PROFILE"]


    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE,
                               self.__nameStr,
                               ipAddr)
        self.data = None
        self.tlv = None

        self.version = 0
        self.usageState = 0
        self.opState = 0
        self.adminState = 0
        self.fill = 0
        self.availStatusBits = 0
        self.powerStatusBits = 0
        self.infoStatusBits = 0
        self.adminStatusBits = 0
        self.opStatusBits = 0



    def unpackDataStruct(self, data):
        self.data = data
        self.tlv = TlvUnpacker(False, data)

        try:
            self.version = self.tlv.unpackFmtItem(self.__version)
        except Exception as e:
            raise TlvExceptionNoVersion(self.tlvId)

        if self.version > 1:
            raise TlvExceptionUnsupportedVersion(self.tlvId,
                                                 self.version)

        if self.version == 1:
            if len(data) != self.__elementSize:
                raise TlvExceptionWrongSize(self.tlvId,
                                            self.version,
                                            len(data),
                                            self.__elementSize)

        self.version = self.tlv.unpackFmtItem(self.__version)
        self.usageState = self.tlv.unpackFmtItem(self.__usageState)
        self.opState = self.tlv.unpackFmtItem(self.__opState)
        self.adminState = self.tlv.unpackFmtItem(self.__adminState)
        self.availStatusBits = self.tlv.unpackFmtItem(self.__availStatusBits)
        self.powerStatusBits = self.tlv.unpackFmtItem(self.__powerStatusBits)
        self.infoStatusBits = self.tlv.unpackFmtItem(self.__infoStatusBits)
        self.adminStatusBits = self.tlv.unpackFmtItem(self.__adminStatusBits)
        self.opStatusBits = self.tlv.unpackFmtItem(self.__opStatusBits)


    def dumpDataBuffer(self):
        retStr = self.__nameStr + "\n\r"
        retStr += self.dumpHexBuf(self.data)
        return retStr

    def usageStateStr(self):
        retStr = "Unknown"

        if self.usageState == 0:
            retStr = "Idle"
        elif self.usageState == 1:
            retStr = "Active"

        return retStr

    def opStateStr(self):
        retStr = "Unknown"

        if self.opState == 0:
            retStr = "Enabled"
        elif self.opState == 1:
            retStr = "Disabled"

        return retStr

    def adminStateStr(self):
        retStr = "Unknown"

        if self.adminState == 0:
            retStr = "Unlocked"
        elif self.adminState == 1:
            retStr = "Locked"

        return retStr

    def decodeStatusBits(self, bitStr, val):
        retStr = ""
        checkedBits = 0

        for i in range(0,len(bitStr)):
            mask = (1 << i)
            checkedBits += mask

            if (mask & val):
                bs = "Set"
            else:
                bs = "Clear"

            retStr += ("[%02d] %s = %s\n\r" % (i, bitStr[i], bs))

        if ((~checkedBits & val) > 0):
            retStr += "***Unknown Bits set" + "0x{:x}".format((val & (~checkedBits)))
            retStr += "\n\r"

        return retStr

    def dumpDecodedBuf(self):

        retStr = self.__nameStr + "\n\r"
        retStr += ("%s = %u\n\r" % (self.__version.name, self.version))

        retStr += ("%s = %u (%s)\n\r" % (self.__usageState.name,
                                      self.usageState,
                                      self.usageStateStr()))

        retStr += ("%s = %u (%s)\n\r" % (self.__opState.name,
                                      self.opState,
                                      self.opStateStr()))

        retStr += ("%s = %u (%s)\n\r" % (self.__adminState.name,
                                      self.adminState,
                                      self.adminStateStr()))

        retStr += ("%s = 0x%08x\n\r" % (self.__availStatusBits.name, self.availStatusBits))
        retStr += self.decodeStatusBits(self.__availStatusBitsStr, self.availStatusBits)

        retStr += ("%s = 0x%08x\n\r" % (self.__powerStatusBits.name, self.powerStatusBits))
        retStr += self.decodeStatusBits(self.__powerStatusBitsStr, self.powerStatusBits)

        retStr += ("%s = 0x%08x\n\r" % (self.__infoStatusBits.name, self.infoStatusBits))
        retStr += self.decodeStatusBits(self.__infoStatusBitsStr, self.infoStatusBits)

        retStr += ("%s = 0x%08x\n\r" % (self.__adminStatusBits.name, self.adminStatusBits))
        retStr += self.decodeStatusBits(self.__adminStatusBitsStr, self.adminStatusBits)

        retStr += ("%s = 0x%08x\n\r" % (self.__opStatusBits.name, self.opStatusBits))
        retStr += self.decodeStatusBits(self.__opStatusBitsStr, self.opStatusBits)

        return retStr

    def dumpShortDecodedBuf(self):

        retStr = ""
        retStr += " {:>2d}".format(self.version)
        retStr += " {:>2d}".format(self.usageState)
        retStr += " {:>2d}".format(self.opState)
        retStr += " {:>2d}".format(self.adminState)
        retStr += " {:08X}".format(self.availStatusBits)
        retStr += " {:08X}".format(self.powerStatusBits)
        retStr += " {:08X}".format(self.infoStatusBits)
        retStr += " {:08X}".format(self.adminStatusBits)
        retStr += " {:08X}".format(self.opStatusBits)

        return retStr

    @classmethod
    def dumpHeaderStrings(self):
        return self.__headerLabelStr, self.__headerItemStr


    def testMsg(self):
        msg = struct.pack("!hh", self.tlvId, self.__elementSize)
        msg += struct.pack(self.__version.fmtStr, 1)
        msg += struct.pack(self.__usageState.fmtStr, 2)
        msg += struct.pack(self.__opState.fmtStr, 2)
        msg += struct.pack(self.__adminState.fmtStr, 2)
        msg += struct.pack(self.__fill.fmtStr, 0,0,0,0)
        msg += struct.pack(self.__availStatusBits.fmtStr, 2)
        msg += struct.pack(self.__powerStatusBits.fmtStr, 2)
        msg += struct.pack(self.__infoStatusBits.fmtStr, 2)
        msg += struct.pack(self.__adminStatusBits.fmtStr, 2)
        msg += struct.pack(self.__opStatusBits.fmtStr, 2)

        return msg


