

from lib.gateway_constants.InverterConstants import *
from tlvDataObject import *


class TlvAifFreqWattElement(TlvDataObject):
    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_FREQ_WATT_STRUCTURED_TLV_TYPE,
                               "AIF Freq-Watt Data",
                               ipAddr)

        
        self.loadFullDataDef(dataStruct(1, "version", 0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "enable", 1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "preDisturbanceModeEnable", 2, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "curveName", 3, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "curveId", 4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "kof", 6, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "dbof", 10, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "kuf", 14, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "dbuf", 18, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "responseTimeMsec", 22, 4, "!I"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
