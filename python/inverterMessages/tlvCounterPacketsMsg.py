
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterPacketMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE,
                               "Packet Counters",
                               ipAddr)
       
        self.loadFullDataDef(dataStruct(1, "version",                   0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "pkt_fill",                  1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Analyzed",                  2, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "Unicast",                   6, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "McastForMe",               10, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "EncapCmdSet",              14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "EncapCmdSetOthers",        16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DynmicPowerFactorCmd",     18, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "McastForOtherInverter",    22, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "McastForOtherPcc",         24, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "McastForOtherFeed",        26, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "IpNotForMe",               28, 2, "!H"))


    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
