import struct
import os
import MySQLdb
import MySQLdb.cursors
import time
import traceback

from lib.db import prepared_act_on_database, FETCH_ONE, FETCH_ALL, EXECUTE


from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvEnergyMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_ENERGY_REPORT_STRUCTURED_TLV_TYPE,
                               "Energy Msg",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",            0,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "fill",               1,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "serial",             2, 14, "12s"))
        self.loadFullDataDef(dataStruct(1, "product",           16, 22, "20s"))
        self.loadFullDataDef(dataStruct(1, "swRev",             38, 34, "32s"))
        self.loadFullDataDef(dataStruct(1, "schema",            72,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "type",              74,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "fillB",             75,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "flags",             76,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "utcTimestampStart", 78,  4, "!L"))
        self.loadFullDataDef(dataStruct(1, "interval",          82,  2, "!h"))
        self.loadFullDataDef(dataStruct(1, "DcVsolar",          84,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "DcIsolar",          88,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "AcVout",            92,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "AcIout",            96,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "phase",            100,  2, "!h"))
        self.loadFullDataDef(dataStruct(1, "watt",             102,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "var",              106,  4, "!f"))
        self.loadFullDataDef(dataStruct(1, "temp",             110,  4, "!f"))

    #
    # unpack the input data
    #
    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            if (i.name == 'serial' or
                i.name == 'product' or
                i.name == 'swRev'):
                self.loadDataVal(i.name, self.tlv.unpackString(i))

            else:
                self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    #
    # persist the data to the database
    #
    def persistData(self, rxTime):
        execute_string = "INSERT INTO %s " \
                         "VALUES (NULL, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,0);" % \
                         (DB_ENERGY_REPORTS_TABLE,
                          "%s", "%s", "%s", "%s", "%s", "%s", "%s",
                          "%s", "%s", "%s", "%s", "%s", "%s", "%s")

        timeStr = time.strftime("%Y-%m-%d %H:%M:%S ", time.localtime(rxTime))
        some_args = (timeStr,
                     self.getDataValByName("serial"),
                     self.getDataValByName("utcTimestampStart"),
                     self.getDataValByName("schema"),
                     self.getDataValByName("flags"),
                     self.getDataValByName("interval"),
                     self.getDataValByName("DcVsolar"),
                     self.getDataValByName("DcIsolar"),
                     self.getDataValByName("AcVout"),
                     self.getDataValByName("AcIout"),
                     self.getDataValByName("phase"),
                     self.getDataValByName("watt"),
                     self.getDataValByName("var"),
                     self.getDataValByName("temp"))

        prepared_act_on_database(EXECUTE, execute_string, some_args)



    #
    # build the acknowledgment TLV
    #
    def buildAcknowledgeData(self, rxTime, ok):
        ackTlv = struct.pack('!H', GTW_TO_SG424_REPORT_ACK_TLV_TYPE)
        ackTlv += struct.pack('!H', 6)

        flag = ERPT_ACK_MSG_RECEIVED_FLAG
        if (ok == False):
            flag = ERPT_ACK_MSG_REJECTED_FLAG

        ackTlv += struct.pack('B', flag)
        ackTlv += struct.pack('B', self.getDataValByName("type"))
        ackTlv += struct.pack('!L', self.getDataValByName("utcTimestampStart"))

        return ackTlv

    #
    # get data for logging
    #
    def logStr(self):
        flags = self.getDataValByName("flags")

        s = ", sn="
        s += str(self.getDataValByName("serial"))
        s += ", utc="
        s += str(self.getDataValByName("utcTimestampStart"))
        s += ", sch="
        s += str(self.getDataValByName("schema"))
        s += ", dur="
        s += '{:04d}'.format(self.getDataValByName("interval"))
        s += ", dcV="
        s += '{:06.3f}'.format(self.getDataValByName("DcVsolar"))
        s += ", dcI="
        s += '{:06.3f}'.format(self.getDataValByName("DcIsolar"))
        s += ", acV="
        s += '{:07.3f}'.format(self.getDataValByName("AcVout"))
        s += ", ph="
        s += '{:03d}'.format(self.getDataValByName("phase"))
        s += ", w="
        s += '{:05.1f}'.format(self.getDataValByName("watt"))
        s += ", var="
        s += '{:07.3f}'.format(self.getDataValByName("var"))
        s += ", temp="
        s += '{:05.2f}'.format(self.getDataValByName("temp"))

        s += ", flag="
        s += '0x{:04X}'.format(flags)

        s += " ("
        if (flags & ERPT_FLAG_LOCKED):
            s += 'locked, '
        else:
            s += 'unlocked, '

        if (flags & EPRT_FLAG_DISABLED):
            s += 'disabled'
        else:
            s += 'not disabled'

        if (flags & ERPT_FLAG_CATCHUP_REPORT):
            s += ', catchUp'
        s += ")"

        return s

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
