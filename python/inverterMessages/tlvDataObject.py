#
#
"""
tlvDataObject.py

Contains two classes
TlvUnpacker - Ultilties for unpacking data in a TLV
TlvDataObject -  TLV Object class

"""

import struct
import string

from collections import namedtuple

dataStruct = namedtuple("dataStruct", "version name offset size fmtStr")

class TlvException(Exception):
    """Base Class for TLV execptions"""
    pass

class TlvExceptionNoMaster(TlvException):
    def __init__(self):
        self.msg = "No master TLV found"


class TlvExceptionNoVersion(TlvException):
    def __init__(self, tlvId):
        self.msg = "No Version in data for TLV id=%u" % (tlvId)

class TlvExceptionUnsupportedVersion(TlvException):
    def __init__(self, tlvId, version):
        self.msg = "Unsupport version in data for for TLV id=%u, version=%u" % (tlvId, version)


class TlvExceptionWrongSize(TlvException):
    def __init__(self, tlvId, version, rxSize, expSize):
        self.msg = "Incorrect buffer size for TLV id=%u, version=%u, got %u bytes, expected %u" % \
                   (tlvId, version, rxSize, expSize)

class TlvExceptionNoNameData(TlvException):
    def __init__(self, name):
        self.msg = ("data with name %s not found" % name)

class TlvExceptionDataIndexOutOfRange(TlvException):
    def __init__(self, index):
        self.msg = ("data index %u is out of range" % index)

class TlvExceptionCannotParseData(TlvException):
    def __init__(self, tlvId, version):
        self.msg = "Unable to load data forTLV id=%u, version=%u" % (tlvId, version)

class TlvExceptionCannotLoadData(TlvException):
    def __init__(self, tlvId):
        self.msg = "Unable to load data forTLV id=%u" % tlvId

class TlvUnpacker(object):
    def __init__(self, debug, data):
        self.debug = debug
        self.data = data

    def getLength(self):
        return len(self.data)

    def getValue(self):
        return self.data

    def getValueInRange(self, start, end):
        if start < 0 or end < 0:
            raise Exception('Range less than zero')
        if start >  end:
            raise Exception('Start greater then end')
        if end > self.getLength():
            raise Exception('End greater then length')

        return self.data[start:end]

    def unpackFmtItem(self, fmt):
        # type: (object) -> object
        #print("unpack %s %s, offset=%d, size=%d" % (fmt.name,fmt.fmtStr, fmt.offset,fmt.size))
        item = None

        # Special case, if size is 0 then the data structure element was removed, but return
        # 0 so data can be logged/displayed properly
        if fmt.size == 0:
            item  = 0
        else:
            if fmt.offset >= 0 and fmt.offset <= self.getLength() - fmt.size:
                (item,) = struct.unpack_from(fmt.fmtStr, self.data, fmt.offset)
            else:
                raise Exception('Unable to parse data %s at offset=%u' %
                                (fmt.name, fmt.offset))
        return item

    def unpackArray(self, fmt):
        #print("unpackArray %s %s, offset=%d, size=%d" % (fmt.name, fmt.fmtStr, fmt.offset, fmt.size))
        item = None

        if fmt.offset >= 0 and fmt.offset <= self.getLength() - fmt.size:
            item = struct.unpack_from(fmt.fmtStr, self.data, fmt.offset)
        else:
            raise Exception('Unable to parse data %s at offset=%u' %
                            (fmt.name, fmt.offset))
        return item

    def unpackSizedU16Array(self, fmt, size):
        #print("unpackArray %s %s, offset=%d, size=%d" % (fmt.name, fmt.fmtStr, fmt.offset, fmt.size))
        item = None

        if fmt.offset >= 0 and fmt.offset <= self.getLength() - fmt.size:
            item = struct.unpack_from(fmt.fmtStr, self.data, fmt.offset)
        else:
            raise Exception('Unable to parse data %s at offset=%u' %
                            (fmt.name, fmt.offset))
        return item

    def unpackString(self, fmt):
        #print("unpackString %s %s, offset=%d, size=%d" % (fmt.name, fmt.fmtStr, fmt.offset, fmt.size))
        item = ""

        #extract the character until NULL is found or length of string
        if fmt.offset >= 0 and fmt.offset <= self.getLength() - fmt.size:
            for char_c in self.data[fmt.offset : (fmt.offset + fmt.size)]:
                if ord(char_c) != 0:
                    item += chr(ord(char_c))
                else:
                    break
        else:
            raise Exception('Unable to parse data %s at offset=%u' %
                            (fmt.name, fmt.offset))
        return item


    def unpackU16(self, offset):
        if offset >= 0 and offset <= self.getLength() - 2:
            (item,) = struct.unpack_from("!H", self.data, offset)
        else:
            raise Exception('Unable to parse U16 at offset=%u' % offset)

        return item

    def unpackU8(self, offset):
        if offset >= 0 and offset <= self.getLength() - 1:
            (item,) = struct.unpack_from("B", self.data, offset)
        else:
            raise Exception('Unable to parse U8 at offset=%u' % offset)

        return item


class TlvDataObject(object):
    def __init__(self, id, str, ipAddr):
        self.tlvId = id
        self.str = str
        self.ipAddr = ipAddr
        self.dataDef = []
        self.fullDataDef = []
        self.maxVerSupported = 0
        self.dataVal = {}
        self.dataSize = 0
        self.dataBuf = None
        self.version = 0
        self.tlv = None
        self.useHighestVersion = False

    def __str__(self):
        return ("IP=%s TLV=%u, %s" % (self.ipAddr, self.tlvId, self.str))

    def getTlvId(self):
        return self.tlvId

    def getIpAddr(self):
        return self.ipAddr

    def loadDataSize(self,size):
        self.dataSize = size
        return

    def getDataSize(self):
        return self.dataSize

    def getDataName(self):
        return self.str

    def loadDataDef(self,dd):
        self.dataDef.append(dd)
        return

    def loadFullDataDef(self, dd):
        self.fullDataDef.append(dd)

        if dd.version > self.maxVerSupported:
            self.maxVerSupported = dd.version
        return

    def loadVersionDataDef(self, dataVersion):
        size = 0

        for dd in self.fullDataDef:
            if (dd.version <= dataVersion):
                self.dataDef.append(dd)
                size += dd.size

        self.loadDataSize(size)
        return


    def loadData(self,buf):
        self.dataBuf = buf
        return

    def getData(self):
        return self.dataBuf

    def getDataDefByIndex(self,index):
        #print ("in getDataDefByIndex, index=%u" % index)
        if index >= 0 and index < len(self.dataDef):
            return self.dataDef[index]

        raise TlvExceptionDataIndexOutOfRange(index)

    def getDataDefByName(self,name):
        #print ("in getDataDefByName, name=%s" % name)
        for dd in self.dataDef:
            if (dd.name == name):
                return dd

        raise TlvExceptionNoNameData(name)

    def loadDataVal(self,name, val):
        #print ("in loadDataVal, name=%s" % name)
        self.dataVal[name] = val
        return

    def getDataValByName(self,name):
        #print ("in getDataValByName, name=%s" % name)
        if name in self.dataVal:
            return self.dataVal[name]

        raise TlvExceptionNoNameData(name)

    def getDataValueArray(self):
        return self.dataVal

    def getDataDefArray(self):
        return self.dataDef



    def unpackCommon(self, data):
        self.loadData(data)
        self.tlv = TlvUnpacker(False, data)

        try:
            self.version = self.tlv.unpackU8(0)
        except Exception as e:
            raise TlvExceptionNoVersion(self.tlvId)

        if self.version > self.maxVerSupported:
            raise TlvExceptionUnsupportedVersion(self.tlvId, self.version)

        # load data defination used for the version of data from the inverter
        self.loadVersionDataDef(self.version)

        if len(data) != self.getDataSize():
            raise TlvExceptionWrongSize(self.tlvId,
                                        self.version,
                                        len(data),
                                        self.getDataSize())


    def unpackDataStruct(self, data):
        return("unpack () Not Implemented for TLV=%u" % self.tlvId)

    def persistData(self, rxTime):
        print ("persistData () Not Implemented for TLV=%u" % self.tlvId)
        return False

    def buildAcknowledgeData(self, rxTime, ok):
        print ("sendAcknowledge () Not Implemented for TLV=%u" % self.tlvId)
        return False

    def dumpDecodedBuf(self):
        retStr = self.getDataName() + "\n\r"

        if self.useHighestVersion:
            for i in self.dataDef:
                retStr += ("%s = %s\n\r" % (i.name, str(self.getDataValByName(i.name))))

        else:
            for i in self.dataDef:
                retStr += ("%s = %s\n\r" % (i.name, str(self.getDataValByName(i.name))))

        return retStr

    def dumpShortDecodedBuf(self):
        retStr = ""

        if self.useHighestVersion:
            for dd in self.dataDef:
                if (dd.version <= self.version):
                    retStr += str(self.getDataValByName(dd.name)) + ","

        else:
            for dd in self.fullDataDef:
                if (dd.version <= self.version):
                    retStr += str(self.getDataValByName(dd.name)) + ","
                else:
                    retStr += "X,"

        return retStr

    def dumpDataBuffer(self):
        retStr = self.getDataName() + "\n\r"
        retStr += self.dumpHexBuf(self.dataBuf)
        return retStr

    def dumpHeaderStrings(self):
        retStr = ""

        for dd in self.fullDataDef:
            if self.useHighestVersion:
                if dd.version == self.maxVerSupported:
                    retStr += dd.name + ","
            else:
                retStr += dd.name + ","



        return self.getDataName(), retStr


    @classmethod
    def dumpHexBuf(self, data):
        if data is None:
            return "No Data Loaded"

        buf = ""
        lineCnt = int((len(data) / 8) + 1)
        idx = 0
        remaining = len(data)
        for j in range(0, lineCnt):
            hexStr = ""
            asciiStr = ""

            if (remaining >= 8):
                count = 8
            else:
                count = remaining

            remaining -= count
            for jj in range(0, count):

                # for Python 3.x
                #val = data[idx]
                # for Python 2.x
                (val,) = struct.unpack_from("B", data[idx])
                idx += 1
                hexStr += "%02x " % val

                if all(c in string.printable for c in chr(val)):
                    if (val < 0x20):
                        asciiStr += "."
                    else:
                        asciiStr += chr(val)
                else:
                    asciiStr += "."

            if count < 8:
                for jj in range(count, 8):
                    hexStr += "   "

            if (j == 0):
                buf += "\r\n"

            buf += (("%3s" % " " + "[%03u] " % ((j) * 8)) + hexStr + "  " + asciiStr + "\r\n")

        return buf


    def loadSpecificVersionDataDef(self, dataVersion):
        size = 0

        for dd in self.fullDataDef:
            if (dd.version == dataVersion):
                self.dataDef.append(dd)
                size += dd.size

        self.loadDataSize(size)
        return


    def unpackCommonSpecificVersion(self, data):
        self.loadData(data)
        self.tlv = TlvUnpacker(False, data)

        try:
            self.version = self.tlv.unpackU8(0)
        except Exception as e:
            raise TlvExceptionNoVersion(self.tlvId)

        if self.version > self.maxVerSupported:
            raise TlvExceptionUnsupportedVersion(self.tlvId, self.version)

        # load data defination used for the version of data from the inverter
        self.loadSpecificVersionDataDef(self.version)

        if len(data) != self.getDataSize():
            raise TlvExceptionWrongSize(self.tlvId,
                                        self.version,
                                        len(data),
                                        self.getDataSize())

