
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvAifVoltVarElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_VOLT_VAR_STRUCTURED_TLV_TYPE,
                               "AIF Volt-Var Data",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",                    0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "enable",                     1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "curveName",                  2, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "spare",                      3, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "curveId",                    4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "voltPoint1Pct",              6, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "varPoint1Pct",              10, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint2Pct",             14, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "varPoint2Pct",              18, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint3Pct",             22, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "varPoint3Pct",              26, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "voltPoint4Pct",             30, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "varPoint4Pct",              34, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "responseTimeMsec",          38, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "deadBandOperation",         42, 2, "!H"))

        self.loadDataSize(44)

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
