#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import socket
import struct
import string
import time
import datetime
import os
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.GatewayConstants import *
import gateway_utils as gu
import tlv_utils

from tlvMsgPeek import *
from tlvObjectList import *
from tlvDataObject import *

class GptsTlvQuery(object):
    def __init__(self, argV):
        self.argV = argV
        self.doHexDump = False
        self.doShortDump = False
        self.doMsgDump = False
        self.u32Data = -2

    @staticmethod
    def print_query_help():
        print("- send tlv query message for testing:       gpts.py iqm                <id=xx hex short list msg ipAddr>")
        print("                                                                        'xx' = TLV Id - required")
        print("                                                                        'list', if set than list out query TLVs supported")
        print("                                                                        'hex', if set than hex dump is output")
        print("                                                                        'short', if set than short dump is output")
        print("                                                                        'msg', if set than dump whole message in hex")
        print("                                                                        'ipAddr' ip address - required")

    @staticmethod
    def print_supported_TLVs():
        print("Supported Query TLVs:")
        print("%u - Power Status" % GTW_TO_SG424_PSA_POWER_STATUS_U16_TLV_TYPE)
        print("%u - Power All Status" % GTW_TO_SG424_PSA_POWER_ALL_STATUS_U16_TLV_TYPE)
        print("%u - XET Info" % GTW_TO_SG424_XET_INFO_TLV_TYPE)
        print("%u - AIF VRT" % GTW_TO_SG424_XET_AIF_VRT_TLV_TYPE)
        print("%u - AIF FRT" % GTW_TO_SG424_XET_AIF_FRT_TLV_TYPE)
        print("%u - AIF Volt-Watt" % GTW_TO_SG424_XET_AIF_VOLT_WATT_TLV_TYPE)
        print("%u - AIF Volt-Var" % GTW_TO_SG424_XET_AIF_VOLT_VAR_TLV_TYPE)
        print("%u - AIF Freq-Watt" % GTW_TO_SG424_XET_AIF_FREQ_WATT_TLV_TYPE)
        print("%u - AIF Curves" % GTW_TO_SG424_XET_AIF_CURVE_TLV_TYPE)
        print("%u - AIF Status" % GTW_TO_SG424_XET_AIF_STATUS_TLV_TYPE)
        print("%u - Counter Query" % GTW_TO_SG424_PACKED_COUNTERS_QUERY_U32_TLV_TYPE)

    def send_unicast_query(self, ip_addr, tlvMsgId, msgData):

        # Assumption: the IP has been validated to the extent that it looks like an IP.
        SG424_data = None
        packed_data = gu.add_master_tlv(1)
        if self.u32Data >= 0:
            packed_data = gu.append_user_tlv_32bit_unsigned_long(packed_data, tlvMsgId, self.u32Data)
        else:
            packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, tlvMsgId, msgData)

        # OPEN THE SOCKET:
        try:
            fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        except Exception:  # as error:
            print("socket.socket failed ...")

        else:
            # SEND THE PACKET:
            try:
                fancy_socks.sendto(packed_data, (ip_addr, SG424_PRIVATE_UDP_PORT))

            except Exception:  # as error:
                print("sendto failed ...")

            # Now wait for a response:
            else:
                try:
                    fancy_socks.settimeout(3.0)
                    packet = fancy_socks.recvfrom(1024)

                except Exception:
                    # Timed out waiting to receive a packet. Simply means the inverter
                    # at that IP is not responding.
                    pass

                else:
                    # Packet received, data is in private_port_packet field.
                    (addr, port, SG424_data) = gu.get_udp_components(packet)

                    try:
                        if self.doMsgDump:
                            print ("Dump of RX msg:")
                            print(TlvDataObject.dumpHexBuf(SG424_data))

                        tlvList = msgPeek(False, SG424_data)
                        #tlvList = msgPeek(True, SG424_data)

                        msgs = tlvList.getTlvDict()

                        for key in msgs:
                            try:
                                obj = TlvObjects(key, ip_addr)
                                tlvObj = obj.getObj()

                                if tlvObj is None:
                                    raise Exception("Unknown TLV type=%u" % key)

                                print(tlvObj)
                                tlvObj.unpackDataStruct(msgs[key])

                                if tlvObj.getTlvId() == SG424_TO_GTW_MASTER_U16_TLV_TYPE:
                                    pass
                                else:
                                    print(tlvObj.dumpDecodedBuf())
                                    if True == self.doHexDump:
                                        print(tlvObj.dumpDataBuffer())

                                    if True == self.doShortDump:
                                        print(tlvObj.dumpShortDecodedBuf())

                            except TlvExceptionWrongSize as e:
                                print(e.msg)
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionNoVersion as e:
                                print(e.msg)
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionUnsupportedVersion as e:
                                print(e.msg)
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionNoMaster as e:
                                print(e.msg)

                            except  Exception as e:
                                print(("Can't parse TLV id=%u, len=%u " % (key, len(msgs[key])) + str(e)))
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))


                    except Exception as e:
                        print ("Unable to parse tmp: " + str(e))

            # Close the socket:
            try:
                fancy_socks.close()

            except Exception:  # as error:
                print("close socket failed ...")

            else:
                pass

        return SG424_data


    def parse_cmd_line_tlv_query(self, argv):
        tlvId = 0
        hexDump = False
        shortDump = False
        listTlvs = False
        msgDump = False
        u32Data = -2
        newArgs = []

        # peel out 'tlvId' command line args if they exist and
        # create a newArgs list
        for arg in argv:
            if arg.upper() == "HEX":
                hexDump = True
            elif arg.upper() == "SHORT":
                shortDump = True
            elif arg.upper() == "LIST":
                listTlvs = True
            elif arg.upper() == "MSG":
                msgDump = True
            elif "ID" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("Bad tlv id, missing")
                    tlvId = 0

                try:
                    tlvId = int(sp[1])
                except:
                    print ("Bad tlv ID, can not covert to int")
                    tlvId = 0
                    pass
            elif "U32" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("U32 Data, missing")
                    u32Data = -1

                try:
                    u32Data = int(sp[1])
                except:
                    print ("Bad U32 Data, can not covert to int")
                    u32Data = -1
                    pass

            else:
                newArgs.append(arg)

        return tlvId, listTlvs, hexDump, shortDump, msgDump, u32Data, newArgs

    def send(self):

        audit_start_utc = int(time.time())
        tlvId, listTlvs, hexDump, shortDump, msgDump, u32Data, newArgs = self.parse_cmd_line_tlv_query(self.argV)

        if (listTlvs):
            self.print_supported_TLVs()
            return

        self.doHexDump = hexDump
        self.doShortDump = shortDump
        self.doMsgDump = msgDump
        self.u32Data = u32Data

        if len(newArgs) == 3:
            # The next option is a comma-separated list of IP addresses.
            ip_list = newArgs[2]
        else:
            print("Wrong number of user arguments ...")
            self.print_query_help()
            return

        # IP list is supplied by the user.
        ip_addr_list = ip_list.split(",")
        if len(ip_addr_list) != 1:
            print("There should be one IP Address")
            self.print_query_help()
            return

        ip_addr = ip_addr_list[0]

        if not gu.is_ip_looka_lika_unicast(ip_addr):
            # Does not look like an IP address. Drop or quit? Let's just quit.
            print("THIS IP %s DOES NOT LOOKA LIKA IP, SO SKIPPING" % (ip_addr))
            self.print_query_help()
            return

        # ts = "GPTS PSA: POWER STATUS AUDIT ON " + str(len(inverter_id_list)) + " ENTRIES FROM "
        ts = "TLV Query of addres=%s, tlv id=%u" % (ip_addr, tlvId)
        print(ts)

        self.send_unicast_query(ip_addr, tlvId, 1234)

        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        ts = "TLV Query: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        print(ts)
        return