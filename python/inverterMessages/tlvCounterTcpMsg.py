
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterTcpMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE,
                               "TCP Counters",
                               ipAddr)
        self.loadFullDataDef(dataStruct(1, "version",               0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "tcp_fill",              1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "HttpSocketWhacks2",     2, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "HttpSocketWhacks3",     4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "LinkupWhackSockets",    6, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UnexpectedEvent",       8, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "InvalidSocket",        10, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ExceedSocketCount",    12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PutTxFree",            14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ClinetSocketWhacks",   16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UdpCksumNotRequired",  18, 2, "!H"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))
        
    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())
        
        return msg
