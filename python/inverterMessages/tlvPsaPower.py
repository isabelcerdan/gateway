
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvPsaPower(TlvDataObject):
    __headerLabelStr = "|                   Power Data                                                                                               0x "
    __headerItemStr  = "|       mac           uptime      dcV      dcI      dcW    phase   acVrms      acI      wac   varPwr     freq    tempC  G  M IC "

    __nameStr = "PSA Power"

    __version          = dataStruct(1, " version",        0, 1, "!B")
    __macAddr          = dataStruct(1, " mac",            1, 6, "!BBBBBB")
    __fillByte         = dataStruct(1, " fillb",          7, 1, "!B")
    __connGrid_2       = dataStruct(2, " connGrid",       7, 1, "!B")
    __uptime           = dataStruct(1, " upTime",         8, 4, "!I")
    __actualDCV        = dataStruct(1, " actualDCV",     12, 2, "!H")
    __actualDCI        = dataStruct(1, " actualDCI",     14, 2, "!h")
    __actualDCW        = dataStruct(1, " actualDCW",     16, 2, "!H")
    __phaseAngleDeg    = dataStruct(1, " phaseAngleDeg", 18, 2, "!h")
    __fillLong         = dataStruct(1, " fillLong",      20, 4, "!BBBB")
    __fillBytes_2      = dataStruct(2, " fillBytes",     20, 3, "!BBB")
    __msmState_2       = dataStruct(2, " msmState",      23, 1, "!B")
    __vrmsVline        = dataStruct(1, " vrmsVLine",     24, 4, "!f")
    __accIline         = dataStruct(1, " accILine",      28, 4, "!f")
    __wacPline         = dataStruct(1, " wacPline",      32, 4, "!f")
    __varReactivePower = dataStruct(1, " VarReactPwr",   36, 4, "!f")
    __freqLineHz       = dataStruct(1, " freqLineHz",    40, 4, "!f")
    __tempC            = dataStruct(1, " tempC",         44, 4, "!f")
    __invCtrl_2        = dataStruct(2, " invCtrl",       48, 2, "!H")

    __elementSize = 48
    __elementSizeV2 = 50

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE,
                               self.__nameStr,
                               ipAddr)
        self.data = None
        self.tlv = None

        self.version = 0
        self.macAddr = bytearray(6)
        self.upTime = 0
        self.actualDCV = 0
        self.actualDCI = 0
        self.actualDCW = 0
        self.phaseAngleDeg = 0
        self.vrmsVLine = 0
        self.accILine = 0
        self.wacPline = 0
        self.varReactPwr = 0
        self.freqLineHz = 0
        self.tempC = 0

        # new for version 2
        self.connGrid = 0
        self.msmState = 0
        self.invCtrl = 0

    def unpackDataStruct(self, data):
        self.data = data
        self.tlv = TlvUnpacker(False, data)
        try:
           self.version = self.tlv.unpackFmtItem(self.__version)

        except Exception as e:
            raise TlvExceptionNoVersion(self.tlvId)

        if self.version > 2:
            raise TlvExceptionUnsupportedVersion(self.tlvId,
                                                 self.version)

        if self.version == 1:
            if len(data) != self.__elementSize:
                raise TlvExceptionWrongSize(self.tlvId,
                                            self.version,
                                            len(data),
                                            self.__elementSize)
        elif self.version == 2:
            if len(data) != self.__elementSizeV2:
                raise TlvExceptionWrongSize(self.tlvId,
                                            self.version,
                                            len(data),
                                            self.__elementSizeV2)

        self.macAddr     = self.tlv.unpackArray(self.__macAddr)
        self.upTime      = self.tlv.unpackFmtItem(self.__uptime)
        self.actualDCV      = self.tlv.unpackFmtItem(self.__actualDCV) / 1000.0
        self.actualDCI      = self.tlv.unpackFmtItem(self.__actualDCI) / 1000.0
        self.actualDCW      = self.tlv.unpackFmtItem(self.__actualDCW)
        self.phaseAngleDeg       = self.tlv.unpackFmtItem(self.__phaseAngleDeg)
        self.vrmsVLine      = self.tlv.unpackFmtItem(self.__vrmsVline)
        self.accILine       = self.tlv.unpackFmtItem(self.__accIline)
        self.wacPline       = self.tlv.unpackFmtItem(self.__wacPline)
        self.varReactPwr    = self.tlv.unpackFmtItem(self.__varReactivePower)
        self.freqLineHz        = self.tlv.unpackFmtItem(self.__freqLineHz)
        self.tempC        = self.tlv.unpackFmtItem(self.__tempC)

        if self.version == 2:
            self.connGrid = self.tlv.unpackFmtItem(self.__connGrid_2)
            self.msmState = self.tlv.unpackFmtItem(self.__msmState_2)
            self.invCtrl  = self.tlv.unpackFmtItem(self.__invCtrl_2)

    def persistData(self, rxTime):

        timeStr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(rxTime))

        update_string = "UPDATE %s " \
                        "SET %s = %s, %s = %s, %s = %s, %s = %s, " \
                        "    %s = IF(%s IS NULL or %s < '2011-00-00 00:00:00', %s, %s) " \
                        "WHERE %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                            INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                            INVERTERS_TABLE_WATTS_COLUMN_NAME, "%s",
                                            INVERTERS_TABLE_UPTIME_COLUMN_NAME, "%s",
                                            INVERTERS_TABLE_LAST_RESP_COLUMN_NAME, "%s",
                                            INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                            INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                            INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME, "%s",
                                            INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                            INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")

        update_args = (INVERTERS_TABLE_COMM_STRING_SG424_APPLICATION,
                       self.actualDCW,
                       self.upTime,
                       timeStr,
                       timeStr,
                       self.ipAddr)

        gu.prepared_act_on_database(EXECUTE, update_string, update_args)

        return True

    def dumpDataBuffer(self):
        retStr = self.__nameStr + "\n\r"
        retStr += self.dumpHexBuf(self.data)
        return retStr

    def dumpDecodedBuf(self):
        retStr = self.__nameStr + "\n\r"
        retStr += ("%s = %u\n\r" % (self.__version.name, self.version))
        retStr += ("%s = %02x:%02x:%02x:%02x:%02x:%02x\n\r" %
                (self.__macAddr.name,
                 self.macAddr[0],
                 self.macAddr[1],
                 self.macAddr[2],
                 self.macAddr[3],
                 self.macAddr[4],
                 self.macAddr[5]))
        retStr += ("%s = %d\n\r" % (self.__uptime.name, self.upTime))
        retStr += ("%s = %f\n\r" % (self.__actualDCV.name, self.actualDCV))
        retStr += ("%s = %f\n\r" % (self.__actualDCI.name, self.actualDCI))
        retStr += ("%s = %u\n\r" % (self.__actualDCW.name, self.actualDCW))
        retStr += ("%s = %u\n\r" % (self.__phaseAngleDeg.name, self.phaseAngleDeg))
        retStr += ("%s = %f\n\r" % (self.__vrmsVline.name, self.vrmsVLine))
        retStr += ("%s = %f\n\r" % (self.__accIline.name, self.accILine))
        retStr += ("%s = %f\n\r" % (self.__wacPline.name, self.wacPline))
        retStr += ("%s = %f\n\r" % (self.__varReactivePower.name, self.varReactPwr))
        retStr += ("%s = %f\n\r" % (self.__freqLineHz.name, self.freqLineHz))
        retStr += ("%s = %f\n\r" % (self.__tempC.name, self.tempC))

        if self.version == 2:
            retStr += ("%s = %u\n\r" % (self.__msmState_2.name, self.connGrid))
            retStr += ("%s = %u\n\r" % (self.__connGrid_2.name, self.msmState))
            retStr += ("%s = %02x\n\r" % (self.__invCtrl_2.name, self.invCtrl))

        return retStr

    def dumpShortDecodedBuf(self):
        retStr = ""
        #retStr += "{:>2d} ".format(self.version)
        retStr += "{:=02x}:".format(self.macAddr[0])
        retStr += "{:=02x}:".format(self.macAddr[1])
        retStr += "{:=02x}:".format(self.macAddr[2])
        retStr += "{:=02x}:".format(self.macAddr[3])
        retStr += "{:=02x}:".format(self.macAddr[4])
        retStr += "{:=02x} ".format(self.macAddr[5])

        retStr += "{:>8d} ".format(self.upTime)
        retStr += "{:>8.3f} ".format(self.actualDCV)
        retStr += "{:>8.3f} ".format(self.actualDCI)
        retStr += "{:>8d} ".format(self.actualDCW)
        retStr += "{:>8d} ".format(self.phaseAngleDeg)

        retStr += "{:>8.3f} ".format(self.vrmsVLine)
        retStr += "{:>8.3f} ".format(self.accILine)
        retStr += "{:>8.3f} ".format(self.wacPline)
        retStr += "{:>8.3f} ".format(self.varReactPwr)
        retStr += "{:>8.3f} ".format(self.freqLineHz)
        retStr += "{:>8.3f} ".format(self.tempC)

        if self.version == 2:
            retStr += "{:>2d} ".format(self.connGrid)
            retStr += "{:>2d} ".format(self.msmState)
            retStr += "{:=02x} ".format(self.invCtrl)
        else:
            retStr += "XX "
            retStr += "XX "
            retStr += "XX "
        return retStr

    @classmethod
    def dumpHeaderStrings(self):
        return self.__headerLabelStr, self.__headerItemStr

    def testMsg(self):
        #msg = bytearray(self.__psaPowerVer2ElementSize)
        msg = struct.pack("!hh", self.tlvId, self.__elementSizeV2)
        msg += struct.pack(self.__version.fmtStr, 2)
        msg += struct.pack(self.__macAddr.fmtStr, 2,3,4,5,6,7)
        msg += struct.pack(self.__connGrid_2.fmtStr, 1)
        msg += struct.pack(self.__uptime.fmtStr, 1)
        msg += struct.pack(self.__actualDCV.fmtStr, 48999)
        msg += struct.pack(self.__actualDCI.fmtStr, 1100)
        msg += struct.pack(self.__actualDCW.fmtStr, 400)
        msg += struct.pack(self.__phaseAngleDeg.fmtStr, 1)
        msg += struct.pack(self.__fillBytes_2.fmtStr, 1,2,3)
        msg += struct.pack(self.__msmState_2.fmtStr, 1)
        msg += struct.pack(self.__vrmsVline.fmtStr, 120.2)
        msg += struct.pack(self.__accIline.fmtStr, 1.02)
        msg += struct.pack(self.__wacPline.fmtStr, 122.0)
        msg += struct.pack(self.__varReactivePower.fmtStr, 1)
        msg += struct.pack(self.__freqLineHz.fmtStr, 59.904)
        msg += struct.pack(self.__tempC.fmtStr, 24.44)
        msg += struct.pack(self.__invCtrl_2.fmtStr, 1)

        return msg
