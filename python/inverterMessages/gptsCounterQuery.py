#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import socket
import struct
import string
import time
import datetime
import os
import traceback
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.GatewayConstants import *
import gateway_utils as gu
import tlv_utils

from tlvMsgPeek import *
from power_status.psa_util import *

from tlvCounterTlvMsg import *
from tlvCounterPacketsMsg import *
from tlvCounterMiscMsg import *
from tlvCounterEepromError import *
from tlvCounterErptsMsg import *
from tlvCounterLldpMsg import *
from tlvCounterMacMsg import *
from tlvCounterTcpMsg import *
from tlvCounterDebugs import *


SG424_TLV_COUNTERS_SELECTOR       = 0x00000001
SG424_PACKET_COUNTERS_SELECTOR    = 0x00000002
SG424_MISC_COUNTERS_SELECTOR      = 0x00000004
SG424_EEPROM_COUNTERS_SELECTOR    = 0x00000008
SG424_ERPT_COUNTERS_SELECTOR      = 0x00000010
SG424_LLDP_COUNTERS_SELECTOR      = 0x00000020
SG424_MAC_COUNTERS_SELECTOR       = 0x00000040
SG424_TCP_COUNTERS_SELECTOR       = 0x00000080
SG424_DEBUG_COUNTERS_SELECTOR     = 0x00000100



class GptsCounterQuery(object):
    def __init__(self, argV):
        self.argV = argV
        self.doHexDump = False
        self.doMsgDump = False
        self.delayTime = False
        self.dumpToTerminal = False
        self.dumpShortString = False
        self.counterMask = 0
        self.ipCount = 0
        self.deleteFile = False
        self.logFp = None


    @staticmethod
    def print_query_help():
        print("- send query for packed counters:       gpts.py counters  <terminal msg hex short delay=d ctr=xx ipAddr>")
        print("                                                          'hex', if set than hex dump is output")
        print("                                                          'msg', if set than dump whole message in hex")
        print("                                                          'short', if set than dump data as a short string")
        print("                                                          'delete', if set than delete existing log file")
        print("                                                          'terminal', required if any data dumped to terminal 'hex, msg, short'")
        print("                                                          'ipAddr' not required, comma seperated list of ip address")
        print("                                                                   if not specified then all inverters are queried")
        print("                                                                   list of IP address needs to be last command parameter")
        print("                                                          'ctr', REQUIRED, a comma seperated list of counters to retrieve from inverter")
        print("                                                                 all - all counters")
        print("                                                                 mac - MAC counters")
        print("                                                                 tlv - TLV msg counters")
        print("                                                                 pkt - packet related counters")
        print("                                                                 misc - Miscellaneous counters")
        print("                                                                 eeprom - EEPROM related counters")
        print("                                                                 erpt - Energy Report  counters")
        print("                                                                 tcp - TCP counters")
        print("                                                                 lldp - LLDP counters")
        print("                                                                 debug - inverter debug counters")
    #
    # parse_cmd_line_tlv_query
    #
    # parse the command line
    #
    def parse_cmd_line_tlv_query(self, argv):
        newArgs = []
        # peel out 'tlvId' command line args if they exist and
        # create a newArgs list
        for arg in argv:
            if arg.upper() == "HEX":
                self.doHexDump = True

            elif arg.upper() == "MSG":
                self.doMsgDump = True

            elif arg.upper() == "SHORT":
                self.dumpShortString = True

            elif arg.upper() == "TERMINAL":
                self.dumpToTerminal = True

            elif arg.upper() == "DELETE":
                self.deleteFile = True

            elif "CTR" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("Bad list of counters")
                else:
                    sl = sp[1].split(",")
                    if len(sl) == 0:
                        print("ZERO-LENGTH COUNTER LIST - EXITING")
                        return
                    else:
                        for ctr in sl:
                            if ctr.upper() == "ALL":
                                self.counterMask = 0xFFFF
                            elif ctr.upper() == "MAC":
                                self.counterMask |= SG424_MAC_COUNTERS_SELECTOR
                            elif ctr.upper() == "TLV":
                                self.counterMask |= SG424_TLV_COUNTERS_SELECTOR
                            elif ctr.upper() == "PKT":
                                self.counterMask |= SG424_PACKET_COUNTERS_SELECTOR
                            elif ctr.upper() == "MISC":
                                self.counterMask |= SG424_MISC_COUNTERS_SELECTOR
                            elif ctr.upper() == "EEPROM":
                                self.counterMask |= SG424_EEPROM_COUNTERS_SELECTOR
                            elif ctr.upper() == "ERPT":
                                self.counterMask |= SG424_ERPT_COUNTERS_SELECTOR
                            elif ctr.upper() == "LLDP":
                                self.counterMask |= SG424_LLDP_COUNTERS_SELECTOR
                            elif ctr.upper() == "TCP":
                                self.counterMask |= SG424_TCP_COUNTERS_SELECTOR
                            elif ctr.upper() == "DEBUG":
                                self.counterMask |= SG424_DEBUG_COUNTERS_SELECTOR


            elif "DELAY" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("Bad delay time value, missing")
                    self.delayTime = -1.0

                try:
                    self.delayTime = float(sp[1])
                except:
                    print ("Bad delay time value, can not covert to float")
                    self.delayTime = -1.0
                    pass

            else:
                newArgs.append(arg)

        return newArgs

    #
    # send_unicast_query
    #
    # send the unicast query to the inverter and wait for the response
    #
    def send_unicast_query(self, ip_addr, tlvMsgId, msgData):

        # Assumption: the IP has been validated to the extent that it looks like an IP.
        retObjList = {}
        SG424_data = None
        packed_data = gu.add_master_tlv(1)
        packed_data = gu.append_user_tlv_32bit_unsigned_long(packed_data, tlvMsgId, msgData)

        # OPEN THE SOCKET:
        try:
            fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        except Exception:  # as error:
            print("socket.socket failed ...")

        else:
            # SEND THE PACKET:
            try:
                fancy_socks.sendto(packed_data, (ip_addr, SG424_PRIVATE_UDP_PORT))

            except Exception:  # as error:
                print("sendto failed ...")

            # Now wait for a response:
            else:
                try:
                    fancy_socks.settimeout(3.0)
                    packet = fancy_socks.recvfrom(1024)

                except Exception:
                    # Timed out waiting to receive a packet. Simply means the inverter
                    # at that IP is not responding.
                    pass

                else:
                    # Packet received, data is in private_port_packet field.
                    (addr, port, SG424_data) = gu.get_udp_components(packet)

                    try:
                        if self.dumpToTerminal:
                            if self.doMsgDump:
                                print ("Dump of RX msg:")
                                print(TlvDataObject.dumpHexBuf(SG424_data))

                        tlvList = msgPeek(False, SG424_data)
                        #tlvList = msgPeek(True, SG424_data)

                        msgs = tlvList.getTlvDict()

                        for key in msgs:
                            try:
                                obj = TlvObjects(key, ip_addr)
                                tlvObj = obj.getObj()

                                if tlvObj is None:
                                    raise Exception("Unknown TLV type=%u" % key)

                                tlvObj.unpackDataStruct(msgs[key])

                                if tlvObj.getTlvId() == SG424_TO_GTW_MASTER_U16_TLV_TYPE:
                                    pass
                                else:
                                    retObjList[tlvObj.getTlvId()] = tlvObj

                                    if self.dumpToTerminal:
                                        print(tlvObj)

                                        print(tlvObj.dumpDecodedBuf())
                                        if True == self.doHexDump:
                                            print(tlvObj.dumpDataBuffer())

                                        if self.dumpShortString:
                                            print(tlvObj.dumpShortDecodedBuf())

                                    else:
                                        pass


                            except TlvExceptionWrongSize as e:
                                print(e.msg)
                                print(traceback.format_exc())
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionNoVersion as e:
                                print(e.msg)
                                print(traceback.format_exc())
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionUnsupportedVersion as e:
                                print(e.msg)
                                print(traceback.format_exc())
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))

                            except TlvExceptionNoMaster as e:
                                print(e.msg)
                                print(traceback.format_exc())

                            except  Exception as e:
                                print(("Can't parse TLV id=%u, len=%u " % (key, len(msgs[key])) + str(e)))
                                print(traceback.format_exc())
                                if len(msgs[key]) > 0:
                                    print(TlvDataObject.dumpHexBuf(msgs[key]))
                                print traceback.format_exc()


                    except Exception as e:
                        print ("Unable to parse tmp: " + str(e))
                        print(traceback.format_exc())

            # Close the socket:
            try:
                fancy_socks.close()

            except Exception:  # as error:
                print("close socket failed ...")

            else:
                pass

        return retObjList
    #
    # Log Header
    #
    # Get the header strings for each counter class for later logging/printing
    #
    def logHeader(self):
        headerStr = "year,time,ip_addr,sn,"

        tlvCntr = TlvCounterTlvMsg("0.0.0.0")
        pktCntr = TlvCounterPacketMsg("0.0.0.0")
        miscCntr = TlvCounterMiscMsg("0.0.0.0")
        eepromCntr = TlvCounterEepromErrMsg("0.0.0.0")
        erptsCntr = TlvCounterErptMsg("0.0.0.0")
        lldpCntr = TlvCounterLldpMsg("0.0.0.0")
        macCntr = TlvCounterMacMsg("0.0.0.0")
        tcpCntr = TlvCounterTcpMsg("0.0.0.0")
        debugCntr = TlvCounterDebugsMsg("0.0.0.0")

        if (self.counterMask & SG424_TLV_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = tlvCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_PACKET_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = pktCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_MISC_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = miscCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_EEPROM_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = eepromCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_ERPT_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = erptsCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_LLDP_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = lldpCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_MAC_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = macCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_TCP_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = tcpCntr.dumpHeaderStrings()
            headerStr += ctrStr

        if (self.counterMask & SG424_DEBUG_COUNTERS_SELECTOR) != 0:
            nameStr, ctrStr = debugCntr.dumpHeaderStrings()
            headerStr += ctrStr

        return(headerStr.rstrip(","))

    #
    # Log Data
    #
    # Get the short counter strings for each counter class for later logging/printing
    #
    def logData(self, ip_addr, sn, tlvList):
        csv = time.strftime("%Y-%m-%d,%H:%M:%S")
        csv += "," + str(ip_addr)
        csv += "," + str(sn) + ","

        if (len(tlvList) > 0):
            if (self.counterMask & SG424_TLV_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_PACKET_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_MISC_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_EEPROM_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_ERPT_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_LLDP_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_MAC_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_TCP_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

            if (self.counterMask & SG424_DEBUG_COUNTERS_SELECTOR) != 0:
                if SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE in tlvList:
                    csv += tlvList[SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE].dumpShortDecodedBuf()

        else:
            csv += "no_response"

        return (csv.rstrip(","))


    #
    # get_inverter_counters
    #
    # main function for getting counters from the inverters
    #
    def get_inverter_counters(self):

        audit_start_utc = int(time.time())
        newArgs = self.parse_cmd_line_tlv_query(self.argV)

        if len(newArgs) == 3:
            # The next option is a comma-separated list of IP addresses.
            ip_list = newArgs[2]

            # IP list is supplied by the user.
            separated_ip_list = ip_list.split(",")
            if len(separated_ip_list) == 0:
                print("ZERO-LENGTH IP LIST")
                return

            # First check that each IP in the list is seemingly valid.
            for this_ip in separated_ip_list:
                if not gu.is_ip_looka_lika_unicast(this_ip):
                    # Does not look like an IP address. Drop or quit? Let's just quit.
                    print("**** %s is not an IP, skipping" % (this_ip))
                    separated_ip_list.remove(this_ip)

            count, inverter_id_list = PsaUtil.create_inverters_id_list_from_list(separated_ip_list)

        elif len(newArgs) < 3:
            count, inverter_id_list = PsaUtil.create_inverters_id_list()

        else:
            print("Too many user arguments ...")
            self.print_query_help()
            return

        if self.delayTime < 0.0:
            print ("Bad delay time value")
            self.print_query_help()
            return

        if self.counterMask == 0:
            print ("No Counters selected")
            self.print_query_help()
            return

        self.ipCount = count

        if self.ipCount == 0:
            print ("No IP address to query")
            self.print_query_help()
            return

        if self.dumpToTerminal and self.ipCount > 1:
            print ("Can only output to terminal if 1 IP address to query")
            self.print_query_help()
            return

        if self.deleteFile == True:
            terminal_line_command = "sudo rm " + COUNTER_GPTS_RESULTS_PATH_AND_FILE_NAME
            donut_care_return = os.system(terminal_line_command)
            print("Counters: file %s deleted before start" % (COUNTER_GPTS_RESULTS_PATH_AND_FILE_NAME))

            # First things first: open the text file into which the results of the audit will be written.
        try:
            self.logFp = open(COUNTER_GPTS_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception:  # as err_string:
            print("Counters: FAILED TO OPEN %s FILE" % (COUNTER_GPTS_RESULTS_PATH_AND_FILE_NAME))
            return

        print("Checking %u inverters" % self.ipCount)

        if self.dumpToTerminal == False:
            print("Data will be written to the log file %s" % COUNTER_GPTS_RESULTS_PATH_AND_FILE_NAME)

        header = self.logHeader()
        if self.dumpToTerminal and self.dumpShortString:
            print(header)
        else:
            self.logFp.write(header + LINE_TERMINATION)
            self.logFp.flush()

        count = 0
        for inverters_list_index in range(0, len(inverter_id_list)):

            inverter_id = inverter_id_list[inverters_list_index]

            (ip_addr, mac, sn, string_id, string_position) = \
                gu.base_inverter_info_from_inverters_id(inverter_id)

            # Get the counters and data of interest:
            if ip_addr:
                tlvList = self.send_unicast_query(ip_addr,
                                                    GTW_TO_SG424_PACKED_COUNTERS_QUERY_U32_TLV_TYPE,
                                                    self.counterMask)

                counters = self.logData(ip_addr, sn, tlvList)
                if self.dumpToTerminal == False:
                    self.logFp.write(counters + LINE_TERMINATION)
                    self.logFp.flush()

                # ----------------------------------------
            else:
                #self.logger.info("PASSING OVER NULL IP FROM INVERTERS ID " + str(inverter_id))
                print("PASSING OVER NULL IP FROM INVERTERS ID " + str(inverter_id))

            if self.dumpToTerminal == False:
                count += 1

                if self.delayTime > 0:
                    next_start = time.time() + self.delayTime
                    while True:
                        if time.time() >= next_start:
                            break
                        time.sleep(0.100)

                        #check for interruptions here

                complete = (count * 1.0) / self.ipCount
                for x in range(1,9):
                    limit = 1.0 - (x * 0.1)
                    if (complete > limit):
                        print ("complete is >= %f percent" % (limit * 100))
                        break


        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        ts = "Counter Query: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        if self.dumpToTerminal:
            print(ts)
        else:
            self.logFp.write(ts + LINE_TERMINATION)
            self.logFp.flush()

        return