
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *

class TlvAifFrequencyRtsElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_FRT_STRUCTURED_TLV_TYPE,
                               "AIF FRT Data",
                               ipAddr)


        self.loadFullDataDef(dataStruct(1, "version",                   0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Rule21OrRule14H_Defaults",  1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "ofr1TripTimeMsec",          2, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ofr1MagnitudeHz",           6, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ofr2TripTimeMsec",          10, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ofr2MagnitudeHz",           14, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ufr1TripTimeMsec",          18, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ufr1MagnitudeHz",           22, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ufr2TripTimeMsec",          26, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ufr2MagnitudeHz",           30, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ofrRtsMagnitudeHz",         34, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ufrRtsMagnitudeHz",         38, 4, "!f"))


    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
