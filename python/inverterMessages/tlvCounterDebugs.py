
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterDebugsMsg(TlvDataObject):


    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE,
                               "Debug Counters",
                               ipAddr)
        self.tlv = None
        self.version = 0

        self.loadFullDataDef(dataStruct(1, "Dbg1",   0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg2",   1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg3",   2, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg4",   3, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg5",   4, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg6",   5, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg7",   6, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg8",   7, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg9",   8, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg10",  9, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg11", 10, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg12", 11, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg13", 12, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg14", 13, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg15", 14, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg16", 15, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg17", 16, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg18", 17, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg19", 18, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg20", 19, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg21", 20, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg22", 21, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg23", 22, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Dbg24", 23, 1, "B"))

        # The data in this message has no version, force to 1
        self.version = 1


    def unpackDataStruct(self, data):
        self.loadData(data)
        self.tlv = TlvUnpacker(False, data)

        # load data defination used for the version of data from the inverter
        # this data structure has not version
        self.loadVersionDataDef(1)

        if len(data) != self.getDataSize():
            raise TlvExceptionWrongSize(self.tlvId,
                                        self.version,
                                        len(data),
                                        self.getDataSize())

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
