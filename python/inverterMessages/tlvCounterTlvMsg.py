
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterTlvMsg(TlvDataObject):
    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE,
                               "TLV Msg Counters",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",                          0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "tlv_fill",                         1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Curtail",                          2, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "Regulate",                         6, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "SeqNum",                          10, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "DcKaGone",                        14, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "DcKaBack",                        18, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "KaLate",                          22, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "SeqRepeat",                       26, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "SeqOOS",                          30, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "RampRateUp",                      34, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "RampRateDown",                    38, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ErrConfigSolarInverterPcsUnitId", 42, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrEncapCmdToSolarInverter",      44, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrEncapCmdStartEndMismatch",     46, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrInconsistSeqNumCurtReg",       48, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PacketTooBig",                    50, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "LengthMismatch",                  52, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrTooManyUserTlvs",              54, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrLengthGoesPastPayloadSize",    56, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrPointerBeyondLimit",           58, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrBadTlv",                       60, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrInvalidConfigData",            62, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ErrModuleCodingError",            64, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "TlvBufferTooSmall",               66, 2, "!H"))

        self.useHighestVersion = True

        self.loadFullDataDef(dataStruct(2, "version",                          0, 1, "B"))
        self.loadFullDataDef(dataStruct(2, "tlv_fill",                         1, 1, "B"))
        self.loadFullDataDef(dataStruct(2, "Curtail",                          2, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "Regulate",                         6, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "SeqNum",                          10, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "DcKaGone",                        14, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "DcKaBack",                        18, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "KaLate",                          22, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "SeqRepeat",                       26, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "SeqOOS",                          30, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "RampRateUp",                      34, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "RampRateDown",                    38, 4, "!I"))
        self.loadFullDataDef(dataStruct(2, "ErrConfigSolarInverterPcsUnitId", 42, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrEncapCmdToSolarInverter",      44, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrEncapCmdStartEndMismatch",     46, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrInconsistSeqNumCurtReg",       48, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "PacketTooBig",                    50, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "LengthMismatch",                  52, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrTooManyUserTlvs",               0, 0, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrLengthGoesPastPayloadSize",    54, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrPointerBeyondLimit",           56, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrBadTlv",                       58, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrInvalidConfigData",            60, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "ErrModuleCodingError",            62, 2, "!H"))
        self.loadFullDataDef(dataStruct(2, "TlvBufferTooSmall",               64, 2, "!H"))

    def unpackDataStruct(self, data):
        self.unpackCommonSpecificVersion(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True


    def testMsg(self):
        msg = bytearray(self.getDataSize())
        
        return msg
