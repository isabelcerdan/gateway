
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvAifCurveElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_CURVE_STRUCTURED_TLV_TYPE,
                               "AIF Curve Data",
                               ipAddr)

        
        self.loadFullDataDef(dataStruct(1, "version",                    0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltWattEnable",             1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltWattCurveName",          2, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "spare",                      3, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltWattCurveId",            4, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "voltWattResponseTimeMsec",   6, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "voltVarEnable",             10, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltVarCurveName",          11, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "voltVarCurveId",            12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "voltVarResponseTimeMsec",   14, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "freqWattEnable",            18, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "freqWattCurveName",         19, 1, "c"))
        self.loadFullDataDef(dataStruct(1, "freqWattCurveId",           20, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "freqWattResponseTimeMsec",  22, 4, "!I"))


    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
