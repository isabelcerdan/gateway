import struct
import os
import time
import traceback
import MySQLdb
import MySQLdb.cursors
from lib.db import prepared_act_on_database, FETCH_ONE, FETCH_ALL, EXECUTE

import gateway_utils as gu
import traceback
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *
from tlvDataObject import *

alarmBits = namedtuple("alarmBits", "bitIndex alarmName active swVer msgVer")

class TlvAlarmMsg(TlvDataObject):

    _alarmDef = []

    _alarmDef.append(alarmBits(0, "Reserved0", True, 311, 1))
    _alarmDef.append(alarmBits(1, "Reserved1_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(2, "PsolarLow", True, 311, 1))
    _alarmDef.append(alarmBits(3, "HighTemp", True, 311, 1))
    _alarmDef.append(alarmBits(4, "LowTemp", True, 311, 1))
    _alarmDef.append(alarmBits(5, "FlineHighRT", True, 311, 1))
    _alarmDef.append(alarmBits(6, "FlineLowRT", True, 311, 1))
    _alarmDef.append(alarmBits(7, "IsolarHigh", True, 311, 1))
    _alarmDef.append(alarmBits(8, "IsolarLow_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(9, "IoutHigh_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(10, "LogicFault", True, 311, 1))
    _alarmDef.append(alarmBits(11, "LossOfPhaseLock", True, 311, 1))
    _alarmDef.append(alarmBits(12, "LossOfFline_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(13, "LossOfHwZc_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(14, "LossOfSwZc_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(15, "MsmWdt", False, 311, 1))
    
    _alarmDef.append(alarmBits(16, "DC_OV_ProtectonFault", True, 311, 1))
    _alarmDef.append(alarmBits(17, "OperatorMajor_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(18, "OperatorReset", True, 311, 1))
    _alarmDef.append(alarmBits(19, "PsolarHigh", True, 311, 1))
    _alarmDef.append(alarmBits(20, "VsolarHigh", True, 311, 1))
    _alarmDef.append(alarmBits(21, "VsolarLow_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(22, "VlineHighTier2", True, 311, 1))
    _alarmDef.append(alarmBits(23, "VlineLowTier2", True, 311, 1))
    _alarmDef.append(alarmBits(24, "VoutHigh_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(25, "FreqLow", True, 311, 1))
    _alarmDef.append(alarmBits(26, "FreqHigh", True, 311, 1))
    _alarmDef.append(alarmBits(27, "PsolarHighWarn_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(28, "EthernetFailure", True, 311, 1))
    _alarmDef.append(alarmBits(29, "BkgWdt", True, 311, 1))
    _alarmDef.append(alarmBits(30, "IslandDetected", True, 311, 1))
    _alarmDef.append(alarmBits(31, "FltbFault_DEP", False, 311, 1))

    _alarmDef.append(alarmBits(32, "PeriodicReset_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(33, "OC1_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(34, "AC_OC_ProtectionFault", True, 311, 1))
    _alarmDef.append(alarmBits(35, "Foldback_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(36, "VlineHighWarn", True, 311, 1))
    _alarmDef.append(alarmBits(37, "VlineLowWarn", True, 311, 1))
    _alarmDef.append(alarmBits(38, "OcFaultRelayOpen_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(39, "FltbFaultRelayOpen_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(40, "VoutLowWarn_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(41, "Phy0ErrWarn", True, 311, 1))
    _alarmDef.append(alarmBits(42, "Phy1ErrWarn", True, 311, 1))
    _alarmDef.append(alarmBits(43, "IsolarHighWarn", True, 311, 1))
    _alarmDef.append(alarmBits(44, "BadProfile", True, 311, 1))
    _alarmDef.append(alarmBits(45, "VlineHighTier1", True, 311, 1))
    _alarmDef.append(alarmBits(46, "VlineLowTier1", True, 311, 1))
    _alarmDef.append(alarmBits(47, "FailedPhaseLock", True, 311, 1))

    _alarmDef.append(alarmBits(48, "TempFoldbackWarn_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(49, "VlineHighTier3", True, 311, 1))
    _alarmDef.append(alarmBits(50, "VlineLowTier3", True, 311, 1))
    _alarmDef.append(alarmBits(51, "FrequencyFaultRts", True, 311, 1))
    _alarmDef.append(alarmBits(52, "DcDutyCycle_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(53, "Trov2", True, 311, 1))
    _alarmDef.append(alarmBits(54, "HttpCloseSocket2", True, 311, 1))
    _alarmDef.append(alarmBits(55, "HttpCloseSocket3", True, 311, 1))
    _alarmDef.append(alarmBits(56, "FailedIgnition", True, 311, 1))
    _alarmDef.append(alarmBits(57, "PTPER_OOR", True, 311, 1))
    _alarmDef.append(alarmBits(58, "PsolarWarmStart_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(59, "VsolarWarmStart_DEP", False, 311, 1))
    _alarmDef.append(alarmBits(60, "HttpFutureErResponse", True, 313, 1))
    _alarmDef.append(alarmBits(61, "TEMP_FAIL", True, 313, 1))
    _alarmDef.append(alarmBits(62, "LOCKED_Reminder", True, 313, 1))
    _alarmDef.append(alarmBits(63, "DISABLED_Reminder", True, 313, 1))

    _alarmDef.append(alarmBits(64, "EnterServiceReminder", True, 313, 1))
    _alarmDef.append(alarmBits(65, "LastResetWdt", True, 313, 1))
    _alarmDef.append(alarmBits(66, "UpgradeReset", True, 313, 1))
    _alarmDef.append(alarmBits(67, "UnknownHttpRequest", True, 313, 1))
    _alarmDef.append(alarmBits(68, "BOB_FAIL", True, 313, 1))
    _alarmDef.append(alarmBits(69, "COP_Disaster", True, 313, 1))
    _alarmDef.append(alarmBits(70, "VoltageFaultRts", True, 313, 1))
    _alarmDef.append(alarmBits(71, "COP_ProtectionStorm", True, 313, 1))
    _alarmDef.append(alarmBits(72, "TcpClientSocketWhack", True, 313, 1))
    _alarmDef.append(alarmBits(73, "AifDbGetErrors", True, 313, 1))
    _alarmDef.append(alarmBits(74, "GenericDbGetErrors", True, 313, 1))
    _alarmDef.append(alarmBits(75, "InverterGoneDownstream", True, 313, 1))
    _alarmDef.append(alarmBits(76, "InverterGoneUpstream", True, 313, 1))
    _alarmDef.append(alarmBits(77, "GtwyInvFdDtctTimeout", True, 313, 1))
    _alarmDef.append(alarmBits(78, "DISABLED", True, 313, 1))

    for i in range(79, 127):
        _alarmDef.append(alarmBits(i, "Unused_" + str(i), False, 0, 1))

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_ALARM_REPORT_STRUCTURED_TLV_TYPE,
                               "Alarm Msg",
                               ipAddr)

        self.alarmCurVal = {}
        self.alarmPrevVal = {}
        self.changeList = {}

        self.loadFullDataDef(dataStruct(1, "version",            0,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "fill",               1,  1, "B"))
        self.loadFullDataDef(dataStruct(1, "serial",             2, 14, "12s"))
        self.loadFullDataDef(dataStruct(1, "product",           16, 22, "20s"))
        self.loadFullDataDef(dataStruct(1, "swRev",             38, 34, "32s"))
        self.loadFullDataDef(dataStruct(1, "schema",            72,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "type",              74,  1,  "B"))
        self.loadFullDataDef(dataStruct(1, "fillB",             75,  1,  "B"))
        self.loadFullDataDef(dataStruct(1, "flags",             76,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "utcTimestampStart", 78,  4, "!L"))
        self.loadFullDataDef(dataStruct(1, "faults",            82,  16, "!8H"))
        self.loadFullDataDef(dataStruct(1, "prevFaults",        98,  16, "!8H"))

    #
    # unpack the input data and collect the fault data
    #
    def unpackDataStruct(self, data):
        self.unpackCommon(data)
        
        try:
            for i in self.getDataDefArray():
                if (i.name == 'serial' or
                    i.name == 'product' or
                    i.name == 'swRev'):
                    self.loadDataVal(i.name, self.tlv.unpackString(i))

                elif (i.name == 'faults' or
                      i.name == 'prevFaults'):
                    self.loadDataVal(i.name, self.tlv.unpackArray(i))

                else:
                    self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

        except Exception as e:
            print "Unable to process Data"
            raise TlvExceptionCannotParseData(self.tlvId, self.version)

        try:
            self.loadFaults()
        except Exception as e:
            print "Unable to load Data"
            raise TlvExceptionCannotLoadData(self.tlvId)

        try:
            self.checkFaults()
        except Exception as e:
            print "Unable to check Data"
            raise TlvExceptionCannotLoadData(self.tlvId)
    #
    # persist the data to the database
    #
    def persistData(self, rxTime):
        faults = self.getDataValByName("faults")
        prevFaults = self.getDataValByName("prevFaults")

        changeStr = ""
        changeCnt = len(self.changeList)
        idx = 0
        for k, v in self.changeList.items():
            idx += 1
            changeStr += v['alarmName'] + ": " + v['status']
            if idx < changeCnt:
                changeStr += ", "

        execute_string = "INSERT INTO %s " \
                         "VALUES (NULL, %s,%s,%s,%s,%s," \
                         "        %s,%s,%s,%s,%s,%s,%s,%s," \
                         "        %s,%s,%s,%s,%s,%s,%s,%s," \
                         "        0, 0, %s);" % \
                         (DB_ALARM_REPORTS_TABLE,
                          "%s", "%s", "%s", "%s", "%s",
                          "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s",
                          "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s",
                          "%s")

        timeStr = time.strftime("%Y-%m-%d %H:%M:%S ", time.localtime(rxTime))
        some_args = (timeStr,
                     self.getDataValByName("serial"),
                     self.getDataValByName("utcTimestampStart"),
                     self.getDataValByName("schema"),
                     self.getDataValByName("flags"),
                     faults[0], faults[1],
                     faults[2], faults[3],
                     faults[4], faults[5],
                     faults[6], faults[7],
                     prevFaults[0], prevFaults[1],
                     prevFaults[2], prevFaults[3],
                     prevFaults[4], prevFaults[5],
                     prevFaults[6], prevFaults[7],
                     changeStr)

        prepared_act_on_database(EXECUTE, execute_string, some_args)


    #
    # build the acknowledgment TLV
    #
    def buildAcknowledgeData(self, rxTime, ok):

        ackTlv = struct.pack('!H', GTW_TO_SG424_REPORT_ACK_TLV_TYPE)
        ackTlv += struct.pack('!H', 6)

        flag = ERPT_ACK_MSG_RECEIVED_FLAG
        if (ok == False):
            flag = ERPT_ACK_MSG_REJECTED_FLAG

        ackTlv += struct.pack('B', flag)
        ackTlv += struct.pack('B', self.getDataValByName("type"))
        ackTlv += struct.pack('!L', self.getDataValByName("utcTimestampStart"))

        return ackTlv


    #
    # Build two arrays of the states of faults and prevFaults
    # used later in checkFaults
    #
    def loadFaults(self):
        faults = self.getDataValByName("faults")
        prevFaults = self.getDataValByName("prevFaults")

        bitIdx = 0
        for f in faults:
            for i in range(0,16):
                if ((1 << i) & f) != 0:
                    self.alarmCurVal[bitIdx] = 1
                else:
                    self.alarmCurVal[bitIdx] = 0
                bitIdx += 1

        bitIdx = 0
        for f in prevFaults:
            for i in range(0, 16):
                if ((1 << i) & f) != 0:
                    self.alarmPrevVal[bitIdx] = 1
                else:
                    self.alarmPrevVal[bitIdx] = 0
                bitIdx += 1

    #
    # Builds a change list of the faults
    #
    def checkFaults(self):
        for alarmIdx in range(0, len(self.alarmCurVal)):

            if (self.alarmCurVal[alarmIdx] != self.alarmPrevVal[alarmIdx]):
                changes = {}
                changes['alarmName'] = self._alarmDef[alarmIdx].alarmName
                changes['cur'] = self.alarmCurVal[alarmIdx]
                changes['prev'] = self.alarmPrevVal[alarmIdx]

                if (self.alarmCurVal[alarmIdx] == 1):
                    changes['status'] = "set"
                else:
                    changes['status'] = "clear"

                self.changeList[alarmIdx] = changes

            elif self.alarmCurVal[alarmIdx] == 1:
                changes = {}
                changes['alarmName'] = self._alarmDef[alarmIdx].alarmName
                changes['status'] = "still_set"
                changes['cur'] = self.alarmCurVal[alarmIdx]
                changes['prev'] = self.alarmPrevVal[alarmIdx]

                self.changeList[alarmIdx] = changes

    #
    # get data for logging
    #
    def logStr(self):
        flags = self.getDataValByName("flags")

        s = ", sn="
        s += str(self.getDataValByName("serial"))
        s += ", utc="
        s += str(self.getDataValByName("utcTimestampStart"))
        s += ", sch="
        s += str(self.getDataValByName("schema"))

        s += ", flag="
        s += '0x{:04X}'.format(flags)
        s += " ("
        if (flags & ERPT_FLAG_LOCKED):
            s += 'locked, '
        else:
            s += 'unlocked, '

        if (flags & EPRT_FLAG_DISABLED):
            s += 'disabled'
        else:
            s += 'not disabled'

        s += ")"

        return s

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg

    


