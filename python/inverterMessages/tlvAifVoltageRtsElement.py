
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvAifVoltageRtsElement(TlvDataObject):
    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_VRT_STRUCTURED_TLV_TYPE,
                               "AIF VRT Data",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",                   0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "Rule21OrRule14H_Defaults",  1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "ov1MomentaryCessation",     2, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "spare",                     3, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "ov1TripTimeMsec",           4, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ov1MagnitudePct",           8, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ov2TripTimeMsec",          12, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "ov2MagnitudePct",          16, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "uv1TripTimeMsec",          20, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "uv1MagnitudePct",          24, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "uv2TripTimeMsec",          28, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "uv2MagnitudePct",          32, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "uv3TripTimeMsec",          36, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "uv3MagnitudePct",          40, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "ovrRtsMagnitudePct",       44, 4, "!f"))
        self.loadFullDataDef(dataStruct(1, "uvrRtsMagnitudePct",       48, 4, "!f"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
