
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *



class TlvCounterMacMsg(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE,
                               "MAC Counters",
                               ipAddr)
        self.loadFullDataDef(dataStruct(1, "version",                      0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "mac_fill",                     1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "LostUpstream",                 2, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "LostDownstream",               6, 4, "!I"))
        self.loadFullDataDef(dataStruct(1, "NextPktError",                10, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "BitsZeroError",               12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "CrcError",                    14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "ByteCountError",              16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "RxNokError",                  18, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UnkEthFrameType",             20, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "FrameWithLength",             22, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "OutOfRangeNextPktPtr",        24, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DummyForceWrapDiscard",       26, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PktDiscardBeforeGetHdr",      28, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "IpProtUdpProcessFailed",      30, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UdpCksumMismatchDiscards",    32, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "UdpNonMatchSocketDiscards",   34, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "PktDiscardByGetHeader",       36, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "DriverBadReadPointerMacInit", 38, 2, "!H"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

        
    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())
        
        return msg
