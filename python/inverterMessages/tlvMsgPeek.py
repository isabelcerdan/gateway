#
#
"""
tlvMsgPeek.py

Contains two classes
ParseTlv - Finds and validates a TLV in the data stream
MsgPeek -  Inspects the input message and builds a dictionary of the the TLVs

"""
import struct
from lib.gateway_constants.InverterConstants import *
from tlvObjectList import *

class ParseTlv(object):
    def __init__(self, debug, tlv_msg):
        self.debug = debug
        self.tlvCount = 0
        self.isGtwMasterTlv = False
        self.isInverterMasterTlv = False
        self.tlv_msg = tlv_msg
        self.tlv_type = 0
        self.tlv_length = 0
        self.tlv_value = []
        self.msg_size = 0

        self.deconstruct()

    def isGtwTlv(self):
        return False

    def isGtwMasterTlv(self):
        return self.isGtwMasterTlv

    def isInverterMasterTlv(self):
        return self.isInverterMasterTlv

    def getTlvCount(self):
        return self.tlvCount

    def getType(self):
        return self.tlv_type

    def getLength(self):
        return self.tlv_length

    def getValue(self):
        return self.tlv_value

    def getMsgSize(self):
        return self.msg_size

    def getTlvSize(self):
        if self.tlv_length > 0:
            return self.tlv_length + 4
        else:
            return 0

    def deconstruct(self):
        if self.debug == True:
            print("tlvMsg length=%u" % len(self.tlv_msg))
            d = self.tlv_msg[0:5]
            print(TlvDataObject.dumpHexBuf(d))

        if len(self.tlv_msg) >= 6:
            (_tlv_type,_length) = struct.unpack_from("!HH", self.tlv_msg, 0)

            if _tlv_type < SG424_TO_GTW_MIN_ID or _tlv_type > SG424_TO_GTW_MAX_ID:
                raise Exception('The TLV type (%u) out of range' % _tlv_type)

            if _tlv_type == SG424_TO_GTW_MASTER_U16_TLV_TYPE:
                self.isInverterMasterTlv = True

            if self.debug:
                print("tlvMsg type=%u" % _tlv_type)
                print("tlvMsg length=%u" % _length)

            if _length <= (len(self.tlv_msg) - 4):
                self.tlv_value = self.tlv_msg[4:(4 + _length)]
                self.msg_size =_length + 4
                self.tlv_type = _tlv_type
                if (_length % 2) != 0:
                    _length += 1

                    if self.debug:
                        print("updating tlvMsg length=%u" % _length)

                self.tlv_length =_length


                if self.isInverterMasterTlv:
                    if len(self.tlv_value) == 2:
                        (count,) = struct.unpack_from("!H", self.tlv_value, 0)
                        self.tlvCount = count
                    else:
                        raise Exception('The TLV Master length (%u) should be 2 bytes' % len(self.tlv_value))
            else:
                raise Exception('The TLV Msg value does not match the length')
        else:
            raise Exception('The TLV Msg is not long enough to parse')

        return


class msgPeek(ParseTlv):
    def __init__(self, debug, tlv_msg):
        ParseTlv.__init__(self, debug, tlv_msg)
        self.tlvs = []
        self.tlvDict = {}
        self.count = 0

        if ParseTlv.isInverterMasterTlv(self):
            self.count = ParseTlv.getTlvCount(self)

            if debug:
                print("Master found, TLV count = %u" % self.count)

            tlvMetaData = [ParseTlv.getType(self),
                           ParseTlv.getLength(self),
                           ParseTlv.getValue(self)]
            self.tlvs.append(tlvMetaData)
            self.tlvDict[ParseTlv.getType(self)] = ParseTlv.getValue(self)
            if debug:
                print("getType = %u, getLength= %u" % (ParseTlv.getType(self),ParseTlv.getLength(self)))


            nextTlvOffset = ParseTlv.getTlvSize(self)

            if debug:
                print("nextTlvOffset = %u" % nextTlvOffset)

            for i in range(0, self.count):
                nextTlv = tlv_msg[nextTlvOffset: len(tlv_msg)]
                nextMeta = ParseTlv(debug, nextTlv)

                tlvMetaData = [nextMeta.getType(),
                               nextMeta.getLength(),
                               nextMeta.getValue()]
                self.tlvs.append(tlvMetaData)
                self.tlvDict[nextMeta.getType()] = nextMeta.getValue()
                nextTlvOffset += nextMeta.getTlvSize()

        else:
            raise Exception('Master TLV not found')


        ##
        ## get a string buffer containing the meta data
        ## for the returned TLVs

    def __str__(self):
        s = ""
        for meta in self.tlvs:
            s += ("type=%d, length=%d, tlvSize=%d\n" %
                  (meta[0],
                   meta[1],
                   len(meta[2])))

        return (s)

        ##
        ## get a list of the TLVs and in format (type, length, value)

    def getTlvs(self):
        return self.tlvs

        ##
        ## get a dictionary of the TLV data in where
        ##   'key' is TLV Id
        ##   'value' is the TLV data

    def getTlvDict(self):
        return self.tlvDict

