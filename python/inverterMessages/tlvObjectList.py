#
#
"""
tlvObjectList.py

Contains one class
TlvObject - Returns an unloaded TLV object Type.

"""

from lib.gateway_constants.InverterConstants import *
from tlvMasterSG424 import *
from tlvPsaPower import *
from tlvPsaStatusCurrent import *
from tlvPsaStatusLatched import *
from tlvXetInfoElement import *
from tlvAifVoltageRtsElement import *
from tlvAifFrequencyRtsElement import *
from tlvAifVoltWattElement import *
from tlvAifVoltVarElement import *
from tlvAifFreqWattElement import *
from tlvAifCurveElement import *
from tlvAifStatusElement import *
from tlvCounterTlvMsg import *
from tlvCounterPacketsMsg import *
from tlvCounterMiscMsg import *
from tlvCounterEepromError import *
from tlvCounterErptsMsg import *
from tlvCounterLldpMsg import *
from tlvCounterMacMsg import *
from tlvCounterTcpMsg import *
from tlvEnergyMsg import *
from tlvAlarmMsg import *
from tlvCounterDebugs import *

class TlvObjects(object):
    def __init__(self, tlvType, ipAddr):
        self.tlvType = tlvType
        self.ipAddr = ipAddr
        #self.tlvData = tlvData

    def getObj(self):
        if SG424_TO_GTW_MASTER_U16_TLV_TYPE == self.tlvType:
            return(TlvMasterSG424(self.ipAddr))
        elif SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvPsaPower(self.ipAddr))
        elif SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvPsaStatusLatched(self.ipAddr))
        elif SG424_TO_GTW_CURRENT_STATUS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvPsaStatusCurrent(self.ipAddr))
        elif SG424_TO_GTW_XET_INFO_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvXetInfoElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_VRT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifVoltageRtsElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_FRT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifFrequencyRtsElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_VOLT_WATT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifVoltWattElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_VOLT_VAR_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifVoltVarElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_FREQ_WATT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifFreqWattElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_CURVE_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifCurveElement(self.ipAddr))
        elif SG424_TO_GTW_XET_AIF_STATUS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAifStatusElement(self.ipAddr))
        elif SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterTlvMsg(self.ipAddr))
        elif SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterPacketMsg(self.ipAddr))
        elif SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterMiscMsg(self.ipAddr))
        elif SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterEepromErrMsg(self.ipAddr))
        elif SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterErptMsg(self.ipAddr))
        elif SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterLldpMsg(self.ipAddr))
        elif SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterMacMsg(self.ipAddr))
        elif SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterTcpMsg(self.ipAddr))
        elif SG424_TO_GTW_ENERGY_REPORT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvEnergyMsg(self.ipAddr))
        elif SG424_TO_GTW_ALARM_REPORT_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvAlarmMsg(self.ipAddr))
        elif SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE == self.tlvType:
            return (TlvCounterDebugsMsg(self.ipAddr))

        else:
            return None


