
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvXetInfoElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_INFO_STRUCTURED_TLV_TYPE,
                               "XET Info",
                               ipAddr)

        self.macAddr = bytearray(6)

        self.loadFullDataDef(dataStruct(1, "version",           0,   1, "B"))
        self.loadFullDataDef(dataStruct(1, "spare",             1,   1, "B"))
        self.loadFullDataDef(dataStruct(1, "hwModel",           2,   2, "!H"))
        self.loadFullDataDef(dataStruct(1, "swBuildType",       4,   2, "!H"))
        self.loadFullDataDef(dataStruct(1, "swVersion",         6,  32, "32s"))
        self.loadFullDataDef(dataStruct(1, "productNum",        38, 20, "20s"))
        self.loadFullDataDef(dataStruct(1, "mgiType",           58,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "bootloaderVersion", 60,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "swProfile",         62,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "hwVersion",         64,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "hwRevision",        66,  2, "!H"))
        self.loadFullDataDef(dataStruct(1, "serialNumber",      68, 12, "12s"))
        self.loadFullDataDef(dataStruct(1, "macAddress",        80,  6, "BBBBBB"))

    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            if (i.name == "macAddress"):
                self.macAddr = self.tlv.unpackArray(i)
                macStr = ("%02x:%02x:%02x:%02x:%02x:%02x" %
                          (self.macAddr[0],
                           self.macAddr[1],
                           self.macAddr[2],
                           self.macAddr[3],
                           self.macAddr[4],
                           self.macAddr[5]))

                self.loadDataVal(i.name, macStr)
            elif (i.name == "serialNumber" or
                  i.name == "swVersion" or
                  i.name == "productNum"):
                self.loadDataVal(i.name, self.tlv.unpackString(i))
            else:
                self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
