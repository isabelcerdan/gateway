
import struct
import os
import time

import gateway_utils as gu

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *

from tlvDataObject import *


class TlvAifStatusElement(TlvDataObject):

    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_XET_AIF_STATUS_STRUCTURED_TLV_TYPE,
                               "AIF Status Data",
                               ipAddr)

        self.loadFullDataDef(dataStruct(1, "version",                            0, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltWattRegulatingStatus",           1, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltWattPreDisturbanceModeEnable",   2, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "FreqWattRegStatusGtwOrInsight",      3, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "voltVarRegulatingStatus",            4, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "spareByte0",                         5, 1, "B"))
        self.loadFullDataDef(dataStruct(1, "spareWord0",                         6, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord1",                         8, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord2",                         10, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord3",                         12, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord4",                         14, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord5",                         16, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord6",                         18, 2, "!H"))
        self.loadFullDataDef(dataStruct(1, "spareWord7",                         20, 2, "!H"))


    def unpackDataStruct(self, data):
        self.unpackCommon(data)

        for i in self.getDataDefArray():
            self.loadDataVal(i.name, self.tlv.unpackFmtItem(i))

    def persistData(self, rxTime):
        # Nothing to persist
        return True

    def testMsg(self):
        msg = bytearray(self.getDataSize())

        return msg
