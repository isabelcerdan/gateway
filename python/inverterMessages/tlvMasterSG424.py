
import struct
from lib.gateway_constants.InverterConstants import *
from tlvDataObject import *

class TlvMasterSG424(TlvDataObject):
    def __init__(self, ipAddr):
        TlvDataObject.__init__(self,
                               SG424_TO_GTW_MASTER_U16_TLV_TYPE,
                               "SG424 Master TLV",
                               ipAddr)
        self.data = None
        self.count = 0
        self.tlvCount = 0

    def unpackDataStruct(self, data):
        self.data = data
        self.tlv = TlvUnpacker(False, data)

        try:
            self.count = self.tlv.unpackU16(0)
        except:
            raise TlvExceptionNoMaster()

    def dumpDataBuffer(self):
        return self.dumpHexBuf(self.data)


    def testMsg(self, tlvCount):
        msg = struct.pack("!hhh", self.tlvId, 2, tlvCount)
        return msg

