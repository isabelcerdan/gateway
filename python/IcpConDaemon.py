#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
@created: February 17, 2017

@module: ICP CON Daemon

@copyright: Apparent Inc. 2017

@reference: "ET-7000/PET-7000/ET-7200/PET7200  Series Register Table",
            Copyright  2013 by ICP DAS CO., LTD. All rights are reserved.

@author: steve
'''

import time
import os, signal
from lib.logging_setup import *
import MySQLdb as MyDB
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from daemon.daemon import Daemon
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.IcpConConstants import *
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_NO_NETWORK_PROTECTOR_CONFIGURED, \
                                  ALARM_ID_NETWORK_PROTECTOR_TRIP, \
                                  ALARM_ID_ICPCON_DAEMON_RESTARTED
from pymodbus.constants import Defaults


# Globals
'''
run-time dictionary structure definition:
    {mac_address: (device_name,
                   modbus_id,
                   di0_tripped,
                   di1_tripped,
                   di2_tripped,
                   di3_tripped)
    } 
                   
'''
RUNTIME_DEVICE_NAME_OFFSET = 0
RUNTIME_MODBUS_ID_OFFSET = 1
RUNTIME_D0_TRIPPED_VALUE_OFFSET = 2
RUNTIME_D1_TRIPPED_VALUE_OFFSET = 3
RUNTIME_D2_TRIPPED_VALUE_OFFSET = 4
RUNTIME_D3_TRIPPED_VALUE_OFFSET = 5

ICPCON_MAX_DB_ERRORS = 5
ICPCON_MAX_MODBUS_ERRORS = 3
ICPCON_UPDATE_INTERVAL = 5

# icpcon_devices register list
volatile_register_list = [ICPCON_DEVICES_TBL_DI0_VOLATILE_REGISTER_NAME,
                          ICPCON_DEVICES_TBL_DI1_VOLATILE_REGISTER_NAME,
                          ICPCON_DEVICES_TBL_DI2_VOLATILE_REGISTER_NAME,
                          ICPCON_DEVICES_TBL_DI3_VOLATILE_REGISTER_NAME]
trip_register_list = [ICPCON_DEVICES_TBL_DI0_REGISTER_NAME,
                      ICPCON_DEVICES_TBL_DI1_REGISTER_NAME,
                      ICPCON_DEVICES_TBL_DI2_REGISTER_NAME,
                      ICPCON_DEVICES_TBL_DI3_REGISTER_NAME]


def sig_handler(signum, frame):
    print "Caught signal:", signum
    os._exit(1)

            
class IcpConDaemon(Daemon):

    alarm_id = ALARM_ID_ICPCON_DAEMON_RESTARTED

    '''
    @summary: The IcpConThread is designed to poll multiple network protection devices.
              It will poll every device on every poll cycle.  If a trip event is detected,
              the controller daemon will transition to the 'tripped' state, waiting for
              a manual reset event.  While in the 'tripped' state, no polling will take 
              place.  In other words, a single 'trip' event disables polling of all 
              network protection devices until the reset occurs.
    '''
    def __init__(self, **kwargs):
        super(IcpConDaemon, self).__init__(**kwargs)
        self.runtime_dict = {}
        self.poll_freq_ms = float(ICPCON_DEFAULT_POLLING_FREQUENCY / 1000.0)
        self.db_errors = 0
        self.modbus_errors = 0
        self.trip_alarm_sent = False

        '''
        Set up run-time dictionary.
        '''
        try:
            self.get_runtime_parameters()
        except Exception as error:
            raise Exception(error)
        self.logger.info(repr(self.runtime_dict))
        '''
        Set up the initial conditions for the state machine.
        '''
        self.transition_to_normal()
        '''
        Now check to see if things are trashed-on-arrival.
        '''
        if self.in_protection_alert():
            Alarm.occurrence(ALARM_ID_NETWORK_PROTECTOR_TRIP)
            Alarm.request_clear(ALARM_ID_NETWORK_PROTECTOR_TRIP)
            self.transition_to_tripped()
        
    def get_runtime_parameters(self):
        self.logger.info('Getting run-time parameters...')
        '''
        @summary: This function spins through the database and extracts run-time parameters
                  for the devices, and uses the information to build the run-time dictionary
                  {runtime_dict}.
        '''
        found = False
        params_string = 'select %s, %s, %s, %s, %s, %s, %s from %s;' % \
                        (ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME,
                         ICPCON_DEVICES_TBL_DEVICE_NAME_COLUMN_NAME,
                         ICPCON_DEVICES_TBL_MODBUS_ID_COLUMN_NAME,
                         ICPCON_DEVICES_TBL_DI0_TRIPPED_VALUE,
                         ICPCON_DEVICES_TBL_DI1_TRIPPED_VALUE,
                         ICPCON_DEVICES_TBL_DI2_TRIPPED_VALUE,
                         ICPCON_DEVICES_TBL_DI3_TRIPPED_VALUE,
                         DB_ICPCON_DEVICES_TABLE)
        args = []
        try:
            devices = prepared_act_on_database(FETCH_ALL, params_string, args)
        except Exception as error:
            error_string = 'Unable to fetch run-time parameters - %s' % repr(error)
            self.logger.exception(error_string)
            self.state = ICPCON_STATE_ERROR
                
        for device in devices:
            self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]] = \
                              (device[ICPCON_DEVICES_TBL_DEVICE_NAME_COLUMN_NAME], 
                               device[ICPCON_DEVICES_TBL_MODBUS_ID_COLUMN_NAME],
                               device[ICPCON_DEVICES_TBL_DI0_TRIPPED_VALUE],
                               device[ICPCON_DEVICES_TBL_DI1_TRIPPED_VALUE],
                               device[ICPCON_DEVICES_TBL_DI2_TRIPPED_VALUE],
                               device[ICPCON_DEVICES_TBL_DI3_TRIPPED_VALUE])
            found = True
        
        if not found:
            raise Exception('Failed to get run-time parameters')

    '''
    read_discrete_inputs() reads the first three DI registers.
    '''    
    def read_discrete_inputs(self):
        mysql_string = 'select %s, %s from %s;' % \
            (ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME, ICPCON_DEVICES_TBL_MODBUS_ID_COLUMN_NAME,
             DB_ICPCON_DEVICES_TABLE)
        args = []
        try:
            devices = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            err_string = 'DB select failed for %s - %s' % (DB_ICPCON_DEVICES_TABLE, repr(error))
            self.logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False

        for device in devices:
            Defaults.Timeout = 0.75
            modbus_id = int(device[ICPCON_DEVICES_TBL_MODBUS_ID_COLUMN_NAME])
            try:
                client = ModbusClient(device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME], 
                                      port=ICPCON_MODBUS_PORT)
            except ValueError as error:
                self.modbus_errors += 1
                err_string = "Error with ICP host address %s - %s" % \
                             (device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME],
                              repr(error))   
                self.logger.error(err_string)
                if self.modbus_errors >= ICPCON_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    self.create_modbus_alert_entry()
                    return False
                continue
            
            if not client.connect():
                err_string = "Connect failed for ICP unit at %s" % \
                             device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME]
                self.logger.error(err_string)
                self.modbus_errors += 1
                if self.modbus_errors >= ICPCON_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum connection errors exceeded.')
                    self.create_modbus_alert_entry()
                    client.close()
                    return False
                continue

            try:
                discrete_inputs = client.read_discrete_inputs(ICPCON_DISCRETE_INPUT_REGISTER_OFFSET, 
                                                              count=ICPCON_NUM_DI_REGISTERS, 
                                                              unit=modbus_id)
            except Exception as error:
                err_string = 'read_discrete_inputs() failed - %s' % repr(error)
                self.logger.error(err_string)
                self.modbus_errors += 1
                if self.modbus_errors >= ICPCON_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum connection errors exceeded.')
                    self.create_modbus_alert_entry()
                    client.close()
                    return False
                continue
            
            client.close()

            if discrete_inputs is None:
                err_string = "Unable to read discrete inputs for device: %s" % \
                             device[ICPCON_DEVICES_TBL_DEVICE_NAME_COLUMN_NAME]
                self.logger.error(err_string)
                self.modbus_errors += 1
                if self.modbus_errors >= ICPCON_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum modbus errors exceeded.')
                    self.create_modbus_alert_entry()
                    return False
                continue
            else:
                '''
                Update the volatile register set. 
                '''
                self.modbus_errors = 0
                bit_list = []
                for i in range(ICPCON_NUM_DI_REGISTERS):
                    if discrete_inputs.getBit(i):
                        bit = 1
                    else:
                        bit = 0
                    bit_list.append(bit)
                update_string = 'update %s set %s = %s, %s = %s, %s = %s, %s = %s, '\
                                '%s = %s where %s = %s;' % \
                                (DB_ICPCON_DEVICES_TABLE, 
                                 volatile_register_list[0], '%s',
                                 volatile_register_list[1], '%s',
                                 volatile_register_list[2], '%s',
                                 volatile_register_list[3], '%s',
                                 ICPCON_DEVICES_TBL_TIMESTAMP_COLUMN_NAME, 'CURRENT_TIMESTAMP',
                                 ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME, '%s')
                args = [bit_list[0], bit_list[1], bit_list[2], bit_list[3], 
                        device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME]]
                try:
                    prepared_act_on_database(EXECUTE, update_string, args)
                except Exception as error:
                    error_string = 'Unable to update volatile register list for device at %s - %s' \
                                    % (device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME],
                                       repr(error))
                    self.logger.error(error_string)
                    self.db_errors += 1
                    if self.db_errors > ICPCON_MAX_DB_ERRORS:
                        self.logger.critical('Maximum database errors exceeded.')
                        return False
        return True

    def create_modbus_alert_entry(self):
        '''
        @attention: We only call this function when we are unable to communicate with
                    the PET-7202.  This is a Bad Thing, as we are unable to tell if
                    the external protectors are trying to signal us with trip events.
                    So we create a trip event for ALL the discrete inputs and rattle
                    EVERYONES cage.
        '''
        mysql_string = 'select * from %s' % DB_ICPCON_DEVICES_TABLE
        args = ''
        try:
            devices = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            err_string = 'DB select failed for %s - %s' % (DB_ICPCON_DEVICES_TABLE, repr(error))
            self.logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False

        for device in devices:
            input_name_list = []
            input_name_list.append(device[ICPCON_DEVICES_TBL_DI0_INPUT_NAME])
            input_name_list.append(device[ICPCON_DEVICES_TBL_DI1_INPUT_NAME])
            input_name_list.append(device[ICPCON_DEVICES_TBL_DI2_INPUT_NAME])
            input_name_list.append(device[ICPCON_DEVICES_TBL_DI3_INPUT_NAME])
            for input_name in input_name_list:
                mac_addr = device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]
                ip_addr = device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME]
                device_name = device[ICPCON_DEVICES_TBL_DEVICE_NAME_COLUMN_NAME]
                critical_string = 'Unable to communicate with Network Protector %s at %s' % (mac_addr, ip_addr)
                self.logger.critical(critical_string)
                alert_string = "insert into %s (%s, %s, %s, %s) " \
                               "values (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE " \
                               "%s = %s, %s = %s, %s = %s;" % \
                    (DB_PROTECTION_ALERT_TABLE, PROTECTION_ALERT_MAC_ADDR, PROTECTION_ALERT_IP_ADDR,
                     PROTECTION_ALERT_DEVICE_NAME, PROTECTION_ALERT_INPUT_NAME,
                     '%s', '%s', '%s', '%s',
                     PROTECTION_ALERT_IP_ADDR, '%s', PROTECTION_ALERT_DEVICE_NAME, '%s', PROTECTION_ALERT_INPUT_NAME, '%s')
                args = [mac_addr, ip_addr, device_name, input_name, ip_addr, device_name, input_name]
                try:
                    prepared_act_on_database(EXECUTE, alert_string, args)
                except Exception as error:
                    error_string = 'Unable to insert protection alert entry into database for device %s - %s' % (mac_addr, repr(error))
                    self.logger.critical(error_string)
                    self.db_errors += 1
                    if self.db_errors > ICPCON_MAX_DB_ERRORS:
                        self.logger.critical('Maximum database errors exceeded.')
                        return False
                else:
                    if not self.trip_alarm_sent:
                        Alarm.occurrence(ALARM_ID_NETWORK_PROTECTOR_TRIP)
                        Alarm.request_clear(ALARM_ID_NETWORK_PROTECTOR_TRIP)
                        self.logger.info('Entering device tripped state...')
                        self.trip_alarm_sent = True
                        #self.transition_to_tripped()
        self.set_all_registers_tripped()
        return True
    
    def set_all_registers_tripped(self):
        mysql_string = 'select * from %s' % DB_ICPCON_DEVICES_TABLE
        args = ''
        try:
            devices = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            err_string = 'DB select failed for %s - %s' % (DB_ICPCON_DEVICES_TABLE, repr(error))
            self.logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False
            return False

        for device in devices:
            for i in range(ICPCON_NUM_DI_REGISTERS):
                update_string = 'update %s set %s = %s;' % \
                                (DB_ICPCON_DEVICES_TABLE, trip_register_list[i], '%s')
                args = [self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]][RUNTIME_D0_TRIPPED_VALUE_OFFSET + i],]
                try:
                    prepared_act_on_database(EXECUTE, update_string, args)
                except Exception as error:
                    error_string = 'Unable to update register %d for device at %s - %s' \
                                    % (i,
                                       device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME],
                                       repr(error))
                    self.logger.error(error_string)
                    self.db_errors += 1
                    if self.db_errors > ICPCON_MAX_DB_ERRORS:
                        self.logger.critical('Maximum database errors exceeded.')
                        return False
        return True
        
    def process_triggers(self):
        mysql_string = 'select * from %s' % DB_ICPCON_DEVICES_TABLE
        args = ''
        try:
            devices = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            err_string = 'DB select failed for %s - %s' % (DB_ICPCON_DEVICES_TABLE, repr(error))
            self.logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False

        for device in devices:
            trigger = False
            input_name_list = []
            update_trip_register_list = []
            if device[ICPCON_DEVICES_TBL_DI0_VOLATILE_REGISTER_NAME] == \
                    self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]][RUNTIME_D0_TRIPPED_VALUE_OFFSET]:
                trigger = True
                input_name_list.append(device[ICPCON_DEVICES_TBL_DI0_INPUT_NAME])
                update_trip_register_list.append((ICPCON_DEVICES_TBL_DI0_REGISTER_NAME, device[ICPCON_DEVICES_TBL_DI0_VOLATILE_REGISTER_NAME]))
            if device[ICPCON_DEVICES_TBL_DI1_VOLATILE_REGISTER_NAME] == \
                    self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]][RUNTIME_D1_TRIPPED_VALUE_OFFSET]:
                trigger = True
                input_name_list.append(device[ICPCON_DEVICES_TBL_DI1_INPUT_NAME])
                update_trip_register_list.append((ICPCON_DEVICES_TBL_DI1_REGISTER_NAME, device[ICPCON_DEVICES_TBL_DI1_VOLATILE_REGISTER_NAME]))
            if device[ICPCON_DEVICES_TBL_DI2_VOLATILE_REGISTER_NAME] == \
                    self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]][RUNTIME_D2_TRIPPED_VALUE_OFFSET]:
                trigger = True
                input_name_list.append(device[ICPCON_DEVICES_TBL_DI2_INPUT_NAME])
                update_trip_register_list.append((ICPCON_DEVICES_TBL_DI2_REGISTER_NAME, device[ICPCON_DEVICES_TBL_DI2_VOLATILE_REGISTER_NAME]))
            if device[ICPCON_DEVICES_TBL_DI3_VOLATILE_REGISTER_NAME] == \
                    self.runtime_dict[device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]][RUNTIME_D3_TRIPPED_VALUE_OFFSET]:
                trigger = True
                input_name_list.append(device[ICPCON_DEVICES_TBL_DI3_INPUT_NAME])
                update_trip_register_list.append((ICPCON_DEVICES_TBL_DI3_REGISTER_NAME, device[ICPCON_DEVICES_TBL_DI3_VOLATILE_REGISTER_NAME]))
            if trigger:
                '''
                We have a trigger - update the alert table!
                '''
                for input_name in input_name_list:
                    mac_addr = device[ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME]
                    ip_addr = device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME]
                    device_name = device[ICPCON_DEVICES_TBL_DEVICE_NAME_COLUMN_NAME]
                    alert_string = "insert into %s (%s, %s, %s, %s) " \
                                   "values (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE " \
                                   "%s = %s, %s = %s, %s = %s;" % \
                        (DB_PROTECTION_ALERT_TABLE, PROTECTION_ALERT_MAC_ADDR, PROTECTION_ALERT_IP_ADDR,
                         PROTECTION_ALERT_DEVICE_NAME, PROTECTION_ALERT_INPUT_NAME,
                         '%s', '%s', '%s', '%s',
                         PROTECTION_ALERT_IP_ADDR, '%s', PROTECTION_ALERT_DEVICE_NAME, '%s', PROTECTION_ALERT_INPUT_NAME, '%s')
                    args = [mac_addr, ip_addr, device_name, input_name, ip_addr, device_name, input_name]
                    try:
                        prepared_act_on_database(EXECUTE, alert_string, args)
                    except Exception as error:
                        error_string = 'Unable to insert protection alert entry into database for device %s - %s' % (mac_addr, repr(error))
                        self.logger.critical(error_string)
                        self.db_errors += 1
                        if self.db_errors > ICPCON_MAX_DB_ERRORS:
                            self.logger.critical('Maximum database errors exceeded.')
                            return False
                    else:
                        if not self.trip_alarm_sent:
                            Alarm.occurrence(ALARM_ID_NETWORK_PROTECTOR_TRIP)
                            Alarm.request_clear(ALARM_ID_NETWORK_PROTECTOR_TRIP)
                            self.trip_alarm_sent = True

                for trip_register_name, trip_register_value in update_trip_register_list:
                    '''
                    Update the trip register set. 
                    '''
                    update_string = 'update %s set %s = %s, %s = %s where %s = %s;' % \
                                    (DB_ICPCON_DEVICES_TABLE,
                                     trip_register_name, '%s',
                                     ICPCON_DEVICES_TBL_TIMESTAMP_COLUMN_NAME, 'CURRENT_TIMESTAMP',
                                     ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME, '%s')
                    args = [trip_register_value, device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME]]
                    try:
                        prepared_act_on_database(EXECUTE, update_string, args)
                    except Exception as error:
                        error_string = 'Unable to update register %s for device at %s - %s' \
                                        % (trip_register_name,
                                           device[ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME],
                                           repr(error))
                        self.logger.error(error_string)
                        self.db_errors += 1
                        if self.db_errors > ICPCON_MAX_DB_ERRORS:
                            self.logger.critical('Maximum database errors exceeded.')
                            return False
                    
        return True

    '''
    House rules: if any entry has its 'tripped' field set, we are still 
                 in the TRIPPED state.  If there are no entries in the
                 table, we can't be in the TRIPPED state.
    returns: True if we are in the TRIPPED state. 
    '''
    def in_protection_alert(self):
        mysql_string = 'select * from %s;' % DB_PROTECTION_ALERT_TABLE 
        args = []
        try:
            alert_results = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            error_string = 'Failed to fetch PROTECTION ALERT fields from the database - %s' \
                            % repr(error)
            self.logger.error(error_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
            return True
        else:
            if not alert_results:
                # No entries - we've been unchained!
                return False
            else:
                return True
            
    '''
    @summary: Transition to the NORMAL processing state and reset the email_sent_count
              field in the admin_email table.  This will release the Notify Daemon to
              start spamming on the next 'tripped' event.
    '''
    def transition_to_normal(self):
        reset_string = 'update %s set %s = 0;' % (DB_NETPROTECT_EMAIL_TABLE, NETPROTECT_EMAIL_SEND_COUNT)
        args = ''
        try:
            prepared_act_on_database(EXECUTE, reset_string, args)
        except Exception as error:
            error_string = 'Failed to reset EMAIL SENT COUNT field in the database - %s' \
                            % repr(error)
            self.logger.error(error_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False

        '''
        Reset the latched register contents to their NOT tripped state.
        '''
        clear_string = 'update %s set %s = %s, %s = %s, %s = %s, %s = %s;' % \
                       (DB_ICPCON_DEVICES_TABLE,
                        ICPCON_DEVICES_TBL_DI0_REGISTER_NAME, '%s',
                        ICPCON_DEVICES_TBL_DI1_REGISTER_NAME, '%s',
                        ICPCON_DEVICES_TBL_DI2_REGISTER_NAME, '%s',
                        ICPCON_DEVICES_TBL_DI3_REGISTER_NAME, '%s')
        args = [int(not self.runtime_dict[self.runtime_dict.keys()[0]][RUNTIME_D0_TRIPPED_VALUE_OFFSET]), \
                int(not self.runtime_dict[self.runtime_dict.keys()[0]][RUNTIME_D1_TRIPPED_VALUE_OFFSET]), \
                int(not self.runtime_dict[self.runtime_dict.keys()[0]][RUNTIME_D2_TRIPPED_VALUE_OFFSET]), \
                int(not self.runtime_dict[self.runtime_dict.keys()[0]][RUNTIME_D3_TRIPPED_VALUE_OFFSET])]
        try:
            prepared_act_on_database(EXECUTE, clear_string, args)
        except Exception as error:
            error_string = 'Failed to reset register contents fields in the database - %s' \
                            % repr(error)
            self.logger.error(error_string)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return False
            
        self.state = ICPCON_STATE_NORMAL
        self.trip_alarm_sent = False
        self.logger.info('Entering NORMAL state...')
        return True                

    def transition_to_error(self):
        self.state = ICPCON_STATE_ERROR
        self.logger.critical('Entering ERROR state.')
        return True

    def transition_to_tripped(self):
        self.state = ICPCON_STATE_TRIPPED
        self.logger.info('Entering TRIPPED state...')
        return True

    def disabled(self):
        '''
        @summary: This function is checked every time we go through the main
                  processing loop.  It is needed to prevent a race condition
                  that occurs between disabling the daemon from the GUI and
                  the monitor actually whacking it.  We saw writes to the
                  protection_alert table *after* the GUI had cleared it upon
                  disabling the network protector.
        '''
        query_string = 'SELECT %s from %s WHERE %s = %s;' % (DAEMON_CONTROL_TABLE_ENABLED_COLUMN_NAME,
                                                             DB_DAEMON_CONTROL_TABLE_NAME,
                                                             DAEMON_CONTROL_TABLE_NAME_COLUMN_NAME,
                                                             "%s")
        args = ["icpcond"]
        try:
            result = prepared_act_on_database(FETCH_ONE, query_string, args)
        except Exception as error:
            self.logger.error('Unable to get enabled field - %s' % error)
            self.db_errors += 1
            if self.db_errors > ICPCON_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
                return True

        if result is None:
            self.logger.error('Daemon control table entry not provisioned.')
            return True
        
        if result['enabled'] == 0:
            return True
        
        return False
            
    def run(self):
        '''
        Read the inputs forever.  The sequence for normal processing is:
            1) Read the device registers for all devices.  Set the 'tripped' field
               in the database for the triggering register.
            2) Process the recorded triggers.
            3) Snooze for awhile.
        '''
        while True:
            self.update()

            '''
            Need to make sure we haven't been disabled by the GUI, but the
            Monitor hasn't gotten around to whacking us.
            '''
            if self.disabled():
                self.logger.info('IcpConDaemon disabled - exiting.')
                return True
            
            if self.state == ICPCON_STATE_NORMAL:
                if not self.read_discrete_inputs():
                    self.transition_to_error()
                elif not self.process_triggers():
                    self.transition_to_error()
                elif self.in_protection_alert():
                    self.transition_to_tripped()
                time.sleep(self.poll_freq_ms)
            elif self.state == ICPCON_STATE_TRIPPED:
                if not self.read_discrete_inputs():
                    self.transition_to_error()
                elif not self.process_triggers():
                    self.transition_to_error()
                elif not self.in_protection_alert():
                    self.transition_to_normal()
                time.sleep(self.poll_freq_ms)
            else:
                '''
                Abnormal processing: monitor the protection_alert table until someone
                clears the offending entries.
                '''
                if not self.in_protection_alert():
                    # It appears that someone cleared the protection alert table.  
                    # Wait another normal polling cycle and read it again.
                    time.sleep(self.poll_freq_ms)
                    if not self.in_protection_alert():
                        # OK, now I believe it.
                        self.transition_to_normal()
                        continue
                time.sleep(ICPCON_ERROR_CYCLE)


if __name__ == '__main__':

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    d = IcpConDaemon()
    d.run()

