#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import setproctitle
from gateway_utils import *

from lib.logging_setup import *

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_FD_SOCKET_FAILURE, \
                                  ALARM_ID_FD_SENDTO_FAILURE, \
                                  ALARM_ID_FD_CLOSE_FAILURE, \
                                  ALARM_ID_FD_FEED_INDEX_ERROR, \
                                  ALARM_ID_FD_EMPTY_IP_LIST

def instantiate_feed_discovery():
    try:
        setproctitle.setproctitle('%s-feed_discoveryd' % GATEWAY_PROCESS_PREFIX)
        FeedDiscoveryProcess()
    except Exception as ex:
        logger = setup_logger(__name__)
        logger.exception("Unhandled exception occurred during feed discovery.")

    
class FeedDiscoveryProcess(object):
    def __init__(self):
        self.this_current_date_and_time = None
        self.start_utc_time = None
        self.complete_utc_time = None
        self.end_utc_time = 0
        self.my_logger = None
        self.handler = None
        self.inverters_id = 0
        self.discover_command = ""
        self.feed_discovery_mode = FEED_DISCOVERY_MODE_NIGHT

        # Lists:
        self.string_id_total_list = []
        self.feed_name_total_list = []
        self.string_assigned_feed_list = []
        self.string_assignment_accepted_list = []

        # Add solar current set of readings
        self.standby_solar_current_readings_list = []
        self.active_solar_current_readings_list = []
        self.standby_accumulated_solar_current_readings_list = []
        self.active_accumulated_solar_current_readings_list = []
        self.differential_solar_current_readings_list = []
        self.string_assigned_feed_from_current_readings_list = []
        self.string_assignment_accepted_from_current_readings_list = []

        self.decided_feed_for_this_string = ""
        self.this_string_id = ""
        self.this_feed_name = ""
        self.number_of_user_tlvs = 0
        self.expected_inverter_fd_state = FEED_DISCOVERY_INVALID_FD_STATE
        self.string_id_list_length = 0
        self.next_string_index = 0
        self.fd_by_string_status = None
        self.fd_by_string_feed_discovered = None
        self.min_current_differential = 0

        # Data specific to feed discovery control:
        self.current_string_id_to_discover = ""
        self.current_string_number_of_inverters = 0
        self.current_fd_command = 0
        self.current_fd_command_length = 0
        self.current_ip_list = []
        self.current_solar_feed_name = None
        self.current_ip_to_send = None
        self.current_feed_to_send = None
        # ??? self.current_string_id = ""

        # With data initialized, get to work:
        self.setup_logger()

        # ----------------------------------------
        self.start_feed_discovery()
        # ----------------------------------------

    def log_this_hex_data(self, some_hex_data):
        this_string = "hex data:"
        my_data_string = ""
        for char_char in some_hex_data:
            this_string += hex(ord(char_char))[2:].zfill(2) + " "
        self.log_info(this_string)

    def setup_logger(self):
        self.my_logger = setup_logger("FeedDiscoveryProcess")

    def log_info(self, message):
        self.my_logger.info(message)

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)

    def send_generic_multicast_fd_command(self):

        # Construct a multicast IP to target standard/solar inverters:
        multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
        this_port = SG424_PRIVATE_UDP_PORT

        # All feed discovery commands are SHORTs.
        if self.current_fd_command_length == FEED_DETECT_FIELD_LENGTH:
            number_of_user_tlv = 1
            dummy_data = 1234
            packed_data = add_master_tlv(number_of_user_tlv)
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, self.current_fd_command, dummy_data)

            # OPEN THE SOCKET:
            try:
                fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            except Exception: # as multicast_error:
                # Not expected ...
                self.raise_and_clear_alarm(ALARM_ID_FD_SOCKET_FAILURE)
            else:

                # SEND THE PACKET:
                fancy_socks.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 3)
                try:
                    fancy_socks.sendto(packed_data, (multicast_ip, this_port))
                except Exception: # as multicast_error:
                    # Has been known to happen. Close the socket.
                    self.raise_and_clear_alarm(ALARM_ID_FD_SENDTO_FAILURE)
                    try:
                        fancy_socks.close()
                    except Exception: # as multicast_error:
                        # Never really expected, but you never know ...
                        self.raise_and_clear_alarm(ALARM_ID_FD_CLOSE_FAILURE)
                    else:
                        # Notice already made that the send failed, the close was OK,
                        # so nothing to do here.
                        pass

                else:

                    # Close the socket.
                    try:
                        fancy_socks.close()
                    except Exception: # as multicast_error:
                        # Never really expected, but you never know ...
                        self.raise_and_clear_alarm(ALARM_ID_FD_CLOSE_FAILURE)
                    else:
                        #
                        this_string = "send MCAST TLV COMMAND %s OK" % (self.current_fd_command)
                        self.log_info(this_string)
        else:
            this_string = "FD(1): INVALID COMMAND LENGTH %s FOR FD COMMAND %s" % (self.current_fd_command_length,
                                                                                  self.current_fd_command)
            self.log_info(this_string)

        return

    def get_inverter_feed_discovery_state(self):

        this_fd_state = FEED_DISCOVERY_INVALID_FD_STATE
        this_ram_inv_ctrl_value = 0

        # Construct and send a FD QUERY TLV packet, and wait for a reply.
        number_of_user_tlv = 1
        dummy_data = 1234
        packed_data = add_master_tlv(number_of_user_tlv)
        packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_FD_QUERY_STATE_TLV_TYPE_U16_TLV_TYPE, dummy_data)

        # OPEN THE SOCKET:
        try:
            # No bind here, let the OS choose the port for the reply, since there is an immediate wait.
            fancy_socks = socket.socket(type=socket.SOCK_DGRAM)
        except Exception: # as error:
            self.raise_and_clear_alarm(ALARM_ID_FD_SOCKET_FAILURE)
        else:

            # Make a single request to get the inverter's FD state. There are no retries.
            try:
                fancy_socks.sendto(packed_data, (self.current_ip_to_send, SG424_PRIVATE_UDP_PORT))
            except Exception: # as error:
                # This is not good. Nothing to do with the inverter. Bail out of the loop.
                self.raise_and_clear_alarm(ALARM_ID_FD_SENDTO_FAILURE)
            else:
                # Now wait for a response:
                try:
                    fancy_socks.settimeout(5.0)
                    self.private_port_packet = fancy_socks.recvfrom(256)
                except Exception:
                    # Failure to get a response. Could be many reasons: the inverter does not understand TLV,
                    # the request was lost en route or reply lost on its way back. Or there is no longer an
                    # inverter at the indicated IP address. More than likely, it is a communication issue with
                    # the HOS inverter.
                    this_string = "GIFDS: %s: recvfrom timed out" % (self.current_ip_to_send)
                    self.log_info(this_string)
                    this_fd_state = FEED_DISCOVERY_INVERTER_NOT_RESPONDING
                else:
                    # Packet is received. BTW: is it necessary to verify that some_ip is the current_ip_to_send?
                    (some_ip, some_port, SG424_data) = get_udp_components(self.private_port_packet)
                    # log_this_hex_data(self, some_hex_data):

                    (tlv_id_list, tlv_data_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                    # There are only 2 TLVs returned in the QUERY FD command. Ignore all others that may be included.
                    if (len(tlv_id_list) == len(tlv_data_list)) and len(tlv_id_list) != 0:
                        for iggy in range(0, len(tlv_id_list)):
                            if tlv_id_list[iggy] == SG424_TO_GTW_FD_STATE_U16_TLV_TYPE:
                                this_fd_state = tlv_data_list[iggy]
                            elif tlv_id_list[iggy] == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:
                                this_ram_inv_ctrl_value  = tlv_data_list[iggy]
                            else:
                                # Ignore this TLV.
                                pass

                    else:
                        # Got a response, but no data included? A corrupted response from the inverter?
                        this_string = "GIFDS: %s -> rcv'd zero length TLV response to FD query" % (self.current_ip_to_send)
                        self.log_info(this_string)

            # Close the socket:
            try:
                fancy_socks.close()
            except Exception: # as error:
                self.raise_and_clear_alarm(ALARM_ID_FD_CLOSE_FAILURE)
            else:
                #
                pass

        return (this_fd_state, this_ram_inv_ctrl_value)

    def send_generic_unicast_fd_command(self):

        this_port = SG424_PRIVATE_UDP_PORT

        # All feed discovery commands are SHORTs.
        if self.current_fd_command_length == FEED_DETECT_FIELD_LENGTH:
            number_of_user_tlv = 1
            dummy_data = 1234
            packed_data = add_master_tlv(number_of_user_tlv)
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, self.current_fd_command, dummy_data)

            # OPEN THE SOCKET:
            try:
                fancy_socks = socket.socket(type=socket.SOCK_DGRAM)
            except Exception: # as error:
                self.raise_and_clear_alarm(ALARM_ID_FD_SOCKET_FAILURE)
            else:

                # SEND THE PACKET:
                try:
                    fancy_socks.sendto(packed_data, (self.current_ip_to_send, this_port))
                except Exception: # as error:
                    # Has been known to happen. Close the socket.
                    self.raise_and_clear_alarm(ALARM_ID_FD_SENDTO_FAILURE)
                    try:
                        fancy_socks.close()
                    except Exception: # as unicast_error:
                        # Never really expected, but you never know ...
                        self.raise_and_clear_alarm(ALARM_ID_FD_CLOSE_FAILURE)
                    else:
                        # Notice already made that the send failed, the close was OK,
                        # so nothing to do here.
                        pass

                else:

                    # Close the socket.
                    try:
                        fancy_socks.close()
                    except Exception: # as unicast_error:
                        # Never really expected, but you never know ...
                        self.raise_and_clear_alarm(ALARM_ID_FD_CLOSE_FAILURE)
                    else:
                        # Don't log - pollutes too much.
                        # this_string = "send UCAST TLV COMMAND " + str(self.current_fd_command) \
                        #               + " TO " + self.current_ip_to_send + " OK"
                        # self.log_info(this_string)
                        pass
        else:
            this_string = "FD(2): INVALID COMMAND LENGTH %s FOR FD COMMAND %s" % (self.current_fd_command_length,
                                                                                  self.current_fd_command)
            self.log_info(this_string)

        return

    def is_inverters_table_ok_for_fd(self):

        inverters_table_is_okey_dokey = True
        string_id_not_a_null_ip = False
        string_id_not_initialized = False
        string_position_not_a_digit_encountered = False
        string_position_out_of_range_encountered = False

        # Extract all entries from the inverters table with ... SAY SOMETHING INTELLIGENT.
        select_criteria = "%s, %s, %s, %s" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                              INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                              INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                              INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
        select_ip_string = "SELECT %s FROM %s;" % (select_criteria, DB_INVERTER_NXO_TABLE)
        inverters_feed = []
        inverters_feed = prepared_act_on_database(FETCH_ALL, select_ip_string, ())
        for each_inverter in inverters_feed:
            # Check the validity of string ID and string position:
            this_inverters_id = each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME]
            this_ip_address = each_inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            this_string_id = each_inverter[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
            this_string_position = str(each_inverter[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME])

            # The "rules" for integrity checks have been relaxed, such that:
            #
            # - IP address is not NULL
            # - string_id_not_initialized
            # - string_position_not_a_digit_encountered
            # - string_position_out_of_range_encountered

            # First check that the IP address is not NULL.
            if not this_ip_address:
                string_id_not_a_null_ip = True
                this_string = "inverters ID %s -> NULL IP" % (this_inverters_id)
                self.log_info(this_string)

            # Next check that the string ID is a digit.
            elif this_string_id == "00:00:00":
                # String ID is the last 3 bytes of the HOS's MAC. If 00:00:00 then
                # string discovery was not launched or failed.
                this_string = "inverters ID %s (IP %s) -> stringId not initialized" % (this_inverters_id, this_ip_address)
                self.log_info(this_string)
                string_id_not_initialized = True

            # Check that the string position is a digit.
            elif not this_string_position.isdigit():
                # String position is NOT a digit.
                this_string = "inverters ID %s (IP %s) -> stringPosition not a digit: %s" % (this_inverters_id,
                                                                                             this_ip_address,
                                                                                             this_string_position)
                self.log_info(this_string)
                string_position_out_of_range_encountered = True

            # Finally, check that the string position is within 1..8.
            elif (int(this_string_position) < 1) \
              or (int(this_string_position) > MAX_NUMBER_OF_INVERTERS_IN_STRING):
                # The string position is out of range.
                this_string = "inverters ID %s(IP %s) -> stringPosition out-of-range: %s" % (this_inverters_id,
                                                                                             this_ip_address,
                                                                                             this_string_position)
                self.log_info(this_string)
                string_position_out_of_range_encountered = True

            else:
                # All is good. Nothing to do. Go to the next entry in the inverters table.
                pass

        if string_id_not_a_null_ip \
            or string_id_not_initialized \
            or string_position_not_a_digit_encountered \
            or string_position_out_of_range_encountered:
            # String position needs to be re-run.
            this_string = "DATABASE IP/STRING ID/POSITION NEEDS RESOLUTION, PROCEED WITH FD ANYWAY"
            self.log_info(this_string)

        return inverters_table_is_okey_dokey

    def build_string_discovery_list(self):
        # Create a "total list" of distinct string IDs from the fd_by_string table.
        # Although sequential, the string ID list may have "holes". Deal with it.

        # Select string: SELECT * FROM fd_by_string;

        self.string_id_total_list = []

        # Extract all rows from the fd_by_string table whose string ID is not zero. This is
        # the list whose FEED is to be discovered.
        # SELECT stringId FROM fd_by_string
        select_criteria = "%s" % (FD_BY_STRING_TBL_STRING_ID_COLUMN_NAME)
        select_string = "SELECT %s FROM %s;" % (select_criteria, FD_BY_STRING_TABLE)
        string_id_list = prepared_act_on_database(FETCH_ALL, select_string, ())

        # Construct the list of string IDs.
        for string in string_id_list:
            string_id = string[FD_BY_STRING_TBL_STRING_ID_COLUMN_NAME]
            if not string_id == "00:00:00":
                # String ID is not zero, add it to the list.
                self.string_id_total_list.append(string_id)
                this_string = "ADDING STRING ID %s TO THE LIST" % (string_id)
                self.log_info(this_string)
            else:
                # A string ID of 0 means the string positioning needs to be run.
                # This string obviously not added to the list, but this should
                # have been caught in the initial audit of the inverters table
                # when this process first launched. Something must have happened
                # since.
                this_string = "unexpected: encountered uninitialized string ID"
                self.log_info(this_string)

    def set_min_current_differential(self):

        # The number of strings in the system is determined by the number of
        # rows in the inverters table with stringPosition of 1.
        self.min_current_differential = 0
        select_string = "SELECT COUNT(*) FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME, "%s")
        select_args = (1,)
        string_count = prepared_act_on_database(FETCH_ROW_COUNT, select_string, select_args)

        if string_count < SMALL_SYSTEM_NUMBER_OF_STRINGS_CUTOFF:
            self.min_current_differential = MIN_CURRENT_DIFFERENTIAL_TINY_SYSTEM

        elif string_count < LARGER_SYSTEM_NUMBER_OF_STRINGS_CUTOFF:
            self.min_current_differential = MIN_CURRENT_DIFFERENTIAL_MEDIUM_SYSTEM

        else:
            self.min_current_differential = MIN_CURRENT_DIFFERENTIAL_LARGER_SYSTEM = 10.0
        this_string = "System with %s strings: min current differential: %s" % (string_count, self.min_current_differential)
        self.log_info(this_string)

    def set_all_fd_by_string_status_in_queue(self):
        # Purpose: Set the status field in the fd_by_string table to in_queue.

        set_string = "UPDATE %s SET %s = %s;" % (FD_BY_STRING_TABLE, FD_BY_STRING_TBL_STATUS_COLUMN_NAME, "%s")
        set_args = (self.fd_by_string_status,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def update_fd_by_string_status(self):
        # Purpose: update the status field of the fd_by_string table based on string ID.

        set_string = "UPDATE %s SET %s = %s where %s = %s;" % (FD_BY_STRING_TABLE,
                                                               FD_BY_STRING_TBL_STATUS_COLUMN_NAME, "%s",
                                                               FD_BY_STRING_TBL_STRING_ID_COLUMN_NAME, "%s")

        set_args = (self.fd_by_string_status, self.current_string_id_to_discover,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def update_fd_by_string_status_and_feed(self):
        # self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_SUCCESS
        # self.fd_by_string_feed_discovered = self.decided_feed_for_this_string

        set_string = "UPDATE %s SET %s = %s, %s = %s where %s = %s;" % (FD_BY_STRING_TABLE,
                                                                        FD_BY_STRING_TBL_STATUS_COLUMN_NAME, "%s",
                                                                        FD_BY_STRING_TBL_FEED_DISCOVERED_COLUMN_NAME, "%s",
                                                                        FD_BY_STRING_TBL_STRING_ID_COLUMN_NAME, "%s")
        set_args = (self.fd_by_string_status, self.fd_by_string_feed_discovered, self.current_string_id_to_discover,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def get_string_ip_list(self):

        # Extract all rows from the inverters table whose string ID is the current one.
        select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                              DB_INVERTER_NXO_TABLE,
                                                              INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        select_args = (self.current_string_id_to_discover,)
        ip_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        # Construct the list of string IDs.
        self.current_ip_list = []
        for each_ip in ip_list:
            this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            self.current_ip_list.append(this_ip)

    def get_db_solar_power(self):
        if self.current_solar_feed_name not in ["A", "B", "C"]:
            raise Exception("Feed discovery can only be run on feed A, B or C: %s" % self.current_solar_feed_name)

        real_power_field = "Real_Power_%s_FP" % self.current_solar_feed_name
        current_field = "Current_%s_FP" % self.current_solar_feed_name
        sql = "SELECT %s, %s FROM solar_readings_1 WHERE row_id = 1" % (real_power_field, current_field)
        this_solar_read = prepared_act_on_database(FETCH_ONE, sql, [])

        # flip polarity of consumption readings (-import/+export) to match
        # feed discovery expectation of production readings (+export/-import)
        solar_power_value = -this_solar_read[real_power_field]
        solar_current_value = -this_solar_read[current_field]

        return solar_power_value, solar_current_value

    def is_feed_discovery_canceled(self):
        proceed_canceled = False
        select_string = "SELECT * FROM %s;" % FEED_DISCOVERY_TABLE
        feed_discovery_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        feed_command = feed_discovery_row[FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME]

        if feed_command == FEED_DISCOVERY_COMMAND_CANCEL_DISCOVERY:
            # The command indicates that the process was canceled.
            proceed_canceled = True

        return proceed_canceled

    def check_feed_name_change(self):
        select_string = "SELECT %s,%s,%s FROM %s where %s = %s;" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                                    INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                    INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                                                    DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        select_args = (self.this_string_id,)
        all_feed_names = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        this_string = "CURRENT DB STATUS FOR STRING ID %s" % (self.this_string_id)
        self.log_info(this_string)
        this_string = ""
        for feed_name in all_feed_names:
            this_string += "%s" % (feed_name[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
            if not feed_name[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]:
                this_string += "(NULL->%s) " % (self.this_feed_name)
            else:
                # The database has a valid feed name. Will it change?
                this_string += "(%s->%s" % (feed_name[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME], self.this_feed_name)
                if feed_name[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME] != self.this_feed_name:
                    # This would be a good place for a Gateway alarm!
                    this_string += "*CHANGED*) "
                else:
                    this_string += ") "
        self.log_info(this_string)

    def update_feed_db_component(self):
        # Purpose: update the feed name based on what the discovery process determined.

        # First things first: before updating the feed name, verify if a change has
        # been detected. If so, write to the log.
        self.check_feed_name_change()

        # Update the FEED NAME for all entries in the inverters table that correspond to
        # the specified string ID.
        this_string = "UPDATING DB STRING ID %s WITH FEED NAME %s" % (self.this_string_id, self.this_feed_name)
        self.log_info(this_string)
        set_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                               INVERTERS_TABLE_FEED_NAME_COLUMN_NAME, "%s",
                                                               INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        set_args = (self.this_feed_name, self.this_string_id,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def inverters_feed_name_update(self):

        # Extract all entries from the inverters table with the indicated string ID.
        select_criteria = "SELECT %s, %s FROM %s WHERE %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                    INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                                                    DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        select_args = (self.this_string_id,)
        inverters_to_config = []
        inverters_to_config = prepared_act_on_database(FETCH_ALL, select_criteria, select_args)
        for each_inverter in inverters_to_config:
            self.current_ip_to_send = each_inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            self.current_feed_to_send = each_inverter[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]
            self.current_string_id_to_discover = self.this_string_id

            # Send this config update to the inverter:
            if self.current_feed_to_send == "A":
                this_feed_value = int(FEED_A_MULTICAST)  # 1
            elif self.current_feed_to_send == "B":
                this_feed_value = int(FEED_B_MULTICAST)  # 2
            else:
                # Only FEED_C_MULTICAST remains ...
                this_feed_value = int(FEED_C_MULTICAST)  # 4
            this_pcc_value = 1
            donut_care = is_send_feed_pcc_command(self.current_ip_to_send, this_pcc_value, this_feed_value)

    def get_num_inverters_on_current_string(self):

        inverters_count = 0
        select_string = "SELECT COUNT(*) FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        select_args = (self.current_string_id_to_discover,)
        inverters_count = prepared_act_on_database(FETCH_ROW_COUNT, select_string, select_args)
        return inverters_count

    def clear_standby_accumulated_solar_current_readings_list(self):
        for iggy in range(0, len(self.feed_name_total_list)):
            self.standby_accumulated_solar_current_readings_list[iggy] = 0.0

    def clear_active_accumulated_solar_current_readings_list(self):
        for iggy in range(0, len(self.feed_name_total_list)):
            self.active_accumulated_solar_current_readings_list[iggy] = 0.0

    def take_standby_meter_readings(self):

        # First clear the "accumulated" standby readings list for all feeds.
        # The accumulation is made for each reading.
        self.clear_standby_accumulated_solar_current_readings_list()

        for pop in range(0, NUMBER_OF_SOLAR_READS_FOR_DISCOVERY):
            time.sleep(TIMEOUT_BETWEEN_SOLAR_READS)
            # Read all solar meters.
            for iggy in range(0, len(self.feed_name_total_list)):
                self.current_solar_feed_name = self.feed_name_total_list[iggy]
                solar_power, solar_current = self.get_db_solar_power()
                self.standby_solar_current_readings_list[iggy][pop] = solar_current
                self.standby_accumulated_solar_current_readings_list[iggy] += solar_current

        # Now that the readings are complete, write to the log. First a small "log header",
        # of the form "TOTALS: A/B/C: ..." (account for 1, 2, or 3 feeds).
        standby_current_readings_log = "STANDBY SOLAR CURRENT READINGS (TOTALS: "
        for iggy in range(0, len(self.feed_name_total_list)):
            standby_current_readings_log += self.feed_name_total_list[iggy] + "=" \
                                  + str(self.standby_accumulated_solar_current_readings_list[iggy])
            if iggy < (len(self.feed_name_total_list) - 1):  # formatted display for log
                standby_current_readings_log += "/"

        standby_current_readings_log += "): "

        # Fetch the readings from the standby list, and format the string for log display.
        for pop in range(0, NUMBER_OF_SOLAR_READS_FOR_DISCOVERY):
            for iggy in range(0, len(self.feed_name_total_list)):
                standby_current_readings_log += str(self.standby_solar_current_readings_list[iggy][pop])
                if iggy < (len(self.feed_name_total_list) - 1):
                    standby_current_readings_log += "/"

            # Add a comma and a space before advancing to the next set of solar readings.
            if pop < (NUMBER_OF_SOLAR_READS_FOR_DISCOVERY - 1):
                standby_current_readings_log += ", "

        # Finally, add the string to the log.
        self.log_info(standby_current_readings_log)

    def take_active_meter_readings(self):

        # First clear the "accumulated" standby readings list for all feeds.
        # The accumulation is made for each reading.
        self.clear_active_accumulated_solar_current_readings_list()

        for pop in range(0, NUMBER_OF_SOLAR_READS_FOR_DISCOVERY):
            time.sleep(TIMEOUT_BETWEEN_SOLAR_READS)
            # Read all solar meters.
            for iggy in range(0, len(self.feed_name_total_list)):
                self.current_solar_feed_name = self.feed_name_total_list[iggy]
                solar_power, solar_current = self.get_db_solar_power()
                self.active_solar_current_readings_list[iggy][pop] = solar_current
                self.active_accumulated_solar_current_readings_list[iggy] += solar_current

        # Now that the readings are complete, write to the log. First a small "log header",
        # of the form "...A/B/C:" (account for 1, 2, or 3 feeds).
        active_current_readings_log = "ACTIVE SOLAR CURRENT READINGS (TOTALS: "
        for iggy in range(0, len(self.feed_name_total_list)):
            active_current_readings_log += "%s=%s" % (self.feed_name_total_list[iggy],
                                                      self.active_accumulated_solar_current_readings_list[iggy])
            if iggy < (len(self.feed_name_total_list) - 1):  # formatted display for log
                active_current_readings_log += "/"

        active_current_readings_log += "): "

        # Fetch the readings from the active list, and format the string for log display.
        for pop in range(0, NUMBER_OF_SOLAR_READS_FOR_DISCOVERY):
            for iggy in range(0, len(self.feed_name_total_list)):
                active_current_readings_log += str(self.active_solar_current_readings_list[iggy][pop])
                if iggy < (len(self.feed_name_total_list) - 1):
                    active_current_readings_log += "/"

            # Add a comma and a space before advancing to the next set of solar readings.
            if pop < (NUMBER_OF_SOLAR_READS_FOR_DISCOVERY - 1):
                active_current_readings_log += ", "

        # Finally, add the string to the log.
        self.log_info(active_current_readings_log)

    def display_ip_for_each_string(self):

        this_string = "Inverter IP on strings for discovery:"
        self.log_info(this_string)
        for iggy in range(0, len(self.string_id_total_list)):

            # Extract all rows from the inverters table whose string ID is the current one.
            select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                  DB_INVERTER_NXO_TABLE,
                                                                  INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
            select_args = (self.string_id_total_list[iggy],)
            ip_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
            # Construct the list of string IDs.
            this_string = "string ID %s: " % self.string_id_total_list[iggy]
            for each_ip in ip_list:
                this_ip_address = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                this_string += " %s" % this_ip_address
            self.log_info(this_string)

    def one_string_discovery(self):

        # This method invoked for as many strings there are in the system. Before doing
        # anything else, send all inverters a "feed detect mode reminder" command: inverters
        # will kick themselves out of FD mode if not periodically reminded of an on-going
        # FD detect process.
        self.current_fd_command = GTW_TO_SG424_FD_MODE_REMINDER_U16_TLV_TYPE
        self.current_fd_command_length = FD_MODE_REMINDER_TLV_DATA_FIELD_LENGTH
        self.send_generic_multicast_fd_command()

        # Set the "current string" in the database. This provides a progress indicator
        # to the GUI user. Then get the list of IP addresses for this string. Each of
        # the inverters in that list will be sent a series of TLV command to enter the
        # "FD START" mode, and thereafter, solar power readings will be taken from all
        # feeds.
        #
        self.current_string_number_of_inverters = self.get_num_inverters_on_current_string()
        this_string = "feed me, Seymor: string ID %s (%s inverters, string number %s of %s)" \
                                                                             % (self.current_string_id_to_discover,
                                                                                self.current_string_number_of_inverters,
                                                                                self.next_string_index,
                                                                                self.string_id_list_length)
        self.log_info(this_string)
        self.update_db_current_string()
        self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_STBY_MEASURE
        self.update_fd_by_string_status()

        self.get_string_ip_list()

        if len(self.current_ip_list) > 0:

            # There is no retry if feed discovery fails.

            # -------------------------------------------------------------------------------------
            # 1: Take a set of "standby" solar meter readings over a short period of time while
            #    the network of inverters are in some form of standby operation (most would be in
            #    WAIT_TO_END state, and 1 string is expected to be in FD STBY state), say: 20 reads
            #    over a 1-minute period. Wait 30 seconds or so to allow the inverters to settle down.

            # INVERTER VALIDATION THAT THEY ARE IN THE EXPECTED FD STATE:
            expected_fd_state = True
            non_responding_inverters = 0
            for each_ip in self.current_ip_list:
                self.current_ip_to_send = each_ip
                (current_fd_state, inverter_ram_inv_ctrl) = self.get_inverter_feed_discovery_state()
                if current_fd_state != FEED_DISCOVERY_STBY_STATE:
                    expected_fd_state = False
                    this_string = "INVERTER %s NOT FD STBY, BUT IN STATE %s (INV_CTRL = 0x%s)" % (self.current_ip_to_send,
                                                                                                  current_fd_state,
                                                                                                  hex(inverter_ram_inv_ctrl)[2:].zfill(2))
                    self.log_info(this_string)
                    if current_fd_state == FEED_DISCOVERY_INVERTER_NOT_RESPONDING:
                        non_responding_inverters += 1

            # If none of the inverters response to query, then bail. This is known
            # to happen if the head-of-string inverter is having issues.
            if non_responding_inverters == len(self.current_ip_list):
                # There was no communication to any inverter on this string.
                # To help make it easy to parse and cull the log file for results,
                # add this entry to the log.
                self.decided_feed_for_this_string = DETECTED_FEED_NO_COMM
                this_string = "STRING ID %s IS LIKELY ON FEED %s" % (self.current_string_id_to_discover, self.decided_feed_for_this_string)
                self.log_info(this_string)

                # Enter a log notice that will make it easy to extract data
                # for an excel spreadsheet.
                this_string = "EXCEL:"
                this_string += self.current_string_id_to_discover
                this_string += "," + str(self.current_string_number_of_inverters)
                this_string += "," + str(self.next_string_index)
                for iggy in range(0, len(self.feed_name_total_list)):
                    this_string += ",%s,0.0,0.0" % (self.feed_name_total_list[iggy])
                for iggy in range(0, len(self.feed_name_total_list)):
                    this_string += "," + "0.0"
                this_string += "," + self.decided_feed_for_this_string

                # Now write this string to the log.
                self.log_info(this_string)

                # Update the fd_by_string table.
                self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_NO_COMM
                self.update_fd_by_string_status()

                del self.current_ip_list[:]
                return

            if expected_fd_state == True:
                this_string = "Validation: inverters on string ID %s in FD_STBY ..." % (self.current_string_id_to_discover)
                self.log_info(this_string)

            this_string = "WAIT FOR INVERTERS TO QUIET DOWN ..."
            self.log_info(this_string)
            time.sleep(TIME_FOR_INVERTERS_TO_QUIET_DOWN)
            this_string = "TAKING STANDBY METER READINGS ..."
            self.log_info(this_string)
            self.take_standby_meter_readings()

            # -------------------------------------------------------------------------------------
            # 2: Command those inverters in the current string to go into FD DETECT mode (unicast).
            #    Give the inverters time to draw max power, about a minute.
            this_string = "Send inverters on string ID %s into FD ACTIVE ..." % (self.current_string_id_to_discover)
            self.log_info(this_string)

            # Update the status field in th3e fd_by_string table.
            self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_ACT_MEASURE
            self.update_fd_by_string_status()

            self.current_fd_command = GTW_TO_SG424_FD_ENTER_DETECT_STATE_U16_TLV_TYPE
            self.current_fd_command_length = FD_ENTER_DETECT_STATE_TLV_DATA_FIELD_LENGTH
            for each_ip in self.current_ip_list:
                self.current_ip_to_send = each_ip
                self.send_generic_unicast_fd_command()
            # Give plenty of time for the messages to be delivered and take effect. On the
            # inverter side, it has to acquire PLL, and that's about 5 to 10 seconds at
            # least.
            time.sleep(TIME_FOR_INVERTER_TO_ACQUIRE_PLL)

            # -------------------------------------------------------------------------------------
            # INVERTER VALIDATION THAT THEY ARE IN THE FEED DETECT/ACTIVE FD STATE:
            #
            # 3: Before going any further, verify/audit that the inverters on this string are
            #    in the desired state.
            expected_fd_state = True
            for each_ip in self.current_ip_list:
                self.current_ip_to_send = each_ip
                (current_fd_state, inverter_ram_inv_ctrl) = self.get_inverter_feed_discovery_state()
                # Inverters are in "active vars correction" when in this state:
                if current_fd_state != FEED_DISCOVERY_WAIT_TO_END_STATE:
                    expected_fd_state = False
                    this_string = "INVERTER %s NOT FD ACTIVE, BUT IN STATE %s (INV_CTRL = 0x%s)" % (self.current_ip_to_send,
                                                                                                    current_fd_state,
                                                                                                    hex(inverter_ram_inv_ctrl)[2:].zfill(2))
                    self.log_info(this_string)
            if expected_fd_state == True:
                this_string = "Validation: inverters on string ID %s in FD_ACT ..." % (self.current_string_id_to_discover)
                self.log_info(this_string)

            # -------------------------------------------------------------------------------------
            # 4: Take a set of "active" solar meter readings. First give the inverters time to draw
            #    max power.
            this_string = "STRING NOW IN FD ACTIVE, WAIT TO DRAW MAX ENERGY ..."
            self.log_info(this_string)
            time.sleep(TIME_FOR_INVERTERS_TO_RAMP_FD_START)
            this_string = "TAKING ACTIVE METER READINGS ..."
            self.log_info(this_string)
            self.take_active_meter_readings()

            # -------------------------------------------------------------------------------------
            # 5. Display total accumulated standby/active current readings:
            this_string = "ACCUMULATED AMPS per FEED ACT/STBY:"
            self.log_info(this_string)
            for iggy in range(0, len(self.feed_name_total_list)):
                difference = abs(self.active_accumulated_solar_current_readings_list[iggy] \
                               - self.standby_accumulated_solar_current_readings_list[iggy])
                difference = '{0:.2f}'.format(difference)
                this_string = self.feed_name_total_list[iggy] + ", " \
                              + str(self.active_accumulated_solar_current_readings_list[iggy]) \
                              + ", " \
                              + str(self.standby_accumulated_solar_current_readings_list[iggy]) \
                              + " diff = " + str(difference)
                self.log_info(this_string)

            # -------------------------------------------------------------------------------------
            # 6. On the basis of accumulated current readings, determine which feed is the best
            #    candidate for feed assignment:
            self.decided_feed_for_this_string = DETECTED_FEED_YUCK

            # First calculate the active-standby difference for all feeds:
            feed_act_stby_difference = []
            for iggy in range(0, len(self.feed_name_total_list)):
                difference = abs(self.active_accumulated_solar_current_readings_list[iggy] \
                               - self.standby_accumulated_solar_current_readings_list[iggy])
                feed_act_stby_difference.append(difference)

            # Next get the entry with the largest act/stby difference:
            max_value = max(feed_act_stby_difference)
            max_index = feed_act_stby_difference.index(max_value)
            tentative_feed_name = self.feed_name_total_list[max_index]

            # And finally, determine if the largest difference on the tentative feed exceeds
            # the act/stby differences on the remaining feeds by the minimum differential.
            # Hmmmm ... not easy to explain. Example:
            # - 3 feeds, say the act/stby measurements were:
            #   -> A, 32.521, 32.889 diff = 0.368
            #   -> B, 19.598, 32.272 diff = 12.674
            #   -> C, 33.421, 32.645 diff = 0.776
            # The largest difference is feed B = 12.674, and for 3 feeds, differential = 10.0amps, so:
            # 12.674 - 0.368 is less than 10.0 therefore NOT feed A
            # and 12.674 - 0.776 is less than 10.0 therefore NOT feed C
            # therefore, the tentative feed assignment is A

            for iggy in range(0, len(self.feed_name_total_list)):
                if iggy != max_index:
                    # Only check against other feeds, not yourself.
                    difference = abs(max_value - feed_act_stby_difference[iggy])
                    if difference > self.min_current_differential:
                        pass
                    else:
                        # Less than the min differential - feed detect failed.
                        # Might be due to too few inverters on the string, or
                        # some other disturbance in the network of inverters.
                        this_string = "REJECT CANDIDATE FEED %s (DIFF TOO NARROW: %s,%s)" % (tentative_feed_name,
                                                                                             feed_act_stby_difference[iggy],
                                                                                             difference)
                        self.log_info(this_string)
                        tentative_feed_name = DETECTED_FEED_YUCK
                        break

            self.decided_feed_for_this_string = tentative_feed_name

            this_string = "BASED ON SOLAR CURRENT READINGS, STRING ID %s IS LIKELY ON FEED %s" \
                                                                                  % (self.current_string_id_to_discover,
                                                                                     self.decided_feed_for_this_string)
            self.log_info(this_string)

            # ---------------------------------------------------------------------------------
            # 7. Determine which feed the current string is most likely connected to.
            try:
                self.string_assigned_feed_list[self.next_string_index - 1] = \
                                                 self.decided_feed_for_this_string
            except Exception: # as error:
                self.raise_and_clear_alarm(ALARM_ID_FD_FEED_INDEX_ERROR)

            if self.decided_feed_for_this_string == DETECTED_FEED_YUCK:
                # This string cannot be associated with a specific feed.
                # Try again.
                # The "index" is 1-based, so access the list as 0-based rank:
                self.string_assignment_accepted_list[self.next_string_index - 1] = False
            else:
                # Done. Break out of the while loop.
                self.string_assignment_accepted_list[self.next_string_index - 1] = True

            # end while ...

            # Update the status, feed_discovered fields in the fd_by_string table.
            if self.decided_feed_for_this_string == DETECTED_FEED_YUCK:
                self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_FAILED
                self.fd_by_string_feed_discovered = "FD_BY_STRING_FEED_DISCOVERED_NONE"
            else:
                self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS_SUCCESS
                self.fd_by_string_feed_discovered = self.decided_feed_for_this_string
            self.update_fd_by_string_status_and_feed()

            # -------------------------------------------------------------------------------------
            # 8. In this section, write a string to the log that will make it easy to pull data into
            #    an excel spread sheet. The string format should look something like this:
            #
            # EXCEL:272,8,7,A,474.366,473.619,B,483.985,485.343,C,478.428,493.515,0.747,1.358,15.087,
            # 15.087,C
            #
            # consisting of 18 fields (separated by coma):
            #
            #  1: string ID
            #  2: number of inverters on that string
            #  3: FD run # (example: number 7 out of 48 strings)
            #  loop over the number of feds:
            #      4: FEED NAME
            #      5: standby solar current accumulated reading
            #      6: active solar current accumulated reading
            #      7: FEED NAME
            #      8: standby solar current accumulated reading
            #      9: active solar current accumulated reading
            #     10: FEED NAME
            #     11: standby solar current accumulated reading
            #     12; active solar current accumulated reading
            # loop over the number of feeds:
            #     13: difference first feed: active - standby solar current readings
            #     14; difference second feed: active - standby solar current readings
            #     15; difference third feed: active - standby solar current readings
            # 16: largest solarCurrent difference
            # 17: declared string based on solarCurrent readings (could be: A, B, C, YUCK, NO_COMM)

            this_string = "EXCEL:"
            this_string += str(self.current_string_id_to_discover)
            this_string += "," + str(self.current_string_number_of_inverters)
            this_string += "," + str(self.next_string_index)
            for iggy in range(0, len(self.feed_name_total_list)):
                this_string += ",%s,%s,%s" % (self.feed_name_total_list[iggy],
                                              self.active_accumulated_solar_current_readings_list[iggy],
                                              self.standby_accumulated_solar_current_readings_list[iggy])

            for iggy in range(0, len(self.feed_name_total_list)):
                difference = abs(self.standby_accumulated_solar_current_readings_list[iggy]
                               - self.active_accumulated_solar_current_readings_list[iggy])
                difference = '{0:.2f}'.format(difference)
                this_string += "," + str(difference)
            this_string += "," + self.decided_feed_for_this_string

            # Now write this string to the log.
            self.log_info(this_string)
            # -------------------------------------------------------------------------------------

            # -------------------------------------------------------------------------------------
            # 9. Command those inverters in the current string to LEAVE the detect state (whereupon
            #    they will go to FD_WAIT_FOR_NORMAL state) via unicast. There is no need for any
            #    settling time.
            this_string = "Send inverters on string ID %s into FD_WAIT_FOR_NORMAL ..." % (self.current_string_id_to_discover)
            self.log_info(this_string)
            self.current_fd_command = GTW_TO_SG424_FD_LEAVE_DETECT_STATE_U16_TLV_TYPE
            self.current_fd_command_length = FD_LEAVE_DETECT_STATE_TLV_DATA_FIELD_LENGTH
            for each_ip in self.current_ip_list:
                self.current_ip_to_send = each_ip
                self.send_generic_unicast_fd_command()
            # Give plenty of time for the messages to be delivered and take effect.
            time.sleep(10)

            # INVERTER VALIDATION THAT THEY ARE IN THE WAIT_FOR_NORMAL_OP FD STATE:
            # Now that the inverters on that string have been sent to FD LEAVE state,
            # Verify that in fact, they are in that state.
            expected_fd_state = True
            for each_ip in self.current_ip_list:
                self.current_ip_to_send = each_ip
                (current_fd_state, inverter_ram_inv_ctrl) = self.get_inverter_feed_discovery_state()
                if current_fd_state != FEED_DISCOVERY_WAIT_FOR_NORMAL_OP_STATE:
                    expected_fd_state = False
                    this_string = "INVERTER %s NOT WAIT_FOR_NORMAL, BUT IN STATE %s (INV_CTRL = 0x%s)" % (self.current_ip_to_send,
                                                                                                          current_fd_state,
                                                                                                          hex(inverter_ram_inv_ctrl)[2:].zfill(2))
                    self.log_info(this_string)
            if expected_fd_state == True:
                this_string = "Validation: inverters on string ID %s in FD_WAIT_FOR_NORMAL ..." % (self.current_string_id_to_discover)
                self.log_info(this_string)

            this_string = "FEED DETECT STRING ID %s COMPLETE" % (self.current_string_id_to_discover)
            self.log_info(this_string)

        else:
            # There were no IP addresses associated with this string. This is not right,
            # quite inconsistent and unexpected. Unless the database was being updated
            # while this script was running!?
            self.raise_and_clear_alarm(ALARM_ID_FD_EMPTY_IP_LIST)

        # With this string completed, release lists that were allocated
        # but no longer required.
        del self.current_ip_list[:]

    def perform_all_feed_discovery(self):

        # Purpose: - create a "total list" of distinct string IDs from the inverters table.
        #            Although sequential, the string ID list may have "holes". Deal with it.
        # -------------------------------------------------------------------------------
        #
        # Preliminary step: command all inverters in the network to be "vewy vewy qwiet".
        #
        # Send a multicast "ENTER FD STBY MODE" command to all inverters, which sends the
        # inverters into "FEED STANDBY" mode.
        this_string = "SEND ALL INVERTERS TO FD STBY VIA MULTICAST ..."
        self.log_info(this_string)
        self.current_fd_command = GTW_TO_SG424_FD_ENTER_STBY_STATE_U16_TLV_TYPE
        self.current_fd_command_length = FD_ENTER_STBY_STATE_TLV_DATA_FIELD_LENGTH
        self.send_generic_multicast_fd_command()
        time.sleep(TIME_FOR_INVERTERS_TO_ENTER_FD_STANDBY)

        # Not sure if this is really necessary, but send the command one more time.
        self.current_fd_command = GTW_TO_SG424_FD_ENTER_STBY_STATE_U16_TLV_TYPE
        self.current_fd_command_length = FD_ENTER_STBY_STATE_TLV_DATA_FIELD_LENGTH
        self.send_generic_multicast_fd_command()

        # -------------------------------------------------------------------------------
        # For informational purpose, tracking, investigation, debugging, etc., get the
        # list of IP addresses for each string that feed discovery will operate on.
        self.display_ip_for_each_string()
        # -------------------------------------------------------------------------------

        # Set the status field in the fd_by_string table to in_queue.
        self.fd_by_string_status = FD_BY_STRING_STATUS_IN_QUEUE_STATUS
        self.set_all_fd_by_string_status_in_queue()

        # -------------------------------------------------------------------------------
        # Declare the following 2 multi-dimensional arrays:
        #
        #  - "standby solar meter readings" array
        #  - "active solar meter readings" array.
        #
        # Their sizes are based on the number of feeds in the system and the number
        # of readings to be taken for each cycle.
        #
        # Declare these 2 arrays to hold the result of feed assignment and the assigned feed name
        # for each string being discovered.
        self.string_assignment_accepted_list = [0 for x in range(len(self.string_id_total_list))]
        self.string_assigned_feed_list = [0 for x in range(len(self.string_id_total_list))]

        # Declare similar arrays for taking solar current readings. Initial testing has
        # shown that solarPower is unreliable for feed discovery. So add solar current
        # set of arrays.
        self.standby_solar_current_readings_list = \
                                         [[0 for y in range(NUMBER_OF_SOLAR_READS_FOR_DISCOVERY)] \
                                             for x in range(len(self.feed_name_total_list))]
        self.active_solar_current_readings_list = \
                                         [[0 for y in range(NUMBER_OF_SOLAR_READS_FOR_DISCOVERY)] \
                                             for x in range(len(self.feed_name_total_list))]

        self.standby_accumulated_solar_current_readings_list = \
                                                 [0 for x in range(len(self.feed_name_total_list))]
        self.active_accumulated_solar_current_readings_list = \
                                                 [0 for x in range(len(self.feed_name_total_list))]
        self.differential_solar_current_readings_list = \
                                                 [0 for x in range(len(self.feed_name_total_list))]

        self.string_assigned_feed_from_current_readings_list = \
                                                 [0 for x in range(len(self.string_id_total_list))]
        self.string_assignment_accepted_from_current_readings_list = \
                                                 [0 for x in range(len(self.string_id_total_list))]

        # -------------------------------------------------------------------------------
        # Perform feed discovery for all strings in the system. Before moving from one
        # string to the next, check if the process has been canceled (from the GUI).
        process_cancelled = False
        for each_string_id in self.string_id_total_list:
            self.current_string_id_to_discover = each_string_id
            self.next_string_index += 1
            # -------------------------------
            self.one_string_discovery()
            # -------------------------------

            if self.is_feed_discovery_canceled():
                # Feed discovery has been cancelled.
                this_string = "FD CANCELED"
                self.log_info(this_string)
                process_cancelled = True
                break

        # Wrap-up: prepare for FEED update in the database, and update within all inverters.
        #

        if process_cancelled:
           this_string = "FD PROCESS ... CANCELLED"
           self.log_info(this_string)
        else:

            # Display a summary of the overl results.
            this_string = "FEED DISCOVERY SUMMARY:"
            self.log_info(this_string)
            for iggy in range(0, len(self.string_id_total_list)):
                this_string = "string ID " + self.string_id_total_list[iggy] + " : "
                if self.string_assignment_accepted_list[iggy]:
                    # The discovery on this string was accepted:
                    this_string += "%s <- OK" % (self.string_assigned_feed_list[iggy])
                else:
                    # Discovery on this string failed...
                    this_string += " *failed*"
                self.log_info(this_string)

            # Make the final commits.
            this_string = "FINAL FEED DISCOVERY COMMITS:"
            self.log_info(this_string)
            for iggy in range(0, len(self.string_id_total_list)):

                if self.string_assignment_accepted_list[iggy]:
                    # The discovery on this string was accepted. Update the FEED NAME
                    # in the inverters table that correspond to the string ID.
                    self.this_string_id = self.string_id_total_list[iggy]
                    self.this_feed_name = self.string_assigned_feed_list[iggy]
                    self.update_feed_db_component()

                    # Now update the inverters on this string ID:
                    self.inverters_feed_name_update()

                else:
                    # Disco very on this string failed...
                    this_string = "STRING ID %s DISCOVERY FAILED - NO UPDATES" % (self.string_id_total_list[iggy])
                    self.log_info(this_string)

        # Done done done.

        # -------------------------------------------------------------------------------
        #
        # FINALLY: before process termination, command ALL inverters back to normal operation.
        #
        # Send a multicast "EXIT FD STBY MODE" command to all inverters, which returns the
        # inverters back into standard operation.
        self.current_fd_command = GTW_TO_SG424_FD_EXIT_U16_TLV_TYPE
        self.current_fd_command_length = FD_EXIT_TLV_DATA_FIELD_LENGTH
        self.send_generic_multicast_fd_command()
        # Send it one more time.
        self.current_fd_command = GTW_TO_SG424_FD_EXIT_U16_TLV_TYPE
        self.current_fd_command_length = FD_EXIT_TLV_DATA_FIELD_LENGTH
        self.send_generic_multicast_fd_command()
        # -------------------------------------------------------------------------------

    def build_feed_name_list(self):
        # Purpose: create a few lists based on:
        #            - feed name (A, B, C ...)
        #            - accumulated solar meter readings
        #            - averaged solar meter readings
        #            - solar meter readings differences between active and standby
        #              feed detect mode

        self.feed_name_total_list = []

        # Extract all rows from the gateway meter table.
        sql = "SELECT feed_name FROM power_regulators WHERE has_solar=1 GROUP BY feed_name ORDER BY feed_name"
        feed_name_list = prepared_act_on_database(FETCH_ALL, sql, ())
        # Construct the list of feed names.
        for feed_name in feed_name_list:
            this_feed_name = str(feed_name['feed_name'])
            self.feed_name_total_list.append(this_feed_name)

    def is_discovery_command_ok(self):
        proceed_with_discovery = False
        select_string = "SELECT * FROM %s;" % FEED_DISCOVERY_TABLE
        feed_discovery_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        self.discover_command = feed_discovery_row[FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME]

        if self.discover_command == FEED_DISCOVERY_COMMAND_START_DISCOVERY:
            # The command indicates that the process can proceed. So good to go.
            proceed_with_discovery = True

        return proceed_with_discovery

    def set_discovery_command(self):
        # Set the feed discovery command.
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME, "%s")
        set_args = (self.discover_command, )
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def set_discovery_start_time(self):
        # Set the feed discovery start time.
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_START_TIME_COLUMN_NAME, "%s")
        set_args = (self.this_current_date_and_time, )
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def set_discovery_completion_time(self):

        # Set the feed discovery completion time.
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_COMPLETION_TIME_COLUMN_NAME, "%s")
        set_args = (self.this_current_date_and_time, )
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def set_night_mode_only(self):
        # Purpose: Feed discovery could be performed during day time when the inverters
        #          have solar power capability, or at night. When at night, it is intended
        #          to send the inverters into "feed detect" mode, whereupon they will draw
        #          energy rather than produce it.
        #
        #          Currently, only nigHt mode of feed discovery is implemented.

        # Set the mode to night.
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_MODE_COLUMN_NAME, "%s")
        set_args = (self.feed_discovery_mode, )
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def increment_discovery_runs(self):

        # Read and increment the number of discovery runs:
        select_string = "SELECT * FROM %s;" % FEED_DISCOVERY_TABLE
        runs_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        runs_count = str(runs_row[FEED_DISCOVERY_TBL_RUNS_COLUMN_NAME])
        if runs_count.isdigit():
            # It's a digit. Increment it.
            current_run_count = int(runs_count) + 1
        else:
            # Runs_count is not a digit, probably NULL. Set it to 1, since
            # presumably, this is the 1st time that feed disco very is run.
            current_run_count = 1
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_RUNS_COLUMN_NAME, "%s")
        set_args = (current_run_count, )
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def update_db_number_of_strings(self):
        # Purpose: update the "number of strings" to discover in the feed discovery table.

        number_of_strings_to_discover = len(self.string_id_total_list)
        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_NUMBER_OF_STRINGS_COLUMN_NAME, "%s")
        set_args = (number_of_strings_to_discover,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def update_db_current_string(self):
        # Purpose: update the "current string" in the feed discovery table.
        #
        # TODO: now that FD BY STRING/GUI capability is available, the current_string
        #       column in the feed_discovery_control table is no longer required.
        #       This function can now be removed, and the current_string column can be
        #       deleted from the feed_discovery_control table.

        set_string = "UPDATE %s SET %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                 FEED_DISCOVERY_TBL_CURRENT_STRING_COLUMN_NAME, "%s")
        set_args = (self.current_string_id_to_discover,)
        prepared_act_on_database(EXECUTE, set_string, set_args)

    def start_feed_discovery(self):
        # Purpose: determine the number of strings in the database, and for each string,
        #          perform a "feed detect"ocess. Essentially:
        #
        #            - set all strings in the network into "feed detect standby" mode
        #            - for each string in the system:
        #                - send inverters into feed detect active mode
        #                - take a bunch of solar meter readings from each phase
        #                - determine which feed had the highest power draw

        self.this_current_date_and_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.start_utc_time = int(time.time())

        this_string = "***FEED DISCOVERY STARTED (PID %s): %s (UTC = %s)" % (str(os.getpid()),
                                                                             str(self.this_current_date_and_time),
                                                                             str(self.start_utc_time))
        self.log_info(this_string)

        # -----------------------------------------------------------------------------------------
        #

        # NIGHT MODE: currently, only night mode is supported. So not only is it not
        #             checked, it is unilaterally set to night mode, until the day comes
        #             when this script is updated to choose either day or night mode of
        #             feed discovery.
        self.feed_discovery_mode = FEED_DISCOVERY_MODE_NIGHT
        self.set_night_mode_only()
        # -----------------------------------------------------------------------------------------

        if self.is_discovery_command_ok():
            # Set the discovery command to "in progress", and the start time of this
            # process. Also set the completion time to some null value.
            self.discover_command = FEED_DISCOVERY_COMMAND_DISCOVERY_IN_PROGRESS
            self.set_discovery_command()
            self.increment_discovery_runs()
            self.set_discovery_start_time()

            # Re-use the current date/time variable, since it is not used elsewhere.
            self.this_current_date_and_time = "1970-01-01 00:00:00"
            self.set_discovery_completion_time()

            # Preliminary preparation: read the inverters table, and for each entry:
            #
            #     - if stringID is not a digit, then set it to 0
            #     - if stringID is 0, then set string position to 0
            #
            if self.is_inverters_table_ok_for_fd():

                # Gather up the list of strings for which the feeds are to be discovered.
                self.build_string_discovery_list()

                # Based on the numbe of strings in the system, determine the
                # minimum acceptable current differential for comparisson
                # between "standby" and "active" feed detect states.
                self.set_min_current_differential()

                # Is there 1 or more strings whose feed is to be discovered?
                self.string_id_list_length = len(self.string_id_total_list)
                self.next_string_index = 0
                if not self.string_id_list_length == 0:
                    # The list is not zero. There is at least 1 string with a feed to be discovered.
                    # Since this process will be reading from the gateway (solar) meters, verify
                    # that meters have been configured.
                    this_string = "FD to be performed on %s inverter strings" % (self.string_id_list_length)
                    self.log_info(this_string)
                    self.update_db_number_of_strings()

                    self.build_feed_name_list()
                    feed_name_list_length = len(self.feed_name_total_list)
                    if not feed_name_list_length == 0:
                        # There is a string list and a feed name list.
                        # Go ahead and perform the actual feed discovery process.

                        # -------------------------------
                        self.perform_all_feed_discovery()
                        # -------------------------------
                    else:
                        # The feed name (or gateway meters) list was empty.
                        this_string = "NXO HEAP table empty, no discovery performed"
                        self.log_info(this_string)

                else:
                    # The list was empty.
                    this_string = "string ID list empty, no discovery performed"
                    self.log_info(this_string)

            else:
                # The inverters table had inconsistent data that would make feed discovery
                # unreliable.
                this_string = "Inverters table inconsistent, feed detect process abandoned"
                self.log_info(this_string)

        else:
            # Discovery command was not "start the discovery". This is a database issue,
            # either the monitor is not running, or perhaps a previous discovery process
            # was inadvertantly stopped, and cleanup is required.
            this_string = "discovery command not %s, no discovery performed" % (FEED_DISCOVERY_COMMAND_START_DISCOVERY)
            self.log_info(this_string)

        # There's nothing left to do. So say it, then do it.
        self.this_current_date_and_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.complete_utc_time = int(time.time())
        total_process_time = self.complete_utc_time - self.start_utc_time
        this_string = "***FEED DISCOVERY DONE AT %s (UTC = %s) (TOTAL TIME: %s)" % (str(self.this_current_date_and_time),
                                                                                    str(self.complete_utc_time),
                                                                                    total_process_time)
        self.log_info(this_string)

        # Set the discovery command, and the end time of this process.
        self.discover_command = FEED_DISCOVERY_COMMAND_NO_COMMAND
        self.set_discovery_command()
        self.set_discovery_completion_time()

        # With the feed discovery processing exiting here, right now, tout-de-suite,
        # have the process itself exit cleanly, to prevent a lingering zombie process.
        os._exit(1)

if __name__ == '__main__':
    FeedDiscoveryProcess()
