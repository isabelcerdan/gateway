#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
@module: DbUpdate.py

@description: DbUpdate is a CLI interface to the Gateway Database.  It allow
              script writters to modify a single column in an existing table
              in the database, optionally using a 'where' clause.

@author:     steve

@copyright:  2018 Apparent Energy Inc. All rights reserved.
'''

import sys
import os
from contextlib import closing
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
import argparse


__all__ = []
__version__ = 0.1
__date__ = '2018-07-02'
__updated__ = '2018-07-02'

logger = setup_logger('DbUpdate')

parser = argparse.ArgumentParser(description='CLI for MySQL table updates')

DbUpdate_command_input_dict = \
    {
        '--usage': ['DbUpdate --table <table name> --column <column name> [--where-column <column name> --where-value <value>]', 0],
        '--table': ['Database table name', 1],
        '--column': ['Set column name', 1],
        '--column-value': ['Set column value', 1],
        '--where-column-1': ['Where column name 1', 1],
        '--where-value-1': ['Where column value 1', 1],
        '--where-column-2': ['Where column name 2', 1],
        '--where-value-2': ['Where column value 2', 1]
    }

def prepared_act_on_database(action, command, args):
    try:
        with closing(MyDB.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                  db=DB_NAME, cursorclass=my_cursor.DictCursor)) as connection:
            with connection as cursor:
                cursor.execute(command, args)
                if action == FETCH_ONE:
                    return cursor.fetchone()
                elif action == FETCH_ALL:
                    return cursor.fetchall()
                return True
    except Exception as error:
        critical_string = "act_on_database failed. \nAction: %s \nString: %s \nArgs: %s - %s" \
                          % (action, command, args, repr(error))
        logger.exception(critical_string)
        raise Exception

def get_args():
    global parser 

    for command, articles in DbUpdate_command_input_dict.iteritems():
        if articles[1] < 1:
            parser.add_argument(command,
                                action = 'store_true',
                                help=articles[0])
        else:
            parser.add_argument(command,
                                action="store",
                                nargs=1,
                                help=articles[0])
    return parser.parse_args()

'''
Time to figure out what the user wants...
'''
clargs = get_args()
clicmd = vars(clargs)

if clicmd['usage'] is True:
    print 'DbUpdate --table <table name> --column <column name> --column-value <value>' \
          '[--where-column <column name> --where-value <value>]'
    exit(0)

if clicmd['table'] is None:
    logger.error('Missing table name parameter')
    print('Missing table name parameter')
    exit(1)

if clicmd['column'] is None:
    logger.error('Missing column name parameter')
    print('Missing column name parameter')
    exit(2)

if clicmd['column_value'] is None:
    logger.error('Missing column value parameter')
    print('Missing column value parameter')
    exit(3)

if clicmd['where_column_1'] is None:
    sql = 'UPDATE %s SET %s = %s;' % (clicmd['table'][0], clicmd['column'][0], "%s")
    args = [clicmd['column_value'][0]]
elif clicmd['where_column_2'] is None:
    sql = 'UPDATE %s SET %s = %s WHERE %s = %s;' % (clicmd['table'][0], clicmd['column'][0], "%s",
                                                    clicmd['where_column_1'][0], "%s")
    args = [clicmd['column_value'][0], clicmd['where_value_1'][0]]
else:
    sql = 'UPDATE %s SET %s = %s WHERE %s = %s AND %s = %s;' % (clicmd['table'][0],
                                                                clicmd['column'][0],
                                                                "%s",
                                                                clicmd['where_column_1'][0],
                                                                "%s",
                                                                clicmd['where_column_2'][0],
                                                                "%s")
    args = [clicmd['column_value'][0], clicmd['where_value_1'][0], clicmd['where_value_2'][0]]

try:
    print sql, args
    prepared_act_on_database(EXECUTE, sql, args)
except Exception:
    logger.exception('Database action failed')
    print 'Database action failed'
    exit(4)

exit(0)
    