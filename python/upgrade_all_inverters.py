#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import logging
import struct
import logging.handlers
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
from multiprocessing.dummy import Pool as ThreadPool
# from multiprocessing import Pool as ProcPool
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from upgrade_one_inverter import *
import setproctitle

from lib.db import prepared_act_on_database

from lib.logging_setup import *

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_UPG_ALL_INVALID_COMMAND_ERROR, \
                                  ALARM_ID_UPG_ALL_MISSING_LEGACY_BOOT_ERROR, \
                                  ALARM_ID_UPG_ALL_THREAD_POOL_FAILURE, \
                                  ALARM_ID_UPG_ALL_JOIN_FAILURE, \
                                  ALARM_ID_UPG_ALL_MISSING_FILE_ERROR

logger = setup_logger('upgrade_all_inverters')

def instantiate_upgrade_all_inverters_process():
    setproctitle.setproctitle('%s-upgrade-alld' % GATEWAY_PROCESS_PREFIX)
    UpgradeAllInvertersProcess()
    
    
class UpgradeAllInvertersProcess(object):
    def __init__(self, _=None):
        self.total_inverter_inverters_id = []
        self.total_inverter_percent_progress = []
        self.number_inverters_to_upgrade = 0
        self.current_date_and_time = None
        self.start_utc_time = None
        self.end_utc_time = 0
        self.my_logger = None
        self.handler = None

        self.one_inverter_logger = None
        self.one_inverter_handler = None

        self.terminal_debug_enabled = False
        self.basic_startup_string = ""

        # From the in verter_upgrad table:
        self.upgrade_id = 0
        self.upgrade_directory = None
        self.upgrade_file_name = None
        self.upgrade_path_and_file_name = None
        self.bootloader_upgrade_directory = None
        self.bootloader_upgrade_file_name = None
        self.bootloader_path_and_file_name = None
        self.latest_boot_loader_version = 0
        self.post_rev6_prelim_file_name = None
        self.pre_rev6_prelim_file_name = None

        self.upgrade_command = None

        # With data initialized, get to work:
        self.setup_logger()

        # ----------------------------------------
        self.upgrade_all_inverters()
        # ----------------------------------------

    def setup_logger(self):
        self.my_logger = setup_logger("UPG_ALL")

        # -----------------------------------------
        # Each instance of the upgrade_one_inverter script will use the following
        # logging handler:
        self.one_inverter_logger = setup_logger("UPG_ONE")

    def log_info(self, message, error=False):
        self.my_logger.info(message, exc_info=(error != False))

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)


    def verify_upgrade_specifications(self):
        # Verify that everything is in place to proceed with an upgrade:
        #
        # - upgrade command in the inverter_upgrade table is correct
        # - existence of the specified hex upgrade file
        # - existence of the boot loader file in case a full upgrade
        upgrade_specifications = False

        # Before starting and doing anything useful, ensure inverter upgrade can proceed.
        inverter_impediment_string = gu.any_impediment_to_inverter_upgrade()
        if inverter_impediment_string:
            self.basic_startup_string = "***UPGRADE PROCESS NOT STARTED DUE TO IMPEDIMENT (%s) (%s - UTC = %s)" \
                                        % (inverter_impediment_string,
                                           self.current_date_and_time,
                                           self.start_utc_time)
            self.log_info(self.basic_startup_string)
            this_update_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_UPGRADE_TABLE,
                                                             INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s")
            this_upgrade_args = (INVERTER_UPGRADE_COMMAND_NO_COMMAND,)
            prepared_act_on_database(EXECUTE, this_update_string, this_upgrade_args)
            return upgrade_specifications

        self.basic_startup_string = "***UPGRADE PROCESS START AT %s (UTC = %s)"\
                                    % (str(self.current_date_and_time),
                                       str(self.start_utc_time))

        # Read the inverter_upgrade table and extract the command field.
        select_inverter_upgrade_string = "SELECT * FROM %s;" % DB_INVERTER_UPGRADE_TABLE
        inverter_upgrade_row = prepared_act_on_database(FETCH_ONE, select_inverter_upgrade_string, ())

        self.upgrade_id = inverter_upgrade_row[INVERTER_UPGRADE_TABLE_UPGRADE_ID_COLUMN_NAME]
        self.upgrade_directory = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_SOFTWARE_DIRECTORY_COLUMN_NAME])
        self.upgrade_file_name = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_FILE_NAME_COLUMN_NAME])
        self.bootloader_upgrade_directory = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_BOOTLOADER_DIRECTORY_COLUMN_NAME])
        self.bootloader_upgrade_file_name = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_BOOTLOADER_FILE_NAME_COLUMN_NAME])
        self.latest_boot_loader_version = inverter_upgrade_row[INVERTER_UPGRADE_TABLE_BOOT_REV_COLUMN_NAME]
        self.post_rev6_prelim_file_name = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_POST_REV6_PRELIM_UPGRADE_FILE])
        self.pre_rev6_prelim_file_name = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_PRE_REV6_PRELIM_UPGRADE_FILE])
        self.upgrade_command = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME])

        self.basic_startup_string += ", FILE: %s" % self.upgrade_file_name

        # First check if the command field is set to "start an upgrade".
        # If it's anything else, something is wrong.
        if self.upgrade_command != INVERTER_UPGRADE_COMMAND_START_UPGRADE:
            # The command field does not indicate an upgrade to be started.
            # Why was this script called? The upgrade command field may
            # be corrupted.
            self.raise_and_clear_alarm(ALARM_ID_UPG_ALL_INVALID_COMMAND_ERROR)
            return upgrade_specifications

        else:
            # Increment the upgrade ID and write "in progress" in the upgrade command field name.
            self.upgrade_id += 1
            update_string = "UPDATE %s SET %s = %s, %s = %s, %s = %s;" \
                            % (DB_INVERTER_UPGRADE_TABLE,
                               INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s",
                               INVERTER_UPGRADE_TABLE_UPLOAD_TIME_COLUMN_NAME, "%s",
                               INVERTER_UPGRADE_TABLE_UPGRADE_ID_COLUMN_NAME, "%s")
            update_args = (INVERTER_UPGRADE_COMMAND_UPGRADE_IN_PROGRESS, self.current_date_and_time, self.upgrade_id)
            prepared_act_on_database(EXECUTE, update_string, update_args)

        # Construct the complete path/file name for the upgrade file AND bootloader.
        self.upgrade_path_and_file_name = self.upgrade_directory + "/" + self.upgrade_file_name
        self.bootloader_path_and_file_name = ""\
                                           + self.bootloader_upgrade_directory\
                                           + "/"\
                                           + self.bootloader_upgrade_file_name

        # Check existence of upgrade file. Bail if not found.
        if not os.path.isfile(self.upgrade_path_and_file_name):
            # Upgrade file not found or does not exist. Cannot upgrade.
            self.raise_and_clear_alarm(ALARM_ID_UPG_ALL_MISSING_FILE_ERROR)

            # Reset the upgrade command field back to "no command".
            update_string = "update %s set %s = %s;" % (DB_INVERTER_UPGRADE_TABLE,
                                                        INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME,
                                                        "%s")

            update_args = (INVERTER_UPGRADE_COMMAND_NO_COMMAND, )
            prepared_act_on_database(EXECUTE, update_string, update_args)

        else:
            # Good to go. Update the upload_time and upgrade_command fields
            # in the inverter_upgrade table, and increment the upgrade_id as well.
            #
            # One or more inverters may need to be updated from legacy code. If the
            # bootCombo file does not exist, note this in the log, but continue with
            # the upgrade anyway: upgrades of such inverters, if any, will fail.
            #
            if os.path.isfile(self.bootloader_path_and_file_name):
                # If there are any inverters running legacy code, then they too, are to be
                # upgraded, including the boot loader. In preparation for this possibility,
                # copy the legacy bootloader upgrade file to this same directory.
                this_os_cp_command = "sudo cp " + self.bootloader_path_and_file_name + " ."
                os.system(this_os_cp_command)

                # To support boot loader upgrade of inverters running R31 or earlier of
                # the bootloader, copy the companion bootCombo file (same file name but
                # without LEGACY_ in the file name):
                companion_file_name = self.bootloader_upgrade_file_name.replace('LEGACY_', '')
                companion_boot_combo = ""\
                                     + self.bootloader_upgrade_directory\
                                     + "/"\
                                     + companion_file_name
                this_os_cp_command = "sudo cp " + companion_boot_combo + " ."
                os.system(this_os_cp_command)

            else:
                # Legacy bootloader upgrade file not found or does not exist.
                # Log it as a warning, but continue nevertheless, as it may not
                # be needed.
                self.raise_and_clear_alarm(ALARM_ID_UPG_ALL_MISSING_LEGACY_BOOT_ERROR)

            # Unfortunately, despite the fact that the upgrade file exists, it must be
            # located in the same directory in which the script is running, otherwise
            # the TFTP process will not work
            # So copy the file from its current location to the python directory.
            #
            # NOTE: this upgrade cannot go ahead if the primary application upgrade
            #       file is missing, its existence was verified just above. On the
            #       other hand, this upgrade can proceed if the bootCombo is missing,
            #       because is not yet known if it will be needed.
            this_os_cp_command = "sudo cp " + self.upgrade_path_and_file_name + " ."
            os.system(this_os_cp_command)

            # Finally ... in case the inverter is running some form of legacy code, or an old boot loader,
            # copy the 2 "preliminary" upgrade files to the running directory. Both files are in the
            # boot loader upgrade directory (same as location of boot loader file).
            this_os_cp_command = "sudo cp %s/%s ." % (self.bootloader_upgrade_directory, self.post_rev6_prelim_file_name)
            os.system(this_os_cp_command)
            this_os_cp_command = "sudo cp %s/%s ." % (self.bootloader_upgrade_directory, self.pre_rev6_prelim_file_name)
            os.system(this_os_cp_command)

            upgrade_specifications = True

        return upgrade_specifications

    # -----------------------------------------------------------------------------
    #
    #
    # Name: build_total_inverter_upgrade_list
    #
    # Purpose: create a "total list" of inverters_id for all inverters to be upgraded,
    #          where this total list is ordered on the basis of string position:
    #
    #          - create an inverters_id and string position lists from the batabase for all
    #            rows whose update status is "upgrade waiting".
    #          - zip these 2 lists into a list of tuples
    #          - order this tuple list from high-to-low on the basis of string position
    #          - copy the inverters_id from this ordered tuple list into the "total list"
    #            of inverters_id.

    def build_total_inverter_upgrade_list(self):

        inverters_id_list = []
        string_position_list = []
        self.total_inverter_inverters_id = []
        self.total_inverter_percent_progress = []

        # Extract all IPs from the inverters table to be upgraded.
        selection_criteria = "%s, %s, %s, %s" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                 INVERTERS_TABLE_COMM_COLUMN_NAME,
                                                 INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME,
                                                 INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
        select_ip_string = "SELECT %s FROM %s ORDER BY %s DESC;" % (selection_criteria, DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
        inverters_ip = None
        inverters_ip = prepared_act_on_database(FETCH_ALL, select_ip_string, ())

        for each_inverter in inverters_ip:
            upgrade_status = each_inverter[INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME]
            inverter_comm = each_inverter[INVERTERS_TABLE_COMM_COLUMN_NAME]

            if not inverter_comm == INVERTERS_TABLE_COMM_STRING_MGI_220:

                if upgrade_status == INVERTERS_UPGRADE_STATUS_UPGRADE_WAITING:

                    # To be upgraded. Set its upgrade_status to in_queue, progress to 0.
                    inverters_id_list.append(each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])
                    string_position_list.append(each_inverter[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME], )
                    set_upgrade_args = (INVERTERS_UPGRADE_STATUS_IN_QUEUE, 0,
                                        each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])
                else:
                    # Not waiting. Skip over this entry. Set its status to no_action.
                    set_upgrade_args = (INVERTERS_UPGRADE_STATUS_NO_ACTION, 0,
                                        each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])

                set_upgrade_string = "UPDATE %s SET %s = %s, %s = %s  where %s = %s;"\
                                     % (DB_INVERTER_NXO_TABLE,
                                        INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                        INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                        INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
                prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

            else:
                # Most likely it is "no-action", or perhaps some other upgrade_status left-over
                # from a previous upgrade. Or might have been an MGI-220. Set its upgrade_status
                # to no_action and progress to 0.
                this_string = "SKIPPING OVER PRESUMED MGI-220, inverters_id = " \
                              + str(each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])
                self.log_info(this_string)
                set_upgrade_string = "UPDATE %s SET %s = %s, %s = %s  where %s=%s;" \
                                     % (DB_INVERTER_NXO_TABLE,
                                        INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                        INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                        INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
                set_upgrade_vars = (INVERTERS_UPGRADE_STATUS_NO_ACTION, 0,
                                    each_inverter[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])
                prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_vars)

        # The 2 inverters_id and stringPosition lists have been created, paired as they were
        # extracted from the database. Zip both lists in a tupled list, with string position
        # as the 1st element of each tuple.
        zipped_id_and_string_position = zip(string_position_list, inverters_id_list)

        # Sort this tuple list from high to low.
        zipped_id_and_string_position.sort(reverse=True)

        # Finally, append the inverters_id from the sorted tuple list into the total list
        # (and don't forget about the percentage of progress list, initialized to zero):
        list_length = len(inverters_id_list)
        for iggy in range(0, list_length):
            self.total_inverter_inverters_id.append(zipped_id_and_string_position[iggy][1])
            self.total_inverter_percent_progress.append(0)

    #
    #
    # END OF build_total_inverter_upgrade_list
    #
    #
    # -----------------------------------------------------------------------------
    #
    #
    # Name: upgrade_all_inverters
    #
    # Purpose: upgrade all inverters chosen by the user:
    #
    # 1. validate the specifications for upgrade
    # 2. create a list of all inverters to be upgraded on basis of inverters_id
    # 3. sort the list based on decreasing order of the string position
    # 4. upgrade those inverters
    # 5. do a few more times:
    #    5.1 get all those inverters that failed the upgrade
    #    5.2 re-create the ordered list
    #    5.3 perform the upgrade on those inverters
    #

    def upgrade_all_inverters(self):

        self.current_date_and_time = time.strftime("%Y-%m-%d %H:%M")
        self.start_utc_time = int(time.time())

        # This script was invoked by the monitor to perform an upgrade of a group of
        # selected inverters. Verify that it has been properly commanded to do so.
        if self.verify_upgrade_specifications():

            # The upgrade specifications are good. Start the upgrade.

            self.build_total_inverter_upgrade_list()
            self.number_inverters_to_upgrade = len(self.total_inverter_inverters_id)
            this_string = self.basic_startup_string + " -> UPGRADE "\
                        + str(self.number_inverters_to_upgrade)\
                        + " INVERTERS"
            self.log_info(this_string)

            if self.number_inverters_to_upgrade > 0:

                # -----------------------------------------------------------------------
                #
                # The ordered upgrade list has been populated. This section creates a small
                # pool of resources and launches the "upgrade just this one inverter" script
                # for each inverter in the upgrade list. Once the pool of scripts for the
                # upgrade of each inverter is running,
                arg_list = []
                for iggy in range(0, self.number_inverters_to_upgrade):
                    argument = (self.total_inverter_inverters_id[iggy],
                                self.upgrade_path_and_file_name,
                                self.bootloader_path_and_file_name,
                                self.latest_boot_loader_version,
                                self.one_inverter_logger)
                    arg_list.append(argument)

                # Allocate the pool of resources:
                try:
                    upgrade_script_pool = ThreadPool(min(self.number_inverters_to_upgrade, MAX_NUMBER_CONCURRENT_UPGRADES))
                except Exception as error:
                    self.raise_and_clear_alarm(ALARM_ID_UPG_ALL_THREAD_POOL_FAILURE)
                else:

                    # Launch the processes (use map_async, not apply_async):
                    upgrade_script_pool.map(UpgradeOneInverterScript, arg_list)

                    # cleanup:
                    try:
                        upgrade_script_pool.close()
                        upgrade_script_pool.join()
                    except Exception as error:
                        self.raise_and_clear_alarm(ALARM_ID_UPG_ALL_JOIN_FAILURE)

            # Get here because there are no more inverters to upgrade. Or there were none.
            # The upgrade completed normally (with or without failures), or may have been
            # cancelled by the user. In any event, this process is done. All that remains
            # is to write a completion notice to the log, delete the upgrade files that
            # were copied to the directory in which this script was launched, and clean up
            # the database.
            self.current_date_and_time = time.strftime("%Y-%m-%d %H:%M")
            self.end_utc_time = int(time.time())
            #  self.start_utc_time = int(time.time())
            elapsed_seconds = self.end_utc_time - self.start_utc_time

            # Were there any failed upgrades?
            failure_count = 0
            select_string = "SELECT COUNT(*) FROM %s where %s = %s;" \
                            % (DB_INVERTER_NXO_TABLE,
                               INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s")
            select_args = (INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED, )
            failure_count = prepared_act_on_database(FETCH_ROW_COUNT, select_string, select_args)

            this_string = "***UPGRADE PROCESS COMPLETED AT %s (UTC = %s ELAPSED = %s) -> %s UPGRADE FAILURES***"\
                          % (self.current_date_and_time,
                             self.end_utc_time,
                             elapsed_seconds,
                             failure_count)
            self.log_info(this_string)

            # Delete the upgrade files (application file, bootCombo, companion bootCombo, preliminary).
            companion_boot_combo = self.bootloader_upgrade_file_name.replace('LEGACY_', '')
            this_os_rm_command = "sudo rm %s %s %s %s %s" % (self.upgrade_file_name,
                                                             self.bootloader_upgrade_file_name,
                                                             companion_boot_combo,
                                                             self.post_rev6_prelim_file_name,
                                                             self.pre_rev6_prelim_file_name)
            os.system(this_os_rm_command)

            # Process is complete. Write to the inverter upgrade table.
            # Only the upgrade_command field is updated.
            this_update_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_UPGRADE_TABLE,
                                                             INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s")
            this_upgrade_args = (INVERTER_UPGRADE_COMMAND_NO_COMMAND, )
            prepared_act_on_database(EXECUTE, this_update_string, this_upgrade_args)

        else:
            # Upgrade specification incorrect or impediment. Log written, nothing to do here.
            pass

        # The logging handlers are no longer needed.
        self.my_logger.removeHandler(self.handler)
        self.one_inverter_logger.removeHandler(self.one_inverter_handler)

        # There's nothing left to do. So do it.

    #
    #
    # END OF upgrade_all_inverters
    # -----------------------------------------------------------------------------

def main():
    UpgradeAllInvertersProcess()
    return

if __name__ == '__main__':
    main()
    exit(0)
