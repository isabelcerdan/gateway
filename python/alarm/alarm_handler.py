import logging
import alarm

class AlarmHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        if hasattr(record, 'alarm_id'):
            a = alarm.Alarm.find(record.alarm_id)
            a.occurrence()
