# encoding: utf-8
"""
  Description: Alarm model to manage gateway alarms

  To define a new alarm:
  
  1. Add a new alarm id in alarm_constants.py.
  2. Add a new Alarm.create statement in python/alarms/__init__.py.

  Copyright Apparent Inc. 2017-2018

@author: patrice de muizon
"""

import time
from lib.logging_setup import *

from lib.db import prepared_act_on_database
from lib.gateway_constants.DBConstants import *
from alarm_constants import *
from alarm_subscriber import AlarmSubscriber
from event.event_constants import *


class Alarm(object):
    logger = setup_logger(__name__)

    def __init__(self, **kwargs):
        self.id = kwargs.get(DB_ALARMS_TBL_ID_COLUMN_NAME, 0)
        self.name = kwargs.get(DB_ALARMS_TBL_NAME_COLUMN_NAME, None)
        self.severity = kwargs.get(DB_ALARMS_TBL_SEVERITY_COLUMN_NAME, None)
        self.strategy = kwargs.get(DB_ALARMS_TBL_STRATEGY_COLUMN_NAME, None)
        self.er_alarm = kwargs.get(DB_ALARMS_TBL_ER_ALARM_COLUMN_NAME, 0) == 0
        self.defined_count = kwargs.get(DB_ALARMS_TBL_DEFINED_COUNT_COLUMN_NAME, 0)
        self.defined_duration = kwargs.get(DB_ALARMS_TBL_DEFINED_DURATION_COLUMN_NAME, 0)
        self.current_count = kwargs.get(DB_ALARMS_TBL_CURRENT_COUNT_COLUMN_NAME, 0)
        self.current_begin = kwargs.get(DB_ALARMS_TBL_CURRENT_BEGIN_COLUMN_NAME, None)
        self.raised = kwargs.get(DB_ALARMS_TBL_RAISED_COLUMN_NAME, 0) == 1
        self.clear_pending = kwargs.get(DB_ALARMS_TBL_CLEAR_PENDING_COLUMN_NAME, 0) == 1
        self.last_raised = kwargs.get(DB_ALARMS_TBL_LAST_RAISED_COLUMN_NAME, None)
        self.debounce_duration = kwargs.get(DB_ALARMS_TBL_DEBOUNCE_DURATION_COLUMN_NAME, 900)

    def save(self):
        if not self.find(self.id):
            sql = "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % \
                     (DB_ALARMS_TABLE, DB_ALARMS_TBL_ID_COLUMN_NAME, DB_ALARMS_TBL_NAME_COLUMN_NAME,
                      DB_ALARMS_TBL_SEVERITY_COLUMN_NAME, DB_ALARMS_TBL_STRATEGY_COLUMN_NAME,
                      DB_ALARMS_TBL_ER_ALARM_COLUMN_NAME, DB_ALARMS_TBL_DEFINED_COUNT_COLUMN_NAME,
                      DB_ALARMS_TBL_DEFINED_DURATION_COLUMN_NAME, DB_ALARMS_TBL_LAST_RAISED_COLUMN_NAME,
                      DB_ALARMS_TBL_DEBOUNCE_DURATION_COLUMN_NAME,
                      "%s", "%s",  "%s", "%s", "%s", "%s", "%s", "%s", "%s")
            args = [self.id, self.name, self.severity, self.strategy, self.er_alarm, self.defined_count,
                    self.defined_duration, self.last_raised, self.debounce_duration]
        else:
            sql = "UPDATE %s SET %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s WHERE %s = %s" % \
                    (DB_ALARMS_TABLE, DB_ALARMS_TBL_CURRENT_COUNT_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_CURRENT_BEGIN_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_RAISED_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_CLEAR_PENDING_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_LAST_RAISED_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_DEBOUNCE_DURATION_COLUMN_NAME, "%s",
                     DB_ALARMS_TBL_ID_COLUMN_NAME, "%s")
            args = [self.current_count, self.current_begin, self.raised, self.clear_pending,
                    self.last_raised, self.debounce_duration, self.id]

        return prepared_act_on_database(EXECUTE, sql, args)

    def _occurrence(self):

        # increment incident count
        self.current_count += 1

        # if this is the first incident, record begin time
        if self.current_count == 1:
            self.current_begin = time.time()

        # check if alarm has been raised by this occurrence
        self.raised = self.check_raised()

        # if alarm was raised, save last raised time and reset pending clears
        if self.raised:
            self.last_raised = int(time.time())
            self.clear_pending = False

        # if we've reached the defined count for this alarm
        if self.current_count >= self.defined_count:

            # reset counters to start detecting future errors
            self.current_count = 0
            self.current_begin = None

        # save changes to database
        return self.save()

    def _request_clear(self):
        if self.raised:
            self.clear_pending = True
            return self.save()

    def message(self):
        return "%s - %s" % (self.id, self.name)

    def raised_duration(self):
        if self.last_raised:
            return time.time() - self.last_raised
        else:
            return 0

    # clear raised alarms if debounce duration has passed
    def attempt_clear(self):

        # if alarm is raised and the raised duration exceeds debounce duration
        if self.raised and self.raised_duration() > self.debounce_duration:

            # check if we should clear the raised flag
            if self.clear_pending or self.strategy != STRATEGY_SET_AND_EXPLICITLY_CLEAR:
                self.clear()

    def clear(self):
        self.raised = False
        self.last_raised = None
        self.clear_pending = False
        Alarm.logger.info("Cleared alarm %s" % self.name)
        return self.save()

    def current_duration(self):
        if self.current_begin:
            return time.time() - self.current_begin
        else:
            return 0

    def check_raised(self):
        if self.strategy in (STRATEGY_SET_AND_AUTO_CLEAR, STRATEGY_SET_AND_EXPLICITLY_CLEAR):
            return self.current_count > 0
        elif self.strategy == STRATEGY_MAX_FAILURES_WITHIN_DURATION:
            return self.current_count >= self.defined_count and self.current_duration() <= self.defined_duration
        elif self.strategy == STRATEGY_MIN_SUCCESSES_WITHIN_DURATION:
            return self.current_count < self.defined_count and self.current_duration() >= self.defined_duration

    def index(self):
        return self.id - 1

    @staticmethod
    def drop_alarms():
        sql = "DELETE FROM %s" % (DB_ALARMS_TABLE)
        prepared_act_on_database(EXECUTE, sql)

    @staticmethod
    def find(id):
        sql = "SELECT * FROM %s WHERE %s = %s" % \
              (DB_ALARMS_TABLE, DB_ALARMS_TBL_ID_COLUMN_NAME, "%s")
        args = [id]
        alarm_record = prepared_act_on_database(FETCH_ONE, sql, args)
        if not alarm_record:
            return False
        return Alarm(**alarm_record)

    @staticmethod
    def find_all():
        sql = "SELECT * FROM %s" % DB_ALARMS_TABLE
        alarm_records = prepared_act_on_database(FETCH_ALL, sql, [])
        alarms = []
        for alarm_record in alarm_records:
            alarms.append(Alarm(**alarm_record))
        return alarms

    @staticmethod
    def count():
        sql = "SELECT COUNT(*) FROM %s" % DB_ALARMS_TABLE
        count = prepared_act_on_database(FETCH_ROW_COUNT, sql)
        return count

    @staticmethod
    def create(**kwargs):
        a = Alarm(**kwargs)
        if not a:
            return False
        a.save()
        return a

    @staticmethod
    def occurrence(alarm_id, **kwargs):
        """
        This method increments the alarm incident count and records the time at which
        the first incident occurred to test for max count within duration, if necessary.
        """
        alarm = Alarm.find(alarm_id)
        if not alarm:
            return False

        prev_raised = alarm.raised
        alarm._occurrence()

        #  only log when an alarm is raised (and not when it is re-raised)
        if prev_raised is False and alarm.raised is True:
            alarm_message = "'%s' alarm was raised!!!" % alarm.name
            message = kwargs.get('message', None)
            if message:
                alarm_message += ' %s' % message
            event_tags = {'event_tags': [EVENT_TAG_ALARM] + kwargs.get('event_tags', [])}

            # notify alarm subscribers that alarm was raised
            subscribers = AlarmSubscriber.find_by_alarm(alarm)
            for subscriber in subscribers:
                subscriber.notify()

            if alarm.severity == SEVERITY_CRITICAL:
                Alarm.logger.critical(alarm_message, extra=event_tags)
            elif alarm.severity == SEVERITY_MAJOR:
                Alarm.logger.error(alarm_message, extra=event_tags)
            elif alarm.severity == SEVERITY_WARNING:
                Alarm.logger.warning(alarm_message, extra=event_tags)
            elif alarm.severity == SEVERITY_INFO:
                Alarm.logger.info(alarm_message, extra=event_tags)

        return True

    @staticmethod
    def request_clear(alarm_id):
        """
        This method is called to request clearing an alarm when the alarm's criteria is
        detected to no longer be True (e.g. temperature drops below non-critical threshold).

        The request to clear an alarm (rather than directly clearing) is necessary to avoid
        the situation where an alarm is set and cleared before it is recorded in the
        alarm_history. This approach guarantees that a discrete alarm event is not missed.
        """
        alarm = Alarm.find(alarm_id)
        if not alarm:
            return False
        return alarm._request_clear()


if __name__ == '__main__':
    Alarm.occurrence(134)
