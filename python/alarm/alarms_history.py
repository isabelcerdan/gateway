from base64 import b64encode
import httplib
import time

from bitarray import bitarray
from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database
from lib.logging_setup import setup_logger
from alarm import Alarm


class AlarmsHistory(object):
    def __init__(self):

        self.logger = setup_logger(__name__)

        # dirty is used to determine whether
        # to save a new alarm_history record
        self.dirty = False

        # use a bitarray for storing alarm states as bit fields
        self.alarms_bitarray = bitarray(int(Alarm.count()), endian='little')
        self.alarms_bitarray.setall(False)

        self.last_update = 0
        self.update_interval = 60   # update every 60 seconds

    def time_since_last_update(self):
        return int(time.time()) - self.last_update

    #
    # The update method should be called once every minute to potentially create
    # a new alarms_history record if any alarm has changed since the last update.
    # The method also handles clearing alarms after debounce period has transpired
    # without any new incidents of the alarm.
    #
    def update(self):
        if self.time_since_last_update() >= self.update_interval:
            self.logger.debug('Updating alarms history...')
            self.dirty = False
            self.last_update = int(time.time())

            # Iterate through all defined alarms
            for alarm in Alarm.find_all():

                try:
                    # check if the raised state (held in alarms_bitarray) has changed
                    if self.alarms_bitarray[alarm.index()] != alarm.raised:

                        # if so, set the dirty flag to True
                        self.dirty = True

                        # save the new alarm raised state
                        self.alarms_bitarray[alarm.index()] = alarm.raised
                        self.logger.debug('Alarms history detected change in alarm bit id %s (raised=%s)' % (alarm.id, alarm.raised))

                    # attempt to clear the alarm in case the debounce period has transpired
                    alarm.attempt_clear()
                except IndexError as ex:
                    self.logger.exception("Unable to process alarm (alarm id=%s)" % alarm.id)

            # if the raised state of any alarm has changed, save a new alarms_history record
            if self.dirty:
                self.save()

                # TODO: When the call to send_er_alarms is enabled, the latency for this method
                # goes up significantly and may require running from a separate thread.
                # self.send_er_alarms()

    #
    # NOT YET IMPLEMENTED ON THE AQ SERVER SIDE!!
    #
    def send_er_alarms(self):
        sql = "SELECT %s FROM %s" % (INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME, DB_GATEWAY_TABLE)
        gateway = prepared_act_on_database(FETCH_ONE, sql)

        sql = "SELECT URL_AQ FROM %s" % (DB_ENERGY_REPORT_CONFIG_TABLE)
        url_aq = prepared_act_on_database(FETCH_ONE, sql)

        alarms = b64encode(self.alarms_bitarray.tobytes())
        conn = httplib.HTTPConnection(url_aq)
        path = "/upload.php?serialnumber=%s&alarms=%s" % (gateway[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME], alarms)
        conn.request("GET", path)
        response = conn.getresponse()

    # save a new alarms_history record
    def save(self):
        sql = "INSERT INTO %s (%s, %s) VALUES (%s, %s)" % \
              (DB_ALARMS_HISTORY_TABLE,
               DB_ALARMS_HISTORY_TBL_TIME_COLUMN_NAME, DB_ALARMS_HISTORY_TBL_ALARMS_COLUMN_NAME,
               "%s", "%s")
        args = [time.time(), self.alarms_bitarray.to01()]
        prepared_act_on_database(EXECUTE, sql, args)
