from datetime import datetime
import email.utils
from email.mime.text import MIMEText
from smtplib import SMTPException
import smtplib

from lib.db import *
from lib.gateway_constants.vault import *
import lib.gateway
from lib.logging_setup import setup_logger
from lib.helpers import merge_dicts


class AlarmSubscriber(object):
    MAX_SMTP_ERRORS = 10

    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.alarm = kwargs['alarm']
        self.to_name = kwargs.get('name', 'UNKNOWN')
        self.to_address = kwargs['email_address']
        gateway = lib.gateway.Gateway.find_by()
        self.site_name = gateway.name
        self.from_name = "%s Gateway: Alarm Notifications" % self.site_name
        self.from_address = "apparent-eng@apparent.com"
        self.created_at = kwargs.get('created_at', None)

        self.smtp_server = GATEWAY_GMAIL_SMTP_SERVER
        self.smtp_server_port = GATEWAY_GMAIL_SMTP_SERVER_PORT
        self.smtp_email_address = GATEWAY_GMAIL_ADDRESS
        self.smtp_email_password = GATEWAY_GMAIL_PASSWORD
        self.smtp_error_cnt = 0

        self.logger = setup_logger(__name__)

    def notify(self):
        msg_body = "%s alarm was raised at %s on gateway site %s." % \
                   (self.alarm.name, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), self.site_name)
        msg = MIMEText(msg_body)
        msg['To'] = email.utils.formataddr((self.to_name, self.to_address))
        msg['From'] = email.utils.formataddr((self.from_name, self.from_address))
        msg['Subject'] = '%s Gateway: %s Alarm Raised' % (self.site_name, self.alarm.name)

        while self.smtp_error_cnt < AlarmSubscriber.MAX_SMTP_ERRORS:
            self.logger.info('Attempting to send email to %s for alarm %s...' % (self.to_address, self.alarm.name))
            server = smtplib.SMTP(self.smtp_server, self.smtp_server_port)
            server.set_debuglevel(False)  # show communication with the server
            server.ehlo()
            server.starttls()
            try:
                server.login(self.smtp_email_address, self.smtp_email_password)
            except Exception as err:
                error_string = 'Unable to login to SMTP server - %s' % repr(err)
                self.logger.error(error_string)
                server.quit()
                self.smtp_error_cnt += 1
                if self.smtp_error_cnt > AlarmSubscriber.MAX_SMTP_ERRORS:
                    self.logger.error('Maximum SMTP errors exceeded.')
                    return False
                continue

            try:
                server.sendmail(self.smtp_email_address,
                                email.utils.formataddr((self.to_name, self.to_address)), msg.as_string())
                # https://docs.python.org/2/tutorial/errors.html
                # The finally clause is executed "on the way out" when any other clause
                # of the try statement is left via a break, continue or return statement.
                return True
            except SMTPException:
                err_string = 'Unable to send email notification to %s' % self.to_address
                self.logger.error(err_string)
                self.smtp_error_cnt += 1
                if self.smtp_error_cnt > AlarmSubscriber.MAX_SMTP_ERRORS:
                    self.logger.error('Maximum SMTP errors exceeded.')
                    return False
            finally:
                server.quit()

        return False

    @staticmethod
    def find_by_alarm(alarm):
        sql = "select _asub.* from alarm_subscribers as _asub JOIN alarm_subscriptions as _aspt " \
              "ON _asub.id = _aspt.subscriber_id WHERE _aspt.alarm_id = %s AND _aspt.enabled = %s" % \
              ("%s", "%s")
        results = prepared_act_on_database(FETCH_ALL, sql, [alarm.id, 1])
        alarm_subscribers = []
        for result in results:
            result = merge_dicts(result, {'alarm': alarm})
            alarm_subscribers.append(AlarmSubscriber(**result))

        return alarm_subscribers
