__all__ = ['alarm', 'alarms_history', 'alarm_constants', 'alarm_handler']

from alarm import Alarm
from alarm_constants import *
from alarm_handler import AlarmHandler

# drop all alarms
Alarm.drop_alarms()

# re-add alarms
Alarm.create(id=ALARM_ID_GATEWAY_REBOOT,
             name='Gateway Reboot',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_CPU_CORE_TEMP,
             name='Motherboard Sensor - High CPU Core Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_CPU_CORE_TEMP,
             name='Motherboard Sensor - Low CPU Core Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_CPU_TEMP,
             name='Motherboard Sensor - High CPU Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_CPU_TEMP,
             name='Motherboard Sensor - Low CPU Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_SYSTEM_TEMP,
             name='Motherboard Sensor - High System Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_SYSTEM_TEMP,
             name='Motherboard Sensor - Low System Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_PERIPHERAL_TEMP,
             name='Motherboard Sensor - High Peripheral Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_PERIPHERAL_TEMP,
             name='Motherboard Sensor - Low Peripheral Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_DIMM_TEMP,
             name='Motherboard Sensor - High DIMM Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_DIMM_TEMP,
             name='Motherboard Sensor - Low DIMM Temperature',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_FAN_STATUS,
             name='Motherboard Sensor - High Fan Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_FAN_STATUS,
             name='Motherboard Sensor - Low Fan Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_VCCP_STATUS,
             name='Motherboard Sensor - High VCCP Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_VCCP_STATUS,
             name='Motherboard Sensor - Low VCCP Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_VDIMM_STATUS,
             name='Motherboard Sensor - High VDIMM Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_VDIMM_STATUS,
             name='Motherboard Sensor - Low VDIMM Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_12V_STATUS,
             name='Motherboard Sensor - High 12V Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_12V_STATUS,
             name='Motherboard Sensor - Low 12V Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_5VCC_STATUS,
             name='Motherboard Sensor - High 5 VCC Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_5VCC_STATUS,
             name='Motherboard Sensor - Low 5 VCC Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_3_3VCC_STATUS,
             name='Motherboard Sensor - High 3.3 VCC Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_3_3VCC_STATUS,
             name='Motherboard Sensor - Low 3.3 VCC Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_VBAT_STATUS,
             name='Motherboard Sensor - High VBAT Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_VBAT_STATUS,
             name='Motherboard Sensor - Low VBAT Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_3_3V_AUX_STATUS,
             name='Motherboard Sensor - High 3.3V AUX Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_3_3V_AUX_STATUS,
             name='Motherboard Sensor - Low 3.3V AUX Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_MOBO_SENSORS_READ_ERROR,
             name='Motherboard Sensor - Read Error',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_NETWORK_PROTECTOR_TRIP,
             name='Network Protector Tripped',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_GATEWAY_MONITOR_RESTARTED,
             name='Gateway Monitor Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_A_RESTARTED,
             name='Inverter Daemon A Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_B_RESTARTED,
             name='Inverter Daemon B Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_C_RESTARTED,
             name='Inverter Daemon C Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC_METER_INTERFACE_RESTARTED,
             name='PCC Meter Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SOLAR_METER_INTERFACE_RESTARTED,
             name='Solar Meter Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ICPCON_DAEMON_RESTARTED,
             name='IcpCon Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_NOTIFY_DAEMON_RESTARTED,
             name='Notify Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_STRING_MON_DAEMON_RESTARTED,
             name='String Discovery Monitor Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_AUDITOR_DAEMON_RESTARTED_DEPRECATED,                                                      #  39
             name='Alarm ID deprecated',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC_METER_MODBUS_READ_ERROR,
             name='PCC Meter Modbus Read Errors',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_MAX_FAILURES_WITHIN_DURATION,
             defined_count=20,
             defined_duration=60)

Alarm.create(id=ALARM_ID_SOLAR_METER_MODBUS_READ_ERROR,
             name='Solar Meter Modbus Read Errors',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_MAX_FAILURES_WITHIN_DURATION,
             defined_count=20,
             defined_duration=60)

Alarm.create(id=ALARM_ID_ENERGY_BUCKET_50_CAPACITY,
             name='Energy Bucket At 50pct Capacity',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_ENERGY_BUCKET_90_CAPACITY,
             name='Energy Bucket At 90pct Capacity',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_D_RESTARTED,
             name='Inverter Daemon D Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_E_RESTARTED,
             name='Inverter Daemon E Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_DAEMON_F_RESTARTED,
             name='Inverter Daemon F Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC2_METER_INTERFACE_RESTARTED,
             name='PCC2 Meter Interface Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC2_METER_MODBUS_READ_ERROR,
             name='PCC2 Meter Modbus Read Errors',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_MAX_FAILURES_WITHIN_DURATION,
             defined_count=20,
             defined_duration=60)

Alarm.create(id=ALARM_ID_BRAINBOX_UNREACHABLE,
             name='Brainbox Unreachable',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             debounce_duration=60,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_HIGH_5V_DUAL_STATUS,
             name='Motherboard Sensor - High 5V Dual Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_LOW_5V_DUAL_STATUS,
             name='Motherboard Sensor - Low 5V Dual Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_CHASSIS_INTRU_STATUS,
             name='Motherboard Sensor - Chassis Intru Status',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_NO_NETWORK_PROTECTOR_CONFIGURED,
             name='No network protector device configured',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_ENERGY_REVIEW_SEND_DATA_FAILURE,
             name='Energy Review - Send Data Failure',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC_1_METER_STALE,
             name='PCC 1 Meter - Stale Data',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_PCC_2_METER_STALE,
             name='PCC 2 Meter - Stale Data',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_SOLAR_1_METER_STALE,
             name='SOLAR 1 Meter - Stale Data',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_NETWORK_PROTECTOR_MODBUS_FAIL,
             name='Network Protector - ModBus Failure',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_NETWORK_PROTECTOR_DB_FAIL,
             name='Network Protector - Database Failure',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_NPLOG_NOT_CONFIGURED,
             name='Network Protector Logger not configured',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_UNABLE_TO_START_NPLOG,
             name='Unable to start Network Protector Logger',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_NPLOG_DAEMON_RESTARTED,
             name='Network Protector Logger restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR,
             debounce_duration=75)

Alarm.create(id=ALARM_ID_GTW_UTILS_SEND_CLI_COMMAND_BY_TLV_ERROR,
             name='Gateway Utilities - send cli',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR,
             name='Gateway Utilities - get TLV data',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR,
             name='Gateway Utilities - master TLV',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_GET_TLV_TYPE_ERROR_DEPRECATED,                                                  #  66
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_TLV_GET_DATA_ERROR_DEPRECATED,                                                  #  67
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR,
             name='Gateway Utilities - get TLV 16bit',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR,
             name='Gateway Utilities - get TLV 32bit',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR,
             name='Gateway Utilities - NBNS',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR,
             name='Gateway Utilities - send TLV',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_72_DEPRECATED,                                                                            #  72
             name='Alarm ID 72 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_73_DEPRECATED,                                                                            #  73
             name='Alarm ID 73 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ALL_INVALID_COMMAND_ERROR,                                                            #  74
             name='Upgrade ALL - invalid DB command',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ALL_MISSING_LEGACY_BOOT_ERROR,                                                        #  75
             name='Upgrade ALL - missing legacy bootloader',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ALL_THREAD_POOL_FAILURE,                                                              #  76
             name='Upgrade ALL - process thread',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_77_DEPRECATED,                                                                            #  77
             name='Alarm ID 77 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ALL_JOIN_FAILURE,                                                                      # 78
             name='Upgrade ALL - process join',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ALL_MISSING_FILE_ERROR,                                                               #  79
             name='Upgrade ALL - missing upgrade file',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_80_DEPRECATED,                                                                            #  80
             name='Alarm ID 80 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_81_DEPRECATED,                                                                            #  81
             name='Alarm ID 81 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_82_DEPRECATED,                                                                            #  82
             name='Alarm ID 82 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_83_DEPRECATED,                                                                            #  83
             name='Alarm ID 83 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_84_DEPRECATED,                                                                            #  84
             name='Alarm ID 84 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_85_DEPRECATED,                                                                            #  85
             name='Alarm ID 85 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_86_DEPRECATED,                                                                            #  86
             name='Alarm ID 86 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_87_DEPRECATED,                                                                            #  87
             name='Alarm ID 87 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_88_DEPRECATED,                                                                            #  88
             name='Alarm ID 88 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_89_DEPRECATED,                                                                            #  89
             name='Alarm ID 89 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_TFTP_CLOSE_ERROR,                                                                 #  90
             name='Upgrade failed - tftp close',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_91_DEPRECATED,                                                                            #  91
             name='Alarm ID 91 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_92_DEPRECATED,                                                                            #  92
             name='Alarm ID 92 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_93_DEPRECATED,                                                                            #  93
             name='Alarm ID 93 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_MISSING_BOOT_ERROR,                                                               #  94
             name='Upgrade failed - missing boot file',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_95_DEPRECATED,                                                                            #  95
             name='Alarm ID 95 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_96_DEPRECATED,                                                                            #  96
             name='Alarm ID 96 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_97_DEPRECATED,                                                                            #  97
             name='Alarm ID 97 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_98_DEPRECATED,                                                                            #  98
             name='Alarm ID 98 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_99_DEPRECATED,                                                                            #  99
             name='Alarm ID 99 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_100_DEPRECATED,                                                                           # 100
             name='Alarm ID 100 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_101_DEPRECATED,                                                                           # 101
             name='Alarm ID 101 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_102_DEPRECATED,                                                                           # 102
             name='Alarm ID 102 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_103_DEPRECATED,                                                                           # 103
             name='Alarm ID 103 deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_PREP_ACT_ON_DB_FAILURE,
             name='FeedDetect - Prepared act-on-db',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_DB_FAILURE,
             name='FeedDetect - generic DB',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_SOCKET_FAILURE,
             name='FeedDetect - socket',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_SENDTO_FAILURE,
             name='FeedDetect - sendto',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_CLOSE_FAILURE,
             name='FeedDetect - close',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_FEED_INDEX_ERROR,
             name='FeedDetect - list index error',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_FD_EMPTY_IP_LIST,
             name='FeedDetect - empty IP list',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PFCTRL_PREP_ACT_ON_DB_FAILURE_DEPRECATED,                                                 # 111
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PFCTRL_DB_FAILURE_DEPRECATED,                                                             # 112
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_DISK_USAGE_50_CAPACITY,
             name='Disk Usage Above 50pct Capacity',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_DISK_USAGE_90_CAPACITY,
             name='Disk Usage Above 90pct Capacity',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_DISK_USAGE_99_CAPACITY,
             name='Disk Usage Above 99pct Capacity',
             severity=SEVERITY_CRITICAL,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_DISK_USAGE_READ_ERROR,
             name='Disk Usage Read Error',
             severity=SEVERITY_CRITICAL,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_TLV_RANK_MANGLED_ERROR_DEPRECATED,                                              # 117
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_GET_TLV_LENGTH_ERROR,                                                           # 118
             name='Gateway Utilities - get TLV length',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UNSUCCESSFUL_STRING_AUDIT,                                                                # 119
             name='String Audit failed to complete',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_STRINGMON_UNHANDLED_EXCEPTION,                                                            # 120
             name='Unhandled exception in String Audit',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_DB_FAILURE_DEPRECATED,                                                                # 121
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_PREP_ACT_ON_DB_FAILURE_DEPRECATED,                                                    # 122
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_DAEMON_RESTARTED,                                                                      #123
             name='SIS - daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_POTENTIAL_QUERY_COMPROMISED_DEPRECATED,                                               # 124
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_INVALID_RUNMODE_RECEIVED_DEPRECATED,
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_SOCKET_SOCKET_ERROR_DEPRECATED,                                                 # 126
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_SEND_TO_ERROR_DEPRECATED,                                                       # 127
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_UNEXP_TLV_IN_RUNMODE_QUERY_DEPRECATED,                                          # 128
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_SOCKET_CLOSE_ERROR_DEPRECATED,                                                  # 129
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVALID_PF_COMMAND_TLV,                                                         # 130
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_AUDITOR_LEGACY_PHASE_ANGLE_DEPRECATED,                                                    # 131
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVALID_PACK,
             name='Gateway Utilities - inval data pack',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVALID_UNPACK,
             name='Gateway Utilities - inval data unpack',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC_METER_OFFLINE,
             name='PCC Meter Offline',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             debounce_duration=60,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_SIS_DATABASE_UPDATE_LIST_MISMATCH_DEPRECATED,                                             # 135
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVAL_HOS_RESPONSE_LENGTH_DEPRECATED,                                           # 136
             name='Alarm ID deprecated',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_CONTROLLER_UNHANDLED_EXCEPTION,
             name='CESS 400 controller unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_CONTROLLER_EXITING,
             name='CESS 400 controller exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MONITOR_UNHANDLED_EXCEPTION,
             name='CESS 400 monitor unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MONITOR_EXITING,
             name='CESS 400 monitor exiting',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_ALARM_UNHANDLED_EXCEPTION,
             name='CESS 400 Alarm monitor unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_ALARM_EXITING,
             name='CESS 400 Alarm monitor exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MAX_CELL_VOLTAGE_ALARM,
             name='CESS 400 maximum cell voltage alarm',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MIN_CELL_VOLTAGE_ALARM,
             name='CESS 400 minimum cell voltage alarm',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_NO_DISCHARGE_LIMIT_RESPONSE_ALARM,
             name='CESS 400 No discharge limit response defined',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_NO_CHARGE_LIMIT_RESPONSE_ALARM,
             name='CESS 400 No charge limit response defined',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_EXCESS_EXPORT_VIOLATION,
             name='Excess Export Violation - Inverter heartbeat was stopped',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SCHEDULER_DAEMON_RESTARTED,
             name='Scheduler Daemon was restarted.',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_MONITOR_DAEMON_RESTARTED,
             name='ESS Monitor daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_CONTROLLER_DAEMON_RESTARTED,
             name='ESS Controller daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_ALARM_DAEMON_RESTARTED,
             name='ESS Alarm daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_DISCHARGE_LIMIT_REACHED,
             name='CESS 400 discharge limit reached',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_CHARGE_LIMIT_REACHED,
             name='CESS 400 charge limit reached',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_DATA_LOGGER_EXITING,
             name='CESS 400 Data Logger exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_DATA_LOGGER_UNHANDLED_EXCEPTION,
             name='CESS 400 Data Logger unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_DATA_LOGGER_RESTARTED,
             name='ESS Data Logger restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_AC_CONVERTER_COMMUNICATIONS,
             name='CESS 400 AC Converter Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_BATTERY_STACK_COMMUNICATIONS,
             name='Battery stack Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_MULTIFUNCTIONAL_AMMETER_COMMUNICATIONS,
             name='Multifunctional ammeter Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_REMOTE_COMMUNICATIONS_ABNORMITY_UC,
             name='CESS 400 Remote Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_MASTER_COMMUNICATIONS_ABNORMITY_UC,
             name='CESS 400 Master Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_156_ADAS_COMMUNICATIONS_ABNORMITY_UC,
             name='CESS 400 ADAS Communications abnormity_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_CONTROL_CABINET_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 Control cabinet severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_IGBT_CABINET_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 IGBT cabinet severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_TRANSFORMER_CABINET_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 Transformer cabinet severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_TRANSFORMER_SEVERE_OVER_TEMPERATURE_UC,                        # 166
             name='CESS 400 Transformer severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_LC_FILTER_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 LC filter severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_DC_EMI_FILTER_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 DC EMI filter severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_AC_EMC_FILTER_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 AC EMC filter severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_LC_CAPACITY_1_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 LC capacity 1 severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_157_LC_CAPACITY_2_SEVERE_OVER_TEMPERATURE_UC,
             name='CESS 400 LC capacity 2 severe over temperature_UC',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_SYSALARMS_EXITING,                                                               # 172
             name='CESS 400 System Alarms process exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_SYSALARMS_UNHANDLED_EXCEPTION,                                                   # 173
             name='CESS 400 System Alarms process unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_PRE_CONTACTOR_CLOSE_FAILED_PCS,                             # 174
             name='CESS 400 DC pre-contactor close failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_AC_PRE_CONTACTOR_CLOSE_FAILED_PCS,                             # 175
             name='CESS 400 AC pre-contactor close failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_AC_MAIN_CONTACTOR_CLOSE_FAILED_PCS,                            # 176
             name='CESS 400 AC main contactor close failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_SMART_CONTROLLED_BREAKER1_CLOSE_FAILED_PCS,                 # 177
             name='CESS 400',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_MAIN_CONTACTOR_CLOSE_FAILED_PCS,                            # 178
             name='CESS 400 DC main contactor close failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_AC_BREAKER_TRIPPING_PCS,                                       # 179
             name='CESS 400 AC breaker tripping_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_AC_MAIN_CONTACTOR_ABNORMITY_DISCONNECT_WHILE_RUNNING_PCS,      # 180
             name='CESS 400 AC main contactor abnormity disconnect while running_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_BREAKER_ABNORMITY_DISCONNECT_WHILE_RUNNING_PCS,
             name='CESS 400 DC breaker abnormity disconnect while running_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_AC_MAIN_CONTACTOR_OPEN_FAILED_PCS,
             name='CESS 400 AC main contactor open failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_SMART_CONTROLLED_BREAKER1_OPEN_FAILED_PCS,
             name='CESS 400 DC smart controlled breaker1 open failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_DC_MAIN_CONTACTOR_OPEN_FAILED_PCS,
             name='CESS 400 DC main contactor open failed_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_HARDWARE_PDP_FAILURE_PCS,
             name='CESS 400 Hardware PDP failure_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15A_MASTER_EMERGENCY_STOP_PCS,
             name='CESS 400 Master emergency stop_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_SHORT_CIRCUIT_PROTECTION_PCS,
             name='CESS 400 DC short circuit protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_OVERVOLTAGE_PROTECTION_PCS,
             name='CESS 400 DC overvoltage protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_UNDERVOLTAGE_PROTECTION_PCS,
             name='CESS 400 DC undervoltage protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_REVERSE_OR_DISCONNECT_PCS,
             name='CESS 400 DC reverse or disconnect_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_OPEN_CIRCUIT_PROTECTION_PCS,
             name='CESS 400 DC open circuit protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_RECTIFIER_VOLTAGE_ABNORMAL_PROTECTION_PCS,
             name='CESS 400 Rectifier voltage abnormal protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_DC_OVERCURRENT_PROTECTION_PCS,
             name='CESS 400 DC overcurrent protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_A_PEAK_CURRENT_PROTECTION_PCS,
             name='CESS 400 Phase A peak current protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_B_PEAK_CURRENT_PROTECTION_PCS,
             name='CESS 400 Phase B peak current protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_C_PEAK_CURRENT_PROTECTION_PCS,
             name='CESS 400 Phase C peak current protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_A_CURRENT_VIRTUAL_VALUE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase A current virtual value high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_B_CURRENT_VIRTUAL_VALUE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase B current virtual value high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_PHASE_C_CURRENT_VIRTUAL_VALUE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase C current virtual value high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_GRID_PHASE_A_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Grid phase A voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_GRID_PHASE_B_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Grid phase B voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15C_GRID_PHASE_C_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Grid phase C voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_CONVERTER_PHASE_A_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Converter phase A voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_CONVERTER_PHASE_B_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Converter phase B voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_CONVERTER_PHASE_C_VOLTAGE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Converter phase C voltage sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_AC_CURRENT_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 AC current sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_DC_CURRENT_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 DC current sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_A_OVERTEMPERATURE_PROTECTION_PCS,
             name='CESS 400 Phase A overtemperature protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_B_OVERTEMPERATURE_PROTECTION_PCS,
             name='CESS 400 Phase B overtemperature protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_C_OVERTEMPERATURE_PROTECTION_PCS,
             name='CESS 400 Phase C overtemperature protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_A_TEMPERATURE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Phase A temperature sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_B_TEMPERATURE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Phase B temperature sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_C_TEMPERATURE_SAMPLING_INVALIDATION_PCS,
             name='CESS 400 Phase C temperature sampling invalidation_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_A_PRECHARGE_UNMET_PROTECTION_PCS,
             name='CESS 400 Phase A precharge unmet protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_B_PRECHARGE_UNMET_PROTECTION_PCS,
             name='CESS 400 Phase B precharge unmet protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_C_PRECHARGE_UNMET_PROTECTION_PCS,
             name='CESS 400 Phase C precharge unmet protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_PHASE_SEQUENCE_FAULT_PROTECTION_PCS,
             name='CESS 400 Phase sequence fault protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15D_DSP_PROTECTION_PCS,
             name='CESS 400 DSP protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_A_GRID_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase A grid voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_A_GRID_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase A grid voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_B_GRID_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase B grid voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_B_GRID_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase B grid voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_C_GRID_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase C grid voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_C_GRID_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase C grid voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_A_GRID_VOLTAGE_SEVERE_LOW_PROTECTION_PCS,
             name='CESS 400 Phase A grid voltage severe low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_A_GRID_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_PCS,
             name='CESS 400 Phase A grid voltage first section low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_B_GRID_VOLTAGE_SEVERE_LOW_PROTECTION_PCS,
             name='CESS 400 Phase B grid voltage severe low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_B_GRID_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_PCS,
             name='CESS 400 Phase B grid voltage first section low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_C_GRID_VOLTAGE_SEVERE_LOW_PROTECTION_PCS,
             name='CESS 400 Phase C grid voltage severe low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_PHASE_C_GRID_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_PCS,
             name='CESS 400 Phase C grid voltage first section low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_FREQUENCY_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Frequency severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_FREQUENCY_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Frequency first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_FREQUENCY_SEVERE_LOW_PROTECTION_PCS,
             name='CESS 400 Frequency severe low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15E_FREQUENCY_FIRST_SECTION_LOW_PROTECTION_PCS,
             name='CESS 400 Frequency first section low protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_GRID_PHASE_A_DELETION_PCS,
             name='CESS 400 Grid phase A deletion_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_GRID_PHASE_B_DELETION_PCS,
             name='CESS 400 Grid phase B deletion_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_GRID_PHASE_C_DELETION_PCS,
             name='CESS 400 Grid phase C deletion_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_ISLANDING_PROTECTION_PCS,
             name='CESS 400 Islanding protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_A_LOW_VOLTAGE_RIDE_THROUGH_PCS,
             name='CESS 400 Phase A low voltage ride through _PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_B_LOW_VOLTAGE_RIDE_THROUGH_PCS,
             name='CESS 400 Phase B low voltage ride through _PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_C_LOW_VOLTAGE_RIDE_THROUGH_PCS,
             name='CESS 400 Phase C low voltage ride through _PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_A_INVERTER_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase A Inverter voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_A_INVERTER_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase A Inverter voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_B_INVERTER_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase B Inverter voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_B_INVERTER_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase B Inverter voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_C_INVERTER_VOLTAGE_SEVERE_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase C Inverter voltage severe high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_PHASE_C_INVERTER_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_PCS,
             name='CESS 400 Phase C Inverter voltage first section high protection_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_15F_INVERTER_PEAK_VOLTAGE_HIGH_PROTECTION_CAUSE_BY_AC_DISCONNECT_PCS,
             name='CESS 400 Inverter peak voltage high protection cause by AC disconnect_PCS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_NO_USABLE_BATTERY_STRING_BATT,
             name='CESS 400 No usable Battery string_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_BATTERY_STACK_FIRST_SECTION_CREEPAGE_BATT,
             name='CESS 400 Battery stack first section creepage_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_BATTERY_STACK_SEVERE_CREEPAGE_BATT,
             name='CESS 400 Battery stack severe creepage_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_BATTERY_STACK_STARTUP_FAILED_BATT,
             name='CESS 400 Battery stack startup failed_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_BATTERY_STACK_STOP_FAILED_BATT,
             name='CESS 400 Battery stack stop failed_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_164_COMMUNICATIONS_BETWEEN_BSMU_AND_UC_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and UC disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_1_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 1 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_2_DISCONNECTED_BATT,
             name='CESS 400 The Comm. between BSMU and BECU 2 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_3_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 3 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_4_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 4 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_5_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 5 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_6_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 6 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_7_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 7 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_165_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_8_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU 8 disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_1_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 1 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_2_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 2 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_3_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 3 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_4_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 4 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_5_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 5 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_6_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 6 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_7_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 7 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_166_BATTERY_STRING_8_MAIN_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 8 main contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_1_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 1 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_2_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 2 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_3_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 3 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_4_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 4 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_5_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 5 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_6_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 6 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_7_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 7 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_167_BATTERY_STRING_8_PRE_CONTACTOR_ABNORMITY_DETECTED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 8 pre-contactor abnormity detected by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_1_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 1 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_2_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 2 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_3_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 3 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_4_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 4 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_5_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 5 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_6_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 6 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_7_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 7 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_168_BATTERY_STRING_8_MAIN_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 8 main contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_1_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 1 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_2_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 2 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_3_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 3 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_4_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 4 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_5_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 5 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_6_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 6 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_7_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 7 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_169_BATTERY_STRING_8_PRE_CONTACTOR_ABNORMITY_CONTROLLED_BY_BATTERY_STACK_BATT,
             name='CESS 400 Battery string 8 pre-contactor abnormity controlled by battery stack_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_VOLTAGE_SAMPLING_CIRCUIT_ABNORMITY_BATT,
             name='CESS 400 Battery voltage sampling circuit abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_POWER_SUPPLY_RELAY_DISCONNECTED_BATT,
             name='CESS 400 Battery power supply relay disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_VOLTAGE_SAMPLING_ROUTE_DISCONNECTED_BATT,
             name='CESS 400 Battery voltage sampling route disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_TEMPERATURE_SAMPLING_ROUTE_DISCONNECTED_BATT,
             name='CESS 400 Battery temperature sampling route disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_INSIDE_CAN_DISCONNECTED_BATT,
             name='CESS 400 Battery inside CAN disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_CATHODE_LEAKAGE_DETECTION_ABNORMITY_BATT,
             name='CESS 400 Battery cathode leakage detection abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_ANODE_LEAKAGE_DETECTION_ABNORMITY_BATT,
             name='CESS 400 Battery anode leakage detection abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_CURRENT_SAMPLING_CIRCUIT_ABNORMITY_BATT,
             name='CESS 400 Battery current sampling circuit abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_BATTERY_CELL_INVALIDATION_BATT,
             name='CESS 400 Battery battery cell invalidation_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_MAIN_CONTACTOR_DETECTED_ABNORMITY_BATT,
             name='CESS 400 Battery main contactor detected abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_PRE_CONTACTOR_DETECTED_ABNORMITY_BATT,
             name='CESS 400 Battery pre-contactor detected abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_FAN_CONTACTOR_DETECTED_ABNORMITY_BATT,
             name='CESS 400 Battery fan contactor detected abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_POWER_SUPPLY_RELAY_DETECTED_ABNORMITY_BATT,
             name='CESS 400 Battery power supply relay detected abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16C_BATTERY_INTERMEDIATE_CONTACTOR_DETECTED_ABNORMITY_BATT,
             name='CESS 400 Battery intermediate contactor detected abnormity_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_SEVERE_OVERTEMPERATURE_BATT,
             name='CESS 400 Battery severe overtemperature_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_SMOKE_ALARM_BATT,
             name='CESS 400 Battery smoke alarm_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_BLOWN_FUSE_INDICATOR_FAILURE_BATT,
             name='CESS 400 Battery blown fuse indicator failure_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_FIRST_SECTIONL_LEAKAGE_BATT,
             name='CESS 400 Battery first sectionl leakage_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_SEVERE_LEAKAGE_BATT,
             name='CESS 400 Battery severe leakage_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_COMMUNICATIONS_BETWEEN_BSMU_AND_BECU_DISCONNECTED_BATT,
             name='CESS 400 Communications between BSMU and BECU disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_INTERMEDIATE_CONTACTOR_DISCONNECTED_BATT,
             name='CESS 400 Battery intermediate contactor disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_16D_BATTERY_POWER_SUPPLY_RELAY_CONTACTOR_DISCONNECTED_BATT,
             name='CESS 400 Battery power supply relay contactor disconnected_BATT',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_AC_PRE_CONTACTOR_CLOSE_FAILED_MASTER,
             name='CESS 400 AC pre-contactor close failed_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_AC_MAIN_CONTACTOR_CLOSE_FAILED_MASTER,
             name='CESS 400 AC main contactor close failed_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_AC_PRE_CONTACTOR_OPEN_FAILED_MASTER,
             name='CESS 400 AC pre-contactor open failed_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_AC_MAIN_CONTACTOR_OPEN_FAILED_MASTER,
             name='CESS 400 AC main contactor open failed_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_A_VOLTAGE_SEVERE_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase A voltage severe high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_A_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase A voltage first section high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_B_VOLTAGE_SEVERE_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase B voltage severe high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_B_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase B voltage first section high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_C_VOLTAGE_SEVERE_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase C voltage severe high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_C_VOLTAGE_FIRST_SECTION_HIGH_PROTECTION_MASTER,
             name='CESS 400 Phase C voltage first section high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_A_VOLTAGE_SEVERE_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase A voltage severe low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_A_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase A voltage first section low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_B_VOLTAGE_SEVERE_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase B voltage severe low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_B_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase B voltage first section low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_C_VOLTAGE_SEVERE_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase C voltage severe low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_172_PHASE_C_VOLTAGE_FIRST_SECTION_LOW_PROTECTION_MASTER,
             name='CESS 400 Phase C voltage first section low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_173_FREQUENCY_SEVERE_HIGH_PROTECTION_MASTER,
             name='CESS 400 Frequency severe high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_173_FREQUENCY_FIRST_SECTION_HIGH_PROTECTION_MASTER,
             name='CESS 400 Frequency first section high protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_173_FREQUENCY_SEVERE_LOW_PROTECTION_MASTER,
             name='CESS 400 Frequency severe low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_173_FREQUENCY_FIRST_SECTION_LOW_PROTECTION_MASTER,
             name='CESS 400 Frequency first section low protection_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_ABNORMITY_173_EMERGENCY_STOP_MASTER,
             name='CESS 400 Emergency stop_MASTER',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_SYSALARMS_RESTARTED,
             name='CESS 400 System Alarm process restarted',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ARTYSEN_CHARGER_CONTROLLER_UNHANDLED_EXCEPTION,
             name='Artysen charger controller unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ARTYSEN_CHARGER_CONTROLLER_EXITING,
             name='Artysen charger controller exiting',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVALID_SET_AIF_IMMED_COMMAND,
             name='Gateway Utilities - inval TLV in set aif immed command',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_INVALID_SET_AIF_SETTINGS_COMMAND,
             name='Gateway Utilities - inval TLV in set aif settings command',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_POWER_FACTOR_SANITY,
             name='PowerfactorControl - mismatch sanity count',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_MONITOR_EXITING,
             name='CESS 40 Monitor Exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_MONITOR_UNHANDLED_EXCEPTION,
             name='CESS 40 Unhandled Exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_CONTROLLER_UNHANDLED_EXCEPTION,
             name='CESS 40 unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_CONTROLLER_EXITING,
             name='CESS 40 Controller exiting',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_ALARM_DAEMON_EXITING,
             name='CESS 40 Alarm Daemon exiting',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_40_MONITOR_DAEMON_RESTARTED,
             name='CESS 40 Alarm Monitor restarted',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_40_ALARM_DAEMON_RESTARTED,
             name='CESS 40 Alarm deamon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_ALARM_UNHANDLED_EXCEPTION,
             name='CESS 40 unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_ALARM_EXITING,
             name='CESS 40 Alarm process exiting',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_MAX_CELL_VOLTAGE_ALARM,
             name='CESS 40 Maximum cell voltage exceeded',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_MIN_CELL_VOLTAGE_ALARM,
             name='CESS 40 Minimum cell voltage limit exceeded',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_NO_CHARGE_LIMIT_RESPONSE_ALARM,
             name='CESS 40 No charge limit response',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_NO_DISCHARGE_LIMIT_RESPONSE_ALARM,                                                # 356
             name='CESS 40 No discharge limit response',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_40_DISCHARGE_LIMIT_REACHED,                                                      # 357
             name='CESS 40 discharge limit reached',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_40_CHARGE_LIMIT_REACHED,                                                         # 358
             name='CESS 40 charge limit reached',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_SYSALARMS_UNHANDLED_EXCEPTION,                                                    # 359
             name='CESS 40 System Alarms unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_40_SYSALARMS_EXITING,                                                                # 360
             name='CESS 40 System Alarms process exiting',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MAX_ALLOWABLE_CHARGE_ACTIVE_POWER,                                               # 361
             name='CESS 400 max allowable charge active power is zero',
             severity=SEVERITY_CRITICAL,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MAX_ALLOWABLE_DISCHARGE_ACTIVE_POWER,                                            # 362
             name='CESS 400 max allowable discharge active power is zero',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MAX_ALLOWABLE_APPARENT_POWER,                                                    # 363
             name='CESS 400 max allowable apparent power is zero',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CESS_400_MAX_ALLOWABLE_REACTIVE_POWER,                                                    # 364
             name='CESS_400 max allowable reactive power is zero',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_IRRADIANCE_DAEMON_RESTARTED_DEPRECATED,                                                   # 365
             name='Alarm ID deprecated',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SIS_TLV_INVERTER_UPDATE_LIST_MISMATCH_DEPRECATED,                                         # 366
             name='Alarm ID deprecated',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ER_SYNC_DAEMON_RESTARTED,                                                                 # 367
             name='Energy Review Sync Daemon restarted',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_METER_ONLINE_DAEMON_RESTARTED,                                                            # 368
             name='Meter online daemon was restarted',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCC2_METER_OFFLINE,                                                                       # 369
             name='PCC2 Meter Offline',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             debounce_duration=60,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_SOLAR_METER_OFFLINE,                                                                      # 370
             name='Solar Meter Offline',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             debounce_duration=60,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_BATTERY_METER_OFFLINE,                                                                    # 371
             name='Battery Meter Offline',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             debounce_duration=60,
             strategy=STRATEGY_SET_AND_EXPLICITLY_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_WAS_ZERO_ERROR,                                                   # 372
             name='Gateway Utilities - Next TLV rank zero',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_EXCEEDS_PACKED_DATA_ERROR,                                        # 373
             name='Gateway Utilities - Next TLV rank exceeds packed data',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BYD_CESS_400_START_SCRIPT_FAILED,                                                         # 374
             name='BYD CESS 400 start script failed',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_PUBLISHER_EXITING,
             name = 'EVE BMU status publisher exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_PUBLISHER_UNHANDLED_EXCEPTION,
             name = 'EVE BMU status publisher unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_PUBLISHER_RESTARTED,
             name = 'EVE BMU status publisher restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DATA_AGGREGATOR_EXITING,
             name = 'EVE BMU Data Aggregator exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DATA_AGGREGATOR_UNHANDLED_EXCEPTION,
             name = 'EVE BMU Data Aggregator unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DATA_AGGREGATOR_RESTARTED,
             name = 'EVE BMU Data Aggregator restarted',
             severity = SEVERITY_INFO,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_MSG_BROKER_EXITING,
             name = 'Msg Broker exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_MSG_BROKER_UNHANDLED_EXCEPTION,
             name = 'Msg Broker unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_MSG_BROKER_RESTARTED,
             name = 'Msg Broker restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_PUBLISHER_EXITING,
             name = 'BMU data publisher factory exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_PUBLISHER_UNHANDLED_EXCEPTION,
             name = 'BMU data publisher factory unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_PUBLISHER_RESTARTED,
             name = 'BMU data publisher factory restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_SUBSCRIBER_EXITING,
             name = 'BMU data subscriber factory exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_SUBSCRIBER_UNHANDLED_EXCEPTION,
             name = 'BMU data suscriber factory unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_BMU_DATA_SUBSCRIBER_RESTARTED,
             name = 'BMU data subscriber factory restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_SUBSCRIBER_EXITING,
             name = 'EVE BMU status subscriber exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_SUBSCRIBER_UNHANDLED_EXCEPTION,
             name = 'EVE BMU status subscriber unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_STATUS_SUBSCRIBER_RESTARTED,
             name = 'EVE BMU status subscriber restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_BATTERY_CELL_OVER_VOLTAGE_PROTECTION,
             name = 'EVE BMU battery cell over voltage protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_BATTERY_CELL_LOW_VOLTAGE_PROTECTION,
             name = 'EVE BMU battery cell low voltage protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_BATTERY_PACK_OVER_VOLTAGE_PROTECTION,
             name = 'EVE BMU battery pack over voltage protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_BATTERY_PACK_LOW_VOLTAGE_PROTECTION,
             name = 'EVE BMU battery pack low voltage protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CHARGING_OVER_CURRENT_PROTECTION,
             name = 'EVE BMU charging over current protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DISCHARGING_OVER_CURRENT_PROTECTION,
             name = 'EVE BMU discharging over current protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_SHORT_CIRCUIT_PROTECTION,
             name = 'EVE BMU short circuit protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CHARGER_OVERVOLTAGE_PROTECTION,
             name = 'EVE BMU charger overvoltage protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CHARGING_HIGH_TEMPERATURE_PROTECTION,
            name = 'EVE BMU charging high temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DISCHARGING_HIGH_TEMPERATURE_PROTECTION,
             name = 'EVE BMU discharging high temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CHARGING_LOW_TEMPERATURE_PROTECTION,
             name = 'EVE BMU charging low temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DISCHARGING_LOW_TEMPERATURE_PROTECTION,
             name = 'EVE BMU discharging low temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_MOSFET_HIGH_TEMPERATURE_PROTECTION,
             name = 'EVE BMU MOSFET high temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_ENVIRONMENT_HIGH_TEMPERATURE_PROTECTION,
             name = 'EVE BMU environment high temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_ENVIRONMENT_LOW_TEMPERATURE_PROTECTION,
             name = 'EVE BMU environment low temperature protection',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_OVERVOLTAGE,
             name = 'EVE BMU warning battery cell overvoltage',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_LOW_VOLTAGE,
             name = 'EVE BMU warning battery cell low voltage',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_OVERVOLTAGE,
             name = 'EVE BMU warning battery pack overvoltage',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_LOW_VOLTAGE,
             name = 'EVE BMU warning battery pack low voltage',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_CHARGING_OVER_CURRENT,
             name = 'EVE BMU warning charging over current',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_DISCHARGING_OVER_CURRENT,
             name = 'EVE BMU warning discharging over current',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_CHARGING_HIGH_TEMPERATURE,
             name = 'EVE BMU environment low temperature protection',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_DISCHARGING_HIGH_TEMPERATURE,
             name = 'EVE BMU environment low temperature protection',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_CHARGING_LOW_TEMPERATURE,
             name = 'EVE BMU environment low temperature protection',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_DISCHARGING_LOW_TEMPERATURE,
             name = 'EVE BMU discharging low temperature',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_HIGH_TEMPERATURE,
             name = 'EVE BMU environment high temperature',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_LOW_TEMPERATURE,
             name = 'EVE BMU environment low temperature',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_MOSFET_HIGH_TEMPERATURE,
             name = 'EVE BMU mosfet high temperature',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_WARNING_SOC_LOW,
             name = 'EVE BMU SOC low',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CHARGING_MOSFET_FAULT,
             name = 'EVE BMU charging MOSFET fault',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_DISCHARGING_MOSFET_FAULT,
             name = 'EVE BMU discharging MOSFET fault',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_TEMPERATURE_SENSOR_FAULT,
             name = 'EVE BMU temperature sensor fault',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_BATTERY_CELL_FAULT,
             name = 'EVE BMU battery cell fault',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_FRONT_END_SAMPLING_COMMUNICATION_FAULT,
             name = 'EVE BMU front end sampling communication fault',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CONTROLLER_EXITING,
             name = 'EVE BMU Controller exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_EVE_BMU_CONTROLLER_UNHANDLED_EXCEPTION,
             name = 'EVE BMU Controller unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_DATA_PUBLISHER_FACTORY_EXITING,
             name = 'Artesyn data publisher factory exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_DATA_PUBLISHER_FACTORY_UNHANDLED_EXCEPTION,
             name = 'Artesyn data publisher factory unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_DATA_PUBLISHER_FACTORY_RESTARTED,
             name = 'Artesyn data publisher factory restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_EXITING,
             name = 'Artesyn charger controller exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_UNHANDLED_EXCEPTION,
             name = 'Artesyn charger controller unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_RESTARTED,
             name = 'Artesyn charger controller restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VOUT_OV_FAULT,
             name = 'Artesyn charger controller vout ov fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_IOUT_OV_FAULT,
             name = 'Artesyn charger controller iout ov fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VIN_UV_FAULT,
             name = 'Artesyn charger controller vin uv fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_TEMPERATURE_FAULT,
             name = 'Artesyn charger controller temperature fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_FAULT,
             name = 'Artesyn charger controller CML fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_OFF_FAULT,
             name = 'Artesyn charger controller OFF fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_NONE_OF_THE_ABOVE_FAULT,
             name = 'Artesyn charger controller none of the above fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_UNSUPPORTED_CMD,
             name = 'Artesyn charger controller CML unsupported command',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_INVALID_DATA,
             name = 'Artesyn charger controller CML invalid data',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PACKET_ERROR,
             name = 'Artesyn charger controller CML packet data',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_MEMORY_FAULT,
             name = 'Artesyn charger controller CML memory fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PROCESSOR_FAULT,
             name = 'Artesyn charger controller CML processor fault',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_MASTER_DATA_LOGGER_EXITING,
             name = 'AESS master data logger exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_MASTER_DATA_LOGGER_UNHANDLED_EXCEPTION,
             name = 'AESS master data logger unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_MASTER_DATA_LOGGER_RESTARTED,
             name = 'AESS master data logger restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_BMU_DATA_LOGGER_EXITING,
             name = 'AESS bmu data logger exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_BMU_DATA_LOGGER_UNHANDLED_EXCEPTION,
             name = 'AESS bmu data logger unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_BMU_DATA_LOGGER_RESTARTED,
             name = 'AESS bmu data logger restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_AESS_DISCHARGER_CONTROLLER_RESTARTED,
             name = 'Apparent discharger controller restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CONTROLLER_FACTORY_EXITING,                                                     # 454
             name = 'Artesyn controller factory exiting',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CONTROLLER_FACTORY_UNHANDLED_EXCEPTION,                                         # 455
             name = 'Artesyn controller factory unhandled exception',
             severity = SEVERITY_MAJOR,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id = ALARM_ID_ARTESYN_CONTROLLER_FACTORY_RESTARTED,                                                   # 456
             name = 'Artesyn controller factory restarted',
             severity = SEVERITY_WARNING,
             er_alarm = False,
             strategy = STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_BATTERY_METER_INTERFACE_RESTARTED,                                                        # 457
             name='Battery Meter Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_EXITING,                                                                       # 458
             name='ESS limits exiting',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_UNHANDLED_EXCEPTION,                                                           # 459
             name='ESS limits unhandled exception',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_RESTARTED,                                                                     # 460
             name='ESS limits restarted',
             severity=SEVERITY_INFO,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_OVER_LIMIT,                                                       # 461
             name='ESS limits cell voltage over limit',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_UNDER_LIMIT,                                                      # 462
             name='ESS limits cell voltage under limit',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_CELL_TEMPERATURE_OVER_LIMIT,                                                   # 463
             name='ESS limits cell temperature over limit',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_MOSFET_TEMPERATURE_OVER_LIMIT,                                                 # 464
             name='ESS limits MOSFFET temperature over limit',
             severity=SEVERITY_MAJOR,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_BATTERY_SOC_OVER_LIMIT,                                                        # 465
             name='ESS limits battery SOC over limit',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_BATTERY_SOC_UNDER_LIMIT,                                                       # 466
             name='ESS limits battery SOC under limit',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_LIMITS_ENVIRONMENT_TEMPERATURE_OVER_LIMIT,                                            # 467
             name='ESS limits environment temperature over limit',
             severity=SEVERITY_MAJOR,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_SOFTWARE_PROFILE_REGION_NAME_MISMATCH,                                                    # 468
             name='SW_PROF_REGION_NAME_MISMATCH',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_MANAGER_DAEMON_RESTARTED,                                                             # 469
             name='ESS Manager Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_MSG_BROKER_MONITOR_RESTARTED,                                                             # 470
             name='Message Broker Daemon Restarted',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_EVE_BMU_DAEMON_RESTARTED,                                                                 # 471
             name='EVE BMU Daemon Restarted',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PSA_DAEMON_RESTARTED,                                                                      #472
             name='PSA - daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UNSUCCESSFUL_PCS_UNIT_ID_ASSOCIATION_CONTROL,                                             # 473
             name='PUI_ASSOC failed to complete',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCS_UNIT_ID_ASSOCIATION_UNHANDLED_EXCEPTION,                                              # 474
             name='PUI_ASSOC Unhandled exception',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCS_UNIT_ID_ASSOCIATION_BMU_ID_0_DETECTED,                                                # 475
             name='PUI_ASSOC encountered BMU ID 0 in aess_bmu_data table',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_PCS_UNIT_ID_ASSOCIATION_TLV_INTERFACE_BUG,                                                # 476
             name='PUI_ASSOC TLV DATA UNEQUAL ID AND DATA LENGTHS',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ESS_FIXER_RESTARTED,                                                                      # 477
             name='ESS Fixer restarted',
             severity=SEVERITY_MAJOR,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_EXPORT_READINGS_ERROR,                                                                    # 478
             name='Export Readings Failed',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_UPGRADE_FAILURE_INTERVENTION_REQUIRED,                                            # 479
             name='Upgrade failed - manual intervention required',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_CHARGER_ASSOC_UNEXP_EXEC_EVENT,                                                           # 480
             name='Charger Association - unexpected code execution',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_PERSISTENT_APP_MODE_AFTER_URESET,                                                 # 481
             name='Upgrade failed - persistent app mode after multiple upgrade resets',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_PROCESS_EXCEPTION,                                                                # 482
             name='Upgrade failed - process exception encountered',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_DB_BOOT_REV_BOOT_COMBO_NOT_ALIGNED,                                               # 483
             name='Upgrade failed - DB boot_rev bootCombo file not in alignment',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_SUSPICIOUS_UPGRADE_FILE,                                                          # 484
             name='Upgrade failed - user chosen upgrade file suspicious',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_UPG_ONE_INVAL_RUNMODE_IN_QUERY_RESPONSE,                                                  # 485
             name='Upgrade failed - TLV query reply contained invalid runmode',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_POWER_REGULATOR_DAEMON_RESTARTED,                                                         # 486
             name='Power Regulator Daemon Restarted',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_RESTARTED,                                                                  # 487
             name='Groups Daemon Restarted',
             severity=SEVERITY_CRITICAL,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_TLV_UTILS_DB_GROUP_ID_NULL_ROW,                                                           # 488
             name='TLV Utils - group ID row fetch returned NULL data',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RESET_DEFAULTS,                                                  # 489
             name='Groups Daemon - group ID 1 data reset to defaults',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RE_CREATED,                                                      # 490
             name='Groups Daemon - group ID 1 entry missing, re-inserted',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_INVERTERS_GROUP_ID_MISSING_FROM_GROUPS,                                     # 491
             name='Groups Daemon - inverters with group ID not in groups table',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_IRRADIANCE_GROUP_INCONSISTENT,                                              # 492
             name='Groups Daemon - irradiance groups data inconsistent',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_GROUPS_TABLE_INCONSISTENT,                                                  # 493
             name='Groups Daemon - groups table inconsistent',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_INVERTER_ASSIGNED_TO_NULL_GROUP,                                            # 494
             name='Groups Daemon - inverter assigned to NULL GROUP',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_ENERGY_REPORT_CONFIG_INCONSISTENT,                                          # 495
             name='Groups Daemon - energy_report_config table inconsistent',
             severity=SEVERITY_WARNING,
             er_alarm=True,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_ERPT_DAEMON_RESTARTED,                                                                    # 496
             name='ERPT - daemon restarted',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_GROUPS_DAEMON_INVERTERS_NULL_GROUP_ID,                                                    # 497
             name='Groups Daemon - inverters with NULL group Id',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)

Alarm.create(id=ALARM_ID_INVERTER_ASSOCIATION_INTERNAL_ERROR,                                                      # 498
             name='Inverter association process - internal error',
             severity=SEVERITY_WARNING,
             er_alarm=False,
             strategy=STRATEGY_SET_AND_AUTO_CLEAR)
