#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

from power_regulator.power_regulator_daemon import *
from meter.meter_daemon import *
from upgrade_all_inverters import *
from power_factor_control import *
from power_status.psa_daemon import PsaDaemon
from groups_daemon import GroupsDaemon
from feed_discovery import *
from StringMonDaemon import instantiate_stringmonscript
from pui_association import instantiate_puiassocscript
from charger_association import instantiate_chargerassocscript
from NotifyDaemon import NotifyDaemon
from IcpConDaemon import IcpConDaemon
from NPLogDaemon import NPLogDaemon
from EVE.MsgBroker import MsgBrokerDaemon
from energy_review.er_sync_daemon import ERSyncDaemon
from scheduler.scheduler import SchedulerDaemon
from ess.ess_manager import EssManagerDaemon
from alarm.alarm import *
from alarm.alarms_history import AlarmsHistory
from lib.gateway import Gateway
from daemon.daemon_monitor import DaemonMonitor
from ess.ess_unit import EssUnit
from energy_bucket.energy_bucket_task import EnergyBucketTask
from energy_reports.erpt_daemon import ErptDaemon
import MySQLdb as MyDB
from multiprocessing import Process, Lock
from lib.db import *
from lib.logging_setup import *
from lib.sensor import Sensor

# The monitor runs on forever, sleeps once every ...
MONITOR_SLEEP_PERIOD_SECS = 0.8

# The "no production allowed" message is logged only once every ...
NO_PRODUCTION_LOG_FREQUENCY_SECS = 60

class GatewayMonitor(object):
    def __init__(self, **kwargs):
        self.no_restarts = kwargs.get('no_restarts', False)
        self.logger = setup_logger('GatewayMonitor')
        self.logger.info("***GATEWAY MONITOR DAEMON STARTED WITH NEW LOG***")
        self.alarms_history = AlarmsHistory()
        self.energy_bucket_task = EnergyBucketTask()
        self.byd_cess_400_lock = Lock()
        self.gateway = Gateway.find_by()
        self.get_outta_jail_packet_sent = 0
        self.throttle_no_production_log_messages = 0
        if not self.gateway.string_sight:
            # If this Gateway is in standard NXO operation mode, a control heartbeat will be multicasted,
            # else inverters will be taken out of gateway mode and be allowed to run feral.
            self.send_control_heartbeat()
        else:
            self.logger.info("Running with String Sight Capabilities")
        self.daemon_monitors = []
        self.reinit_daemon_monitors()

        # Do this before we get to initialization loop. should only run once at startup.
        self.reinit_upgrade()
        self.reinit_feed_discovery()
        self.reinit_pui_assoc()
        self.reinit_charger_assoc()
        self.reinit_stringmon_audit()
        self.check_feed_discovery_control_table()
        self.energy_bucket_task.get_bucket_sample()

    def run(self):
        while True:
            Sensor.read_all()
            self.alarms_history.update()
            self.energy_bucket_task.handle_bucket()

            self.start_upgrade()
            self.start_feed_discovery()
            self.start_stringmon_audit()
            self.start_pcs_unit_id_association()
            self.start_charger_association()

            self.gateway.reload()
            if self.gateway.update_config:
                self.reinit_daemon_monitors()
                self.gateway.update_config = False
                self.gateway.save()

            if self.gateway.system_restart:
                self.gateway.system_restart = False
                self.gateway.save()
                self.logger.info('XX Restarting Gateway XX')
                exit()

            if not self.gateway.string_sight and not self.nxo_active():
                self.send_control_heartbeat()

            # monitor all daemons; starting, stopping, restarting as necessary
            for d in self.daemon_monitors:
                d.monitor()

            time.sleep(MONITOR_SLEEP_PERIOD_SECS)

    def nxo_active(self):
        return [d.enabled for d in self.daemon_monitors if d.type == 'powerregulatord' and d.enabled].count(True) != 0

    def log_control_heart_beat(self, this_string):
        # Log the heart beat message every so often, about once a minute.
        if this_string:
            # There's a string to log.
            loop_limit = NO_PRODUCTION_LOG_FREQUENCY_SECS / MONITOR_SLEEP_PERIOD_SECS
            if (self.throttle_no_production_log_messages == 0) or (self.throttle_no_production_log_messages > loop_limit):
                self.logger.info(this_string)
                self.throttle_no_production_log_messages = 0
            self.throttle_no_production_log_messages += 1

    def send_control_heartbeat(self):
        packed_data = None
        this_string = ""

        # This method called when NXO is not active and system is not String Sight.
        if self.gateway.operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE:
            # A heart beat command will be constructed and sent, which contains a curtailment command of 0%,
            # fallback and U-AIF data. Any issues with fallback and/or U-AIF data will be in the returned string.
            (packed_data, construction_problems) = construct_standby_heartbeat_packed_data()
            this_string = "Controlled heartbeat enabled - NO production allowed"
            if construction_problems:
                this_string += " (%s)" % construction_problems
            # In case the system is subsequently switched to observation mdoe:
            self.get_outta_jail_packet_sent = 0
        elif self.gateway.operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE:
            # Under observation mode, so send a get-outta-jail command to the inverters which will take them
            # out of gateway Control. But send this packet once only.
            #
            # TODO: should the GATEWAY_CTRL bit in INV_CTRL be audited at least once?
            #
            # Currently: GATEWAY_CTRL/INV_CTRL is not audited in the inverters. So the packet is sent a few times,
            #            as a best-effort to let inverters run feral.
            self.get_outta_jail_packet_sent += 1
            if self.get_outta_jail_packet_sent <= MAX_SEND_GET_OUTTA_JAIL_PACKETS:
                this_string = "MULTICAST INVERTERS GET OUTTA JAIL PACKET (#%s of %s)" % (self.get_outta_jail_packet_sent,
                                                                                         MAX_SEND_GET_OUTTA_JAIL_PACKETS)
                packed_data = construct_inverter_get_outta_jail_packed_data()
            else:
                packed_data = ""

        # Now send.
        if packed_data:
            multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            if not is_send_multicast_packet_and_close_socket(packed_data, multicast_ip, SG424_PRIVATE_UDP_PORT):
                this_string += "BUT *FAILED* TO MULTICAST"

        # The frequency of this log is controlled to avoid annoying the user that might be riveted
        # to the terminal log. This is mostly desirable when there's no production due to no demand.
        self.log_control_heart_beat(this_string)

    def reinit_daemon_monitors(self):
        daemons = [
            MeterDaemon,
            PowerRegulatorDaemon,
            ERSyncDaemon,
            IcpConDaemon,
            NotifyDaemon,
            NPLogDaemon,
            PowerFactorControlDaemon,
            EssManagerDaemon,
            SchedulerDaemon,
            PsaDaemon,
            ErptDaemon,
            MsgBrokerDaemon,
            GroupsDaemon
        ]

        for d in daemons:
            self.daemon_monitors += DaemonMonitor.find_or_create_all(d, extra={'no_restarts': self.no_restarts})
            for monitor in self.daemon_monitors:
                monitor.stop()

    def reinit_stringmon_audit(self):
        string_to_reinit_stringmon = "UPDATE %s SET %s = %s;" % \
                                     (DB_STRINGMON_CONTROL_TABLE,
                                      STRINGMON_CTL_TBL_AUDIT_STATUS_COLUMN_NAME,
                                      '%s')
        args = ['', ]
        prepared_act_on_database(EXECUTE, string_to_reinit_stringmon, args)

    def start_stringmon_audit(self):
        status = self.get_stringmon_status()
        if status == STRINGMON_START_COMMAND:
            try:
                stringid_script = Process(target=instantiate_stringmonscript, args=[])
                stringid_script.start()
            except Exception as error:
                error_string = 'Unable to launch StringMon audit script - %s' % error
                self.logger.exception(error_string)
                return False
        return True

    def start_pcs_unit_id_association(self):
        command = self.get_pcs_unit_id_assoc_command()
        if command == PUI_ASSOC_START_COMMAND:
            try:
                pui_assoc_script = Process(target=instantiate_puiassocscript, args=[])
                pui_assoc_script.start()
            except Exception as error:
                error_string = 'Unable to launch PCS UNIT ID ASSOC script - %s' % error
                self.logger.exception(error_string)
                return False
        return True

    def start_charger_association(self):
        command = self.get_charger_assoc_command()
        if command == CHARGER_ASSOC_START_COMMAND:
            try:
                charger_assoc_script = Process(target=instantiate_chargerassocscript(), args=[])
                charger_assoc_script.start()
            except Exception as error:
                error_string = 'Unable to launch CHARGER ASSOC script - %s' % error
                self.logger.exception(error_string)
                return False
        return True

    def get_upgrade_list(self):
        sql = "SELECT * FROM %s;" % DB_INVERTER_UPGRADE_TABLE
        result = prepared_act_on_database(FETCH_ALL, sql, ())
        if not result:
            result = []
        return result[:]

    def get_feed_discovery_list(self):
        sql = "SELECT * FROM %s;" % FEED_DISCOVERY_TABLE
        result = prepared_act_on_database(FETCH_ALL, sql, ())
        if not result:
            result = []
        return result[:]

    def get_stringmon_status(self):
        sql = 'select %s FROM %s;' % (STRINGMON_CTL_TBL_AUDIT_STATUS_COLUMN_NAME,
                                      DB_STRINGMON_CONTROL_TABLE)
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, [])
        except (MyDB.Error, MyDB.Warning) as dberr:
            error_string = 'Unable to get audit status - %s' % dberr
            self.logger.error(error_string)
            return None
        return result['audit_status']

    def get_pcs_unit_id_assoc_command(self):
        command = False
        sql = 'select %s FROM %s;' % (PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                      DB_PUI_ASSOCIATION_CONTROL_TABLE)
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, [])
            if result:
                command = result[PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
        except (MyDB.Error, MyDB.Warning) as dberr:
            error_string = 'Unable to get pui_assoc command - %s' % dberr
            self.logger.error(error_string)
            return None
        return command

    def get_charger_assoc_command(self):
        command = False
        sql = 'select %s FROM %s;' % (CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                      DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, [])
            if result:
                command = result[CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
        except (MyDB.Error, MyDB.Warning) as dberr:
            error_string = 'Unable to get charger_assoc command - %s' % dberr
            self.logger.error(error_string)
            return None
        return command

    def reinit_upgrade(self):
        # inverter_upgrade table: set upgrade_command = no_command
        select_string = "UPDATE %s SET %s = %s;" \
                        % (DB_INVERTER_UPGRADE_TABLE,
                           INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s")
        select_args = (INVERTER_UPGRADE_COMMAND_NO_COMMAND,)
        prepared_act_on_database(EXECUTE, select_string, select_args)

        # inverters table: set upgrade_status = no_action, upgrade_progress = 0
        select_string = "UPDATE %s  SET %s = %s,  %s = %s;" \
                        % (DB_INVERTER_NXO_TABLE,
                           INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                           INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s")
        select_args = (INVERTERS_UPGRADE_STATUS_NO_ACTION, 0)
        prepared_act_on_database(EXECUTE, select_string, select_args)

    def reinit_feed_discovery(self):
        # feed_discovery_control table: set command = no_command
        select_string = "UPDATE %s SET %s = %s" % (FEED_DISCOVERY_TABLE,
                                                   FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME, "%s")
        select_args = (FEED_DISCOVERY_COMMAND_NO_COMMAND,)
        prepared_act_on_database(EXECUTE, select_string, select_args)

        # fb_by_string table: delete all rows.
        select_string = "DELETE FROM %s;" % (FD_BY_STRING_TABLE)
        prepared_act_on_database(EXECUTE, select_string, ())

    def reinit_pui_assoc(self):
        # PCS Unit ID Association control table: set command = no_command, status = idle
        update_string = "UPDATE %s SET %s=%s,%s=%s" % (DB_PUI_ASSOCIATION_CONTROL_TABLE,
                                                       PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME, "%s",
                                                       PUI_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME, "%s")
        update_args = (PUI_ASSOC_NO_COMMAND_COMMAND, PUI_ASSOC_IDLE_STATUS)
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def reinit_charger_assoc(self):
        # Charger Association control table: set command = no_command, status = idle
        update_string = "UPDATE %s SET %s=%s,%s=%s" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE,
                                                       CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME, "%s",
                                                       CHARGER_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME, "%s")
        update_args = (CHARGER_ASSOC_NO_COMMAND_COMMAND, CHARGER_ASSOC_IDLE_STATUS)
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def check_feed_discovery_control_table(self):
        # Ensure the feed_discovery_control table has 1 entry. If this table is left empty,
        # it causes hiccups for other daemons such as power factor control and sis.
        fetch_results = []
        fetch_string = "SELECT COUNT(*) FROM %s;" % FEED_DISCOVERY_TABLE
        number_of_rows = prepared_act_on_database(FETCH_ROW_COUNT, fetch_string, ())
        if number_of_rows == 1:
            # The fd table is fine, nothing to do.
            pass
        else:
            # Might have more than 1 row (unlikely), or 0 rows. Modify this table for 1-row only.
            # First: empty the table.
            delete_string = "delete from %s;" % (FEED_DISCOVERY_TABLE)
            prepared_act_on_database(EXECUTE, delete_string, ())

            # Add the 1 row to this table:
            # insert into feed_discovery_control (command,runs,mode,number_of_strings,
            # current_string,start_time,completion_time)
            # values('no_command',0,'night',1,1,'1970-01-01 00:00:00','1970-01-01 00:00:00');
            fd_insert = "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s) " \
                        "VALUES (%s,%s,%s,%s,%s,%s,%s);" % (FEED_DISCOVERY_TABLE,
                                                            FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_RUNS_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_MODE_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_NUMBER_OF_STRINGS_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_CURRENT_STRING_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_START_TIME_COLUMN_NAME,
                                                            FEED_DISCOVERY_TBL_COMPLETION_TIME_COLUMN_NAME,
                                                            "%s", "%s", "%s", "%s", "%s", "%s", "%s")
            fd_args = ('no_command', 0, 'night', 1, 1, '1970-01-01 00:00:00', '1970-01-01 00:00:00')
            prepared_act_on_database(EXECUTE, fd_insert, fd_args)

    def start_upgrade(self):
        upgrade_to_run = []
        upgrade_list = self.get_upgrade_list()
        for task in upgrade_list:
            if str(task['upgrade_command']) == 'start_upgrade':
                upgrade_to_run.append(str(task['upgrade_command']))
        if len(upgrade_to_run) != 1:
            return

        for upgrade in upgrade_list:
            self.logger.info('upgrade_script = %s' % repr(upgrade))
            try:
                upgrade_script = Process(target=instantiate_upgrade_all_inverters_process, args=[])
                upgrade_script.start()
            except Exception as error:
                self.logger.exception('Unable to create upgrade script.')

    def start_feed_discovery(self):
        feed_discovery_to_run = []
        for task in self.get_feed_discovery_list():
            if str(task['command']) == 'start':
                feed_discovery_to_run.append(str(task['command']))
        if len(feed_discovery_to_run) != 1:
            return

        for discovery in feed_discovery_to_run:
            self.logger.info('start_feed_discovery: %s' % repr(discovery))
            try:
                discovery = Process(target=instantiate_feed_discovery, args=[])
                discovery.start()
            except Exception:
                self.logger.critical('Unable to create feed discovery:')

    def stop_daemons(self):
        if self.daemon_monitors:
            for d in self.daemon_monitors:
                d.stop()


# "fork" the process to work in a daemonized environment:
# http://stackoverflow.com/questions/881388/what-is-the-reason-for-performing-a-double-fork-when-creating-a-daemon
gateway_monitor = None
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    setproctitle.setproctitle('%s-monitord' % GATEWAY_PROCESS_PREFIX)
    gateway_monitor = GatewayMonitor(no_restarts=True)
    gateway_monitor.run()
else:
    try:
        pid = os.fork()
        if pid == -1:
            print("BYE 1")
            exit()
        elif pid == 0:
            print("BYE 2")
            exit()
        else:
            setproctitle.setproctitle('%s-monitord' % GATEWAY_PROCESS_PREFIX)
            gateway_monitor = GatewayMonitor()
            gateway_monitor.run()
    except Exception as ex:
        logger = setup_logger('GatewayMonitor')
        logger.exception("Unhandled exception caught in gateway monitor.")
        if gateway_monitor:
            gateway_monitor.stop_daemons()
        raise ex
