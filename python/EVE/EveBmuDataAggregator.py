#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: EVE Battery Management Unit Data Aggregator

@description: The Aggregator is tasked with , and updating the
              appropriate fields in the master info table for the GUI.

@copyright: Apparent Inc. 2018

@reference: 

@author: steve

@created: June 15, 2018
"""
from lib.db import prepared_act_on_database
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from alarm.alarm_constants import ALARM_ID_EVE_BMU_DATA_AGGREGATOR_RESTARTED
from ess.ess_daemon import EssDaemon

EVE_BMU_DATA_AGGREGATOR_DEFAULT_POLLING_SECONDS = 2
AGGREGATOR_SOCKET_TIMEOUT = 5.0


class EveBmuDataAggregatorDaemon(EssDaemon):
    polling_freq_sec = EVE_BMU_DATA_AGGREGATOR_DEFAULT_POLLING_SECONDS
    alarm_id = ALARM_ID_EVE_BMU_DATA_AGGREGATOR_RESTARTED

    def __init__(self, **kwargs):
        super(EveBmuDataAggregatorDaemon, self).__init__(**kwargs)
        self.db_errors = 0

    def get_bmu_ids(self):
        return ",".join([str(b) for b in self.ess_unit.get_bmu_list()])

    def aggregate_battery_indicators(self):
        sql = "SELECT AVG(current) AS current, AVG(voltage) AS voltage, " \
              "AVG(soc) AS soc, AVG(soh) AS soh, AVG(mosfet_temperature) AS mosfet_temperature,"\
              " AVG(environment_temperature) AS environment_temperature FROM " \
              "(SELECT * FROM aess_bmu_data WHERE state IS NULL OR " \
              "state NOT IN ('Maintenance', 'Fault') AND bmu_id IN (%s)) AS valid_bmu_data" % "%s"
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, [self.get_bmu_ids()])
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU averages.')

        indicator_dict = {}
        indicator_dict = response[0]
        update_sql = "UPDATE %s SET %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s WHERE ess_id = %s" % \
                     (DB_ESS_MASTER_INFO_TABLE,
                      ESS_MASTER_INFO_BATTERY_STACK_CURRENT_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_BATTERY_STACK_VOLTAGE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_BATTERY_STACK_SOH_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_TABLE_AVG_MOSFET_TEMPERATURE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_TABLE_AVG_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, "%s",
                      "%s")
        args = [indicator_dict[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME],
                indicator_dict[AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME],
                indicator_dict[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME],
                indicator_dict[AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME],
                indicator_dict[AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME],
                indicator_dict[AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME],
                self.ess_unit.id]
        try:
            response = prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU averages.')

    def calculate_min_cell_voltage(self):
        sql = "SELECT MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), " \
              "MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s), MIN(%s) FROM %s " \
              "WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" % \
              (AESS_BMU_DATA_TABLE_CELL_VOLTAGE_1_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_2_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_3_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_4_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_5_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_6_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_7_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_8_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_9_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_10_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_11_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_12_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_13_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_14_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_15_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_16_COLUMN_NAME,
               DB_AESS_BMU_DATA_TABLE_NAME,
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU cell averages.')
        min_cell_voltage_dict = response[0]
        min_cell_voltage = min(min_cell_voltage_dict.values())
        
        update_sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                              ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME, "%s",
                                                              ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [min_cell_voltage, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU min cell voltage.')

    def calculate_max_cell_voltage(self):
        sql = "SELECT MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), " \
              "MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s), MAX(%s) FROM %s " \
              "WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" % \
              (AESS_BMU_DATA_TABLE_CELL_VOLTAGE_1_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_2_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_3_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_4_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_5_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_6_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_7_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_8_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_9_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_10_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_11_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_12_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_13_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_14_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_15_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_VOLTAGE_16_COLUMN_NAME,
               DB_AESS_BMU_DATA_TABLE_NAME,
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU cell averages.')
        max_cell_voltage_dict = response[0]
        max_cell_voltage = max(max_cell_voltage_dict.values())
        
        update_sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                              ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME, "%s",
                                                              ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [max_cell_voltage, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU max cell voltage.')

    def calculate_min_cell_temp(self):
        sql = "SELECT MIN(%s), MIN(%s), MIN(%s), MIN(%s) from %s " \
              "WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" % \
              (AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_1_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_2_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_3_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_4_COLUMN_NAME,
               DB_AESS_BMU_DATA_TABLE_NAME,
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU min cell temps.')
        
        min_cell_temp_dict = {}
        min_cell_temp_dict = response[0]
        min_cell_temp = min(min_cell_temp_dict.values())
        update_sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                  ESS_MASTER_INFO_BATTERY_MIN_CELL_TEMPREATURE_COLUMN_NAME, "%s",
                                                                  ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [min_cell_temp, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU max cell temp.')
                                
    def calculate_max_cell_temp(self):
        sql = "SELECT MAX(%s), MAX(%s), MAX(%s), MAX(%s) from %s " \
              "WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" % \
              (AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_1_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_2_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_3_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_4_COLUMN_NAME,
               DB_AESS_BMU_DATA_TABLE_NAME,
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU max cell temps.')
        
        max_cell_temp_dict = response[0]
        max_cell_temp = max(max_cell_temp_dict.values())
        update_sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                    ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME, "%s",
                                                    ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [max_cell_temp, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU max cell temp.')

    def calculate_min_max_mosfet_temp(self):
        sql = "SELECT MIN(%s), MAX(%s) from %s WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" % \
              (AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME,
               AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME,
               DB_AESS_BMU_DATA_TABLE_NAME, 
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
               "%s",
               "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU mosfet temps.')

        mosfet_dict = response[0]
        min_mosfet_temp = min(mosfet_dict.values())
        max_mosfet_temp = max(mosfet_dict.values())
        update_sql = "UPDATE %s SET %s = %s, %s = %s WHERE %s = %s" % \
                     (DB_ESS_MASTER_INFO_TABLE,
                      ESS_MASTER_INFO_TABLE_MIN_MOSFET_TEMPERATURE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_TABLE_MAX_MOSFET_TEMPERATURE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [min_mosfet_temp, max_mosfet_temp, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU max cell temp.')

    def calculate_min_max_environment_temp(self):
        sql = "SELECT MIN(%s), MAX(%s) FROM %s WHERE %s != %s OR %s != %s AND bmu_id IN (%s)" \
              % (AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME,
                 AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME,
                 DB_AESS_BMU_DATA_TABLE_NAME,
                 AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
                 "%s",
                 AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
                 "%s",
                 "%s")
        args = [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE, self.get_bmu_ids()]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except Exception:
            self.logger.critical('Unable to query database.')
            raise DatabaseError('Failed to fetch BMU environment temps.')
        environment_dict = response[0]
        min_environment_temp = min(environment_dict.values())
        max_environment_temp = max(environment_dict.values())
        update_sql = "UPDATE %s SET %s = %s, %s = %s WHERE %s = %s" % \
                     (DB_ESS_MASTER_INFO_TABLE,
                      ESS_MASTER_INFO_TABLE_MIN_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_TABLE_MAX_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, "%s",
                      ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [min_environment_temp, max_environment_temp, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_sql, args)
        except Exception:
            self.logger.critical('Unable to update database.')
            raise DatabaseError('Failed to update BMU max cell temp.')
                                    
    def work(self):
        self.aggregate_battery_indicators()
        self.calculate_min_cell_voltage()
        self.calculate_max_cell_voltage()
        self.calculate_min_cell_temp()
        self.calculate_max_cell_temp()
        self.calculate_min_max_mosfet_temp()
        self.calculate_min_max_environment_temp()
