#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
"""
@module: EVE.EveBmuDaemonCLI.py

@description: EVE.EveBmuDaemonCLI is a command-line-interface module for the EveBmuDaemon.

@author:     steve

@copyright:  2018 Apparent Energy, Inc. All rights reserved.
"""

import argparse
import zmq
import time

from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.gateway_constants.EveBmuConstants import ParameterError
from lib.helpers import extract_cmd

from lib.logging_setup import *

logger = setup_logger('EveBmuDaemonCLI')

"""
Time to figure out what the user wants...
"""
parser = argparse.ArgumentParser(description='CLI for the EVE BMU Daemon')
parser.add_argument("--ess-unit-id", default=1, help="ESS unit id")
help = 'Set alarm <pack_overvoltage | cell_overvoltage | pack_undervoltage | cell_undervoltage> <bmu_id> <value>'
parser.add_argument("--set-alarm", nargs=3, help=help)
help = 'Set protection <pack_overvoltage | cell_overvoltage | pack_undervoltage | cell_undervoltage> <bmu_id> <value>'
parser.add_argument("--set-protection", nargs=3, help=help)
help = 'Update protection data <value>'
parser.add_argument("--update-protection-data", nargs=1, help=help)

clargs = parser.parse_args()
ess_unit_id, command, params = extract_cmd(clargs)
if command is None:
    raise ParameterError("None or invalid command given")
param_str = " ".join(params)
print 'param_str = %s' % param_str
cmd = "|".join([command, param_str])
print 'cmd = %s' % cmd

context = zmq.Context(1)
pub_socket = context.socket(zmq.PUB)
pub_socket.hwm = 100
pub_socket.setsockopt(zmq.LINGER, 0)
pub_socket.connect(MSG_BROKER_XSUB_URL)

time.sleep(PUB_CONNECT_WAIT_TIME)

topic = "essid%d-evebmucontroller" % int(ess_unit_id)
pub_socket.send_multipart([topic, cmd])
pub_socket.close()
context.term()

exit(0)
