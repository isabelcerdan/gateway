#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
@module: MsgBroker.py

@description: The MsgBroker is a well-known network point for publishers and
              subscribers to attach, to use as both as source and a destination.
              The MsgBroker maintains a message queue for each subscriber (client).

@copyright:  Apparent Energy, Inc. 2018

@created: July 12, 2018

@author: steve
'''

import zmq
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_PORT, \
                                               MSG_BROKER_XPUB_PORT, \
                                               MSG_BROKER_CAPTURE_PORT
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_MSG_BROKER_RESTARTED
from lib.logging_setup import setup_logger
import threading
from lib.exceptions import ProcessingError, ZmqError
import time


MSG_BROKER_UPDATE_INTERVAL = 5
MSG_BROKER_MAX_DB_ERRORS = 5
MSG_BROKER_SOCKET_TIMEOUT = 5000 #ms


class MsgBrokerThread(threading.Thread):
    alarm_id = ALARM_ID_MSG_BROKER_RESTARTED
    
    def __init__(self):
        threading.Thread.__init__(self, name='MsgBrkrThrd')
        self.ctx = None
        self.frontend = None
        self.backend = None
        self.capture = None
        self.db_errors = 0
        self.logger = setup_logger(__name__)

    def run(self):
        self.ctx = zmq.Context()
        # Socket facing Publisher
        self.logger.debug('Binding to XSUB socket...')
        try:
            self.frontend = self.ctx.socket(zmq.XSUB)
            self.frontend.bind("tcp://*:%s" % MSG_BROKER_XSUB_PORT)
        except zmq.ZMQBindError as error:
            error_string = 'Unable to create or bind to XSUB socket - %s' % repr(error)
            self.logger.exception(error_string)
            self.frontend.close()
            self.ctx.term()
            raise ZmqError(error_string)
        except Exception as error:
            self.logger.exception(error)
            raise ZmqError(error)

        self.logger.debug('Binding to XPUB socket...')
        try:
            self.backend = self.ctx.socket(zmq.XPUB)
            self.backend.bind("tcp://*:%s" % MSG_BROKER_XPUB_PORT)
        except zmq.ZMQError as error:
            error_string = 'Unable to create or bind to XPUB socket - %s' % repr(error)
            self.logger.exception(error_string)
            self.frontend.close()
            self.backend.close()
            self.ctx.term()
            raise ZmqError(error_string)
        except Exception as error:
            self.logger.exception(error)
            raise ZmqError(error)

        poller = zmq.Poller()
        poller.register(self.frontend, zmq.POLLIN)
        poller.register(self.backend, zmq.POLLIN)
        while True:
            events = dict(poller.poll(1000))
            if self.frontend in events:
                message = self.frontend.recv_multipart()
                self.logger.debug('Received a message on XSUB port [%s]' % repr(message))
                self.backend.send_multipart(message)
            if self.backend in events:
                message = self.backend.recv_multipart()
                self.logger.debug('Received a message on XPUB port [%s]' % repr(message))
                self.frontend.send_multipart(message)

        # We never get here...
        self.frontend.close()
        self.backend.close()
        self.ctx.term()


class MsgBrokerDaemon(Daemon):
    alarm_id = ALARM_ID_MSG_BROKER_RESTARTED
    always_enabled = True

    def __init__(self, **kwargs):
        super(MsgBrokerDaemon, self).__init__(**kwargs)
        self.thread = None

    def run(self):
        try:
            self.logger.debug('Creating MsgBroker thread...')
            self.thread = MsgBrokerThread()
            self.logger.debug('Created message broker thread...')
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)

        try:
            self.thread.start()
            self.logger.debug('Started message broker thread...')
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)

        while True:
            self.update()

            time.sleep(0.100)
            try:
                self.thread.join(10.0)
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            if not self.thread.is_alive():
                self.logger.error('MsgBrokerThread thread died...')
                self.thread = MsgBrokerThread()
                try:
                    self.logger.info('Restarting message broker thread...')
                    self.thread.start()
                except Exception as error:
                    self.logger.exception(error)
                    raise ProcessingError(error)
            else:
                self.logger.debug('MsgBroker thread is alive...')
                pass
            

if __name__ == '__main__':
    d = MsgBrokerDaemon(name='msgbrokerd', id_field='name')
    d.run()
