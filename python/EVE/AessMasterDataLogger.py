#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: AessMasterDataLogger.py

@description: Read the current database values for selected fields of the
              master info table and write them to a circular database file.  

@copyright: Apparent Inc.  2018

@created: September 21, 2018

@author: steve
"""
from ess.ess_daemon import EssDaemon
import collections
from lib.db import prepared_act_on_database, FETCH_ALL
from alarm.alarm_constants import ALARM_ID_AESS_MASTER_DATA_LOGGER_RESTARTED
from readings_logger.readings_logger import ReadingsLogger


class AessMasterDataLoggerDaemon(EssDaemon):
    polling_freq_sec = 5
    alarm_id = ALARM_ID_AESS_MASTER_DATA_LOGGER_RESTARTED

    def __init__(self, **kwargs):
        super(AessMasterDataLoggerDaemon, self).__init__(**kwargs)
        self.readings_logger = ReadingsLogger(basename="ess_master_info")

    def work(self):
        sql = "SELECT * FROM ess_master_info WHERE ess_id = %s" % "%s"
        master_info = prepared_act_on_database(FETCH_ALL, sql, [self.ess_unit.id])
        for registers in master_info:
            dict = collections.OrderedDict(registers)
            self.readings_logger.write_reading(dict)

