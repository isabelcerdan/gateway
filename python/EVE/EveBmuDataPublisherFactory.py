#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: Battery Management Unit Data Publisher

@description: The Data Publisher reads the operational registers of the EVE BMU
              and streams the results to the MsgBroker. 

@copyright: Apparent Energy Inc. 2018

@reference: 

@author: steve

@created: July 22, 2018
"""
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Defaults
from pymodbus.exceptions import ModbusException, ModbusIOException
from pymodbus.pdu import ExceptionResponse
from zmq.backend.cython.utils import ZMQError
import time
import zmq
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from lib.gateway_constants.ZmqConstants import *
from lib.logging_setup import *
import json
import threading
from alarm.alarm_constants import ALARM_ID_BMU_DATA_PUBLISHER_RESTARTED
from ess.ess_daemon import EssDaemon


class EveBmuDataPublisherThread(threading.Thread):
    def __init__(self, sds_id=None, zctx=None, **kwargs):
        self.ess_unit = kwargs.pop('ess_unit')
        threading.Thread.__init__(self, **kwargs)

        self.logger = setup_logger('EveBmuDatPubThr.sds%s' % sds_id)
        #self.logger.setLevel(logging.DEBUG)
        self.sds_id = sds_id
        self.sds_ip_addr = ''
        self.bmu_list = []
        self.db_errors = 0
        self.client = None
        self.polling_freq_sec = 1 #EVE_BMU_DEFAULT_POLLING_MS
        self.polling_modbus_id = EVE_MIN_MODBUS_ID
        self.register_contents_dict = {}
        self.pub_socket = None
        self.zctx = zctx

    def instantiate_modbus_client(self):
        self.logger.debug('Instantiating MODbus client at %s...' % self.sds_ip_addr)
        modbus_errs = 0
        Defaults.Timeout = 0.75
        while True:
            try:
                self.client = ModbusClient(self.sds_ip_addr, port=502)
            except Exception as error:
                modbus_errs += 1
                err_string = "MODbus error on EVE BMU address %s - %s" % \
                             (self.sds_ip_addr, repr(error))   
                self.logger.exception(err_string)
                if modbus_errs >= EVE_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    raise ConnectError('Maximum ModBus errors exceeded.')
                continue
            break
    
    def client_connect(self):
        """
        @note: calling client_connect() connects to the external unit.  It is the 
               responsibility of  the caller of this function to close the connection.
        """
        if self.client is None:
            self.logger.critical('No MODbus client!')
            raise ConnectError('Failure to find MODbus client')
        modbus_errs = 0
        while True:
            try:
                if not self.client.connect():
                    err_string = "MODbus connect failed for EVE BMU unit at %s" % self.sds_ip_addr
                    self.logger.error(err_string)
                    modbus_errs += 1
                    if modbus_errs >= EVE_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus connection errors exceeded.')
                        self.client.close()
                        raise ConnectError('Maximum MODbus connection errors exceeded.')
                    continue
                break
            except Exception as error:
                raise ConnectError('Connect failed - %s' % error)
        return True

    def get_sds_ip_addr(self, sds_id=None):
        sql = "SELECT %s FROM %s WHERE %s = %s" % (AESS_SERIAL_DEVICE_SERVER_TABLE_IP_ADDR_COLUMN_NAME,
                                                               DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME,
                                                               AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME, "%s")
        args = [sds_id,]
        try:
            response = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception:
            self.logger.critical('Unable to fetch run-time parameters.')
            raise DatabaseError('Failed to fetch run-time parameters.')
        try:
            self.sds_ip_addr = response[AESS_SERIAL_DEVICE_SERVER_TABLE_IP_ADDR_COLUMN_NAME]
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError('Unable to extract SDS IP address')
        self.logger.debug('SDS ip addr = %s for sds%d' % (self.sds_ip_addr, sds_id))
        return True

    def get_bmu_list(self):
        sql = "SELECT %s, %s from %s where %s = %s;" % (AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                        AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME,
                                                        DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                        AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME,
                                                        "%s")
        args = [self.sds_id,]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except:
            raise
        
        for resp in response:
            self.bmu_list.append((resp[AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME],
                                  resp[AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME]))

    def get_mfg_info(self, bmu_id=None, modbus_id=None):
        modbus_errors = 0
        reg_list = [VERSION_INFORMATION_REG_OFFSET, MODEL_SN_REG_OFFSET, PACK_SN_REG_OFFSET]
        for reg in reg_list:
            try:
                response = self.client.read_holding_registers(address=reg,
                                                              count=1,
                                                              unit=modbus_id)
            except ModbusException as ioerr:
                modbus_errors += 1
                self.logger.exception('MODbus exception on read operation on bmu id %d, modbus id %d -%s' % 
                                      (bmu_id, modbus_id, ioerr))
                if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            except ModbusIOException as ioerr:
                modbus_errors += 1
                self.logger.exception('MODbus IOexception on read operation on bmu id %d, modbus id %d -%s' % 
                                      (bmu_id, modbus_id, ioerr))
                if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            except Exception as ex:
                self.logger.exception("Unable to read MODbus register 0x%x - %s" % (reg, ex))
                raise
            #self.logger.debug('Response type = %s' % type(response))
            if response is None:
                self.logger.debug('No response for register 0x%x' % reg)
                self.register_contents_dict.clear()
                return
            elif isinstance(response, ExceptionResponse):
                modbus_errors += 1
                self.logger.exception('MODbus ExceptionResponse detected on bmu id %d, modbus id %d for register 0x%x' % \
                                      (bmu_id, modbus_id, reg))
                if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            elif isinstance(response, ModbusIOException):
                modbus_errors += 1
                self.logger.error('MODbus IO ExceptionResponse detected on bmu id %d, modbus id %d for register 0x%x' % \
                                  (bmu_id, modbus_id, reg))
                if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')            
            elif response.function_code < 0x80:
                self.register_contents_dict[reg] = response.registers[reg]
            else:
                self.logger.warning('Unknown MODbus response on read attempt for register 0x%x on bmu id %d' % \
                                    (bmu_id, reg))
    
        return True
    
    def monitor_bmu(self, bmu_id=None, modbus_id=None):
        modbus_errors = 0
        response = None
        reg = BATTERY_MODULE_CURRENT_REG_OFFSET
        reg_count = CHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_REG_OFFFSET+1
        try:
            response = self.client.read_holding_registers(address=reg,
                                                          count=reg_count,
                                                          unit=modbus_id)
        except ModbusException as ioerr:
            modbus_errors += 1
            self.logger.exception('MODbus exception on read operation on bmu id %d, modbus id %d -%s' % 
                                  (bmu_id, modbus_id, ioerr))
            if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                raise ReadError('Maximum MODbus errors exceeded')
        except ModbusIOException as ioerr:
            modbus_errors += 1
            self.logger.exception('MODbus IOexception on read operation on bmu id %d, modbus id %d -%s' % 
                                  (bmu_id, modbus_id, ioerr))
            if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                raise ReadError('Maximum MODbus errors exceeded')
        except Exception as ex:
            self.logger.exception("Unable to read MODbus register 0x%x - %s" % (reg, ex))
            raise
        #self.logger.debug('Response type = %s' % type(response))
        if response is None:
            self.logger.debug('No response for register 0x%x' % reg)
            self.register_contents_dict.clear()
            return
        elif isinstance(response, ExceptionResponse):
            modbus_errors += 1
            self.logger.exception('MODbus ExceptionResponse detected on bmu id %d, modbus id %d for register 0x%x' % \
                                  (bmu_id, modbus_id, reg))
            if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                raise ReadError('Maximum MODbus errors exceeded')
        elif isinstance(response, ModbusIOException):
            modbus_errors += 1
            self.logger.error('MODbus IO ExceptionResponse detected on bmu id %d, modbus id %d for register 0x%x' % \
                              (bmu_id, modbus_id, reg))
            if modbus_errors > EVE_MAX_MODBUS_ERRORS:
                raise ReadError('Maximum MODbus errors exceeded')            
        elif response.function_code < 0x80:
            for reg in range(BATTERY_MODULE_CURRENT_REG_OFFSET, CHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_REG_OFFFSET+1):
                self.register_contents_dict[reg] = response.registers[reg]
        else:
            self.logger.warning('Unknown MODbus response on read attempt for register 0x%x on bmu id %d' % \
                                (bmu_id, reg))
    
        return True

    def pub_mfg_info(self):
        '''
        Spin through all the BMUs, extracting manufacturing information.
        '''
        topic_base = 'bmu'
        while True:
            for entry in self.bmu_list:
                bmu_id, modbus_num = entry
                try:
                    self.get_mfg_info(bmu_id, modbus_num)
                except:
                    raise

                if self.register_contents_dict is not None:
                    try:
                        topic = 'bmu%d#' % int(bmu_id)
                        #self.logger.debug('topic = %s' % topic)
                        self.pub_socket.send_multipart([topic, json.dumps(self.register_contents_dict)])
                    except Exception as error:
                        self.logger.exception(error)
                        raise ProcessingError(error)
                self.register_contents_dict.clear()

    def run(self):
        self.logger.info("EVE BMU Data Publisher thread sds%d starting..." % self.sds_id)

        try:
            self.get_sds_ip_addr(self.sds_id)
        except:
            raise

        try:
            self.instantiate_modbus_client()
        except:
            raise
        
        try:
            self.get_bmu_list()
        except:
            raise

        """
        Socket to send messages to the Gateway Broker.
        """
        connect_err_cnt = 0
        while True:
            try:
                """
                @note: When setting up the publisher socket, set the LINGER option
                       to zero, to avoid having the process hanging around
                       trying to deliver messages after it's socket has been closed.
                       
                       Set the high water mark for the publisher socket to 100
                       messages, to avoid gobbling up memory if one or more of the
                       subscriber processes fails and is not restarted.
                """
                self.pub_socket = self.zctx.socket(zmq.PUB)
                self.pub_socket.hwm = EVE_DATA_PUBLISHER_HWM
                self.pub_socket.setsockopt(zmq.LINGER, 0)
                url = MSG_BROKER_XSUB_URL
                self.logger.info('Connecting to %s' % url)
                self.pub_socket.connect(url)
                time.sleep(PUB_CONNECT_WAIT_TIME)
            except Exception as error:
                self.logger.exception(error)
                connect_err_cnt += 1
                if connect_err_cnt <= EVE_MAX_ZMQ_ERRORS:
                    error_string = 'Unable to create TCP send socket for BMU data - %s' \
                                    % repr(error)
                    self.logger.error(error_string)
                    self.pub_socket.close()
                    self.zctx.term()
                    return False
                time.sleep(EVE_CONNECT_WAIT_TIME)            
            else:
                break

        """
        Main monitor loop.  Walk the MODbus chain from 1 - 11.  Lather.  Rinse.  Repeat.
        """
        info_skip_cnt = 2
        topic_base = 'bmu'
        while True:
            for entry in self.bmu_list:
                # start_time = timeit.default_timer()
                bmu_id, modbus_num = entry
                try:
                    self.monitor_bmu(bmu_id, modbus_num)
                except:
                    raise

                if self.register_contents_dict is not None:
                    try:
                        topic = "%s%s#" % (topic_base, str(bmu_id))
                        #self.logger.debug('Sending %s- %s' % (topic, repr(self.register_contents_dict)))
                        self.pub_socket.send_multipart([topic, json.dumps(self.register_contents_dict)])
                    except Exception as error:
                        self.logger.exception(error)
                        raise ZmqError(error)
                self.register_contents_dict.clear()
                
                # elapsed = timeit.default_timer() - start_time
                # print 'elapsed time %s = %f' % (topic, elapsed)

            ''' Uncomment skip count decrement to get mfg info. '''
            #info_skip_cnt -= 1
            if info_skip_cnt <= 0:
                self.pub_mfg_info()
                info_skip_cnt = 3600  # Works out to about 2 hours or so in clock time.
                
        self.pub_socket.close()    
        self.zctx.close()
        self.zctx.term()        

"""
End of BMU Publisher thread logic
"""


class EveBmuDataPublisherFactoryDaemon(EssDaemon):
    polling_freq_sec = EVE_BMU_DEFAULT_POLLING_FREQ
    alarm_id = ALARM_ID_BMU_DATA_PUBLISHER_RESTARTED

    def __init__(self, sds_id=None, zctx=None, **kwargs):
        super(EveBmuDataPublisherFactoryDaemon, self).__init__(**kwargs)
        self.thread_list = []
        self.sds_list = []
        self.db_errors = 0

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s WHERE %s = %s" % (AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME,
                                                   DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME,
                                                   AESS_SERIAL_DEVICE_SERVER_TABLE_ESS_UNIT_ID_COLUMN_NAME, "%s")
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, [self.ess_unit.id])
        except Exception:
            self.logger.critical('Unable to fetch SDS operational parameters.')
            raise DatabaseError('Failed to fetch SDS operational parameters.')
        
        for resp in response:
            self.sds_list.append(resp[AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME])
        
    def run(self):
        self.update()

        try:
            self.get_operational_parameters()
        except:
            raise
        
        try:
            zctx = zmq.Context()            
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        for sds_id in self.sds_list:
            thread_name = 'sds%d' % int(sds_id)
            thread = EveBmuDataPublisherThread(
                        sds_id,
                        zctx,
                        name=thread_name,
                        ess_unit=self.ess_unit)
            try:
                self.logger.debug('Starting thread sds%d' % sds_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)
        
        """
        @note: The factory joins a thread in its list every second, taking approximately
               30 seconds to process the list.
        """
        while True:
            self.update()
            
            try:
                for thread in self.thread_list:
                    try:
                        thread_name = thread.getName()
                        thread.join(1.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('BmuDataPublisher thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        sds_num = int(thread_name[3:])
                        thread = EveBmuDataPublisherThread(
                                        sds_num,
                                        zctx,
                                        name=thread_name,
                                        ess_unit=self.ess_unit)
                        try:
                            self.logger.debug('Restarting data publisher thread sds%d' % sds_num)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                break

        self.logger.info('BmuDataPublisherFactory leaving main control loop')
        return False
