#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: EveBmuDataSubscriber.py

@description: Process the raw BMU data for one or more MODbus strings of a single rack.

@created: July 19, 2018

@copyright: Apparent Energy Inc.  2018

@author: steve
"""

import zmq
from lib.gateway_constants.ZmqConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from ess.ess_daemon import EssDaemon
import json
from lib.logging_setup import *
import numpy as np
from zmq.backend.cython.utils import ZMQError
import threading
from alarm.alarm_constants import ALARM_ID_BMU_DATA_SUBSCRIBER_RESTARTED
import time

SUBSCRIBER_SOCKET_TIMEOUT = 5000 #ms
SUBSCRIBER_UPDATE_INTERVAL = 5 #sec

bmu_register_mapping_dict = {
    BATTERY_MODULE_CURRENT_REG_OFFSET : AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
    BATTERY_MODULE_VOLTAGE_REG_OFFSET : AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME,
    BATTERY_MODULE_SOC_REG_OFFSET : AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME,
    BATTERY_MODULE_SOH_REG_OFFSET : AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME,
    REMAINING_CAPACITY_REG_OFFSET : AESS_BMU_DATA_TABLE_REMAINING_CAPACITY_COLUMN_NAME,
    FULL_CHARGED_CAPACITY_REG_OFFSET : AESS_BMU_DATA_TABLE_FULL_CHARGED_CAPACITY_COLUMN_NAME,
    DESIGN_CAPACITY_REG_OFFSET : AESS_BMU_DATA_TABLE_DESIGN_CAPACITY_COLUMN_NAME,
    BATTERY_CYCLE_COUNT_REG_OFFSET : AESS_BMU_DATA_TABLE_CYCLE_COUNT_COLUMN_NAME,
    CELL_VOLTAGE_1_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_1_COLUMN_NAME,
    CELL_VOLTAGE_2_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_2_COLUMN_NAME,
    CELL_VOLTAGE_3_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_3_COLUMN_NAME,
    CELL_VOLTAGE_4_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_4_COLUMN_NAME,
    CELL_VOLTAGE_5_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_5_COLUMN_NAME,
    CELL_VOLTAGE_6_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_6_COLUMN_NAME,
    CELL_VOLTAGE_7_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_7_COLUMN_NAME,
    CELL_VOLTAGE_8_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_8_COLUMN_NAME,
    CELL_VOLTAGE_9_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_9_COLUMN_NAME,
    CELL_VOLTAGE_10_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_10_COLUMN_NAME,
    CELL_VOLTAGE_11_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_11_COLUMN_NAME,
    CELL_VOLTAGE_12_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_12_COLUMN_NAME,
    CELL_VOLTAGE_13_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_13_COLUMN_NAME,
    CELL_VOLTAGE_14_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_14_COLUMN_NAME,
    CELL_VOLTAGE_15_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_15_COLUMN_NAME,
    CELL_VOLTAGE_16_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_VOLTAGE_16_COLUMN_NAME,
    CELL_TEMPERATURE_1_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_1_COLUMN_NAME,
    CELL_TEMPERATURE_2_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_2_COLUMN_NAME,
    CELL_TEMPERATURE_3_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_3_COLUMN_NAME,
    CELL_TEMPERATURE_4_REG_OFFSET : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_4_COLUMN_NAME,
    MOSFET_TEMPERATURE_REG_OFFSET : AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME,
    ENVIRONMENT_TEMPERATURE_REG_OFFSET : AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME
}


class EveBmuDataSubscriberThread(threading.Thread):
    xlat_cell_num_to_col_dict = {
        1 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_1_COLUMN_NAME,
        2 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_2_COLUMN_NAME,
        3 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_3_COLUMN_NAME,
        4 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_4_COLUMN_NAME,
        5 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_5_COLUMN_NAME,
        6 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_6_COLUMN_NAME,
        7 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_7_COLUMN_NAME,
        8 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_8_COLUMN_NAME,
        9 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_9_COLUMN_NAME,
        10 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_10_COLUMN_NAME,
        11 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_11_COLUMN_NAME,
        12 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_12_COLUMN_NAME,
        13 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_13_COLUMN_NAME,
        14 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_14_COLUMN_NAME,
        15 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_15_COLUMN_NAME,
        16 : EVE_BMU_MONITOR_DATA_CELL_VOLTAGE_16_COLUMN_NAME
    }
    
    xlat_temp_sensor_num_to_col_dict = {
        1 : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_1_COLUMN_NAME,
        2 : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_2_COLUMN_NAME,
        3 : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_3_COLUMN_NAME,
        4 : AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_4_COLUMN_NAME
    }
        
    def __init__(self, sds_id=None, ctx=None, **kwargs):
        self.ess_unit = kwargs.pop('ess_unit')
        threading.Thread.__init__(self, **kwargs)
        self.shutdown_flag = threading.Event()
        self.ctx = ctx
        self.sds_id = sds_id  
        self.register_update_string = ''
        self.subscriber = None
        self.bmu_list = []
        self.bmu_data = {}
        self.bmu_id = None
        self.db_values_dict = {}

        self.logger = setup_logger('EveBmuDatSubThr.sds%d' % sds_id)
        #self.logger.setLevel(logging.DEBUG)

        self.bmu_register_set_dict = {
            BATTERY_MODULE_CURRENT_REG_OFFSET  : self.process_battery_pack_current,
            BATTERY_MODULE_VOLTAGE_REG_OFFSET  : self.process_battery_pack_voltage,
            BATTERY_MODULE_SOC_REG_OFFSET      : self.process_soc,
            BATTERY_MODULE_SOH_REG_OFFSET      : self.process_soh,
            REMAINING_CAPACITY_REG_OFFSET      : self.process_remaining_capacity,
            FULL_CHARGED_CAPACITY_REG_OFFSET   : self.process_full_charged_capacity,
            DESIGN_CAPACITY_REG_OFFSET         : self.process_design_capacity,
            BATTERY_CYCLE_COUNT_REG_OFFSET     : self.process_battery_cycle_count,
            MOSFET_TEMPERATURE_REG_OFFSET      : self.process_mosfet_temperature,
            ENVIRONMENT_TEMPERATURE_REG_OFFSET : self.process_environment_temperature
        }

    def build_update_string(self):
        if not self.db_values_dict:
            self.logger.warning('Empty database values dictionary.')
            return ''
        
        update_list = ["INSERT INTO "]
        update_list.append(DB_AESS_BMU_DATA_TABLE_NAME)
        update_list.append(" (%s, " % AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
        for field_name in self.db_values_dict.iterkeys():
            try:
                update_list.append("%s" % field_name)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") VALUES (%d, " % self.bmu_id)
        for register_value in self.db_values_dict.itervalues():
            try:
                update_list.append("%f" % register_value)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") ON DUPLICATE KEY UPDATE ")
        
        for field_name, register_value in self.db_values_dict.iteritems():
            try:
                update_list.append('%s=%f ' % (field_name, register_value))
                update_list.append(', ')
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
        update_list.pop()  # pop the final comma
        update_list.append(";")
        update_str = ''.join(update_list)
        #self.logger.debug('SQL = %s' % update_str)

        return update_str[:]
        
    def test_bit(self, register, bit_pos):                            
        if (register & (1 << bit_pos)):
            return True
        else:
            return False

    def process_battery_pack_current(self, register):
        current = float(np.int16(register)) * float(BATTERY_MODULE_CURRENT_SCALING_FACTOR)
        #self.logger.debug('Battery pack current: %.3fA' % current)
        self.db_values_dict[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME] = current

    def process_battery_pack_voltage(self, register):
        voltage = float(np.uint16(register)) * float(BATTERY_MODULE_VOLTAGE_SCALING_FACTOR)
        #self.logger.debug('Battery pack voltage: %.3fV' % voltage)
        self.db_values_dict[AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME] = voltage
    
    def determine_bmu_state(self, soc=None, bmu_id=None):
        sql = "SELECT %s from %s WHERE %s = %s;" % (AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
                                                    DB_AESS_BMU_DATA_TABLE_NAME,
                                                    AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                    "%s")
        args = [bmu_id,]
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception as error:
            raise DatabaseError(error)
        
        if result[AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME] == EVE_BMU_MAINTENANCE_STATE or \
           result[AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME] == EVE_BMU_FAULT_STATE:
            return
        
        ''' Fetch the ess unit id. '''
        sql = "SELECT %s from %s where %s = %s;" % (PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME,
                                                    DB_PCS_UNITS_TABLE_NAME,
                                                    PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                    "%s")
        args = [bmu_id,]
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception as error:
            raise DatabaseError(error)
        if result[PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME] is None:
            self.logger.warning('BMU %s does not have an associated PCS_UNIT' % bmu_id)
        else:
            ess_unit_id = result[PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME]

        ''' Fetch the charge and discharge limits. '''
        sql = "SELECT %s, %s from %s WHERE %s = %s" % (ESS_UNITS_TABLE_SOC_CHARGE_LIMIT_COLUMN_NAME,
                                                        ESS_UNITS_TABLE_SOC_DISCHARGE_LIMIT_COLUMN_NAME,
                                                        DB_ESS_UNITS_TABLE,
                                                        ESS_UNITS_ID_COLUMN_NAME,
                                                        "%s")
        args = [ess_unit_id,]
        try:
            result = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception as error:
            raise DatabaseError(error)
        
        state = EVE_BMU_UNINITIALIZED_STATE
        soc = float(soc/100.0)
        if soc >= float(result[ESS_UNITS_TABLE_SOC_CHARGE_LIMIT_COLUMN_NAME]):
            state = EVE_BMU_CHARGED_RTD_STATE
        elif soc <= float(result[ESS_UNITS_TABLE_SOC_DISCHARGE_LIMIT_COLUMN_NAME]):
            state = EVE_BMU_DISCHARGED_RTC_STATE
        else:
            state = EVE_BMU_UNRESTRICTED_STATE
        sql = "UPDATE %s SET %s = %s WHERE %s = %s;" % (DB_AESS_BMU_DATA_TABLE_NAME,
                                                        AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME,
                                                        "%s",
                                                        AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                        "%s")
        args = [state, bmu_id]
        try:
            prepared_act_on_database(EXECUTE, sql, args)
        except Exception as error:
            raise DatabaseError(error)
        
    def process_soc(self, register):   
        soc = float(np.uint8(register)) * float(BATTERY_MODULE_SOC_SCALING_FACTOR)
        #self.logger.debug('SOC: %.2f%%' % soc)
        self.db_values_dict[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME] = soc

    def process_soh(self, register):   
        soh = float(np.uint8(register)) * float(BATTERY_MODULE_SOH_SCALING_FACTOR)
        #self.logger.debug('SOH: %.2f%%' % soh)
        self.db_values_dict[AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME] = soh
    
    def process_remaining_capacity(self, register):
        remaining_capacity = float(np.uint16(register)) * float(REMAINING_CAPACITY_SCALING_FACTOR)
        #self.logger.debug('Remaining Capacity = %.2f%%' % remaining_capacity)
        self.db_values_dict[AESS_BMU_DATA_TABLE_REMAINING_CAPACITY_COLUMN_NAME] = remaining_capacity
        
    def process_design_capacity(self, register):
        design_capacity = float(np.uint16(register)) * float(DESIGN_CAPACITY_SCALING_FACTOR)
        #self.logger.debug('Design Capacity = %.2f%%' % design_capacity)
        self.db_values_dict[AESS_BMU_DATA_TABLE_DESIGN_CAPACITY_COLUMN_NAME] = design_capacity
        
    def process_full_charged_capacity(self, register):
        full_charged_capacity = float(np.uint16(register)) * float(FULL_CHARGED_CAPACITY_SCALING_FACTOR)
        #self.logger.debug('Full charged capacity: %.2fAH' % full_charged_capacity)
        self.db_values_dict[AESS_BMU_DATA_TABLE_FULL_CHARGED_CAPACITY_COLUMN_NAME] = full_charged_capacity
        
    def process_battery_cycle_count(self, register):
        cycle_count = np.uint16(register)
        #self.logger.debug('Cycle count: %d' % cycle_count)
        self.db_values_dict[AESS_BMU_DATA_TABLE_CYCLE_COUNT_COLUMN_NAME] = int(cycle_count)
        
    def process_mosfet_temperature(self, register):
        mosfet_temperature = float(np.int16(register)) * float(MOSFET_TEMPERATURE_SCALING_FACTOR)
        #self.logger.debug('MOSFET temperature: %.3fC' % mosfet_temperature)
        self.db_values_dict[AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME] = mosfet_temperature
    
    def process_environment_temperature(self, register):
        environment_temperature = float(np.int16(register)) * float(ENVIRONMENT_TEMPERATURE_SCALING_FACTOR)
        #self.logger.debug('Environment temperature: %.3fC' % environment_temperature)
        self.db_values_dict[AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME] = environment_temperature
        
    def process_cell_voltage(self, register):
        cell_num = register - (CELL_VOLTAGE_1_REG_OFFSET - 1)
        reg_value = self.bmu_data[str(register)]
        cell_voltage = float(np.uint16(reg_value)) * CELL_VOLTAGE_SCALING_FACTOR
        #self.logger.debug('Cell %d voltage = %.3fV' % (cell_num, cell_voltage))
        self.db_values_dict[EveBmuDataSubscriberThread.xlat_cell_num_to_col_dict[cell_num]] = cell_voltage
    
    def process_cell_temperature(self, register):
        cell_num = register - (CELL_TEMPERATURE_1_REG_OFFSET - 1)
        reg_value = self.bmu_data[str(register)]
        cell_temperature = float(np.uint16(reg_value)) * CELL_TEMPERATURE_SCALING_FACTOR
        #self.logger.debug('Cell %d temperature = %.3fC' % (cell_num, cell_temperature))
        self.db_values_dict[EveBmuDataSubscriberThread.xlat_temp_sensor_num_to_col_dict[cell_num]] = cell_temperature
    
    def process_register_set(self):
        for register in self.bmu_data.iterkeys():
            #self.logger.debug('Processing register %s...' % register)
            if int(register) >= CELL_VOLTAGE_1_REG_OFFSET and \
               int(register) <= CELL_VOLTAGE_16_REG_OFFSET:
                self.process_cell_voltage(int(register))
            elif int(register) >= CELL_TEMPERATURE_1_REG_OFFSET and \
                 int(register) <= CELL_TEMPERATURE_4_REG_OFFSET:
                self.process_cell_temperature(int(register))
            elif int(register) >= RESERVED_8 and int(register) <= RESERVED_14:
                pass
                #self.logger.debug('Skipping register %s...' % register)
            elif int(register) >= RESERVED_37 and int(register) <= CHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_REG_OFFFSET:
                pass
                #self.logger.debug('Skipping register %s...' % register)
            else:
                try:
                    #self.logger.debug('Processing register %s...' % register)
                    self.bmu_register_set_dict[int(register)](self.bmu_data[register])
                except KeyError as error:
                    self.logger.exception('Error processing register %s' % register)
                    raise ProcessingError(error)

        try:
            sql = self.build_update_string()
        except:
            raise
        
        if sql == '':
            return
        
        try:
            prepared_act_on_database(EXECUTE, sql, [])
        except:
            raise

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s WHERE %s = %s;" % (AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                    DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                    AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME,
                                                    "%s")
        args = [self.sds_id,]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except:
            raise
        
        for resp in response:
            self.bmu_list.append(resp['bmu_id'])

    def run(self):
        self.logger.info('BmuDatSubThr [sds%d] starting.' % self.sds_id)

        try:
            self.subscriber = self.ctx.socket(zmq.SUB)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        try:
            self.subscriber.connect(MSG_BROKER_XPUB_URL)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        """
        @note: the wildcard logic is selecting for all possible combinations of
               sds_id.bmu_id.  Individual subscriber threads will probably
               focus on one SDS ID/one MODbus chain. 
        for wildcard_rack_num in ("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"):
            for wildcard_sds_num in ("0", "1"):
                for wildcard_module_num in ("1", "2", "3",  "4", "5", "6", "7", "8", "9", "10", "11"):
                    wildcard_str = "rack%s.sds%s.module%s" % (wildcard_rack_num, wildcard_sds_num, wildcard_module_num) 
                    self.subscriber.setsockopt(zmq.SUBSCRIBE, wildcard_str)
                    logger.debug('Subscribing to %s...' % wildcard_str )        
        """
        try:
            self.get_operational_parameters()
        except:
            raise
        
        try:
            for bmu_id in self.bmu_list:
                wildcard_str = "bmu%d#" % bmu_id 
                self.subscriber.setsockopt(zmq.SUBSCRIBE, wildcard_str)
                self.logger.debug('Subscribing to %s...' % wildcard_str )        
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)

        poller = zmq.Poller()
        poller.register(self.subscriber, zmq.POLLIN)

        count = 0
        while True:
            if self.shutdown_flag.is_set():
                self.logger.info('Received shutdown signal')
                break    

            zsocks = dict(poller.poll(SUBSCRIBER_SOCKET_TIMEOUT))
            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                try:
                    [topic, data] = self.subscriber.recv_multipart()
                except zmq.ZMQError as e:
                    self.logger.exception('recv_multipart failed for %s' % topic)
                    if e.errno == zmq.ETERM:
                        break           # Interrupted
                except KeyboardInterrupt:
                    break
                #self.logger.debug('Received a message for %s - [%s]...' % (topic, repr(data)))
                try:
                    start = time.time()
                    self.bmu_id = int(topic[3:-1])
                    self.bmu_data = json.loads(data)
                    self.process_register_set()
                    done = time.time()
                    elapsed = done - start
                    self.logger.debug('elapsed time = %s for BMU %s' % (elapsed, self.bmu_id))
                except:
                    raise
                self.db_values_dict.clear()

        self.subscriber.close()
        self.ctx.term()
        self.logger.debug("Data subscriber thread received %d messages" % count)
    

class EveBmuDataSubscriberFactoryDaemon(EssDaemon):
    polling_freq_sec = EVE_BMU_DEFAULT_POLLING_FREQ
    alarm_id = ALARM_ID_BMU_DATA_SUBSCRIBER_RESTARTED

    def __init__(self, **kwargs):
        super(EveBmuDataSubscriberFactoryDaemon, self).__init__(**kwargs)
        self.thread_list = []
        self.sds_list = []
        self.bmu_list = []
        self.db_errors = 0

    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread__stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def get_operational_parameters(self):
        self.logger.debug('getting operational data for EveBmuDataSubscriberFactoryDaemon...')
        sql = "SELECT %s FROM %s WHERE %s = %s" % (AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME,
                                                   DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME,
                                                   AESS_SERIAL_DEVICE_SERVER_TABLE_ESS_UNIT_ID_COLUMN_NAME, "%s")
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, [self.ess_unit.id])
        except Exception:
            self.logger.critical('Unable to fetch SDS operational parameters.')
            raise DatabaseError('Failed to fetch SDS operational parameters.')
        print response
        for sid in response:
            self.sds_list.append(sid['sds_id'])
        #print 'sds_list', self.sds_list
        
    def run(self):
        self.update()

        try:
            self.get_operational_parameters()
        except:
            raise
        
        try:
            zctx = zmq.Context.instance(1)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        for sds_id in self.sds_list:
            try:
                thread_name = 'sds%d' % int(sds_id)
                thread = EveBmuDataSubscriberThread(sds_id, zctx, name=thread_name, ess_unit=self.ess_unit)
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            
            try:
                self.logger.debug('Starting data subscriber thread sds%s' % sds_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)

        """
        @note: The factory joins a thread in its list every second, taking approximately
               30 seconds to process the list.
        """
        while True:
            self.update()

            try:
                for thread in self.thread_list:
                    thread_name = ''
                    try:
                        thread_name = thread.getName() 
                        thread.join(1.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('EveBmuDataSubscriber thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        sds_num = int(thread_name[3:])
                        thread = EveBmuDataSubscriberThread(sds_num, zctx, name=thread_name, ess_unit=self.ess_unit)
                        try:
                            self.logger.debug('Restarting data subscriber thread sds%s' % sds_num)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                break
                
        self.logger.info('EveBmuDataSubscriberFactory fell through main loop')
        return False
