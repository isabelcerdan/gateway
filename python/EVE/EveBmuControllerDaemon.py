"""
@module: EveBmuControllerDaemon.py

@description: Program the alarm and protection registers in the EVE BMU.

@reference: Technical Specification of LiFePO4 (51.2V 90Ah), March 2, 2018

@copyright: Apparent Energy Inc.  2018

@created: Aug 28, 2018

@author: steve
"""

import signal
import zmq
import time
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Defaults
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XPUB_URL
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from lib.logging_setup import *
from zmq.backend.cython.utils import ZMQError
from ess.ess_daemon import EssDaemon
from alarm.alarm_constants import ALARM_ID_EVE_BMU_DAEMON_RESTARTED

CONTROLLER_SOCKET_TIMEOUT = 5000  # ms
EVE_BMU_MAX_MODBUS_ERRORS = 5

def sig_handler(signum, frame):
    raise SignalExit("Caught signal %d" % signum)


class EveBmuControllerDaemon(EssDaemon):
    """
    The EveBmuDaemon waits patiently on a POLLER for commands.
    """

    update_timeout = 60.0
    alarm_id = ALARM_ID_EVE_BMU_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        """
        Constructor
        """
        super(EveBmuControllerDaemon, self).__init__(**kwargs)
        self.logger.info("Instantiating EVE BMU Controller Daemon...")
        self.bmu_id = None
        self.sds_id = None
        self.sds_ip_addr = None
        self.sds_sem_name = None
        self.sds_sem = None
        self.client = None
        self.zctx = None
        self.subscriber = None
        self.poller = None
        self.topic = "essid%d-evebmucontroller" % self.ess_unit.id
        self.zmq_errors = 0
        self.db_errors = 0
        self.register_contents_dict = {}
        self.db_values_dict = {}
        self.command_dict = \
            {
                'set_alarm': self.set_alarm,
                'set_protection': self.set_protection
            }
        self.alarm_func_dict = \
            {
                'pack_overvoltage': self.set_pack_overvoltage_alarm,
                'cell_overvoltage': self.set_cell_overvoltage_alarm,
                'pack_undervoltage': self.set_pack_undervoltage_alarm,
                'cell_undervoltage': self.set_cell_undervoltage_alarm,
            }
        self.protection_func_dict = \
            {
                'pack_overvoltage': self.set_pack_overvoltage_protection,
                'cell_overvoltage': self.set_cell_overvoltage_protection,
                'pack_undervoltage': self.set_pack_undervoltage_protection,
                'cell_undervoltage': self.set_cell_undervoltage_protection
            }

    def instantiate_modbus_client(self, sds_ip_addr):
        self.logger.debug('Instantiating MODbus client at %s...' % sds_ip_addr)
        modbus_errs = 0
        Defaults.Timeout = 0.75
        while True:
            try:
                self.client = ModbusClient(sds_ip_addr, port=502)
            except Exception as error:
                modbus_errs += 1
                err_string = "MODbus error on EVE BMU address %s - %s" % \
                             (sds_ip_addr, repr(error))
                self.logger.exception(err_string)
                if modbus_errs >= EVE_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    raise ConnectError('Maximum ModBus errors exceeded.')
                continue
            break

    def client_connect(self):
        """
        @note: calling client_connect() connects to the external unit.  It is the 
               responsibility of  the caller of this function to close the connection.
        """
        if self.client is None:
            self.logger.critical('No MODbus client!')
            raise ConnectError('Failure to find MODbus client')
        modbus_errs = 0
        while True:
            try:
                if not self.client.connect():
                    err_string = "MODbus connect failed for EVE BMU"
                    self.logger.error(err_string)
                    modbus_errs += 1
                    if modbus_errs >= EVE_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus connection errors exceeded.')
                        self.client.close()
                        raise ConnectError('Maximum MODbus connection errors exceeded.')
                    continue
                break
            except Exception as error:
                raise ConnectError('Connect failed - %s' % error)
        return True

    def close_modbus_client(self):
        try:
            self.client.close()
        except:
            raise ConnectError('Unable to disconnect MODbus client')

    def write_holding_registers(self, offset=None, modbus_id=None, contents=None):
        if offset is None or contents is None or modbus_id is None:
            raise ParameterError('Invalid input parameters.')

        modbus_errs = 0
        while True:
            try:
                self.client.write_register(offset,
                                           count=1,
                                           value=contents,
                                           unit=modbus_id)
            except Exception as error:
                err_string = 'write_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= EVE_BMU_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus write errors exceeded.')
                    self.close_modbus_client()
                    raise WriteError('Maximum MODbus write errors per transaction exceeded.')

                """
                Close the connection, re-connect and try again.
                """
                try:
                    self.close_modbus_client()
                    self.client_connect()
                except:
                    raise ConnectError('Unable to reconnect to unit after MODbus write error')
            else:
                break

        try:
            self.client.close()
        except Exception as error:
            raise ProcessingError(error)

        return True

    def get_sds_ip_addr(self, sds_id=None):
        sql = "SELECT %s FROM %s WHERE %s = %s;" % (AESS_SERIAL_DEVICE_SERVER_TABLE_IP_ADDR_COLUMN_NAME,
                                                    DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME,
                                                    AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME, "%s")
        args = [sds_id, ]
        try:
            response = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception:
            self.logger.critical('Unable to fetch SDS ip address.')
            raise DatabaseError('Failed to fetch SDS ip address.')
        sds_ip_addr = response[AESS_SERIAL_DEVICE_SERVER_TABLE_IP_ADDR_COLUMN_NAME]
        self.logger.debug('SDS ip addr = %s for SDS %d' % (sds_ip_addr, sds_id))

        return sds_ip_addr

    def get_modbus_id(self, bmu_id=None):
        sql = "SELECT %s from %s WHERE %s = %s;" % (AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME,
                                                    DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                    AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                    "%s")
        args = [bmu_id, ]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        modbus_num = response[AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME]
        self.logger.debug('MODbus number = %s for BMU %s' % (modbus_num, bmu_id))

        return modbus_num

    def get_sds_id(self, bmu_id=None):
        sql = "SELECT %s from %s WHERE %s = %s;" % (AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME,
                                                    DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                    AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                    "%s")
        args = [bmu_id, ]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        self.sds_id = response[AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME]
        self.logger.debug('SDS ID = %s for BMU %s' % (self.sds_id, bmu_id))

        return True

    def create_zmq_context(self):
        while True:
            try:
                self.zctx = zmq.Context()
            except ZMQError as error:
                self.zmq_errors += 1
                self.logger.exception('Failed to instantiate ZMQ context object - %s' % error)
                if self.zmq_errors < EVE_MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to instantiate ZMQ context object for the EVE BMU Controller')
            else:
                break

    def create_zmq_objects(self):
        while True:
            try:
                self.subscriber = self.zctx.socket(zmq.SUB)
                self.subscriber.setsockopt(zmq.LINGER, 0)
            except ZMQError as error:
                self.zmq_errors += 1
                self.logger.exception('Failed to create ZMQ socket - %s' % error)
                if self.zmq_errors < EVE_MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ socket for the Controller CLI')
                else:
                    continue
            try:
                self.subscriber.connect(MSG_BROKER_XPUB_URL)
                self.subscriber.setsockopt(zmq.SUBSCRIBE, self.topic)
            except ZMQError as error:
                zerr = repr(error)
                self.logger.error(zerr)
                self.zmq_errors += 1
                self.subscriber.close()
                self.logger.exception('Failed to create ZMQ socket - %s' % error)
                if self.zmq_errors < EVE_MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ socket for the Controller CLI')
                else:
                    continue
            try:
                self.poller = zmq.Poller()
            except ZMQError:
                self.zmq_errors += 1
                self.subscriber.close()
                self.logger.exception('Failed to create ZMQ poller object.')
                if self.zmq_errors < EVE_MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ poller object for the Controller CLI')
                else:
                    continue
            try:
                self.poller.register(self.subscriber, zmq.POLLIN)
            except ZMQError:
                self.zmq_errors += 1
                self.subscriber.close()
                self.poller.unregister(self.subscriber)
                self.logger.exception('Failed to register ZMQ poller')
                if self.zmq_errors < EVE_MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to register ZMQ poller for the Controller CLI')
                else:
                    continue
            break

    def delete_zmq_objects(self):
        self.logger.debug('Deleting ZMQ objects...')
        if self.subscriber is not None:
            self.subscriber.close()
        if self.poller is not None:
            self.poller.unregister(self.subscriber)

    def delete_zmq_context(self):
        if self.zctx is not None:
            self.zctx.term()

    def update_protection_data(self, bmu_id=None, column=None, value=None):
        sql = "INSERT INTO %s (%s) VALUES (%f) WHERE %s = %s;" % (DB_EVE_BMU_PROTECTION_DATA_TABLE_NAME,
                                                                  column,
                                                                  "%f",
                                                                  EVE_BMU_PROTECTION_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                  "%s")
        args = [value, bmu_id]
        prepared_act_on_database(EXECUTE, sql, args)

    def update_protection_data(self, bmu_id=None, column=None, value=None):
        sql = "INSERT INTO %s (%s) VALUES (%f) WHERE %s = %s;" % (DB_EVE_BMU_PROTECTION_DATA_TABLE_NAME,
                                                                  column,
                                                                  "%f",
                                                                  EVE_BMU_PROTECTION_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                  "%s")
        args = [value, bmu_id]
        prepared_act_on_database(EXECUTE, sql, args)

    """
    @summary: the 'set' functions all require values to be supplied in volts units as a floating 
              point number.  Appropriate scaling is performed in the individual functions.
    @param bmu_id: integer
    @param modbus_id: integer
    @param value: float
    """

    def set_pack_overvoltage_alarm(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / PACK_OVERVOLTAGE_ALARM_SCALING_FACTOR)
        self.write_holding_registers(PACK_OVERVOLTAGE_ALARM_REG_OFFSET, modbus_id, scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_PACK_OV_ALARM_COLUMN_NAME,
                                    scaled_value)

    def set_pack_overvoltage_protection(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / PACK_OVERVOLTAGE_PROTECTION_SCALING_FACTOR)
        self.write_holding_registers(PACK_OVERVOLTAGE_PROTECTION_REG_OFFSET, modbus_id, scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_PACK_OV_PROTECTION_COLUMN_NAME,
                                    scaled_value)

    def set_cell_overvoltage_alarm(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / CELL_OVERVOLTAGE_ALARM_SCALING_FACTOR)
        self.write_holding_registers(CELL_OVERVOLTAGE_ALARM_REG_OFFSET, modbus_id, scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_CELL_OV_ALARM_COLUMN_NAME,
                                    scaled_value)

    def set_cell_overvoltage_protection(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / CELL_OVERVOLTAGE_PROTECTION_SCALING_FACTOR)
        self.write_holding_registers(CELL_OVERVOLTAGE_PROTECTION_REG_OFFSET,
                                     modbus_id,
                                     scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_CELL_OV_PROTECTION_COLUMN_NAME,
                                    scaled_value)

    def set_pack_undervoltage_alarm(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / PACK_UNDERVOLTAGE_SCALING_FACTOR)
        self.write_holding_registers(PACK_UNDERVOLTAGE_ALARM_REG_OFFSET, modbus_id, scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_PACK_UV_ALARM_COLUMN_NAME,
                                    scaled_value)

    def set_pack_undervoltage_protection(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / PACK_UNDERVOLTAGE_PROTECTION_SCALING_FACTOR)
        self.write_holding_registers(PACK_UNDERVOLTAGE_PROTECTION_REG_OFFSET, modbus_id, scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_PACK_UV_PROTECTION_COLUMN_NAME,
                                    scaled_value)

    def set_cell_undervoltage_alarm(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / CELL_UNDERVOLTAGE_ALARM_SCALING_FACTOR)
        self.write_holding_registers(CELL_UNDERVOLTAGE_ALARM_REG_OFFSET,
                                     modbus_id,
                                     scaled_value)
        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_CELL_UV_ALARM_COLUMN_NAME,
                                    scaled_value)

    def set_cell_undervoltage_protection(self, bmu_id=None, modbus_id=None, value=None):
        scaled_value = value * (1.0 / CELL_UNDERVOLTAGE_PROTECTION_SCALING_FACTOR)
        self.write_holding_registers(CELL_UNDERVOLTAGE_PROTECTION_REG_OFFSET, modbus_id, scaled_value)

        self.update_protection_data(bmu_id,
                                    EVE_BMU_PROTECTION_DATA_TABLE_CELL_UV_PROTECTION_COLUMN_NAME,
                                    scaled_value)

    def comm_setup(self, bmu_id=None):
        sds_id = self.get_sds_id(bmu_id)
        sds_ip_addr = self.get_sds_ip_addr(sds_id)
        self.instantiate_modbus_client(sds_ip_addr)
        self.client_connect()
        modbus_id = self.get_modbus_id(bmu_id)

        return modbus_id

    def set_alarm(self, name=None, bmu_id=None, value=None):
        modbus_id = self.comm_setup(bmu_id)
        self.alarm_func_dict[name](bmu_id, modbus_id, value)
        self.close_modbus_client()

    def set_protection(self, name=None, bmu_id=None, value=None):
        modbus_id = self.comm_setup(bmu_id)
        self.protection_func_dict[name](bmu_id, modbus_id, value)
        self.close_modbus_client()

    def run(self):
        self.create_zmq_context()
        self.create_zmq_objects()

        """
        Main loop - wait for commands from the GUI on the REPly socket.
        """
        while True:
            try:
                zsocks = dict(self.poller.poll(CONTROLLER_SOCKET_TIMEOUT))
            except Exception as error:
                str_err = repr(error)
                self.logger.error(str_err)
                continue

            self.update()
            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                [topic, request] = self.subscriber.recv_multipart()
                request_list = request.split("|")
                request_len = len(request_list)
                try:
                    if request_len == 2:
                        self.command_dict[request_list[0]](request_list[1])
                    else:
                        self.command_dict[request_list[0]](request_list[1],
                                                           request_list[2],
                                                           request_list[3])
                except KeyError as error:
                    str_err = 'Invalid key: %s' % repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except (ConnectError, DatabaseError, ParameterError) as error:
                    str_err = repr(error.message)
                    self.logger.error(str_err)
                    break
                except ReadError as error:
                    str_err = repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except WriteError as error:
                    str_err = repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except TypeError as error:
                    str_err = repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except NotSupportedError as error:
                    str_err = repr(error.message)
                    self.logger.warning(str_err)
                    continue
                except Exception as error:
                    self.logger.exception('Unable to process command')
                    continue

        self.delete_zmq_objects()
        self.delete_zmq_context()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    # signal.signal(signal.SIGKILL, sig_handler)

    d = EveBmuControllerDaemon(id=1)
    d.run()

    exit(0)
