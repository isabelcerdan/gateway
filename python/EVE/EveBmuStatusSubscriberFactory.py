#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: EveBmuStatusSubscriberFactory.py

@description: Process the raw BMU data for one or more MODbus strings of a single rack.

@created: July 31, 2018

@copyright: Apparent Energy Inc.  2018

@author: steve
"""

import time
import zmq
from zmq.backend.cython.utils import ZMQError
from lib.gateway_constants.ZmqConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
import json
from lib.logging_setup import *
from alarm.alarm import ALARM_ID_EVE_BMU_STATUS_SUBSCRIBER_RESTARTED, \
    ALARM_ID_EVE_BMU_BATTERY_CELL_OVER_VOLTAGE_PROTECTION, \
    ALARM_ID_EVE_BMU_BATTERY_CELL_LOW_VOLTAGE_PROTECTION, \
    ALARM_ID_EVE_BMU_BATTERY_PACK_OVER_VOLTAGE_PROTECTION, \
    ALARM_ID_EVE_BMU_BATTERY_PACK_LOW_VOLTAGE_PROTECTION, \
    ALARM_ID_EVE_BMU_CHARGING_OVER_CURRENT_PROTECTION, \
    ALARM_ID_EVE_BMU_DISCHARGING_OVER_CURRENT_PROTECTION, \
    ALARM_ID_EVE_BMU_SHORT_CIRCUIT_PROTECTION, \
    ALARM_ID_EVE_BMU_CHARGER_OVERVOLTAGE_PROTECTION, \
    ALARM_ID_EVE_BMU_CHARGING_HIGH_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_DISCHARGING_HIGH_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_CHARGING_LOW_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_DISCHARGING_LOW_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_MOSFET_HIGH_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_ENVIRONMENT_HIGH_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_ENVIRONMENT_LOW_TEMPERATURE_PROTECTION, \
    ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_OVERVOLTAGE, \
    ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_LOW_VOLTAGE, \
    ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_OVERVOLTAGE, \
    ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_LOW_VOLTAGE, \
    ALARM_ID_EVE_BMU_WARNING_CHARGING_OVER_CURRENT, \
    ALARM_ID_EVE_BMU_WARNING_DISCHARGING_OVER_CURRENT, \
    ALARM_ID_EVE_BMU_WARNING_CHARGING_HIGH_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_DISCHARGING_HIGH_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_CHARGING_LOW_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_DISCHARGING_LOW_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_HIGH_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_LOW_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_MOSFET_HIGH_TEMPERATURE, \
    ALARM_ID_EVE_BMU_WARNING_SOC_LOW, \
    ALARM_ID_EVE_BMU_CHARGING_MOSFET_FAULT, \
    ALARM_ID_EVE_BMU_DISCHARGING_MOSFET_FAULT, \
    ALARM_ID_EVE_BMU_TEMPERATURE_SENSOR_FAULT, \
    ALARM_ID_EVE_BMU_BATTERY_CELL_FAULT, \
    ALARM_ID_EVE_BMU_FRONT_END_SAMPLING_COMMUNICATION_FAULT
from alarm.alarm import Alarm
import numpy as np
from ess.ess_daemon import EssDaemon
import threading

SUBSCRIBER_SOCKET_TIMEOUT = 5000  # ms
EVE_BMU_DEFAULT_POLLING_FREQ = 1


class EveBmuStatusSubscriberThread(threading.Thread):
    xlat_warning_bit_pos_to_col_dict = {
        0: AESS_BMU_STATUS_TABLE_BATTERY_CELL_OVERVOLTAGE_WARNING_COLUMN_NAME,
        1: AESS_BMU_STATUS_TABLE_BATTERY_CELL_LOW_VOLTAGE_WARNING_COLUMN_NAME,
        2: AESS_BMU_STATUS_TABLE_BATTERY_PACK_OVERVOLTAGE_WARNING_COLUMN_NAME,
        3: AESS_BMU_STATUS_TABLE_BATTERY_PACK_LOW_VOLTAGE_WARNING_COLUMN_NAME,
        4: AESS_BMU_STATUS_TABLE_CHARGING_OVER_CURRENT_WARNING_COLUMN_NAME,
        5: AESS_BMU_STATUS_TABLE_DISCHARGING_OVER_CURRENT_WARNING_COLUMN_NAME,
        8: AESS_BMU_STATUS_TABLE_CHARGING_HIGH_TEMPERATURE_WARNING_COLUMN_NAME,
        9: AESS_BMU_STATUS_TABLE_DISCHARGING_HIGH_TEMPERATURE_WARNING_COLUMN_NAME,
        10: AESS_BMU_STATUS_TABLE_CHARGING_LOW_TEMPERATURE_WARNING_COLUMN_NAME,
        11: AESS_BMU_STATUS_TABLE_DISCHARGING_LOW_TEMPERATURE_WARNING_COLUMN_NAME,
        12: AESS_BMU_STATUS_TABLE_ENVIRONMENT_HIGH_TEMPERATURE_WARNING_COLUMN_NAME,
        13: AESS_BMU_STATUS_TABLE_ENVIRONMENT_LOW_TEMPERATURE_WARNING_COLUMN_NAME,
        14: AESS_BMU_STATUS_TABLE_MOSFET_HIGH_TEMPERATURE_WARNING_COLUMN_NAME,
        15: AESS_BMU_STATUS_TABLE_SOC_LOW_WARNING_COLUMN_NAME
    }

    xlat_protection_bit_pos_to_col_dict = {
        0: AESS_BMU_STATUS_TABLE_BATTERY_CELL_OVER_VOLTAGE_PROTECTION_COLUMN_NAME,
        1: AESS_BMU_STATUS_TABLE_BATTERY_CELL_LOW_VOLTAGE_PROTECTION_COLUMN_NAME,
        2: AESS_BMU_STATUS_TABLE_BATTERY_PACK_OVER_VOLTAGE_PROTECTION_COLUMN_NAME,
        3: AESS_BMU_STATUS_TABLE_BATTERY_PACK_LOW_VOLTAGE_PROTECTION_COLUMN_NAME,
        4: AESS_BMU_STATUS_TABLE_CHARGING_OVER_CURRENT_PROTECTION_COLUMN_NAME,
        5: AESS_BMU_STATUS_TABLE_DISCHARGING_OVER_CURRENT_PROTECTION_COLUMN_NAME,
        6: AESS_BMU_STATUS_TABLE_SHORT_CIRCUIT_PROTECTION_COLUMN_NAME,
        7: AESS_BMU_STATUS_TABLE_CHARGER_OVERVOLTAGE_PROTECTION_COLUMN_NAME,
        8: AESS_BMU_STATUS_TABLE_CHARGING_HIGH_TEMPERATURE_PROTECTION_COLUMN_NAME,
        9: AESS_BMU_STATUS_TABLE_DISCHARGING_HIGH_TEMPERATURE_PROTECTION_COLUMN_NAME,
        10: AESS_BMU_STATUS_TABLE_CHARGING_LOW_TEMPERATURE_PROTECTION_COLUMN_NAME,
        11: AESS_BMU_STATUS_TABLE_DISCHARGING_LOW_TEMPERATURE_PROTECTION_COLUMN_NAME,
        12: AESS_BMU_STATUS_TABLE_MOSFET_HIGH_TEMPERATURE_PROTECTION_COLUMN_NAME,
        13: AESS_BMU_STATUS_TABLE_ENVIRONMENT_HIGH_TEMPERATURE_PROTECTION_COLUMN_NAME,
        14: AESS_BMU_STATUS_TABLE_ENVIRONMENT_LOW_TEMPERATURE_PROTECTION_COLUMN_NAME
    }

    xlat_status_fault_bit_pos_to_col_dict = {
        0: AESS_BMU_STATUS_TABLE_CHARGING_MOSFET_FAULT_COLUMN_NAME,
        1: AESS_BMU_STATUS_TABLE_DISCHARGING_MOSFET_FAULT_COLUMN_NAME,
        2: AESS_BMU_STATUS_TABLE_TEMPERATURE_SENSOR_FAULT_COLUMN_NAME,
        3: None,
        4: AESS_BMU_STATUS_TABLE_BATTERY_CELL_FAULT_COLUMN_NAME,
        5: AESS_BMU_STATUS_TABLE_FRONT_END_SAMPLING_COMMUNICATION_FAULT_COLUMN_NAME,
        6: None,
        7: None,
        8: AESS_BMU_STATUS_TABLE_STATE_OF_CHARGE_COLUMN_NAME,
        9: AESS_BMU_STATUS_TABLE_STATE_OF_DISCHARGE_COLUMN_NAME,
        10: AESS_BMU_STATUS_TABLE_CHARGING_MOSFET_IS_ON_COLUMN_NAME,
        11: AESS_BMU_STATUS_TABLE_DISCHARGING_MOSFET_IS_ON_COLUMN_NAME,
        12: AESS_BMU_STATUS_TABLE_CHARGING_LIMITER_IS_ON_COLUMN_NAME,
        13: None,
        14: AESS_BMU_STATUS_TABLE_CHARGER_INVERSED_COLUMN_NAME,
        15: AESS_BMU_STATUS_TABLE_HEATER_IS_ON_COLUMN_NAME
    }

    xlat_protection_bit_pos_to_alarm_dict = {
        0: ALARM_ID_EVE_BMU_BATTERY_CELL_OVER_VOLTAGE_PROTECTION,
        1: ALARM_ID_EVE_BMU_BATTERY_CELL_LOW_VOLTAGE_PROTECTION,
        2: ALARM_ID_EVE_BMU_BATTERY_PACK_OVER_VOLTAGE_PROTECTION,
        3: ALARM_ID_EVE_BMU_BATTERY_PACK_LOW_VOLTAGE_PROTECTION,
        4: ALARM_ID_EVE_BMU_CHARGING_OVER_CURRENT_PROTECTION,
        5: ALARM_ID_EVE_BMU_DISCHARGING_OVER_CURRENT_PROTECTION,
        6: ALARM_ID_EVE_BMU_SHORT_CIRCUIT_PROTECTION,
        7: ALARM_ID_EVE_BMU_CHARGER_OVERVOLTAGE_PROTECTION,
        8: ALARM_ID_EVE_BMU_CHARGING_HIGH_TEMPERATURE_PROTECTION,
        9: ALARM_ID_EVE_BMU_DISCHARGING_HIGH_TEMPERATURE_PROTECTION,
        10: ALARM_ID_EVE_BMU_CHARGING_LOW_TEMPERATURE_PROTECTION,
        11: ALARM_ID_EVE_BMU_DISCHARGING_LOW_TEMPERATURE_PROTECTION,
        12: ALARM_ID_EVE_BMU_MOSFET_HIGH_TEMPERATURE_PROTECTION,
        13: ALARM_ID_EVE_BMU_ENVIRONMENT_HIGH_TEMPERATURE_PROTECTION,
        14: ALARM_ID_EVE_BMU_ENVIRONMENT_LOW_TEMPERATURE_PROTECTION
    }

    xlat_warning_bit_pos_to_alarm_dict = {
        0: ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_OVERVOLTAGE,
        1: ALARM_ID_EVE_BMU_WARNING_BATTERY_CELL_LOW_VOLTAGE,
        2: ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_OVERVOLTAGE,
        3: ALARM_ID_EVE_BMU_WARNING_BATTERY_PACK_LOW_VOLTAGE,
        4: ALARM_ID_EVE_BMU_WARNING_CHARGING_OVER_CURRENT,
        5: ALARM_ID_EVE_BMU_WARNING_DISCHARGING_OVER_CURRENT,
        6: None,
        7: None,
        8: ALARM_ID_EVE_BMU_WARNING_CHARGING_HIGH_TEMPERATURE,
        9: ALARM_ID_EVE_BMU_WARNING_DISCHARGING_HIGH_TEMPERATURE,
        10: ALARM_ID_EVE_BMU_WARNING_CHARGING_LOW_TEMPERATURE,
        11: ALARM_ID_EVE_BMU_WARNING_DISCHARGING_LOW_TEMPERATURE,
        12: ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_HIGH_TEMPERATURE,
        13: ALARM_ID_EVE_BMU_WARNING_ENVIRONMENT_LOW_TEMPERATURE,
        14: ALARM_ID_EVE_BMU_WARNING_MOSFET_HIGH_TEMPERATURE,
        15: ALARM_ID_EVE_BMU_WARNING_SOC_LOW
    }

    xlat_fault_bit_pos_to_alarm_dict = {
        0: ALARM_ID_EVE_BMU_CHARGING_MOSFET_FAULT,
        1: ALARM_ID_EVE_BMU_DISCHARGING_MOSFET_FAULT,
        2: ALARM_ID_EVE_BMU_TEMPERATURE_SENSOR_FAULT,
        3: None,
        4: ALARM_ID_EVE_BMU_BATTERY_CELL_FAULT,
        5: ALARM_ID_EVE_BMU_FRONT_END_SAMPLING_COMMUNICATION_FAULT,
        6: None,
        7: None,
        8: None,
        9: None,
        10: None,
        11: None,
        12: None,
        13: None,
        14: None,
        15: None
    }

    def __init__(self, sds_id=None, ctx=None, **kwargs):
        self.ess_unit = kwargs.pop('ess_unit')
        threading.Thread.__init__(self, **kwargs)
        self.shutdown_flag = threading.Event()
        self.ctx = ctx
        self.sds_id = sds_id
        self.register_update_string = ''
        self.subscriber = None
        self.bmu_data = {}
        self.bmu_list = []
        self.bmu_id = None
        self.db_values_dict = {}
        self.logger = setup_logger('EveBmuStatSubThr.sds%d' % sds_id)
        # self.logger.setLevel(logging.DEBUG)

        self.bmu_status_warning_register_dict = {
            WARNING_FLAG_REG_OFFSET: self.process_warning_flag,
            PROTECTION_FLAG_REG_OFFSET: self.process_protection_flag,
            FAULT_STATUS_REG_OFFSET: self.process_fault_status_flag,
            BALANCE_STATUS_REG_OFFSET: self.process_balance_status
        }

    def set_ess_capability(self):
        sql = "UPDATE %s set %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                       ESS_MASTER_INFO_TABLE_CAPABILITY_COLUMN_NAME, 'Fault',
                                                       ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, [self.ess_unit.id])

    """
    @note: 'Idle' is not really an option here.  There is currently no mechanism for
           tracking prior states of the ESS.  Needs a bit of thought.
    """

    def clear_ess_fault_state(self):
        sql = "UPDATE %s set %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                       ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME, 'Stopped',
                                                       ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, [self.ess_unit.id])

    def build_update_string(self):
        if not self.db_values_dict:
            raise ProcessingError('Empty database values dictionary.')

        update_list = ["INSERT INTO "]
        update_list.append(DB_AESS_BMU_STATUS_TABLE_NAME)
        update_list.append(" (%s, " % AESS_BMU_STATUS_TABLE_BMU_ID_COLUMN_NAME)
        for field_name in self.db_values_dict.iterkeys():
            try:
                update_list.append("%s" % field_name)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") VALUES (%d, " % self.bmu_id)
        for register_value in self.db_values_dict.itervalues():
            try:
                update_list.append("\"%s\"" % register_value)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") ON DUPLICATE KEY UPDATE ")
        for field_name, register_value in self.db_values_dict.iteritems():
            try:
                update_list.append('%s=\"%s\" ' % (field_name, register_value))
                update_list.append(', ')
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
        update_list.pop()  # pop the final comma
        update_list.append(";")
        update_str = ''.join(update_list)
        # self.logger.debug('SQL = %s' % update_str)

        return update_str[:]

    def test_bit(self, register=None, bit_pos=None):
        return (register & (1 << bit_pos))

    def process_warning_flag(self, register):
        self.logger.debug('Processing warning flag register...')
        for bit in range(0, NUM_WARNING_BITS):
            if bit == 6 or bit == 7:
                continue
            if self.test_bit(register, bit) is True:
                self.logger.warning('Warning: %s is ON' % warning_flag_dict[bit])
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_warning_bit_pos_to_col_dict[bit]] = 'ON'
                Alarm.occurrence(EveBmuStatusSubscriberThread.xlat_warning_bit_pos_to_alarm_dict[bit])
            else:
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_warning_bit_pos_to_col_dict[bit]] = 'OFF'

    def process_protection_flag(self, register):
        self.logger.debug('Processing protection flag register...')
        for bit in range(0, NUM_PROTECTION_BITS):
            if self.test_bit(register, bit) is True:
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_protection_bit_pos_to_col_dict[bit]] = 'ON'
                Alarm.occurrence(EveBmuStatusSubscriberThread.xlat_protection_bit_pos_to_alarm_dict[bit])
                self.set_ess_capability()  # Hit the brakes!
            else:
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_protection_bit_pos_to_col_dict[bit]] = 'OFF'
                Alarm.request_clear(EveBmuStatusSubscriberThread.xlat_protection_bit_pos_to_alarm_dict[bit])

    def process_fault_status_flag(self, register):
        self.logger.debug('Processing fault/status flag register...')
        for bit in range(0, NUM_FAULT_STATUS_BITS):
            if EveBmuStatusSubscriberThread.xlat_status_fault_bit_pos_to_col_dict[bit] is None:
                continue
            if self.test_bit(register, bit) is True:
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_status_fault_bit_pos_to_col_dict[bit]] = 'ON'
                if EveBmuStatusSubscriberThread.xlat_fault_bit_pos_to_alarm_dict[bit] is None:
                    continue
                else:
                    Alarm.occurrence(EveBmuStatusSubscriberThread.xlat_fault_bit_pos_to_alarm_dict[bit])
                    self.set_ess_capability()  # Hit the brakes!
            else:
                self.db_values_dict[EveBmuStatusSubscriberThread.xlat_status_fault_bit_pos_to_col_dict[bit]] = 'OFF'

    def process_balance_status(self, register):
        self.logger.debug('Processing balance status flag register...')
        balance_status = np.uint16(register)
        self.logger.debug('Balance status : 0x%x' % balance_status)
        self.db_values_dict[AESS_BMU_STATUS_TABLE_BALANCE_STATUS_COLUMN_NAME] = balance_status

    def process_register_set(self):
        if not self.bmu_data:
            raise ProcessingError('Empty BMU data set')

        for register in self.bmu_data.iterkeys():
            if int(register) >= WARNING_FLAG_REG_OFFSET and int(register) <= BALANCE_STATUS_REG_OFFSET:
                self.bmu_status_warning_register_dict[int(register)](self.bmu_data[register])
            else:
                continue
        try:
            sql = self.build_update_string()
        except ProcessingError:
            raise

        try:
            prepared_act_on_database(EXECUTE, sql, [])
        except:
            raise

    def subscribe(self, topic=None):
        try:
            for bmu_id in self.bmu_list:
                wildcard_str = "bmu%d#" % bmu_id
                self.subscriber.setsockopt(zmq.SUBSCRIBE, wildcard_str)
                self.logger.debug('Subscribing to %s...' % wildcard_str)
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)

    def unsubscribe(self, topic=None):
        if topic is None:
            return True

        try:
            for bmu_id in self.bmu_list:
                wildcard_str = "bmu%d#" % bmu_id
                self.subscriber.setsockopt(zmq.UNSUBSCRIBE, wildcard_str)
                self.logger.debug('Unsubscribing to %s...' % wildcard_str)
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s WHERE %s = %s" % (AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                    DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                    AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME,
                                                    "%s")
        args = [self.sds_id, ]
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, args)
        except:
            raise

        for resp in response:
            self.bmu_list.append(resp['bmu_id'])

    def run(self):
        self.logger.info('BmuStatSubThr [sds%d] starting.' % self.sds_id)

        try:
            self.get_operational_parameters()
        except:
            raise

        try:
            self.subscriber = self.ctx.socket(zmq.SUB)
            self.subscriber.connect(MSG_BROKER_XPUB_URL)
            poller = zmq.Poller()
            poller.register(self.subscriber, zmq.POLLIN)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        try:
            self.subscribe()  # to all BMUs in the chain
        except:
            raise

        while True:
            for n in range(0, len(self.bmu_list)):
                if self.shutdown_flag.is_set():
                    self.logger.info('Received shutdown signal')
                    break

                zsocks = dict(poller.poll(SUBSCRIBER_SOCKET_TIMEOUT))
                if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                    try:
                        [topic, data] = self.subscriber.recv_multipart()
                    except ZMQError as e:
                        self.logger.exception('recv_multipart failed for %s' % self.bmu_name)
                        if e.errno == zmq.ETERM:
                            break  # Interrupted
                    except KeyboardInterrupt:
                        break
                    except Exception as error:
                        self.logger.exception(error)
                        raise ProcessingError(error)

                    if topic is None or data is None:
                        self.logger.info('Received a message with no topic or data frame.')
                        continue

                    try:
                        # start_time = timeit.default_timer()
                        self.bmu_id = int(topic[3:-1])
                        self.bmu_data = json.loads(data)
                        # self.logger.debug('Received %s message' % topic)
                        # self.logger.debug(self.bmu_data)
                        self.process_register_set()
                        # elapsed = timeit.default_timer() - start_time
                        # print 'elapsed time', elapsed
                    except ProcessingError:
                        self.logger.warning('Empty update list')
                        continue
                    except Exception as error:
                        self.logger.exception(error)
                        return False

            """ Skip any messages while we are sleeping..."""
            self.subscriber.close()
            time.sleep(5)
            try:
                self.subscriber = self.ctx.socket(zmq.SUB)
                self.subscribe()  # to all BMUs in the chain
                self.subscriber.connect(MSG_BROKER_XPUB_URL)
                poller = zmq.Poller()
                poller.register(self.subscriber, zmq.POLLIN)
            except:
                raise

        self.subscriber.close()
        self.ctx.term()


class EveBmuStatusSubscriberFactoryDaemon(EssDaemon):
    alarm_id = ALARM_ID_EVE_BMU_STATUS_SUBSCRIBER_RESTARTED
    polling_freq_sec = EVE_BMU_DEFAULT_POLLING_FREQ

    def __init__(self, **kwargs):
        super(EveBmuStatusSubscriberFactoryDaemon, self).__init__(**kwargs)
        self.thread_list = []
        self.sds_list = []
        self.db_errors = 0

    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread_stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s WHERE %s = %s" % (AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME,
                                                   DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME,
                                                   AESS_SERIAL_DEVICE_SERVER_TABLE_ESS_UNIT_ID_COLUMN_NAME, "%s")
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, [self.ess_unit.id])
        except Exception:
            self.logger.critical('Unable to fetch SDS operational parameters.')
            raise DatabaseError('Failed to fetch SDS operational parameters.')
        for sid in response:
            self.sds_list.append(sid['sds_id'])

    def run(self):
        self.update()

        try:
            self.get_operational_parameters()
        except:
            raise

        try:
            ctx = zmq.Context.instance()
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        for sds_id in self.sds_list:
            try:
                thread_name = 'sds%d' % int(sds_id)
                thread = EveBmuStatusSubscriberThread(int(sds_id), ctx, name=thread_name, ess_unit=self.ess_unit)
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)

            try:
                self.logger.info('Starting status subscriber thread sds%s' % sds_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)

        """
        @note: The factory joins a thread in its list every few seconds.
        """
        while True:
            self.update()

            try:
                for thread in self.thread_list:
                    thread_name = ''
                    try:
                        thread_name = thread.getName()
                        thread.join(1.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        """ Remove the dead thread from the list and create a new one. """
                        self.logger.error('EveBmuStatusSubscriber thread %s died' % thread_name)
                        self.thread_list.remove(thread)
                        sds_num = int(thread_name[3:])
                        thread = EveBmuStatusSubscriberThread(sds_num, ctx, name=thread_name, ess_unit=self.ess_unit)
                        try:
                            self.logger.warning('Restarting status subscriber thread sds%d' % sds_num)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                break

            time.sleep(self.polling_freq_sec)

        self.logger.info('EveBmuStatusSubscriberFactory fell through main loop')
        return False
