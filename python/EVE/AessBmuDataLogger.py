#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: AessBmuDataLogger.py

@description: Read the current database values for selected fields of the
              BMU data table and write them to a circular database file.  

@copyright: Apparent Inc.  2018

@created: September 24, 2018

@author: steve
"""
import time

from alarm.alarm_constants import ALARM_ID_AESS_BMU_DATA_LOGGER_RESTARTED
from lib.gateway_constants.EveBmuConstants import *
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
from ess.ess_daemon import EssDaemon

AESS_DEFAULT_POLLING_FREQ_SEC = 60
AESS_DEFAULT_LOG_CYCLE = 2592000    # 30 days of 1-second data
AESS_MAX_DB_ERRORS = 5

logger = setup_logger('AessBmuDataLogger')

try:
    bmu_data_select_statement = "SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " \
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " \
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s " \
                                "WHERE %s = %s;" % \
                               (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_REMAINING_CAPACITY_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_FULL_CHARGED_CAPACITY_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_DESIGN_CAPACITY_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CYCLE_COUNT_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_1_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_2_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_3_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_4_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_5_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_6_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_7_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_8_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_9_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_10_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_11_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_12_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_13_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_14_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_15_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_VOLTAGE_16_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_1_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_2_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_3_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_CELL_TEMPERATURE_4_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME,
                                AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME,
                                DB_AESS_BMU_DATA_TABLE_NAME,
                                AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                "%s")
except Exception as error:
    logger.exception('Mis-configured select statement')
    raise ParameterError(error)


class AessBmuDataLoggerDaemon(EssDaemon):

    alarm_id = ALARM_ID_AESS_BMU_DATA_LOGGER_RESTARTED
    update_timeout = 60.0
    polling_freq_sec = AESS_DEFAULT_POLLING_FREQ_SEC

    def __init__(self, **kwargs):
        super(AessBmuDataLoggerDaemon, self).__init__(**kwargs)
        self.log_cycle = AESS_DEFAULT_LOG_CYCLE
        self.db_errors = 0
        self.bmu_list = []
        
    def log_bmu_data(self, bmu_id=None):
        args = [bmu_id,]
        try:
            registers = prepared_act_on_database(FETCH_ONE, bmu_data_select_statement, args)
        except:
            raise
        if registers is None:
            return True
        
        replace_list = ['REPLACE INTO %s SET %s = (SELECT MOD(COALESCE(MAX(%s), 0), %d)' \
                        ' + 1 FROM %s AS t), ' % \
                        (DB_AESS_BMU_DATA_LOG_TABLE_NAME, 
                         AESS_BMU_DATA_LOG_ROW_ID_COLUMN_NAME,
                         AESS_BMU_DATA_LOG_ENTRY_ID_COLUMN_NAME,
                         self.log_cycle,
                         DB_AESS_BMU_DATA_LOG_TABLE_NAME)]
        set_list = []
        for register_name, register_value in registers.iteritems():
            set_list.append('%s=%s' % (register_name, register_value))
            set_list.append(', ')
        set_list.pop() # pop the final comma
        set_list.append(';')
        replace_list.append(''.join(set_list))
        replace_string = ''.join(replace_list)
        args = []
        try:
            prepared_act_on_database(EXECUTE, replace_string, args)
        except Exception as error:
            err_string = 'Unable to update %s - %s' % (DB_AESS_BMU_DATA_LOG_TABLE_NAME, repr(error))
            logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > AESS_MAX_DB_ERRORS:
                raise DatabaseError('Maximum database errors exceeded.')
            return False
        return True

    def run(self):
        try:
            self.bmu_list = self.ess_unit.get_bmu_list()
        except:
            raise

        while True:
            for bmu_id in self.bmu_list:
                self.update()
                try:
                    self.log_bmu_data(bmu_id)
                except:
                    raise
            
            time.sleep(self.polling_freq_sec)


if __name__ == '__main__':

    a = AessBmuDataLoggerDaemon()
    a.run()
