#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: EveBmuProtectionSubscriber.py

@description: Process the raw BMU data for one or more MODbus strings of a single rack.

@created: July 19, 2018

@copyright: Apparent Energy Inc.  2018

@author: steve
"""

import zmq
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
from contextlib import closing
from lib.gateway_constants.ZmqConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from daemon.daemon import Daemon
import json
from lib.logging_setup import *
import numpy as np
from zmq.backend.cython.utils import ZMQError
import threading


SUBSCRIBER_SOCKET_TIMEOUT = 5000 #ms
SUBSCRIBER_UPDATE_INTERVAL = 5 #sec


class EveBmuProtectionSubscriberThread(threading.Thread):
    def __init__(self, sds_id=None, ctx=None, **kwargs):
        threading.Thread.__init__(self, **kwargs)
        self.shutdown_flag = threading.Event()
        self.ctx = ctx
        self.sds_id = sds_id  
        self.register_update_string = ''
        self.subscriber = None
        self.bmu_list = []
        self.bmu_data = {}
        self.bmu_id = None
        self.db_values_dict = {}

        self.logger = setup_logger('EveBmuProtSubThr.sds%d' % sds_id)
        #self.logger.setLevel(logging.DEBUG)

        self.bmu_register_set_dict = {
            PACK_OVERVOLTAGE_ALARM_REG_OFFSET             : self.process_pack_ov_alarm,
            PACK_UNDERVOLTAGE_ALARM_REG_OFFSET            : self.process_pack_uv_alarm,
            CELL_OVERVOLTAGE_ALARM_REG_OFFSET             : self.process_cell_overvoltage_alarm,
            CELL_UNDERVOLTAGE_ALARM_REG_OFFSET            : self.process_cell_undervoltage_alarm,
            CHARGING_OVER_CURRENT_ALARM_REG_OFFSET        : self.process_charging_over_current_alarm,
            DISCHARGING_OVER_CURRENT_ALARM_REG_OFFSET     : self.process_discharging_over_current_alarm,
            CHARGING_OVER_TEMPERATURE_ALARM_REG_OFFSET    : self.process_charging_over_temperature_alarm,
            DISCHARGING_OVER_TEMPERATURE_ALARM_REG_OFFSET : self.process_discharging_over_temperature_alarm,
            MOSFET_OVER_TEMPERATURE_ALARM_REG_OFFSET      : self.process_mosfet_over_temperature_alarm,
            ENVIRONMENT_OVER_TEMPERATURE_ALARM_REG_OFFSET : self.process_environment_over_temperature_alarm
        }

    def prepared_act_on_database(self, action, command, args):
        try:
            with closing(MyDB.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                      db=DB_NAME, cursorclass=my_cursor.DictCursor)) as connection:
                with connection as cursor:
                    cursor.execute(command, args)
                    if action == FETCH_ONE:
                        return cursor.fetchone()
                    elif action == FETCH_ALL:
                        return cursor.fetchall()
                    return True
        except Exception as error:
            critical_string = "act_on_database failed. \nAction: %s \nString: %s \nArgs: %s - %s" \
                              % (action, command, args, repr(error))
            self.logger.exception(critical_string)
            raise DatabaseError(error)

    def build_update_string(self):
        if not self.db_values_dict:
            self.logger.warning('Empty database values dictionary.')
            return ''
        
        update_list = ["INSERT INTO "]
        update_list.append(DB_EVE_BMU_PROTECTION_DATA_TABLE_NAME)
        update_list.append(" (%s, " % EVE_BMU_PROTECTION_DATA_TABLE_BMU_ID_COLUMN_NAME)
        for field_name in self.db_values_dict.iterkeys():
            try:
                update_list.append("%s" % field_name)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") VALUES (%d, " % self.bmu_id)
        for register_value in self.db_values_dict.itervalues():
            try:
                update_list.append("%f" % register_value)
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            update_list.append(", ")
        update_list.pop()
        update_list.append(") ON DUPLICATE KEY UPDATE ")
        
        for field_name, register_value in self.db_values_dict.iteritems():
            try:
                update_list.append('%s=%f ' % (field_name, register_value))
                update_list.append(', ')
            except KeyError as error:
                self.logger.exception(error)
                raise ProcessingError(error)
        update_list.pop()  # pop the final comma
        update_list.append(";")
        update_str = ''.join(update_list)
        self.logger.debug('SQL = %s' % update_str)

        return update_str[:]
        
    def process_pack_ov_alarm(self, register):
        ov_alarm = float(np.uint16(register)) * float(PACK_OVERVOLTAGE_ALARM_SCALING_FACTOR)
        self.logger.debug('Battery pack overvoltage alarm: %.3fmV' % ov_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_PACK_OV_ALARM_COLUMN_NAME] = ov_alarm

    def process_pack_uv_alarm(self, register):   
        uv_alarm = float(np.uint8(register)) * float(PACK_UNDERVOLTAGE_SCALING_FACTOR)
        self.logger.debug('Battery pack undervoltage alarm: %.3f%%' % uv_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_PACK_UV_ALARM_COLUMN_NAME] = uv_alarm
    
    def process_cell_overvoltage_alarm(self, register):
        cell_ov_alarm = float(np.uint8(register)) * float(CELL_OVERVOLTAGE_ALARM_SCALING_FACTOR)
        self.logger.debug('Cell overvoltage alarm: %.3f%%' % cell_ov_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_CELL_OV_ALARM_COLUMN_NAME] = cell_ov_alarm
    
    def process_cell_undervoltage_alarm(self, register):
        cell_uv_alarm = float(np.uint8(register)) * float(CELL_UNDERVOLTAGE_ALARM_SCALING_FACTOR)
        self.logger.debug('Cell undervoltage alarm: %.3f%%' % cell_uv_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_CELL_UV_ALARM_COLUMN_NAME] = cell_uv_alarm

    def process_charging_over_current_alarm(self, register):
        charging_oc_alarm = float(np.uint8(register))
        self.logger.debug('Charging overcurrent alarm: %.3f%%' % charging_oc_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_CHARGING_OC_ALARM_COLUMN_NAME] = charging_oc_alarm
    
    def process_discharging_over_current_alarm(self, register):
        discharging_oc_alarm = float(np.uint8(register))
        self.logger.debug('Discharging overcurrent alarm: %.3f%%' % discharging_oc_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_CHARGING_OC_ALARM_COLUMN_NAME] = discharging_oc_alarm

    def process_charging_over_temperature_alarm(self, register):
        charging_ot_alarm = float(np.uint8(register)) * float(CHARGING_OVER_TEMPERATURE_ALARM_SCALING_FACTOR)
        self.logger.debug('Charging overcurrent alarm: %.3f%%' % charging_ot_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_CHARGING_OC_ALARM_COLUMN_NAME] = charging_ot_alarm
    
    def process_discharging_over_temperature_alarm(self, register):
        discharging_ot_alarm = float(np.uint8(register)) * float(CHARGING_OVER_TEMPERATURE_ALARM_SCALING_FACTOR)
        self.logger.debug('Charging overcurrent alarm: %.3f%%' % discharging_ot_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_DISCHARGING_OT_ALARM_COLUMN_NAME] = discharging_ot_alarm

    def process_mosfet_over_temperature_alarm(self, register):
        mosfet_ot_alarm = float(np.uint8(register)) * float(MOSFET_OVER_TEMPERATURE_ALARM_SCALING_FACTOR)
        self.logger.debug('MOSFET overtemperature alarm: %.3f%%' % mosfet_ot_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_MOSFET_OT_ALARM_COLUMN_NAME] = mosfet_ot_alarm
    
    def process_environment_over_temperature_alarm(self, register):
        env_ot_alarm = float(np.uint8(register)) * float(ENVIRONMENT_OVER_TEMPERATURE_ALARM_SCALING_FACTOR)
        self.logger.debug('Environment overtemperature alarm: %.3f%%' % env_ot_alarm)
        self.db_values_dict[EVE_BMU_PROTECTION_DATA_TABLE_ENVIRONMENT_OT_ALARM_COLUMN_NAME] = env_ot_alarm
    
    def process_register_set(self):
        for register in self.bmu_data.iterkeys():
            #self.logger.debug('Processing register %s...' % register)
            #self.logger.debug('Processing register %s...' % register)
            self.bmu_register_set_dict[int(register)](self.bmu_data[register])

        sql = self.build_update_string()

        if sql == '':
            return
        
        prepared_act_on_database(EXECUTE, sql, [])

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s WHERE %s = %s;" % (AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME,
                                                    DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME,
                                                    AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME,
                                                    "%s")
        args = [self.sds_id,]
        response = prepared_act_on_database(FETCH_ALL, sql, args)

        for resp in response:
            self.bmu_list.append(resp['bmu_id'])
        
    def run(self):
        self.logger.info('BmuProtSubThr [sds%d] starting.' % self.sds_id)

        try:
            self.subscriber = self.ctx.socket(zmq.SUB)
            self.subscriber.connect('tcp://%s:%d' % (MSG_BROKER_IP_ADDRESS, MSG_BROKER_XPUB_PORT))
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        """
        @note: The wildcard logic is selecting for all possible combinations of bmu_id.
               Individual subscriber threads will focus on one SDS ID/one MODbus chain.
        """
        self.get_operational_parameters()

        try:
            for bmu_id in self.bmu_list:
                wildcard_str = "bmu%d#" % bmu_id 
                self.subscriber.setsockopt(zmq.SUBSCRIBE, wildcard_str)
                self.logger.debug('Subscribing to %s...' % wildcard_str )        
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError(error)
        
        poller = zmq.Poller()
        poller.register(self.subscriber, zmq.POLLIN)

        while True:
            if self.shutdown_flag.is_set():
                self.logger.info('Received shutdown signal')
                break    

            zsocks = dict(poller.poll(SUBSCRIBER_SOCKET_TIMEOUT))
            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                try:
                    [topic, data] = self.subscriber.recv_multipart()
                except zmq.ZMQError as e:
                    self.logger.exception('recv_multipart failed for %s' % topic)
                    if e.errno == zmq.ETERM:
                        break           # Interrupted
                except KeyboardInterrupt:
                    break
                self.logger.debug('Received a message for %s - [%s]...' % (topic, repr(data)))
                #start_time = timeit.default_timer()
                self.bmu_id = int(topic[3:-1])
                self.bmu_data = json.loads(data)
                #self.logger.debug(self.db_values_dict)
                self.process_register_set()
                #elapsed = timeit.default_timer() - start_time
                #self.logger.debug('elapsed time: %s' % elapsed)

                self.db_values_dict.clear()

        self.subscriber.close()
        self.ctx.term()
    

class EveBmuProtectionSubscriberFactoryDaemon(Daemon):

    polling_freq_sec = EVE_BMU_DEFAULT_POLLING_FREQ

    def __init__(self, **kwargs):
        super(EveBmuProtectionSubscriberFactoryDaemon, self).__init__(**kwargs)
        self.thread_list = []
        self.sds_list = []
        self.bmu_list = []
        self.db_errors = 0

    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread__stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def get_operational_parameters(self):
        sql = "SELECT %s FROM %s;" % (AESS_SERIAL_DEVICE_SERVER_TABLE_SDS_ID_COLUMN_NAME,
                                      DB_AESS_SERIAL_DEVICE_SERVER_TABLE_NAME)
        try:
            response = prepared_act_on_database(FETCH_ALL, sql, [])
        except Exception:
            self.logger.critical('Unable to fetch SDS operational parameters.')
            raise DatabaseError('Failed to fetch SDS operational parameters.')
        for sid in response:
            self.sds_list.append(sid['sds_id'])
        
    def run(self):
        self.update()
        self.get_operational_parameters()

        try:
            ctx = zmq.Context.instance()
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        for sds_id in self.sds_list:
            try:
                thread_name = 'sds%d' % int(sds_id)
                thread = EveBmuProtectionSubscriberThread(sds_id, ctx, name=thread_name)
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            
            try:
                self.logger.debug('Starting protection subscriber thread sds%s' % sds_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)
        
        """
        @note: The factory joins a thread in its list every 10 seconds, taking approximately
               300 seconds to process the list.
        """
        while True:
            self.update()

            try:
                for thread in self.thread_list:
                    thread_name = ''
                    try:
                        thread_name = thread.getName() 
                        thread.join(10.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('EveBmuProtectionSubscriber thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        sds_num = int(thread_name[3:])
                        thread = EveBmuProtectionSubscriberThread(sds_num, ctx, name=thread_name)
                        try:
                            self.logger.debug('Restarting data subscriber thread sds%s' % sds_num)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                break
                
        self.logger.warning('EveBmuProtectionSubscriberFactory fell through main loop')
        return False


if __name__ == '__main__':
    e = EveBmuProtectionSubscriberFactoryDaemon()
    e.run()
