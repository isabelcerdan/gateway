"""
Created on Dec 17, 2018

@author: steve
"""

import zmq
from zmq.backend.cython.utils import ZMQError
from lib.logging_setup import setup_logger
from lib.exceptions import ZmqError
from lib.gateway_constants.ZmqConstants import MSG_BROKER_CAPTURE_URL
from ess.ess_daemon import EssDaemon
from alarm.alarm_constants import ALARM_ID_MSG_BROKER_MONITOR_RESTARTED


class MsgBrokerMonitor(EssDaemon):
    alarm_id = ALARM_ID_MSG_BROKER_MONITOR_RESTARTED

    def __init__(self, **kwargs):
        super(MsgBrokerMonitor, self).__init__(**kwargs)
        self.ctx = None
        self.capture = None
        self.poller = None
        self.logger = setup_logger('MsgBrokerMonitor')

    def run(self):
        self.ctx = zmq.Context()
        try:
            self.capture = self.ctx.socket(zmq.SUB)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        try:
            sub_str = u''
            self.capture.connect(MSG_BROKER_CAPTURE_URL)
            self.capture.setsockopt_string(zmq.SUBSCRIBE, sub_str)
        except zmq.ZMQBindError as error:
            error_string = 'Unable to connect to capture socket - %s' % repr(error)
            self.logger.exception(error_string)
            self.capture.close()
            self.ctx.term()
            raise ZmqError(error_string)
        except Exception as error:
            self.logger.exception(error)
            raise ZmqError(error)

        try:
            self.poller = zmq.Poller()
            self.poller.register(self.capture, zmq.POLLIN)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)
        
        while True:
            self.update()
            zsocks = dict(self.poller.poll(5000))
            if self.capture in zsocks and zsocks[self.capture] == zmq.POLLIN:
                try:
                    [topic, data] = self.capture.recv_multipart()
                except zmq.ZMQError as e:
                    self.logger.exception('recv_multipart failed for %s' % topic)
                    if e.errno == zmq.ETERM:
                        break           # Interrupted
                except KeyboardInterrupt as error:
                    self.logger.exception(error)
                    break
                except ValueError as error:
                    self.logger.exception(error)
                    continue

                self.logger.debug('Received: %s - %s' % (topic, data))


if __name__ == '__main__':
    pass
