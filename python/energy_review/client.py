import time
import datetime
import requests
from requests.adapters import HTTPAdapter
from pytz import timezone
from tzlocal import get_localzone

from lib.db import prepared_act_on_database, get_default, FETCH_ONE, EXECUTE
from lib.gateway_constants.DBConstants import DB_ENERGY_REPORT_CONFIG_TABLE
from lib.logging_setup import setup_logger
from meter_reading_mapping import meter_reading_mapping
from catch_up_report import CatchUpReport
import alarm

# increment if ER/AQ needs to know about different data handling
SCHEMA_VERSION = 1


class Client(object):
    DEFAULT_TRANSMIT_FREQUENCY = 15 * 60
    DEFAULT_TIMEOUT = 1
    DEFAULT_RETRIES = 1
    MAX_BATCH_SIZE = 100

    def __init__(self, **kwargs):
        self.enabled = kwargs.get('enabled', 0) == 1
        self.gateway_base_sn = kwargs.get('gateway_base_sn', None)
        self.url_aq_meter = kwargs.get('URL_AQ_Meter', None)
        self.interval = get_default(kwargs, 'transmit_frequency', Client.DEFAULT_TRANSMIT_FREQUENCY)
        self.timeout = get_default(kwargs, 'timeout', Client.DEFAULT_TIMEOUT)
        self.last_sent = kwargs.get('last_sent', None)
        self.retries = kwargs.get('retries', Client.DEFAULT_RETRIES)
        self.max_batch_size = kwargs.get('max_catchup_report_batch_size', Client.MAX_BATCH_SIZE)
        self.logger = setup_logger("er-client")

    def reload(self):
        sql = "SELECT * FROM energy_report_config"
        record = prepared_act_on_database(FETCH_ONE, sql, [])
        if record:
            return self.__init__(**record)

    def send(self, gateway_readings):
        """
        Send gateway readings to Energy Review. First we map the readings and then
        we send the readings to Energy Review as an energy report.

        :param gateway_readings: dictionary of gateway readings (e.g. pcc_readings_1, etc.)
        :return: True if send was successful. False if send was unsuccessful.
        """
        send_success = False
        try:
            # Gateway meter is identified by its gateway serial number and brainbox id
            er_readings = self.map_readings(gateway_readings)
            params = '&'.join(['='.join([k, str(v)]) for k, v in er_readings.items()]).replace(' ', '%20')
        except Exception as ex:
            self.logger.error("Failed to create post-dict: %s" % str(ex))
            return False

        self.logger.info("Attempting to send energy report %s for %s..." %
                         (er_readings['time'], er_readings['SerialNumber']))
        try:
            s = requests.Session()
            s.mount("http://", HTTPAdapter(max_retries=self.retries))
            r = s.get(self.url_aq_meter, params=params, timeout=self.timeout)
            if r.status_code == 200:
                send_success = True
            else:
                self.logger.info("ER response had status code %d." % (r.status_code))
        except requests.exceptions.Timeout:
           self.logger.warning("Request timed out")
        except Exception as ex:
           self.logger.error("ER get request failed: %s" % str(ex))

        if send_success:
            self.logger.info("Successfully sent energy report %s for %s..." %
                             (er_readings['time'], er_readings['SerialNumber']))
            self.update_last_sent()
        else:
            self.logger.info("Failed to send energy report %s for %s..." %
                             (er_readings['time'], er_readings['SerialNumber']))

        return send_success

    def serial_number(self, brainbox_id):
        return "-".join([self.gateway_base_sn, str(brainbox_id)])

    def map_readings(self, gateway_readings):

        # Energy Review expects time to be sent in UTC. The timestamp stored in gateway
        # readings tables are in local time (unfortunately) so we need to convert to UTC.
        utc_timestamp = get_localzone().localize(gateway_readings['timestamp']).astimezone(timezone('UTC'))

        # Set up the list of info to send, with info that can't be found from gateway_readings
        er_readings = {
            'SerialNumber': self.serial_number(gateway_readings['brainbox_id']),
            'deviceclass': 48,
            'modbusdevicenumber': gateway_readings['modbusdevicenumber'],
            'time': datetime.datetime.strftime(utc_timestamp, '%Y-%m-%d %H:%M:%S'),
            'schema': SCHEMA_VERSION
        }

        # map gateway readings to energy review readings
        for gateway_field, er_field in meter_reading_mapping.items():
            if gateway_field in gateway_readings and gateway_readings[gateway_field] is not None:
                er_readings[er_field] = gateway_readings[gateway_field]

        return er_readings

    def update_last_sent(self):
        sql = "UPDATE %s SET %s = %s" % (DB_ENERGY_REPORT_CONFIG_TABLE, 'last_sent', "%s")
        prepared_act_on_database(EXECUTE, sql, [time.time()])

    def check_alarm(self):
        # raise an alarm if we haven't sent a report in 30 minutes
        if (self.last_sent + 60 * 30) < time.time():
            alarm.Alarm.occurrence(alarm.ALARM_ID_ENERGY_REVIEW_SEND_DATA_FAILURE)

    def wait(self):
        """
        The interval value is the number of seconds until the next energy report
        should be transmitted to energy review. Here we determine how long we
        should sleep before sending out the next energy report. We add one second
        to ensure the "on-interval" readings are ready by the time we awake.
        :return:
        """
        time.sleep((self.interval - (time.time() % self.interval)) + 1)

    def save_catch_up_report(self, gateway_readings):
        # remove row id and entry id since these fields are auto-populated when creating new catchup report
        del gateway_readings['row_id']
        del gateway_readings['entry_id']

        data = ", ".join(["%s='%s'" % (k, v) for k, v in gateway_readings.iteritems()])
        sql = "REPLACE INTO energy_report_catchup SET row_id = (SELECT MOD(COALESCE(MAX(entry_id), 0), %s) + 1 " \
              "FROM energy_report_catchup AS t), %s;" % (CatchUpReport.MAX_CATCHUP_REPORTS, data)
        prepared_act_on_database(EXECUTE, sql, [])
        self.logger.info("Saved catch-up report %s for %s." %
                         (gateway_readings['timestamp'], self.serial_number(gateway_readings['brainbox_id'])))

    def send_catch_up_reports(self):
        """
        Queries the catch-up reports table and attempts to send them up to Energy Review.
        Only sends a batch at a time so as not to flood the server with too many reports
        all at once. Batch size is user specified and can be changed in table
        energy_report_config : column max_batch_size.
        """

        count = CatchUpReport.count()
        if count > 0:
            batch_size = min(count, self.max_batch_size)
            self.logger.info("Found %s catch-up reports. Send batch of %s to Energy Review." % (count, batch_size))

            # try sending any catch-up reports that may have accumulated
            for catch_up_report in CatchUpReport.find_all(batch_size):
                self.logger.debug("Attempting to send catch-up report (row_id=%s)..." % (catch_up_report['row_id']))
                if self.send(catch_up_report):
                    self.logger.debug("Successfully sent catch-up report (row_id=%s)..." % (catch_up_report['row_id']))
                    CatchUpReport.delete_by(row_id=catch_up_report['row_id'])
                    self.logger.debug("Deleted catch-up report (row_id=%s)..." % (catch_up_report['row_id']))
                else:
                    self.logger.warning("Unable to send catch-up report (row_id=%s)..." % (catch_up_report['row_id']))
                    break
    
            remaining_count = CatchUpReport.count()
            num_successful = count - remaining_count
            if num_successful != batch_size:
                self.logger.warning("Backup reports: %s of %s successful. %s of %s failed. Will try again later." %
                                    (num_successful, batch_size, batch_size - num_successful, batch_size))
            else:
                self.logger.info("Successfully sent %s catch-up reports to Energy Review. "
                                 "%s remaining catch-up reports to send." % (batch_size, remaining_count))
        else:
            self.logger.info("No catch up reports outstanding.")

    @staticmethod
    def find():
        sql = "SELECT * FROM %s" % DB_ENERGY_REPORT_CONFIG_TABLE
        record = prepared_act_on_database(FETCH_ONE, sql, [])
        if record:
            return Client(**record)
        else:
            return None

