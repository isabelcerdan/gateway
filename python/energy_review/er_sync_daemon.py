from alarm.alarm import *
from client import Client
from meter.meter import Meter
from daemon.daemon import Daemon


class ERSyncDaemon(Daemon):

    alarm_id = ALARM_ID_ER_SYNC_DAEMON_RESTARTED
    update_timeout = 900.0
    source_table = "energy_report_config"
    source_id_field = "gateway_base_sn"

    def __init__(self, **kwargs):
        super(ERSyncDaemon, self).__init__(**kwargs)
        self.client = Client.find()
        self.meters = Meter.find_all_by(ER_Connect=1, enabled=1)

    def run(self):
        while True:
            self.update()
            self.client.wait()
            self.client.reload()
            if self.client.enabled:
                connected = True
                self.meters = Meter.find_all_by(ER_Connect=1, enabled=1)
                for meter in self.meters:
                    readings = meter.current_interval_readings()
                    if readings is not None:
                        # avoid having to wait for timeout when not connected
                        if connected:
                            connected = self.client.send(readings)
                        if not connected:
                            self.client.save_catch_up_report(readings)
                self.client.check_alarm()
                if connected:
                    self.client.send_catch_up_reports()
