from lib.db import prepared_act_on_database, EXECUTE, FETCH_ALL, FETCH_ROW_COUNT, ROW_COUNT_COLUMN_NAME
from lib.gateway_constants.DBConstants import ER_CATCHUP_REPORTS_TABLE_NAME


class CatchUpReport(object):
    MAX_CATCHUP_REPORTS = 60*24*3*4     # 60 minutes/hour x 24 hours/day x 3 days x 4 meters = 17280

    @staticmethod
    def count():
        sql = 'SELECT %s FROM %s' % (ROW_COUNT_COLUMN_NAME, ER_CATCHUP_REPORTS_TABLE_NAME)
        return prepared_act_on_database(FETCH_ROW_COUNT, sql, None)

    @staticmethod
    def delete_by(**kwargs):
        sql = "DELETE FROM energy_report_catchup"
        where_clause = " AND ".join(["%s = %s" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        prepared_act_on_database(EXECUTE, sql, None)

    @staticmethod
    def find_all(batch_size):
        sql = 'SELECT * FROM %s ORDER BY timestamp ASC LIMIT %s' % (ER_CATCHUP_REPORTS_TABLE_NAME, batch_size)
        return prepared_act_on_database(FETCH_ALL, sql, None)
