#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import socket
import struct
import time
import sys
import urllib
import pexpect
import MySQLdb as MyDB
from contextlib import closing
import MySQLdb.cursors as my_cursor

from lib.db import prepared_act_on_database

import os
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *

import httplib

import gateway_utils as gu


from lib.logging_setup import *

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_UPG_ONE_TFTP_CLOSE_ERROR, \
                                  ALARM_ID_UPG_ONE_MISSING_BOOT_ERROR, \
                                  ALARM_ID_UPG_ONE_UPGRADE_FAILURE_INTERVENTION_REQUIRED, \
                                  ALARM_ID_UPG_ONE_PERSISTENT_APP_MODE_AFTER_URESET, \
                                  ALARM_ID_UPG_ONE_PROCESS_EXCEPTION, \
                                  ALARM_ID_UPG_ONE_DB_BOOT_REV_BOOT_COMBO_NOT_ALIGNED, \
                                  ALARM_ID_UPG_ONE_SUSPICIOUS_UPGRADE_FILE, \
                                  ALARM_ID_UPG_ONE_INVAL_RUNMODE_IN_QUERY_RESPONSE


logger = setup_logger('upgrade_one_inverter')

MAX_NUMBER_GET_RUNMODE_RETRIES = 2

MAX_CONSECUTIVE_TFTP_2_BLOCK_COUNT = 8

TFTP_XFER_RESULT_UNKNOWN = 0
TFTP_XFER_RESULT_SUCCESSFUL = 1
TFTP_XFER_RESULT_FAILED_LIKELY_COMM = 2
TFTP_XFER_RESULT_FAILED_LIKELY_REJECTED = 3
TFTP_XFER_RESULT_FAILED_INVAL_FILE = 4

# Provide a "start transfer delay" of the upgrade process based on string position:
#
# -> inverters closer to the head-of-string have a greater start delay than those closer to
#    the end-of-string. The desire is to allow, as much as possible, those inverters closer
#    to the end of a string to complete the transfer before those inverters ahead of them,
#    to minimize TFTP transfer disruptions when inverters reset.
#
# NOTE: because of vagaries an ethernet network in conjunction with packet transmission between duets within
#       a string, the attempt to stagger the start of TFTP transmissions did not work as well as hoped. It
#       turns out that in order to guarantee that end-inverters complete a transfer before any other inverter
#       ahead in a string, the start delay had to be huge. On the other hand, if upgrading a large number of
#       inverters, then the probability of 2 of more inverters in the same string being upgraded at the same
#       time is low. So the strat delay based on string position did not do all that much good.
#
# Conclusion: might be better off with a BOOT LOADER/BOOT UPDATER code change in the inverter, with a longer
#             timeout before the inverter resets. Say: an additional 15 or 20 seconds?
STRING_POSITION_DELAY = [21, 18, 15, 12, 9, 6, 3, 0]

#
# Over-all process description can be found in the daemon_description WORD document.
#
# This script does NOT run as a daemon: it is launched by the upgrade_all_script.py
# script, runs to completion and terminates.
#
# Parameters passed to this script when invoked by upgradeAllInverters:
#
# - the index into the inverters table for the inverter to be upgraded
# - the application filename for the TFTP transfer
# - the bootloader filename for the TFTP transfer
# - the value of the latest boot loader for all inverters
# - logging handler (gotten by the mama script)


class UpgradeOneInverterScript(object):
    def __init__(self, var_splat):
        (this_inverters_id, this_upgrade_file, this_bootloader_file, latest_boot_version_short, one_inverter_logger) = var_splat

        # Retrieve the parameters:
        self.inverters_id = this_inverters_id
        self.inverter_upgrade_file = this_upgrade_file
        self.application_upgrade_file = this_upgrade_file
        self.bootloader_upgrade_file = this_bootloader_file
        self.latest_boot_version_short = latest_boot_version_short
        self.preliminary_upgrade_file = ""
        self.my_logger = one_inverter_logger

        self.ip_to_upgrade = None
        self.inverter_upgrade_file_size = 0
        self.runmode = RUNMODE_UNKNOWN
        self.upgrade_result = False
        self.tftp_transfer_result = TFTP_RESULT_UNKNOWN
        self.upgrade_file_for_tftp_process = None
        self.number_of_blocks_sent = 0
        self.remainder_as_percent = 0
        self.total_blocks_estimated = 0
        self.boot_loader_version_short = 0
        self.number_of_user_tlv = 0

        self.version_string = None
        self.boot_version_string = None
        self.xet_info_string = None
        self.number_of_resets = 0

        self.boot_updater_set_rstpri_result = 1
        self.boot_updater_clear_rstpri_result = 1
        self.boot_updater_set_rstpri_result_present = False
        self.boot_updater_clear_rstpri_result_present = False
        self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_NO_ACTION
        self.preliminary_upgrade_required = False

        # For timing display purpose:
        self.start_utc_time = int(time.time())
        self.end_utc_time = 0

        # With the logger setup performed by the calling script, no local log setup is required here.

        # ---------------------------------------
        try:
            self.upgrade_one_inverter()  # <- CALL THE SCRIPT'S MAIN PROCESS!!!
        except Exception as error:
            self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_PROCESS_EXCEPTION)
        # ---------------------------------------

    def log_info(self, message):
        self.my_logger.info(message)

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)

    def set_upgrade_progress(self):
        set_progress_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_progress_args = (self.remainder_as_percent, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_progress_string, set_progress_args)

    def set_db_inverters_upgrade_status(self):
        # When writing the upgrade_status field, leave the upgrade_progress field alone: if failed, the progress value
        # could conceivably be useful for investigation. If anyone cared to look.
        if self.upgrade_result:
            # Write the upgrade status field, as well as the version, bootloader version, the reset count,
            # set the uptime and WATTS to 0, and COMM to APP.
            final_status_string = "UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s where %s=%s;" \
                                  % (DB_INVERTER_NXO_TABLE,
                                     INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_VERSION_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_BOOT_VERSION_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_RESETS_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_UPTIME_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
            final_status_args = (self.final_upgrade_status, self.version_string, self.boot_version_string, self.number_of_resets, 0,
                                 INVERTERS_TABLE_COMM_STRING_SG424_APPLICATION, self.inverters_id)
        else:
            # Only the upgrade status field in the table is written.
            final_status_string = "UPDATE %s SET %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                        INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
            final_status_args = (self.final_upgrade_status,  self.inverters_id)
        prepared_act_on_database(EXECUTE, final_status_string, final_status_args)

    def is_get_query_by_tlv(self):
        # Purpose: query an inverter and retrieve its runmode and other data related to upgrade.
        # Because of inverter resets, a timeout and a single retry is done upon query failure.
        get_query_succeeded = False
        query_tlv_list = []
        query_data_list = []

        SG424_data = gu.send_and_get_query_data(self.ip_to_upgrade)
        if not SG424_data:
            # One more time after a short wait.
            time.sleep(20)
            SG424_data = gu.send_and_get_query_data(self.ip_to_upgrade)
        (query_tlv_list, query_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)

        if (len(query_tlv_list) == len(query_data_list)) and len(query_tlv_list) > 0:
            get_query_succeeded = True
            self.boot_updater_set_rstpri_result_present = False
            self.boot_updater_clear_rstpri_result_present = False
            # Loop through the user TLVs anx extract the TLVs of interest. All others are ignored.
            for iggy in range(0, len(query_tlv_list)):

                if query_tlv_list[iggy] == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                    # Validate the runmode returned by the inverter. It might have lost its mind.
                    runmode = query_data_list[iggy]
                    if (runmode == RUNMODE_SG424_APPLICATION) or (runmode == RUNMODE_SG424_BOOT_LOADER) or (runmode == RUNMODE_SG424_BOOT_UPDATER):
                        self.runmode = query_data_list[iggy]
                    else:
                        self.runmode = RUNMODE_UNKNOWN

                elif query_tlv_list[iggy] == SG424_TO_GTW_VERSION_STRING_TLV_TYPE:
                    self.version_string = query_data_list[iggy]
                    # A valid string was extracted. Update the database with this version.
                    gu.update_inverters_version_column_by_ip(self.version_string, self.ip_to_upgrade)

                elif query_tlv_list[iggy] == SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE:
                    self.xet_info_string = query_data_list[iggy]

                elif query_tlv_list[iggy] == SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE:
                    self.number_of_resets = query_data_list[iggy]
                    gu.update_inverters_resets_column_by_ip(self.ip_to_upgrade, self.number_of_resets)

                elif query_tlv_list[iggy] == SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE:
                    # This "string" is just a number, no "R" preceeding it. Convert it for subsequent use.
                    self.boot_version_string = query_data_list[iggy]
                    self.boot_loader_version_short = int(self.boot_version_string)

                elif query_tlv_list[iggy] == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_SET_RESULT_U16_TLV_TYPE:
                    self.boot_updater_set_rstpri_result = query_data_list[iggy]
                    self.boot_updater_set_rstpri_result_present = True

                elif query_tlv_list[iggy] == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_CLEAR_RESULT_U16_TLV_TYPE:
                    self.boot_updater_clear_rstpri_result = query_data_list[iggy]
                    self.boot_updater_clear_rstpri_result_present = True

                else:
                    # All other TLVs that may be returned in a query are donut_care. The same holds true
                    # for all other TLVs that may be unknown to the Gateway itself.
                    pass
            # end ... for loop

        else:
            # Zero-length list: no response.
            pass
        return get_query_succeeded

    def transfer_hex_file(self):
        # Perform a TFTP transfer. Prepare the string to be written to the log.
        log_string = "IP %s: TFTP " % (self.ip_to_upgrade)
        tftp_xfer_result = TFTP_XFER_RESULT_UNKNOWN
        self.tftp_transfer_result = TFTP_ATTEMPTED_AND_FAILED
        self.upgrade_result = False
        if not self.upgrade_file_for_tftp_process:
            self.log_info("ip %s: transfer_hex_file HAS NULL FILE" % (self.ip_to_upgrade))
            return TFTP_XFER_RESULT_FAILED_INVAL_FILE

        tftp_command = "/usr/bin/tftp " + self.ip_to_upgrade
        put_command = "put " + self.upgrade_file_for_tftp_process
        expect_this = pexpect.spawn(tftp_command)

        # Not sure if these 2 statements are required for TFTP purpose:
        expect_this.maxsize = 1  # Turns off buffering
        expect_this.timeout = 75  # default is 30

        # The original timeout of 300 (5mins) was too long, eventually settling on 90 seconds.
        # Setting "rexmt 1" had the effect of re-transmitting every 1 second for the timeout
        # duration, and that's just way too fast: if a group of inverters are behind others
        # that are going through a reset, there's no value in sending so fast. So it is set
        # to 5.
        expect_this.expect('tftp> ', timeout=10)
        expect_this.sendline('binary')
        expect_this.sendline('rexmt 5')
        expect_this.sendline('timeout 90')
        expect_this.sendline('trace')
        expect_this.sendline(put_command)
        expect_this.sendline('quit')

        self.number_of_blocks_sent = 0
        previous_percent_mark = 0
        block_number_as_string = "0"
        last_block_number_as_string = "0"
        retransmitted_packet_count = 0

        number_of_2_blocks_only_sent = 0

        for tftp_response in expect_this:
            if 'sent DATA' in tftp_response:
                # Extract the block number from the response, which looks like this:
                #
                # sent DATA <block=482, 512 bytes>
                # received ACK <block=482>
                #
                # So split the string based on "equal" symbol, then split the 2nd
                # component based on "comma":
                #
                # A block count of 2 is significant for inverter upgrade: if the count remains at only 2 blocks
                # for a consecutive number of times, it is an indication that the inverter is rejecting the file.
                # This happens when the inverter is expecting an R6R upgrade file containing the "special signature"
                # but it is missing, or the inverter is running an older version of the boot loader when it is not
                # expecting the "special signature" but the upgrade file contains it.
                sent_data_as_components = tftp_response.split('=')
                block_as_components = sent_data_as_components[1].split(',')
                block_number_as_string = block_as_components[0]

                if block_number_as_string == "2":
                    number_of_2_blocks_only_sent += 1
                    if number_of_2_blocks_only_sent > MAX_CONSECUTIVE_TFTP_2_BLOCK_COUNT:
                        # Upgrade file rejection by the inverter. Give up now, do not wait until "Transfer timed out".
                        log_string += "FAILED (2BC) - LIKELY FILE REJECTION (%s BLOCKS OUT OF %s)" % (block_number_as_string, self.total_blocks_estimated)
                        self.log_info(log_string)
                        tftp_xfer_result = TFTP_XFER_RESULT_FAILED_LIKELY_REJECTED
                        break

                # Differentiating a packet transmit from a packet re-transmit (otherwise,
                # the percentage of completion can come out to more than 100%).
                if self.number_of_blocks_sent == 0:
                    # This is the 1st packet. So:
                    last_block_number_as_string = block_number_as_string
                    self.number_of_blocks_sent += 1
                else:
                    # Not the 1st block. Is this block being re-transmitted?
                    if last_block_number_as_string != block_number_as_string:
                        # Not re-transmitted.
                        last_block_number_as_string = block_number_as_string
                        self.number_of_blocks_sent += 1
                    else:
                        # Packet re-transmitted: increment total packet re-transmit count.
                        retransmitted_packet_count += 1

                # Update progress based on 1% increment of completion.
                self.remainder_as_percent = (self.number_of_blocks_sent * 100) / self.total_blocks_estimated
                if self.remainder_as_percent != 0:
                    current_percent_mark = self.remainder_as_percent % 1
                    if current_percent_mark == 0:
                        # The percentage boundary has been reached. Display it but once.
                        if self.remainder_as_percent != previous_percent_mark:
                            previous_percent_mark = self.remainder_as_percent
                            self.set_upgrade_progress()

            elif 'Sent ' in tftp_response:
                # This string is returned by TFTP when the transfer is complete.
                self.upgrade_result = True
                self.tftp_transfer_result = TFTP_ATTEMPTED_AND_SUCCEEDED
                log_string += "SUCCESS (%s), BLOCKS XMIT'D = %s (RE-XMIT = %s)" % (self.upgrade_file_for_tftp_process, self.number_of_blocks_sent, retransmitted_packet_count)
                self.log_info(log_string)
                tftp_xfer_result = TFTP_XFER_RESULT_SUCCESSFUL
                break

            elif 'Transfer timed out.' in tftp_response:
                # TFTP process failed. The actual number of blocks sent including re-transmits is in the
                # self.number_of_blocks_sent field. Extract the number of actual blocks transmitted.
                number_blocks_sent = int(block_number_as_string)
                if number_blocks_sent == 2:
                    # Normally, if this happens, this upgrade effort would have given up earlier, as determined by
                    # the "2-block count" detection mechanism above.
                    log_string += "FAILED (TMO) - LIKELY FILE REJECTION (%s BLOCKS OUT OF %s)" % (block_number_as_string, self.total_blocks_estimated)
                    self.log_info(log_string)
                    tftp_xfer_result = TFTP_XFER_RESULT_FAILED_LIKELY_REJECTED
                else:
                    log_string += "FAILED, DUE TO TIME OUT (%s BLOCKS OUT OF %s)" % (block_number_as_string,
                                                                                     self.total_blocks_estimated)
                    self.log_info(log_string)
                    tftp_xfer_result = TFTP_XFER_RESULT_FAILED_LIKELY_COMM
                break

            else:
                # Just wait for one of the above. Normally, if you print the response, you get:
                #
                # tftp_response = binary
                # tftp_response = tftp> rexmt 5
                # tftp_response = tftp> timeout 90
                # tftp_response = tftp> trace
                # tftp_response = Packet tracing on.
                # tftp_response = tftp> put /usr/share/apparent/upload/SG424_DevComboR313p1.hex
                # tftp_response = sent WRQ <file=/usr/share/apparent/upload/SG424_DevComboR313p1.hex, mode=octet>
                # tftp_response = received ACK <block=0>
                # tftp_response = received ACK <block=1>
                # .
                # .
                # .
                # tftp_response = quit
                pass

        # Note: When upgrades were launched from the terminal, there was no error with
        #       the call to interact(). But when launched from the gateway_monitor, it      
        #       would throw the following exception:
        #
        #       expect.interact FAILED [ERROR]: (25, 'Inappropriate ioctl for device')
        #
        #       Yet the transfer succeeded. Since interact() "gives control of the child
        #       process to the interactive user (the human at the keyboard)", then it is
        #       not actually required when launched by the gateway_monitor daemon, or even
        #       from the terminal. So it was removed. It sort of makes sense that it would
        #       throw an exception when launched from a daemon. This note is entered here
        #       for all the trouble it caused during development/testing.
        # try:
        #    expect_this.interact()
        # except Exception as error:
        #    this_string = "expect.interact FAILED"
        #    self.log_major(this_string, error)
        #    return
        #    #break

        try:
            expect_this.close()
        except Exception as error:
            self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_TFTP_CLOSE_ERROR)

        return tftp_xfer_result

    def upgrade_boot_loader_from_updater(self):
        # Come here only if the inverter is currently in boot updater mode. Start the upgrade with the
        # bootCombo file, which will install the boot loader. The updater's version has been validated.
        boot_loader_upgrade_ok = False
        self.bootloader_upgrade_file = self.bootloader_upgrade_file.replace('LEGACY_', '')
        if not os.path.isfile(self.bootloader_upgrade_file):
            # Missing???
            self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_MISSING_BOOT_ERROR)
        else:
            # The file exists, use it.
            self.upgrade_file_for_tftp_process = self.bootloader_upgrade_file
            bootloader_file_size = os.path.getsize(self.bootloader_upgrade_file)
            self.total_blocks_estimated = bootloader_file_size / 512

            # Change the upgrade status in the database to "new boot loader upgrade in progress",
            set_upgrade_string = "UPDATE %s SET %s=%s, %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                              INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                              INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                              INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
            set_upgrade_args = (INVERTERS_UPGRADE_STATUS_LOADER_IN_PROGRESS, 0, self.ip_to_upgrade)
            prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

            # Do the transfer.
            tftp_xfer_result = self.transfer_hex_file()

            if tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
                # Wait to allow the inverter to reset, come up in loader mode and get an IP.
                boot_loader_upgrade_ok = True
                time.sleep(30)
            else:
                # Reason for failure logged. Nothing else to do here. Upgrade will be deemed as failed.
                pass

        return boot_loader_upgrade_ok

    def send_upgrade_reset_via_http(self):
        # Send the inverter an HTTP-based upgrade reset command.
        #
        # NOTE: For some reason, this suddenly stopped working at one ppoint during development, where it was
        #       observed that the request, though sent on the wire, would not cause the inverter in question to
        #       perform and upgrade reset. Yet, when sent via a web browser, the inverter did reset. Wireshark
        #       captures did show a difference in the packet size, even though the packet contained the magic
        #       "/upgrade.htm?upgradereset" URL in both cases. Unfortunately, most SG424 legacy upgrades is from
        #       the 311.1 release, which does not have counters to indicate of the HTTP request was received.
        #
        # So, this is commented out:
        # packet_data = "/upgrade.htm?upgradereset"
        # # http_serv = httplib.HTTPConnection("127.0.0.1", 80)
        # http_serv = httplib.HTTPConnection(self.ip_to_upgrade, 80, timeout=10)
        # http_serv.connect()
        # http_serv.request("GET", packet_data)

        # ... and instead, the HTP request is sent as an OS "terminal line command":
        terminal_line_command = "sudo wget -T 5 --tries=1 http://" + self.ip_to_upgrade + ":80/upgrade.htm?upgradereset"
        self.log_info(" %s LEGACY INVERTER RESET VIA OS COMMAND: %s" % (self.ip_to_upgrade, terminal_line_command))
        return_value = os.system(terminal_line_command)

        # It is up to the caller to provide sufficient sleep upon return to let
        # the inverter settle down after the reset and get an IP address.

    def is_upgrade_cancelled(self):

        upgrade_cancelled = False
        select_inverter_upgrade_string = "SELECT * FROM %s;" % DB_INVERTER_UPGRADE_TABLE
        inverter_upgrade_row = prepared_act_on_database(FETCH_ONE, select_inverter_upgrade_string, ())
        # Check the command field.
        upgrade_command = str(inverter_upgrade_row[INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME])
        if upgrade_command == INVERTER_UPGRADE_COMMAND_CANCEL_UPGRADE:
            # The user canceled the upgrade.
            upgrade_cancelled = True
        else:
            # Upgrade not cancelled. Continue as usual.
            pass

        return upgrade_cancelled

    def is_inverter_gotten_into_boot_mode(self):
        # Call this only if not running the latest expected version of the boot loader.
        # And call this only if the inverter is in communication.
        in_boot_loader_mode = False
        if self.is_get_query_by_tlv():
            if self.runmode == RUNMODE_SG424_BOOT_LOADER:
                return True

            # Not in boot mode, but at least it is responding to TLV. Send it a TLV-based upgrade reset command.
            if not gu.is_send_upgrade_reset_by_tlv(self.ip_to_upgrade):
                # Never observed. Under normal circumstances. Except when the impossible happens.
                self.log_info("IP %s: FAILED TO SEND TLV UPGRADE RESET[1]" % (self.ip_to_upgrade))
            # Give the inverter time to reset and get an IP.
            time.sleep(30)
            if self.is_get_query_by_tlv():
                if self.runmode == RUNMODE_SG424_BOOT_LOADER:
                    return True

        else:
            # Failed to query by TLV, or no response to TLV. Last ditch effort: HTTP upgrade reset and NBNS query.
            pass
        # Failed to query by TLV, via TLV. Try via HTTP reset and query via NBNS.
        self.xet_info_string = gu.send_and_get_nbns_question(self.ip_to_upgrade, NBNS_XETINFO_QUESTION)
        (self.runmode, self.boot_loader_version_short,self.version_string) = gu.get_inverter_data_from_xet_info_string(self.xet_info_string)
        if self.runmode == RUNMODE_SG424_BOOT_LOADER:
            in_boot_loader_mode = True
        else:
            # Try an HTTP-based upgrade reset. Give the inverter time to reset and get an IP.
            self.send_upgrade_reset_via_http()
            time.sleep(30)
            self.xet_info_string = gu.send_and_get_nbns_question(self.ip_to_upgrade, NBNS_XETINFO_QUESTION)
            (self.runmode, self.boot_loader_version_short,self.version_string) = gu.get_inverter_data_from_xet_info_string(self.xet_info_string)
            if self.runmode == RUNMODE_SG424_BOOT_LOADER:
                in_boot_loader_mode = True
        return in_boot_loader_mode

    def get_prelim_upgrade_file(self):
        this_string = "IP %s: " % (self.ip_to_upgrade)
        if self.xet_info_string:
            # Get the preliminary upgrade file based on XET INFO, which contains the board type.
            if gu.is_inverter_board_rev_6_and_beyond(self.xet_info_string):
                # A rev6 board - use the preliminary R6R file.
                preliminary_upgrade_file = gu.get_inverter_preliminary_upgrade_file_name(INVERTER_UPGRADE_TABLE_POST_REV6_PRELIM_UPGRADE_FILE)
                this_string += "REV6 AND BEYOND PRELIM UPGRADE FILE=%s" % (preliminary_upgrade_file)
            else:
                # Not a rev6 board - use the standard devCombo file.
                preliminary_upgrade_file = gu.get_inverter_preliminary_upgrade_file_name(INVERTER_UPGRADE_TABLE_PRE_REV6_PRELIM_UPGRADE_FILE)
                this_string += "PRE-REV6 PRELIM UPGRADE FILE=%s" % (preliminary_upgrade_file)
        else:
            # XET INFO string is NULL. Go for the pre-rev 6 preliminary file.
            preliminary_upgrade_file = gu.get_inverter_preliminary_upgrade_file_name(INVERTER_UPGRADE_TABLE_PRE_REV6_PRELIM_UPGRADE_FILE)
            this_string += "NULL XET INFO, USE PRE-REV6 PRELIM UPGRADE FILE=%s" % (preliminary_upgrade_file)
        self.log_info(this_string)
        return preliminary_upgrade_file

    def switch_to_other_prelim_upgrade_file(self):
        current_preliminary_upgrade_file = self.preliminary_upgrade_file
        if self.preliminary_upgrade_file == INVERTER_UPGRADE_TABLE_POST_REV6_PRELIM_UPGRADE_FILE:
            self.preliminary_upgrade_file = gu.get_inverter_preliminary_upgrade_file_name(INVERTER_UPGRADE_TABLE_PRE_REV6_PRELIM_UPGRADE_FILE)
        else:
            self.preliminary_upgrade_file = gu.get_inverter_preliminary_upgrade_file_name(INVERTER_UPGRADE_TABLE_POST_REV6_PRELIM_UPGRADE_FILE)
        self.log_info("IP %s: SWITCHING FROM PRELIM %s -> %s" % (self.ip_to_upgrade, current_preliminary_upgrade_file, self.preliminary_upgrade_file))

    def perform_full_preliminary_upgrade(self):
        # 1: Choose the appropriate preliminary upgrade file. Depending on the inverter's hardware revision
        #    (rev 6 or later, or pre-rev 6) in the XET INFO string, the appropriate preliminary upgrade file
        #    is chosen. If the XET INFO string is blank, the pre-rev6 is chosen.
        self.preliminary_upgrade_file = self.get_prelim_upgrade_file()

        # 2: get the inverter into boot loader mode.
        if not self.is_inverter_gotten_into_boot_mode():
            self.log_info("IP %s: PRELIM PROCESS, FAILED TO GET INTO BOOT LOADER MODE" % (self.ip_to_upgrade))
            return False

        # 3: With the inverter in boot loader mode, set the upgrade status as "prelim is in progress",
        #    specify the preliminary file to use for TFTP and transfer it.
        set_upgrade_string = "UPDATE %s SET %s=%s, %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                          INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_upgrade_args = (INVERTERS_UPGRADE_STATUS_PRELIM_IN_PROGRESS, 0, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

        self.upgrade_file_for_tftp_process = self.preliminary_upgrade_file
        tftp_xfer_result = self.transfer_hex_file()
        if not tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
            # Reason for failure logged. If the failure was "likely rejected by the inverter",
            # switch to the other preliminary upgrade file and try that one before giving up
            # (i.e., from the pre-rev6 file to the post-rev 6 file).
            if tftp_xfer_result == TFTP_XFER_RESULT_FAILED_LIKELY_REJECTED:
                # The pre-rev6 was likely rejected because the inverter is looking for the magic rev6 signature in
                # the upgrade file. So try the post-rev6 file that has the signature.
                self.switch_to_other_prelim_upgrade_file()
                self.upgrade_file_for_tftp_process = self.preliminary_upgrade_file
                tftp_xfer_result = self.transfer_hex_file()
                if not tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
                    # Failure to upgrade with both preliminary file types.
                    self.log_info("IP %s: PRELIMINARY UPGRADE FAILED WITH BOTH PRE/POST REV6 FILES" % (self.ip_to_upgrade))
                    return False
            else:
                # Bail for all other failure reasons.
                return False
        # Wait a little longer than normal as the inverter launches the new application code:
        # the preliminary file upgrade often requires extra time for re-programming of the DUET.
        time.sleep(45)

        # 4: with preliminary application file transfer complete, check that inverter is now in application via TLV only.
        if self.is_get_query_by_tlv():
            if not self.runmode == RUNMODE_SG424_APPLICATION:
                self.log_info("IP %s: INVERTER NOT IN APP MODE AS EXPECTED (MODE=%s)" % (self.ip_to_upgrade, self.runmode))
                return False
        else:
            self.log_info("IP %s: NO RESPONSE TO QUERY AFTER SUCCESSFUL PRELIM UPGRADE" % (self.ip_to_upgrade))
            return False

        # 5: at this point, the inverter is running application code that is fully TLV conversant.
        #    Send the inverter a TLV-encoded upgrade reset command, then check for boot loader.
        if not gu.is_send_upgrade_reset_by_tlv(self.ip_to_upgrade):
            # Never observed. Under normal circumstances. Except when the impossible happens.
            self.log_info("IP %s: FAILED TO SEND TLV UPGRADE RESET[2]" % (self.ip_to_upgrade))
        # Give the inverter time to reset and get an IP.
        time.sleep(30)
        if self.is_get_query_by_tlv():
            if not self.runmode == RUNMODE_SG424_BOOT_LOADER:
                # Most likely a comm failure. Or boot loader somehow had the short 20-second timer before jumping
                # to app instead of the longer 2-minute timer.
                self.log_info("IP %s: NOT IN BOOT LOADER MODE AS EXPECTED (MODE=%s)" % (self.ip_to_upgrade, self.runmode))
                return False
        else:
            # No TLV response from the boot loader. It could be a very very old boot loader, so try an NBNS query.
            self.xet_info_string = gu.send_and_get_nbns_question(self.ip_to_upgrade, NBNS_XETINFO_QUESTION)
            (self.runmode, self.boot_loader_version_short,self.version_string) = gu.get_inverter_data_from_xet_info_string(self.xet_info_string)
            if not self.runmode == RUNMODE_SG424_BOOT_LOADER:
                self.log_info("IP %s: NO TLV/NBNS RESPONSE AFTER UPGRADE RESET (MODE=%s)" % (self.ip_to_upgrade, self.runmode))
                return False

        # 6: come here because the inverter is in boot loader mode. Transfer the bootCombo file, expected that
        #    afterwards, the inverter will come up in boot updater mode.
        #
        #    As with the 2 vesions of the preliminary applciation file that was just transfered, there are 2 versions
        #    of the bootCombo: "LEGACY" version that does not contain the magic rev6 signature, and the rev6-ready
        #    version which does contain the rev6 signature. The method used to determine if rev6 or not was based on
        #    the serial number; but not only did that prove to be unreliable, it may not be available here if this
        #    upgrade is an attempt to recover from a previous failure. So bootCombo transfer is 1st attempted with
        #    the LEGACY version of the bootCombo, and if that should fail, a 2nd transfer attempt is made with
        #    the non-LEGACY version.
        #
        #    Set the upgrade status in the inverters table to indicate that "updater is in progress".
        set_upgrade_string = "UPDATE %s SET %s=%s, %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                          INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_upgrade_args = (INVERTERS_UPGRADE_STATUS_UPDATER_IN_PROGRESS, 0, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

        self.upgrade_file_for_tftp_process = self.bootloader_upgrade_file
        tftp_xfer_result = self.transfer_hex_file()
        if not tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
            # Reason for failure logged. As this was an attempt to transfer the DevCombo file, if the reason for
            # failure was "likely file rejection", then change the bootCombo file from LEGACY to non-LEGACY, and
            # try one more time. A short wait is required to allow the transfer task on the inverter to redress
            # itself before a new transfer can be initiated.
            time.sleep(10)
            self.upgrade_file_for_tftp_process = self.bootloader_upgrade_file
            self.upgrade_file_for_tftp_process = self.upgrade_file_for_tftp_process.replace("_LEGACY", "")
            self.log_info("IP %s: XFER FAILED, SWITCH FROM %s -> %s" % (self.ip_to_upgrade,
                                                                        self.bootloader_upgrade_file,
                                                                        self.upgrade_file_for_tftp_process))
            tftp_xfer_result = self.transfer_hex_file()
            if not tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
                # Failed to transfer both versions of the bootCombo. Abject failure either way.
                self.log_info("IP %s: XFER FAILED WITH BOTH BOOT COMBO FILE TYPES" % (self.ip_to_upgrade))
                return False

        # Sleep to allow inverter to come up in boot updater mode.
        time.sleep(30)
        if self.is_get_query_by_tlv():
            if self.runmode == RUNMODE_SG424_BOOT_UPDATER:
                # Make sure it is the boot loader version as expected.
                if self.boot_loader_version_short == self.latest_boot_version_short:
                    self.log_info("IP %s: INVERTER RUNNING LATEST R%s UPDATER" % (self.ip_to_upgrade, self.boot_loader_version_short))
                else:
                    # This is very unexpected. Upgrade with the boot combo file should yield an inverter running
                    # the latest expected boot version. Most likely it is a database issue: the bootCombo file
                    # to use is not aligned with the boot_rev value in the inverter_upgrade table.
                    self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_DB_BOOT_REV_BOOT_COMBO_NOT_ALIGNED)
                    self.log_info("IP %s: INVAL UPDATER VERSION (%s EXPECTING %s), file=%s" % (self.ip_to_upgrade,
                                                                                               self.boot_loader_version_short,
                                                                                               self.latest_boot_version_short,
                                                                                               self.bootloader_upgrade_file))
                    return False
            else:
                self.log_info("IP %s: INVERTER NOT IN UPDATER MODE AS EXPECTED (mode=%s)" % (self.ip_to_upgrade, self.runmode))
                return False
        else:
            self.log_info("IP %s: NO RESPONSE TO QUERY AFTER UPGRADING TO UPDATER" % (self.ip_to_upgrade))
            return False

        # 7: Transfer the bootCombo file again, then check that the inverter comes up in BOOT LOADER mode.
        #    If LOADER, verify it has the expected version. Since it is running with the latest boot updater,
        #    the bootCombo file to use is the rev6 version of the file - so remove _LEGACY from the file name
        #    if not already done so.
        #
        #    Set the upgrade status in the inverters table to indicate that "loader is in progress".
        set_upgrade_string = "UPDATE %s SET %s=%s, %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                          INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_upgrade_args = (INVERTERS_UPGRADE_STATUS_LOADER_IN_PROGRESS, 0, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

        self.upgrade_file_for_tftp_process = self.bootloader_upgrade_file
        self.upgrade_file_for_tftp_process = self.upgrade_file_for_tftp_process.replace("_LEGACY", "")
        tftp_xfer_result = self.transfer_hex_file()
        if not tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
            # Reason for failure logged. Nothing else to do here.
            return False
        time.sleep(30)
        if self.is_get_query_by_tlv():
            if self.runmode == RUNMODE_SG424_BOOT_LOADER:
                # Make sure it is the boot loader version as expected.
                if self.boot_loader_version_short == self.latest_boot_version_short:
                    self.log_info("IP %s: INVERTER RUNNING LATEST R%s LOADER" % (self.ip_to_upgrade, self.boot_loader_version_short))
                else:
                    self.log_info("IP %s: INVAL LOADER VERSION, GOT %s EXPECTING %s" % (self.ip_to_upgrade,
                                                                                        self.boot_loader_version_short,
                                                                                        self.latest_boot_version_short))
                    return False
            else:
                self.log_info("IP %s: INVERTER NOT IN BOOT LOADER AS EXPECTED" % (self.ip_to_upgrade))
                return False
        else:
            self.log_info("IP %s: NO RESPONSE TO QUERY AFTER UPGRADING TO *NEW* LOADER" % (self.ip_to_upgrade))
            return False

        # Got here, all is fine.
        return True

    def verify_upgrade_readiness(self):
        # Firmware upgrade is simple and straightforward once an inverter is running the latest boot loader.
        # If not, a pre-upgrade of the *new* boot loader will be performed, where the gateway enforces boot
        # loader upgrade, the user has no say in this. Additionally, as part of the boot loader pre-upgrade
        # when needed, the application firmware itself must be pre-upgraded if it is not running a minimum
        # level of a pre-determined SVN number (part of the version string), because the DUET must be running
        # a version that is compatible with the BOOT UPDATER.
        #
        # This function determines if the inverter will need a preliminary upgrade.
        self.preliminary_upgrade_required = False

        # How is this inverter doing? Is it in communication?
        if self.is_get_query_by_tlv():
            # Got a response to TLV query. Does it have the latest boot loader?
            if self.boot_loader_version_short == self.latest_boot_version_short:
                # This inverter is good to go with standard firmware upgrade.
                pass
            else:
                # TLV was returned, but boot version not at the latest version. It needs a preliminary upgrade.
                # Really old TLV-capable application code does not return the boot loader version, but may be
                # available in the XET INFO string. It is sufficient to know that it wasn't in the TLV list,
                # so the XET INFO isn't bothered with. If the inverter is running realy old code, the boot loader
                # version is not returned in the TLV query response. It might be available in the XET string,
                # if present.
                if self.boot_loader_version_short == 0:
                    (meh_running_mode, self.boot_loader_version_short, meh_firmware_version) = gu.get_inverter_data_from_xet_info_string(self.xet_info_string)
                self.log_info("IP %s: (TLV) NEEDS BOOT LOADER UPGRADE FROM R%s" % (self.ip_to_upgrade, self.boot_loader_version_short))
                self.preliminary_upgrade_required = True
        else:
            # Failure to query by TLV. Either it's a comm failure, or a non-TLV capable inverter. Query by NBNS.
            self.xet_info_string = gu.send_and_get_nbns_question(self.ip_to_upgrade, NBNS_XETINFO_QUESTION)
            (self.runmode, self.boot_loader_version_short, self.version_string) = gu.get_inverter_data_from_xet_info_string(self.xet_info_string)
            if self.runmode == RUNMODE_UNKNOWN:
                # No response to TLV or NBNS. Consider it failed one way or another.
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                self.log_info("IP %s: UPGRADE FAILED, NO COMMUNICATION WITH INVERTER" % (self.ip_to_upgrade))
                self.set_db_inverters_upgrade_status()
                return
            else:
                # XET INFO indicates something. Check to determine if it is TLV capable after all.
                if self.boot_loader_version_short == self.latest_boot_version_short:
                    # This is rare: inverter did not respond to a TLV query but responded to NBNS immediately
                    # afterwards. Someone won the lottery.
                    self.log_info("IP %s: INVERTER WITH R%s BOOT RESPONDED TO XET INFO BUT NOT TLV" % (self.ip_to_upgrade, self.boot_loader_version_short))
                else:
                    #
                    self.log_info("IP %s: (NBNS) NEEDS BOOT LOADER UPGRADE FROM R%s" % (self.ip_to_upgrade, self.boot_loader_version_short))
                    self.preliminary_upgrade_required = True
        return

    def verify_and_get_inverter_into_boot_mode(self):
        # --------------------------------------------------------------------------------------------------------------
        # The inverter has replied to query and is running with the latest boot version, therefore ready for standard
        # application upgrade. It may be in APP, LOADER or UPDATER mode. If it is in BOOT LOADER mode, then this is
        # likely a recovery from a previous failed upgrade attempt, but there are other legitimate for being in this
        # mode. If it is in BOOT UPDATER mode, then certainly this is an upgrade recovery from a previous failed
        # upgrade, in which case: apply/upgrade with the bootCombo file.
        #
        # Otherwise, the best effort is made to get this inverter into boot loader mode.
        self.runmode = RUNMODE_UNKNOWN

        consecutive_updater_upgrades = 0
        consecutive_tlv_upgrade_resets = 0
        while self.runmode != RUNMODE_SG424_BOOT_LOADER:

            # Query the inverter to get its running mode. The query function itself applies a short timeout
            # and a second attempt to re-query if the 1st one fails.
            if not self.is_get_query_by_tlv():
                # Hmmm ... failed to query. Sleep a bit in case it's intermittent comm interruption due to
                # other inverters upgrading, then query 1 more time before giving up.
                time.sleep(15)
                if not self.is_get_query_by_tlv():
                    self.runmode = RUNMODE_UNKNOWN
                    # Enough effort spent querying this inverter. This upgrade is failed.
                    self.log_info("IP %s: MAX ATTEMPTS REACHED TO QUERY INVERTER" % (self.ip_to_upgrade))
                    break

            if self.runmode == RUNMODE_SG424_APPLICATION:
                # With the inverter in APP mode, send an UPGRADE RESET command by TLV, sleep a bit to give the inverter
                # time to reset, come up in boot mode and get an IP.
                if consecutive_tlv_upgrade_resets >= SEND_TO_BOOT_MODE_MAX_ATTEMPTS:
                    # Max attempts reached to get this inverter into boot mode. Generally not observed, except in the
                    # rare circumstance where the inverter only has an APP but no boot loader. Such an inverter would
                    # be a development board, presumably un-potted and therefore recoverable with a puck.
                    self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_PERSISTENT_APP_MODE_AFTER_URESET)
                    self.log_info("IP %s: PERSISTENT APP MODE AFTER %s TLV URESET ATTEMPTS" % (self.ip_to_upgrade, SEND_TO_BOOT_MODE_MAX_ATTEMPTS))
                    break
                # Send this inverter a reset-upgrade command by TL
                consecutive_tlv_upgrade_resets += 1
                if not gu.is_send_upgrade_reset_by_tlv(self.ip_to_upgrade):
                    # Never observed. Under normal circumstances. Except when the impossible happens.
                    self.log_info("IP %s: FAILED TO SEND TLV UPGRADE RESET[3]" % (self.ip_to_upgrade))
                time.sleep(30)

            elif self.runmode == RUNMODE_SG424_BOOT_LOADER:
                # What was wanted all along: inverter in boot loader mode, ready for application upgrade.
                break

            elif self.runmode == RUNMODE_SG424_BOOT_UPDATER:
                # With the inverter in boot updater mode, upgrade with the bootCombo to apply the latest
                # required boot loader version. This is likely a recovery effort of a previously failed
                # upgrade while the updater was in the midst of upgrading/replacing the boot loader.
                # The contents of the boot loader within the inverter cannot be made known, so it is
                # upgraded
                if consecutive_updater_upgrades == 0:
                    self.log_info("IP %s: UPGRADE FROM UPDATER->LOADER" % (self.ip_to_upgrade))
                    if self.upgrade_boot_loader_from_updater():
                        # Increment this counter to avoid infinite UPDATER<->LOADER upgrade. This is known to
                        # happen if the boot loader itself is in the short 20-sec wait before jumping to APP.
                        consecutive_updater_upgrades += 1
                    else:
                        # Failed. Retain the runmode, and break out of the while. This process will bail.
                        break
                else:
                    # Already passed here - not expected under normal circumstances. The best explanation is that
                    # the boot loader is in the short 20-second delay mode before jumping to application, in this
                    # case: jumping back to boot updater. This is a rare circumstance, worthy of a lottery winning.
                    # This inverter will need manual intervention to upgrade it to normal/standard application.
                    # An alarm is raised to alert someone that this inverter needs special attention.
                    self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_UPGRADE_FAILURE_INTERVENTION_REQUIRED)
                    self.log_info("%s: UPG FAILED (CONSEC UPDATER ATTEMPTS, LOADER PRESUMED IN 20SEC MODE" % (self.ip_to_upgrade))
                    self.log_info("%s: MANUAL INTERVENTION REQUIRED FOR UPGRADE RECOVERY" % (self.ip_to_upgrade))
                    break

            else:
                # Any other run mode: failure to re-query. Most likely an inverter in APP mode was commanded to reset,
                # but there was no reply to a subsequent query. Or the inverter was in UPDATER mode, and after a
                # successful transfer of the bootCombo, the inverter is no longer responding. Consider this upgrade as
                # failed. If if the inverter was assigned a different IP coming out of reset, the upgrade will have to
                # be done all over again. This is not an expected condition.
                pass

        # ... end of while
        return

    def upgrade_inverter_from_latest_boot_loader(self):
        self.get_runmode_attempts = 0

        # Change the status to "in progress".
        set_upgrade_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                       INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                       INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_upgrade_args = (INVERTERS_UPGRADE_STATUS_IN_PROGRESS, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

        # Proceed with the actual upgrade.
        self.upgrade_file_for_tftp_process = self.inverter_upgrade_file
        tftp_xfer_result = self.transfer_hex_file()

        if tftp_xfer_result == TFTP_XFER_RESULT_SUCCESSFUL:
            # Upgrade succeeded, interrogate the inverter to gets its current runmode, expecting it to be in
            # application mode. Sleep a bit to give the inverter time to complete the (automatic) reset after
            # upgrade and presumably come up in APP mode and to get an ip.
            time.sleep(30)

            self.runmode = RUNMODE_UNKNOWN
            if not self.is_get_query_by_tlv():
                # Communication is interrupted? Sleep a wee bit and try just 1 more time.
                time.sleep(15)
                if not self.is_get_query_by_tlv():
                    # Too bad, the transfer was seemingly good, but subsequent query of the inverter failed.
                    # This is known to happen in poor ethernet networks. Consider the upgrade as failed.
                    self.log_info("IP %s: XFER SUCCESS BUT FAILED TO QUERY EXPECTED APP MODE" % (self.ip_to_upgrade))
                    self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                    self.upgrade_result = False

            if self.runmode == RUNMODE_SG424_APPLICATION:
                # Expected state. So nothing else to do here except declare success.
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_SUCCESS

            elif self.runmode == RUNMODE_SG424_BOOT_UPDATER:
                # A valid runmode but not expected. Log it. Consider the upgrade as failed.
                self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_SUSPICIOUS_UPGRADE_FILE)
                self.log_info("IP %s: EXPECTED APP MODE NOT UPDATER - SUSPICIOUS UPGRADE FILE %s" % (self.ip_to_upgrade, self.inverter_upgrade_file))
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                self.upgrade_result = False

            elif self.runmode == RUNMODE_SG424_BOOT_LOADER:
                # Also valid but not expected. Maybe the upgrade file itself was bad. Consider the upgrade as failed.
                self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_SUSPICIOUS_UPGRADE_FILE)
                self.log_info("IP %s: EXPECTED APP MODE NOT LOADER - SUSPICIOUS UPGRADE FILE %s" % (self.ip_to_upgrade, self.inverter_upgrade_file))
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                self.upgrade_result = False

            else:
                # Transfer was good. The query was good. The runmode returned was none of the above.
                # Data corruption? Or inverter returnes a new value not coded in the gateway? Consider
                # the upgrade as failed.
                self.raise_and_clear_alarm(ALARM_ID_UPG_ONE_INVAL_RUNMODE_IN_QUERY_RESPONSE)
                self.log_info("IP %s: TLV QUERY REPLY WITH UNKNOWN INVERTER MODE" % (self.ip_to_upgrade))
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                self.upgrade_result = False

        else:
            # Transfer failed, info already written to the log. Nothing to do here.
            pass

        return

    def upgrade_one_inverter(self):

        # -> the main entry point to upgrading an SG424 inverter.

        # --------------------------------------------------------------------------------------------------------------
        # 1: first get the IP address based on the inverters_id.
        (self.ip_to_upgrade, string_position) = gu.db_get_ip_and_string_position_from_inverter_id(self.inverters_id)

        # --------------------------------------------------------------------------------------------------------------
        # 2: Validate the IP.
        if (not self.ip_to_upgrade) or (self.ip_to_upgrade == ZERO_IP_ADDRESS):
            # IP address from the database is NULL.
            self.log_info("DB inverters_id %s: INVAL IP" % (self.inverters_id))
            self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_NULL_IP
            self.set_db_inverters_upgrade_status()
            return

        # --------------------------------------------------------------------------------------------------------------
        # 3: check if upgrade was canceled.
        if self.is_upgrade_cancelled():
            # Upgrade cancelled by the user. Write to the log, set the upgrade status to cancelled with 0%.
            self.log_info("IP %s: UPGRADE CANCELLED" % (self.ip_to_upgrade))
            update_string = "UPDATE %s SET %s=%s, %s=%s where %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                         INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                         INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                         INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
            update_args = (INVERTERS_UPGRADE_STATUS_COMPLETE_CANCELLED, 0, self.ip_to_upgrade)
            prepared_act_on_database(EXECUTE, update_string, update_args)
            return

        # --------------------------------------------------------------------------------------------------------------
        # 4: as part of the process to upgrading this inverter: set the upgrade_status to
        #    "in_progress" and the upgrade_progress to 0.
        set_upgrade_string = "UPDATE %s SET %s = %s, %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                                INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                                INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s",
                                                                                INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
        set_upgrade_args = (INVERTERS_UPGRADE_STATUS_IN_PROGRESS, 0, self.ip_to_upgrade)
        prepared_act_on_database(EXECUTE, set_upgrade_string, set_upgrade_args)

        # --------------------------------------------------------------------------------------------------------------
        # 5: based on string position, apply a start delay. The closer to the head-of-string the inverter is,
        #    the longer the start delay is. This ensures that inverters at the end of the string have a better
        #    chance to start their upgrade process, which reduces the number of re-transmits when all inverters
        #    complete the upgrade process. However, for the time it takes to perform a transfer, inverters closer
        #    to the HOS will usually complete first, which will nevertheless interfere somewhat with downstream
        #    inverters. That's life.
        if string_position > 0 and string_position <= 8:
            start_delay = STRING_POSITION_DELAY[string_position - 1]
            time.sleep(start_delay)
        else:
            # Not in a valid string position, it needs to be run. So no delay.
            pass

        # --------------------------------------------------------------------------------------------------------------
        # 6: ready to proceed with actual upgrade activity.
        self.inverter_upgrade_file_size = os.path.getsize(self.inverter_upgrade_file)
        self.total_blocks_estimated = self.inverter_upgrade_file_size / 512
        self.preliminary_upgrade_required = False
        self.runmode = RUNMODE_UNKNOWN

        # --------------------------------------------------------------------------------------------------------------
        # 7: Preamble: will this inverter require a preliminary upgrade from old code/old loader? Check for readiness.
        self.verify_upgrade_readiness()

        # --------------------------------------------------------------------------------------------------------------
        # 8: Based on readiness check, will a preliminary upgrade required?
        if self.preliminary_upgrade_required == True:
            # Then perform this preliminary upgrade. If it is successful, then the inverter will be ready
            # for standard upgrade.
            if not self.perform_full_preliminary_upgrade():
                self.log_info("IP %s: FAILED TO COMPLETE PRELIMINARY UPGRADE" % (self.ip_to_upgrade))
                self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED
                self.set_db_inverters_upgrade_status()
                return

        # --------------------------------------------------------------------------------------------------------------
        # 9: the inverter is running with the latest required boot version. Get it into boot loader mode.
        if self.runmode != RUNMODE_UNKNOWN:
            self.verify_and_get_inverter_into_boot_mode()

        # --------------------------------------------------------------------------------------------------------------
        # 10. get here when the inverter in question has met all the conditions for simple application code upgrade.
        if self.runmode == RUNMODE_SG424_BOOT_LOADER:
            self.upgrade_inverter_from_latest_boot_loader()
        else:
            # Several reasons for coming here: inverter is simply not communicating, or failed to get the inverter
            # into boot loader mode, or responses to initial queries were good, but subsequently, communication failed.
            self.final_upgrade_status = INVERTERS_UPGRADE_STATUS_COMPLETE_FAILED

        # Write the final result to the database.
        self.set_db_inverters_upgrade_status()

        # There is no logging handler to remove, as it is setup by the calling routine.

        # There's nothing left to do. So do it.
