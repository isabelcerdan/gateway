from zmq.backend.cython.utils import ZMQError
import zmq

from lib.logging_setup import *

MAX_ZMQ_ERRORS = 10


class ZmqError(Exception):
    """
    Exception raised when ZMQ error maximum is exceeded.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class ZmqTimeout(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)

    
class ZmqComm(object):    
    
    logger = setup_logger('ZMQComm')
    
    def __init__(self, port=None, polling_freq_sec=None):
        """
        @constructor
        """
        self._zctx = None
        self._server = None
        self._poller = None
        self._port = port
        self._zmq_errors = 0
        self._zsock = None
        self._polling_freq_sec = polling_freq_sec
        try:
            self._create_zmq_context()
        except ZMQError:
            raise ZmqError("Failed to initialize ZMQComm")
        
        try:
            self._create_zmq_objects()
        except ZMQError:
            raise ZmqError("Failed to initialize ZMQComm")
        
    def _create_zmq_context(self):
        while True:
            try:
                self._zctx = zmq.Context()
            except ZMQError as error:
                self._zmq_errors += 1
                ZmqComm.logger.exception('Failed to instantiate ZMQ context object - %s' % error)
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')
            else:
                return True
            
    def _delete_zmq_context(self):
        if self._zctx is not None:
            self._zctx.close()
            self._zctx.term()
    
    def _create_zmq_objects(self):
        while True:
            try:
                self._server = self._zctx.socket(zmq.REP)
            except ZMQError as error:
                ZmqComm.logger.exception('Failed to create ZMQ socket - %s' % error)
                self._zmq_errors += 1
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')

            try:
                self._server.bind("tcp://*:%d" % self._port)
            except ZMQError as error:
                zerr = repr(error)
                ZmqComm.logger.exception("Cannot bind to port %s: %s" % (self._port, zerr))
                self._server.setsockopt(zmq.LINGER, 0)
                self._server.close()
                ZmqComm.logger.exception('Failed to create ZMQ socket - %s' % error)
                self._zmq_errors += 1
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')

            try:
                self._poller = zmq.Poller()
            except ZMQError:
                self._server.setsockopt(zmq.LINGER, 0)
                self._server.close()
                ZmqComm.logger.exception('Failed to create ZMQ poller object.')
                self._zmq_errors += 1
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')

            try:
                self._poller.register(self._server, zmq.POLLIN)
            except ZMQError:
                self._server.setsockopt(zmq.LINGER, 0)
                self._server.close()
                self._poller.unregister(self._server)
                ZmqComm.logger.exception('Failed to register ZMQ poller')
                self._zmq_errors += 1
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')
            break

    def _listen(self):
        while True:
            try:
                self._zsock = dict(self._poller.poll(self._polling_freq_sec * 1000))
            except ZMQError as error:
                str_err = repr(error)
                ZmqComm.logger.exception(str_err)
                self._zmq_errors += 1
                if self._zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Maximum ZMQ errors exceeded')
                try:
                    self._delete_zmq_objects()
                except ZmqError:
                    raise
                try:
                    self._create_zmq_objects()
                except ZmqError:
                    raise
            else:
                return True
            
    def _delete_zmq_objects(self):
        ZmqComm.logger.debug('Deleting ZMQ objects...')
        if self._server is not None:
            self._server.setsockopt(zmq.LINGER, 0)
            self._server.close()
        if self._poller is not None:
            self._poller.unregister(self._server)

    def recv_msg(self):
        try:
            self._listen()
        except ZmqError:
            raise

        if self._server in self._zsock and self._zsock[self._server] == zmq.POLLIN:
            request = self._server.recv()
            command, parameter = request.split("|")
            return command, parameter
        else:
            raise ZmqTimeout('Receive message timeout')

    def recv_multipart_msg(self):
        try:
            self._listen()
        except ZmqError:
            raise

        if self._server in self._zsock and self._zsock[self._server] == zmq.POLLIN:
            [address, contents] = self._server.recv_multipart()
            return [address, contents]
        else:
            raise ZmqTimeout('Receive message timeout')

    def send_msg(self, msg=None):
        if msg is None:
            ZmqComm.logger.warning('send_msg called with no parameter')
            return False
        try:
            self._server.send(msg)
        except ZMQError:
            raise ZmqError('Failed to send message')
        return True
    
    def send_multipart_msg(self, topic=None, msg=None):
        if msg is None or topic is None:
            ZmqComm.logger.warning('send_multipart_msg called with moissing parameter')
            return False
        try:
            self._server.send_multipart(topic, msg)
        except ZMQError:
            raise ZmqError('Failed to send multipart message')
        return True
    
    def close(self):
        """
        @destructor
        """
        self._delete_zmq_objects()
        self._delete_zmq_context()
