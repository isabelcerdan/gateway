import gateway_utils as gu
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *
from lib import logging_setup
from collections import namedtuple
import struct

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_SOFTWARE_PROFILE_REGION_NAME_MISMATCH, \
                                  ALARM_ID_TLV_UTILS_DB_GROUP_ID_NULL_ROW


def dont_unpack(udp_packet, next_rank):
    # THIS WILL BREAK THE TLV READING if it is ever called.
    return None, next_rank

def dont_pack(packet, tlv_id, value):
    return packet


def get_tlv_data_6hex_bytes(packed_data, tlv_rank):
    length, new_rank = gu.get_tlv_header_length(packed_data, tlv_rank)

    if length != 6:
        return None, None

    try:
        hex_pair1, hex_pair2, hex_pair3, new_rank = gu.get_funky_packed_mac(packed_data, new_rank)
        addr = '%04x%04x%04x' % (hex_pair1, hex_pair2, hex_pair3)
        mac_address = ':'.join([addr[i:i+2] for i in range(0, len(addr), 2)])
        return mac_address, new_rank
    except Exception:
        return None, None



class TLVPacker:

    # All defined GTW_TO_SG424 TLVs should be included in this tuple. If any are missing, the risk is a pack exception.
    HOW_TO_PACK = {
                                                                          # TODO: get rid of these trailing numbers, they're just in the way.
        GTW_TO_SG424_MASTER_U16_TLV_TYPE : dont_pack,                                                                            # 101
        GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                         # 102
        GTW_TO_SG424_REGULATION_U16_TLV_TYPE: gu.append_user_tlv_16bit_unsigned_short,                                           # 103
        GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE : gu.append_user_tlv_32bit_unsigned_long,                           # 104
        DEPRECATED_GTW_TO_SG424_1_TLV_TYPE : dont_pack,                                                                          # 105
        DEPRECATED_GTW_TO_SG424_2_TLV_TYPE : dont_pack,                                                                          # 106
        GTW_TO_SG424_CONFIG_PCC_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                          # 107
        GTW_TO_SG424_CONFIG_FEED_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                         # 108
        DEPRECATED_GTW_TO_SG424_3_TLV_TYPE : dont_pack,                                                                          # 109
        GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                               # 110
        GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                      # 111
        GTW_TO_SG424_CLI_COMMAND_STRING_TLV_TYPE : gu.append_user_tlv_string,                                                    # 112
        GTW_TO_SG424_HOS_QUERY_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                           # 113
        GTW_TO_SG424_DEBUG_COUNTERS_QUERY_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                # 114
        GTW_TO_SG424_FD_ENTER_STBY_STATE_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                 # 115
        GTW_TO_SG424_FD_ENTER_DETECT_STATE_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                               # 116
        GTW_TO_SG424_FD_LEAVE_DETECT_STATE_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                               # 117
        GTW_TO_SG424_FD_EXIT_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                             # 118
        GTW_TO_SG424_FD_MODE_REMINDER_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                    # 119
        DEPRECATED_GTW_TO_SG424_4_TLV_TYPE : dont_pack,                                                                          # 120
        GTW_TO_SG424_FD_QUERY_STATE_TLV_TYPE_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                             # 121
        GTW_TO_SG424_INIT_DEBUG_COUNTERS_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                 # 122
        GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                           # 123
        GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE : gu.append_user_tlv_string,                                            # 124
        GTW_TO_SG424_UPGRADE_RESET_BY_MAC_STRING_TLV_TYPE : gu.append_user_tlv_string,                                           # 125
        GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE : gu.append_user_tlv_string,                                             # 126
        GTW_TO_SG424_UPGRADE_RESET_BY_SN_STRING_TLV_TYPE : gu.append_user_tlv_string,                                            # 127
        GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,          # 128
        DEPRECATED_GTW_TO_SG424_5_TLV_TYPE : dont_pack,                                                                          # 129
        GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,       # 130
        DEPRECATED_GTW_TO_SG424_6_TLV_TYPE : dont_pack,                                                                          # 131
        GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,     # 132
        GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,      # 133
        DEPRECATED_GTW_TO_SG424_7_TLV_TYPE : dont_pack,                                                                          # 134
        GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,         # 135
        GTW_TO_SG424_TEST_KEEP_ALIVE_U32_TLV_TYPE : gu.append_user_tlv_32bit_unsigned_long,                                      # 136
        GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                # 137
        GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_RAM_ONLY_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,              # 138
        GTW_TO_SG424_GET_DC_SM_OUTPUT_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                    # 139
        GTW_TO_SG424_GET_AIF_IMMED_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                            # 140
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_ALL_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                                # 141
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_ALL_LEADING_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,               # 142
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                                  # 143
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_LEADING_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                 # 144
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                                  # 145
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_LEADING_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                 # 146
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                                  # 147
        GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_LEADING_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                 # 148
        GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                   # 149
        GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                      # 150
        GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                     # 151
        GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                 # 152
        GTW_TO_SG424_UPDATE_INV_CTRL_SLEEP_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                               # 153
        DEPRECATED_GTW_TO_SG424_8_TLV_TYPE : dont_pack,                                                                          # 154
        GTW_TO_SG424_UPDATE_INV_CTRL_LOW_POWER_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                           # 155
        GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                     # 156
        GTW_TO_SG424_CLEAR_ALL_INV_CTRL_BITS_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                             # 157
        GTW_TO_SG424_QUERY_BY_MAC_6HEX_BYTES_TLV_TYPE : gu.append_user_hex_numbers,                                              # 158
        GTW_TO_SG424_QUERY_BY_SERIAL_NUMBER_STRING_TLV_TYPE : gu.append_user_tlv_string,                                         # 159
        GTW_TO_SG424_GET_AIF_SETTINGS_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                         # 160
        GTW_TO_SG424_GET_AIF_VOLTAGE_RTS_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                      # 161
        GTW_TO_SG424_GET_AIF_FREQUENCY_RTS_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                    # 162
        GTW_TO_SG424_GET_AIF_VOLT_WATT_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                        # 163
        GTW_TO_SG424_GET_AIF_VOLT_VAR_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                         # 164
        GTW_TO_SG424_GET_AIF_FREQUENCY_WATT_GROUP_DATA_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                   # 165
        GTW_TO_SG424_AIF_SETTINGS_AC_VOLTAGE_OFFSET_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                     # 166
        GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 167
        GTW_TO_SG424_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 168
        GTW_TO_SG424_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_IN_EEPROM_U32_TLV_TYPE : gu.append_user_tlv_32bit_unsigned_long,  # 169
        GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                     # 170
        GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 171
        GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 172
        GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 173
        GTW_TO_SG424_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,              # 174
        GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                # 175
        GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,      # 176
        GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,                  # 177
        GTW_TO_SG424_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_IN_EEPROM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,       # 178
        GTW_TO_SG424_GET_MOM_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                             # 179
        DEPRECATED_GTW_TO_SG424_9_TLV_TYPE : dont_pack,                                                                          # 180
        DEPRECATED_GTW_TO_SG424_10_TLV_TYPE: dont_pack,                                                                          # 181
        GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,             # 182
        GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,               # 183
        GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE : gu.append_user_tlv_32bit_float,             # 184
        GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE : gu.append_user_tlv_string,                                                     # 185
        DEPRECATED_GTW_TO_SG424_11_TLV_TYPE: dont_pack,                                                                          # 186
        GTW_TO_SG424_PCS_UNIT_ID_CONFIG_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                  # 187
        GTW_TO_SG424_GET_DC_GWA_SM_OUTPUT_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                # 188
        GTW_TO_SG424_GET_CATCH_UP_REPORTS_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                # 189
        DEPRECATED_GTW_TO_SG424_12_TLV_TYPE: dont_pack,                                                                          # 190
        DEPRECATED_GTW_TO_SG424_13_TLV_TYPE: dont_pack,                                                                          # 191
        GTW_TO_SG424_GET_EERPT_SERVER_CONFIG_DATA_TLV_TYPE :  gu.append_user_tlv_16bit_unsigned_short,                           # 192
        DEPRECATED_GTW_TO_SG424_14_TLV_TYPE: dont_pack,                                                                          # 193
        DEPRECATED_GTW_TO_SG424_15_TLV_TYPE: dont_pack,                                                                          # 194
        DEPRECATED_GTW_TO_SG424_16_TLV_TYPE: dont_pack,                                                                          # 195
        DEPRECATED_GTW_TO_SG424_17_TLV_TYPE: dont_pack,                                                                          # 196
        DEPRECATED_GTW_TO_SG424_18_TLV_TYPE: dont_pack,                                                                          # 197
        DEPRECATED_GTW_TO_SG424_19_TLV_TYPE: dont_pack,                                                                          # 198
        GTW_TO_SG424_EERPT_SERVER_ENABLE_U16_TLV_TYPE :  gu.append_user_tlv_16bit_unsigned_short,                                # 199
        GTW_TO_SG424_EERPT_SERVER_RATE_U16_TLV_TYPE :  gu.append_user_tlv_16bit_unsigned_short,                                  # 200
        GTW_TO_SG424_EERPT_SERVER_URL_STRING_TLV_TYPE :  gu.append_user_tlv_16bit_unsigned_short,                                # 201
        GTW_TO_SG424_EERPT_SERVER_PORT_U16_TLV_TYPE :  gu.append_user_tlv_16bit_unsigned_short,                                  # 202
        GTW_TO_SG424_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE: gu.append_user_tlv_string,                                          # 203
        GTW_TO_SG424_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE: gu.append_user_tlv_string,                                           # 204
        GTW_TO_SG424_PSA_POWER_STATUS_U16_TLV_TYPE: gu.append_user_tlv_16bit_unsigned_short,                                     # 205
        GTW_TO_SG424_PSA_POWER_ALL_STATUS_U16_TLV_TYPE: gu.append_user_tlv_16bit_unsigned_short,                                 # 206
        GTW_TO_SG424_XET_INFO_TLV_TYPE : dont_pack,                                                                              # 207
        GTW_TO_SG424_XET_AIF_VRT_TLV_TYPE : dont_pack,                                                                           # 208
        GTW_TO_SG424_XET_AIF_FRT_TLV_TYPE : dont_pack,                                                                           # 209
        GTW_TO_SG424_XET_AIF_VOLT_WATT_TLV_TYPE : dont_pack,                                                                     # 210
        GTW_TO_SG424_XET_AIF_VOLT_VAR_TLV_TYPE : dont_pack,                                                                      # 211
        GTW_TO_SG424_XET_AIF_FREQ_WATT_TLV_TYPE : dont_pack,                                                                     # 212
        GTW_TO_SG424_XET_AIF_CURVE_TLV_TYPE : dont_pack,                                                                         # 213
        GTW_TO_SG424_XET_AIF_STATUS_TLV_TYPE : dont_pack,                                                                        # 214
        GTW_TO_SG424_PACKED_COUNTERS_QUERY_U32_TLV_TYPE : gu.append_user_tlv_32bit_unsigned_long,                                # 215
        GTW_TO_SG424_REPORT_ACK_TLV_TYPE : dont_pack,                                                                            # 216
        GTW_TO_SG424_GROUP_ID_CONFIG_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                     # 217
        GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                 # 218
        GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE : gu.append_user_tlv_16bit_unsigned_short,                                  # 219

    }

    def __init__(self):
        self.outgoing_tlv_pairs = []

    def add(self, outgoing_tlv_id, outgoing_tlv_value):
        self.outgoing_tlv_pairs.append( (outgoing_tlv_id, outgoing_tlv_value) )

    def send_to(self, ip_as_string):
        finished_packet = self.pack()
        if finished_packet is None:
            pass
        elif gu.is_send_tlv_packed_data(ip_as_string, finished_packet):
            pass # try once
        elif gu.is_send_tlv_packed_data(ip_as_string, finished_packet):
            pass # try a 2nd time
        else: # if 2 tries failed, log it as an error.
            logger = logging_setup.setup_logger("TLVPacker")
            logger.error("Unable to send TLV packet to inverter at %s" % ip_as_string)

    def pack(self):
        number_of_tlvs = len(self.outgoing_tlv_pairs)

        if number_of_tlvs == 0:
            return None

        packet = gu.add_master_tlv(number_of_tlvs)
        for (tlv_id, value) in self.outgoing_tlv_pairs:
            append = TLVPacker.HOW_TO_PACK[tlv_id]
            packet = append(packet, tlv_id, value)

        return packet

    def __str__(self):
        text = "%d total TLVs: " % len(self.outgoing_tlv_pairs)
        for tlv_id, tlv_val in self.outgoing_tlv_pairs:
            text += "%d(%s) " % (tlv_id, str(tlv_val))
        return text

'''
class TLVUnpacker:
    # --- I AM UNTESTED --- *** BUT NOT FOR LONG, USAGE BY gateway_utils COMING SOON *** #

    def __init__(self, udp_packet):
        self.tlv_ids = []
        self.tlv_vals = []
        self.marker = 0
        starting_rank, self.length = gu.evaluate_master_tlv(udp_packet)
        if starting_rank != 0:
            self._parse(udp_packet, starting_rank)

    # As with the packer, the entries in the unpacker are ordered as they are defined in the
    # constants file, and their numeric ID is also included (starting from 1001).
    HOW_TO_UNPACK = {
                                                                     # TODO: get rid of these trailing numbers, they're just in the way.
        SG424_TO_GTW_MASTER_U16_TLV_TYPE : dont_unpack,                                                                          # 1001
        DEPRECATED_SG424_TO_GTW_1_TLV_TYPE : dont_unpack,                                                                        # 1002
        DEPRECATED_SG424_TO_GTW_9_TLV_TYPE : dont_unpack,                                                                        # 1003
        SG424_TO_GTW_VERSION_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                           # 1004
        SG424_TO_GTW_PRODUCT_PART_NUMBER_STRING_TLV_TYPE : gu.get_tlv_data_string,                                               # 1005
        SG424_TO_GTW_SERIAL_NUMBER_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                     # 1006
        SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                       # 1007
        SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                             # 1008
        SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                            # 1009
        SG424_TO_GTW_RUNMODE_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                                # 1010
        SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                      # 1011
        SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                          # 1012
        SG424_TO_GTW_CATCH_UP_REPORTS_ACTIVE_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                # 1013
        SG424_TO_GTW_CATCH_UP_REPORTS_BOTH_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                  # 1014
        SG424_TO_GTW_CATCH_UP_REPORTS_INFO_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                  # 1015
        DEPRECATED_SG424_TO_GTW_2_TLV_TYPE : dont_unpack,                                                                        # 1016
        SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE : get_tlv_data_6hex_bytes,                                                 # 1017
        DEPRECATED_SG424_TO_GTW_3_TLV_TYPE : dont_unpack,                                                                        # 1018
        SG424_TO_GTW_DEBUG_COUNTERS_U32_ARRAY_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                    # 1019
        SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                       # 1020
        DEPRECATED_SG424_TO_GTW_4_TLV_TYPE : dont_unpack,                                                                        # 1021
        SG424_TO_GTW_FD_STATE_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                               # 1022
        SG424_TO_GTW_SW_PROFILE_U16_TLV_TYPE: gu.get_tlv_data_16bit_unsigned_short,                                              # 1023
        SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                               # 1024
        DEPRECATED_SG424_TO_GTW_5_TLV_TYPE : dont_unpack,                                                                        # 1025
        DEPRECATED_SG424_TO_GTW_6_TLV_TYPE : dont_unpack,                                                                        # 1026
        SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                      # 1027
        SG424_TO_GTW_UPTIME_SECS_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                             # 1028
        DEPRECATED_SG424_TO_GTW_7_TLV_TYPE : dont_unpack,                                                                        # 1029
        SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_SET_RESULT_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                      # 1030
        SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_CLEAR_RESULT_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                    # 1031
        SG424_TO_GTW_ENERGY_REPORT_CONTROL_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                  # 1032
        SG424_TO_GTW_DC_SM_OUTPUT_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                      # 1033
        DEPRECATED_SG424_TO_GTW_8_TLV_TYPE : dont_unpack,                                                                        # 1034
        SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,           # 1035
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                    # 1036
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,        # 1037
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                       # 1038
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,      # 1039
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,       # 1040
        SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                      # 1041
        SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,          # 1042
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                              # 1043
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                                       # 1044
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                                        # 1045
        DEPRECATED_SG424_TO_GTW_8_TLV_TYPE : dont_unpack,                                                                        # 1046
        DEPRECATED_SG424_TO_GTW_8_TLV_TYPE : dont_unpack,                                                                        # 1047
        SG424_TO_GTW_KEEP_ALIVE_GONE_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                         # 1048
        SG424_TO_GTW_KEEP_ALIVE_BACK_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                         # 1049
        SG424_TO_GTW_KEEP_ALIVE_LATE_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                         # 1050
        SG424_TO_GTW_SEQUENCE_NUMBER_REPEAT_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                  # 1051
        SG424_TO_GTW_SEQUENCE_NUMBER_OOS_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                     # 1052
        SG424_TO_GTW_AIF_SETTINGS_AC_VOLTAGE_OFFSET_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                      # 1053
        SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                # 1054
        SG424_TO_GTW_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                # 1055
        SG424_TO_GTW_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_FROM_EEPROM_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,   # 1056
        SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                      # 1057
        SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                # 1058
        SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                # 1059
        SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                # 1060
        SG424_TO_GTW_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,               # 1061
        SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                 # 1062
        SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,       # 1063
        SG424_TO_GTW_AIF_SETTINGS_POWER_WAVERING_WATTS_FROM_EEPROM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                   # 1064
        SG424_TO_GTW_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_FROM_EEPROM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,        # 1065
        SG424_TO_GTW_CURTAILMENT_PACKETS_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                     # 1066
        SG424_TO_GTW_REGULATION_PACKETS_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                      # 1067
        SG424_TO_GTW_BATTERY_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                                # 1068
        SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                              # 1069
        SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                            # 1070
        SG424_TO_GTW_MOM_ALL_BITS_U32_TLV_TYPE : gu.get_tlv_data_32bit_unsigned_long,                                            # 1071
        SG424_TO_GTW_MOM_STEP_SIZE_HI_WM_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                    # 1072
        SG424_TO_GTW_MOM_VREF_SETTING_HI_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                   # 1073
        SG424_TO_GTW_MOM_VREF_SETTING_LO_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                   # 1074
        SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_HI_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                              # 1075
        SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_LO_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                              # 1076
        SG424_TO_GTW_MOM_PHASE_ANGLE_360_ACTUAL_INUSE_INDEGREES_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,               # 1077
        SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_HI_WM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                          # 1078
        SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_LO_WM_F32_TLV_TYPE : gu.get_tlv_data_32bit_float,                          # 1079
        SG424_TO_GTW_MOM_TS_PHASE_ERROR_HI_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                 # 1080
        SG424_TO_GTW_MOM_TS_PHASE_ERROR_LO_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                 # 1081
        SG424_TO_GTW_MOM_PLINE_WRMS_HI_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                     # 1082
        SG424_TO_GTW_MOM_PLINE_WRMS_LO_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                                     # 1083
        SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_HI_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                  # 1084
        SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_LO_WM_I16_TLV_TYPE : gu.get_tlv_data_16bit_signed_short,                  # 1085
        SG424_TO_GTW_MOM_FAULT_NUMBER_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                       # 1086
        SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                # 1087
        SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_FROM_EEPROM_F32_TLV_TYPE: gu.get_tlv_data_32bit_float,                 # 1088
        SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_FROM_EEPROM_F32_TLV_TYPE: gu.get_tlv_data_32bit_float,               # 1089
        SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE : gu.get_tlv_data_string,                                                        # 1090
        DEPRECATED_SG424_TO_GTW_10_TLV_TYPE: dont_unpack,                                                                        # 1091
        SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE : gu.get_tlv_data_16bit_unsigned_short,                                            # 1092
        SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE  : gu.get_tlv_data_string,                                                 # 1093

    }

    def _parse(self, udp_packet, starting_rank):
        next_rank = starting_rank
        for i in range(0, self.length):
            next_type, next_rank = gu.get_tlv_type(udp_packet, next_rank)
            if next_rank == 0:
                break
            else:
                unpacking_method = self.HOW_TO_UNPACK[next_type]
                tlv_value, next_rank = unpacking_method(udp_packet, next_rank)
                if tlv_value != None:
                    self.tlv_ids.append(next_type)
                    self.tlv_vals.append(tlv_value)
            # finished unpacking this tlv. continue to next iteration.
        # end for loop. done unpacking the entire udp message.

    # -----------------------------------------------------------
    # Make this class iterable so that we can use a for-each loop
    # for (tlv_id, tlv_value) in TLVUnpacker:
    #     do something with the ids and values

    def __iter__(self):
        return self

    def next(self):
        if self.marker == self.length:
            raise StopIteration
        else:
            return self.get_next_pair()

    def get_next_pair(self):
        self.marker += 1
        return (self.tlv_ids[self.marker - 1], self.tlv_vals[self.marker - 1])

    # End of functions needed for iteration
    # -----------------------------------------------------------

    def get_tlv_ids_list(self):
        return self.tlv_ids

    def get_tlv_vals_list(self):
        return self.tlv_vals

    def get_ids_and_vals_lists(self):
        return (self.tlv_ids, self.tlv_vals)
'''

class TLVTranslator:

    """
    :author: class structure by Sydney Park with base functionality from Ed Proulx

    This class is used for processing a list of TLVs and generating output.

    INPUT
    As an input, it should take two arrays:
     - tlv_id_list  : this will name the types of TLVs that the TLVTranslator will process
     - tlv_val_list : this will name the values of the TLVs indicated by the ID list

    PROCESSING
    The information should have at the ready for each possible TLV type:
     - The name of the database column that corresponds to the TLV type (if the database stores it at all)
     - The way that the TLV should be parsed for DB storage (the inverters might send us a couple of different data types)
     - The way that the TLV should be displayed in the free-form text file
     - Whether we should update the database with any discrepancies, simply check for discrepancies,
            or simply report the values that the inverter gave us
     - Index for storage of each TLV type in this class's arrays. Order is used for uniform output and
            to identify which TLVs have not been reported
    The TLVTranslator will be in charge of storing the data that it extracts from the TLV lists.

    OUTPUT
    We want it to use the data it found to build output in multiple formats.
     - Free-form text output indicating the values read, and whether they matched the information in the database.
     - An excel-friendly line of text containing only the reported values (also, matching header string if needed)
     - A pair of lists indicating what should be updated in the database
            (one list for column names, one list for new values)
    """

    ''' CONSTANTS '''

    INDEXOF_RUNMODE_TLV_TYPE = 0
    INDEXOF_VERSION_TLV_TYPE = 1
    INDEXOF_BOOT_VERSION_TLV_TYPE = 2
    INDEXOF_PROD_PART_NO_TLV_TYPE = 3
    INDEXOF_SW_PROFILE_TLV_TYPE = 4
    INDEXOF_SERIAL_NO_TLV_TYPE = 5
    INDEXOF_MAC_ADDRESS_TLV_TYPE = 6
    INDEXOF_NUMBER_OF_RESETS_TLV_TYPE = 7
    INDEXOF_UPTIME_SECS_TLV_TYPE = 8
    INDEXOF_CONFIG_FEED_TLV_TYPE = 9
    INDEXOF_CONFIG_PCC_TLV_TYPE = 10
    INDEXOF_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_TLV_TYPE = 11
    INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE = 12
    INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE = 13
    INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE = 14
    INDEXOF_XET_INFO_TLV_TYPE = 15
    INDEXOF_INV_CTRL_TLV_TYPE = 16
    INDEXOF_KEEP_ALIVE_TIMEOUT_TLV_TYPE = 17
    INDEXOF_ENERGY_REPORT_CONTROL_TLV_TYPE = 18
    INDEXOF_DC_SM_INSTANTANEOUS_WATTS_TLV_TYPE = 19
    INDEXOF_DC_SM_INSTANTANEOUS_VOLTS_TLV_TYPE = 20
    INDEXOF_DC_SM_INSTANTANEOUS_AMPS_TLV_TYPE = 21
    INDEXOF_DC_GWA_SM_OUTPUT_TLV_TYPE = 22
    INDEXOF_CURTAILMENT_PACKETS_TLV_TYPE = 23
    INDEXOF_REGULATION_PACKETS_TLV_TYPE = 24
    INDEXOF_KEEP_ALIVE_GONE_TLV_TYPE = 25
    INDEXOF_KEEP_ALIVE_BACK_TLV_TYPE = 26
    INDEXOF_KEEP_ALIVE_LATE_TLV_TYPE = 27
    INDEXOF_SEQ_NUM_REPEAT_TLV_TYPE = 28
    INDEXOF_SEQ_NUM_OOS_TLV_TYPE = 29
    INDEXOF_UPSTREAM_LINK_TOGGLE_COUNT_TLV_TYPE = 30
    INDEXOF_DOWNSTREAM_LINK_TOGGLE_COUNT_TLV_TYPE = 31
    INDEXOF_BATTERY_TLV_TYPE = 32
    INDEXOF_FALLBACK_POWER_SETTING_TLV_TYPE = 33
    INDEXOF_NTP_URL_SG424_TLV_TYPE = 34
    INDEXOF_PCS_GROUP_ID_TLV_TYPE = 35
    INDEXOF_GROUP_ID_TLV_TYPE = 36
    INDEXOF_ER_SERVER_HTTP_TLV_TYPE = 37
    INDEXOF_ER_GTW_TLV_TLV_TYPE = 38

    # For maintenance purposes, ALWAYS keep unknown as the last type. Adjust its value if new TLVs are added.
    INDEXOF_UNKNOWN_TLV_TYPE = 39

    NUMBER_OF_TLV_TYPES = INDEXOF_UNKNOWN_TLV_TYPE   # this prevents the 'unknown' type from being output

    TLV_FORMAT_ASCII = 0
    TLV_FORMAT_BINARY = 1

    DEFAULT_COLUMN_NAME = ''
    DEFAULT_TLV_VALUE = "NOT_REPORTED"


    INCLUDE_VALUES_NOT_REPORTED = True
    OMIT_VALUES_NOT_REPORTED = False
    VERBOSE_OUTPUT = False

    TEXTOUT_NO_UPDATE_REQUIRED_AGREE = 0
    TEXTOUT_NO_UPDATE_REQUIRED_DISAGREE = 1
    TEXTOUT_UPDATE_INVERTER_REQUIRED = 2
    TEXTOUT_UPDATE_DATABASE_REQUIRED = 3
    TEXTOUT_NO_COMPARISON_PERFORMED = 4
    TEXTOUT_NOT_IN_DATABASE = 5
    TEXTOUT_GTW_CTRL_MULTICAST_OK = 6
    TEXTOUT_GTW_CTRL_MULTICAST_NOK = 7
    TEXTOUT_U_AIF_NOT_CONFIGURED = 8
    TEXTOUT_SW_PROF_NOT_IN_REGION_NAME = 9
    TEXTOUT_SW_PROF_NOT_IN_EXPECTED_REGION = 10
    TEXTOUT_FEED_NAME_NOT_CONFIGURED = 11

    ''' Conversion from TLV values to something that we can understand and work with '''

    @staticmethod
    def build_tlv_names_array():
        names = [''] * TLVTranslator.NUMBER_OF_TLV_TYPES

        # tlvinfo is a named tuple and we use the following fields:
        #  - index : the index in the array where we will store this TLV's info
        #  - db_col_name : the name of the DB col that stores this TLV's info, to be used as a title
        #  - text_heading : a human-understandable title for the name of this TLV

        for tlvinfo in TLVTranslator.lookup_table.values():
            # noinspection PyTypeChecker
            names[tlvinfo.index] = tlvinfo.text_heading
        # end for

        return names
    # end

    ''' Converting TLVs into something meaningful '''

    def convert_hex_to_str(self, value):
        return "0x" + str(hex(value)[2:].zfill(2))

    # No longer used - all floats now extracted as such.
    def convert_float_to_str(self, value):
        new_report_value = struct.unpack('f', struct.pack('I', value))[0]
        return round(new_report_value, 3)

    # TODO: is there a better way to round to 2 or to 3 decimals from the dictionary?
    def format_float_to_2_decimals(self, value):
        return round(value, 2)

    def format_float_to_3_decimals(self, value):
        return round(value, 3)

    def convert_string_to_lower(self, value):
        return str(value).lower()

    '''
    NO LONGER USED
    def custom_convert_runmode(self, value):
        return_value = INVERTERS_TABLE_STATUS_STRING_UNKNOWN
        if value == RUNMODE_SG424_APPLICATION:
            return_value = INVERTERS_TABLE_STATUS_STRING_APPLICATION
        elif value == RUNMODE_SG424_BOOT_LOADER:
            return_value = INVERTERS_TABLE_STATUS_STRING_BOOT_LOADER
        elif value == RUNMODE_SG424_BOOT_UPDATER:
            return_value = INVERTERS_TABLE_STATUS_STRING_BOOT_UPDATER
        return return_value
    '''

    def custom_convert_runmode_for_comm(self, value):
        return_value = INVERTERS_TABLE_COMM_STRING_UNKNOWN
        if value == RUNMODE_SG424_APPLICATION:
            return_value = INVERTERS_TABLE_COMM_STRING_SG424_APPLICATION
        elif value == RUNMODE_SG424_BOOT_LOADER:
            return_value = INVERTERS_TABLE_COMM_STRING_BOOT_LOADER
        elif value == RUNMODE_SG424_BOOT_UPDATER:
            return_value = INVERTERS_TABLE_COMM_STRING_BOOT_UPDATER
        return return_value

    def custom_convert_feed(self, value):
        return gu.get_feed_name_as_string(int(value))

    def use_raw_value(self, value):
        return value


    ''' Acting on the values '''

    def record_plain_value(self, plain_value, ordered_tlv_index):
        self.reported_markers[ordered_tlv_index] = True
        self.ordered_values_plain[ordered_tlv_index] = plain_value

    def handle_feed_with_inverter_update(self, value, incoming_tlv_id):
        # HANDLE THE FOLLOWING ODDITY: feed from the database is a varchar, feed from the inverter is an integer.
        # When received by the inverter, the feed-as-integer is converted to a varchar, and a comparison is made.
        # If the comparison fails, then: - the database feed-as-varchar is converted to feed-as-integer,
        #                                - the TLV is constructed with that integer and the inverter is updated.
        #
        # Whenever the FEED is updated in the inverter, the TLV packet needs the companion TLV PCC value (though
        # PCC is not stored in the database).
        #
        # Finally: on aggregate systems, FEED_NAME is setup as NULL. So the value returned from the inverter must
        #          equate to the "ALL" value.
        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]
        column_name = special_tuple.db_col_name

        if self.inverter_db_row[column_name] == "NULL":
            # FEED_NAME has not been configured.
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_FEED_NAME_NOT_CONFIGURED)
        else:
            agreed = self.consistency_check(column_name, value)
            if agreed:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
            elif column_name in self.inverter_db_row:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_UPDATE_INVERTER_REQUIRED)
                feed_as_integer = gu.get_feed_name_as_integer(self.inverter_db_row[column_name])
                self.queue_for_inverter_update(incoming_tlv_id, feed_as_integer)
                pcc_value = 1
                if feed_as_integer == 0:
                    pcc_value = 0
                self.queue_for_inverter_update(SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE, pcc_value)
            else:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NOT_IN_DATABASE)

    def handle_with_inverter_update(self, value, incoming_tlv_id):
        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]
        column_name = special_tuple.db_col_name

        agreed = self.consistency_check(column_name, value)
        if agreed:
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
        elif column_name in self.inverter_db_row:
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_UPDATE_INVERTER_REQUIRED)
            self.queue_for_inverter_update(incoming_tlv_id, self.inverter_db_row[column_name])
        else:
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NOT_IN_DATABASE)

    def custom_handler_group_data(self, value, incoming_tlv_id):
        # The inverter will have returned: fallback, NTP URL, irradiance, gateway control (as per INV_CTRL),
        # ER send-to-server/HTTP, ER send-to-gateway/TLV and AIF ID TL data. That data is based on groups,
        # so not stored on an inverter basis. Get this data from the database based on this inverter's
        # group ID and determine if the data is consistent.
        #
        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]
        column_name = special_tuple.db_col_name

        agreed = self.consistency_check(column_name, value)
        if agreed:
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
        elif column_name in self.inverter_db_row:
            # UPDATE THE DATABASE
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_UPDATE_DATABASE_REQUIRED)
            self.queue_for_db_update(INVERTERS_TABLE_GROUP_AUDIT_REQUIRED_COLUMN_NAME, 1)
        else:
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NOT_IN_DATABASE)

    def do_not_handle(self, value, incoming_tlv_id):
        pass

    '''

    UNIVERSAL AIF CONFIGURATION ...

    def handle_u_aif_with_inverter_update(self, value, incoming_tlv_id):
        # HANDLE THIS CONDITION: the values received from the inverter are not stored against the inverters table.
        #                        Instead, they are to be compared against those in the UNIVERSAL-AIF table. Any
        #                        discrepancies are updated in the inverter itself.
        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]
        column_name = special_tuple.db_col_name
        universal_aif_data_row = gu.get_universal_aif_db_row()
        agreed = False

        if universal_aif_data_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME] != REGION_NAME_UNDEFINED:
            # The U-AIF table has been configured for a specific region.
            if incoming_tlv_id == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE:
                if value == universal_aif_data_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME]:
                    agreed = True
            elif incoming_tlv_id == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE:
                if value == universal_aif_data_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME]:
                    agreed = True
            elif incoming_tlv_id == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE:
                if value == universal_aif_data_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME]:
                    agreed = True
            elif incoming_tlv_id == SG424_TO_GTW_REGION_NAME_STRING_TLV_TYPE:
                if value == universal_aif_data_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]:
                    agreed = True
            if agreed:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
            else:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_UPDATE_INVERTER_REQUIRED)
                self.queue_for_inverter_update(incoming_tlv_id, self.inverter_db_row[column_name])

        else:
            # Slap the user - U-AIF has not been configured.
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_U_AIF_NOT_CONFIGURED)

    '''

    def custom_handler_sw_profile(self, value, incoming_tlv_id):
        # HANDLE THIS CONDITION: the software profile received from the inverter is stored in the inverters table.
        #                        It is compared to the REGION NAME in the UNIVERSAL AIF table. If the inverter's
        #                        software profile is not considered to be in "this region", an alarm is raised.
        #
        # Formerly, and until recently: the Gateway would detect a software discrepancy and if necessary, command
        # the inverter with the "appropriate" software profile. Due to manufacturing requirements and the physical
        # labeling of the inverter cases, no such update is to be made. Just detect a perceived discrepancy and
        # raise an alarm.


        # REGION NAME by software profile. Although many software profiles can be specified in an SG424 inverter,
        # only a few are valid insofar as the Gateway is concerned. Currently, 120v-only software profiles are defined.
        region_name_by_software_profile = {SWPROFILE_SG424_NA120         : (REGION_NAME_NA120),
                                           SWPROFILE_SG424_HI120         : (REGION_NAME_HI120),
                                           SWPROFILE_SG424_BATTERY_NA120 : (REGION_NAME_NA120),
                                           SWPROFILE_SG424_BATTERY_HI120 : (REGION_NAME_HI120),
                                           SWPROFILE_SG424_BATTERY_CA120 : (REGION_NAME_CA120),
                                           SWPROFILE_SG424_CA120         : (REGION_NAME_CA120),
                                           SWPROFILE_SG424_MFG120        : (REGION_NAME_MFG120)
                                          }

        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]
        column_name = special_tuple.db_col_name
        universal_aif_data_row = gu.get_universal_aif_db_row()
        agreed = False

        if universal_aif_data_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME] != REGION_NAME_UNDEFINED:
            # The U-AIF table has been configured for a specific region.
            region_name = universal_aif_data_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]
            '''
            if value == universal_aif_data_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]:
                agreed = True
            if agreed:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
            else:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_SW_PROF_NOT_IN_REGION_NAME)
            '''
            # Instead, and for now:
            expected_region_name = region_name_by_software_profile[value]
            if expected_region_name == region_name:
                agreed = True
            if agreed:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
            else:
                # Write to the results file, and raise an alarm.
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_SW_PROF_NOT_IN_EXPECTED_REGION)
                Alarm.occurrence(ALARM_ID_SOFTWARE_PROFILE_REGION_NAME_MISMATCH)
                logger = logging_setup.setup_logger("TLVUtils")
                logger.info("REGION NAME %s MISMATCH FROM %s (SW PROF %s)" % (region_name, self.inverter_ip_address, value))

        else:
            # Slap the user - U-AIF has not been configured.
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_U_AIF_NOT_CONFIGURED)

    def handle_op_mode_with_inverter_update(self, value, incoming_tlv_id):
        # HANDLE THE FOLLOWING ODDITY: database contains the operation mode in the gateway table. Inverter reports
        # INV_CTRL as a 16-bit hex value, with the most significant bit indicating if it is under gateway control.
        #
        # First determine if value (string representing INV_CTRL as hex with leading 0x) indicates if the inverter is
        # under gateway control or not.
        under_gateway_control = gu.is_gateway_control_set_in_inv_ctrl(value)
        special_tuple = TLVTranslator.lookup_table[incoming_tlv_id]

        # Get the site's operation mode from the gateway table.
        op_mode_string = gu.get_gateway_operation_mode()
        if op_mode_string == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE:
            op_mode = 1
        elif op_mode_string == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE:
            op_mode = 0

        if under_gateway_control and (op_mode == 1):
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
        elif (not under_gateway_control) and (op_mode == 0):
            self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE)
        else:
            # In all other cases, inverter is to be updated: if op_mode is POWER_CONTROL_MODE, then GATEWAY_CONTROL bit
            # should be set, else it should be cleared. Ordinarily, this would be a unicast command to "that" inverter.
            #
            # Note: since all inverters should have the same setting based on operation mode, this particular
            #       update is multicasted, to ensure that other out-of-sync inverters get the update. Calling
            #       this method may result in several such multicast packets. This is not an issue, as it would
            #       be low-rate packet sending, and ensures that as many inverters get updated if and when there
            #       are ethernet communciation problems.
            if gu.is_pack_and_send_gateway_control(op_mode):
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_GTW_CTRL_MULTICAST_OK)
            else:
                self.add_to_textfile(special_tuple.index, value, TLVTranslator.TEXTOUT_GTW_CTRL_MULTICAST_NOK)

    def handle_with_db_update(self, value, incoming_tlv_id):
        ordered_tlv_index = TLVTranslator.lookup_table[incoming_tlv_id].index
        db_col = TLVTranslator.lookup_table[incoming_tlv_id].db_col_name

        if self.consistency_check(db_col, value):  # they agreed
            out_condition = TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE

        elif db_col in self.inverter_db_row:  # db and value disagreed
            out_condition = TLVTranslator.TEXTOUT_UPDATE_DATABASE_REQUIRED
            # only update if we know that there is a db column
            if db_col != self.DEFAULT_COLUMN_NAME:
                self.queue_for_db_update(db_col, value)

        else: # the column didn't exist in the database
            out_condition = TLVTranslator.TEXTOUT_NOT_IN_DATABASE

        self.add_to_textfile(ordered_tlv_index, value, out_condition)

    def handle_with_db_additive(self, value, incoming_tlv_id):
        # Passed value is added to that already in the database. *** CURRENTLY NOT USED ***
        ordered_tlv_index = TLVTranslator.lookup_table[incoming_tlv_id].index
        db_col = TLVTranslator.lookup_table[incoming_tlv_id].db_col_name
        out_condition = TLVTranslator.TEXTOUT_UPDATE_DATABASE_REQUIRED
        database_value = self.inverter_db_row[db_col]
        new_value = database_value + value
        self.queue_for_db_update(db_col, new_value)
        self.add_to_textfile(ordered_tlv_index, new_value, out_condition)

    def handle_with_db_comparison(self, value, incoming_tlv_id):
        tup = TLVTranslator.lookup_table[incoming_tlv_id]
        db_col = tup.db_col_name
        ordered_tlv_index = tup.index

        if self.consistency_check(db_col, value):  # they agreed
            out_condition = TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE
        else:   # db and value disagreed
            out_condition = TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_DISAGREE
        # end if/else
        self.add_to_textfile(ordered_tlv_index, value, out_condition)

    def handle_with_output_only(self, value, incoming_tlv_id):
        self.add_to_textfile(TLVTranslator.lookup_table[incoming_tlv_id].index, value, TLVTranslator.TEXTOUT_NO_COMPARISON_PERFORMED)

    def consistency_check(self, db_col, actual):
        """
        Compares the value reported by the inverter against the
        value stored in the database.
        :param db_col: Name of DB column where to find the stored value
        :param actual: Value reported by the inverter
        :return: True if consistent. False if inconsistent.
        """
        if db_col not in self.inverter_db_row:
            self.log_error("Column %s does not currently exist in the database" % db_col)
            return False

        return self.inverter_db_row[db_col] == actual

    def custom_handler_macaddress(self, value, tlv_id):
        tup = TLVTranslator.lookup_table[tlv_id]
        ordered_tlv_index = tup.index
        db_col = tup.db_col_name

        if db_col not in self.inverter_db_row:
            mac_in_db = ""
        else:
            mac_in_db = self.inverter_db_row[db_col].lower()

        if value.lower() == mac_in_db:
            out_condition = TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE
        else:
            self.queue_for_db_update(db_col, value)
            out_condition = TLVTranslator.TEXTOUT_UPDATE_DATABASE_REQUIRED
        self.add_to_textfile(ordered_tlv_index, value, out_condition)

    ''' Setting up the various types of output '''

    def queue_for_db_update(self, column, argument):
        self.db_update_cols.append(column)
        self.db_update_args.append(argument)

    def queue_for_inverter_update(self, incoming_tlv_type, value):
        # This method translates incoming TLV IDs into outgoing IDs
        self.outgoing_tlv_manager.add(TLVTranslator.incoming_to_outgoing_tlv_lut[incoming_tlv_type], value)

    def add_to_textfile(self, ordered_tlv_index, plain_value, output_condition):
        pretty_value = str(plain_value)

        if output_condition == TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_AGREE:
            pretty_value = "OK = %s" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_NO_UPDATE_REQUIRED_DISAGREE:
            pretty_value = "%s (database disagreed)" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_UPDATE_INVERTER_REQUIRED:
            pretty_value = "%s *** INVERTER UPDATED ***" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_UPDATE_DATABASE_REQUIRED:
            pretty_value = "%s *** DB UPDATED ***" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_NOT_IN_DATABASE:
            pretty_value = "%s (Not found in database)" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_GTW_CTRL_MULTICAST_OK:
            pretty_value = "%s (multicaset send OK)" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_GTW_CTRL_MULTICAST_NOK:
            pretty_value = "%s (multicast send *FAILED*)" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_NO_COMPARISON_PERFORMED:
            pass
        elif output_condition == TLVTranslator.TEXTOUT_U_AIF_NOT_CONFIGURED:
            pretty_value = "%s *** UNIVERSAL-AIF NOT CONFIGURED ***" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_SW_PROF_NOT_IN_EXPECTED_REGION:
            pretty_value = "%s *** NOT IN EXPECTED GATEWAY REGION ***" % pretty_value
        elif output_condition == TLVTranslator.TEXTOUT_FEED_NAME_NOT_CONFIGURED:
            pretty_value = "%s *** FEED_NAME NOT CONFIGURED IN DATABASE ***" % pretty_value
        else:
            error_string = "%s TLV had an unrecognized file formatting command." % self.ordered_tlv_names[ordered_tlv_index]
            self.log_error(error_string)

        self.ordered_values_pretty[ordered_tlv_index] = pretty_value

    def log_error(self, message):
        self.error_log_string += message + LINE_TERMINATION

    ''' LOOKUP TABLE '''

    TLVInfo = namedtuple("TLVInfo", "db_col_name text_heading index parse_method handler")

    """
    The lookup table is in charge of storing all the information about each TLV type and how
    it should be translated from TLV to a value we can use, and from the usable value to output.
    For many TLVs, you can simply keep using it as it came from the inverter. However, several need special
    conversions and a few even need their own unique handlers.
    This lookup table and the definitions of the methods it stores will handle all of that for you,
    so that from the loop, it appears as though each TLV is handled the same way.
    """
    lookup_table = {
        SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_COMM_COLUMN_NAME,
                            "RUNMODE",
                            INDEXOF_RUNMODE_TLV_TYPE,
                            custom_convert_runmode_for_comm,
                            handle_with_db_update),
        SG424_TO_GTW_VERSION_STRING_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_VERSION_COLUMN_NAME,
                            "VERSION",
                            INDEXOF_VERSION_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_BOOT_VERSION_COLUMN_NAME,
                            "BOOT_LOADER_VERSION",
                            INDEXOF_BOOT_VERSION_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_PRODUCT_PART_NUMBER_STRING_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME,
                            "PRODUCT_PART_NUM",
                            INDEXOF_PROD_PART_NO_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_SW_PROFILE_U16_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_SW_PROFILE_COLUMN_NAME,
                            "SW_PROFILE",
                            INDEXOF_SW_PROFILE_TLV_TYPE,
                            use_raw_value,
                            custom_handler_sw_profile), # JEP handle_with_db_update),
        SG424_TO_GTW_SERIAL_NUMBER_STRING_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                            "SERIAL_NUMBER",
                            INDEXOF_SERIAL_NO_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                            "MAC",
                            INDEXOF_MAC_ADDRESS_TLV_TYPE,
                            convert_string_to_lower,
                            custom_handler_macaddress),
        SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_RESETS_COLUMN_NAME,
                            "NUMBER_OF_RESETS",
                            INDEXOF_NUMBER_OF_RESETS_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_UPTIME_SECS_U32_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_UPTIME_COLUMN_NAME,
                            "UPTIME",
                            INDEXOF_UPTIME_SECS_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                            "FEED",
                            INDEXOF_CONFIG_FEED_TLV_TYPE,
                            custom_convert_feed,
                            handle_feed_with_inverter_update),
        SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE:
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "PCC",
                            INDEXOF_CONFIG_PCC_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_CONNECT_COLUMN_NAME,
                            "CONNECT",
                            INDEXOF_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_TLV_TYPE,
                            use_raw_value,
                            handle_with_inverter_update),
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE :
                    TLVInfo(UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME,
                            "PF",
                            INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_TLV_TYPE,
                            format_float_to_3_decimals,
                            handle_with_output_only), # REMOVED: handle_u_aif_with_inverter_update),
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE :
                    TLVInfo(UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME,
                            "PF_C_LEAD",
                            INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),  # REMOVED: handle_u_aif_with_inverter_update),
        SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE :
                    TLVInfo(UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME,
                            "PF_ENA",
                            INDEXOF_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),  # REMOVED: handle_u_aif_with_inverter_update),
        SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "XET_INFO",
                            INDEXOF_XET_INFO_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "INV_CTRL",
                            INDEXOF_INV_CTRL_TLV_TYPE,
                            convert_hex_to_str,
                            handle_op_mode_with_inverter_update),
        SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "KEEP_ALIVE_TIMEOUT",
                            INDEXOF_KEEP_ALIVE_TIMEOUT_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_ENERGY_REPORT_CONTROL_U16_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "ERC",
                            INDEXOF_ENERGY_REPORT_CONTROL_TLV_TYPE,
                            convert_hex_to_str,
                            handle_with_output_only),
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_WATTS_COLUMN_NAME,
                            "DC_WATTS",
                            INDEXOF_DC_SM_INSTANTANEOUS_WATTS_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "DC_VOLTS",
                            INDEXOF_DC_SM_INSTANTANEOUS_VOLTS_TLV_TYPE,
                            format_float_to_2_decimals,
                            handle_with_output_only),
        SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "DC_AMPS",
                            INDEXOF_DC_SM_INSTANTANEOUS_AMPS_TLV_TYPE,
                            format_float_to_2_decimals,
                            handle_with_output_only),
        SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE:
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "DC_GWA_SM_OUTPUT",
                            INDEXOF_DC_GWA_SM_OUTPUT_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_CURTAILMENT_PACKETS_U32_TLV_TYPE:
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "CURTAIL",
                            INDEXOF_CURTAILMENT_PACKETS_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),  # NOT THIS ONE! -> handle_with_db_additive),
        SG424_TO_GTW_REGULATION_PACKETS_U32_TLV_TYPE:
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "REGULATION",
                            INDEXOF_REGULATION_PACKETS_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),  # NOT THIS ONE! -> handle_with_db_additive),
        SG424_TO_GTW_KEEP_ALIVE_GONE_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "KEEP_ALIVE_GONE",
                            INDEXOF_KEEP_ALIVE_GONE_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_KEEP_ALIVE_BACK_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "KEEP_ALIVE_BACK",
                            INDEXOF_KEEP_ALIVE_BACK_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_KEEP_ALIVE_LATE_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "KEEP_ALIVE_LATE",
                            INDEXOF_KEEP_ALIVE_LATE_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_SEQUENCE_NUMBER_REPEAT_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "SEQ_NUM_REPEAT",
                            INDEXOF_SEQ_NUM_REPEAT_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_SEQUENCE_NUMBER_OOS_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "SEQ_NUM_OOS",
                            INDEXOF_SEQ_NUM_OOS_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "TOGGLE_UP",
                            INDEXOF_UPSTREAM_LINK_TOGGLE_COUNT_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE :
                    TLVInfo(DEFAULT_COLUMN_NAME,
                            "TOGGLE_DOWN",
                            INDEXOF_DOWNSTREAM_LINK_TOGGLE_COUNT_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_BATTERY_U16_TLV_TYPE :
                    TLVInfo(INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                            "BATTERY",
                            INDEXOF_BATTERY_TLV_TYPE,
                            use_raw_value,
                            handle_with_db_update),
        SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE :
                    TLVInfo(GROUPS_TABLE_FALLBACK_COLUMN_NAME,
                            "FALLBACK",
                            INDEXOF_FALLBACK_POWER_SETTING_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE :
                    TLVInfo(GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME,
                            "NTP_URL",
                            INDEXOF_NTP_URL_SG424_TLV_TYPE,
                            use_raw_value,
                            handle_with_output_only),
        SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                            "PCS_GROUP_ID",
                            INDEXOF_PCS_GROUP_ID_TLV_TYPE,
                            use_raw_value,
                            handle_with_inverter_update),
        SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE:
                    TLVInfo(INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                            "GROUP_ID",
                            INDEXOF_GROUP_ID_TLV_TYPE,
                            use_raw_value,
                            custom_handler_group_data),

    }


    incoming_to_outgoing_tlv_lut = {
        # DEPRECATED/REMOVED:
        # SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE: GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE,
        # SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE : GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE,
        # SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE : GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE,
        SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE : GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE,
        # SG424_TO_GTW_REGION_NAME_STRING_TLV_TYPE : GTW_TO_SG424_REGION_NAME_STRING_TLV_TYPE,
        # SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE : GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE,
        SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE : GTW_TO_SG424_CONFIG_FEED_U16_TLV_TYPE,
        SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE : GTW_TO_SG424_CONFIG_PCC_U16_TLV_TYPE,
        SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE : GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE,
        # SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE : GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE,
        SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE : GTW_TO_SG424_PCS_UNIT_ID_CONFIG_U16_TLV_TYPE
    }

    ''' METHODS FOR READING IN TLVS '''

    def __init__(self, tlv_ids, tlv_vals, inverter_db_row, regional_settings, inverters_audited_so_far, ip_address, string, string_position):
        """
        The constructor is used purely for PROCESSING the input.
        It prepares the output as part of this processing, but does not
        send that output anywhere. If you want to get the output,
        you should use the static method do_all_processing.

        :param tlv_ids:  A list of TLV types that the inverter gave us
        :param tlv_vals: The values corresponding to the types given in tlv_ids
        :param inverter_db_row: The DB row for this inverter, used for DB comparison and some output
        :param inverters_audited_so_far: An iteration number that is used for printing to the excel string
        """
        def merge_dicts(dict_to_keep_defaults, dict_to_add):
            if dict_to_add is None:
                return dict_to_keep_defaults
            for key in dict_to_add.keys():
                if key not in dict_to_keep_defaults:
                    dict_to_keep_defaults[key] = dict_to_add[key]
            # end for
            return dict_to_keep_defaults

        # TLV values received from the inverter, either exactly as reported, or formatted with info about DB update
        self.ordered_values_plain = [TLVTranslator.DEFAULT_TLV_VALUE] * TLVTranslator.NUMBER_OF_TLV_TYPES
        self.ordered_values_pretty = [TLVTranslator.DEFAULT_TLV_VALUE] * TLVTranslator.NUMBER_OF_TLV_TYPES
        # whether each type of TLV was reported by the inverter or not
        self.reported_markers = [False] * TLVTranslator.NUMBER_OF_TLV_TYPES

        # the items that need to be updated in the database. by default we will assume no update required
        self.db_update_cols = []
        self.db_update_args = []
        self.outgoing_tlv_manager = TLVPacker()
        self.inverter_update_problem_flag = False

        if not self.valid_tlv_inputs(tlv_ids, tlv_vals):
            return

        # the names of each type of TLV
        self.ordered_tlv_names = TLVTranslator.build_tlv_names_array()

        # this info will need to be accessed later for our output
        self.inverter_db_row = merge_dicts(inverter_db_row, regional_settings)
        self.inverters_audited = inverters_audited_so_far
        self.inverter_ip_address = ip_address
        self.inverter_string = string
        self.inverter_string_position = string_position

        # To support auditing of reported inverter data against the group ID table for that inverter:
        self.inverter_group_id_was_reported = False
        self.inverter_fallback_was_reported = False
        self.inverter_ntp_server_was_reported = False
        self.inverter_inv_ctrl_was_reported = False

        for this_tlv_id, this_tlv_value in zip(tlv_ids, tlv_vals):
            if this_tlv_id == SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE:
                self.inverter_reported_group_id = this_tlv_value
                self.inverter_group_id_was_reported = True
            elif this_tlv_id == SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE:
                self.inverter_reported_fallback = this_tlv_value
                self.inverter_fallback_was_reported = True
            elif this_tlv_id == SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE:
                self.inverter_reported_ntp_server = this_tlv_value
                self.inverter_ntp_server_was_reported = True
            elif this_tlv_id == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:
                self.inverter_reported_inv_ctrl = this_tlv_value
                self.inverter_inv_ctrl_was_reported = True

            # ... NOT READY FOR er_server_http
            # ... NOT READY FOR er_grtw_tlv
            # ... NOT READY FOR AIF_ID

            else:
                pass

        # configuring our output for the text file
        self.output_style = TLVTranslator.INCLUDE_VALUES_NOT_REPORTED
                # options are
                # - INCLUDE_VALUES_NOT_REPORTED
                # - OMIT_VALUES_NOT_REPORTED

        self.error_log_string = ''

        for i in range(0, len(tlv_ids)):
            if tlv_ids[i] not in self.lookup_table:
                self.log_error("Could not use TLV ID #%s, #%d in list." % (tlv_ids[i], i+1))
                continue

            #setup the processing
            tlv_tuple = self.lookup_table[tlv_ids[i]]

            """
            The next few lines are where the dictionary comes in handy - all TLVs can be treated
            the same in this loop, because the dictionary told us which method to call on them.
            As long as the methods stored in the dictionary can all use the same arguments, 
            this loop looks clean.
            """

            # methods that will be used to read, interpret, and report the values
            parse = tlv_tuple.parse_method
            handle = tlv_tuple.handler
            # what position in the output array this TLV will live
            index = tlv_tuple.index

            # now DO IT!
            meaningful_value = parse(self, tlv_vals[i])
            self.record_plain_value(meaningful_value, index)
            handle(self, meaningful_value, tlv_ids[i])

        # end for
    # end def

    @staticmethod
    def verify_query_data_against_db(tlv_ids_list, tlv_vals_list, database_row, universal_aif,
                                     some_output_stream, inverters_audited_so_far,
                                     ip_address, string, string_pos):
        """
        This is a method that will take your list of TLV IDs and values, process it,
        and send the output where the caller told it to send it. The user can call this one-liner
        and be done thinking about it!

        NOTE this does not close the output stream, and assumes the output stream was open when it is given.

        :param tlv_ids_list: List of TLV types given by inverter
        :param tlv_vals_list: Values corresponding to types in tlv_ids_list
        :param database_row: DB row about the inverter in question, used for DB  comparison and some output
        :param some_output_stream: The output stream where the caller wants me to print the text file
        :param inverters_audited_so_far: An iteration number used for printing to the excel string
        """

        # The constructor does all the processing so we don't have to worry about it
        helper = TLVTranslator(tlv_ids_list, tlv_vals_list, database_row, universal_aif,
                               inverters_audited_so_far, ip_address, string, string_pos)

        # update database
        helper.send_database_update()

        # update the inverters
        helper.send_inverter_update()

        # write free form text to the output stream provided
        some_output_stream.write(helper.get_text_file_output())

        return helper


    # This method is called by the PSA Daemon as fallback in case the new query is not responded to
    # by the inverter. This method does not send any updates to the inverter (that is DICA's job)
    @staticmethod
    def verify_psa_query_data_against_db(tlv_ids_list,
                                         tlv_vals_list,
                                         database_row,
                                         ip_address):
        """
        :param tlv_ids_list: List of TLV types given by inverter
        :param tlv_vals_list: Values corresponding to types in tlv_ids_list
        :param database_row: DB row about the inverter in question, used for DB  comparison and some output

        """

        # The constructor does all the processing so we don't have to worry about it
        helper = TLVTranslator(tlv_ids_list, tlv_vals_list, database_row, None,
                               0, ip_address, 0, 0)

        # update database
        helper.send_database_update()

        return helper

    def send_database_update(self):
        # Copied from Ed's code. Thanks Ed!

        # Resolve any issues with the database.
        column_list_length = len(self.db_update_cols)
        argument_list_length = len(self.db_update_args)

        # The inverters behaved themselves. Good!
        if (column_list_length == 0) and (argument_list_length == 0):
            # No database issues, so nothing to do here.
            pass
        # The inverters and the database did not agree on some things
        elif column_list_length == argument_list_length:

            #start building a MySQL string
            big_update_string = "update %s set " % DB_INVERTER_NXO_TABLE

            # add all of the things we need to update in the DB
            for iggy in range(0, column_list_length):
                big_update_string += self.db_update_cols[iggy] + " = %s"
                if iggy < argument_list_length - 1:
                    big_update_string += ", "
                # end if
            # end for

            # finish formatting the MySQL string
            big_update_string += " where " + INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME + " = %s;"

            # send it off to the DB :)
            gu.prepared_act_on_database(EXECUTE, big_update_string, self.db_update_args + [self.inverter_ip_address])
        #end if
    # end def

    def send_inverter_update(self):
        self.outgoing_tlv_manager.send_to(self.inverter_ip_address)

    @staticmethod
    def valid_tlv_inputs(ids, vals):
        if  len(ids) == len(vals):
            return True
        else:
            return False
        # end if/else
    # end def


    ''' METHODS FOR RETRIEVING OUTPUT '''

    TEXT_OUT_HEADER_WIDTH = 26
    def get_text_file_output(self):
        text = ''
        for i in range(0, len(self.ordered_values_pretty)):
            if self.reported_markers[i] or (self.output_style == TLVTranslator.INCLUDE_VALUES_NOT_REPORTED):
                text += TLVTranslator.format_text_out_line(self.ordered_tlv_names[i], self.ordered_values_pretty[i])
        # end for
        text += TLVTranslator.format_excel_text_out_line("EXCEL", self.get_excel_string())

        # validate string position numbers
        if self.inverter_string_position < 1 or self.inverter_string_position > MAX_NUMBER_OF_INVERTERS_IN_STRING:
            text += "*** WARNING *** Inverter string position %d is outside the allowable range!%s" % \
                    (self.inverter_string_position, LINE_TERMINATION)

        # validate string id
        if self.inverter_string == DEFAULT_STRING_ID:
            text += "*** WARNING *** String ID %s is the default!%s" % \
                    (self.inverter_string, LINE_TERMINATION)

        if TLVTranslator.VERBOSE_OUTPUT:
            text += self.error_log_string

        if self.inverter_update_problem_flag:
            text += "*** WARNING *** Stubborn inverter %s refused to be updated.%s" % (self.inverter_ip_address, LINE_TERMINATION)

        return text
    # end def

    @staticmethod
    def format_text_out_line(left_value, right_value):
        return " - %-*s : %s%s" % (TLVTranslator.TEXT_OUT_HEADER_WIDTH, left_value, right_value, LINE_TERMINATION)

    @staticmethod
    def format_excel_text_out_line(left_value, right_value):
        # The same as above but add a comma after the colon for excel importation.
        return " - %-*s :, %s%s" % (TLVTranslator.TEXT_OUT_HEADER_WIDTH, left_value, right_value, LINE_TERMINATION)

    @staticmethod
    def get_excel_title_string():
        # names of each field in the specifed order
        columns = TLVTranslator.build_tlv_names_array()

        excel_string = '%s,%s,%s,%s' % ("INVERTERS_AUDITED",
                                        "IP",
                                        "STRING",
                                        "STRING_POSITION")
        for i in range(0, len(columns)):
            excel_string += "," + columns[i]

        return excel_string + LINE_TERMINATION

    def get_excel_string(self):
        # # Prepare the 1-line EXCEL string. Use number_of_inverters_audited_so_far as the index.
        # excel_string = "- EXCEL: ,%s,%s,%s,%s" % (self.number_of_inverters_audited_so_far,
        #                                           self.inverter_ip_address,
        #                                           self.inverter_string_id,
        #                                           self.inverter_string_position)
        #
        # # Build the excel line for this inverter
        # for tlv_type_index in range(0, NUMBER_OF_TLV_INDEXES):
        #     excel_string += "," + report_strings[tlv_type_index]

        excel_string = '%s,%s,%s,%s' % (self.inverters_audited,
                                        self.inverter_ip_address,
                                        self.inverter_string,
                                        self.inverter_string_position)

        for i in range(0, len(self.ordered_values_plain)):
            excel_string += ",%s" % str(self.ordered_values_plain[i])

        return excel_string

    def get_db_update_info(self):
        return (self.db_update_cols, self.db_update_args)

    def get_error_log_string(self):
        return self.error_log_string
# end class TLVTranslator
