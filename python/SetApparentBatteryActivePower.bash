#!/bin/bash
# Set the Apparent battery active power.
#
# copyright Apparent Inc. 2019
#
power=$1

# Set the battery mode to 'manual'.
/usr/share/apparent/python/EssCLI.py --set-battery-mode manual

# Send the battery a 'run' command to wake it up.
/usr/share/apparent/python/EssCLI.py --run
sleep 5

# Send the battery a 'set active power' command.
/usr/share/apparent/python/EssCLI.py --active-power ${power}

