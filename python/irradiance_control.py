#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import os
import time
import math
import socket
import struct
import calendar
from datetime import datetime

import logging
import logging.handlers

import MySQLdb as MyDB
from contextlib import closing
import MySQLdb.cursors as my_cursor

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
import setproctitle

from lib.helpers import datetime_to_utc
from lib.db import prepared_act_on_database
from lib.logging_setup import *
from gateway_utils import *


# VERY LOCAL DEFINITIONS CAN GO HERE ...

# This script was launched by the monitor to provide irradiance control. There is a control sequence between
# the monitor and irradiance control ("updated") to keep this daemon going.
#
# This script is a single-process daemon

THE_UNIVERSE_IS_A_REALLY_BIG_PLACE = True

def instantiate_irradiance_control():
    try:
        setproctitle.setproctitle('%s-irrad' % GATEWAY_PROCESS_PREFIX)
        IrradianceControlProcess()
    except Exception as ex:
        logger = setup_logger('irractrl')
        logger.exception('Unhandled exception caught in irractrl daemon')


class IrradianceControlProcess(object):
    def __init__(self, _=None):

        self.enabled = 0
        self.started = 0
        self.updated = 0
        self.irradiance_pid = None

        self.max_inverters = 0
        self.site_capacity = 0
        self.inverter_power = 0
        self.query_frequency = 0
        self.last_query_utc = 0
        self.num_queries = 0
        self.num_no_responses = 0

        self.nxo_multipler_A = 0
        self.nxo_multipler_B = 0
        self.nxo_multipler_C = 0

        self.setup_logger()

        # ----------------------------------------
        self.run_irradiance_control()
        # ----------------------------------------

    def setup_logger(self):
        self.my_logger = setup_logger("irractrl")

    def log_info(self, message):
        self.my_logger.info(message)

    def raise_and_clear_alarm(this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)

    def set_irradiance_control_updated(self):

        # Synopsis: set the irradiance_control_is_running bit in the control table,
        #           to prevent the monitor from launching another process.
        update_string = "update %s set %s = %s;" % (DB_IRRADIANCE_CONTROL_TABLE,
                                                    DB_IRRA_CONTROL_TBL_UPDATED_COLUMN_NAME, "%s")
        update_args = (datetime_to_utc(datetime.utcnow()), )
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def setup_running_parameters(self):
        # Run once on startup.
        select_string = "SELECT * FROM %s;" % DB_IRRADIANCE_CONTROL_TABLE
        irra_row = prepared_act_on_database(FETCH_ONE, select_string, ())

        if irra_row:
            self.enabled = irra_row[DB_IRRA_CONTROL_TBL_ENABLED_COLUMN_NAME]
            self.started = irra_row[DB_IRRA_CONTROL_TBL_STARTED_COLUMN_NAME]
            self.updated = irra_row[DB_IRRA_CONTROL_TBL_UPDATED_COLUMN_NAME]
            self.max_inverters = irra_row[DB_IRRA_CONTROL_TBL_MAX_INVERTERS_COLUMN_NAME]

            self.nxo_multipler_A = irra_row[DB_IRRA_CONTROL_TBL_NXO_MULTIPLIER_A_COLUMN_NAME]
            self.nxo_multipler_B = irra_row[DB_IRRA_CONTROL_TBL_NXO_MULTIPLIER_B_COLUMN_NAME]
            self.nxo_multipler_C = irra_row[DB_IRRA_CONTROL_TBL_NXO_MULTIPLIER_C_COLUMN_NAME]

            self.query_frequency = irra_row[DB_IRRA_CONTROL_TBL_QUERY_FREQ_COLUMN_NAME]
            self.num_queries = irra_row[DB_IRRA_CONTROL_TBL_NUM_QUERIES_COLUMN_NAME]
            self.num_no_responses = irra_row[DB_IRRA_CONTROL_TBL_NUM_NO_RESPONSES_COLUMN_NAME]

            # Write back site_capacity (calculated) and inverter_power (0W).
            select_string = "SELECT COUNT(*) FROM %s;" % DB_INVERTER_NXO_TABLE
            fetch_results = prepared_act_on_database(FETCH_ONE, select_string, ())
            total_number_of_inverters = int(fetch_results['COUNT(*)'])
            self.site_capacity = total_number_of_inverters * 250
            update_string = "UPDATE %s SET %s = %s, %s = %s;" % (DB_IRRADIANCE_CONTROL_TABLE,
                                                                 DB_IRRA_CONTROL_TBL_SITE_CAPACITY_COLUMN_NAME, "%s",
                                                                 DB_IRRA_CONTROL_TBL_INVERTER_POWER_COLUMN_NAME, "%s")
            update_args = (self.site_capacity, 0)
            prepared_act_on_database(EXECUTE, update_string, update_args)

        else:
            # Irradiance control table empty???
            # TODO: ALARM!!!
            self.log_info("IRRADIANCE CONTRPOL TABLE EMPTY!!!")

    def init_irradiance_inverters_table(self):
        update_string = "UPDATE %s set %s = %s, %s = %s, %s = %s;" % (DB_IRRADIANCE_INVERTERS_TABLE,
                                                                      DB_IRRA_INVERTERS_TBL_LAST_RESPONDED_COLUMN_NAME, "%s",
                                                                      DB_IRRA_INVERTERS_TBL_LAST_RESPONSE_UTC_COLUMN_NAME, "%s",
                                                                      DB_IRRA_INVERTERS_TBL_LAST_WATTS_COLUMN_NAME, "%s")
        update_args = (0, 0, 0)
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def update_running_parameters(self):
        # Run every time through the main loop. Allows the user to change some of the parameters
        # that guide the behavior of the irradiance controller.
        select_string = "SELECT %s,%s,%s FROM %s;" % (DB_IRRA_CONTROL_TBL_MAX_INVERTERS_COLUMN_NAME,
                                                      DB_IRRA_CONTROL_TBL_QUERY_FREQ_COLUMN_NAME,
                                                      DB_IRRA_CONTROL_TBL_NUM_NO_RESPONSES_COLUMN_NAME,
                                                      DB_IRRADIANCE_CONTROL_TABLE)
        irra_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        if irra_row:
            self.max_inverters = irra_row[DB_IRRA_CONTROL_TBL_MAX_INVERTERS_COLUMN_NAME]
            self.query_frequency = irra_row[DB_IRRA_CONTROL_TBL_QUERY_FREQ_COLUMN_NAME]
            self.num_no_responses = irra_row[DB_IRRA_CONTROL_TBL_NUM_NO_RESPONSES_COLUMN_NAME]

    def is_control_frequency_time_expired(self):
        time_expired = False
        if self.current_utc - self.last_query_utc >= self.query_frequency:
            # Time to check in with the irradiance inverters.
            time_expired = True
            self.last_query_utc = self.current_utc
        return time_expired

    def mark_as_started(self):

        # Synopsis: write the irradiance-control-is-started in the control table,
        #           to prevent the monitor from launching another process.
        update_string = "update %s set %s = %s, %s = %s, %s = %s;" % (DB_IRRADIANCE_CONTROL_TABLE,
                                                                      DB_IRRA_CONTROL_TBL_STARTED_COLUMN_NAME, "%s",
                                                                      DB_IRRA_CONTROL_TBL_UPDATED_COLUMN_NAME, "%s",
                                                                      DB_IRRA_CONTROL_TBL_PID_COLUMN_NAME, "%s")
        update_args = ('1', datetime_to_utc(datetime.utcnow()), str(self.irradiance_pid))
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def log_this_hex_data(self, some_hex_data):
        # Will log the raw hex data in packed data:
        this_string = "HEX DATA: "
        if some_hex_data:
            for char_char in some_hex_data:
                this_string += hex(ord(char_char))[2:].zfill(2) + " "
        self.log_info(this_string)

    def query_irradiance_inverters(self):
        #
        # - go through irradiance_inverters table, for each entry:
        #         - query
        #         - extract watts value
        #         - write watts to last_watts
        #         - write utc to last_reponded
        # - when done, update the irraduiance control table:
        #         - sum watts written to inverter_power\
        #         - num_queries incremented
        #         - num_no_responses incremented if necessary


        irra_ip_list = get_irra_inverter_ip_list()
        if len(irra_ip_list) > 0:
            queries = 0
            num_no_responses = 0
            power_this_time = 0
            query_tlv_list = []
            query_data_list = []

            for iggy in range (0, len(irra_ip_list)):
                # Query this inverter.
                queries += 1
                if len(query_tlv_list) != 0:
                    del query_tlv_list[:]
                    del query_data_list[:]
                ip_address = irra_ip_list[iggy]
                SG424_data = send_and_get_query_data(ip_address)
                (query_tlv_list, query_data_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)

                update_string = None
                update_args = []
                if len(query_tlv_list) > 0:
                    # Inverter responded to the request, retrieve the watts that were reported.
                    for pop in range(0, len(query_tlv_list)):
                        if query_tlv_list[pop] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE:
                            # Found the little bugger ...
                            these_watts = query_data_list[pop]
                            power_this_time += these_watts

                            # MORE STUFF TO COME
                            # ip_address  | mac_address       | serial_number | last_responded | last_watts
                            # Prepare to write to the irradiance inverters table:
                            this_time = time.strftime("%I:%M:%S")
                            this_time_utc = int(time.time())
                            update_string = "UPDATE %s set %s = %s, %s = %s, %s = %s WHERE %s = %s;" \
                                                           % (DB_IRRADIANCE_INVERTERS_TABLE,
                                                              DB_IRRA_INVERTERS_TBL_LAST_RESPONDED_COLUMN_NAME, "%s",
                                                              DB_IRRA_INVERTERS_TBL_LAST_RESPONSE_UTC_COLUMN_NAME, "%s",
                                                              DB_IRRA_INVERTERS_TBL_LAST_WATTS_COLUMN_NAME, "%s",
                                                              DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME, "%s")
                            update_args = (1, this_time_utc, these_watts, ip_address)
                            break
                else:
                    # Did not repond. Something wrong with the network? # Prepare to write to the irradiance inverters table:
                    num_no_responses += 1
                    update_string = "UPDATE %s set %s = %s WHERE %s = %s;" % (DB_IRRADIANCE_INVERTERS_TABLE,
                                                                              DB_IRRA_INVERTERS_TBL_LAST_RESPONDED_COLUMN_NAME, "%s",
                                                                              DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME, "%s")
                    update_args = (0, ip_address)
                # Update the irradiance inverters table for this particular IP.
                prepared_act_on_database(EXECUTE, update_string, update_args)

            # ... for

            # Done. Update the irradiance control table with the number of queries/no_responses.
            # Read, compare and update.
            select_string = "SELECT * FROM %s;" % DB_IRRADIANCE_CONTROL_TABLE
            irra_row = prepared_act_on_database(FETCH_ONE, select_string, ())

            if irra_row:
                self.inverter_power = irra_row[DB_IRRA_CONTROL_TBL_INVERTER_POWER_COLUMN_NAME]
                db_queries = self.num_queries = irra_row[DB_IRRA_CONTROL_TBL_NUM_QUERIES_COLUMN_NAME]
                db_num_no_responses = irra_row[DB_IRRA_CONTROL_TBL_NUM_NO_RESPONSES_COLUMN_NAME]
                db_inverter_power = power_this_time

                # Update data and write it back to the database:
                db_inverter_power = power_this_time
                db_queries += queries
                db_num_no_responses += num_no_responses
                update_string = "update %s set %s = %s, %s = %s, %s = %s;" % (DB_IRRADIANCE_CONTROL_TABLE,
                                                                              DB_IRRA_CONTROL_TBL_INVERTER_POWER_COLUMN_NAME, "%s",
                                                                              DB_IRRA_CONTROL_TBL_NUM_QUERIES_COLUMN_NAME, "%s",
                                                                              DB_IRRA_CONTROL_TBL_NUM_NO_RESPONSES_COLUMN_NAME, "%s")
                update_args = (db_inverter_power, db_queries, db_num_no_responses)
                prepared_act_on_database(EXECUTE, update_string, update_args)

            else:
                # Never seen before ...
                # TODO: ALARM!!!
                self.log_info("IRRADIANCE_CONTROL TABLE EMPTY!!!")

        else:
            # The irradiance_inverters table is empty. There are no irradiance inverters.
            pass
        pass




    def run_irradiance_control(self):

        # Synopsis: -
        #           -
        #           -
        #           -

        # Get the current (utc) time, and prepare a startup banner message.
        this_time = time.strftime("%I:%M:%S")
        this_time_utc = int(time.time())
        self.current_utc = this_time_utc
        self.last_query_utc = self.current_utc
        self.irradiance_pid = os.getpid()

        startup_banner_string = "***IRRA STARTED (PID %s) AT %s (UTC = %s) ***" % (self.irradiance_pid, this_time, this_time_utc)
        self.log_info(startup_banner_string)

        # ---------------------------------------------------------------------
        # Until further notice ...
        departure_banner_string = "***IRRA SELF-DEPARTED UNTIL FURTHER NOTICE ***"
        self.log_info(departure_banner_string)
        exit(0)
        # ---------------------------------------------------------------------

        # ---------------------------------------------------------------------
        # Retrieve running parameters from the irradiance_control table.
        self.setup_running_parameters()
        # ---------------------------------------------------------------------

        # ---------------------------------------------------------------------
        # Init the irradsiance inverters table if rows have been defined.
        self.init_irradiance_inverters_table()
        # ---------------------------------------------------------------------

        # Set this daemon as started.
        self.mark_as_started()

        # Control variable, for regular monitor update.
        loco_loop_timer = 0

        # Run forever until eternity is reached.
        while THE_UNIVERSE_IS_A_REALLY_BIG_PLACE:

            # Keep it as simple as possible:
            #
            # -
            # -
            # -
            # -
            # -
            # -

            time.sleep(1)
            self.current_utc = int(time.time())

            # Always do this, even if suspended. The monitor needs to know if this process is running or not.
            loco_loop_timer += 1
            if loco_loop_timer >= SET_UPDATE_BIT_FREQUENCY:
                self.set_irradiance_control_updated()
                loco_loop_timer = 0

            '''

            IRRADIANCE MACHINE HAS BEEN TAKEN OUT OF COMMISSION UNTIL FURTHER NOTICE ...

            # Check the few parameters that the user can modify for on-the-fly changes in this daemon's behavior.
            self.update_running_parameters()

            # -------------------------------------------------------------------------------------
            #
            # The actual irradiance control.
            #
            # Ready to check the irradiance inverters?
            if self.is_control_frequency_time_expired():
                self.query_irradiance_inverters()
            # -------------------------------------------------------------------------------------
            '''
        # ... while

def main():
    IrradianceControlProcess()
    return

if __name__ == '__main__':
    main()
    exit(0)
