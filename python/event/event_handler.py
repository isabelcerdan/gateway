import logging
import event

class EventHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        if hasattr(record, 'event_tags'):
            e = event.Event(severity=record.levelname,
                            process=record.processName,
                            message=record.message,
                            tags=record.event_tags)
            e.save()

