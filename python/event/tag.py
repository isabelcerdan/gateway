import time

from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database
from lib.model import Model, RecordNotFound


class Tag(Model):
    sql_table = 'tags'

    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.name = kwargs.get('name')
        self.time = kwargs.get('time', None)

    def save(self):
        if not self.time:
            self.time = time.time()
        sql = "INSERT INTO tags (name, time) VALUES (%s, %s)"
        args = [self.name, int(self.time)]
        self.id = prepared_act_on_database(EXECUTE, sql, args)

    @staticmethod
    def find_or_create_by_name(name):
        try:
            tag = Tag.find_by(name=name)
        except RecordNotFound:
            tag = Tag(name=name)
            tag.save()

        return tag
