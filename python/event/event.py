import time

from alarm.alarm_constants import SEVERITY_CRITICAL
from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database
from tag import Tag


class EventTag:
    def __init__(self, **kwargs):
        self.event_id = kwargs.get('event_id')
        self.tag_id = kwargs.get('tag_id')

    def save(self):
        sql = "INSERT INTO events_tags (event_id, tag_id) VALUES (\"%s\", \"%s\")"
        args = [self.event_id, self.tag_id]
        prepared_act_on_database(EXECUTE, sql, args)


class Event:
    def __init__(self, **kwargs):
        self.severity = kwargs.get('severity', SEVERITY_CRITICAL)
        self.process = kwargs.get('process', '')
        self.message = kwargs.get('message', '')
        self.tags = kwargs.get('tags', [])

    def save(self):
        sql = "INSERT INTO %s (%s, %s, %s, %s) VALUES (%s, %s, %s, %s)" % \
              (DB_EVENTS_TABLE,  DB_EVENTS_TBL_SEVERITY_COLUMN_NAME,
               DB_EVENTS_TBL_TIME_COLUMN_NAME, DB_EVENTS_TBL_PROCESS_COLUMN_NAME,
               DB_EVENTS_TBL_MESSAGE_COLUMN_NAME, "%s", "%s", "%s", "%s")
        args = [self.severity, time.time(), self.process, self.message]
        event_id = prepared_act_on_database(EXECUTE, sql, args)

        if len(self.tags):
            for tag_name in self.tags:
                tag = Tag.find_or_create_by_name(tag_name)
                et = EventTag(event_id=event_id, tag_id=tag.id)
                et.save()
