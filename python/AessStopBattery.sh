#!/bin/sh
# Stop the Pimentel ESS-One battery.
#
# copyright Apparent Inc. 2019
#

# Stop the battery
sudo /usr/share/apparent/.py2-virtualenv/bin/python /usr/share/apparent/python/EssCLI.py --stop

sleep 2

/usr/share/apparent/python/DbCompare.py --table ess_master_info \
                                        --column unit_work_state \
                                        --where-column ess_id \
                                        --where-value 1 \
                                        --compare-value Stopped

if [ $? -ne 0 ]
then
  echo "Stop command failed."
else
  echo "Battery successfully stopped."
fi
