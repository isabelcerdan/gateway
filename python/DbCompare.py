#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
@module: DbCompare.py

@description: DbCompare is a CLI interface to the Gateway Database.  It allows
              script writters to retrieve a single column in an existing table
              in the database, optionally using a 'where' clause, and compare
              it to an expected value.

@author:     steve

@copyright:  2018 Apparent Energy Inc. All rights reserved.
'''

import sys
import os
from contextlib import closing
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
import argparse
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_BYD_CESS_400_START_SCRIPT_FAILED


__all__ = []
__version__ = 0.1
__date__ = '2018-10-03'
__updated__ = '2018-10-03'

logger = setup_logger('DbCompare')

parser = argparse.ArgumentParser(description='CLI for MySQL table selects')

DbCompare_command_input_dict = \
    {
        '--usage': ['DbCompare --table <table name> --column <column name> [--where-column <column name> --where-value <value>]', 0],
        '--table': ['Database table name', 1],
        '--column': ['Set column name', 1],
        '--compare-value' : ['Value to compare with database', 1],
        '--where-column': ['Where column name', 1],
        '--where-value': ['Where column value', 1]
    }

def prepared_act_on_database(action, command, args):
    try:
        with closing(MyDB.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                  db=DB_NAME, cursorclass=my_cursor.DictCursor)) as connection:
            with connection as cursor:
                cursor.execute(command, args)
                if action == FETCH_ONE:
                    return cursor.fetchone()
                elif action == FETCH_ALL:
                    return cursor.fetchall()
                return True
    except Exception as error:
        critical_string = "act_on_database failed. \nAction: %s \nString: %s \nArgs: %s - %s" \
                          % (action, command, args, repr(error))
        logger.exception(critical_string)
        raise Exception

def get_args():
    global parser 

    for command, articles in DbCompare_command_input_dict.iteritems():
        if articles[1] < 1:
            parser.add_argument(command,
                                action = 'store_true',
                                help=articles[0])
        else:
            parser.add_argument(command,
                                action="store",
                                nargs=1,
                                help=articles[0])
    return parser.parse_args()

'''
Time to figure out what the user wants...
'''
clargs = get_args()
clicmd = vars(clargs)

if clicmd['usage'] is True:
    print 'DbCompare --table <table name> --column <column name> --compare-value <value>' \
          '[--where-column <column name> --where-value <value>]'
    exit(1)

if clicmd['table'] is None:
    logger.error('Missing table name parameter')
    print('Missing table name parameter')
    exit(2)

if clicmd['column'] is None:
    logger.error('Missing column name parameter')
    print('Missing column name parameter')
    exit(3)

if clicmd['compare_value'] is None:
    logger.error('Missing compare value')
    print('Missing compare value')
    exit(4)

if clicmd['where_column'] is None:
    sql = 'SELECT %s FROM %s;' % (clicmd['column'][0], clicmd['table'][0])
    args = []
else:
    sql = 'SELECT %s FROM %s WHERE %s = %s;' % (clicmd['column'][0], clicmd['table'][0],
                                                clicmd['where_column'][0], "%s")
    args = [clicmd['where_value'][0],]
    
try:
    print sql, args
    result = prepared_act_on_database(FETCH_ONE, sql, args)
except Exception:
    logger.exception('Database action failed')
    print 'Database action failed'
    exit(5)

if result[clicmd['column'][0]] == clicmd['compare_value'][0]:
    logger.info('Database value is equal to comparison value.')
    print 'Database value is equal to comparison value.'
    exit(0)
else:
    logger.error('Database value is NOT equal to comparison value.')
    Alarm.occurrence(ALARM_ID_BYD_CESS_400_START_SCRIPT_FAILED)
    print 'Database value is NOT equal to comparison value.'
    exit(6)



