#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: ArtesynChassisRelayCLI.py

@description:

@created: Nov 12, 2018

@copyright: Apparent Inc. 2018

@author: steve
"""
import argparse
import zmq
import time

from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.gateway_constants.ArtesynConstants import artesyn_chassis_relay_cmd_dict
from lib.helpers import get_args, extract_cmd

"""
Time to figure out what the user wants...
"""
parser = argparse.ArgumentParser(description='CLI for the Artesyn Chassis Relay Daemon')
clargs = get_args(artesyn_chassis_relay_cmd_dict, parser)
ess_unit_id, command, param = extract_cmd(clargs)
if command is None:
    print "ChassisRelay: None or invalid command given"
    exit(1)
if param is True:
    param_str = 'None'
else:
    try:
        param_str = str(param)
    except TypeError:
        print "ChassisRelay: None or invalid command given"
        exit(1)
cmd_str = str(command)
cmd = "|".join((cmd_str, param_str))


context = zmq.Context(1)
pub_socket = context.socket(zmq.PUB)
pub_socket.hwm = 100
pub_socket.setsockopt(zmq.LINGER, 0)
pub_socket.connect(MSG_BROKER_XSUB_URL)

time.sleep(PUB_CONNECT_WAIT_TIME)

topic = "essid%d-relay" % int(ess_unit_id)
pub_socket.send_multipart([topic, cmd])
pub_socket.close()
context.term()

exit(0)
