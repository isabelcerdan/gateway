#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: ArtesynChargerCLI.py

@description:

@created: Nov 12, 2018

@copyright: Apparent Inc. 2018

@author: steve
"""
import argparse
import zmq
import time

from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.gateway_constants.ArtesynConstants import artesyn_controller_cmd_dict


def extract_cmd(clargs=None):
    args_dict = vars(clargs)
    for command, param in args_dict.iteritems():
        if param is not None and param is not False:
            return command, param
    return None, None

"""
Time to figure out what the user wants...
"""
parser = argparse.ArgumentParser(description='CLI for the Artesyn Controller Daemon')
for argument, kwargs in artesyn_controller_cmd_dict.iteritems():
    parser.add_argument(argument, **kwargs)
clargs = parser.parse_args()
    
command, params = extract_cmd(clargs)
chassis_id = params[0]
param_str = ' '.join(params)
cmd = "|".join([command, param_str])
print 'CLI command: %s' % cmd

context = zmq.Context(1)
pub_socket = context.socket(zmq.PUB)
pub_socket.hwm = 100
pub_socket.setsockopt(zmq.LINGER, 0)
pub_socket.connect(MSG_BROKER_XSUB_URL)

time.sleep(PUB_CONNECT_WAIT_TIME)

topic = "chassis%d#" % int(chassis_id)
pub_socket.send_multipart([topic, cmd])
pub_socket.close()
context.term()

exit(0)
