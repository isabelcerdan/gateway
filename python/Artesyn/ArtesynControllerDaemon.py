#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: ArtesynController.py

@description: This is a single-threaded version of the ArtesynControllerFactoryDaemon,
              to be used _only_ with the ArtesynControllerCLI.

@created: January 23, 2019

@copyright: Apparent Energy Inc.  2019

@author: steve
"""
import ctypes
import socket
import time
from struct import *

import numpy as np
import zmq
from alarm.alarm_constants import ALARM_ID_ARTESYN_CHARGER_CONTROLLER_RESTARTED
from daemon.daemon import Daemon
from lib.gateway_constants.ArtesynConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
from lib.gateway_constants.ZmqConstants import *
from lib.logging_setup import *
from zmq.backend.cython.utils import ZMQError
from lib.exceptions import *
import PMbusDataUtils as pdu


SUBSCRIBER_SOCKET_TIMEOUT = 5000 #ms

mod_num_to_reg_addr_dict = { 1 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1,
                             2 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2,
                             3 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3,
                             4 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4,
                             5 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5,
                             6 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6,
                             7 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7,
                             8 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8
                           }

reg_addr_to_mod_num_dict = { ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1 : 1,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2 : 2,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3 : 3,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4 : 4,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5 : 5,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6 : 6,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7 : 7,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8 : 8
                           }


class ArtesynControllerDaemon(Daemon):
    alarm_id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_RESTARTED
    update_timeout = 60.0

    CHASSIS_STATE_POWERED_OFF = 0
    CHASSIS_STATE_POWERED_ON = 1
    REQUEST_RETRIES = 3
    REQUEST_TIMEOUT = 5000 #ms
    SOCKET_TIMEOUT = 30 #sec
    
    def __init__(self, **kwargs):
        super(ArtesynControllerDaemon, self).__init__(**kwargs)

        self.zctx = None
        self.register_update_string = ''
        self.server = None
        self.seq_num = 0
        self.header = 0
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.reserved1 = 0
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.reserved2 = 0
        self.num_cmds = None
        self.unit_addr = None
        self.int_dev_addr = None
        self.reserved3 = 0
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_data = [0] * ARTESYN_CMD_DATA_BUF_LEN
        self.cmd_code = None
        self.send_buf = ctypes.create_string_buffer(ARTESYN_MSG_BUF_LEN)
        self.recv_buf = ctypes.create_string_buffer(ARTESYN_MSG_BUF_LEN)
        self.charger_list = []
        self.chassis_state = ArtesynControllerDaemon.CHASSIS_STATE_POWERED_OFF
        self.model_dict = {}
        self.ip_addr = None
        self.UDPSock = None
        self.chassis_id = None

        self.controller_command_dict = \
            {
                'chassis_up' : self.chassis_up,
                'operation': self.operation,
                'operation_module': self.operation_module,
                'operation_module_all': self.operation_module_all,
                'write_protect' : self.write_protect,
                'write_protect_module' : self.write_protect_module,
                'write_protect_module_all' : self.write_protect_module_all,
                'clear_faults' : self.clear_faults,
                'clear_module_faults' : self.clear_module_faults,
                'set_vref' : self.set_vref,
                'set_vref_chassis' : self.set_vref_chassis,
                'set_output_current_level' : self.set_output_current_level,
                'chassis_disable' : self.chassis_disable,
                'power_up' : self.power_up,
                'power_down' : self.power_down
            }
        self.db_errors = 0
        self.logger.setLevel(logging.DEBUG)

    def is_off(self):
        return ArtesynControllerDaemon.CHASSIS_STATE_POWERED_OFF == self.chassis_state

    def construct_udp_socket(self):
        try:
            self.UDPSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.UDPSock.settimeout(1.0)
        except socket.error:
            raise ConnectError('Unable to open UDP socket.')

    def create_cmd_msg(self):
        """ Constant fields """
        self.reserved1 = 0
        self.reserved2 = 0
        self.reserved3 = 0

        self.seq_num += 1
        seq_num = np.uint32(self.seq_num)
        pack_into('<I', self.send_buf, 0, seq_num)
        num_cmds = np.uint8(ARTESYN_COMMAND_TYPE_MSG << 7) | np.uint8(self.split << 5) | np.uint8(self.num_cmds)
        
        if  int(self.int_dev_addr) == 0:
            addr_seg = np.uint8(self.unit_addr << 4)
        else:
            addr_seg = np.uint8(self.unit_addr << 4) | np.uint8(mod_num_to_reg_addr_dict[int(self.int_dev_addr)])

        data_len_seg = np.uint8((self.read_write << 6) | np.uint8(self.cmd_data_len))
        cmd_seg = np.uint8(self.cmd_code)
        pack_into('>BBBB', self.send_buf, 4, num_cmds, addr_seg, data_len_seg, cmd_seg)
        if self.cmd_data_len > 0:
            offset = 8
            for i in range(0, self.cmd_data_len):
                pack_into('>B', self.send_buf, offset, np.uint8(self.cmd_data[i]))
                offset += 1
        
        return True

    def send_charger_cmd(self):
        """ Uncomment the following statement to get debug output of the message fields. """
        #self.log_cmd_msg()
        
        self.construct_udp_socket()
        ihp = (self.ip_addr, ARTESYN_CHASSIS_UDP_PORT)
        try:
            self.UDPSock.sendto(self.send_buf, ihp)
        except socket.error:
            self.UDPSock.close()
            raise CommunicationError(socket.error)
        self.UDPSock.close()

    def read_charger_response(self):
        if self.UDPSock == None:
            print("UDPSock None!!!")
        self.recv_buf, addr = self.UDPSock.recvfrom(1024)
        self.UDPSock.close()

        return True

    def check_response_error(self):
        hex_resp = bytearray(self.recv_buf)
#        seq_num = np.uint32(hex_resp[0])
#        if seq_num != self.seq_num:
        resp_seg = np.uint8(hex_resp[4])
        
        #print 'resp_seg 0x%02x' % resp_seg
        #err_seg = np.uint8(hex_resp[5])
        #print 'err_seg 0x%02x' % err_seg
        #addr_seg = np.uint8(hex_resp[6])
        #print 'addr_seg 0x%02x' % addr_seg
        #resp_data_len = np.uint8(hex_resp[7])
        #print 'resp_data_len 0x%02x' % resp_data_len
        #cmd_code = np.uint8(hex_resp[8])
        #print 'cmd_code 0x%02x' % cmd_code

        return resp_seg & ARTESYN_RESP_MSG_NORMAL_BIT_MASK
        
    def get_chassis_ip_addr(self, chassis_id=None):
        sql = 'SELECT %s from %s WHERE %s = %s;' % (AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME,
                                                    DB_AESS_CHARGER_CHASSIS_TABLE_NAME,
                                                    AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME,
                                                    "%s")
        args = [chassis_id,]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        self.ip_addr = response[AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME]
        return True

    def get_charger_vref(self, module=None):
        sql = "SELECT %s FROM %s WHERE %s = %s AND %s = %s;" % (ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                                DB_ARTESYN_CHARGER_DATA_TABLE_NAME,
                                                                ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                                "%s",
                                                                ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                                "%s")
        args = [module, self.chassis_id]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        if response is None:
            return None
        else:
            return response[ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME]
    
    def set_db_vref(self, vref=None, module=None):
        sql = "INSERT INTO %s (%s, %s, %s) VALUES (%s, %s, %s) " \
              "ON DUPLICATE KEY UPDATE %s=%s, %s=%s, %s=%s" % (DB_ARTESYN_CHARGER_DATA_TABLE_NAME,
                                                               ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                               ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                               ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                               "%s", "%s", "%s",
                                                               ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                               "%s",
                                                               ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                               "%s",
                                                               ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                               "%s")
        args = [vref, module, self.chassis_id, vref, module, self.chassis_id]
        prepared_act_on_database(EXECUTE, sql, args)
        
        return True
    
    def clear_cmd_data_fields(self):
        self.cmd_code = 0
        self.cmd_data_len = 0
        self.cmd_data = [0] * ARTESYN_CMD_DATA_BUF_LEN 

    def get_charger_model(self, module):
        sql = "SELECT %s from %s WHERE %s = %s AND %s = %s;" % (AESS_CHARGER_MODULE_MODEL_NUM_COLUMN_NAME,
                                                    DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                    AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME,
                                                    "%s",
                                                    AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                    "%s")
        args = [self.chassis_id, module]
        result = prepared_act_on_database(FETCH_ONE, sql, args)
        
        self.model_dict[module] = result[AESS_CHARGER_MODULE_MODEL_NUM_COLUMN_NAME]
                
    def _send_relay_cmd(self, cmd_str=None, param=None):
        success = False
        param_str = str(param)
        cmd = "|".join([cmd_str, param_str])
        try:
            pub_socket = self.zctx.socket(zmq.PUB)
        except ZMQError as error:
            pub_socket.close()
            raise ZmqError(error)

        pub_socket.setsockopt(zmq.LINGER, 0)
        pub_socket.hwm = 100

        pub_socket.connect(MSG_BROKER_XSUB_URL)

        time.sleep(PUB_CONNECT_WAIT_TIME)

        topic = "essid%d-relay" % 1 # JEP: FOR NOW self.ess_unit.id
        pub_socket.send_multipart([topic, cmd])
        pub_socket.close()

        return success

    """
    API command functions.
    """
    def chassis_up(self):
        if self.is_off():
            self.logger.info('chassis_up: ALREADY OFF ...')
            return False
        
        """ Remove write protect for chassis. """
        self.logger.debug('Remove write protect for chassis %d...' % int(self.chassis_id))
        self.write_protect(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM)
        
        """ Remove write protect for all charger modules in chassis. """
        self.logger.debug('Remove write protect for all charger modules in chassis %d...' % int(self.chassis_id))
        self.write_protect_module_all(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM)
            
        """ Enable chassis."""
        self.logger.debug('Enable chassis %d...' % int(self.chassis_id))
        self.operation(ARTESYN_ISOCOMM_ENABLE_PARAM)

        """ Disable all charger modules in the chassis. """
        self.operation_module_all(ARTESYN_ISOCOMM_DISABLE_PARAM)

    def operation(self, param=None):
        if self.is_off():
            self.logger.info('operation: ALREADY OFF ...')
            return False
        
        """
        The OPERATION command is used to toggle Global Module Enable/Disable.
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            raise ParameterError(error_str)
        if param == ARTESYN_ISOCOMM_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_ENABLE_CMD)
        elif param == ARTESYN_ISOCOMM_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_DISABLE_CMD)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OPERATION
        self.create_cmd_msg()

        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def operation_module(self, module_id=None, param=None):
        if self.is_off():
            self.logger.info('operation_module: ALREADY OFF ...')
            return False
        
        """
        The OPERATION command is used to toggle Global Module Enable/Disable.
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            raise ParameterError(error_str)
        if param == ARTESYN_ISOCOMM_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_ENABLE_CMD)
        elif param == ARTESYN_ISOCOMM_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_DISABLE_CMD)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        if module_id is None:
            raise ParameterError('Missing charger module number')

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_id)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OPERATION
        self.create_cmd_msg()

        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def operation_module_all(self, param=None):
        if self.is_off():
            self.logger.info('operation_module_all: ALREADY OFF ...')
            return False
        
        """ Enable/disable all charger modules in a chassis. """
        for module_id in self.charger_list:
            self.operation_module(module_id, param)
  
    def write_protect(self, param=None):
        if self.is_off():
            self.logger.info('write_protect: ALREADY OFF ...')
            return False
        
        """
        The WRITE_PROTECT command is used to control writing to the ISOCOMM.  The
        intent of this command is to provide protection against accidental changes.        
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            raise ParameterError(error_str)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_CMD_DATA)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_CMD_DATA)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        #print 'len = 0x%x' % len(self.cmd_data)
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_WRITE_PROTECT
        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def write_protect_module(self, module_id=None, param=None):
        if self.is_off():
            self.logger.info('write_protect_module: ALREADY OFF ...')
            return False
        
        if param is None:
            error_str = 'Missing write-protect enable/disable parameter'
            raise ParameterError(error_str)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_CMD_DATA)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_CMD_DATA)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)

        if module_id is None:
            raise ParameterError('Missing charger module number')
       
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_id)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_WRITE_PROTECT
        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def write_protect_module_all(self, param=None):
        if self.is_off():
            self.logger.info('write_protect_module_all: ALREADY OFF ...')
            return False
        
        """ Remove/set write protect for all charger modules in chassis. """
        for module_id in self.charger_list:
            self.write_protect_module(module_id, param)

    def clear_faults(self):
        """
        Standard PMBUS command to remove the warning or fault bits set in the status register.
        """
        if self.is_off():
            self.logger.info('clear_faults: ALREADY OFF ...')
            return False
        
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.num_cmds = 1
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_CLEAR_FAULTS

        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def clear_module_faults(self, module_num=None):
        """
        Standard PMBUS command to remove the warning or fault bits set in the status register.
        """
        if self.is_off():
            self.logger.info('clear_module_faults: ALREADY OFF ...')
            return False
        
        if module_num is None:
            raise ParameterError('Missing module number parameter')

        """ Initialize message. """
        self.num_cmds = 1
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_num)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_CLEAR_FAULTS

        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def set_vref(self, module=None, vref=None):
        if self.is_off():
            self.logger.info('set_vref: ALREADY OFF ...')
            return False
        
        if module is None:
            raise ParameterError('Missing module number parameter')
        if vref is None:
            raise ParameterError('Missing vref parameter')

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.clear_cmd_data_fields()
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module)
        self.read_write = ARTESYN_WRITE_CMD
        direct_data = pdu.float_to_direct(float(vref), ARTESYN_MODULE_CMD_CODE_VREF_SCALING_FACTOR)
        #print 'direct_data = 0x%x' % direct_data
        self.cmd_data_len = ARTESYN_MODULE_CMD_CODE_VREF_DATA_LEN + 1
        byte_mask = np.uint32(0xff)
        self.cmd_data[0] = ARTESYN_MODULE_CMD_CODE_VREF_DATA_LEN
        for i in range(1, self.cmd_data_len):
            cmd_data = (direct_data & byte_mask) >> ((i-1) * 8)
            #print 'mask = 0x%x, cmd_data = 0x%x np(cmd_data) = 0x%x' % (byte_mask, cmd_data, np.uint8(cmd_data))
            self.cmd_data[i] = np.uint8(cmd_data)
            #print 'self.cmd_data[%d] = 0x%x' % (i, self.cmd_data[i])
            byte_mask = byte_mask << 8
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_VREF
        self.create_cmd_msg()
        self.send_charger_cmd()
        self.set_db_vref(vref, module)

        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)
        
        return True
    
    def set_vref_chassis(self, vref=None):
        if self.is_off():
            self.logger.info('set_vref_chassis: ALREADY OFF ...')
            return False
        
        for module in self.charger_list:
            self.set_vref(module, vref)

    def set_output_current_level(self, module=None, required_output_current=None):
        """
        @summary: Command to set the output current level.
        @param:   module number, desired output current level.
        """
        # There were problems with this during development, this try block retained.
        try:
            self.logger.info('set_output_current_level(%d, %d)' % (int(module), float(required_output_current)))
        except Exception as error:
            self.logger.exception('set_output_current_level(%s, %s) - %s' % (str(module),
                                                                             str(required_output_current),
                                                                             repr(error)))
            return
        
        if module is None:
            raise ParameterError('Missing module number parameter')
        if required_output_current is None:
            raise ParameterError('Missing desired output current parameter')

        if float(required_output_current) == 0.0:
            if self.is_off():
                self.logger.info('set_output_current_level/0.0: ALREADY OFF ...')
                return True
            else:
                """ Disable (turn off) the charger module. """
                self.operation_module(module, ARTESYN_ISOCOMM_DISABLE_PARAM)
                return True
        try:
            vref = self.get_charger_vref(module)
        except Exception as error:
            self.logger.exception('get_charger_vref failed - %s' % (repr(error)))

        if vref is None:
            self.logger.warn('Attempt to set output current level without previously setting VREF')
            return
        if self.is_off():
            self.logger.info('set_output_current_level ALREADY OFF, WILL POWER UP ...')
            self.power_up()
            self.chassis_up()
            self.set_vref_chassis(vref)
        else:
            self.operation_module(int(module), ARTESYN_ISOCOMM_ENABLE_PARAM)

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module)
        self.read_write = ARTESYN_WRITE_CMD
        
        ''' Figure out what kind of charger we are dealing with. '''
        self.get_charger_model(module)
        if self.model_dict[int(module)] == ARTESYN_48V_MODULE_MODEL_NUM:
            Inominal = ARTESYN_48V_MODULE_NOMINAL_CURRENT
        else:
            Inominal = ARTESYN_80V_MODULE_NOMINAL_CURRENT

        """ Calculate new OC_FLM value from desired current level. """
        if vref <= ARTESYN_MODULE_NOMINAL_VOLTAGE:
            oc_flm = float(required_output_current) / Inominal
        else:
            oc_flm = (float(required_output_current) * float(vref)) / Inominal
        oc_flm = min(oc_flm, Inominal)

        # As a print, not particularly useful.
        # print 'Final OC_FAULT_LIMIT_MULTIPLIER for chassis %d module %d = %f' % (int(self.chassis_id), int(module), oc_flm)

        direct_data = pdu.float_to_direct(float(oc_flm), ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_SCALING_FACTOR)
        self.cmd_data_len = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_DATA_LEN + 1
        self.cmd_data[0] = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_DATA_LEN
        byte_mask = np.uint32(0xff)
        for i in range(1, self.cmd_data_len):
            cmd_data = (direct_data & byte_mask) >> ((i-1) * 8)
            #print 'cmd_data[%d] = 0x%02x' % (i, cmd_data)
            try:
                self.cmd_data[i] = np.uint8(cmd_data)
            except Exception as error:
                self.logger.exception('cmd_data index: %s' % repr(error))
            byte_mask = byte_mask << 8
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER
        self.create_cmd_msg()

        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)
        return True

    def chassis_disable(self):
        if self.is_off():
            self.logger.info('chassis_disable: ALREADY OFF ...')
            return False
        
        self.operation(self.chassis_id, ARTESYN_ISOCOMM_DISABLE_PARAM)
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def module_save(self, reg_addr=None):
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_code = ARTESYN_CMD_CODE_MODULE_SAVE
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(reg_addr)
        self.create_cmd_msg()
        self.send_charger_cmd()

        """
        Set timer to 1650ms, the observed completion time per charger module.
        A fully-stuffed chassis (8-modules) will take a little over 13 seconds
        to save its state.
        """
        time.sleep(1.650)

    def power_up(self):
        """
        This command steps the chassis through a power-up sequence.  First, the
        ChassisRelayDaemon is sent a command to turn power on to the unit.  Then,
        chassis_up() is called to initialize the chassis.  At this point, the 
        chassis should be ready to receive operational commands. 
        """
        self.logger.info('Sending power_on command to relay %s + 30-sec sleep ...' % (self.chassis_id))
        self._send_relay_cmd('power_on', self.chassis_id)
        self.logger.info('Sleep 30')
        ''' It takes a minimum of 25 seconds for the chassis to boot up. '''
        time.sleep(30.0)
        self.logger.info('Sleep over, chassis should be up ...')
        self.chassis_state = ArtesynControllerDaemon.CHASSIS_STATE_POWERED_ON

        self.chassis_up()
        
    def power_down(self):
        self._send_relay_cmd('power_off', self.chassis_id)
        return True
                
    def run(self):
        self.logger.info('YES, I AM UP & RUNNING ...')
        try:
            self.zctx = zmq.Context(1)
        except ZMQError as error:
            raise

        try:
            subscribe_str = 'chassis'
            self.server = self.zctx.socket(zmq.SUB)
            self.server.setsockopt(zmq.SUBSCRIBE, subscribe_str)
        except ZMQError as error:
            raise ZmqError(error)
        
        try:
            self.server.connect(MSG_BROKER_XPUB_URL)
        except ZMQError as error:
            self.server.close()
            raise ZmqError(error)

        self.chassis_id = 0
        poller = zmq.Poller()
        poller.register(self.server, zmq.POLLIN)
        
        while True:
            self.update()
            self.clear_cmd_data_fields()
            
            try:
                zsocks = dict(poller.poll(ArtesynControllerDaemon.SOCKET_TIMEOUT))
            except zmq.ZMQError as error:
                str_err = repr(error)
                self.logger.exception(error)
                continue

            if self.server in zsocks and zsocks[self.server] == zmq.POLLIN:
                try:
                    data = self.server.recv()
                except zmq.ZMQError as error:
                    self.logger.exception(error)
                    continue
                self.logger.info('Received command: %s' % data)

                """
                We can receive either a list or a dictionary from our fans.
                """
                command_list = data.split("|")
                if len(command_list) == 1:
                    # Expecting chassis specification. Must be a digit.
                    command = command_list[0]
                    if "chassis" in command:
                        # Strip off "chassis" from command.
                        command = command.strip("chassis")
                        # Strip off the trailing # symbol
                        command = command.strip("#")
                        if command.isdigit():
                            self.chassis_id = int(command)
                            self.logger.info('Charger commands based on chassis ID %s' % self.chassis_id)
                        else:
                            self.logger.info('Expected chassis ID not present')
                    else:
                        self.logger.info('Expected chassis specification not present')

                elif len(command_list) == 2:
                    command = command_list[0]
                    param_str = command_list[1]
                    param_list = param_str.split(" ")
                    self.get_chassis_ip_addr(self.chassis_id)
                    self.logger.info('IP FROM CHASSIS ID %s = %s' % (self.chassis_id, self.ip_addr))

                    try:
                        if param_list is 'None':
                            self.controller_command_dict[command]()
                        elif len(param_list) == 1:
                            self.controller_command_dict[command]()
                        elif len(param_list) == 2:
                            self.controller_command_dict[command](param_list[1])
                        elif len(param_list) == 3:
                            self.controller_command_dict[command](param_list[1], param_list[2])
                    except KeyError as error:
                        command = 'Invalid key: %s' % repr(error.message)
                    except (ConnectError, DatabaseError, ParameterError, WriteError) as error:
                        command = repr(error.message)
                    except TypeError as error:
                        command = 'TypeError: %s' % repr(error.message)
                    except Exception as error:
                        command = 'Exception: %s' % repr(error.message)

                else:
                    self.logger.info('Received invalid command length %s' % len(command_list))


        self.server.close()


if __name__ == '__main__':
    ac = ArtesynControllerDaemon()
    ac.run()

