#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
@module: ArtesynChassisRelayDaemon.py

@description:

@reference: ET‐7x00/PET‐7x00 Series Register Table
            2013 by ICP DAS CO., LTD

@copyright: Apparent Energy Inc.  2018

@created: Nov 12, 2018

@author: steve
'''
from lib.logging_setup import *
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.ArtesynConstants import *
from pymodbus.constants import Defaults
from ess.ess_daemon import EssDaemon
import zmq
from zmq.backend.cython.utils import ZMQError
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XPUB_URL
from lib.exceptions import *

CHASSIS_RELAY_MODBUS_PORT = 502
CHASSIS_RELAY_MODBUS_UNIT = 1
CHASSIS_RELAY_MAX_MODBUS_ERRORS = 10

CHASSIS_RELAY_RELAY_0_REG_ADDR = 0x0
CHASSIS_RELAY_RELAY_1_REG_ADDR = 0x1
CHASSIS_RELAY_RELAY_2_REG_ADDR = 0x2

MAX_ZMQ_ERRORS = 5


class ArtesynChassisRelayDaemon(EssDaemon):
    relay_number_to_address = [CHASSIS_RELAY_RELAY_0_REG_ADDR,
                               CHASSIS_RELAY_RELAY_1_REG_ADDR,
                               CHASSIS_RELAY_RELAY_2_REG_ADDR
                              ]

    def __init__(self, **kwargs):
        super(ArtesynChassisRelayDaemon, self).__init__(**kwargs)
        #self.logger = setup_logger(__name__)
        self.command_dict = {'power_off' : self.power_off, 'power_on' : self.power_on }
        self.zmq_errors = 0
        self.topic = "essid%d-relay" % self.ess_unit.id

    def client_connect(self, relay_ip_addr=None):
        '''
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of the caller of this
               function to close the connection.
        '''
        modbus_errs = 0
        Defaults.Timeout = 0.75
        while True:
            try:
                self.client = ModbusClient(relay_ip_addr,
                                           port=CHASSIS_RELAY_MODBUS_PORT)
            except ValueError as error:
                modbus_errs += 1
                err_string = "Unable to create MODbus client at Netprotector ip address %s - %s" % \
                             (CHASSIS_RELAY_MODBUS_UNIT, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= CHASSIS_RELAY_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    raise ConnectError('Maximum MODbus errors exceeded trying to connect')
                continue
            break

        modbus_errs = 0
        while True:
            if not self.client.connect():
                err_string = "MODbus connect failed for Artysen netprotector unit at %s" % relay_ip_addr
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CHASSIS_RELAY_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus connection errors exceeded.')
                    self.client.close()
                    raise ConnectError('Maximum MODbus errors exceeded trying to connect')
                continue
            break
        return True

    def write_relay_register(self, relay_num=None, value=None):
        '''
        Perform a MODbus write_coil to set/reset a single relay on the PET-7002.
        '''
        if value is None:
            raise ParameterError('Missing charger module setting')

        try:
            self.client.write_coil(ArtesynChassisRelayDaemon.relay_number_to_address[relay_num],
                                   value,
                                   unit=ARTESYN_MODBUS_UNIT)
        except Exception as error:
            raise CommunicationError(error)
        finally:
            self.client.close()
    
    def create_zmq_context(self):
        self.zmq_errors = 0
        while True:
            try:
                self.zctx = zmq.Context()
            except ZMQError as error:
                self.zmq_errors += 1
                self.logger.exception('Failed to instantiate ZMQ context object - %s' % error)
                if self.zmq_errors < MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to instantiate ZMQ context object for the Relay CLI')
            else:
                break

    def delete_zmq_context(self):
        if self.zctx is not None:
            self.zctx.close()
            self.zctx.term()

    def create_zmq_objects(self):
        self.zmq_errors = 0
        while True:
            try:
                self.subscriber = self.zctx.socket(zmq.SUB)
                self.subscriber.setsockopt(zmq.LINGER, 0)
            except ZMQError as error:
                self.zmq_errors += 1
                self.logger.exception('Failed to create ZMQ socket - %s' % error)
                if self.zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ socket for the Chassis Relay daemon')
                else:
                    continue
            try:
                self.subscriber.connect(MSG_BROKER_XPUB_URL)
                self.subscriber.setsockopt(zmq.SUBSCRIBE, self.topic)
            except ZMQError as error:
                zerr = repr(error)
                self.logger.error(zerr)
                self.zmq_errors += 1
                self.subscriber.close()
                self.logger.exception('Failed to create ZMQ socket - %s' % error)
                if self.zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ socket for the Chassis Relay daemon')
                else:
                    continue
            try:
                self.poller = zmq.Poller()
            except ZMQError:
                self.zmq_errors += 1
                self.subscriber.close()
                self.logger.exception('Failed to create ZMQ poller object.')
                if self.zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to create ZMQ poller object for the Chassis Relay daemon')
                else:
                    continue
            try:
                self.poller.register(self.subscriber, zmq.POLLIN)
            except ZMQError:
                self.zmq_errors += 1
                self.subscriber.close()
                self.poller.unregister(self.subscriber)
                self.logger.exception('Failed to register ZMQ poller')
                if self.zmq_errors > MAX_ZMQ_ERRORS:
                    raise ZmqError('Failed to register ZMQ poller for Chassis Relay daemon')
                else:
                    continue

            break
        
    def delete_zmq_objects(self):
        self.logger.debug('Deleting ZMQ objects...')
        if self.poller is not None:
            self.poller.unregister(self.subscriber)
        if self.subscriber is not None:
            self.subscriber.close()

    def get_relay_info(self, chassis_id=None):
        sql = "SELECT %s, %s FROM %s WHERE %s = %s;" % (AESS_CHARGER_CHASSIS_RELAY_IP_ADDR_COLUMN_NAME,
                                                        AESS_CHARGER_CHASSIS_RELAY_NUM_COLUMN_NAME,
                                                        DB_AESS_CHARGER_CHASSIS_TABLE_NAME,
                                                        AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME,
                                                        "%s")
        args = [chassis_id,]
        results = prepared_act_on_database(FETCH_ONE, sql, args)

        if results is None:
            return None, None
            
        return results[AESS_CHARGER_CHASSIS_RELAY_IP_ADDR_COLUMN_NAME], \
               results[AESS_CHARGER_CHASSIS_RELAY_NUM_COLUMN_NAME]
    
    def set_chassis_status(self, chassis_id=None, status=None):
        sql = "UPDATE %s set %s = %s where %s = %s;" % (DB_AESS_CHARGER_CHASSIS_TABLE_NAME,
                                                   AESS_CHARGER_CHASSIS_STATUS_COLUMN_NAME,
                                                   "%s",
                                                   AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME,
                                                   "%s")
        args = [status, chassis_id]
        self.logger.info(sql % (status, chassis_id))
        prepared_act_on_database(EXECUTE, sql, args)

    def setup_write(self, chassis_id=None):
        relay_ip_addr, relay_num = self.get_relay_info(chassis_id)

        if relay_ip_addr is None or relay_num is None:
            raise ParameterError('No entry for chassis_id = %s' % chassis_id) 
        
        self.logger.info('Using relay %s, %s' % (relay_ip_addr, relay_num))
        self.client_connect(relay_ip_addr)

        return relay_num

    def power_off(self, chassis_id=None):
        self.logger.info('Powering off chassis %s' % chassis_id)
        relay_num = self.setup_write(chassis_id)

        self.write_relay_register(relay_num, ARTESYN_RELAY_OFF)
        self.set_chassis_status(chassis_id, ARTESYN_CHASSIS_STATE_POWERED_OFF)

        self.client.close()
        
    def power_on(self, chassis_id=None):
        self.logger.info('Powering on chassis %s' % chassis_id)
        relay_num = self.setup_write(chassis_id)

        self.write_relay_register(relay_num, ARTESYN_RELAY_ON)
        self.set_chassis_status(chassis_id, ARTESYN_CHASSIS_STATE_POWERED_ON)

        self.client.close()

    def run(self):
        self.create_zmq_context()
        self.create_zmq_objects()

        '''
        Main monitor loop.
        '''
        while True:
            self.update()
            try:
                zsocks = dict(self.poller.poll(self.polling_freq_sec * 1000))
            except ZMQError as error:
                err_str = repr(error)
                self.logger.exception(err_str)
                self.delete_zmq_objects()
                self.create_zmq_objects()
                continue

            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                [topic, request] = self.subscriber.recv_multipart()
                command, chassis_id = request.split("|")
                try:
                    if chassis_id is None:
                        self.logger.error('Missing chassis_id parameter')
                        continue
                    else:
                        self.command_dict[command](chassis_id)
                except (KeyError, ConnectError, ParameterError, WriteError, DatabaseError) as error:
                    err_str = repr(error.message)
                    self.logger.error(err_str)
                    continue

        self.delete_zmq_objects()
        self.delete_zmq_context()
        
    @staticmethod
    def find(feed_name):
        sql = "SELECT * FROM artysen_netprotector_daemon_control"
        result = prepared_act_on_database(FETCH_ONE, sql, [])
        if result:
            return ArtesynChassisRelayDaemon(**result)
        else:
            return None


if __name__ == '__main__':
    d = ArtesynChassisRelayDaemon(id=1)
    d.run()



