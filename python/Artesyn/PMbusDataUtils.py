'''
@module: PMbusDataUtils.py

@copyright: Apparent Energy Inc.  2018

@creared: July 19, 2018

@author: steve
'''
import numpy as np
import math

    
'''
@summary: convert a LinearFloat5_11 formatted uint16 into a floating point value
'''
def lf5_11_to_float(ldf_value=None):
    if ldf_value is None:
        return float(0)

    exponent = np.int8(np.uint16(ldf_value) >> 11) # extract exponent as MS 5 bits
    mantissa = np.int16(np.uint16(ldf_value)) & 0x7ff # extract mantissa as LS 11 bits
    if (exponent > 0x0F):
        exponent |= 0xE0        # sign extend exponent
    if (mantissa > 0x03FF):
        mantissa |= 0xF800     # sign extend mantissa

    exp8 = np.int8(exponent)
    #print 'exponent, mantissa', exp8, np.int16(mantissa)
    return float(mantissa * pow(float(2), exp8)) # compute value as mantissa * 2^(exponent)

'''
@summary: convert a float value to LinearFloat5_11 Format.
'''
def float_to_lf5_11(float_value=None):
    if float_value is None:
        return np.uint16(0)

    exponent = int(-16)
    mantissa = int(float_value / pow(2, exponent))
    while exponent < 15:
        if (mantissa >= -1024) and (mantissa <= 1023):
            break
        exponent += 1
        mantissa = int(float_value / pow(2, exponent))

    uexp = exponent << 11
    uman = np.uint16(mantissa) & 0x07ff
    return np.uint16(uexp) | np.uint16(uman)

'''
@summary: convert a float to a Linear Direct format.
'''
def float_to_direct(float_value=None, scaling_factor=None):
    direct_value = int((float_value * scaling_factor))
    return direct_value

'''
@summary: convert a Linear Direct format to a float.
'''
def direct_to_float(direct_value=None, scaling_factor=None):
    float_value = float(direct_value) * (1.0/scaling_factor)
    return float_value

'''
@summary: convert an integer to a three-byte array (MSB is not converted.).
'''
def int_to_be_3_bytes(n):
    ''' MSB is not converted in this function. '''
    b = bytearray([0, 0, 0])   # init
    b[2] = n & 0xFF
    n >>= 8
    b[1] = n & 0xFF
    n >>= 8
    b[0] = n & 0xFF
    #n >>= 8
    #b[0] = n & 0xFF    # MSB
    
    return b

def int_to_le_3_bytes(n):
    b = bytearray([0, 0, 0])   # init
    b[0] = n & 0xFF
    n >>= 8
    b[1] = n & 0xFF
    n >>= 8
    b[2] = n & 0xFF
    
    return b

def reverse_bits(value, num_bits):
    result = 0
    for i in range(num_bits):
        result <<= 1
        result |= value & 1
        value >>= 1
    return result

if __name__ == '__main__':
    w = 50.0
    z = float_to_lf5_11(w)
    print 'float_to_lf5_11(%f) = 0x%x' % (w, z)
    y = lf5_11_to_float(z)
    print 'lf5_11_to_float(0x%x) = %f' % (z, y)
    
    a = 0x98968
    b = direct_to_float(a, 10000)
    print 'direct_to_float(0x%x) = %f' % (a, b)
    c = float_to_direct(b, 10000)
    print 'float_to_direct(%f) = 0x%x' % (b, c)
    
    h = float_to_direct(w, 10000)
    print 'float_to_direct(%f) = 0x%x' % (w, h)
    i = direct_to_float(h, 10000)
    print 'direct_to_float(0x%x) = %f' % (h, i)
    
    ftd = float_to_direct(w, 10000)
    boofer = int_to_be_3_bytes(ftd)
    big_end_arr = np.ndarray(shape=(3,), dtype='>u1', buffer=boofer)
    print 'big_end_arr [0] = 0x%x, [1] = 0x%x, [2] = 0x%x' % (big_end_arr[0], big_end_arr[1], big_end_arr[2])
    print big_end_arr
    doofer = int_to_le_3_bytes(ftd)
    little_end_arr = np.ndarray(shape=(3,), dtype='<u1', buffer=doofer)
    print 'little_end_arr [0] = 0x%x, [1] = 0x%x, [2] = 0x%x' % (little_end_arr[0], little_end_arr[1], little_end_arr[2])
    


    
    