import datetime

from ess.ess_daemon import EssDaemon

class ArtesynChassisRelayTimeoutDaemon(EssDaemon):

    def __init__(self, **kwargs):
        super(ArtesynChassisRelayTimeoutDaemon, self).__init__(**kwargs)
        self.timeout_delta = datetime.timedelta(seconds=120)
        self.last_active_charging_time = None
        self.auto_shutdown_behavior = self.ess_unit.auto_shutdown_behavior

    def active_charging_timed_out(self):
        if self.last_active_charging_time is None:
            return True
        return datetime.datetime.now() > self.last_active_charging_time + self.timeout_delta

    def work(self):
        self.logger.info('auto_shutdown_behavior=%s' % self.auto_shutdown_behavior)
        if self.ess_unit.actively_charging():
            self.logger.debug("Actively charging...")
            self.last_active_charging_time = datetime.datetime.now()
        if self.active_charging_timed_out():
            if self.auto_shutdown_behavior is False:
                self.logger.info("Enabling charger chassis auto-shutdown behavior.")
                self.auto_shutdown_behavior = True
                self.ess_unit.save(auto_shutdown_behavior=self.auto_shutdown_behavior)
        else:
            if self.auto_shutdown_behavior is True:
                self.logger.info("Disabling charger chassis auto-shutdown behavior.")
                self.auto_shutdown_behavior = False
                self.ess_unit.save(auto_shutdown_behavior=self.auto_shutdown_behavior)
            
        
