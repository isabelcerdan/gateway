"""
@module: Artesyn Data Publisher Factory

@description: 

@copyright: Apparent Energy Inc. 2018

@reference: 

@author: steve

@created: September 12, 2018
"""
import socket
import time
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_ARTESYN_DATA_PUBLISHER_FACTORY_RESTARTED, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VOUT_OV_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_IOUT_OV_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VIN_UV_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_TEMPERATURE_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_OFF_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_NONE_OF_THE_ABOVE_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_UNSUPPORTED_CMD, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_INVALID_DATA, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PACKET_ERROR, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_MEMORY_FAULT, \
                                  ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PROCESSOR_FAULT
from lib.gateway_constants.ArtesynConstants import *
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
from lib.db import prepared_act_on_database, EXECUTE, FETCH_ONE
import numpy as np
import threading
import zmq
from zmq.backend.cython.utils import ZMQError
from lib.gateway_constants.ZmqConstants import *
import timeit
import json
import ctypes
from ess.ess_daemon import EssDaemon
from lib.exceptions import *


ARTESYN_DEFAULT_POLLING_FREQ = 1  # second
ARTESYN_MAX_DB_ERRORS = 5
ARTESYN_MAX_ZMQ_ERRORS = 5
ARTESYN_CONNECT_WAIT_TIME = 5.0


""" Global operating dictionary definitions """
xlat_status_bit_to_alarm_id = { ARTESYN_STATUS_BYTE_NONE_OF_THE_ABOVE_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_NONE_OF_THE_ABOVE_FAULT,
                                ARTESYN_STATUS_BYTE_CML_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_FAULT,
                                ARTESYN_STATUS_BYTE_TEMPERATURE_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_TEMPERATURE_FAULT,
                                ARTESYN_STATUS_BYTE_VIN_UV_FAULT_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VIN_UV_FAULT,
                                ARTESYN_STATUS_BYTE_IOUT_OV_FAULT_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_IOUT_OV_FAULT,
                                ARTESYN_STATUS_BYTE_VOUT_OV_FAULT_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VOUT_OV_FAULT,
                                ARTESYN_STATUS_BYTE_OFF_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_OFF_FAULT,
                                ARTESYN_STATUS_BYTE_BUSY_BIT : None
                              }
xlat_cml_bit_to_alarm_id = { ARTESYN_STATUS_CML_UNSUPPORTED_CMD_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_UNSUPPORTED_CMD,
                             ARTESYN_STATUS_CML_INVALID_DATA_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_INVALID_DATA,
                             ARTESYN_STATUS_CML_PACKET_ERROR_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PACKET_ERROR,
                             ARTESYN_STATUS_CML_MEMORY_FAULT_BIT : ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_MEMORY_FAULT,
                             ARTESYN_STATUS_CML_PROCESSOR_FAULT_BIT : None,
                             ARTESYN_STATUS_CML_RESERVED_BIT : None,
                             ARTESYN_STATUS_CML_OTHER_COMMUNICATION_FAULT_BIT : None,
                             ARTESYN_STATUS_CML_OTHER_MEMORY_FAULT_BIT : None
                           }
xlat_alarm_id_to_log_msg = { ALARM_ID_ARTESYN_CHARGER_CONTROLLER_NONE_OF_THE_ABOVE_FAULT : 'None of the above fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_FAULT : 'CML fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_TEMPERATURE_FAULT : 'Temperature fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VIN_UV_FAULT : 'VIN undervoltage fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_IOUT_OV_FAULT : 'IOUT overvoltage fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_VOUT_OV_FAULT : 'VOUT overvoltage fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_OFF_FAULT : 'Module OFF fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_UNSUPPORTED_CMD : 'CML Unsupported command on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_INVALID_DATA : 'CML Invalid data on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PACKET_ERROR : 'CML packet error on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_MEMORY_FAULT : 'CML memory fault on module %d',
                             ALARM_ID_ARTESYN_CHARGER_CONTROLLER_CML_PROCESSOR_FAULT : 'CML processor fault on module %d'
                           }

reg_addr_to_mod_num_dict = { ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1 : 1,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2 : 2,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3 : 3,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4 : 4,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5 : 5,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6 : 6,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7 : 7,
                             ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8 : 8
                           }

mod_num_to_reg_addr_dict = { 1 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1,
                             2 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2,
                             3 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3,
                             4 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4,
                             5 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5,
                             6 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6,
                             7 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7,
                             8 : ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8
                           }


class SignalExit(Exception):
    """
        Exception raised when a signal is caught

        Attributes:
            message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ArtesynDataPublisherThread(threading.Thread):
    def __init__(self, polling_freq_sec, chassis_id=None, **kwargs):
        self.ess_unit = kwargs.pop('ess_unit')
        self.logger = setup_logger('ArtesynDataPubThr')
        threading.Thread.__init__(self, **kwargs)
        self.chassis_id = chassis_id
        self.db_errors = 0
        self.seq_num = 0
        self.header = 0
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.reserved1 = 0
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.reserved2 = 0
        self.num_cmds = None
        self.unit_addr = None
        self.int_dev_addr = None
        self.reserved3 = 0
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_data = []
        self.cmd_code = None
        self.send_buf = ctypes.create_string_buffer(16)
        self.recv_buf = ctypes.create_string_buffer(16)
        self.polling_freq_sec = polling_freq_sec

        self.UDPSock = None
        self.construct_udp_socket()

    def construct_udp_socket(self):
        self.logger.info('Constructing UDP socket...')
        try:
            self.UDPSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.UDPSock.settimeout(3.0)
        except socket.error:
            self.logger.exception('Unable to open UDP socket.')
            raise ConnectError('Unable to open UDP socket.')

    def test_bit(self, value=None, bit_pos=None):
        return (value & (1 << bit_pos))

    def set_ess_fault_state(self):
        sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                       ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME, 'Fault',
                                                       ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, [])

    def send_charger_cmd(self, ip_addr=None):
        ihp = (ip_addr, ARTESYN_CHASSIS_UDP_PORT)
        try:
            self.UDPSock.sendto(self.send_buf, ihp)
        except socket.error:
            self.UDPSock.close()
            self.logger.exception('Socket error sending command to chassis %s' % ihp[0])
            raise CommunicationError(socket.error)

    def get_chassis_ip_addr(self, chassis_id=None):
        sql = 'SELECT %s from %s WHERE %s = %s;' % (AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME,
                                                    DB_AESS_CHARGER_CHASSIS_TABLE_NAME,
                                                    AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME,
                                                    "%s")
        args = [chassis_id,]
        try:
            response = prepared_act_on_database(FETCH_ONE, sql, args)
        except Exception as error:
            self.logger.exception('Failed to retrieve chassis IP address')
            raise DatabaseError(error)
        return response[AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME]

    def clear_cmd_data_fields(self):
        self.cmd_code = 0
        self.cmd_data_len = 0
        self.cmd_data = [] 

    def read_charger_response(self):
        try:
            self.recv_buf, addr = self.UDPSock.recvfrom(1024)
        except socket.timeout:
            try:
                self.UDPSock.close()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError('Error closing UDP socket')
            self.logger.exception('Socket error receiving response from chassis')
            self.construct_udp_socket()
            self.logger.warning('Recovering from socket timeout.')
            raise CommunicationError('Socket timeout error on recvfrom()')
        
        return True
    
    def check_response_error(self):
        hex_resp = bytearray(self.recv_buf)
        seq_num = np.uint32(hex_resp[0])
        if seq_num != self.seq_num:
            self.logger.warning('Mismatched sequence number response')
        resp_seg = np.uint8(hex_resp[4])
        print 'resp_seg 0x%02x' % resp_seg
        err_seg = np.uint8(hex_resp[5])
        print 'err_seg 0x%02x' % err_seg
        addr_seg = np.uint8(hex_resp[6])
        print 'addr_seg 0x%02x' % addr_seg
        resp_data_len = np.uint8(hex_resp[7])
        print 'resp_data_len 0x%02x' % resp_data_len
        cmd_code = np.uint8(hex_resp[8])
        print 'cmd_code 0x%02x' % cmd_code

        return resp_seg & ARTESYN_RESP_MSG_NORMAL_BIT_MASK
        
    def interpret_status_byte(self):
        pad_array = bytearray(41-len(bytearray(self.recv_data)))
        msg_array = bytearray(self.recv_data)
        msg_array.extend(pad_array)
        try:
            resp_msg = ArtesynRespMsg.from_buffer(msg_array)
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError('Failed to parse command response')
        for bit in range(0, NUM_ARTESYN_STATUS_BYTE_BITS):
            if self.test_bit(resp_msg.resp_data[0], bit):
                if xlat_status_bit_to_alarm_id[bit] is None:
                    continue
                self.logger.warning(xlat_alarm_id_to_log_msg[xlat_status_bit_to_alarm_id[bit]] % \
                               reg_addr_to_mod_num_dict[self.cmd_msg.int_dev_addr])
                Alarm.occurrence(xlat_status_bit_to_alarm_id[bit])
                
    def interpret_status_cml(self):
        pad_array = bytearray(41-len(bytearray(self.recv_data)))
        msg_array = bytearray(self.recv_data)
        msg_array.extend(pad_array)
        try:
            resp_msg = ArtesynRespMsg.from_buffer(msg_array)
        except Exception as error:
            self.logger.exception(error)
            raise ProcessingError('Failed to parse command response')

        for bit in range(0, NUM_ARTESYN_STATUS_BYTE_BITS):
            if self.test_bit(resp_msg.resp_data[0], bit):
                if xlat_cml_bit_to_alarm_id[bit] is None:
                    continue
                self.logger.warning(xlat_alarm_id_to_log_msg[xlat_cml_bit_to_alarm_id[bit]] % \
                               reg_addr_to_mod_num_dict[self.cmd_msg.int_dev_addr])
                Alarm.occurrence(xlat_cml_bit_to_alarm_id[bit])
                
    def read_status_byte(self, chassis_id=None, module_id=None):
        if chassis_id is None:
            error_str = 'Missing chassis_id parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)

        if module_id is None:
            raise ParameterError('Missing charger module number')

        ip_addr = self.get_chassis_ip_addr(chassis_id)

        self.cmd_msg.num_cmds = 1
        self.cmd_msg.cmd_code = ARTESYN_CMD_CODE_STATUS_BYTE
        self.cmd_msg.cmd_data_len = 0
        self.cmd_msg.read_write = ARTESYN_READ_CMD
        self.cmd_msg.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.cmd_msg.int_dev_addr = mod_num_to_reg_addr_dict[int(module_id)]
        self.send_charger_cmd(ip_addr)

    def read_status_cml(self, chassis_id=None, module_id=None):
        if chassis_id is None:
            error_str = 'Missing chassis_id parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)

        if module_id is None:
            raise ParameterError('Missing charger module number')

        ip_addr = self.get_chassis_ip_addr(chassis_id)
        self.cmd_msg.num_cmds = 1
        self.cmd_msg.cmd_code = ARTESYN_CMD_CODE_STATUS_CML
        self.cmd_msg.cmd_data_len = 0
        self.cmd_msg.read_write = ARTESYN_READ_CMD
        self.cmd_msg.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.cmd_msg.int_dev_addr = mod_num_to_reg_addr_dict[int(module_id)]
        self.send_charger_cmd(ip_addr)

    def monitor_isocomm(self):
        for cmd_code, func in self.isocomm_command_dict.iteritems():
            func(cmd_code)

    def proto_monitor_isocomm(self, chassis_id=None):
        self.cmd_msg.cmd_code = ARTESYN_CMD_CODE_MODULE_DETECTION
        try:
            self.send_charger_cmd()
        except:
            raise CommandError('Failed to send command to Artesyn unit')

        recv_data = ARMUnion()
        try:
            recv_data, addr = self.UDPSock.recvfrom(1024)
        except socket.timeout:
            self.logger.exception('Timeout on UDP receive socket for message %d' %
                             self.cmd_msg.seq_num)
            raise ConnectError('No response to charger command')
        else:
            print [hex(ord(c)) for c in recv_data]
            print 'recv data len', len(recv_data)
            print 'recv_data:', type(recv_data)
            recv_list = list(recv_data)
            for c in range(0,32):
                recv_list.append(0x0)
            print 'recv_list len:', len(recv_list)
            recv_msg = ArtesynRespMsg.from_buffer(bytearray(recv_list))
            print 'cmd_seq_num = 0x%x' % recv_msg.seq_num
            print 'type = ', (recv_msg.type == 1)
            print 'error = 0x%x' % recv_msg.error
            print 'final = ', (recv_msg.final == 1)
            print 'num_resp = 0x%x' % recv_msg.num_resp
            print ''
            print 'resp_status = 0x%x' % recv_msg.resp_status
            print 'cmd_seq_num = 0x%x' % recv_msg.cmd_seq_num
            print 'ihp_unit_addr = 0x%x' % recv_msg.ihp_unit_addr
            print 'int_dev_addr = 0x%x' % recv_msg.int_dev_addr
            print 'resp_data_len = 0x%x' % recv_msg.resp_data_len
            print 'cmd_code = 0x%x' % recv_msg.cmd_code

            print 'recv_msg.data', type(recv_msg.data)
            for c in range(0, recv_msg.resp_data_len):
                print 'data[%d] = 0x%x' % (c, recv_msg.data[c])

    def monitor_modules(self):
        pass
    
    def monitor(self, chassis_id=None):
        self.proto_monitor_isocomm(chassis_id)
        #self.monitor_isocomm()
        #self.monitor_pfc()
        #self.monitor_modules()
        
    def run(self):
        self.logger.info("Artesyn Data Publisher thread chassis%d starting..." % self.chassis_id)

        try:
            self.ctx = zmq.Context()            
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        """
        Socket to send messages to the Gateway Broker.
        """
        connect_err_cnt = 0
        while True:
            try:
                self.pub_socket = self.ctx.socket(zmq.PUB)
                self.pub_socket.connect(MSG_BROKER_XSUB_URL)

                time.sleep(PUB_CONNECT_WAIT_TIME)

            except Exception as error:
                connect_err_cnt += 1
                if connect_err_cnt <= ARTESYN_MAX_ZMQ_ERRORS:
                    error_string = 'Unable to create TCP send socket for BMU data - %s' \
                                    % repr(error)
                    logging.error(error_string)
                    self.pub_socket.close()
                    self.ctx.term()
                    raise ConnectError(error_string)
                time.sleep(ARTESYN_CONNECT_WAIT_TIME)            
            else:
                break

        self.get_chassis_list()

        """
        Main monitor loop.
        """
        topic_base = 'chassis'
        while True:
            for entry in self.chassis_list:
                start_time = timeit.default_timer()
                self.monitor(entry)
                elapsed = timeit.default_timer() - start_time
                self.logger.info('elapsed time %s' % elapsed)

            if self.register_contents_dict is not None:
                try:
                    topic = "%s%s#" % (topic_base , str(self.chassis_id))
                    self.pub_socket.send_multipart([topic, json.dumps(self.register_contents_dict)])
                except Exception as error:
                    self.logger.exception(error)
                    continue
                except Exception as error:
                    self.logger.exception(error)
                    raise ProcessingError(error)
                self.register_contents_dict.clear()

        self.pub_socket.close()    
        self.ctx.close()
        self.ctx.term()        
        self.UDPSock.close()
        

class ArtesynDataPublisherFactoryDaemon(EssDaemon):
    polling_freq_sec = ARTESYN_DEFAULT_POLLING_FREQ
    alarm_id = ALARM_ID_ARTESYN_DATA_PUBLISHER_FACTORY_RESTARTED

    def __init__(self, **kwargs):
        super(ArtesynDataPublisherFactoryDaemon, self).__init__(**kwargs)
        self.chassis_id_list = []
        self.thread_list = []
        self.db_errors = 0
        self.thread_list = []
        
    def get_chassis_ids(self):
        sql = 'SELECT %s FROM %s;' % (AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME, DB_AESS_CHARGER_CHASSIS_TABLE_NAME)
        args = []
        responses = prepared_act_on_database(FETCH_ALL, sql, args)
        resp_dict_list = list(responses)
        for resp_dict in resp_dict_list:
            self.chassis_id_list.append(resp_dict['chassis_id'])
        
    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread__stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def run(self):
        self.update()
        
        """ Get configured chassis ids. """
        try:
            self.get_chassis_ids()
        except:
            raise

        """ Create the threads for all the chassis. """
        for chassis_id in self.chassis_id_list:
            thread_name = 'chassis%d' % chassis_id
            thread = ArtesynDataPublisherThread(self.polling_freq_sec,
                                                chassis_id,
                                                name=thread_name,
                                                ess_unit=self.ess_unit)
            try:
                self.logger.debug('Starting data publisher thread chassis%d' % chassis_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)

        """
        @note: The factory joins a thread in its list every second, taking approximately
               30 seconds to process the list.
        """
        while True:
            self.update()
            
            try:
                for thread in self.thread_list:
                    thread_name = ''
                    try:
                        thread_name = thread.getName() 
                        thread.join(1.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('ArtesynDataPublisher thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        chassis_id = int(thread_name[7:])
                        thread = ArtesynDataPublisherThread(
                                        self.polling_freq_sec,
                                        chassis_id,
                                        name=thread_name,
                                        ess_unit=self.ess_unit)
                        try:
                            self.logger.debug('Restarting data publisher thread %s' % thread_name)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                break
                
        self.logger.info('ArtesynDataPublisherFactory fell through main loop')
        return False
