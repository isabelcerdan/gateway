#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: ArtesynControllerFactory.py

@description: Create a thread-per-chassis environment for processing charger commands.

@created: October 16, 2018

@copyright: Apparent Energy Inc.  2018

@author: steve
"""
import time
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
from contextlib import closing
from lib.gateway_constants.ZmqConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.EveBmuConstants import *
import json
from lib.logging_setup import *
from alarm.alarm_constants import ALARM_ID_ARTESYN_CHARGER_CONTROLLER_RESTARTED
from lib.gateway_constants.ArtesynConstants import *
import zmq
from zmq.backend.cython.utils import ZMQError
import socket
import numpy as np
import PMbusDataUtils as pdu
from struct import *
import ctypes
import threading
from ess.ess_daemon import EssDaemon
from lib.exceptions import *


SUBSCRIBER_SOCKET_TIMEOUT = 5000 #ms

mod_num_to_reg_addr_dict = {
    1: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1,
    2: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2,
    3: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3,
    4: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4,
    5: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5,
    6: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6,
    7: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7,
    8: ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8
}

reg_addr_to_mod_num_dict = {
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_1: 1,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_2: 2,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_3: 3,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_4: 4,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_5: 5,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_6: 6,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_7: 7,
    ARTESYN_INTERNAL_DEVICE_ADDR_MODULE_8: 8
}


class ArtesynChargerControllerThread(threading.Thread):
    CHASSIS_STATE_POWERED_OFF = 0
    CHASSIS_STATE_POWERED_ON = 1
    REQUEST_RETRIES = 3
    REQUEST_TIMEOUT = 5000  # ms
    
    def __init__(self, zctx=None, chassis_id=None, name=None, **kwargs):
        self.logger = setup_logger('ArtCntrltThr.chassis%d' % int(chassis_id))
        self.logger.setLevel(logging.DEBUG)
        self.ess_unit = kwargs.pop('ess_unit')
        threading.Thread.__init__(self, **kwargs)
        self.zctx = zctx
        self.name = name
        self.shutdown_flag = threading.Event()
        self.chassis_id = chassis_id  
        self.register_update_string = ''
        self.subscriber = None
        self.seq_num = 0
        self.header = 0
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.reserved1 = 0
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.reserved2 = 0
        self.num_cmds = None
        self.unit_addr = None
        self.int_dev_addr = None
        self.reserved3 = 0
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_data = [0] * ARTESYN_CMD_DATA_BUF_LEN
        self.cmd_code = None
        self.send_buf = ctypes.create_string_buffer(ARTESYN_MSG_BUF_LEN)
        self.recv_buf = ctypes.create_string_buffer(ARTESYN_MSG_BUF_LEN)
        self.charger_list = []
        self.module_charging_dict = {}
        self.chassis_state = ArtesynChargerControllerThread.CHASSIS_STATE_POWERED_OFF
        self.power_down()
        self.model_dict = {}
        self.subscriber = None
        self.auto_shutdown_behavior = True

        try:
            ''' Set all the chargers to False (not charging).'''
            self.get_charger_list()
            self.module_charging_dict = {i: False for i in self.charger_list}
        except:
            raise
        self.ip_addr = None
        try:
            self.get_chassis_ip_addr()
        except:
            raise
        
        self.UDPSock = None
        try:
            self.construct_udp_socket()
        except:
            raise

        self.command_dict = \
            {
                'chassis_up' : self.chassis_up,
                'operation': self.operation,
                'operation_module': self.operation_module,
                'operation_module_all': self.operation_module_all,
                'write_protect' : self.write_protect,
                'write_protect_module' : self.write_protect_module,
                'write_protect_module_all' : self.write_protect_module_all,
                'clear_faults' : self.clear_faults,
                'clear_module_faults' : self.clear_module_faults,
                'set_vref' : self.set_vref,
                'set_vref_chassis' : self.set_vref_chassis,
                'set_output_current_level' : self.set_output_current_level,
                'chassis_disable' : self.chassis_disable,
                'power_up' : self.power_up,
                'power_down' : self.power_down
            }
        self.db_errors = 0

    def is_off(self):
        return ArtesynChargerControllerThread.CHASSIS_STATE_POWERED_OFF == self.chassis_state
        
    def construct_udp_socket(self):
        self.logger.debug('Constructing UDP socket...')
        try:
            self.UDPSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.UDPSock.settimeout(1.0)
        except socket.error:
            self.logger.exception('Unable to open UDP socket.')
            raise ConnectError('Unable to open UDP socket.')
        self.logger.debug('UDP socket ready...')

    def prepared_act_on_database(self, action, command, args):
        try:
            with closing(MyDB.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                      db=DB_NAME, cursorclass=my_cursor.DictCursor)) as connection:
                with connection as cursor:
                    cursor.execute(command, args)
                    if action == FETCH_ONE:
                        return cursor.fetchone()
                    elif action == FETCH_ALL:
                        return cursor.fetchall()
                    return True
        except Exception as error:
            critical_string = "act_on_database failed. \nAction: %s \nString: %s \nArgs: %s - %s" \
                              % (action, command, args, repr(error))
            self.logger.exception(critical_string)
            raise DatabaseError(critical_string)

    def create_cmd_msg(self):
        """ Constant fields """
        self.reserved1 = 0
        self.reserved2 = 0
        self.reserved3 = 0

        self.seq_num += 1
        seq_num = np.uint32(self.seq_num)
        pack_into('<I', self.send_buf, 0, seq_num)
        num_cmds = np.uint8(ARTESYN_COMMAND_TYPE_MSG << 7) | np.uint8(self.split << 5) | np.uint8(self.num_cmds)
        
        if int(self.int_dev_addr) == 0:
            addr_seg = np.uint8(self.unit_addr << 4)
        else:
            addr_seg = np.uint8(self.unit_addr << 4) | np.uint8(mod_num_to_reg_addr_dict[int(self.int_dev_addr)])
        #self.logger.debug('create addr_seg 0x%02x' % addr_seg)

        data_len_seg = np.uint8((self.read_write << 6) | np.uint8(self.cmd_data_len))
        cmd_seg = np.uint8(self.cmd_code)
        pack_into('>BBBB', self.send_buf, 4, num_cmds, addr_seg, data_len_seg, cmd_seg)
        if self.cmd_data_len > 0:
            offset = 8
            for i in range(0, self.cmd_data_len):
                pack_into('>B', self.send_buf, offset, np.uint8(self.cmd_data[i]))
                offset += 1
        
        return True

    def log_cmd_msg(self):
        self.logger.debug('CMD_MSG:\n'
                          '    seq_num = 0x%04x\n'
                          '    type = 0x%x\n'
                          '    reserved1 = 0x%x\n'
                          '    split = 0x%x\n'
                          '    reserved2 = 0x%x\n'
                          '    num_cmds = 0x%x\n'
                          '    unit_addr = 0x%x\n'
                          '    int_dev_addr = 0x%x\n'
                          '    reserved3 = 0x%x\n'
                          '    read_write = 0x%x\n'
                          '    data_len = 0x%x\n'
                          '    cmd_code = 0x%x\n'
                          % (self.seq_num, self.type, self.reserved1, self.split,
                             self.reserved2, self.num_cmds, self.unit_addr, self.int_dev_addr,
                             self.reserved3, self.read_write, self.cmd_data_len, self.cmd_code))

    def send_charger_cmd(self):
        ''' Uncomment the following statement to get debug output of the message fields. '''
        #self.log_cmd_msg()
        
        ihp = (self.ip_addr, ARTESYN_CHASSIS_UDP_PORT)
        try:
            self.UDPSock.sendto(self.send_buf, ihp)
        except socket.error:
            self.UDPSock.close()
            self.logger.exception('Socket error sending command to chassis %s' % ihp[0])
            raise CommunicationError(socket.error)

    def read_charger_response(self):
        try:
            self.recv_buf, addr = self.UDPSock.recvfrom(1024)
        except socket.timeout:
            pass
            #self.logger.debug('Socket timeout waiting for response from chassis%d' % int(self.chassis_id))
        except Exception as error:
            raise CommunicationError('Socket error on recvfrom() - %s' % error)
        
        return True

    def check_response_error(self):
        hex_resp = bytearray(self.recv_buf)
#        seq_num = np.uint32(hex_resp[0])
#        if seq_num != self.seq_num:
#            self.logger.warning('Mismatched sequence number response [0x%x : 0x%x]' % (seq_num, self.seq_num))
        resp_seg = np.uint8(hex_resp[4])
        

        return resp_seg & ARTESYN_RESP_MSG_NORMAL_BIT_MASK
        
    def get_chassis_ip_addr(self):
        sql = 'SELECT %s from %s WHERE %s = %s;' % (AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME,
                                                    DB_AESS_CHARGER_CHASSIS_TABLE_NAME,
                                                    AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME,
                                                    "%s")
        args = [self.chassis_id,]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        self.ip_addr = response[AESS_CHARGER_CHASSIS_IP_ADDR_COLUMN_NAME]
        self.logger.debug('Chasis ID %s using IP address %s...' % (self.chassis_id, self.ip_addr))
        return True

    def get_charger_vref(self, module=None):
        sql = "SELECT %s FROM %s WHERE %s = %s AND %s = %s;" % (ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                                DB_ARTESYN_CHARGER_DATA_TABLE_NAME,
                                                                ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                                "%s",
                                                                ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                                "%s")
        args = [module, self.chassis_id]
        response = prepared_act_on_database(FETCH_ONE, sql, args)
        if response is None:
            return None
        else:
            return response[ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME]
    
    def set_db_vref(self, vref=None, module=None):
        self.logger.debug('Setting VREF to %f' % float(vref))
        sql = "INSERT INTO %s (%s, %s, %s) VALUES (%s, %s, %s) " \
              "ON DUPLICATE KEY UPDATE %s=%s, %s=%s, %s=%s" % (DB_ARTESYN_CHARGER_DATA_TABLE_NAME,
                                                               ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                               ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                               ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                               "%s", "%s", "%s",
                                                               ARTESYN_CHARGER_DATA_VREF_COLUMN_NAME,
                                                               "%s",
                                                               ARTESYN_CHARGER_DATA_MODULE_NUM_COLUMN_NAME,
                                                               "%s",
                                                               ARTESYN_CHARGER_DATA_CHASSIS_ID_COLUMN_NAME,
                                                               "%s")
        args = [vref, module, self.chassis_id, vref, module, self.chassis_id]
        prepared_act_on_database(EXECUTE, sql, args)
        
        return True
    
    def clear_cmd_data_fields(self):
        self.cmd_code = 0
        self.cmd_data_len = 0
        self.cmd_data = [0] * ARTESYN_CMD_DATA_BUF_LEN 

    def get_charger_list(self):
        sql = "SELECT %s, %s from %s WHERE %s = %s;" % (AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                        AESS_CHARGER_MODULE_MODEL_NUM_COLUMN_NAME,
                                                        DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                        AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME,
                                                        "%s")
        args = [self.chassis_id,]
        results = prepared_act_on_database(FETCH_ALL, sql, args)
        
        for result in results:
            self.charger_list.append(result[AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME])
            self.model_dict[result[AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME]] = \
                result[AESS_CHARGER_MODULE_MODEL_NUM_COLUMN_NAME]
                
    def set_startup_delay_active_flag(self, value=0):
        sql = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                       ESS_MASTER_INFO_TABLE_STARTUP_DELAY_ACTIVE_COLUMN_NAME, "%s",
                                                       ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, sql, args)
            
    def _send_relay_cmd(self, cmd_str=None, param=None):
        success = False
        param_str = str(param)
        cmd = "|".join([cmd_str, param_str])

        try:
            pub_socket = self.zctx.socket(zmq.PUB)
        except ZMQError as error:
            self.logger.exception(error)
            pub_socket.close()
            raise ZmqError(error)
        pub_socket.setsockopt(zmq.LINGER, 0)
        pub_socket.hwm = 100
        pub_socket.connect(MSG_BROKER_XSUB_URL)

        time.sleep(PUB_CONNECT_WAIT_TIME)

        topic = "essid%d-relay" % self.ess_unit.id
        pub_socket.send_multipart([topic, cmd])
        pub_socket.close()

        return success

    def set_charging_state(self, module=None, state=None):
        self.logger.debug('Setting charging state to %d for module %d...' % (state, int(module)))
        self.module_charging_dict[int(module)] = state

    def get_charging_state(self, module=None):
        return self.module_charging_dict.get(int(module), None)

    def get_auto_shutdown_behavior(self):
        sql = "SELECT %s FROM %s WHERE %s = %s" % (ESS_UNITS_TABLE_AUTO_SHUTDOWN_BEHAVIOR_COLUMN_NAME,
                                                   DB_ESS_UNITS_TABLE,
                                                   ESS_UNITS_ID_COLUMN_NAME, "%s")
        response = prepared_act_on_database(FETCH_ONE, sql, [self.ess_unit.id])
        self.auto_shutdown_behavior = response[ESS_UNITS_TABLE_AUTO_SHUTDOWN_BEHAVIOR_COLUMN_NAME]

    """
    API command functions.
    """
    def chassis_up(self):
        """ Remove write protect for chassis. """
        self.logger.debug('Remove write protect for chassis %d...' % self.chassis_id)
        self.write_protect(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM)
        
        """ Remove write protect for all charger modules in chassis. """
        self.logger.debug('Remove write protect for all charger modules in chassis %d...' % self.chassis_id)
        self.write_protect_module_all(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM)
            
        """ Enable chassis."""
        self.logger.debug('Enable chassis %d...' % self.chassis_id)
        self.operation(ARTESYN_ISOCOMM_ENABLE_PARAM)

        """ Disable all charger modules in the chassis. """
        self.operation_module_all(ARTESYN_ISOCOMM_DISABLE_PARAM)

    def operation(self, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process operation command - unit %d is powered down' %
                                int(self.chassis_id))

        """
        The OPERATION command is used to toggle Global Module Enable/Disable.
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)
        if param == ARTESYN_ISOCOMM_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_ENABLE_CMD)
        elif param == ARTESYN_ISOCOMM_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_DISABLE_CMD)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OPERATION
        self.create_cmd_msg()

        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def operation_module(self, module_id=None, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process operation-module command - unit %d is powered down' %
                                int(self.chassis_id))

        """
        The OPERATION command is used to toggle Global Module Enable/Disable.
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)
        if param == ARTESYN_ISOCOMM_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_ENABLE_CMD)
        elif param == ARTESYN_ISOCOMM_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_DISABLE_CMD)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        if module_id is None:
            raise ParameterError('Missing charger module number')

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_id)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OPERATION
        self.create_cmd_msg()

        for i in range(0,3):
            self.send_charger_cmd()
            time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

        if param == ARTESYN_ISOCOMM_ENABLE_PARAM:
            self.logger.debug('Chassis %d Module %d enabled...' % (int(self.chassis_id),int(module_id)))
        else:
            self.logger.debug('Chassis %d Module %d disabled...' % (int(self.chassis_id), int(module_id)))

    def operation_module_all(self, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process operation-module-all command - unit is %d powered down' %
                                int(self.chassis_id))

        """ Enable/disable all charger modules in a chassis. """
        for module_id in self.charger_list:
            self.operation_module(module_id, param)
  
    def write_protect(self, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process write-protect command - unit %d is powered down' %
                                int(self.chassis_id))

        """
        The WRITE_PROTECT command is used to control writing to the ISOCOMM.  The
        intent of this command is to provide protection against accidental changes.        
        """
        if param is None:
            error_str = 'Missing enable/disable parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_CMD_DATA)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_CMD_DATA)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)
        
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_WRITE_PROTECT
        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def write_protect_module(self, module_id=None, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process write-protect-module command - unit %d is powered down' %
                                int(self.chassis_id))

        if param is None:
            error_str = 'Missing write-protect enable/disable parameter'
            self.logger.error(error_str)
            raise ParameterError(error_str)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_ENABLE_CMD_DATA)
        elif param == ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_PARAM:
            cmd_data = np.int8(ARTESYN_ISOCOMM_WRITE_PROTECT_DISABLE_CMD_DATA)
        else:
            raise ParameterError('Invalid operation parameter [%s]' % param)

        if module_id is None:
            raise ParameterError('Missing charger module number')
       
        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_id)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(cmd_data)
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_WRITE_PROTECT
        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def write_protect_module_all(self, param=None):
        if self.is_off():
            self.logger.warning('Attempt to process write-protect-module-all command - unit %d is powered down' %
                                int(self.chassis_id))

        """ Remove/set write protect for all charger modules in chassis. """
        for module_id in self.charger_list:
            self.write_protect_module(module_id, param)

    def clear_faults(self):
        """
        Standard PMBUS command to remove the warning or fault bits set in the status register.
        """
        if self.is_off():
            self.logger.warning('Attempt to process clear-faults command - unit %d is powered down' %
                                int(self.chassis_id))

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.num_cmds = 1
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_CLEAR_FAULTS

        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def clear_module_faults(self, module_num=None):
        """
        Standard PMBUS command to remove the warning or fault bits set in the status register.
        """
        if self.is_off():
            self.logger.warning('Attempt to process clean-module-faults command - unit %d is powered down' %
                                int(self.chassis_id))

        if module_num is None:
            raise ParameterError('Missing module number parameter')

        """ Initialize message. """
        self.num_cmds = 1
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module_num)
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_data_len = 0
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_CLEAR_FAULTS

        self.create_cmd_msg()
        self.send_charger_cmd()
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def set_vref(self, module=None, vref=None):
        if self.is_off():
            self.logger.warning('Attempt to process set-vref command - unit %d is powered down' %
                                int(self.chassis_id))

        if module is None:
            raise ParameterError('Missing module number parameter')
        if vref is None:
            raise ParameterError('Missing vref parameter')

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.clear_cmd_data_fields()
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module)
        self.read_write = ARTESYN_WRITE_CMD
        direct_data = pdu.float_to_direct(float(vref), ARTESYN_MODULE_CMD_CODE_VREF_SCALING_FACTOR)
        self.cmd_data_len = ARTESYN_MODULE_CMD_CODE_VREF_DATA_LEN + 1
        byte_mask = np.uint32(0xff)
        self.cmd_data[0] = ARTESYN_MODULE_CMD_CODE_VREF_DATA_LEN
        for i in range(1, self.cmd_data_len):
            cmd_data = (direct_data & byte_mask) >> ((i-1) * 8)
            self.cmd_data[i] = np.uint8(cmd_data)
            byte_mask = byte_mask << 8
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_VREF
        self.create_cmd_msg()
        self.send_charger_cmd()
        self.set_db_vref(vref, module)

        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)
        
        return True
    
    def set_vref_chassis(self, vref=None):
        for module in self.charger_list:
            self.set_vref(module, vref)

    def set_output_current_level(self, module=None, required_output_current=None):
        """
        @summary: Command to set the output current level.
        @param:   module number, desired output current level.
        """
        if module is None:
            raise ParameterError('Missing module number parameter')
        if required_output_current is None:
            raise ParameterError('Missing desired output current parameter')

        self.logger.debug('REQ OUT CUR = %f for chassis %d module %d...' % (float(required_output_current),
                          int(self.chassis_id), int(module)))

        if float(required_output_current) == 0.0:
            ''' The power for this charger module is 0 (off).  We must determine
                if ALL the charger modules in the chassis are off.
            '''
            if self.get_charging_state(int(module)):
                self.logger.debug('Disabling charger %d/%d...' % (int(module), int(self.chassis_id)))
                self.operation_module(int(module), ARTESYN_ISOCOMM_DISABLE_PARAM)
                self.set_charging_state(int(module), False)

            if self.auto_shutdown_behavior == ARTESYN_AUTO_SHUTDOWN_DISABLED:
                return True

            shut_down = True
            for charger,state in self.module_charging_dict.items():
                if state:
                    shut_down = False
                    break
            if shut_down:
                self.power_down()

            return True
         
        vref = self.get_charger_vref(module)
        if vref is None:
            raise ProcessingError('Attempt to set output current level without previously setting VREF')
        if self.is_off():
            '''
            @note: the chargers must be brought up in a disabled state, to avoid
                   having them immediately start producing power at the default
                   level (max current!)  After we program the module, _then_ we
                   enable it.
            '''
            self.logger.debug('Received active power command for module %d in powered-down chassis %d' %
                              (int(module), int(self.chassis_id)))
            self.power_up()
            self.set_vref_chassis(vref)
            self.operation_module(int(module), ARTESYN_ISOCOMM_ENABLE_PARAM)
        else:
            self.logger.debug('Received active power command for module %d in chassis %d...' % (int(module),
                                                                                                int(self.chassis_id)))
            if self.get_charging_state(int(module)) == False:
                # Re-enable the charger.
                self.operation_module(int(module), ARTESYN_ISOCOMM_ENABLE_PARAM)

        """
        Initialize message.
        @note: In single iHP configurations, unit address should be 0.
        """
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = int(module)
        self.read_write = ARTESYN_WRITE_CMD
        
        ''' Figure out what kind of charger we are dealing with. '''
        if self.model_dict[int(module)] == ARTESYN_48V_MODULE_MODEL_NUM:
            Inominal = ARTESYN_48V_MODULE_NOMINAL_CURRENT
        else:
            Inominal = ARTESYN_80V_MODULE_NOMINAL_CURRENT
        self.logger.debug('Using %f as Inom for chassis %d module %d' % (Inominal, int(self.chassis_id), int(module)))

        '''
        @note: the chargers will not produce power below 5% of their rated capacity.
               If the required output current is less than the minimum rating, we
               set the required current to the minimum value.
        '''
        if self.model_dict[int(module)] == ARTESYN_48V_MODULE_MODEL_NUM:
            ''' Check for current exceeding limits first. '''
            max_limited_output_current = min(float(required_output_current),
                                             float(ARTESYN_48V_MODULE_NOMINAL_CURRENT))
            self.logger.debug('max_limited_output_current = %f for %d/%d...' %
                              (max_limited_output_current, int(module), int(self.chassis_id)))
            adjusted_output_current = max(float(max_limited_output_current),
                                          float(ARTESYN_48V_MIN_CHARGING_CURRENT))
        else:
            max_limited_output_current = min(float(required_output_current),
                                             float(ARTESYN_80V_MODULE_NOMINAL_CURRENT))
            self.logger.debug('max_limited_output_current = %f for %d/%d...' %
                              (max_limited_output_current, int(module), int(self.chassis_id)))
            adjusted_output_current = max(float(max_limited_output_current),
                                          float(ARTESYN_80V_MIN_CHARGING_CURRENT))
        self.logger.debug('adjusted_output_current = %f for charger %d/%s...' % (adjusted_output_current,
                                                                                 int(module), str(self.chassis_id)))

        """ Calculate new OC_FLM value from desired current level. """
        if vref <= ARTESYN_MODULE_NOMINAL_VOLTAGE:
            oc_flm = float(adjusted_output_current) / Inominal
            self.logger.debug('OC_FAULT_LIMIT_MULTIPLIER = %f / %f = %f' % (float(adjusted_output_current),
                                                                            float(Inominal),
                                                                            oc_flm))
        else:
            oc_flm = (float(adjusted_output_current) * float(vref)) / Inominal
            self.logger.debug('OC_FAULT_LIMIT_MULTIPLIER = (%f * %f)/%f = %f' % (float(adjusted_output_current),
                                                                                 float(vref),
                                                                                 float(Inominal),
                                                                                 oc_flm))
        self.logger.debug('Final OC_FAULT_LIMIT_MULTIPLIER for chassis %d module %d = %f' % (int(self.chassis_id), int(module), oc_flm))
        direct_data = pdu.float_to_direct(float(oc_flm),
                                          ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_SCALING_FACTOR)
        self.logger.debug('direct_data = %d' % direct_data)
        self.cmd_data_len = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_DATA_LEN + 1
        self.cmd_data[0] = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER_DATA_LEN
        byte_mask = np.uint32(0xff)
        for i in range(1, self.cmd_data_len):
            cmd_data = (direct_data & byte_mask) >> ((i-1) * 8)
            self.cmd_data[i] = np.uint8(cmd_data)
            byte_mask = byte_mask << 8
        self.cmd_code = ARTESYN_MODULE_CMD_CODE_OC_FAULT_LIMIT_MULTIPLIER
        self.create_cmd_msg()
        self.set_charging_state(int(module), True)
        self.send_charger_cmd()
        self.logger.debug("Sanity check: ocflm = %f, chassis = %d, charger = %d, chassis-state = %d, charging = %d" %
                          (oc_flm, int(self.chassis_id), int(module), int(self.chassis_state), int(self.module_charging_dict[int(module)])))

        return True
            
    def chassis_disable(self):
        if self.is_off():
            self.logger.warning('Attempt to process chassis-disable command - unit %d is powered down' %
                                int(self.chassis_id))

        self.operation(ARTESYN_ISOCOMM_DISABLE_PARAM)
        time.sleep(ARTESYN_INTER_MSG_DELAY_TIME)

    def module_save(self, reg_addr=None):
        self.type = ARTESYN_COMMAND_TYPE_MSG
        self.split = ARTESYN_NO_SPLIT_RESPONSES
        self.num_cmds = 1
        self.unit_addr = ARETSYN_DEFAULT_IHP_UNIT_ADDR
        self.int_dev_addr = ARTESYN_INTERNAL_DEVICE_ADDR_ISOCOMM
        self.read_write = ARTESYN_WRITE_CMD
        self.cmd_code = ARTESYN_CMD_CODE_MODULE_SAVE
        self.cmd_data_len = 1
        self.cmd_data[0] = np.uint8(reg_addr)
        self.create_cmd_msg()
        self.send_charger_cmd()

        '''
        Set timer to 1650ms, the observed completion time per charger module.
        A fully-stuffed chassis (8-modules) will take a little over 13 seconds
        to save its state.
        '''
        time.sleep(1.650)

    def power_up(self):
        '''
        This command steps the chassis through a power-up sequence.  First, the
        ChassisRelayDaemon is sent a command to turn power on to the unit.  Then,
        chassis_up() is called to initialize the chassis.  At this point, the 
        chassis should be ready to receive operational commands. 
        '''
        self.logger.debug('Required output current needed POWER-UP command for chassis %d...' % int(self.chassis_id))
        self._send_relay_cmd('power_on', self.chassis_id)
        
        ''' Let the GUI know that we are sleeping. '''
        self.set_startup_delay_active_flag(1)
        
        ''' It takes a minimum of 25 seconds for the chassis to boot up. '''
        time.sleep(30.0)

        ''' Let the GUI know that we are awake. '''
        self.set_startup_delay_active_flag(0)

        ''' Set the chassis state to ON. '''
        self.chassis_state = ArtesynChargerControllerThread.CHASSIS_STATE_POWERED_ON

        self.logger.debug('executing chassis-up sequence for chassis %d' % int(self.chassis_id))
        self.chassis_up()
        
    def power_down(self):
        self.logger.debug('Last charger getting 0 ROC command caused POWER-DOWN for chassis %d...' % int(self.chassis_id))
        self._send_relay_cmd('power_off', self.chassis_id)
        self.set_startup_delay_active_flag(0)
                
        ''' Last step: set the chassis state to OFF. '''
        self.chassis_state = ArtesynChargerControllerThread.CHASSIS_STATE_POWERED_OFF

    def run(self):
        self.logger.info("Artesyn Controller thread chassis%s starting..." % self.chassis_id)

        subscribe_str = 'chassis%d#' % int(self.chassis_id)
        try:
            self.subscriber = self.zctx.socket(zmq.SUB)
        except ZMQError as error:
            raise ZmqError(error)
        self.subscriber.setsockopt(zmq.SUBSCRIBE, subscribe_str)
        
        try:
            self.subscriber.connect(MSG_BROKER_XPUB_URL)
        except ZMQError as error:
            self.logger.exception(error)
            self.subscriber.close()
            raise ZmqError(error)

        poller = zmq.Poller()
        poller.register(self.subscriber, zmq.POLLIN)

        ''' Insure the chassis relays are in a known state: OFF, upon initialization. '''
        self.logger.debug('Calling power_down() for chassis%d...' % int(self.chassis_id))
        self.power_down()
        
        while True:
            self.clear_cmd_data_fields()
            self.get_auto_shutdown_behavior()

            try:
                zsocks = dict(poller.poll(SUBSCRIBER_SOCKET_TIMEOUT))
            except zmq.ZMQError as error:
                str_err = repr(error)
                self.logger.exception(str_err)
                continue

            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                try:
                    [self.name, data] = self.subscriber.recv_multipart()
                except zmq.ZMQError as error:
                    self.logger.exception(error)
                    continue
                
                '''
                We can receive either a list or a dictionary from our fans.
                '''
                data_str = json.loads(data)
                self.logger.debug("Received command: %s" % data_str)
                if isinstance(data_str, dict):
                    param_dict = json.loads(data)
                    command = param_dict.keys()[0]
                    params = param_dict[command]
                    param_list = params.split(" ")
                    self.logger.debug('Dictionary parameters: %s' % param_list)
                else:
                    command_list = data_str.split("|")
                    command = command_list[0]
                    param_str = command_list[1]
                    param_list = param_str.split(" ")
                self.logger.debug('Received %s|%s' % (command, repr(param_list)))

                try:
                    if param_list is 'None':
                        self.command_dict[command]()
                    elif len(param_list) == 1:
                        self.command_dict[command](param_list[0])
                    elif len(param_list) == 2:
                        self.command_dict[command](param_list[0], param_list[1])
                    elif len(param_list) == 3:
                        self.command_dict[command](param_list[0], param_list[1], param_list[2])
                except KeyError as error:
                    str_err = 'Invalid key: %s' % repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except (ConnectError, DatabaseError, ParameterError, WriteError) as error:
                    str_err = repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except TypeError as error:
                    str_err = 'TypeError: %s' % repr(error.message)
                    self.logger.exception(str_err)
                    continue
                except Exception as error:
                    self.logger.exception('Unable to process command - %s' % error)
                    continue
                
                self.read_charger_response()
                if self.check_response_error() != 0:
                    self.logger.error('iHP error')
                
        self.subscriber.close()


class ArtesynControllerFactoryDaemon(EssDaemon):
    alarm_id = ALARM_ID_ARTESYN_CHARGER_CONTROLLER_RESTARTED
    update_timeout = 60.0

    def __init__(self, **kwargs):
        super(ArtesynControllerFactoryDaemon, self).__init__(**kwargs)
        self.thread_list = []
        self.chassis_list = []
        self.db_errors = 0
        self.zctx = None
        #self.logger.setLevel(logging.DEBUG)
        self.logger.debug('ArtesynControllerFactoryDaemon initialized...')

    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread_stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def get_operational_parameters(self):
        self.logger.debug('Fetching operational parameters...')
        sql = "SELECT * FROM aess_charger_chassis acc " \
                " JOIN aess_charger_module acm on acm.chassis_id = acc.chassis_id " \
                " JOIN pcs_units on pcs_units.id = acm.pcs_unit_id " \
                " WHERE pcs_units.ess_unit_id = %s " \
                " GROUP BY acc.chassis_id" % "%s"
        results = prepared_act_on_database(FETCH_ALL, sql, [self.ess_unit.id])
        for result in results:
            self.chassis_list.append(result[AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME])
        
    def run(self):
        self.update()

        self.get_operational_parameters()

        for chassis_id in self.chassis_list:
            self.logger.debug('chassis%d present in operational parameters...' % chassis_id)

        try:
            self.zctx = zmq.Context(1)
        except ZMQError as error:
            self.logger.exception(error)
            raise

        for chassis_id in self.chassis_list:
            thread_name = 'chassis%d' % int(chassis_id)
            self.logger.debug('Creating thread %s...' % thread_name)
            thread = ArtesynChargerControllerThread(
                zctx=self.zctx,
                chassis_id=chassis_id,
                name=thread_name,
                ess_unit=self.ess_unit)
            self.logger.debug('Created thread %s...' % thread_name)
            thread.start()
            self.logger.debug('Started charger controller thread chassis%s' % chassis_id)
            self.thread_list.append(thread)

        """
        @note: The factory joins a thread in its list every few seconds.
        """
        while True:
            self.update()
            try:
                for thread in self.thread_list:
                    self.update()
                    
                    thread_name = ''
                    try:
                        thread_name = thread.getName() 
                        thread.join(10.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('Artesyn Charger Controller thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        chassis_id = int(thread_name[7:8])   # JEP: int(thread_name[7:])
                        thread = ArtesynChargerControllerThread(
                                        self.zctx,
                                        chassis_id,
                                        name=thread_name,
                                        ess_unit=self.ess_unit)
                        try:
                            self.logger.debug('Restarting Artesyn controller thread chassis%d' % chassis_id)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
                    else:
                        #self.logger.debug('Thread %s is alive...' % thread_name)
                        pass
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                break
            
        self.zctx.term()
        self.logger.warning('ArtesynControllerFactory fell through main loop')
        return False
