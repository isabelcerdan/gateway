#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
@module: LaunchSolarLogger.py

@description: launch the SolarLogger.

@copyright: Apparent Inc. 2018

@author: steve

@created: May 29, 2018
'''

from lib.logging_setup import *
from SolarAltitudeVsPower.SolarLogger import instantiate_solarlogger

logger = setup_logger('SolarLauncher')

try:
    instantiate_solarlogger()
except:
    logger.info('Solar Logger exiting...')

