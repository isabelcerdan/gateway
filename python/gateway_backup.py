#!/usr/share/apparent/.py2-virtualenv/bin/python

import datetime, time
import os
import subprocess
from shutil import copyfile

import boto3
from lib.db import prepared_act_on_database, FETCH_ONE

from uuid import getnode as get_mac

mac = '-'.join('%02X' % ((get_mac() >> 8 * i) & 0xff) for i in reversed(xrange(6)))

ignore_tables = ['pcc_readings_1',
                 'pcc_readings_2',
                 'pcc_readings_combo',
                 'solar_readings_1',
                 'battery_readings_1',
                 'net_prot_log',
                 'curtail_log',
                 'pcc_readings_1_old',
                 'pcc_readings_2_old',
                 'solar_readings_1_old',
                 'battery_readings_1_old',
                 'cess_400_master_data_log']
mysqldump_options = " ".join(["--ignore-table=gateway_db.%s" % t for t in ignore_tables])

backups_path = "/var/lib/apparent/backups"
timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
backup_path = os.path.join(backups_path, timestamp)
if not os.path.exists(backup_path):
    os.makedirs(backup_path)

# backup all non-HEAP tables
cmd = "mysqldump --lock-tables=false --skip-lock-tables %s gateway_db > %s/gateway-db.sql" % (mysqldump_options, backup_path)
print(cmd)
os.system(cmd)

# copy leases file to backup
app_path = "/usr/share/apparent"
repo_path = "/home/gateway/gateway"
if os.path.isfile("/var/lib/misc/dnsmasq.leases"):
    copyfile("/var/lib/misc/dnsmasq.leases", os.path.join(backup_path, "dnsmasq.leases"))
if os.path.isfile(os.path.join(app_path, "VERSION")):
    copyfile(os.path.join(app_path, "VERSION"), os.path.join(backup_path, "VERSION"))
if os.path.isfile(os.path.join(app_path, "VERSION.git")):
    copyfile(os.path.join(app_path, "VERSION.git"), os.path.join(backup_path, "VERSION.git"))
if os.path.isfile(os.path.join(repo_path, "provision", "VERSION.git")):
    copyfile(os.path.join(repo_path, "provision", "VERSION.git"), os.path.join(backup_path, "VERSION-provision.git"))

# compress backup
cmd = "zip -rm %s.zip %s" % (backup_path, backup_path)
print(cmd)
subprocess.call(["zip", "-rm", timestamp + ".zip", timestamp], cwd=backups_path)

# remove all but the last 5 backups
os.system("ls -d %s/* | sort -r | tail -n +6 | xargs -I {} rm -rf -- {}" % backups_path)

sql = "SELECT gateway_base_sn FROM energy_report_config"
results = prepared_act_on_database(FETCH_ONE, sql, [])
if results and results['gateway_base_sn'] is not None and len(results['gateway_base_sn']) > 0:
    gateway_id = '-'.join([results['gateway_base_sn'], mac[-8:]])
else:
    gateway_id = mac

# push backup to s3 (sse=server-side encryption)
bucket = "gateway-db-backups"
cmd = "/usr/bin/aws s3 sync --sse %s/ s3://%s/%s/" % (backups_path, bucket, gateway_id)
print(cmd)
subprocess.call(["/usr/bin/aws", "s3", "sync", "--sse", backups_path, ("s3://%s/%s/" % (bucket, gateway_id))])

client = boto3.client('s3')
# client.upload_file(backup_path + '.zip', bucket, os.path.join(gateway_id, timestamp + '.zip'))

# list objects at s3://gateway-db-backups/08-00-27-BD-12-0A/
result = client.list_objects(Bucket=bucket, Prefix=gateway_id + '/', Delimiter='/')

# grab the relevant backup contents from the returned JSON object
backups = result.get('Contents')
sorted_backups = sorted(backups, key=lambda k: k['Key'])

# lets keep 30 days worth of backups on aws s3
keep = 30
num_remove = len(sorted_backups) - keep
if num_remove > 0:
    # create dictionary object with just keys of objects to be removed
    old_backups = {'Objects': [{'Key': o.get('Key')} for o in sorted_backups[:num_remove]]}

    # delete old backups
    client.delete_objects(Bucket=bucket, Delete=old_backups)
