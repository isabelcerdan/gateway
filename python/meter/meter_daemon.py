#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import time
import datetime

from meter import Meter
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_PCC_METER_INTERFACE_RESTARTED, \
                                  ALARM_ID_PCC2_METER_INTERFACE_RESTARTED, \
                                  ALARM_ID_SOLAR_METER_INTERFACE_RESTARTED, \
                                  ALARM_ID_BATTERY_METER_INTERFACE_RESTARTED
from lib.gateway_constants.DBConstants import METER_ROLE_PCC1, \
                                              METER_ROLE_PCC2, \
                                              METER_ROLE_SOLAR1, \
                                              METER_ROLE_BATTERY1, \
                                              METER_ROLE_BATTERY2

class MeterDaemon(Daemon):

    source_table = 'energy_meter'
    source_id_field = 'meter_role'

    always_enabled = True

    update_timeout = 8.0
    alarm_id = {
        METER_ROLE_PCC1: ALARM_ID_PCC_METER_INTERFACE_RESTARTED,
        METER_ROLE_PCC2: ALARM_ID_PCC2_METER_INTERFACE_RESTARTED,
        METER_ROLE_SOLAR1: ALARM_ID_SOLAR_METER_INTERFACE_RESTARTED,
        METER_ROLE_BATTERY1: ALARM_ID_BATTERY_METER_INTERFACE_RESTARTED,
        METER_ROLE_BATTERY2: ALARM_ID_BATTERY_METER_INTERFACE_RESTARTED
    }
    check_finished = True

    def __init__(self, **kwargs):
        super(MeterDaemon, self).__init__(**kwargs)
        self.finished = kwargs.get('finished', None)
        self.meter = Meter.find_by(meter_role=self.id)
        if self.meter is None:
            raise Exception("No meter defined with role %s." % self.id)
        self.last_valid_read = time.time()

    def run(self):
        try:
            self.update()
            self.meter.open()
            self.update()
            self.meter.sync_subinterval()
            self.update()
            self.meter.sync_time(datetime.datetime.now())

            while not self.finished.is_set():
                self.update()
                if not self.meter.connected:
                    self.logger.error("Attempting to reconnect to meter %s..." % self.meter.name)
                    self.meter.reconnect()
                    self.meter.update_online()
                    time.sleep(1)
                else:
                    self.meter.read_all_registers()
                    self.meter.update_online()
                    if not self.meter.online:
                        self.meter.reconnect()
        except Exception as ex:
            self.logger.exception("Unhandled exception in meter daemon.")
        finally:
            if self.meter is not None:
                self.meter.close()
