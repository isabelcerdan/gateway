#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import time
import os
import json
import datetime
import collections
import requests
import copy
from meter import Meter
from lib.helpers import to_datetime, try_float
from lib.gateway_constants.DBConstants import *



# disable "Insecure Request Warnings" since we knowingly query remote meter with no SSL certificate
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)


class RemoteMeter(Meter):
    DEFAULT_API_PORT = 64436
    READ_ATTEMPTS = 3
    SAMPLES_IN_15_DAYS = 4 * 60 * 60 * 24 * 15  # 5184000 is the number of samples in 15 days

    def __init__(self, **kwargs):
        super(RemoteMeter, self).__init__(**kwargs)
        self.ip_address = kwargs.get('address', None)
        self.port = kwargs.get('port', self.DEFAULT_API_PORT)
        self.url = "https://%s:%s" % (self.ip_address, self.port)
        self.session = requests.Session()
        self.connected = True
        self.last_valid_timestamp = None

    def open(self):
        pass

    def close(self):
        self.session.close()

    def reconnect(self):
        self.session.close()
        self.session = requests.Session()

    def _read_all_registers(self):
        read_attempt = 0
        readings = None
        while read_attempt < self.READ_ATTEMPTS:
            read_attempt += 1
            with requests.session() as s:
                s.keep_alive = False
                try:
                    response = s.get(os.path.join(self.url, "pcc_readings_1.php"), verify=False, timeout=1.0)
                    readings = json.loads(response.content, object_pairs_hook=collections.OrderedDict)
                    for key, value in readings.items():
                        readings[key] = try_float(value)
                    readings['timestamp_received'] = datetime.datetime.now()
                    break
                except Exception as ex:
                    self.logger.exception("GET request to %s failed. Read attempt: %s" % (self.url, read_attempt))


        # save last good reading and compare to current reading to determine valid read

        # Check if this new reading is different from the last remote reading
        if readings is not None:

            # Remove the following before writing to local database
            try:
                readings.pop(METER_READINGS_TABLE_METER_FUNCTION_COLUMN_NAME)
                readings.pop(METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME)
                readings.pop(METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME)
            except KeyError:
                pass
                #self.logger.info("remote meter_readings table not up to date, remote might need an upgrade")

            timestamp = to_datetime(readings['timestamp'])
            if self.last_valid_timestamp is None or timestamp >= self.last_valid_timestamp:
                self.last_valid_timestamp = timestamp
            else:
                readings = None

        # Throttle reading the shared meter by sleeping for 500ms since we currently
        # only write a new meter reading to the database when we see a new timestamp
        # which is only accurate down to the second (not microsecond).
        time.sleep(0.5)

        return readings
