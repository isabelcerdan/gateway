import struct
from lib.logging_setup import *


class MeterRegisterIterator(object):
    def __init__(self, registers, flip_polarity=False):
        self.registers = registers
        self.flip_polarity = flip_polarity
        self.index = 0
        self.logger = setup_logger(__name__)

    def __iter__(self):
        return self

    def next(self):
        try:
            data1 = self.registers.getRegister(self.index)
            self.index += 1
            data2 = self.registers.getRegister(self.index)
            self.index += 1
        except IndexError as ex:
            self.logger.exception("Register index out of range.")
            raise StopIteration()

        if data1 == 32704:
            converted_data = "NULL"
        else:
            try:
                converted_data = "{0:.3f}".format(MeterRegisterIterator.convert2float(data1, data2))
                converted_data = float(converted_data)
            except Exception as ex:
                self.logger.exception("problem with float conversion in register")
                raise StopIteration()

        return converted_data

    @staticmethod
    def convert2float(upper16, lower16):
        upper_bin = '{0:016b}'.format(upper16)
        lower_bin = '{0:016b}'.format(lower16)
        binary_value_32_bit = upper_bin+lower_bin
        return struct.unpack('f', struct.pack('I', int(binary_value_32_bit, 2)))[0]


class MeterRegister(object):
    def __init__(self, **kwargs):
        self.address = kwargs.get('address', None)
        self.read = kwargs.get('read', True)
        self.write = kwargs.get('write', False)
        self.name = kwargs.get('name', "Default")
        self.field_name = kwargs.get('field_name', None)
        self.double = kwargs.get('double', True)
        self.group = kwargs.get('group', None)
        self.tags = kwargs.get('tags', None)
        self.desc = kwargs.get('desc', None)
        self.data = kwargs.get('data', None)
        self.convert = kwargs.get('convert', None)
        self.scale = kwargs.get('scale', 1.0)
        self.units = kwargs.get('units', None)
        self.signed = kwargs.get('signed', True)
        self.raw_1 = kwargs.get('raw_1', None)
        self.raw_2 = kwargs.get('raw_2', None)
        self.flip_polarity = kwargs.get('flip_polarity', False)

        self.count = 1
        if self.double:
            self.count += 1


r149_num_sub_intervals = MeterRegister(address=149, name="Demand number of sub-intervals per interval",
                                       write=True, double=False, signed=False)
r150_sub_interval_length = MeterRegister(address=150, name="Demand sub-interval length (sec)",
                                         write=True, double=False, signed=False)


r155_day_month = MeterRegister(address=155, name="Day Month", write=True, double=False)
r156_hour_year = MeterRegister(address=156, name="Hour Year", write=True, double=False)
r157_seconds_minutes = MeterRegister(address=157, name="Seconds Minutes", write=True, double=False)

# when CT orientation is reversed, net fields need to be flipped while import/export fields need to be swapped
r257_accumulated_real_energy_net = MeterRegister(address=257, name="Accumulated Real Energy: Net", field_name="Accumulated_Real_Energy_Net_FP", flip_polarity=True)
r259_real_energy_import = MeterRegister(address=259, name="Real Energy: Quadrants 1 & 4", field_name="Real_Energy_Import_FP")
r261_real_energy_export = MeterRegister(address=261, name="Real Energy: Quadrants 2 & 3", field_name="Real_Energy_Export_FP")
r263_reactive_energy_q1 = MeterRegister(address=263, name="Reactive Energy: Quadrant 1", field_name="Reactive_Energy_Q1_FP")
r265_reactive_energy_q2 = MeterRegister(address=265, name="Reactive Energy: Quadrant 2", field_name="Reactive_Energy_Q2_FP")
r267_reactive_energy_q3 = MeterRegister(address=267, name="Reactive Energy: Quadrant 3", field_name="Reactive_Energy_Q3_FP")
r269_reactive_energy_q4 = MeterRegister(address=269, name="Reactive Energy: Quadrant 4", field_name="Reactive_Energy_Q4_FP")
r271_apparent_energy_net = MeterRegister(address=271, name="Apparent Energy: Net", field_name="Apparent_Energy_Net_FP", flip_polarity=True)
r273_apparent_energy_import = MeterRegister(address=273, name="Apparent Energy: Quadrants 1 & 4", field_name="Apparent_Energy_Import_FP")
r275_apparent_energy_export = MeterRegister(address=275, name="Apparent Energy: Quadrants 2 & 3", field_name="Apparent_Energy_Export_FP")

r277_total_net_instantaneous_real_power = MeterRegister(address=277, name="Total Net Instantaneous Real (P) Power", field_name="Total_Net_Instantaneous_Real_Power_FP", flip_polarity=True)
r279_total_net_instantaneous_reactive_power = MeterRegister(address=279, name="Total Net Instantaneous Reactive (Q) Power", field_name="Total_Net_Instantaneous_Reactive_Power_FP", flip_polarity=True)
r281_total_net_instantaneous_apparent_power = MeterRegister(address=281, name="Total Net Instantaneous Apparent (S) Power", field_name="Total_Net_Instantaneous_Apparent_Power_FP")
r283_total_power_factor = MeterRegister(address=283, name="Total Power Factor (Total kW / Total kVA)", field_name="Total_Power_Factor_FP", flip_polarity=True)
r285_voltage_ll_avg = MeterRegister(address=285, name="Voltage, L-L (U), average of active phases", field_name="Voltage_LL_Avg_FP")
r287_voltage_ln_avg = MeterRegister(address=287, name="Voltage, L-N (V), average of active phases", field_name="Voltage_LN_Avg_FP")
r289_current_avg = MeterRegister(address=289, name="Current, average of active phases", field_name="Current_Avg_FP")
r291_frequency = MeterRegister(address=291, name="Frequency", field_name="Frequency_FP")
r293_total_real_power_present_demand = MeterRegister(address=293, name="Total Real Power Present Demand", field_name="Total_Real_Power_Present_Demand_FP", flip_polarity=True)
r295_total_reactive_power_present_demand = MeterRegister(address=295, name="Total Reactive Power Present Demand", field_name="Total_Reactive_Power_Present_Demand_FP", flip_polarity=True)
r297_total_apparent_power_present_demand = MeterRegister(address=297, name="Total Apparent Power Present Demand", field_name="Total_Apparent_Power_Present_Demand_FP")
r299_total_real_power_max_demand_import = MeterRegister(address=299, name="Total Real Power Max Demand Import", field_name="Total_Real_Power_Max_Demand_Import_FP", flip_polarity=True)
r301_total_reactive_power_max_demand_import = MeterRegister(address=301, name="Total Reactive Power Max Demand Import", field_name="Total_Reactive_Power_Max_Demand_Import_FP", flip_polarity=True)
r303_total_apparent_power_max_demand_import = MeterRegister(address=303, name="Total Apparent Power Max Demand Import", field_name="Total_Apparent_Power_Max_Demand_Import_FP")
r305_total_real_power_max_demand_export = MeterRegister(address=305, name="Total Real Power Max Demand Export", field_name="Total_Real_Power_Max_Demand_Export_FP", flip_polarity=True)
r307_total_reactive_power_max_demand_export = MeterRegister(address=307, name="Total Reactive Power Max Demand Export", field_name="Total_Reactive_Power_Max_Demand_Export_FP", flip_polarity=True)
r309_total_apparent_power_max_demand_export = MeterRegister(address=309, name="Total Apparent Power Max Demand Export", field_name="Total_Apparent_Power_Max_Demand_Export_FP")

# when CT orientation is reversed, import/export and Q1-4 fields simply need to be swapped (not flipped)
r317_accumulated_real_energy_a_import = MeterRegister(address=317, name="Accumulated Real Energy, Phase A", field_name="Accumulated_Real_Energy_A_Import_FP")
r319_accumulated_real_energy_b_import = MeterRegister(address=319, name="Accumulated Real Energy, Phase B", field_name="Accumulated_Real_Energy_B_Import_FP")
r321_accumulated_real_energy_c_import = MeterRegister(address=321, name="Accumulated Real Energy, Phase C", field_name="Accumulated_Real_Energy_C_Import_FP")
r323_accumulated_real_energy_a_export = MeterRegister(address=323, name="Accumulated Real Energy, Phase A", field_name="Accumulated_Real_Energy_A_Export_FP")
r325_accumulated_real_energy_b_export = MeterRegister(address=325, name="Accumulated Real Energy, Phase B", field_name="Accumulated_Real_Energy_B_Export_FP")
r327_accumulated_real_energy_c_export = MeterRegister(address=327, name="Accumulated Real Energy, Phase C", field_name="Accumulated_Real_Energy_C_Export_FP")
r329_accumulated_q1_reactive_energy_a = MeterRegister(address=329, name="Accumulated Q1 Reactive Energy, Phase A", field_name="Accumulated_Q1_Reactive_Energy_A_FP")
r331_accumulated_q1_reactive_energy_b = MeterRegister(address=331, name="Accumulated Q1 Reactive Energy, Phase B", field_name="Accumulated_Q1_Reactive_Energy_B_FP")
r333_accumulated_q1_reactive_energy_c = MeterRegister(address=333, name="Accumulated Q1 Reactive Energy, Phase C", field_name="Accumulated_Q1_Reactive_Energy_C_FP")
r335_accumulated_q2_reactive_energy_a = MeterRegister(address=335, name="Accumulated Q2 Reactive Energy, Phase A", field_name="Accumulated_Q2_Reactive_Energy_A_FP")
r337_accumulated_q2_reactive_energy_b = MeterRegister(address=337, name="Accumulated Q2 Reactive Energy, Phase B", field_name="Accumulated_Q2_Reactive_Energy_B_FP")
r339_accumulated_q2_reactive_energy_c = MeterRegister(address=339, name="Accumulated Q2 Reactive Energy, Phase C", field_name="Accumulated_Q2_Reactive_Energy_C_FP")
r341_accumulated_q3_reactive_energy_a = MeterRegister(address=341, name="Accumulated Q3 Reactive Energy, Phase A", field_name="Accumulated_Q3_Reactive_Energy_A_FP")
r343_accumulated_q3_reactive_energy_b = MeterRegister(address=343, name="Accumulated Q3 Reactive Energy, Phase B", field_name="Accumulated_Q3_Reactive_Energy_B_FP")
r345_accumulated_q3_reactive_energy_c = MeterRegister(address=345, name="Accumulated Q3 Reactive Energy, Phase C", field_name="Accumulated_Q3_Reactive_Energy_C_FP")
r347_accumulated_q4_reactive_energy_a = MeterRegister(address=347, name="Accumulated Q4 Reactive Energy, Phase A", field_name="Accumulated_Q4_Reactive_Energy_A_FP")
r349_accumulated_q4_reactive_energy_b = MeterRegister(address=349, name="Accumulated Q4 Reactive Energy, Phase B", field_name="Accumulated_Q4_Reactive_Energy_B_FP")
r351_accumulated_q4_reactive_energy_c = MeterRegister(address=351, name="Accumulated Q4 Reactive Energy, Phase C", field_name="Accumulated_Q4_Reactive_Energy_C_FP")
r353_accumulated_apparent_energy_a_import = MeterRegister(address=353, name="Accumulated Apparent Energy, Phase A", field_name="Accumulated_Apparent_Energy_A_Import_FP")
r355_accumulated_apparent_energy_b_import = MeterRegister(address=355, name="Accumulated Apparent Energy, Phase B", field_name="Accumulated_Apparent_Energy_B_Import_FP")
r357_accumulated_apparent_energy_c_import = MeterRegister(address=357, name="Accumulated Apparent Energy, Phase C", field_name="Accumulated_Apparent_Energy_C_Import_FP")
r359_accumulated_apparent_energy_a_export = MeterRegister(address=359, name="Accumulated Apparent Energy, Phase A", field_name="Accumulated_Apparent_Energy_A_Export_FP")
r361_accumulated_apparent_energy_b_export = MeterRegister(address=361, name="Accumulated Apparent Energy, Phase B", field_name="Accumulated_Apparent_Energy_B_Export_FP")
r363_accumulated_apparent_energy_c_export = MeterRegister(address=363, name="Accumulated Apparent Energy, Phase C", field_name="Accumulated_Apparent_Energy_C_Export_FP")
r365_real_power_a = MeterRegister(address=365, name="Real Power, Phase A", field_name="Real_Power_A_FP", flip_polarity=True)
r367_real_power_b = MeterRegister(address=367, name="Real Power, Phase B", field_name="Real_Power_B_FP", flip_polarity=True)
r369_real_power_c = MeterRegister(address=369, name="Real Power, Phase C", field_name="Real_Power_C_FP", flip_polarity=True)
r371_reactive_power_a = MeterRegister(address=371, name="Reactive Power, Phase A", field_name="Reactive_Power_A_FP", flip_polarity=True)
r373_reactive_power_b = MeterRegister(address=373, name="Reactive Power, Phase B", field_name="Reactive_Power_B_FP", flip_polarity=True)
r375_reactive_power_c = MeterRegister(address=375, name="Reactive Power, Phase C", field_name="Reactive_Power_C_FP", flip_polarity=True)
r377_apparent_power_a = MeterRegister(address=377, name="Apparent Power, Phase A", field_name="Apparent_Power_A_FP")
r379_apparent_power_b = MeterRegister(address=379, name="Apparent Power, Phase B", field_name="Apparent_Power_B_FP")
r381_apparent_power_c = MeterRegister(address=381, name="Apparent Power, Phase C", field_name="Apparent_Power_C_FP")
r383_power_factor_a = MeterRegister(address=383, name="Power Factor, Phase A", field_name="Power_Factor_A_FP", flip_polarity=True)
r385_power_factor_b = MeterRegister(address=385, name="Power Factor, Phase B", field_name="Power_Factor_B_FP", flip_polarity=True)
r387_power_factor_c = MeterRegister(address=387, name="Power Factor, Phase C", field_name="Power_Factor_C_FP", flip_polarity=True)
r389_voltage_ab = MeterRegister(address=389, name="Voltage, Phase A-B", field_name="Voltage_AB_FP")
r391_voltage_bc = MeterRegister(address=391, name="Voltage, Phase B-C", field_name="Voltage_BC_FP")
r393_voltage_ac = MeterRegister(address=393, name="Voltage, Phase A-C", field_name="Voltage_AC_FP")
r395_voltage_an = MeterRegister(address=395, name="Voltage, Phase A-N", field_name="Voltage_AN_FP")
r397_voltage_bn = MeterRegister(address=397, name="Voltage, Phase B-N", field_name="Voltage_BN_FP")
r399_voltage_cn = MeterRegister(address=399, name="Voltage, Phase C-N", field_name="Voltage_CN_FP")
r401_current_a = MeterRegister(address=401, name="Current, Phase A", field_name="Current_A_FP")
r403_current_b = MeterRegister(address=403, name="Current, Phase B", field_name="Current_B_FP")
r405_current_c = MeterRegister(address=405, name="Current, Phase C", field_name="Current_C_FP")

veris_registers_257_309 = [
    r257_accumulated_real_energy_net,
    r259_real_energy_import,
    r261_real_energy_export,
    r263_reactive_energy_q1,
    r265_reactive_energy_q2,
    r267_reactive_energy_q3,
    r269_reactive_energy_q4,
    r271_apparent_energy_net,
    r273_apparent_energy_import,
    r275_apparent_energy_export,
    r277_total_net_instantaneous_real_power,
    r279_total_net_instantaneous_reactive_power,
    r281_total_net_instantaneous_apparent_power,
    r283_total_power_factor,
    r285_voltage_ll_avg,
    r287_voltage_ln_avg,
    r289_current_avg,
    r291_frequency,
    r293_total_real_power_present_demand,
    r295_total_reactive_power_present_demand,
    r297_total_apparent_power_present_demand,
    r299_total_real_power_max_demand_import,
    r301_total_reactive_power_max_demand_import,
    r303_total_apparent_power_max_demand_import,
    r305_total_real_power_max_demand_export,
    r307_total_reactive_power_max_demand_export,
    r309_total_apparent_power_max_demand_export
]

veris_registers_317_405 = [
    r317_accumulated_real_energy_a_import,
    r319_accumulated_real_energy_b_import,
    r321_accumulated_real_energy_c_import,
    r323_accumulated_real_energy_a_export,
    r325_accumulated_real_energy_b_export,
    r327_accumulated_real_energy_c_export,
    r329_accumulated_q1_reactive_energy_a,
    r331_accumulated_q1_reactive_energy_b,
    r333_accumulated_q1_reactive_energy_c,
    r335_accumulated_q2_reactive_energy_a,
    r337_accumulated_q2_reactive_energy_b,
    r339_accumulated_q2_reactive_energy_c,
    r341_accumulated_q3_reactive_energy_a,
    r343_accumulated_q3_reactive_energy_b,
    r345_accumulated_q3_reactive_energy_c,
    r347_accumulated_q4_reactive_energy_a,
    r349_accumulated_q4_reactive_energy_b,
    r351_accumulated_q4_reactive_energy_c,
    r353_accumulated_apparent_energy_a_import,
    r355_accumulated_apparent_energy_b_import,
    r357_accumulated_apparent_energy_c_import,
    r359_accumulated_apparent_energy_a_export,
    r361_accumulated_apparent_energy_b_export,
    r363_accumulated_apparent_energy_c_export,
    r365_real_power_a,
    r367_real_power_b,
    r369_real_power_c,
    r371_reactive_power_a,
    r373_reactive_power_b,
    r375_reactive_power_c,
    r377_apparent_power_a,
    r379_apparent_power_b,
    r381_apparent_power_c,
    r383_power_factor_a,
    r385_power_factor_b,
    r387_power_factor_c,
    r389_voltage_ab,
    r391_voltage_bc,
    r393_voltage_ac,
    r395_voltage_an,
    r397_voltage_bn,
    r399_voltage_cn,
    r401_current_a,
    r403_current_b,
    r405_current_c
]
