#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import datetime
from pymodbus.transaction import ModbusRtuFramer
from pymodbus.client.sync import ModbusTcpClient, ModbusSerialClient
from pymodbus.register_write_message import WriteSingleRegisterResponse
from pymodbus.register_read_message import ReadHoldingRegistersResponse
from pymodbus.exceptions import ModbusException
import collections

from meter_register import *
from brainbox import *
from meter.meter import Meter
from meter.exceptions import *


class VerisMeter(Meter):
    MODBUS_SLEEP_BUFFER = 0.08  # seconds

    def __init__(self, **kwargs):
        """
        :param kwargs:
        group_id:       unused (not required)
        address:        virtual serial port (default /dev/<meter_role>, not required)
        baud:           serial port baud rate (default 38400, not required)
        network_comm_type:  modbus connection type: 'rtu' or 'tcp' (default 'rtu', not required)
        ER_Connect:         unused (not required)
        """
        super(VerisMeter, self).__init__(**kwargs)

        self.modbus_device_number = int(get_default(kwargs, 'modbusdevicenumber', 1))
        self.address = kwargs.get('address', '/dev/%s' % self.role).strip()
        self.baud = kwargs.get('baud', 38400)
        self.network_comm_type = get_default(kwargs, 'network_comm_type', 'rtu')

        self.last_valid_read = None
        self.client = None
        self.brainbox = None
        self.veris_registers = veris_registers_257_309 + veris_registers_317_405

    def open(self):
        self.brainbox = Brainbox.find_by(target_interface=self.address)
        if not self.brainbox:
            raise MissingBrainbox()
        self.brainbox.open()
        self.get_modbus_client()

    def sync_subinterval(self):
        self.write_register(r149_num_sub_intervals, 1)
        self.write_register(r150_sub_interval_length, self.subinterval_length)

    def close(self):
        if self.client is not None:
            self.client.close()
        if self.brainbox is not None:
            self.brainbox.close()

    def get_modbus_client(self):
        if self.client is not None:
            self.client.close()

        if self.network_comm_type == 'tcp':
            self.client = ModbusTcpClient(
                        host=self.brainbox.ip_address, 
                        port=502, 
                        framer=ModbusRtuFramer)
        else:
            self.client = ModbusSerialClient(
                    method=self.network_comm_type,
                    port=self.address,
                    baudrate=self.brainbox.baud,
                    unit=self.modbus_device_number,
                    timeout=1,
                    retries=3,
                    reconnects=3)

        self.connected = self.client.connect()

    def combine_bytes(self, msb, lsb):
        return ((msb << 8) & 0xff00) + (lsb & 0x00ff)

    def split_bytes(self, word):
        msb = (word & 0xff00) >> 8
        lsb = word & 0x00ff
        return msb, lsb

    def get_time(self):
        """
        :return: datetime object with
        """
        day, month = self.split_bytes(self.read_register(r155_day_month))
        hour, year = self.split_bytes(self.read_register(r156_hour_year))
        seconds, minutes = self.split_bytes(self.read_register(r157_seconds_minutes))
        return datetime.datetime(int("20%02d" % year), month, day, hour, minutes, seconds)

    def sync_time(self, now):
        """
        Set the meter date/time to now.
        :param now: datetime object with present date/time (i.e. now)
        :return:
        """
        self.write_register(r155_day_month, self.combine_bytes(now.day, now.month))
        self.write_register(r156_hour_year, self.combine_bytes(now.hour, int(str(now.year)[-2:])))
        self.write_register(r157_seconds_minutes, self.combine_bytes(now.second, now.minute))

    def read_register(self, register):
        """
        WARNING: Only supports reading single-precision registers; not double-precision.

        :param register: MeterRegister object
        :return:
        """
        if not register.read:
            self.logger.error("Trying to read a register which is not readable; %s" % register.name)
            raise MeterRegisterUnreadable(register.name)

        try:
            response = self.client.read_holding_registers(
                register.address - 1, register.count, unit=self.modbus_device_number)
            if type(response) is ReadHoldingRegistersResponse:
                return response.getRegister(0)
            else:
                self.logger.error("Unable to read modbus register %s: %s" % (register.name, response))
                return None
        except Exception as ex:
            self.logger.exception("Unable to read modbus register: %s" % register.name)
            return None

    def write_register(self, register, value):
        """
        WARNING: Only supports writing to single-precision registers; double not supported.

        :param register: MeterRegister object
        :param value: integer
        :return:
        """
        if not register.write:
            self.logger.error("Trying to read a register which is read-only; %s" % register.name)
            raise MeterRegisterReadOnly(register.name)

        try:
            response = self.client.write_register(register.address - 1, value, unit=self.modbus_device_number)
            if type(response) is WriteSingleRegisterResponse:
                if response.value == value:
                    return True
            self.logger.error("Unable to write modbus register %s: %s" % (register.name, response))
            return False
        except Exception as ex:
            self.logger.exception("Unable to write modbus register: %s" % register.name)
            return False

    def _read_all_registers(self):
        readings = None
        try:
            success = False
            read_attempts = 0

            while read_attempts < 3 and not success:
                # Try to read the meter 3 times before giving up and getting refreshed settings.
                read_attempts += 1
                try:
                    response = self.client.read_holding_registers(veris_registers_257_309[0].address - 1,
                                                                  len(veris_registers_257_309) * 2,
                                                                  unit=self.modbus_device_number)
                    if issubclass(response.__class__, ModbusException):
                        self.logger.warn("No regs_257_309 read. Read attempt %s: %s" % (read_attempts, response))
                        self.get_modbus_client()
                        success = False
                        time.sleep(VerisMeter.MODBUS_SLEEP_BUFFER)
                        continue
                    holding_registers_257_309 = response
                    response = self.client.read_holding_registers(veris_registers_317_405[0].address - 1,
                                                                  len(veris_registers_317_405) * 2,
                                                                  unit=self.modbus_device_number)
                    if issubclass(response.__class__, ModbusException):
                        self.logger.warn("No regs_317_405 read. Read attempt %s: %s" % (read_attempts, response))
                        self.get_modbus_client()
                        success = False
                        time.sleep(VerisMeter.MODBUS_SLEEP_BUFFER)
                        continue
                    holding_registers_317_405 = response
                    success = True
                except Exception as error:
                    self.logger.exception("read_holding_registers failed. Read attempts: %s" % read_attempts)
                    self.get_modbus_client()
                    success = False
                    time.sleep(VerisMeter.MODBUS_SLEEP_BUFFER)
                    continue

            if not success:
                self.logger.error("no data read")
                return readings

            # combine two 16-bit registers into one 32-bit float reading
            register_iterator = MeterRegisterIterator(holding_registers_257_309)
            for register in veris_registers_257_309:
                register.data = register_iterator.next()

            register_iterator = MeterRegisterIterator(holding_registers_317_405)
            for register in veris_registers_317_405:
                register.data = register_iterator.next()

            self.correct_ct_orientation()
            readings_list = [(r.field_name, r.data) for r in self.veris_registers]
            readings = collections.OrderedDict(readings_list)
        except Exception as ex:
            self.logger.exception("Closing %s meter" % self.name)
        finally:
            return readings

    def correct_ct_orientation(self):
        for register in self.veris_registers:
            if self.ct_orientation_reversed and register.flip_polarity:
                try:
                    register.data = -float(register.data)
                except Exception as ex:
                    pass

        # if CT orientation is reversed we need to swap import/export fields
        # import/export fields seen as sign-less quantities
        if self.ct_orientation_reversed:
            self.swap(r259_real_energy_import,
                      r261_real_energy_export)
            self.swap(r263_reactive_energy_q1,
                      r267_reactive_energy_q3)
            self.swap(r265_reactive_energy_q2,
                      r269_reactive_energy_q4)
            self.swap(r273_apparent_energy_import,
                      r275_apparent_energy_export)
            self.swap(r299_total_real_power_max_demand_import,
                      r305_total_real_power_max_demand_export)
            self.swap(r301_total_reactive_power_max_demand_import,
                      r307_total_reactive_power_max_demand_export)
            self.swap(r303_total_apparent_power_max_demand_import,
                      r309_total_apparent_power_max_demand_export)
            self.swap(r329_accumulated_q1_reactive_energy_a,
                      r341_accumulated_q3_reactive_energy_a)
            self.swap(r331_accumulated_q1_reactive_energy_b,
                      r343_accumulated_q3_reactive_energy_b)
            self.swap(r333_accumulated_q1_reactive_energy_c,
                      r345_accumulated_q3_reactive_energy_c)
            self.swap(r335_accumulated_q2_reactive_energy_a,
                      r347_accumulated_q4_reactive_energy_a)
            self.swap(r337_accumulated_q2_reactive_energy_b,
                      r349_accumulated_q4_reactive_energy_b)
            self.swap(r339_accumulated_q2_reactive_energy_c,
                      r351_accumulated_q4_reactive_energy_c)
            self.swap(r317_accumulated_real_energy_a_import,
                      r323_accumulated_real_energy_a_export)
            self.swap(r319_accumulated_real_energy_b_import,
                      r325_accumulated_real_energy_b_export)
            self.swap(r321_accumulated_real_energy_c_import,
                      r327_accumulated_real_energy_c_export)
            self.swap(r353_accumulated_apparent_energy_a_import,
                      r359_accumulated_apparent_energy_a_export)
            self.swap(r355_accumulated_apparent_energy_b_import,
                      r361_accumulated_apparent_energy_b_export)
            self.swap(r357_accumulated_apparent_energy_c_import,
                      r363_accumulated_apparent_energy_c_export)

    def swap(self, register_a, register_b):
        tmp_data = register_a.data
        register_a.data = register_b.data
        register_b.data = tmp_data
