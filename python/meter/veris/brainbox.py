import subprocess
import urllib2
import os
import signal
import time
import socket

from lib.logging_setup import *
from lib.db import *
from alarm import Alarm, ALARM_ID_BRAINBOX_UNREACHABLE
from lib.model import Model


class Brainbox(Model):
    sql_table = "brainboxes"
    id_field = "brainbox_id"

    DEFAULT_BAUD_RATE = 38400
    DEFAULT_TCP_PORT = 9001
    REMSERIAL_PATH = "/usr/share/apparent/bin/remserial"

    def __init__(self, **kwargs):
        self.id = kwargs['brainbox_id']
        self.virtual_serial_port = kwargs['target_interface']
        self.model_number = kwargs.get('model_number', None)
        self.ip_address = kwargs['IP_Address']
        self.mac_address = kwargs['mac_address']
        self.baud = kwargs.get('baud', self.DEFAULT_BAUD_RATE)
        self.port = kwargs.get('tcp_port', self.DEFAULT_TCP_PORT)
        self.pid = kwargs.get('pid', None)
        self.logger = setup_logger(__name__)
        self.remserial = None

    def open(self):
        # make sure to kill old remserial if present before starting a new one
        if self.pid is not None:
            try:
                os.kill(int(self.pid), signal.SIGTERM)
                self.logger.warning("Killed old remserial (pid=%s) before launching new remserial." % self.pid)
                self.pid = None
            except Exception as ex:
                pass        # old remserial not present which is good

        cmd = "%s -d -r %s -p %s -l %s /dev/ptmx &" % \
              (self.REMSERIAL_PATH, self.ip_address, self.port, self.virtual_serial_port)
        try:
            self.remserial = subprocess.Popen(cmd.split())
            self.pid = self.remserial.pid
        except Exception as ex:
            self.logger.exception("Unable to launch remserial: %s" % cmd)
            self.pid = None
            raise ex
        finally:
            self.save()

        # allow time for remserial to create pseudo-tty so we don't get errors
        time.sleep(1.0)

    def close(self):
        if self.remserial is not None:
            self.remserial.kill()
        self.pid = None
        self.save()

    def save(self):
        sql = "UPDATE brainboxes SET pid = %s WHERE brainbox_id = %s" % ("%s", "%s")
        prepared_act_on_database(EXECUTE, sql, [self.pid, self.id])

    def get_url(self):
        return "http://%s" % self.ip_address

    def unreachable(self):
        unreachable = False
        try:
            urllib2.urlopen(self.get_url(), timeout=0.5)
        except urllib2.URLError as e:
            if type(e.reason) == socket.timeout:
                unreachable = True
        except Exception as ex:
            # Enabling user security on the brainbox web interface causes an access denied
            # unauthorized error that we can safely ignore since all we're concerned about
            # is that we can connect to the brainbox.
            pass

        if unreachable:
            Alarm.occurrence(ALARM_ID_BRAINBOX_UNREACHABLE)
        else:
            Alarm.request_clear(ALARM_ID_BRAINBOX_UNREACHABLE)

        return unreachable
