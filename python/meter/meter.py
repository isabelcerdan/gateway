#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import socket
import time
import os
from datetime import datetime

from lib.gateway_constants.DBConstants import *

from lib.db import get_default
from lib.model import Model
from lib.logging_setup import setup_logger
from energy_review.client import Client
from lib.db import prepared_act_on_database
import alarm
from readings_logger.readings_logger import ReadingsLogger

READINGS_CACHE_REAL_POWER_A              = 'Real_Power_A_FP'
READINGS_CACHE_REAL_POWER_B              = 'Real_Power_B_FP'
READINGS_CACHE_REAL_POWER_C              = 'Real_Power_C_FP'
READINGS_CACHE_TOTAL_NET_INST_REAL_POWER = 'Total_Net_Instantaneous_Real_Power_FP'
READINGS_CACHE_CURRENT_A                 = 'Current_A_FP'
READINGS_CACHE_CURRENT_B                 = 'Current_B_FP'
READINGS_CACHE_CURRENT_C                 = 'Current_C_FP'

class Meter(Model):
    sql_table = 'energy_meter'

    TYPE_VERIS_BRAINBOX = "veris_brainbox"
    TYPE_REMOTE = "remote"
    TYPE_APPARENT = "apparent"

    SAMPLES_IN_15_DAYS = 4 * 60 * 60 * 24 * 15  # 5184000 is the number of samples in 15 days

    role_to_meter_offline_alarm = {
        METER_ROLE_PCC1: alarm.ALARM_ID_PCC_METER_OFFLINE,
        METER_ROLE_PCC2: alarm.ALARM_ID_PCC2_METER_OFFLINE,
        METER_ROLE_SOLAR1: alarm.ALARM_ID_SOLAR_METER_OFFLINE,
        METER_ROLE_BATTERY1: alarm.ALARM_ID_BATTERY_METER_OFFLINE,
        METER_ROLE_BATTERY2: alarm.ALARM_ID_BATTERY_METER_OFFLINE,
        METER_ROLE_BATTERY3: alarm.ALARM_ID_BATTERY_METER_OFFLINE,
        METER_ROLE_BATTERY4: alarm.ALARM_ID_BATTERY_METER_OFFLINE
    }

    def __init__(self, **kwargs):
        """
        :param kwargs:
        meter_id:       integer uniquely identifying meter (not required)
        meter_name:     string identifying meter (not required)
        mac_address:    unused (not required)
        meter_role:     string identifying role: "solar", "pcc","pcc2" (required)
        meter_class:    string identifying meter class (default "48", not required)
        address:        virtual serial port (default /dev/<meter_role>, not required)
        feed_of_phase_A:    feed of phase A if not one-to-one mapping (default 'A', not required)
        feed_of_phase_B:    feed of phase B if not one-to-one mapping (default 'B', not required)
        feed_of_phase_C:    feed of phase C if not one-to-one mapping (default 'C', not required)
        ER_Connect:         unused (not required)
        """

        self.id = kwargs.get('meter_id', None)
        self.name = kwargs.get('meter_name', None)
        self.mac_address = kwargs.get('mac_address', None)
        self.role = get_default(kwargs, 'meter_role', 'pcc').lower()
        self.meter_class = get_default(kwargs, 'meter_class', '48')
        self.address = kwargs.get('address', '/dev/%s' % self.role).strip()
        self.er_connect = kwargs.get('ER_Connect', False)
        self.online_timeout = get_default(kwargs, 'online_timeout', 2.0)
        self.online = kwargs.get('online', True)
        self.delta = kwargs.get('delta', False)
        self.ct_orientation_reversed = kwargs.get('ct_orientation_reversed', False)
        self.enabled = kwargs.get('enabled', False)
        self.started = kwargs.get('started', False)
        self.pid = kwargs.get('pid', None)
        self.updated = kwargs.get('updated', None)
        self.type = kwargs.get('meter_type', Meter.TYPE_VERIS_BRAINBOX)
        self.readings_logger = ReadingsLogger(basename=(self.role + "_readings"))
        self.logger = setup_logger(self.role)

        self.connected = False
        self.last_valid_read = None

        er_client = Client.find()
        self.subinterval_length = er_client.interval if er_client else 900

        # initialize the readings cache
        self.readingsCache = self.build_readings_cache()
        self.defaultReadingsCache = self.build_readings_cache()


    def _log(self, logStr):
        try:
            self.dumpFile = open("/var/log/apparent/meter_feed.log", 'a')
            now = time.time()
            nowStr = time.strftime(" %H:%M:%S.")
            msec = int(round(now * 1000)) % 1000
            self.dumpFile.write(str(os.getpid()) + nowStr + ("%03u  %s " % (msec, self.name) + logStr))

            self.dumpFile.write("\r\n")
            self.dumpFile.flush()
            self.dumpFile.close()

        except Exception:  # as err_string:
            print("FAILED TO OPEN /var/log/apparent/meter_feed.log FILE")

    def open(self):
        pass

    def close(self):
        pass

    def sync_subinterval(self):
        pass

    def sync_time(self, dt):
        pass

    def reconnect(self):
        self.close()
        self.open()

    def read_all_registers(self):
        readings = self._read_all_registers()
        if readings is not None:
            self.last_valid_read = time.time()
            self.write_reading(readings)
        else:
            self.logger.error("No readings were read from meter %s." % self.name)

    def write_reading(self, readings):
        updatedReading = self.write_nxo_readings_to_database(readings)
        self.write_readings_to_database(readings)

        if updatedReading:
            # data already in OrderedDict format
            self.readings_logger.write_reading(readings)

        '''
        else:
            self._log("Duplicate reading")
        '''

    def build_readings_cache(self, readings = None):
        cache = {}
        if readings == None:
            cache[READINGS_CACHE_REAL_POWER_A] = 0.0
            cache[READINGS_CACHE_REAL_POWER_B] = 0.0
            cache[READINGS_CACHE_REAL_POWER_C] = 0.0
            cache[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER] = 0.0
            cache[READINGS_CACHE_CURRENT_A] = 0.0
            cache[READINGS_CACHE_CURRENT_B] = 0.0
            cache[READINGS_CACHE_CURRENT_B] = 0.0

        else:
            cache[READINGS_CACHE_REAL_POWER_A] = readings[READINGS_CACHE_REAL_POWER_A]
            cache[READINGS_CACHE_REAL_POWER_B] = readings[READINGS_CACHE_REAL_POWER_B]
            cache[READINGS_CACHE_REAL_POWER_C] = readings[READINGS_CACHE_REAL_POWER_C]
            cache[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER] = readings[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER]
            cache[READINGS_CACHE_CURRENT_A] = readings[READINGS_CACHE_CURRENT_A]
            cache[READINGS_CACHE_CURRENT_B] = readings[READINGS_CACHE_CURRENT_B]
            cache[READINGS_CACHE_CURRENT_B] = readings[READINGS_CACHE_CURRENT_B]

        return cache

    def write_nxo_readings_to_database(self, readings):
        '''
         :param readings:
        :return: True if written to database, else return False
                 A False value indicates a duplicate values (of the one we care about)
        '''

        # Set unread values to 0.0 prior to loading of the
        # database
        for k,v in readings.items():
            if v == 'NULL':
                #print k,v
                readings[k] = 0.0

        # build list of common reading values to load to the database
        this_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        realPowerA = readings[READINGS_CACHE_REAL_POWER_A]
        realPowerB = readings[READINGS_CACHE_REAL_POWER_B]
        realPowerC = readings[READINGS_CACHE_REAL_POWER_C]
        realPowerTotal = readings[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER]
        currentA = readings[READINGS_CACHE_CURRENT_A]
        currentB = readings[READINGS_CACHE_CURRENT_B]
        currentC = readings[READINGS_CACHE_CURRENT_B]

        #build dictionary of values to combine
        cache = self.build_readings_cache(readings)

        # check if duplicate reading
        # Ignore new data, returning True
        if cache == self.readingsCache:
            return False
        else:
            self.readingsCache = cache

        # Update the various database tables with reading data
        if (self.role == 'pcc'):
            # get PPC_2 Readings for the combo and accum values
            sql = "SELECT * FROM %s WHERE %s=%s AND %s='%s'" % \
                  (DB_METER_READINGS_TABLE,
                   METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                   METER_DATA_TYPE_LAST_VALUE,
                   METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                   METER_ROLE_PCC2)

            otherSide = prepared_act_on_database(FETCH_ONE, sql, [])
            # check if otherside data exists or set to None
            if not otherSide or (otherSide[READINGS_CACHE_REAL_POWER_A] is None):
                otherSide = self.defaultReadingsCache


            # update NXO_STATUS table
            sql = "UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s" % \
                                                        (DB_NXO_STATUS_TABLE,
                                                         NXO_STATUS_EXPORT_PCC_1_A, "%s",
                                                         NXO_STATUS_EXPORT_PCC_1_B, "%s",
                                                         NXO_STATUS_EXPORT_PCC_1_C, "%s",
                                                         NXO_STATUS_EXPORT_PCC_1_DELTA, "%s",
                                                         NXO_STATUS_EXPORT_PCC_COMBO_A, "%s",
                                                         NXO_STATUS_EXPORT_PCC_COMBO_B, "%s",
                                                         NXO_STATUS_EXPORT_PCC_COMBO_C, "%s",
                                                         NXO_STATUS_EXPORT_PCC_COMBO_DELTA, "%s",
                                                         NXO_STATUS_TIMESTAMP_PCC_1, "%s")

            args = [realPowerA,
                    realPowerB,
                    realPowerC,
                    realPowerTotal,
                    realPowerA + otherSide[READINGS_CACHE_REAL_POWER_A],
                    realPowerB + otherSide[READINGS_CACHE_REAL_POWER_B],
                    realPowerC + otherSide[READINGS_CACHE_REAL_POWER_C],
                    realPowerTotal + otherSide[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER],
                    this_date_time]

            prepared_act_on_database(EXECUTE, sql, args)

        elif (self.role == 'pcc2'):
            # get PPC_1 Readings for the combo and accum values
            sql = "SELECT * FROM %s WHERE %s=%s AND %s='%s'" % \
                  (DB_METER_READINGS_TABLE,
                   METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                   METER_DATA_TYPE_LAST_VALUE,
                   METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                   METER_ROLE_PCC1)

            otherSide = prepared_act_on_database(FETCH_ONE, sql, [])
            # check if otherside data exists or set to None
            if not otherSide or (otherSide[READINGS_CACHE_REAL_POWER_A] is None):
                otherSide = self.defaultReadingsCache

            sql = "UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s, %s=%s" % \
                  (DB_NXO_STATUS_TABLE,
                   NXO_STATUS_EXPORT_PCC_2_A, "%s",
                   NXO_STATUS_EXPORT_PCC_2_B, "%s",
                   NXO_STATUS_EXPORT_PCC_2_C, "%s",
                   NXO_STATUS_EXPORT_PCC_2_DELTA, "%s",
                   NXO_STATUS_EXPORT_PCC_COMBO_A, "%s",
                   NXO_STATUS_EXPORT_PCC_COMBO_B, "%s",
                   NXO_STATUS_EXPORT_PCC_COMBO_C, "%s",
                   NXO_STATUS_EXPORT_PCC_COMBO_DELTA, "%s",
                   NXO_STATUS_TIMESTAMP_PCC_2, "%s")

            args = [realPowerA,
                    realPowerB,
                    realPowerC,
                    realPowerTotal,
                    realPowerA + otherSide[READINGS_CACHE_REAL_POWER_A],
                    realPowerB + otherSide[READINGS_CACHE_REAL_POWER_B],
                    realPowerC + otherSide[READINGS_CACHE_REAL_POWER_C],
                    realPowerTotal + otherSide[READINGS_CACHE_TOTAL_NET_INST_REAL_POWER],
                    this_date_time]

            prepared_act_on_database(EXECUTE, sql, args)

        elif (self.role == 'solar'):
            self.readingsCacheSolar = cache
            sql = "UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s, %s=%s" % \
                  (DB_NXO_STATUS_TABLE,
                   NXO_STATUS_EXPORT_SOLAR_1_A, "%s",
                   NXO_STATUS_EXPORT_SOLAR_1_B, "%s",
                   NXO_STATUS_EXPORT_SOLAR_1_C, "%s",
                   NXO_STATUS_EXPORT_SOLAR_1_DELTA, "%s",
                   NXO_STATUS_TIMESTAMP_SOLAR_1, "%s")

            args = [realPowerA,
                    realPowerB,
                    realPowerC,
                    realPowerTotal,
                    this_date_time]
            prepared_act_on_database(EXECUTE, sql, args)

        return True


    def write_readings_to_database(self, readings):
        '''
        Row 1 is the latest data
        Row 2 is data written at the subinterval_length time hack

        :param readings:
        :return:
        '''
        now = datetime.now()
        sec = now.second

        mysql_data = ",".join(["%s=%s" % (k, "%s") for k, _ in readings.items()])
        mysql_args = readings.values()

        mysql_data += ",timestamp=%s"
        mysql_args.append(str(now))
        mysql_args.append(str(self.role))

        sql = "UPDATE %s SET %s WHERE  %s=%s AND %s=%s" % \
              (DB_METER_READINGS_TABLE,
               mysql_data,
               METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
               METER_DATA_TYPE_LAST_VALUE,
               METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
               "%s")

        prepared_act_on_database(EXECUTE, sql, mysql_args)

        if ((((self.subinterval_length == 0) or (self.subinterval_length > 59)) and (sec == 0)) or
            ((sec % self.subinterval_length) == 0)):

            sql = "UPDATE %s SET %s WHERE  %s=%s AND %s=%s" % \
                  (DB_METER_READINGS_TABLE,
                   mysql_data,
                   METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                   METER_DATA_TYPE_INTERVAL_VALUE,
                   METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                   "%s")

            prepared_act_on_database(EXECUTE, sql, mysql_args)

    def update_online(self):
        if self.last_valid_read is None:
            online = False
        else:
            seconds_elapsed = time.time() - self.last_valid_read
            online = seconds_elapsed < self.online_timeout

        if self.online != online:
            if self.online and not online:
                self.logger.error("Detected %s meter went offline!!" % self.name)
                alarm.Alarm.occurrence(self.meter_offline_alarm_id())
            elif not self.online and online:
                self.logger.info("Detected %s meter came back online." % self.name)
                alarm.Alarm.request_clear(self.meter_offline_alarm_id())
            self.online = online
        self.save_online()

    def meter_offline_alarm_id(self):
        return Meter.role_to_meter_offline_alarm[self.role]

    def read_register(self, register):
        raise Exception("Method must be overridden in subclass.")

    def write_register(self, register, value):
        raise Exception("Method must be overridden in subclass.")

    def _read_all_registers(self):
        raise Exception("Method must be overridden in subclass.")

    def correct_ct_orientation(self):
        raise Exception("Method must be overridden in subclass.")

    def reload_online(self):
        sql = "SELECT online FROM energy_meter WHERE meter_id=%s" % "%s"
        record = prepared_act_on_database(FETCH_ONE, sql, [self.id])
        if record is not None:
            self.online = record['online']

    def save_online(self):
        sql = "UPDATE energy_meter SET online=%s WHERE meter_id=%s" % ("%s", "%s")
        args = [self.online, self.id]
        prepared_act_on_database(EXECUTE, sql, args)

    def current_interval_readings(self):
        """
        Get meter readings for most recent sub-interval.
        Row 1 is the latest data
        Row 2 is data written at the subinterval_length time hack
        :return: A dictionary of readings matching energy_report_catchup schema
            or None if no readings found for the most recent sub-interval.
        """

        sql = "SELECT * FROM %s WHERE  %s=%s AND %s=%s" % \
              (DB_METER_READINGS_TABLE,
               METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
               METER_DATA_TYPE_INTERVAL_VALUE,
               METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
               '%s')

        readings = prepared_act_on_database(FETCH_ONE, sql, (self.role,))
        if not readings:
            return None

        # get modbus device number and brainbox id for current meter
        sql = "SELECT modbusdevicenumber, brainbox_id FROM %s JOIN %s ON address = target_interface " \
              "WHERE meter_role = %s;" % (DB_METER_NXO_TABLE, DB_TUNNEL_TABLE, '%s')
        result = prepared_act_on_database(FETCH_ONE, sql, (self.role,))
        if not result:
            self.logger.error("No meters with role=%s" % self.role)
            return None

        # Add brainbox_id and modbus device number to readings
        readings['brainbox_id'] = result['brainbox_id']
        readings['modbusdevicenumber'] = result['modbusdevicenumber']

        return readings

    @staticmethod
    def is_ip_address(addr):

        try:
            socket.inet_aton(addr)
            return True
        except socket.error:
            return False

    @staticmethod
    def find_by(**kwargs):
        sql = "SELECT * FROM energy_meter"
        where_clause = " AND ".join(["%s = '%s'" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        record = prepared_act_on_database(FETCH_ONE, sql, [])
        meter = None
        if record is not None:
            if record['meter_type'] == Meter.TYPE_REMOTE:
                import remote_meter
                meter = remote_meter.RemoteMeter(**record)
            elif record['meter_type'] == Meter.TYPE_VERIS_BRAINBOX:
                import veris.veris_meter
                meter = veris.veris_meter.VerisMeter(**record)
        return meter

    @staticmethod
    def find_all_by(**kwargs):
        sql = "SELECT * FROM energy_meter"
        where_clause = " AND ".join(["%s = '%s'" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        records = prepared_act_on_database(FETCH_ALL, sql, [])
        meters = []
        for record in records:
            if record['meter_type'] == Meter.TYPE_REMOTE:
                import remote_meter
                meters.append(remote_meter.RemoteMeter(**record))
            elif record['meter_type'] == Meter.TYPE_VERIS_BRAINBOX:
                import veris.veris_meter
                meters.append(veris.veris_meter.VerisMeter(**record))
        return meters
