class MeterException(Exception):
    def __init__(self, string):
        self.string = string

    def __str__(self):
        return "[Meter Exception]: %s" % self.string


class MissingBrainbox(MeterException):
    def __init__(self):
        MeterException.__init__(self, "Meter does not have an associated brainbox.")


class MisconfiguredBrainbox(MeterException):
    def __init__(self, brainbox):
        string = "Brianbox misconfigured: ip=%s,port=%s,path=%s" % \
                 (brainbox.ip_address, brainbox.port, brainbox.virtual_serial_port)
        MeterException.__init__(self, string)

class MeterRegisterReadOnly(MeterException):
    def __init__(self, regName):
        MeterException.__init__(self, ("Trying to read a register which is read-only; %s" % regName))

class MeterRegisterUnreadable(MeterException):
    def __init__(self, regName):
        MeterException.__init__(self, ("Trying to read a register which is not readable; %s" % regName))

