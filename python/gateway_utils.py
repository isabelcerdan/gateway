#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import struct
import socket
import fcntl

import httplib
import os
import subprocess
import time
from time import mktime, strptime
import datetime


from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.GatewayConstants import *
from lib.gateway_constants.ArtesynConstants import *
from lib.logging_setup import *
# JEP from lib.db import prepared_act_on_database
from lib.db import prepared_act_on_database, FETCH_ONE, FETCH_ALL, EXECUTE

from math import sqrt

from alarm.alarm import Alarm

from alarm.alarm_constants import ALARM_ID_GTW_UTILS_SEND_CLI_COMMAND_BY_TLV_ERROR, \
                                  ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR, \
                                  ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR, \
                                  ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR, \
                                  ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR, \
                                  ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR, \
                                  ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR, \
                                  ALARM_ID_GTW_UTILS_GET_TLV_LENGTH_ERROR, \
                                  ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_WAS_ZERO_ERROR, \
                                  ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_EXCEEDS_PACKED_DATA_ERROR, \
                                  ALARM_ID_GTW_UTILS_INVALID_PF_COMMAND_TLV, \
                                  ALARM_ID_GTW_UTILS_INVALID_SET_AIF_IMMED_COMMAND, \
                                  ALARM_ID_GTW_UTILS_INVALID_SET_AIF_SETTINGS_COMMAND, \
                                  ALARM_ID_PCS_UNIT_ID_ASSOCIATION_BMU_ID_0_DETECTED, \
                                  ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RESET_DEFAULTS, \
                                  ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RE_CREATED


logger = setup_logger('gateway_utils')

#
#
# General purpose gateway utilities module.
#
#

VALID_HEX = '0''1''2''3''4''5''6''7''8''9''A''B''C''D''E''F''a''b''c''d''e''f'

# -------------------------------------------------------------------------------------------------
#
#  An itty bitty dictionary set to help the construction of a multicast IP address:


# TODO: add support to send to non-gateway inverters as par of the "GROUPS" feature.


valid_inverter_type = {INVERTER_MCAST_SG424_TYPE_ALL, INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY}
valid_pcc_type = {PCC_ALL_MULTICAST, PCC_1_MULTICAST, PCC_2_MULTICAST}
valid_feed = {FEED_ALL_MULTICAST,
              FEED_A_MULTICAST,
              FEED_B_MULTICAST,
              FEED_AB_MULTICAST,
              FEED_C_MULTICAST,
              FEED_AC_MULTICAST,
              FEED_BC_MULTICAST
             }
mcast_by_inverter_type = {INVERTER_MCAST_SG424_TYPE_ALL          : MCAST_TYPE_SG424_ALL_BITS,
                          INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY   : MCAST_TYPE_SG424_SOLAR_ONLY_BITS,
                          INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY : MCAST_TYPE_SG424_BATTERY_ONLY_BITS
                         }
mcast_by_pcc_type = {PCC_ALL_MULTICAST : MCAST_PCC_ALL_BITS,
                     PCC_1_MULTICAST   : MCAST_PCC_1_BITS,
                     PCC_2_MULTICAST   : MCAST_PCC_2_BITS
                    }
mcast_by_feed = {FEED_ALL_MULTICAST : MCAST_FEED_ALL_BITS,
                 FEED_A_MULTICAST   : MCAST_FEED_A_BITS,
                 FEED_B_MULTICAST   : MCAST_FEED_B_BITS,
                 FEED_AB_MULTICAST  : MCAST_FEED_AB_BITS,
                 FEED_C_MULTICAST   : MCAST_FEED_C_BITS,
                 FEED_AC_MULTICAST  : MCAST_FEED_AC_BITS,
                 FEED_BC_MULTICAST  : MCAST_FEED_BC_BITS
                }
mcast_inverter_type_bits = {MCAST_TYPE_SG424_SOLAR_ONLY_BITS, MCAST_TYPE_SG424_BATTERY_ONLY_BITS, MCAST_TYPE_SG424_ALL_BITS}
mcast_pcc_bits = {MCAST_PCC_ALL_BITS, MCAST_PCC_1_BITS, MCAST_PCC_2_BITS}
mcast_feed_bits = {MCAST_FEED_ALL_BITS, MCAST_FEED_A_BITS, MCAST_FEED_B_BITS, MCAST_FEED_C_BITS, MCAST_FEED_AB_BITS, MCAST_FEED_AC_BITS, MCAST_FEED_BC_BITS}


# -------------------------------------------------------------------------------------------------


class GATEWAYUtilities(object):

    def __init__(self):
        # There are no special or peculiar initialization requirements.
        pass


def print_this_hex_data(some_hex_data):
    # Print raw hex data.
    this_string = "HEX DATA (LEN %s): " % (len(some_hex_data))
    for char_char in some_hex_data:
        this_string += hex(ord(char_char))[2:].zfill(2) + " "
    print(this_string)


def get_gateway_ip_address(ifname):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # 0x8915: SIOCGIFADDR
        addr = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
        s.close()
    except (socket.errno, IOError) as err:
        logger.error("Unable to get gateway's i/f %s address - %s" % (ifname, err))
        addr = None
    return addr


def raise_and_clear_alarm(this_alarm):
    Alarm.occurrence(this_alarm)
    Alarm.request_clear(this_alarm)


def add_master_tlv(number_of_user_tlv):
    # Add the MASTER TLV to a byte array. 6 bytes are returned, where:
    # - bytes [0:1] = MASTER TYPE
    # - bytes [2:3] = Length of the Value field
    # - bytes [4:5] = Value, the parameter passed to this method.
    packed_data = struct.pack('!HHH', GTW_TO_SG424_MASTER_U16_TLV_TYPE, MASTER_TLV_DATA_FIELD_LENGTH, number_of_user_tlv)
    return packed_data


# -----------------------------------------------------------------------------
# A SMALL SET OF 16-BIT FUNCTIONS: - append_user_tlv_16bit_unsigned_short
# -                                - append_user_tlv_16bit_signed_short
# -                                - get_tlv_data_16bit_unsigned_short
#                                  - get_tlv_data_16bit_signed_short
def add_user_tlv_16bit_unsigned_short(tlv_type, data):
    # This method used when creating the 1st user TLV (i.e., add instead of append).
    # Add the 16-bit UNSIGNED SHORT "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in a SHORT i.e.: 2
    # - bytes [4:5] = Value; the parameter passed to this method.
    # NOTE: very subtle, unsigned short packing format is the 3rd H = *UPPER CASE!!!*
    packed_data = struct.pack('!HHH', tlv_type, 2, data)
    return packed_data


def append_user_tlv_16bit_unsigned_short(packed_data, tlv_type, data):
    # Append the 16-bit UNSIGNED SHORT "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in a SHORT i.e.: 2
    # - bytes [4:5] = Value; the parameter passed to this method.
    # NOTE: very subtle, unsigned short packing format is the 3rd H = *UPPER CASE!!!*
    packed_data += struct.pack('!HHH', tlv_type, 2, data)
    return packed_data


def append_user_tlv_16bit_signed_short(packed_data, tlv_type, data):
    # Append the 16-bit SIGNED SHORT "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type ***AN UNSIGNED SHORT***
    # - bytes [2:3] = Length of the Value field: number of bytes in a SHORT i.e.: 2 ***AN UNSIGNED SHORT***
    # - bytes [4:5] = Value "data": the parameter passed to this method
    # NOTE: very subtle, signed short packing format is the 3rd h = *lower case!!!*
    packed_data += struct.pack('!HHh', tlv_type, 2, data)
    return packed_data


def get_tlv_data_16bit_unsigned_short(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract/validate the Length value
    # - extract/return the 16-bit SHORT from the Value field
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # NOTE: very subtle, unsigned short un-packing format is the 2nd H = *UPPER CASE!!!*
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 4]
        (tlv_length, tlv_value_short) = struct.unpack('!HH', this_tlv)
        if tlv_length == 2:
            # Advance the rank so that upon return, it will index the next TLV.
            tlv_rank = tlv_rank + 4
        else:
            # Invalid length: a SHORT is 2 bytes. Set rank to 0 to signal a malformed packet.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR)
            tlv_rank = 0
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR)
        tlv_rank = 0
    return (tlv_value_short, tlv_rank)


def get_tlv_data_16bit_signed_short(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract/validate the Length value
    # - extract/return the 16-bit SIGNED SHORT from the Value field
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # NOTE: very subtle, signed short un-packing format is the 2nd h = *lower case!!!*
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 4]
        (tlv_length, tlv_value_short) = struct.unpack('!Hh', this_tlv)
        if tlv_length == 2:
            # Advance the rank so that upon return, it will index the next TLV.
            tlv_rank = tlv_rank + 4
        else:
            # Invalid length: a SHORT is 2 bytes. Set rank to 0 to signal a malformed packet.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR)
            tlv_rank = 0
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_16BIT_ERROR)
        tlv_rank = 0
    return (tlv_value_short, tlv_rank)
# END OF THE SMALL SET OF 16-BIT FUNCTIONS.
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# A SMALL SET OF 32-BIT FUNCTIONS: - append_user_tlv_32bit_unsigned_long
#                                  - append_user_tlv_32bit_signed_long
#                                  - append_user_tlv_32bit_float
#                                  - get_tlv_data_32bit_unsigned_long
#                                  - get_32bit_unsigned_long_from_hex_data
#                                  - get_32bit_signed_long_from_hex_data
#                                  - get_tlv_data_32bit_float
#                                  - add_user_tlv_32bit_unsigned_long   <- this is an "ADD"
def append_user_tlv_32bit_unsigned_long(packed_data, tlv_type, data):
    # Append the 32-bit UNSIGNED LONG "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type ***AN UNSIGNED SHORT***
    # - bytes [2:3] = Length of the Value field: number of bytes in a LONG, i.e.: 4
    # - bytes [4:5:6:7] = Value "data": the parameter passed to this method
    # NOTE: very subtle, unsigned long packing format is the 3rd L = *UPPER CASE!!!*
    packed_data += struct.pack('!HHL', tlv_type, 4, data)
    return packed_data


def append_user_tlv_32bit_signed_long(packed_data, tlv_type, data):
    # NOTE: NO USE HAS BEEN MADE OF THIS FUNCTION IN THE SYSTEM SO FAR.
    # Append the 32-bit SIGNED LONG "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type ***AN UNSIGNED SHORT***
    # - bytes [2:3] = Length of the Value field: number of bytes in a LONG, i.e.: 4
    # - bytes [4:5:6:7] = Value "data": the parameter passed to this method
    # NOTE: very subtle, signed long-unpacking format is the 3rd l = *lower case!!!*
    packed_data += struct.pack('!HHl', tlv_type, 4, data)
    return packed_data


def append_user_tlv_32bit_float(packed_data, tlv_type, data):
    # Append the 32-bit FLOAT "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in a FLOAT, i.e.: 4
    # - bytes [4:5:6:7] = Value: the parameter passed to this method.
    # NOTE: very subtle, 32-bit float packing format is the 3rd f = *lower case!!!*
    packed_data += struct.pack('!HHf', tlv_type, 4, data)
    return packed_data


def get_tlv_data_32bit_unsigned_long(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract/validate the Length value
    # - extract/return the 32-bit UNSIGNED LONG from the Value field
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # NOTE: very subtle, unsigned long un-packing format is the 2nd L = *UPPER CASE!!!*
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 6]
        (tlv_length, tlv_value_long) = struct.unpack('!HL', this_tlv)
        if tlv_length == 4:
            # Advance the rank so that upon return, it will index the next TLV.
            tlv_rank = tlv_rank + 6
        else:
            # Invalid length: a LONG is 4 bytes. Set rank to 0 to signal a malformed packet.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
            tlv_rank = 0
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        # No point doing this until alarm interface is included:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
        tlv_rank = 0
    return (tlv_value_long, tlv_rank)


def get_tlv_data_32bit_signed_long(packed_data, tlv_rank):
    # NOTE: NO USE HAS BEEN MADE OF THIS FUNCTION IN THE SYSTEM SO FAR.
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract/validate the Length value
    # - extract/return the 32-bit SIGNED LONG from the Value field
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # NOTE: very subtle, signed long un-packing format is the 2nd l = *lower case!!!*
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 6]
        (tlv_length, tlv_value_long) = struct.unpack('!Hl', this_tlv)
        if tlv_length == 4:
            # Advance the rank so that upon return, it will index the next TLV.
            tlv_rank = tlv_rank + 6
        else:
            # Invalid length: a LONG is 4 bytes. Set rank to 0 to signal a malformed packet.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
            tlv_rank = 0
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        # No point doing this until alarm interface is included:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
        tlv_rank = 0
    return (tlv_value_long, tlv_rank)


def get_32bit_unsigned_long_from_hex_data(hex_data, hex_rank):
    # This function differs from get_tlv_data_32bit_unsigned_long in that it extracts a
    # 32-bit UNSIGNED LONG based on an index into a packed byte array (no preceeding TLV):
    #     - extract 4 bytes from the byte array
    #     - format the 4-bytes as an UNSIGNED LONG
    #     - returns the 32-bit UNSIGNED LONG and the rank incremented by 4
    # NOTE: very subtle, unsigned long un-packing format is the L = *UPPER CASE!!!*
    these_4_bytes = hex_data[hex_rank:hex_rank + 4]
    (this_unsigned_long,) = struct.unpack('!L', these_4_bytes)
    return (this_unsigned_long, hex_rank + 4)


def get_tlv_data_32bit_float(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract/validate the Length value
    # - extract/return the 32-bit FLOAT from the Value field
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # NOTE: very subtle, 32-bit float un-packing format is the 2nd f = *lower case!!!*
    tlv_value_float = ''
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 6]
        (tlv_length, tlv_value_float) = struct.unpack('!Hf', this_tlv)
        if tlv_length == 4:
            # Advance the rank so that upon return, it will index the next TLV.
            tlv_rank = tlv_rank + 6
        else:
            # Invalid length: a LONG is 4 bytes. Set rank to 0 to signal a malformed packet.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
            tlv_rank = 0
    else:
        # SEEN DURING TESTING: there was a bug on the SG424, in which the number of user TLVs was 2, but in fact,
        # the packed data contained only 1 user TLV.
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_32BIT_ERROR)
        tlv_rank = 0
    return (tlv_value_float, tlv_rank)


def add_user_tlv_32bit_unsigned_long(tlv_type, data):
    # This method used when creating the 1st user TLV (i.e., add instead of append).
    # Add the 32-bit UNSIGNED LONG "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in a SHORT i.e.: 2
    # - bytes [4:5:6:7] = Value; the parameter passed to this method.
    packed_data = struct.pack('!HHL', tlv_type, 4, data)
    return packed_data


def add_user_tlv_32bit_float(tlv_type, data):
    # This method used when creating the 1st user TLV (i.e., add instead of append).
    # Add the 32-bit float to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in a FLOAT, i.e.: 4
    # - bytes [4:5:6:7] = Value; the parameter passed to this method.
    # NOTE: very subtle, 32-bit float packing format is the 3rd f = *lower case!!!*
    packed_data = struct.pack('!HHf', tlv_type, 4, data)
    return packed_data







# END OF THE SMALL SET OF 32-BIT FUNCTIONS.
# -----------------------------------------------------------------------------




# -----------------------------------------------------------------------------
# A SMALL SET OF GENERAL PURPOSE TLV FUNCTIONS: - def get_tlv_header_length
#                                               - get_tlv_type
#                                               - append_user_tlv_string
#                                               - append_user_tlv_string_with_invalid_length
#                                               - append_user_tlv_mac_as_hex
#                                               - append_user_hex_numbers


def get_tlv_header_length(packed_data, tlv_rank):
    # The tlv_rank is indexing the 1st byte of a TLV. Return the Length field, as well
    # as update/return the tlv_rank to index the 1st byte of the Value field of the TLV.
    tlv_header_length = packed_data[tlv_rank:tlv_rank + 2]
    (tlv_length,) = struct.unpack('!H', tlv_header_length)
    tlv_rank += 2
    return (tlv_length, tlv_rank)


def get_tlv_type(packed_data, tlv_rank):
    # The tlv_rank is indexing the 1st byte of a TLV. Validate/return the TLV TYPE, and
    # update/return the tlv_rank to index the 1st byte of the Length field of the TLV.
    packed_data_length = len(packed_data)
    tlv_type = SG424_TO_GTW_INVALID_TLV_TYPE
    if tlv_rank < packed_data_length:
        this_tlv = packed_data[tlv_rank:tlv_rank + 2]
        (tlv_type,) = struct.unpack('!H', this_tlv)
        tlv_rank += 2
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_LENGTH_ERROR)
        tlv_rank = 0
    return (tlv_type, tlv_rank)


def skip_to_next_tlv(packed_data, tlv_rank):
    # This function generally used when an "un-wanted" TLV (well, either unexpected or it's donut care) is encountered
    # in a packed byte stream. The tlv_rank is indexing the length field of a TLV-encoded data in a byte stream. Based
    # on this length, get the rank of the next TLV beyond. If this length pushes the rank past the end of the byte
    # stream, it is a malformed TLV packet, in which case return a tlv rank of 0 to stop processing.
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        this_packed_length = packed_data[tlv_rank:tlv_rank + 2]
        (tlv_length,) = struct.unpack('!H', this_packed_length)
        # Advance the rank so that upon return, it will index the next TLV.
        tlv_rank = tlv_rank + tlv_length + 2
        if (tlv_rank % 2) != 0:
            # Odd length - add one extra to get it back on an even-boundary.
            tlv_rank += 1
        if tlv_rank > packed_data_length:
            # Returning a rank of 0 will ensure that the caller stops processing the packed data.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_EXCEEDS_PACKED_DATA_ERROR)
            tlv_rank = 0
    else:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_NEXT_TLV_RANK_WAS_ZERO_ERROR)
        tlv_rank = 0
    return tlv_rank


def append_user_tlv_string(packed_data, tlv_type, data):
    # Append the string "data" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in the string
    # - bytes [4..n] = Value; the (string) parameter passed to this method.
    data_length = len(data)
    packed_data += struct.pack('!HH', tlv_type, data_length)
    for iggy in data:
        packed_data += struct.pack('!s', iggy)
    if (data_length%2) != 0:
        # Odd-length. Add another byte to the end of the packed structure.
        packed_data += struct.pack('!B', 0)
    return packed_data


def get_tlv_data_string(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract the Length value
    # - extract as many bytes based on this Length
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    #
    # NOTE: TLV strings returned by the SG424 contain its own string length in the 1st
    #       2 bytes of the Value field of the TLV.
    tlv_string = ""
    packed_data_length = len(packed_data)
    if tlv_rank < packed_data_length:
        # Get TLV Length and validate it.
        tlv_data = packed_data[tlv_rank:tlv_rank + 2]
        (tlv_length,) = struct.unpack('!H', tlv_data)

        if tlv_length < (packed_data_length - tlv_rank):
            # The actual string length is in the 1st 2 bytes of the Value field,
            # so extract this length and then extract the string itself:
            data_rank = tlv_rank + 2
            tlv_data = packed_data[data_rank:data_rank + 2]
            (user_string_length,) = struct.unpack('!H', tlv_data)

            if user_string_length < (packed_data_length - data_rank):
                # Increment the rank by 2 to index the 1st character of the string.
                data_rank += 2
                for char_char in packed_data[data_rank:data_rank + user_string_length]:
                    tlv_string += chr(int(ord(char_char)))

                # Advance the tlv_rank (from above) to the next tlv before returning.
                tlv_rank = tlv_rank + 2 + tlv_length
                if tlv_rank % 2 != 0:  # odd numbered, so increment
                    tlv_rank += 1
            else:
                # Invalid string length specified
                # No point doing this until alarm interface is included:
                raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR)
                pass
        else:
            # Invalid TLV Length field
            # No point doing this until alarm interface is included:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR)
            pass
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        # No point doing this until alarm interface is included:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR)
        tlv_rank = 0

    return (tlv_string, tlv_rank)

def get_tlv_data_binary(packed_data, tlv_rank):
    # The tlv_rank is indexing the Length field of a TLV:
    # - extract the Length value
    # - extract as many bytes based on this Length
    # - update/return the tlv_rank to index the 1st byte of the next TLV in the byte array
    # -
    #
    data_buf = []
    packed_data_length = len(packed_data)

    if tlv_rank < packed_data_length:
        # Get TLV Length and validate it.
        tlv_data = packed_data[tlv_rank:tlv_rank + 2]
        (tlv_length,) = struct.unpack('!H', tlv_data)

        #print("length = %d" % tlv_length)
        #print("packed_data_length = %d" % packed_data_length)
        #print("tlv_rank = %d" % tlv_rank)

        if tlv_length < (packed_data_length - tlv_rank):

            tlv_rank += 2
            data_buf = packed_data[tlv_rank:tlv_rank + tlv_length]
            #print("length data_buf = %d" % len(data_buf))

            # print out raw data for testing
            #for i in range(0, (len(data_buf))):
            #    print("data[%d]=0x%x" % (i, ord(data_buf[i])))

            # Advance the tlv_rank (from above) to the next tlv before returning.
            tlv_rank = tlv_rank + tlv_length
            if tlv_rank % 2 != 0:  # odd numbered, so increment
                 tlv_rank += 1

        else:
            # Invalid TLV Length field
            # No point doing this until alarm interface is included:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR)
            pass
    else:
        # SAW THIS DURING TESTING: there was a bug on the SG424, in which the number
        # of user TLVs was 2, but in fact, the packed data contained only 1 user TLV.
        # No point doing this until alarm interface is included:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_GET_TLV_DATA_STRING_ERROR)
        tlv_rank = 0

    return (data_buf, tlv_rank)


def append_user_tlv_string_with_invalid_length(packed_data, tlv_type, some_very_invalid_tlv_length, data):
    # This method creates a packet where the length field of the packet is incorrect.
    # USED FOR TESTING ONLY.
    data_length = len(data)
    packed_data += struct.pack('!HH', tlv_type, some_very_invalid_tlv_length)
    for iggy in data:
        packed_data += struct.pack('!s', iggy)
    if (data_length%2) != 0:
        # Odd-length. Add another byte to the end of the packed structure.
        packed_data += struct.pack('!B', 0)
    return packed_data


def get_hos_string_from_tlv_data(packed_data, tlv_rank):
    hos_string = ""
    (tlv_length, tlv_rank) = get_tlv_header_length(packed_data, tlv_rank)
    if tlv_length == 6:
        # Now extract the next 6 bytes of the MAC:
        try:
            hex_pair1, hex_pair2, hex_pair3, tlv_rank = get_funky_packed_mac(packed_data, tlv_rank)
        except Exception as err:
            logger.error(repr(err))
            tlv_rank = 0
        else:
            try:
                addr = '%04x%04x%04x' % (hex_pair1, hex_pair2, hex_pair3)
            except Exception as err:
                logger.error(repr(err))
                tlv_rank = 0
            else:
                hos_string = ':'.join([addr[i:i+2] for i in range(0, len(addr), 2)])
    else:
        # *MUST* be of length 6, otherwise it is a malformed packet.
        tlv_rank = 0
    return (hos_string, tlv_rank)


def append_user_tlv_mac_as_hex(packed_data, tlv_type, mac_as_string):
    # Append the mac address "mac_as_hex" to the end of a byte array as a TLV:
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field = 6 (hard-coded)
    # - bytes [4..9] = Value, i.e., the 6 bytes of the mac address as hex
    packed_data += struct.pack('!HH', tlv_type, 6)

    # Get rid of the colons, and extract 6 pair of hex values.
    mac_as_string = mac_as_string.replace(':', '').decode('hex')
    for char_char in mac_as_string:
        dis_mac_byte_as_int = int(hex(ord(char_char))[2:].zfill(2), 16)
        packed_data += struct.pack('!B', dis_mac_byte_as_int)
    return packed_data


def append_user_hex_numbers(packed_data, tlv_type, hex_as_string):
    hex_string_length_is_odd = False
    tlv_length = len(hex_as_string)
    if (tlv_length%2) != 0:
        # Odd-length. Add another byte to the end of the packed structure.
        hex_string_length_is_odd = True
    packed_data += struct.pack('!HH', tlv_type, tlv_length)

    for char_char in hex_as_string:
        dis_hex_byte_as_int = int(hex(ord(char_char))[2:].zfill(2), 16)
        packed_data += struct.pack('!B', dis_hex_byte_as_int)
    if hex_string_length_is_odd:
        packed_data += struct.pack('!B', 0)
    return packed_data


def add_user_tlv_string(tlv_type, data):
    # *ADD* the string "data" to the end of a byte array as a TLV:
    # This one differs from append: this one ois used if it is the 1st TLV to be "aded".
    # - bytes [0:1] = user's tlv_type
    # - bytes [2:3] = Length of the Value field: number of bytes in the string
    # - bytes [4..n] = Value; the (string) parameter passed to this method.
    data_length = len(data)
    packed_data = struct.pack('!HH', tlv_type, data_length)
    for iggy in data:
        packed_data += struct.pack('!s', iggy)
    if (data_length%2) != 0:
        # Odd-length. Add another byte to the end of the packed structure.
        packed_data += struct.pack('!B', 0)
    return packed_data


# END OF THE SMALL SET OF GENERAL PURPOSE TLV FUNCTIONS.
# -----------------------------------------------------------------------------


def append_dynamic_power_factor(packed_data, pf_tlv, pf_as_float, pf_leading_tlv, pf_current_leading):
    # Dynamic power factor for a feed contains 2 TLVs: the PF TLV itself,
    # and the power factor "current leading value" for feed A, B or C.
    packed_data = append_user_tlv_32bit_float(packed_data, pf_tlv, pf_as_float)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, pf_leading_tlv, pf_current_leading)
    return packed_data


def get_udp_components(udp_packet_data):
    # A UDP message was received, it contains several components:
    # - [0] = the UDP payload data segment
    # - [1] = sender's IP/port
    packed_data = udp_packet_data[0]
    receiver_data = udp_packet_data[1]
    ip_address = receiver_data[0]
    port = receiver_data[1]
    return (ip_address, port, packed_data)


def evaluate_master_tlv(packed_data):
    # A presumed Gateway/SG424 TLV encoded packet has been received. Examine the 1st 6 bytes of
    # the byte array and validate it:
    # - bytes [0:1]: must contain MASTER TLV TYPE
    # - bytes [2:3]: must contain value "2" (length of the Value field to follow)
    # - bytes [4:5]: contains the number of user TLVs in the remaininder of the byte array.
    tlv_rank = 0
    master_tlv = packed_data[0:6]
    master_tlv_type, master_tlv_length, number_of_user_tlvs = struct.unpack('!HHH', master_tlv)

    # Basic validation: MASTER TLV type, length and number of TLVs in the remainder of the packet.
    # Consider a different alarm for each possible error, even though they would be detected in a
    # development environment only.
    if master_tlv_type != SG424_TO_GTW_MASTER_U16_TLV_TYPE:
        # Bad dog, bad dog ... you bit the hand that feeds you.
        number_of_user_tlvs = 0
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR)
    elif master_tlv_length != MASTER_TLV_DATA_FIELD_LENGTH:
        # Bad dog, bad dog ... you bit the hand that strokes you.
        number_of_user_tlvs = 0
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR)
    elif number_of_user_tlvs > MAX_NUMBER_USER_TLV_IN_A_PACKET:
        # Bad dog, bad dog ... you barfed on the carpet.
        number_of_user_tlvs = 0
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR)
    else:
        # Good packet. Update/return the tlv_rank to index the 1st byte of the user TLV in the byte array.
        tlv_rank = (ANY_TLV_TYPE_FIELD_LENGTH + ANY_TLV_DATA_LENGTH_FIELD_LENGTH + MASTER_TLV_DATA_FIELD_LENGTH)
    return (tlv_rank, number_of_user_tlvs)


def send_and_get_query_data(inverter_ip_address):

    # Assumption: the IP has been validated to the extent that it looks like an IP.
    private_port_packet = None
    SG424_data = None
    number_of_user_tlv = 1
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (inverter_ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:

            # Now wait for a response:
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding.
                pass

            else:

                # Packet received, data is in private_port_packet field.
                (inverter_ip_address, donut_care_port, SG424_data) = get_udp_components(private_port_packet)

        # Close the socket:
        try:
            fancy_socks.close()
        except Exception:  # as error:
            print("close socket failed ...")
        else:
            pass

    return SG424_data


def get_inverter_status_for_association(ip_address):
    # Send a general-purpose query to the inverter; if a response is returned, determine if it is in APP mode.
    runmode = RUNMODE_UNKNOWN
    dc_volts = 0.00
    dc_amps = 0.00
    dc_watts = 0
    dc_gwa_string = None
    under_gateway_control = False
    keep_alive_timeout = 0
    SG424_data = send_and_get_query_data(ip_address)
    (tlv_id_list, tlv_data_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
    if tlv_id_list:
        tlv_of_interest_count = 0
        for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
            # Only the runmode and dc_volts TLV are of interest.
            if tlv_id == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                tlv_of_interest_count += 1
                if tlv_data == SG424_TO_GTW_RUNMODE_APPLICATION:
                    runmode = RUNMODE_SG424_APPLICATION
                elif tlv_data == RUNMODE_SG424_BOOT_LOADER:
                    runmode = RUNMODE_SG424_BOOT_LOADER
                elif tlv_data ==  SG424_TO_GTW_RUNMODE_BOOT_LOADER:
                    runmode = RUNMODE_SG424_BOOT_UPDATER
                else:
                    # ICK!
                    pass
            elif tlv_id == SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE:
                dc_volts = tlv_data
                tlv_of_interest_count += 1
            elif tlv_id == SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE:
                dc_watts = tlv_data
                tlv_of_interest_count += 1
            elif tlv_id == SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE:
                dc_amps = tlv_data
            elif tlv_id == SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE:
                dc_gwa_string = tlv_data
            elif tlv_id == SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE:
                keep_alive_timeout = tlv_data
            elif tlv_id == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:
                inv_ctrl_as_hex_string = "0x%s" % (hex(tlv_data)[2:].zfill(2))
                if is_gateway_control_set_in_inv_ctrl(inv_ctrl_as_hex_string):
                    under_gateway_control = True
            else:
                # It's some other TLV, skip to the next one.
                pass
            if tlv_of_interest_count == 7:
                # Found the TLVs of interest, no point searching the rest, so ...
                break
        # ... for
    else:
        # Zero-length list, so no response.
        pass

    return (runmode, dc_volts, dc_amps, dc_watts, dc_gwa_string, under_gateway_control, keep_alive_timeout)


def extract_dc_sm_state_string(dc_gwa_string):
    # WOpr|Idle|FZro|C 100%|U 100.0%|D   AFAP|F  0%||  0W|
    dc_sm_state_string = None
    if dc_gwa_string:
        dc_gwa_string_components_list = dc_gwa_string.split("|")
        dc_sm_state_string = dc_gwa_string_components_list[0]
    return dc_sm_state_string


def is_inverter_in_app_mode(ip_address):
    # Send a general-purpose query to the inverter; if a response is returned, determine if it is in APP mode.
    inverter_is_in_app_mode = False
    SG424_data = send_and_get_query_data(ip_address)
    (tlv_id_list, tlv_data_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
    if tlv_id_list:
        for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
            # Only the runmode TLV is of interest.
            if tlv_id == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                if tlv_data == SG424_TO_GTW_RUNMODE_APPLICATION:
                    inverter_is_in_app_mode = True
                else:
                    # Only 2 choices remain, so not in APPLICATION mode.
                    # tlv_data == RUNMODE_SG424_BOOT_LOADER:
                    # tlv_data == RUNMODE_SG424_BOOT_UPDATER:
                    pass
                break
            else:
                # It's some other TLV, skip to the next one.
                pass
        # ... for
    else:
        # Zero-length list, so no response.
        pass

    return inverter_is_in_app_mode


def send_and_get_eerpt_data(inverter_ip_address):

    # Assumption: the IP has been validated to the extent that it looks like an IP.
    private_port_packet = None
    SG424_data = None
    number_of_user_tlv = 1
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GET_EERPT_SERVER_CONFIG_DATA_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (inverter_ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:

            # Now wait for a response:
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding.
                pass

            else:

                # Packet received, data is in private_port_packet field.
                (inverter_ip_address, donut_care_port, SG424_data) = get_udp_components(private_port_packet)

        # Close the socket:
        try:
            fancy_socks.close()
        except Exception:  # as error:
            print("close socket failed ...")
        else:
            pass

    return SG424_data



'''
This method not used, cannot resolve the "TypeError: 'module' object is not callable"
when datetime is called. Yet when used within gpts.py as is, no problem?
def get_elapsed_seconds_betwen_2_timestamps(timestamp_1, timestamp_2):
    elapsed_seconds = 0
    day1 = ('%s' % timestamp_1)
    day1 = datetime(*time.strptime(day1, "%Y-%m-%d %H:%M:%S")[:6])
    day2 = ('%s' % timestamp_2)
    day2 = datetime(*time.strptime(day2, "%Y-%m-%d %H:%M:%S")[:6])
    elapsed = abs(day2 - day1).seconds
    print("PAPA FROM %s TO %s elapsed = %s" % (day2, day1, elapsed))
    return elapsed_seconds
'''


def send_and_get_mom_data(inverter_ip_address):

    # Assumption: the IP has been validated to the extent that it looks like an IP.
    private_port_packet = None
    SG424_data = None
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GET_MOM_U16_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (inverter_ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:

            # Now wait for a response:
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding.
                pass
            else:
                # Packet received, data is in private_port_packet. Return just the payload.
                (inverter_ip_address, donut_care_port, SG424_data) = get_udp_components(private_port_packet)

        # Close the socket:
        try:
            fancy_socks.close()
        except Exception:  # as error:
            print("close socket failed ...")
        else:
            pass

    return SG424_data


def print_mom_header():
    # The inverter returns mom data in a raw hex-encoded data block. The gateway extricates components in a
    # specific order, printed on a single line. MOM data is useful when retrieved over a period of time. To make
    # the data in the columns sensible, print this header. The MOM data block "version" is not printed.
    #                      \t\t|         \t|             \t\t|             \t\t|               |               \t|             \t\t|             \t\t|             \t|      |\n
    this_string  = "       \t\t|         \t|             \t\t|             \t\t|               |               \t|             \t\t|             \t\t| ACCS_THREAD \t|      |\n"
    this_string += "       \t\t|         \t|             \t\t|             \t\t|               |               \t|             \t\t|             \t\t|      F32    \t|      |\n"
    this_string += "       \t\t|         \t|             \t\t|  MPX_I16    \t\t|   THD_I16     |   THD_F32     \t|             \t\t|             \t\t|ApparentPower\t|      |\n"
    this_string += "       \t\t| MPX_U16 \t|   MPX_I16   \t\t| ActualPsolar\t\t|  PhaseAngle   |   DutyCycle   \t|  PLL_I16_TS \t\t|  ADC_I16    \t\t|  Calculated \t|      |\n"
    this_string += "       \t\t|StepSize \t| VrefSetting \t\t|     Wdc     \t\t|   360Actual   |TableFillActual\t|  PhaseError \t\t|  PlineWrms  \t\t| FromPsolar  \t|Fault |\n"
    this_string += "AllBits\t\t|   HWM   \t|HWM LWM Delta\t\t|HWM LWM Delta\t\t|InUse_InDegrees|HWM LWM Delta  \t|HWM LWM Delta\t\t|HWM LWM delta\t\t|HWM LWM Delta\t|Number|\n"
    this_string += "-------------------------------------------------------------------------------------------"
    this_string += "---------------------------------------------------------------------------------------------------|"
    print(this_string)


def parse_and_print_mom_data(ip_address, tlv_id_list, tlv_data_list):

    # print("MOM DATA TO BE PARSED FROM %s TLVs" % (number_of_mom_tlv))

    '''
    The MOM data from the inverter contains the following data:

    - 32-bit long:           U32   AllBits_DO_NOT_USE_THIS_EXCEPT_ONCE_AT_STARTUP
    - 16-bit unsigned short: U16   MPX_U16_StepSize_HighWaterMark
    - 16-bit   signed short: I16   MPX_I16_VrefSetting_HighWaterMark
    - 16-bit   signed short: I16   MPX_I16_VrefSetting_LowWaterMark
    - 16-bit   signed short: I16   MPX_I16_ActualPsolarWdc_HighWaterMark
    - 16-bit   signed short: I16   MPX_I16_ActualPsolarWdc_LowWaterMark
    - 16-bit   signed short: I16   THD_I16_PhaseAngle360_Actual_InUse_InDegrees
    - 32-bit float:          F32   THD_F32_DutyCycleTableFillActual_HighWaterMark
    - 32-bit float:          F32   THD_F32_DutyCycleTableFillActual_LowWaterMark
    - 16-bit   signed short: I16   PLL_I16_TS_PhaseError_HighWaterMark
    - 16-bit   signed short: I16   PLL_I16_TS_PhaseError_LowWaterMark
    - 16-bit   signed short: I16   ADC_I16_PlineWrms_HighWaterMark
    - 16-bit   signed short: I16   ADC_I16_PlineWrms_LowWaterMark
    - 16-bit   signed short: I16   ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_HighWaterMark
    - 16-bit   signed short: I16   ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_LowWaterMark
    - 16-bit unsigned short: U16   FaultNumber

    The 32-bit in the above AllBits... are simply displayed a received. As requirements are clarified,
    this long may be parsed and individual bits may be displayed. The bits are currently defined as:

    VU1   THD_F32_FillServoError_Integral_Clamped                     : 1   ...  1
    VU1   THD_F32_FillServoErrorDerivative_Clamped                    : 1   ...  2
    VU1   THD_F32_FillServoErrorDerivative_Clamped_TRO                : 1   ...  3
    VU1   THD_TRO_IsActive                                            : 1   ...  4
    VU1   THD_TRO_TRIGGER_p1_F32_DutyCycleTableFillActual             : 1   ...  5
    VU1   THD_TRO_TRIGGER_1_F32_DutyCycleTableFillActual              : 1   ...  6
    VU1   THD_TRO_TRIGGER_2_F32_DutyCycleTableFillActual              : 1   ...  7
    VU1   THD_MaximumDutyCycleSettingClipEvent                        : 1   ...  8
    VU1   THD_MinimumDutyCycleSettingClipEvent                        : 1   ...  9
    VU1   THD_DC_to_THD_HBridgeClosure_Demands_TRO                    : 1   ... 10
    VU1   THD_DC_to_THD_Ignition_Demands_TRO                          : 1   ... 11
    VU1   THD_MPX_to_THD_PowerSpike_Demands_TRO                       : 1   ... 12
    VU1   MPX_StepSizeIncrease_PowerSpikeInProgress                   : 1   ... 13
    VU1   MPX_StepSizeIncrease_VoltWattPowerSetpointWdc               : 1   ... 14
    VU1   MPX_StepSizeIncrease_VoltWattPowerSetpointWdc_TurboMode     : 1   ... 15
    VU1   MPX_StepSizeIncrease_FreqWattPowerSetpointWdc               : 1   ... 16
    VU1   MPX_StepSizeIncrease_FreqWattPowerSetpointWdc_TurboMode     : 1   ... 17
    VU1   MPX_StepSizeIncrease_FastReconnect_OverrideIsActive         : 1   ... 18
    VU1   MPX_StepSizeIncrease_WaitingForFirstIgnition                : 1   ... 19
    VU1   MPX_StepSizeIncrease_ForNoPowerChange                       : 1   ... 20
    VU1   ADC_FastCurrentSpikeClamping_NegativeStepTaken              : 1   ... 21
    VU1   THD_RealPriorityReducesReactivePower                        : 1   ... 22
    VU1   MPX_ReactivePriorityReducesRealPower                        : 1   ... 23
    VU1   MPX_LimitSetpointViolationBit_Psolar                        : 1   ... 24
    VU1   MPX_WeHaveLostIgnitionDuringOperation                       : 1   ... 25
    VU1   AI_ErrorCorrection_1_Bump                                   : 1   ... 26
    VU1   AI_ErrorCorrection_2_Bump                                   : 1   ... 27
    VU1   MSM_State_Momentary_Cessation                               : 1   ... 28
    '''

    # print_this_hex_data(mom_hex_data)

    # -----------------------------------------------------------------------------------------
    # Retrieve MOM.
    #
    all_bits_encountered = False
    step_size_encountered = False
    vref_setting_hi_encountered = False
    vref_setting_lo_encountered = False
    actual_psolar_wdc_hi_encountered = False
    actual_psolar_wdc_lo_encountered = False
    phase_angle_360_encountered = False
    duty_cycle_table_hi_encountered = False
    duty_cycle_table_lo_encountered = False
    ts_phase_error_hi_encountered = False
    ts_phase_error_lo_encountered = False
    pline_wrms_hi_encountered = False
    pline_wrms_lo_encountered = False
    apparent_power_from_psolar_hi_encountered = False
    apparent_power_from_psolar_lo_encountered = False
    fault_number_encountered = False

    number_of_mom_id = len(tlv_id_list)
    number_of_mom_data = len(tlv_data_list)
    if ((number_of_mom_id == number_of_mom_data) and (number_of_mom_id != 0)):

        # for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
        for iggy in range (0, number_of_mom_id):
            if tlv_id_list[iggy] == SG424_TO_GTW_MOM_ALL_BITS_U32_TLV_TYPE:
                AllBits = tlv_data_list[iggy]
                all_bits_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_STEP_SIZE_HI_WM_U16_TLV_TYPE:
                MPX_U16_StepSize_HighWaterMark = tlv_data_list[iggy]
                step_size_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_VREF_SETTING_HI_WM_I16_TLV_TYPE:
                MPX_I16_VrefSetting_HighWaterMark = tlv_data_list[iggy]
                vref_setting_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_VREF_SETTING_LO_WM_I16_TLV_TYPE:
                MPX_I16_VrefSetting_LowWaterMark = tlv_data_list[iggy]
                vref_setting_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_HI_WM_I16_TLV_TYPE:
                MPX_I16_ActualPsolarWdc_HighWaterMark = tlv_data_list[iggy]
                actual_psolar_wdc_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_LO_WM_I16_TLV_TYPE:
                MPX_I16_ActualPsolarWdc_LowWaterMark = tlv_data_list[iggy]
                actual_psolar_wdc_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PHASE_ANGLE_360_ACTUAL_INUSE_INDEGREES_I16_TLV_TYPE:
                THD_I16_PhaseAngle360_Actual_InUse_InDegrees = tlv_data_list[iggy]
                phase_angle_360_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_HI_WM_F32_TLV_TYPE:
                THD_F32_DutyCycleTableFillActual_HighWaterMark = tlv_data_list[iggy]
                duty_cycle_table_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_LO_WM_F32_TLV_TYPE:
                THD_F32_DutyCycleTableFillActual_LowWaterMark = tlv_data_list[iggy]
                duty_cycle_table_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_TS_PHASE_ERROR_HI_WM_I16_TLV_TYPE:
                PLL_I16_TS_PhaseError_HighWaterMark = tlv_data_list[iggy]
                ts_phase_error_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_TS_PHASE_ERROR_LO_WM_I16_TLV_TYPE:
                PLL_I16_TS_PhaseError_LowWaterMark = tlv_data_list[iggy]
                ts_phase_error_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PLINE_WRMS_HI_WM_I16_TLV_TYPE:
                ADC_I16_PlineWrms_HighWaterMark = tlv_data_list[iggy]
                pline_wrms_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PLINE_WRMS_LO_WM_I16_TLV_TYPE:
                ADC_I16_PlineWrms_LowWaterMark = tlv_data_list[iggy]
                pline_wrms_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_HI_WM_I16_TLV_TYPE:
                ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_HighWaterMark = tlv_data_list[iggy]
                apparent_power_from_psolar_hi_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_LO_WM_I16_TLV_TYPE:
                ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_LowWaterMark = tlv_data_list[iggy]
                apparent_power_from_psolar_lo_encountered = True
            elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_FAULT_NUMBER_U16_TLV_TYPE:
                FaultNumber = tlv_data_list[iggy]
                fault_number_encountered = True
            else:
                # Not your MOM? Might be a new MOM TLV added to the mix that the Gateway is not aware of.
                print("UNKNOWN MOM TLV %s ENCOUNTERED ... skipping past it")

        # for iggy is a jolly good fellow ...

    else:
        print("INCONSISTENT TLD ID/DATA LIST LENGTHS = %s/%s" % (len(number_of_mom_id), len(number_of_mom_data)))
        return


    # Check that all expected MOM TLVs were received. If any were missing, could be 1 of 2 things:
    # - inverter bug and TLV not included
    # - a MOM component has been removed but the gateway has not been updated to reflect the change.
    if all_bits_encountered == False:
        print("DID NOT GET ALL BITS")
        return

    if step_size_encountered == False:
        print("DID NOT GET STEP SIZE")
        return

    if vref_setting_hi_encountered == False or vref_setting_lo_encountered == False:
        print("DID NOT GET VREF SETTING")
        return
    else:
        MPX_I16_VrefSetting_Delta = MPX_I16_VrefSetting_HighWaterMark - MPX_I16_VrefSetting_LowWaterMark

    if actual_psolar_wdc_hi_encountered == False or actual_psolar_wdc_lo_encountered == False:
        print("DID NOT GET ACTUAL PSOLAR WDC")
        return
    else:
        MPX_I16_ActualPsolarWdc_Delta = MPX_I16_ActualPsolarWdc_HighWaterMark - MPX_I16_ActualPsolarWdc_LowWaterMark

    if phase_angle_360_encountered == False:
        print("DID NOT GET PHASE ANGLE")
        return

    if duty_cycle_table_hi_encountered == False or duty_cycle_table_lo_encountered == False:
        print("DID NOT GET DUTY CYCLE")
        return
    else:
        THD_F32_DutyCycleTableFillActual_Delta = THD_F32_DutyCycleTableFillActual_HighWaterMark - THD_F32_DutyCycleTableFillActual_LowWaterMark

    if ts_phase_error_hi_encountered == False or ts_phase_error_lo_encountered == False:
        print("DID NOT GET PHASE EROR")
        return
    else:
        PLL_I16_TS_PhaseError_Delta =  PLL_I16_TS_PhaseError_HighWaterMark - PLL_I16_TS_PhaseError_LowWaterMark

    if pline_wrms_hi_encountered == False or pline_wrms_lo_encountered == False:
        print("DID NOT GET PLINE WRMS")
        return
    else:
        ADC_I16_PlineWrms_Delta = ADC_I16_PlineWrms_HighWaterMark - ADC_I16_PlineWrms_LowWaterMark

    if apparent_power_from_psolar_hi_encountered == False or apparent_power_from_psolar_lo_encountered == False:
        print("DID NOT GET APP POWER FROM PSOLAR")
        return
    else:
        ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_Delta = ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_HighWaterMark - ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_LowWaterMark

    if fault_number_encountered == False:
        print("DID NOT GET FAULT NUMBER")
        return


    # -----------------------------------------------------------------------------------------
    # Print MOM.
    this_string  = "%s\n" % "{:032b}".format(AllBits)
    this_string += "\t\t|%s\t\t" % (MPX_U16_StepSize_HighWaterMark)
    this_string += "|%s\t" % (MPX_I16_VrefSetting_HighWaterMark)
    this_string += "%s\t" % (MPX_I16_VrefSetting_LowWaterMark)
    this_string += "%s\t" % (MPX_I16_VrefSetting_Delta)
    this_string += "|%s\t" % (MPX_I16_ActualPsolarWdc_HighWaterMark)
    this_string += "%s\t" % (MPX_I16_ActualPsolarWdc_LowWaterMark)
    this_string += "%s\t" % (MPX_I16_ActualPsolarWdc_Delta)
    this_string += "|%s\t\t" % (THD_I16_PhaseAngle360_Actual_InUse_InDegrees)
    this_string += "|%s\t" % ('{0:+.2f}'.format(THD_F32_DutyCycleTableFillActual_HighWaterMark)) # print to 2-decimal places
    this_string += "%s\t" % ('{0:+.2f}'.format(THD_F32_DutyCycleTableFillActual_LowWaterMark))   # print to 2-decimal places
    this_string += "%s\t" % ('{0:+.2f}'.format(THD_F32_DutyCycleTableFillActual_Delta))          # print to 2-decimal places
    this_string += "|%s\t" % (PLL_I16_TS_PhaseError_HighWaterMark)
    this_string += "%s\t" % (PLL_I16_TS_PhaseError_LowWaterMark)
    this_string += "%s\t" % (PLL_I16_TS_PhaseError_Delta)
    this_string += "|%s\t" % (ADC_I16_PlineWrms_HighWaterMark)
    this_string += "%s\t" % (ADC_I16_PlineWrms_LowWaterMark)
    this_string += "%s\t" % (ADC_I16_PlineWrms_Delta)
    this_string += "|%s\t" % ( ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_HighWaterMark)
    this_string += "%s\t" % ( ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_LowWaterMark)
    this_string += "%s\t" % ( ACCS_THREAD_F32_ApparentPower_CalculatedFromPsolar_Delta)
    this_string += "|%s\t" % (FaultNumber)
    print(this_string)
    return


def get_gateway_operation_mode():
    operation_mode = ""
    select_string = "SELECT %s from %s;" % (GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME, DB_GATEWAY_TABLE)
    mode = prepared_act_on_database(FETCH_ONE, select_string, ())
    operation_mode = mode[GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME]
    return operation_mode


def is_gateway_in_power_control_mode():
    in_power_control_mode = False
    select_string = "SELECT %s from %s;" % (GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME, DB_GATEWAY_TABLE)
    operation_mode = prepared_act_on_database(FETCH_ONE, select_string, ())
    if operation_mode[GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME] == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE:
        in_power_control_mode = True
    return in_power_control_mode


def is_gateway_in_observation_mode():
    # Companion method to the above, to avoid "not negative" logic.
    in_observation_mode = False
    select_string = "SELECT %s from %s;" % (GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME, DB_GATEWAY_TABLE)
    operation_mode = prepared_act_on_database(FETCH_ONE, select_string, ())
    if operation_mode[GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME] == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE:
        in_observation_mode = True
    return in_observation_mode


def set_gateway_operation_mode(operation_mode):
    if (operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE) \
    or (operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE):
        # Set the value.
        set_string = "UPDATE %s SET %s = %s;" % (DB_GATEWAY_TABLE,
                                                 GATEWAY_TABLE_GATEWAY_OPERATION_MODE_COLUMN_NAME, "%s")
        set_args = (operation_mode, )
        prepared_act_on_database(EXECUTE, set_string, set_args)


def construct_standby_heartbeat_packed_data():
    # Construct a TLV packet consisting of a CURTAIL TLV, SEQUENCE NUMBER TLV.
    #
    # The "site" value for fallback power setting, NTP URL and universal AIF data used to be included in this
    # curtail packet, but has since been removed in favor of group-based data, disseminated to inverters via a
    # group audit process.
    construction_problems = ""

    # Include the initial curtailment value and initial curtail sequence number.
    tlv_user_data_pack = add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE, 0)
    tlv_user_data_pack = append_user_tlv_32bit_unsigned_long(tlv_user_data_pack,
                                                             GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE,
                                                             SEQUENCE_NUMBER_STARTUP_STANDBY_HEART_BEAT)
    number_of_user_tlv = 2

    '''

    UNIVERSAL AIF: the inverter's SOFTWARE PROFILE is now left as is. It is no longer updated
                   as part of the sis audit, nor is it updated upon Gateway startup. It also
                   means FIXED POWER FACTOR is left alone.

    # Include UNIVERSAL-AIF data.
    universal_aif_row_data = get_universal_aif_db_row()
    if universal_aif_row_data:
        region_name = universal_aif_row_data[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]
        fixed_power_factor = universal_aif_row_data[UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME]
        fixed_power_factor_c_lead = universal_aif_row_data[UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME]
        fixed_power_factor_enable = universal_aif_row_data[UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME]
        if region_name == REGION_NAME_UNDEFINED:
            # This will be seen on sites where the Gateway is first upgraded with U-AIF capability.
            construction_problems += " REGION_NAME NOT CONFIGURED"
        else:
            # Add the U-AIF
            # TLVPacker
            tlv_user_data_pack = append_user_tlv_string(tlv_user_data_pack, GTW_TO_SG424_REGION_NAME_STRING_TLV_TYPE, region_name)
            tlv_user_data_pack = append_user_tlv_32bit_float(tlv_user_data_pack, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE, fixed_power_factor)
            tlv_user_data_pack = append_user_tlv_16bit_unsigned_short(tlv_user_data_pack, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE, fixed_power_factor_c_lead)
            tlv_user_data_pack = append_user_tlv_16bit_unsigned_short(tlv_user_data_pack, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE, fixed_power_factor_enable)
            number_of_user_tlv += 4
    else:
        # U-AIF table empty?
        construction_problems += " U-AIF TABLE EMPTY"
    '''

    # Finally, compose the master TLV, then combine the master and user tlv into 1 set of packed data.
    master_tlv = add_master_tlv(number_of_user_tlv)
    tlv_pack = master_tlv + tlv_user_data_pack

    return (tlv_pack, construction_problems)


def construct_inverter_get_outta_jail_packed_data():
    # Construct a TLV packet consisting of a UPDATE_INV_CTRL to command the inverter to NOT be under gateway control.
    tlv_pack = add_master_tlv(1)
    tlv_pack = append_user_tlv_16bit_unsigned_short(tlv_pack, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, 0)
    return tlv_pack


def send_heartbeat_command():
    # When the Gateway is taken out of observation mode into power control mode, a standby-heartbeat is multicasted.
    # Construct a TLV packet consisting of a CURTAIL TLV, SEQUENCE NUMBER TLV, fallback power setting and U-AIF data.
    (tlv_pack, construction_result) = construct_standby_heartbeat_packed_data()

    # Now send.
    multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
    this_string = "SEND HEARTBEAT COMMAND TO ALL INVERTERS ..."
    if is_send_multicast_packet_and_close_socket(tlv_pack, multicast_ip, SG424_PRIVATE_UDP_PORT):
        this_string += "MULTICASTED SUCCESSFULLY"
    else:
        this_string + "BUT *FAILED* TO MULTICAST"

    this_string += construction_result
    return this_string


def is_send_simple_curtail_packet(curtail_value, ip_address):
    packed_data = add_master_tlv(2)
    packed_data = append_user_tlv_32bit_unsigned_long(packed_data, GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE, SEQUENCE_NUMBER_STARTUP_STANDBY_HEART_BEAT)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE, curtail_value)
    # Now send.
    send_result = is_send_tlv_packed_data(ip_address, packed_data)
    return send_result


def send_inverters_clear_gateway_ctrl_command():
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, 0)
    multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
    this_string = "SET GATEWAY OPERATION MODE -> OBSERVATION ... "
    if is_send_multicast_packet_and_close_socket(packed_data, multicast_ip, SG424_PRIVATE_UDP_PORT):
        this_string += "MULTICASTED SUCCESSFULLY"
    else:
        this_string + "BUT *FAILED* TO MULTICAST"
    print(this_string)


def is_pack_and_send_gateway_control(gateway_control_value):
    send_result = True
    multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
    tlv_pack = add_master_tlv(1)
    tlv_pack = append_user_tlv_16bit_unsigned_short(tlv_pack, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, gateway_control_value)
    if is_send_multicast_packet_and_close_socket(tlv_pack, multicast_ip, SG424_PRIVATE_UDP_PORT):
        send_result = True
    else:
        send_result = False
    return send_result


def get_irra_inverter_ip_list():
    irra_ip_list = []
    select_string = "SELECT %s from %s;" % (DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME, DB_IRRADIANCE_INVERTERS_TABLE)
    this_funky_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_ip in this_funky_list:
        irra_ip_list.append(each_ip[DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME])
    return irra_ip_list


def get_max_irra_inverters_from_irra_control():
    max_inverters_count = 0
    select_string = "SELECT %s FROM %s;" % (DB_IRRA_CONTROL_TBL_MAX_INVERTERS_COLUMN_NAME, DB_IRRADIANCE_INVERTERS_TABLE)
    max_inverters_count_row = prepared_act_on_database(FETCH_ONE, select_string, ())
    if max_inverters_count_row:
        max_inverters_count = max_inverters_count_row[DB_IRRA_CONTROL_TBL_MAX_INVERTERS_COLUMN_NAME]
    return max_inverters_count


def is_send_multicast_packet_and_close_socket(packed_data, multicast_ip, this_port):

    send_and_close = False

    # OPEN A SOCKET FOR SENDING:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed (sending) ...")
    else:

        # SEND THE PACKET:
        fancy_socks.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 3)
        try:
            fancy_socks.sendto(packed_data, (multicast_ip, this_port))
        except Exception: # as error:
            print("sendto failed ...")
        else:

            # Packet sent. Now close the socket, and open up a listening socket on a separate but
            # "well-known" port to both the Gateway and the inverters.
            try:
                fancy_socks.close()
            except Exception:  # as error:
                print("close failed (sending)...")
            else:
                send_and_close = True

    return send_and_close



def get_inverter_query_data_by_mac_or_sn(this_tlv, this_mac_or_sn):

    replying_inverter_ip_address = ""
    multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
    this_port = SG424_PRIVATE_UDP_PORT
    SG424_data = None
    packed_data = add_master_tlv(1)
    # Assumption: TLV has been validated as either a MAC or SN before calling this method.
    if this_tlv == GTW_TO_SG424_QUERY_BY_MAC_6HEX_BYTES_TLV_TYPE:
        packed_data = append_user_tlv_mac_as_hex(packed_data, GTW_TO_SG424_QUERY_BY_MAC_6HEX_BYTES_TLV_TYPE, this_mac_or_sn)
    else:
        packed_data = append_user_tlv_string(packed_data, GTW_TO_SG424_QUERY_BY_SERIAL_NUMBER_STRING_TLV_TYPE, this_mac_or_sn)

    if is_send_multicast_packet_and_close_socket(packed_data, multicast_ip, this_port):

        # Multicast packet sent, now setup for listening.
        #
        # NOTE: multicasted TLV command already on its way towards the field of inverters, but this method is
        #       not yet setup for listening! This is being setup right now. Testing shows that the following
        #       setup, binding and listening will happen long before a single inverter responds, so this is
        #       "safe". Stricktly speaking though, this is cause for discomfort.
        gateway_lan_address = get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        try:
            clean_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            clean_socks.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            clean_socks.settimeout(5.0)
        except Exception as error:
            print("socket.socket/setsockopt failed (listening) ...")
        else:
            # Bind socket to local host and "well-known" port
            try:
                clean_socks.bind((gateway_lan_address, GATEWAY_QUERY_RESPONSE_PORT))
            except Exception:  # as error:
                print("bind failed ...")
            else:

                # Wait for a response. Since this method is listening for "any response" after sending out
                # a multicast TLV command, the proper thing to do is to listen for the possibility of more
                # than 1 response. You just never know. The timeout is 5 seconds. Wait for at least 1 more
                # timeout period if a response is received.
                response_has_been_received = False
                still_wiggling = 2
                while still_wiggling > 0:
                    try:
                        private_port_packet = clean_socks.recvfrom(4096)
                    except socket.timeout:
                        # Timed out waiting to receive a packet. This is normal, so wiggle a little less.
                        still_wiggling -= 1
                        if still_wiggling > 0:
                            if response_has_been_received:
                                # At least 1 response ...
                                print("No other response received, wait a bit more ...")
                            else:
                                print("No response received, wait a bit more ...")
                        else:
                            print("No other response received, and no more waiting ...")
                        continue
                    except Exception as error:
                        print("Socket receive error - %s" % error)
                        break
                    else:

                        # Packet received.
                        if not response_has_been_received:
                            # This is the 1st response. Usually only 1 response will be received, so assume this is
                            # coming from the only inverter with that SN/MAC. Extract the data.
                            response_has_been_received = True
                            (replying_inverter_ip_address, donut_care_port, SG424_data) = get_udp_components(private_port_packet)
                            print("RCV'D QBYSN/MAC REPLY FROM %s\nWait again to see if any more responses coming ... " % replying_inverter_ip_address)

                        else:
                            # Normally not expected. This means that a response has been received from a second
                            # inverter on the basis of the same SN or MAC. Something funny is going on. Could be
                            # at least 2 or more inverters with a duplicated SN or MAC - rare, but known to happen.
                            # Just print that responder's IP, ignore the data.
                            (other_inverter_ip_address, donut_care_port, donut_care_data) = get_udp_components(private_port_packet)
                            print("GOT ANOTHER QBYSN/MAC RESPONSE FROM INVERTER AT %s" + other_inverter_ip_address)

                # Close the socket.
                clean_socks.close()

    return replying_inverter_ip_address, SG424_data


def extract_list_of_tlv_id_and_data_from_packed_data(SG424_data):
    #
    # This is a general-purpose function to extract all TLVs known to the Gateway into a pair of lists,
    # each consisting of the list of TLV IDs, and corresponding list of TLV data.
    tlv_id_list = []
    tlv_data_list = []

    # TODO: SOON READY TO BE CONVERTED AND INCLUDED IN THE tlv_utils.py MODULE FOR HOW_TO_UNPACK DICTIONARY/METHOD.

    # Firswt check if there is any packed data to sort through.
    if SG424_data:
        # The Master TLV is mandatory, must be at the beginning of the TLV set so validate it.
        (tlv_rank, number_of_user_tlv) = evaluate_master_tlv(SG424_data)
        if tlv_rank == 0:
            # Master TLV is missing or mal-formed.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_EVALUATE_MASTER_TLV_ERROR)
        else:
            # Loop through the user TLVs.
            for iggy in range(0, number_of_user_tlv):

                if tlv_rank != 0:
                    (tlv_type, tlv_rank) = get_tlv_type(SG424_data, tlv_rank)

                    if tlv_type == DEPRECATED_SG424_TO_GTW_1_TLV_TYPE:                                                  # = 1002
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_2_TLV_TYPE:                                                # = 1003
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_VERSION_STRING_TLV_TYPE:                                              # = 1004
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_PRODUCT_PART_NUMBER_STRING_TLV_TYPE:                                  # = 1005
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_SERIAL_NUMBER_STRING_TLV_TYPE:                                        # = 1006
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE:                                          # = 1007
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_value = tlv_value.lower()
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE:                                              # = 1008
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE:                                             # = 1009
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:                                                 # = 1010
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE:                                         # = 1011
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE:                                             # = 1012
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CATCH_UP_REPORTS_ACTIVE_U16_TLV_TYPE:                                 # = 1013
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CATCH_UP_REPORTS_BOTH_U16_TLV_TYPE:                                   # = 1014
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CATCH_UP_REPORTS_INFO_U16_TLV_TYPE:                                   # = 1015
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_3_TLV_TYPE:                                                # = 1016
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE:                                     # = 1017
                        (tlv_value, tlv_rank) = get_hos_string_from_tlv_data(SG424_data, tlv_rank)
                        tlv_value = tlv_value.lower()
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_4_TLV_TYPE:                                                # = 1018
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_DEBUG_COUNTERS_U32_ARRAY_TLV_TYPE:                                    # = 1019
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE:                                        # = 1020
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_5_TLV_TYPE:                                                # = 1021
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_FD_STATE_U16_TLV_TYPE:                                                # = 1022
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_SW_PROFILE_U16_TLV_TYPE:                                              # = 1023
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:                                                # = 1024
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_6_TLV_TYPE:                                                # = 1025
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_7_TLV_TYPE:                                                # = 1026
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE:                                      # = 1027
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_UPTIME_SECS_U32_TLV_TYPE:                                             # = 1028
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_8_TLV_TYPE:                                                # = 1029
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_SET_RESULT_U16_TLV_TYPE:                       # = 1030
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_CLEAR_RESULT_U16_TLV_TYPE:                     # = 1031
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_ENERGY_REPORT_CONTROL_U16_TLV_TYPE:                                   # = 1032
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DC_SM_OUTPUT_STRING_TLV_TYPE:                                         # = 1033
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_9_TLV_TYPE:                                                # = 1034
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE:            # = 1035
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE:            # = 1036
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE:         # = 1037
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE:               # = 1038
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE:       # = 1039
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE:        # = 1040
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE:              # = 1041
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE:           # = 1042
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE:                               # = 1043
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE:                               # = 1044
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE:                                # = 1045
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_10_TLV_TYPE:                                               # = 1046
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_11_TLV_TYPE:                                               # = 1047
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_KEEP_ALIVE_GONE_U32_TLV_TYPE:                                         # = 1048
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_KEEP_ALIVE_BACK_U32_TLV_TYPE:                                         # = 1049
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_KEEP_ALIVE_LATE_U32_TLV_TYPE:                                         # = 1050
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_SEQUENCE_NUMBER_REPEAT_U32_TLV_TYPE:                                  # = 1051
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_SEQUENCE_NUMBER_OOS_U32_TLV_TYPE:                                     # = 1052
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_AC_VOLTAGE_OFFSET_FROM_EEPROM_F32_TLV_TYPE:              # = 1053
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_FROM_EEPROM_F32_TLV_TYPE:        # = 1054
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE:        # = 1055
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_FROM_EEPROM_U32_TLV_TYPE:   # = 1056
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_FROM_EEPROM_F32_TLV_TYPE:              # = 1057
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_FROM_EEPROM_F32_TLV_TYPE:        # = 1058
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_FROM_EEPROM_F32_TLV_TYPE:        # = 1059
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_FROM_EEPROM_F32_TLV_TYPE:        # = 1060
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE:       # = 1061
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_FROM_EEPROM_F32_TLV_TYPE:         # = 1062
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_FROM_EEPROM_U16_TLV_TYPE:        # = 1063
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_POWER_WAVERING_WATTS_FROM_EEPROM_F32_TLV_TYPE:           # = 1064
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_FROM_EEPROM_U16_TLV_TYPE:         # = 1065
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CURTAILMENT_PACKETS_U32_TLV_TYPE:                                     # = 1066
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_REGULATION_PACKETS_U32_TLV_TYPE:                                      # = 1067
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_BATTERY_U16_TLV_TYPE:                                                 # = 1068
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:                              # = 1069
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:                            # = 1070
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_ALL_BITS_U32_TLV_TYPE:                                            # = 1071
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_unsigned_long(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_STEP_SIZE_HI_WM_U16_TLV_TYPE:                                     # = 1072
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_VREF_SETTING_HI_WM_I16_TLV_TYPE:                                  # = 1073
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_VREF_SETTING_LO_WM_I16_TLV_TYPE:                                  # = 1074
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_HI_WM_I16_TLV_TYPE:                             # = 1075
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_LO_WM_I16_TLV_TYPE:                             # = 1076
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_PHASE_ANGLE_360_ACTUAL_INUSE_INDEGREES_I16_TLV_TYPE:              # = 1077
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_HI_WM_F32_TLV_TYPE:                  # = 1078
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_LO_WM_F32_TLV_TYPE:                  # = 1079
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_TS_PHASE_ERROR_HI_WM_I16_TLV_TYPE:                                # = 1080
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_TS_PHASE_ERROR_LO_WM_I16_TLV_TYPE:                                # = 1081
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_PLINE_WRMS_HI_WM_I16_TLV_TYPE:                                    # = 1082
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_PLINE_WRMS_LO_WM_I16_TLV_TYPE:                                    # = 1083
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_HI_WM_I16_TLV_TYPE:                 # = 1084
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_LO_WM_I16_TLV_TYPE:                 # = 1085
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_signed_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_MOM_FAULT_NUMBER_U16_TLV_TYPE:                                        # = 1086
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE:                 # = 1087
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_FROM_EEPROM_F32_TLV_TYPE:        # = 1088
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_FROM_EEPROM_F32_TLV_TYPE:      # = 1089
                        (tlv_value, tlv_rank) = get_tlv_data_32bit_float(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE:                                           # = 1090
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_12_TLV_TYPE:                                               # = 1091
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE:                                             # = 1092
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE:                                     # = 1093
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_13_TLV_TYPE:                                               # = 1094
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_14_TLV_TYPE:                                               # = 1095
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_15_TLV_TYPE:                                               # = 1096
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_16_TLV_TYPE:                                               # = 1097
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_17_TLV_TYPE:                                               # = 1098
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == DEPRECATED_SG424_TO_GTW_18_TLV_TYPE:                                               # = 1099
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_ENABLE_U16_TLV_TYPE:                                     # = 1100
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_RATE_U16_TLV_TYPE:                                       # = 1101
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_URL_STRING_TLV_TYPE:                                     # = 1102
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_PORT_U16_TLV_TYPE:                                       # = 1103
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE:                               # = 1104
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE:                                # = 1105
                        (tlv_value, tlv_rank) = get_tlv_data_string(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE:                                     # = 1106
                        (tlv_value, tlv_rank) = get_tlv_data_binary(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE:                                   # = 1107
                        (tlv_value, tlv_rank) = get_tlv_data_binary(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    elif tlv_type == SG424_TO_GTW_CURRENT_STATUS_STRUCTURED_TLV_TYPE:                                   # = 1108
                        (tlv_value, tlv_rank) = get_tlv_data_binary(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)

                    # SG424_TO_GTW_XET_INFO_STRUCTURED_TLV_TYPE           # = 1109
                    # SG424_TO_GTW_XET_AIF_VRT_STRUCTURED_TLV_TYPE        # = 1110
                    # SG424_TO_GTW_XET_AIF_FRT_STRUCTURED_TLV_TYPE        # = 1111
                    # SG424_TO_GTW_XET_AIF_VOLT_WATT_STRUCTURED_TLV_TYPE  # = 1112
                    # SG424_TO_GTW_XET_AIF_VOLT_VAR_STRUCTURED_TLV_TYPE   # = 1113
                    # SG424_TO_GTW_XET_AIF_FREQ_WATT_STRUCTURED_TLV_TYPE  # = 1114
                    # SG424_TO_GTW_XET_AIF_CURVE_STRUCTURED_TLV_TYPE      # = 1115
                    # SG424_TO_GTW_XET_AIF_STATUS_STRUCTURED_TLV_TYPE     # = 1116
                    # SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE  # = 1117
                    # SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE    # = 1118
                    # SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE      # = 1119
                    # SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE    # = 1120
                    # SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE      # = 1121
                    # SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE      # = 1122
                    # SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE       # = 1123
                    # SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE       # = 1124
                    # SG424_TO_GTW_ENERGY_REPORT_STRUCTURED_TLV_TYPE      # = 1125
                    # SG424_TO_GTW_ALARM_REPORT_STRUCTURED_TLV_TYPE       # = 1126
                    # SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE     # = 1127
                    # SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE                  # = 1128

                    elif tlv_type == SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE:                                                # = 1128
                        (tlv_value, tlv_rank) = get_tlv_data_16bit_unsigned_short(SG424_data, tlv_rank)
                        tlv_id_list.append(tlv_type)
                        tlv_data_list.append(tlv_value)
                    else:
                        # Skip over this miscreant.
                        tlv_rank = skip_to_next_tlv(SG424_data, tlv_rank)

                else:
                    # Badly formed TLV packed data.
                    del tlv_id_list[:]
                    del tlv_data_list[:]
                    break
            # end ... for loop

    return (tlv_id_list, tlv_data_list)

def print_inverter_tlv_list_length_title(tlv_id_list, tlv_data_list, ip_address, payload_length):
    this_string = "IP %s -> GOT TLV ID/DATA LENGTHS %s/%s (packet=%s BYTES)" % (ip_address,
                                                                                len(tlv_id_list),
                                                                                len(tlv_data_list),
                                                                                payload_length)
    print(this_string)


def print_inverter_tlv_id_and_data_list(tlv_id_list, tlv_data_list):
    # Loop through the user TLVs and print.
    number_of_user_tlv = len(tlv_id_list)

    # for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
    for iggy in range(0, number_of_user_tlv):

        if tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_1_TLV_TYPE:                                                     # = 1002
            print("TLV %s: ignoring deprecated TLV ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_2_TLV_TYPE:                                                   # = 1003
            print("TLV %s: ignoring deprecated TLV ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_VERSION_STRING_TLV_TYPE:                                                 # = 1004
            print("TLV %s: VERSION = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_PRODUCT_PART_NUMBER_STRING_TLV_TYPE:                                     # = 1005
            print("TLV %s: PROD_PART_NO = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_SERIAL_NUMBER_STRING_TLV_TYPE:                                           # = 1006
            print("TLV %s: SERIAL_NO = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE:                                             # = 1007
            print("TLV %s: MAC = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE:                                                 # = 1008
            print("TLV %s: PCC = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE:                                                # = 1009
            print("TLV %s: FEED = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:                                                    # = 1010
            if tlv_data_list[iggy] == RUNMODE_SG424_APPLICATION:
                this_string = "APPLICATION"
            elif tlv_data_list[iggy] == RUNMODE_SG424_BOOT_LOADER:
                this_string = "BOOT_LOADER"
            elif tlv_data_list[iggy] == RUNMODE_SG424_BOOT_UPDATER:
                this_string = "BOOT_UPDATER"
            else:
                this_string = "***NON-SENSICAL***"
            print("TLV %s: RUNMODE = %s -> %s" % (tlv_id_list[iggy], tlv_data_list[iggy], this_string))
        elif tlv_id_list[iggy] == SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE:                                            # = 1011
            print("TLV %s: BOOT_VERSION = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE:                                                # = 1012
            print("TLV %s: XET_INFO = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_ACTIVE_U16_TLV_TYPE:                                    # = 1013
            print("TLV %s: CATCH_UP_REPORTS_ACTIVE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_BOTH_U16_TLV_TYPE:                                      # = 1014
            print("TLV %s: CATCH_UP_REPORTS_BOTH = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_INFO_U16_TLV_TYPE:                                      # = 1015
            print("TLV %s: CATCH_UP_REPORTS_INFO = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_3_TLV_TYPE:                                                   # = 1016
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE:                                        # = 1017
            if len(tlv_data_list[iggy]) == 17: # Exactly 17 characters in a mac: 12:45:78:01:34:67
                print("TLV %s: HOS_MAC = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
            else:
                print("TLV %s: ***BADLY FORMATED MAC*** = %s " % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_4_TLV_TYPE:                                                   # = 1018
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DEBUG_COUNTERS_U32_ARRAY_TLV_TYPE:                                       # = 1019
            print("TLV %s: ***skipping_over_ignoring_unexpected_debug_counters_array***" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE:                                           # = 1020
            print("TLV %s: NUMBER_OF_RESETS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_5_TLV_TYPE:                                                   # = 1021
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_FD_STATE_U16_TLV_TYPE:                                                   # = 1022
            print("TLV %s: FEED_DETECT_STATE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_SW_PROFILE_U16_TLV_TYPE:                                                 # = 1023
            print("TLV %s: SW_PROFILE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:                                                   # = 1024
            print("TLV %s: INV_CTRL = 0x%s" % (tlv_id_list[iggy], hex(tlv_data_list[iggy])[2:].zfill(2)))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_6_TLV_TYPE:                                                   # = 1025
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_7_TLV_TYPE:                                                   # = 1026
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE:                                         # = 1027
            print("TLV %s: KEEP_ALIVE_TIMEOUT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_UPTIME_SECS_U32_TLV_TYPE:                                                # = 1028
            print("TLV %s: UPTIME_SECS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_8_TLV_TYPE:                                                   # = 1029
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_SET_RESULT_U16_TLV_TYPE:                          # = 1030
            print("TLV %s: BOOT_UPDATER_RSTPRI_AF_SET_RESULT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_BOOT_UPDATER_RSTPRI_AF_CLEAR_RESULT_U16_TLV_TYPE:                        # = 1031
            print("TLV %s: BOOT_UPDATER_RSTPRI_AF_CLEAR_RESULT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_ENERGY_REPORT_CONTROL_U16_TLV_TYPE:                                      # = 1032
            print("TLV %s: ERC = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DC_SM_OUTPUT_STRING_TLV_TYPE:                                            # = 1033
            print("TLV %s: DC_SM_OUTPUT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_9_TLV_TYPE:                                                   # = 1034
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE:               # = 1035
            print("TLV %s: AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE:               # = 1036
            print("TLV %s: AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE:            # = 1037
            print("TLV %s: AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE:                  # = 1038
            print("TLV %s: AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.3f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE:          # = 1039
            print("TLV %s: AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE:           # = 1040
            print("TLV %s: AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE:                 # = 1041
            print("TLV %s: AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE:              # = 1042
            print("TLV %s: AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE:                                  # = 1043
            print("TLV %s: DC_SM_INSTANTANEOUS_WATTS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE:                                  # = 1044
            print("TLV %s: DC_SM_INSTANTANEOUS_VOLTS = %s" % (tlv_id_list[iggy], "%.2f" % round(tlv_data_list[iggy], 2)))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE:                                   # = 1045
            print("TLV %s: DC_SM_INSTANTANEOUS_AMPS = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_10_TLV_TYPE:                                                  # = 1046
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_11_TLV_TYPE:                                                  # = 1047
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_KEEP_ALIVE_GONE_U32_TLV_TYPE:                                            # = 1048
            print("TLV %s: KEEP_ALIVE_GONE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_KEEP_ALIVE_BACK_U32_TLV_TYPE:                                            # = 1049
            print("TLV %s: KEEP_ALIVE_BACK = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_KEEP_ALIVE_LATE_U32_TLV_TYPE:                                            # = 1050
            print("TLV %s: KEEP_ALIVE_LATE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_SEQUENCE_NUMBER_REPEAT_U32_TLV_TYPE:                                     # = 1051
            print("TLV %s: SEQ_NUM_REPEAT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_SEQUENCE_NUMBER_OOS_U32_TLV_TYPE:                                        # = 1052
            print("TLV %s: SEQ_NUM_OOS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_AC_VOLTAGE_OFFSET_FROM_EEPROM_F32_TLV_TYPE:                 # = 1053
            print("TLV %s: AIF_SETTINGS_AC_V_OFFSET_FROM_EEPROM = %s" % (tlv_id_list[iggy],  '{0:+.3f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_FROM_EEPROM_F32_TLV_TYPE:           # = 1054
            print("TLV %s: AIF_SETTINGS_MAX_OUT_PWR_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE:           # = 1055
            print("TLV %s: AIF_SETTINGS_RECONN_RR_PCT_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_FROM_EEPROM_U32_TLV_TYPE:      # = 1056
            print("TLV %s: AIF_SETTINGS_RTS_DEL_MSEC_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_FROM_EEPROM_F32_TLV_TYPE:                 # = 1057
            print("TLV %s: AIF_SETTINGS_MAX_OUT_VA_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_FROM_EEPROM_F32_TLV_TYPE:           # = 1058
            print("TLV %s: AIF_SETTINGS_MAX_OUT_VAR_Q1Q4_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_FROM_EEPROM_F32_TLV_TYPE:           # = 1059
            print("TLV %s: AIF_SETTINGS_MIN_PF_Q1_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.3f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_FROM_EEPROM_F32_TLV_TYPE:           # = 1060
            print("TLV %s: AIF_SETTINGS_MIN_PF_Q4_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.3f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_FROM_EEPROM_F32_TLV_TYPE:          # = 1061
            print("TLV %s: AIF_SETTINGS_OUT_VAR_RR_PCT_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_FROM_EEPROM_F32_TLV_TYPE:            # = 1062
            print("TLV %s: AIF_SETTINGS_V_WAVER_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_FROM_EEPROM_U16_TLV_TYPE:           # = 1063
            print("TLV %s: AIF_SETTINGS_V_WAVER_ENA_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_POWER_WAVERING_WATTS_FROM_EEPROM_F32_TLV_TYPE:              # = 1064
            print("TLV %s: AIF_SETTINGS_W_WAVER_FROM_EEPROM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_FROM_EEPROM_U16_TLV_TYPE:            # = 1065
            print("TLV %s: AIF_SETTINGS_REAL_REACT_PRIO_FROM_EEPROM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_CURTAILMENT_PACKETS_U32_TLV_TYPE:                                        # = 1066
            print("TLV %s: CURTAILMENT_PACKETS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_REGULATION_PACKETS_U32_TLV_TYPE:                                         # = 1067
            print("TLV %s: REGULATION_PACKETS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_BATTERY_U16_TLV_TYPE:                                                    # = 1068
            print("TLV %s: BATTERY = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:                                 # = 1069
            print("TLV %s: UPSTREAM_LINK_TOGGLE_COUNT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:                               # = 1070
            print("TLV %s: DOWNSTREAM_LINK_TOGGLE_COUNT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_ALL_BITS_U32_TLV_TYPE:                                               # = 1071
            print("TLV %s: MOM_ALL_BITS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_STEP_SIZE_HI_WM_U16_TLV_TYPE:                                        # = 1072
            print("TLV %s: MOM_STEP_SIZE_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_VREF_SETTING_HI_WM_I16_TLV_TYPE:                                     # = 1073
            print("TLV %s: MOM_VREF_SETTING_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_VREF_SETTING_LO_WM_I16_TLV_TYPE:                                     # = 1074
            print("TLV %s: MOM_VREF_SETTING_LO_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_HI_WM_I16_TLV_TYPE:                                # = 1075
            print("TLV %s: ACTUAL_PSOLAR_WDC_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_ACTUAL_PSOLAR_WDC_LO_WM_I16_TLV_TYPE:                                # = 1076
            print("TLV %s: MOM_ACTUAL_PSOLAR_WDC_LO_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PHASE_ANGLE_360_ACTUAL_INUSE_INDEGREES_I16_TLV_TYPE:                 # = 1077
            print("TLV %s: MOM_PHASE_ANGLE_360_ACTUAL_INUSE_INDEGREES = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_HI_WM_F32_TLV_TYPE:                     # = 1078
            print("TLV %s: MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_HI_WM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_LO_WM_F32_TLV_TYPE:                     # = 1079
            print("TLV %s: MOM_DUTY_CYCLE_TABLE_FILL_ACTUAL_LO_WM = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_TS_PHASE_ERROR_HI_WM_I16_TLV_TYPE:                                   # = 1080
            print("TLV %s: MOM_TS_PHASE_ERROR_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_TS_PHASE_ERROR_LO_WM_I16_TLV_TYPE:                                   # = 1081
            print("TLV %s: MOM_TS_PHASE_ERROR_LO_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PLINE_WRMS_HI_WM_I16_TLV_TYPE:                                       # = 1082
            print("TLV %s: MOM_PLINE_WRMS_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_PLINE_WRMS_LO_WM_I16_TLV_TYPE:                                       # = 1083
            print("TLV %s: MOM_PLINE_WRMS_LO_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_HI_WM_I16_TLV_TYPE:                    # = 1084
            print("TLV %s: MOM_APPARENT_PWR_CALC_FROM_PSOLAR_HI_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_APPARENT_PWR_CALC_FROM_PSOLAR_LO_WM_I16_TLV_TYPE:                    # = 1085
            print("TLV %s: MOM_APPARENT_PWR_CALC_FROM_PSOLAR_LO_WM = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_MOM_FAULT_NUMBER_U16_TLV_TYPE:                                           # = 1086
            print("TLV %s: MOM_FAULT_NUMBER = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE:                    # = 1087
            print("TLV %s: FALLBACK_POWER_SETTING_PERCENT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_FROM_EEPROM_F32_TLV_TYPE:           # = 1088
            print("TLV %s: AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_FROM_EEPROM_F32_TLV_TYPE:         # = 1089
            print("TLV %s: AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT = %s" % (tlv_id_list[iggy], '{0:.2f}'.format(tlv_data_list[iggy])))
        elif tlv_id_list[iggy] == SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE:                                              # = 1090
            print("TLV %s: NTP_SERVER_URL = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_12_TLV_TYPE:                                                  # = 1091
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE:                                                # = 1092
            print("TLV %s: PCS_UNIT_ID = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE:                                        # = 1093
            print("TLV %s: DC_GWA_SM_OUTPUT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_13_TLV_TYPE:                                                  # = 1094
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_14_TLV_TYPE:                                                  # = 1095
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_15_TLV_TYPE:                                                  # = 1096
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_16_TLV_TYPE:                                                  # = 1097
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_17_TLV_TYPE:                                                  # = 1098
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == DEPRECATED_SG424_TO_GTW_18_TLV_TYPE:                                                  # = 1099
            print("TLV %s: ignoring_deprecated_TLV_ID" % (tlv_id_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_ENABLE_U16_TLV_TYPE:                                        # = 1100
            print("TLV %s: EERPT SERVER ENA  = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_RATE_U16_TLV_TYPE:                                          # = 1101
            print("TLV %s: EERPT SERVER RATE = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_URL_STRING_TLV_TYPE:                                        # = 1102
            print("TLV %s: EERPT SERVER URL  = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_PORT_U16_TLV_TYPE:                                          # = 1103
            print("TLV %s: EERPT SERVER PORT = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE:                                  # = 1104
            print("TLV %s: EERPT SERVER USER = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))
        elif tlv_id_list[iggy] == SG424_TO_GTW_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE:                                   # = 1105
            print("TLV %s: EERPT SERVER PASS = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))

        # The following not included:
        # SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE = 1106
        # SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE = 1107
        # SG424_TO_GTW_CURRENT_STATUS_STRUCTURED_TLV_TYPE = 1108
        # SG424_TO_GTW_XET_INFO_STRUCTURED_TLV_TYPE = 1109
        # SG424_TO_GTW_XET_AIF_VRT_STRUCTURED_TLV_TYPE = 1110
        # SG424_TO_GTW_XET_AIF_FRT_STRUCTURED_TLV_TYPE = 1111
        # SG424_TO_GTW_XET_AIF_VOLT_WATT_STRUCTURED_TLV_TYPE = 1112
        # SG424_TO_GTW_XET_AIF_VOLT_VAR_STRUCTURED_TLV_TYPE = 1113
        # SG424_TO_GTW_XET_AIF_FREQ_WATT_STRUCTURED_TLV_TYPE = 1114
        # SG424_TO_GTW_XET_AIF_CURVE_STRUCTURED_TLV_TYPE = 1115
        # SG424_TO_GTW_XET_AIF_STATUS_STRUCTURED_TLV_TYPE = 1116
        # SG424_TO_GTW_TLV_MSGS_COUNTERS_STRUCTURED_TLV_TYPE = 1117
        # SG424_TO_GTW_PACKET_COUNTERS_STRUCTURED_TLV_TYPE = 1118
        # SG424_TO_GTW_MISC_COUNTERS_STRUCTURED_TLV_TYPE = 1119
        # SG424_TO_GTW_EEPROM_COUNTERS_STRUCTURED_TLV_TYPE = 1120
        # SG424_TO_GTW_ERPT_COUNTERS_STRUCTURED_TLV_TYPE = 1121
        # SG424_TO_GTW_LLDP_COUNTERS_STRUCTURED_TLV_TYPE = 1122
        # SG424_TO_GTW_MAC_COUNTERS_STRUCTURED_TLV_TYPE = 1123
        # SG424_TO_GTW_TCP_COUNTERS_STRUCTURED_TLV_TYPE = 1124
        # SG424_TO_GTW_ENERGY_REPORT_STRUCTURED_TLV_TYPE = 1125
        # SG424_TO_GTW_ALARM_REPORT_STRUCTURED_TLV_TYPE = 1126
        # SG424_TO_GTW_DEBUG_COUNTERS_STRUCTURED_TLV_TYPE = 1127

        elif tlv_id_list[iggy] == SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE:                                                   # = 1128
            print("TLV %s: GROUP_ID = %s" % (tlv_id_list[iggy], tlv_data_list[iggy]))

        else:
            # Who is this miscreant?
            # TODO: FIX THIS SUCH THAT THE ID IS PRINTABLE (ElSE IT CRASHES!!!)
            print("TLV : ***UNKNOWN TLV ID ***") # (tlv_id_list[iggy]))

    # end ... for loop


def get_funky_packed_mac(packed_data, tlv_rank):
    # This special porpoise method gets the next three (3) 16-bit UNSIGNED SHORTS
    # that compose a MAC. Presumably, a "get_packed_data_16bit_unsigned_short" method
    # could have been devised and have the user call it 3 times in a row. But knowing
    # it's to be extracted and treated as a MAC, this method has merit.
    this_tlv_data = packed_data[tlv_rank:tlv_rank + 6]
    (short_1, short_2, short_3) = struct.unpack('!HHH', this_tlv_data)
    tlv_rank = tlv_rank + 6
    return (short_1, short_2, short_3, tlv_rank)


def send_cli_command(ip_address, cli_command):
    send_successful = False

    # Construct the CLI COMMAND TLV data that will be inserted into the UDP payload.
    # The data consists of the MASTER TLV (mandatory and must be the 1st TLV),
    # followed by the CLI COMMAND TLV.
    number_of_user_tlv = 1
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_string(packed_data, GTW_TO_SG424_CLI_COMMAND_STRING_TLV_TYPE, cli_command)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_CLI_COMMAND_BY_TLV_ERROR)
        pass
    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
        except:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_CLI_COMMAND_BY_TLV_ERROR)
            pass
        else:
            # Done. No news is good news. Nothing to do here. Go home.
            send_successful = True

        # Close the socket:
        try:
            fancy_socks.close()
        except:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_CLI_COMMAND_BY_TLV_ERROR)
            pass
        else:
            pass

    return send_successful


def is_this_ip_in_database(this_ip):
    ip_in_database = False
    fetch_string = "SELECT COUNT(*) FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                               INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    fetch_args = (this_ip, )
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, (fetch_args))
    number_of_entries = int(fetch_results['COUNT(*)'])
    if number_of_entries == 0:
        # The IP is not in the table.
        ip_in_database = False
    elif number_of_entries == 1:
        # Yes, it's in the database.
        ip_in_database = True
    else:
        # Should be 0 or 1. Means a duplicate entry. Auditor needs to catch this.
        # Return True nevertheless.
        ip_in_database = True
    return ip_in_database


def is_this_ip_battery_inverter(this_ip):
    i_am_battery_inverter = False

    select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (this_ip, )
    inverters_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if inverters_row:
        battery = inverters_row[INVERTERS_TABLE_BATTERY_COLUMN_NAME]
        if battery == 1:
            i_am_battery_inverter = True
    return i_am_battery_inverter


def is_this_mac_address_in_database(this_mac):
    mac_in_database = False
    fetch_string = "SELECT COUNT(*) FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                               INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, "%s")
    fetch_args = (this_mac, )
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, (fetch_args))
    number_of_entries = int(fetch_results['COUNT(*)'])
    if number_of_entries == 0:
        # The mac is not in the table.
        mac_in_database = False
    elif number_of_entries == 1:
        # Yes, it's in the database.
        mac_in_database = True
    else:
        # Should be 0 or 1. Means a duplicate entry. Auditor needs to catch this.
        # Return True nevertheless.
        mac_in_database = True
    return mac_in_database


def is_this_serial_number_in_database(this_serial_number):
    sn_in_database = False
    fetch_string = "SELECT COUNT(*) FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                               INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME, "%s")
    fetch_args = (this_serial_number, )
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, (fetch_args))
    number_of_entries = int(fetch_results['COUNT(*)'])
    if number_of_entries == 0:
        # The serial number is not in the table.
        sn_in_database = False
    elif number_of_entries == 1:
        # Yes, it's in the database.
        sn_in_database = True
    else:
        # Should be 0 or 1. Means a duplicate entry. Auditor needs to catch this.
        # Return True nevertheless.
        sn_in_database = True
    return sn_in_database


def base_inverter_info_from_inverters_id(some_inverters_id):
    # Get the inverter's IP based on inverters_id.
    inverter_ip = ""
    inverter_mac = ""
    inverter_serial_number = ""
    inverter_string_id = ""
    inverter_string_position = 0
    select_string = "SELECT %s,%s,%s,%s,%s FROM %s where %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                            INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                                                                            INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                                                                            INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                                            INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                            DB_INVERTER_NXO_TABLE,
                                                                            INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
    select_args = (some_inverters_id, )
    inverters_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)

    if inverters_row:
        inverter_ip = inverters_row[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        inverter_mac = inverters_row[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME]
        inverter_serial_number = inverters_row[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME]
        inverter_string_id = inverters_row[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
        inverter_string_position = inverters_row[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME]
        if not inverter_ip:
            inverter_ip = ZERO_IP_ADDRESS
        if not inverter_mac:
            inverter_mac = ZERO_MAC_ADDRESS
        if not inverter_serial_number:
            inverter_serial_number = "NULL"
        if not inverter_string_id:
            inverter_string_id = DEFAULT_STRING_ID
    else:
        inverter_ip = ""
    return (inverter_ip, inverter_mac, inverter_serial_number, inverter_string_id, inverter_string_position)


def get_db_inverters_row_data(some_inverter_ip):
    # Get the following columns from the inverters table based on IP.
    #
    # - version
    # - das_boot
    # - prodPartNo
    # - serialNumber
    # - mac_address
    # - number of resets
    # - FEED_NAME
    # - connect
    # - battery discharger
    # - PCS UNIT ID
    # - group ID
    select_string = "SELECT * FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                         INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")

    select_args = (some_inverter_ip,)
    this_db_row_data_list = prepared_act_on_database(FETCH_ONE, select_string, select_args)

    # Check that all string-based rows are populated with "something" to check against.
    if not this_db_row_data_list[INVERTERS_TABLE_VERSION_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_VERSION_COLUMN_NAME] = "NULL"
    if not this_db_row_data_list[INVERTERS_TABLE_BOOT_VERSION_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_BOOT_VERSION_COLUMN_NAME] = "NULL"
    if not this_db_row_data_list[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME] = "NULL"
    if not this_db_row_data_list[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME] = "NULL"
    if not this_db_row_data_list[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME] = "NULL"
    # if not this_db_row_data_list[INVERTERS_TABLE_RESETS_COLUMN_NAME]:                        ... not required, it's an int
    #     this_db_row_data_list[INVERTERS_TABLE_RESETS_COLUMN_NAME] = 0
    if not this_db_row_data_list[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME] = "NULL"
    # if not this_db_row_data_list[INVERTERS_TABLE_CONNECT_COLUMN_NAME]:                       ... not required, it's an int
    #     this_db_row_data_list[INVERTERS_TABLE_CONNECT_COLUMN_NAME] = 1
    #
    # Also not required for ka_gone/ka_back/ka_late/seq_num_repeat/seq_num_oos - they are integers.

    return this_db_row_data_list

def get_db_inverters_row_data_by_sn(some_inverter_sn):
    # Get the following columns from the inverters table based on IP.
    #
    # - version
    # - prodPartNo
    # - IP Address
    # - mac_address
    # - FEED_NAME

    select_string = "SELECT * FROM %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                         INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME, "%s")

    select_args = (some_inverter_sn,)
    this_db_row_data_list = prepared_act_on_database(FETCH_ONE, select_string, select_args)

    # Check that all string-based rows are populated with "something" to check against.
    if not this_db_row_data_list[INVERTERS_TABLE_VERSION_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_VERSION_COLUMN_NAME] = "NULL"

    if not this_db_row_data_list[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME] = "NULL"

    if not this_db_row_data_list[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME] = "NULL"

    if not this_db_row_data_list[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME] = "NULL"

    if not this_db_row_data_list[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]:
        this_db_row_data_list[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME] = "NULL"

    return this_db_row_data_list


def get_database_inverter_link_counters(inverter_ip):
    # Get the following columns from the inverters table based on IP.
    #
    # - resets
    # - uptime
    select_string = "SELECT %s,%s,%s,%s FROM %s where %s = %s;" % (INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                                   INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                   INVERTERS_TABLE_RESETS_COLUMN_NAME,
                                                                   INVERTERS_TABLE_UPTIME_COLUMN_NAME,
                                                                   DB_INVERTER_NXO_TABLE,
                                                                   INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (inverter_ip,)
    link_counters_data_list = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    return link_counters_data_list


# -------------------------------------------------------------------------------------------------
#
# INVERTER GROUPS UTILITIES:
def db_get_inverter_group_id(inverter_ip):
    this_group_id = 0
    select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (inverter_ip, )
    this_row_data = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if this_row_data:
        this_group_id = this_row_data[INVERTERS_TABLE_GROUP_ID_COLUMN_NAME]
    return this_group_id


def db_get_group_id_row_data(group_id):
    select_string = "SELECT * FROM %s where %s = %s;" % (DB_GROUPS_TABLE, GROUPS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
    select_args = (group_id, )
    this_row_data = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    return this_row_data


def db_update_inverter_group_audit_required(inverter_ip, update_bit):
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_GROUP_AUDIT_REQUIRED_COLUMN_NAME, "%s",
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (update_bit, inverter_ip)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def get_group_audit_required_ip_and_id_list():
    ip_list = []
    group_id_list = []
    select_string = "SELECT %s,%s FROM %s WHERE %s=1;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                          INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_GROUP_AUDIT_REQUIRED_COLUMN_NAME)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, ())
    if select_result:
        for each_row in select_result:
            ip_list.append(each_row[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
            group_id_list.append(each_row[INVERTERS_TABLE_GROUP_ID_COLUMN_NAME])
    return (ip_list, group_id_list)


# This is not a group-specific utility, but used by the "groups" daemon.
def db_get_pcc_and_feed_value_by_ip(ip_address):
    select_string = "SELECT %s FROM %s where %s=%s;" % (INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (ip_address, )
    this_row_data = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    feed_name_literal = this_row_data[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]

    feed_value = get_feed_name_as_integer(feed_name_literal)
    pcc_value = 0
    if feed_value != 0:
        pcc_value = 1
    return (pcc_value, feed_value)


def send_and_get_group_audit_command(ip_address, packed_data):

    # Assumption: assume life is good.
    private_port_packet = None
    SG424_data = None

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:
        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:
            # Now wait for a response:
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding.
                pass

            else:
                # Packet received, data is in private_port_packet field.
                (inverter_ip_address, donut_care_port, SG424_data) = get_udp_components(private_port_packet)

        # Close the socket:
        try:
            fancy_socks.close()
        except Exception:  # as error:
            print("close socket failed ...")
        else:
            pass

    return SG424_data


def is_group_id_1_sanity_ok():
    group_1_sanity_ok = True
    select_string = "SELECT * FROM %s WHERE %s=%s;" % (DB_GROUPS_TABLE, GROUPS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
    select_arg = (1, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_result:
        if select_result[GROUPS_TABLE_FALLBACK_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_FALLBACK_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_NTP_URL_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_FORCED_UPGRADE_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_IRRADIANCE_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_IRRADIANCE_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_GATEWAY_CONTROL_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_ER_SERVER_HTTP_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_ER_SERVER_HTTP_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_ER_GTW_TLV_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_ER_GTW_TLV_DEFAULT:
            group_1_sanity_ok = False
        elif select_result[GROUPS_TABLE_AIF_ID_COLUMN_NAME] != GROUPS_TABLE_GROUP_ID_1_AIF_ID_DEFAULT:
            group_1_sanity_ok = False

    else:
        # Group ID 1 missing.
        group_1_sanity_ok = False
    return group_1_sanity_ok


def database_gimme_row_null_count(this_table, this_row):
    select_string = "SELECT COUNT(*) FROM %s WHERE %s IS NULL;" % (this_table, this_row)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    this_null_count = int(select_result['COUNT(*)'])
    return this_null_count


def database_fix_row_null_data(this_table, this_row, this_data):
    update_string = "UPDATE %s SET %s=%s WHERE %s IS NULL;" % (this_table, this_row, "%s", this_row)
    update_arg = (this_data, )
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def rectify_group_id_1_data():
    # Requires that group ID 1 be created if it is missing,
    # else attributes set to their prescribed default values.
    alarm_id_to_raise = 0
    group_id_1_row_data = db_get_group_id_row_data(1)
    if group_id_1_row_data:
        # Fix. Let caller deal with any logging/alarming.
        update_string = "UPDATE %s SET %s=%s,%s=%s,%s=%s,%s=%s,%s=%s,%s=%s,%s=%s,%s=%s WHERE %s=%s;" % \
                                                                 (DB_GROUPS_TABLE,
                                                                  GROUPS_TABLE_FALLBACK_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_IRRADIANCE_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_ER_SERVER_HTTP_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_ER_GTW_TLV_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_AIF_ID_COLUMN_NAME, "%s",
                                                                  GROUPS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
        update_args = (GROUPS_TABLE_GROUP_ID_1_FALLBACK_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_NTP_URL_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_FORCED_UPGRADE_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_IRRADIANCE_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_GATEWAY_CONTROL_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_ER_SERVER_HTTP_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_ER_GTW_TLV_DEFAULT,
                       GROUPS_TABLE_GROUP_ID_1_AIF_ID_DEFAULT,
                       1)
        prepared_act_on_database(EXECUTE, update_string, update_args)
        alarm_id_to_raise = ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RESET_DEFAULTS

    else:
        # Missing? Then fix. Let caller deal with any logging/alarming.
        insert_string = "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);" \
                                                               % (DB_GROUPS_TABLE,
                                                                  GROUPS_TABLE_GROUP_ID_COLUMN_NAME,
                                                                  GROUPS_TABLE_ALIAS_COLUMN_NAME,
                                                                  GROUPS_TABLE_LOCATION_COLUMN_NAME,
                                                                  GROUPS_TABLE_NOTES_COLUMN_NAME,
                                                                  GROUPS_TABLE_FALLBACK_COLUMN_NAME,
                                                                  GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME,
                                                                  GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME,
                                                                  GROUPS_TABLE_IRRADIANCE_COLUMN_NAME,
                                                                  GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME,
                                                                  GROUPS_TABLE_ER_SERVER_HTTP_COLUMN_NAME,
                                                                  GROUPS_TABLE_ER_GTW_TLV_COLUMN_NAME,
                                                                  GROUPS_TABLE_AIF_ID_COLUMN_NAME,
                                                                  "%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")
        insert_args = (1, GROUPS_TABLE_GROUP_ID_1_ALIAS_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_LOCATION_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_NOTES_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_FALLBACK_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_NTP_URL_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_FORCED_UPGRADE_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_IRRADIANCE_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_GATEWAY_CONTROL_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_ER_SERVER_HTTP_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_ER_GTW_TLV_DEFAULT,
                          GROUPS_TABLE_GROUP_ID_1_AIF_ID_DEFAULT)
        prepared_act_on_database(EXECUTE, insert_string, insert_args)
        alarm_id_to_raise = ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RE_CREATED

    return alarm_id_to_raise


def rectify_inverters_missing_group_id(missing_groups_list):
    # Call this when inverters are assigned to a group ID not in the groups table.
    # When detected, set their group ID to the default group and for auditing.
    for each_group_id in missing_groups_list:
        update_string = "UPDATE %s SET %s=%s,%s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_GROUP_ID_COLUMN_NAME, "%s",
                                                                    INVERTERS_TABLE_GROUP_AUDIT_REQUIRED_COLUMN_NAME, "%s",
                                                                    INVERTERS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
        update_args = (1, 1, each_group_id)
        prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def rectify_inverters_null_group_id():
    # update inverters set group_id=1,group_audit_required=1 where group_id IS NULL;
    update_string = "UPDATE %s SET %s=1,%s=1 where %s IS NULL;" % (DB_INVERTER_NXO_TABLE,
                                                                   INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                                                                   INVERTERS_TABLE_GROUP_AUDIT_REQUIRED_COLUMN_NAME,
                                                                   INVERTERS_TABLE_GROUP_ID_COLUMN_NAME)
    prepared_act_on_database(EXECUTE, update_string, ())
    return


def get_inverters_group_id_missing_from_groups_table():
    # Determine if any inverters have a group ID not found in the groups table.
    missing_group_id_list = []

    # Get the list of distinct group_id from the inverters table.
    select_string = "SELECT DISTINCT %s from %s;" % (INVERTERS_TABLE_GROUP_ID_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, ())
    if select_result:
        for each_row in select_result:
            this_group_id = each_row[INVERTERS_TABLE_GROUP_ID_COLUMN_NAME]
            # There is expected to be at least 1 entry from the groups table.
            this_group_row_data = db_get_group_id_row_data(this_group_id)
            if this_group_row_data:
                # Inverters with this group ID are represented in the groups table. Nothing to do here.
                pass
            else:
                # 1 or more inverters have a group ID missing from groups. Will need to be rectified.
                missing_group_id_list.append(this_group_id)
    else:
        # This is not right ... something very wrong here. Unless the inverters table is empty?
        pass

    return missing_group_id_list


def get_inverters_null_group_id_count():
    # select count(*) from inverters where group_id is NULL;
    select_string = "SELECT COUNT(*) FROM %s WHERE %s IS NULL;" % (DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_GROUP_ID_COLUMN_NAME)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    null_count = int(select_result['COUNT(*)'])
    return null_count


def send_and_query_by_encapsulated_group_id(tlv_packed_data):

    multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
    this_port = SG424_PRIVATE_UDP_PORT

    if is_send_multicast_packet_and_close_socket(tlv_packed_data, multicast_ip, this_port):

        # Multicast packet sent, now setup for listening.
        #
        # NOTE: multicasted TLV command already on its way towards the field of inverters, but this method is
        #       not yet setup for listening! This is being setup right now. Testing shows that the following
        #       setup, binding and listening will happen long before a single inverter responds, so this is
        #       "safe". Stricktly speaking though, this is cause for discomfort.
        gateway_lan_address = get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        try:
            clean_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            clean_socks.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            clean_socks.settimeout(5.0)
        except Exception as error:
            print("socket.socket/setsockopt failed (listening) ...")
        else:
            # Bind socket to local host and "well-known" port
            try:
                clean_socks.bind((gateway_lan_address, GATEWAY_QUERY_RESPONSE_PORT))
            except Exception:  # as error:
                print("bind failed ...")
            else:

                # Wait for a response. Since this method is listening for "any response" after sending out
                # a multicast TLV command, the proper thing to do is to listen for the possibility of more
                # than 1 response. You just never know. The timeout is 5 seconds. Wait for at least 1 more
                # timeout period if a response is received.
                still_wiggling = 2
                while still_wiggling > 0:
                    try:
                        private_port_packet = clean_socks.recvfrom(4096)
                    except socket.timeout:
                        # Timed out waiting to receive a packet. This is normal, so wiggle a little less.
                        still_wiggling -= 1
                        if still_wiggling > 0:
                            print("No response received, wait a bit more ...")
                        else:
                            print("No other response received, and no more waiting ...")
                        continue
                    except Exception as error:
                        print("Socket receive error - %s" % error)
                        break
                    else:

                        # Packet received.
                        (ip_address, ip_address_port, SG424_data) = get_udp_components(private_port_packet)
                        payload_length = len(SG424_data)
                        (tlv_id_list, tlv_values_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                        print_inverter_tlv_list_length_title(tlv_id_list, tlv_values_list, ip_address, payload_length)
                        print_inverter_tlv_id_and_data_list(tlv_id_list, tlv_values_list)

                # Close the socket.
                clean_socks.close()

    return


# END OF INVERTER GROUPS UTILITIES:
#
# ----------------------------------------------=--------------------------------------------------


# -------------------------------------------------------------------------------------------------
#
# GPTS TEST UTILITIES
#
# This is a set of quick and dirty test facilities to verify that inverters
# can deal with group ID packets, including badly-formated ones
#
# These functions build TLV packed data to be sent to inverters. The intent is to test a variety
# of "group ID" updates, such as:
#
# - sending a TLV containing 1 group ID specification with variable number of sub-TLVs
# - compound (i.e., multiple) group ID spec
# - badly formatted/corrupted group ID centric packets
#


def build_simple_group_id_packed_tlv_test_1(group_id):
    # Group ID test number 1
    #
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with 1 group ID TLV containing 3 fixed-data
    # sub-TLVs, so the master TLV will indicate "1 user-specified TLV":
    #   - fallback of 13
    #   - NTP URL mamamia.org
    #   - keepAlive time out = 3111
    print("TEST 1 - BUILD SIMPLE GROUP ID PACKET: 1 GROUP WITH 3 SUB-TLVs")

    sub_group_packed_data = add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 13)
    sub_group_packed_data = append_user_tlv_string(sub_group_packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "mamamia.org")
    sub_group_packed_data = append_user_tlv_32bit_unsigned_long(sub_group_packed_data, GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3111)
    sub_group_length = len(sub_group_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 3
    group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_length, group_id)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data + group_id_packed_data + sub_group_packed_data
    return complete_group_id_packed_data


def build_simple_group_id_packed_tlv_test_2(group_id):
    # Group ID test number 2
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with 1 group ID TLV containing 3 fixed-data
    # sub-TLVs, so the master TLV will indicate "1 user-specified TLV":
    #   - fallback of 22
    #   - keepAlive time out = 3222
    #   - power factor 0.98
    #   - NTP URL mama_was_a_rolling_stone.org
    print("TEST 2 - BUILD SIMPLE GROUP ID PACKET: 1 GROUP WITH 4 SUB-TLVs")

    sub_group_packed_data = add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 22)
    sub_group_packed_data = append_user_tlv_32bit_unsigned_long(sub_group_packed_data, GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3222)
    sub_group_packed_data = append_user_tlv_32bit_float(sub_group_packed_data, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE, 0.98)
    sub_group_packed_data = append_user_tlv_string(sub_group_packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "mama_was_a_rolling_stone.org")
    sub_group_length = len(sub_group_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 4
    group_id_packed_data_start = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_length, group_id)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data + group_id_packed_data_start + sub_group_packed_data
    return complete_group_id_packed_data

def build_simple_group_id_packed_tlv_test_3(group_id):
    # Group ID test number 3
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with 1 group ID TLV containing 3 fixed-data
    # sub-TLVs, so the master TLV will indicate "1 user-specified TLV":
    #   - keepAlive time out = 3333
    #   - fallback of 33
    #   - RAMP_RATE_UP_PCT = 0.97
    #   - NTP URL father_knows_best.org
    #   - RAMP_RATE_DOWN_PCT = 0.96
    print("TEST 3 - BUILD SIMPLE GROUP ID PACKET: 1 GROUP WITH 5 SUB-TLVs")

    sub_group_packed_data = add_user_tlv_32bit_unsigned_long(GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3333)
    sub_group_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 33)
    sub_group_packed_data = append_user_tlv_32bit_float(sub_group_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.97)
    sub_group_packed_data = append_user_tlv_string(sub_group_packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "father_knows_best.org")
    sub_group_packed_data = append_user_tlv_32bit_float(sub_group_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_length = len(sub_group_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 5
    group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_length, group_id)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data + group_id_packed_data + sub_group_packed_data
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_4(group_id_1, group_id_2):
    # Group ID test number 4
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with:
    # - 1st group, 3 fixed-data sub-TLVs: - keepAlive time out = 3444
    #                                     - fallback of 34
    #                                     - RAMP_RATE_UP_PCT = 0.97
    # - 2nd group, 6 fixed-data sub-TLVs: - RAMP_RATE_DOWN_PCT = 0.96
    #                                     - NTP URL = father_knows_best.org
    #                                     - MAXIMUM_AC_OUTPUT_POWER = 222.2
    #                                     - MAXIMUM_OUTPUT_VAR_Q1Q4 = 229.3
    #                                     - POWER_WAVERING_WATTS = 0.44
    #                                     - MINIMUM_POWER_FACTOR_Q1 = 0.777
    print("TEST 4 - BUILD COMPOUND GROUP ID PACKET: 1st GROUP: 3 SUB-TLVs, 2nd GROUP: 6 sub-TLVs")

    sub_group_1_packed_data = add_user_tlv_32bit_unsigned_long(GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3444)
    sub_group_1_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_1_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 34)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.97)
    sub_group_1_length = len(sub_group_1_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 3
    group_id_1_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_1_length, group_id_1)
    number_of_user_tlv += 1

    sub_group_2_packed_data = add_user_tlv_32bit_float(GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_2_packed_data = append_user_tlv_string(sub_group_2_packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "have_you_been_good_today.org")
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_IN_EEPROM_F32_TLV_TYPE, 222.2)
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 229.3)
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE, 0.44)
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_IN_EEPROM_F32_TLV_TYPE, 0.777)
    sub_group_2_length = len(sub_group_2_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv += 6
    group_id_2_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_2_length, group_id_2)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data \
                                    + (group_id_1_packed_data + sub_group_1_packed_data) \
                                    + (group_id_2_packed_data + sub_group_2_packed_data)
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_5(group_id_1, group_id_2):
    # Group ID test number 4
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with:
    # - 1st group, 5 fixed-data sub-TLVs: - keepAlive time out = 3666
    #                                     - fallback of 44
    #                                     - RAMP_RATE_UP_PCT = 0.96
    #                                     - MAXIMUM_OUTPUT_VAR_Q1Q4 = 222.2
    #                                     - POWER_WAVERING_WATTS = 0.33
    # - 2nd group, 4 fixed-data sub-TLVs: - NTP URL father_knows_best.org
    #                                     - RAMP_RATE_DOWN_PCT = 0.96
    #                                     - keepAlive time out = 3777
    #                                     - fallback of 55
    print("TEST 5 - BUILD COMPOUND GROUP ID PACKET: 1st GROUP: 5 SUB-TLVs, 2nd GROUP: 4 sub-TLVs")

    sub_group_1_packed_data = add_user_tlv_32bit_unsigned_long(GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3666)
    sub_group_1_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_1_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 44)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 222.2)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE, 0.33)
    sub_group_1_length = len(sub_group_1_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 5
    group_id_1_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_1_length, group_id_1)
    number_of_user_tlv += 1

    sub_group_2_packed_data = add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "believe_me_when_i_say_it.org")
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_2_packed_data = append_user_tlv_32bit_unsigned_long(sub_group_2_packed_data, GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3777)
    sub_group_2_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_2_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 55)
    sub_group_2_length = len(sub_group_2_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv += 4
    group_id_2_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_2_length, group_id_2)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data \
                                    + (group_id_1_packed_data + sub_group_1_packed_data) \
                                    + (group_id_2_packed_data + sub_group_2_packed_data)
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_6(group_id_1, group_id_2):
    # Group ID test number 4
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with:
    #
    # - 1st group, 3 fixed-data sub-TLVs: - keepAlive time out = 3888
    #                                     - fallback = 66
    #                                     - RAMP_RATE_UP_PCT = 0.97
    # - 1 plain NON-group ID TLV for all: - NTP URL = I_am_king_of_the_hill.org
    # - 2nd group, 5 fixed-data sub-TLVs: - NTP URL = father_knows_best.org
    #                                     - RAMP_RATE_DOWN_PCT = 0.95
    #                                     - keepAlive time out = 3999
    #                                     - fallback of 77
    #                                     - RAMP_RATE_UP_PCT = 0.97
    # - 1 plain NON-group ID TLV for all: - MAXIMUM_OUTPUT_VAR_Q1Q4 = 211.1
    print("TEST 6 - BUILD COMPOUND GROUP ID PACKET: 1st GROUP: 3 SUB-TLVs, 1 PLAIN TLV, 2nd GROUP: 5 sub-TLVs, 1 PLAIN TLV")

    sub_group_1_packed_data = add_user_tlv_32bit_unsigned_long(GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3888)
    sub_group_1_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_1_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 66)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.97)
    sub_group_1_length = len(sub_group_1_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv = 3
    group_id_1_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_1_length, group_id_1)
    number_of_user_tlv += 1

    plain_tlv_1 = add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "I_am_king_of_the_hill.org")
    number_of_user_tlv += 1

    sub_group_2_packed_data = add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "meatballs_falling_from_heaven.org")
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE, 0.95)
    sub_group_2_packed_data = append_user_tlv_32bit_unsigned_long(sub_group_2_packed_data, GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3999)
    sub_group_2_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_2_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 77)
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.97)
    sub_group_2_length = len(sub_group_2_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv += 5
    group_id_2_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_2_length, group_id_2)
    number_of_user_tlv += 1

    plain_tlv_2 = add_user_tlv_32bit_float(GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 211.1)
    number_of_user_tlv += 1

    # A complete packed data packet:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data \
                                    + (group_id_1_packed_data + sub_group_1_packed_data) \
                                    + plain_tlv_1 \
                                    + (group_id_2_packed_data + sub_group_2_packed_data) \
                                    + plain_tlv_2
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_7(group_id_1, group_id_2):
    # Group ID test number 4
    # Build and return a packed TLV that can be inserted into UDP packet for sending
    # to an inverter - for test purpose. It is the complete packet, including master.
    #
    # This function builds a packed TLV structure with 2 group ID TLVs:
    # - 1 plain NON-group ID TLV for all: - NTP URL = bake_me_a_pie.org
    # - 1st group, 5 fixed-data sub-TLVs: - keepAlive time out = 4444
    #                                     - fallback = 88
    #                                     - RAMP_RATE_UP_PCT = 0.96
    #                                     - MAXIMUM_OUTPUT_VAR_Q1Q4 = 229.7
    #                                     - another keepAlive time out = 4567
    # - 1 plain NON-group ID TLV for all: - MAXIMUM_OUTPUT_VAR_Q1Q4 = 228.8
    # - 1 plain NON-group ID TLV for all: - POWER_WAVERING_WATTS = 0.55
    # - 2nd group, 2 fixed-data sub-TLVs: - NTP URL father_knows_best.org
    #                                     - RAMP_RATE_DOWN_PCT = 0.96
    print("TEST 7 - BUILD COMPOUND GROUP ID PACKET: 1 PLAIN TLV, 1st GROUP: 5 SUB-TLVs, 2 PLAIN TLVs, 2nd GROUP: 2 sub-TLVs")

    plain_tlv_1 = add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "bake_me_a_pie.org")
    number_of_user_tlv = 1

    sub_group_1_packed_data = add_user_tlv_32bit_unsigned_long(GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, 3666)
    sub_group_1_packed_data = append_user_tlv_16bit_unsigned_short(sub_group_1_packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, 44)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 222.2)
    sub_group_1_packed_data = append_user_tlv_32bit_float(sub_group_1_packed_data, GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE, 0.33)
    sub_group_1_length = len(sub_group_1_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv += 5
    group_id_1_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_1_length, group_id_1)
    number_of_user_tlv += 1

    plain_tlv_2 = add_user_tlv_32bit_float(GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 228.8)
    plain_tlv_3 = add_user_tlv_32bit_float(GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE, 0.55)
    number_of_user_tlv += 2

    sub_group_2_packed_data = add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "wednesday_is_hump_day.org")
    sub_group_2_packed_data = append_user_tlv_32bit_float(sub_group_2_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE, 0.96)
    sub_group_2_length = len(sub_group_2_packed_data) + 2 # extra 2 bytes to encompass the group ID itself
    number_of_user_tlv += 2
    group_id_2_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_2_length, group_id_2)
    number_of_user_tlv += 1

    # A complete packed data packet with 7 user-defined TLVs:
    master_packed_data = add_master_tlv(number_of_user_tlv)
    complete_group_id_packed_data = master_packed_data \
                                    + plain_tlv_1 \
                                    + (group_id_1_packed_data + sub_group_1_packed_data) \
                                    + plain_tlv_2 \
                                    + plain_tlv_3 \
                                    + (group_id_2_packed_data + sub_group_2_packed_data)
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_8():
    # Placeholder for future VALID encapsulated compuund group TLVs.
    complete_group_id_packed_data = None
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_9():
    # Placeholder for future VALID encapsulated compuund group TLVs.
    complete_group_id_packed_data = None
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_10():
    # Placeholder for future VALID encapsulated compuund group TLVs.
    complete_group_id_packed_data = None
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_11():
    # Placeholder for future VALID encapsulated compuund group TLVs.
    complete_group_id_packed_data = None
    return complete_group_id_packed_data


def build_compound_group_id_packed_tlv_test_12():
    # Placeholder for future VALID encapsulated compuund group TLVs.
    complete_group_id_packed_data = None
    return complete_group_id_packed_data


#
# END OF GPTS TEST UTILITIES
#
# -------------------------------------------------------------------------------------------------


def set_or_clear_up_down_link_notice(ip_address, column_name, set_clear_value):
    # No checking performed. This updates the "up_notice" or "dn_notice" column based on IP.
    # NOTE: this function is identical to the following!
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          column_name, "%s",
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (set_clear_value, ip_address)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def set_inverter_last_response_timestamp(inverter_ip, some_date_time):

    set_string = "UPDATE %s " \
                 "SET %s = %s, " \
                 "    %s = IF(%s IS NULL or %s < '2011-00-00 00:00:00', %s, %s) " \
                 "WHERE %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                     INVERTERS_TABLE_LAST_RESP_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                     INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                     INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME, "%s",
                                     INVERTERS_TABLE_FIRST_RESP_COLUMN_NAME,
                                     INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")

    set_args = (some_date_time,
                some_date_time,
                inverter_ip)

    prepared_act_on_database(EXECUTE, set_string, set_args)
    return

def increase_in_counter_value(new_value, start_value):
    increased_value = 0
    if new_value > start_value:
        # An increase. Something of note.
        increased_value = new_value - start_value
    return increased_value


def get_tlv_value_from_list(this_tlv, tlv_list, tlv_values_list):
    tlv_was_found = False
    tlv_value = None

    if len(tlv_list) == len(tlv_values_list):
        # for tlv_id, tlv_data in zip(tlv_list, tlv_values_list):
        for iggy in range (0, len(tlv_list)):
            if tlv_list[iggy] == this_tlv:
                # Found it. Compare the value with the action.
                tlv_value = tlv_values_list[iggy]
                tlv_was_found = True
                break
    return (tlv_was_found, tlv_value)


def db_get_ip_and_string_position_from_inverter_id(this_inverter_id):
    ip_address = ZERO_IP_ADDRESS
    string_position = 0
    select_string = "SELECT %s, %s FROM %s where %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                              INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                              DB_INVERTER_NXO_TABLE,
                                                              INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
    select_args = (this_inverter_id, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result:
        ip_address = select_result[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        string_position = select_result[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME]
    return (ip_address, string_position)


def get_inverter_string_id_by_ip(this_ip):
    string_id = DEFAULT_STRING_ID
    select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (this_ip,)
    string_id_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if string_id_row:
        string_id = string_id_row[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
    return string_id


def get_count_by_string_id_battery_value(string_id, battery_value):
    # Get the number of rows from the inverters table based on string ID with battery = 0 or 1.
    inverter_count = 0
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=%s AND %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                        INVERTERS_TABLE_BATTERY_COLUMN_NAME, "%s")
    select_args = (string_id, battery_value)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    inverter_count = int(select_result['COUNT(*)'])
    return inverter_count



def get_ip_list_by_string_id_battery_value(string_id, battery_value):
    # Get the IP list from the inverters table based on string ID with battery = 0 or 1.
    # select IP_Address from inverters where stringId='01:58:e2' and battery=0;
    ip_list = []
    select_string = "SELECT %s FROM %s WHERE %s=%s AND %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                  DB_INVERTER_NXO_TABLE,
                                                                  INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                  INVERTERS_TABLE_BATTERY_COLUMN_NAME, "%s")
    select_args = (string_id, battery_value)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for each_row in select_result:
        ip_list.append(each_row[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
    return ip_list


def get_inverter_status_from_db(inverter_ip_address):
    inverter_status_string = INVERTERS_TABLE_STATUS_STRING_UNKNOWN
    select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_STATUS_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (inverter_ip_address,)
    status_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if status_row:
        inverter_status_string = status_row[INVERTERS_TABLE_STATUS_COLUMN_NAME]
    return inverter_status_string



def get_comma_separated_list_length(comma_separated_list):
    list_length = 0
    if comma_separated_list:
        list_components = comma_separated_list.split(",")
        list_length = len(list_components)
    return list_length


def create_comma_separated_ip_list_from_database():
    comma_separated_ip_list = ""
    fetch_string = "SELECT %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())
    number_of_rows = len(fetch_results)
    row_rank = 0
    for each_row_data in fetch_results:
        if each_row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]:
            comma_separated_ip_list += each_row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            if row_rank != (number_of_rows - 1):
                comma_separated_ip_list += ","
        else:
            # YIKES! A table entry with a NULL IP. It happens. Database consistency audit (ITCA) needs to be run.
            # For now, skip over.
            pass
        row_rank += 1
    print("CREATED TOTAL LIST=->%s<-" % comma_separated_ip_list)
    return comma_separated_ip_list


'''
NOT USED IN SYSTEM, SINCE STATUS DOES NOTHING. YET.
def update_inverter_status_in_db(status_string, inverter_ip_address):
    set_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                           INVERTERS_TABLE_STATUS_COLUMN_NAME, "%s",
                                                           INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    set_args = (status_string, inverter_ip_address)
    prepared_act_on_database(EXECUTE, set_string, set_args)
'''


def update_all_inverter_status_in_db(status_string):
    set_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_STATUS_COLUMN_NAME, "%s")
    set_args = (status_string, )
    prepared_act_on_database(EXECUTE, set_string, set_args)


# This method is intended to send a packet with a TLV that updates a bit in the inverter's INV_CTRL in EEPROM.
# Typically: irradiance, gateway control, low power or sleep.
def is_sent_inv_ctrl_update_bit_by_ip(this_ip, tlv_id, set_clear_value):
    # Send the command with 1 user-defined TLV:
    packet_sent = False
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id, set_clear_value)
    if is_send_tlv_packed_data(this_ip, packed_data):
        packet_sent = True
    return packet_sent


def is_gateway_control_set_in_inv_ctrl(inv_ctrl_as_hex_string):
    # Is the GATEWAY_CONTROL bit SET or CLEARED in INV_CTRL??
    gateway_control_set = False
    inv_ctrl_hex = inv_ctrl_as_hex_string.replace('0x', '')
    if int(inv_ctrl_hex, 16) & (2 ** INV_CTRL_INV_CTRL_GATEWAY_CONTROL_BIT_RANK):
        # The bit is set.
        gateway_control_set = True
    else:
        # The bit is cleared.
        gateway_control_set = False
    return gateway_control_set


def is_sleep_set_in_inv_ctrl(inv_ctrl_as_hex_string):
    # Is the SLEEP bit SET or CLEARED in INV_CTRL??
    sleep_set = False
    inv_ctrl_hex = inv_ctrl_as_hex_string.replace('0x', '')
    if int(inv_ctrl_hex, 16) & (2 ** INV_CTRL_INV_CTRL_SLEEP_BIT_RANK):
        # The bit is set.
        sleep_set = True
    else:
        # The bit is cleared.
        sleep_set = False
    return sleep_set


def is_hos_mac_ok(hos_mac):
    # Given the MAC of a presumed HOS inverter, extract its stringPosition from the database,
    # and verify that its string position is 1.
    hos_mac_ok = True
    # select stringId, stringPosition from inverters where mac_address = 'e0:1f:0a:01:17:a6';
    select_string = "SELECT %s FROM %s where %s = %s;" % (INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                          DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, "%s")
    select_args = (hos_mac,)
    inverters_string_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if inverters_string_row:
        this_string_position = inverters_string_row[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME]
        if this_string_position == 1:
            hos_mac_ok = True
        else:
            hos_mac_ok = False
    else:
        # No stringPosition corresponding to this mac. Something no right here.
        hos_mac_ok = False
    return hos_mac_ok


def get_string_id(hos_mac):
    # The string ID is the last 3 bytes of the HOS's 6-byte mac.
    # Retrieve it the cheap and dirty way by splitting the string.
    this_string_id_list = hos_mac.split(":")
    this_string_id = this_string_id_list[3] + ":" + this_string_id_list[4] + ":" + this_string_id_list[5]
    #print("GOT THIS STRING ID " + this_string_id)
    return this_string_id


def get_mac_list_from_string_id(hos_string_id):
    # Gets the MAC and IP from all entries of the specified stringId, ordered by stringPosition.
    hos_mac_list = []
    hos_ip_list = []
    select_string = "SELECT %s, %s from %s where %s = %s order by %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                          INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                                                                          DB_INVERTER_NXO_TABLE,
                                                                          INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
    select_args = (hos_string_id,)
    mac_ip_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    if len(mac_ip_list) > 0:
        for each_entry in mac_ip_list:
            hos_mac_list.append(each_entry[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME])
            hos_ip_list.append(each_entry[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
    else:
        # Not expected, but can happen if string monitor was not run. Normally,
        # this is caught by the caller of this method, so nothing to do here.
        pass
    return hos_mac_list, hos_ip_list


def these_macs_compare(this_mac, that_mac):
    if this_mac and that_mac:
        this_mac_upper = this_mac.upper()
        that_mac_upper = that_mac.upper()
        if this_mac_upper == that_mac_upper:
            return True
    else:
        return False


def get_hos_real_mac_list_length(hos_mac_list):
    real_mac_list_length = 0
    for iggy in range(0, 8):
        if is_real_mac(hos_mac_list[iggy]):
            real_mac_list_length += 1
    return real_mac_list_length


def is_real_mac(this_mac):
    if this_mac == ZERO_MAC_ADDRESS:
        return False
    else:
        return True


def update_inverters_comm_column_by_ip(comm_column_string, inverter_ip_address):
    set_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                           INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                           INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    set_args = (comm_column_string, inverter_ip_address)
    prepared_act_on_database(EXECUTE, set_string, set_args)

def update_inverters_version_column_by_ip(version_string, inverter_ip_address):
    set_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                           INVERTERS_TABLE_VERSION_COLUMN_NAME, "%s",
                                                           INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    set_args = (version_string, inverter_ip_address)
    prepared_act_on_database(EXECUTE, set_string, set_args)


def update_inverters_resets_column_by_ip(inverter_ip_address, number_of_resets):
    set_string = "UPDATE %s SET %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                           INVERTERS_TABLE_RESETS_COLUMN_NAME, "%s",
                                                           INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    set_args = (number_of_resets, inverter_ip_address)
    prepared_act_on_database(EXECUTE, set_string, set_args)


def update_inverters_feed_name_column_by_ip(inverter_feed_name, inverter_ip_address):
    update_string = "update %s set %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                              INVERTERS_TABLE_FEED_NAME_COLUMN_NAME, "%s",
                                                              INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (inverter_feed_name, inverter_ip_address)
    prepared_act_on_database(EXECUTE, update_string, update_args)


def update_inverters_string_id_string_position_column_by_ip(inverter_string_id, inverter_string_position, inverter_ip_address):
    update_string = "update %s set %s = %s, %s = %s where %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                       INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                       INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME, "%s",
                                                                       INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (inverter_string_id, inverter_string_position, inverter_ip_address)
    prepared_act_on_database(EXECUTE, update_string, update_args)


def update_inverters_aif_immed_column_by_ip(ip_address, aif_immed_update_column_name_list, aif_immed_update_column_value_list):
    update_string = "update %s set " % (DB_INVERTER_NXO_TABLE)
    update_args = []
    # Build the list of parameters:
    for iggy in range (0, len(aif_immed_update_column_name_list)):
        update_string += "%s = %s" % (aif_immed_update_column_name_list[iggy], "%s")
        if iggy < (len(aif_immed_update_column_name_list) -1):
            update_string += ", "
        update_args.append(aif_immed_update_column_value_list[iggy])

    update_string += " where %s = %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args.append(ip_address)
    prepared_act_on_database(EXECUTE, update_string, update_args)


'''
NOT USED, SO COMMENTED OUT.

def update_inverters_aif_immed_column_all_rows(aif_immed_update_column_name_list, aif_immed_update_column_value_list):
    update_string = "update %s set " % (DB_INVERTER_NXO_TABLE)
    update_args = []
    # Build the list of parameters:
    for iggy in range (0, len(aif_immed_update_column_name_list)):
        update_string += "%s = %s" % (aif_immed_update_column_name_list[iggy], "%s")
        if iggy < (len(aif_immed_update_column_name_list) -1):
            update_string += ", "
        update_args.append(aif_immed_update_column_value_list[iggy])
    update_string += ";"
    prepared_act_on_database(EXECUTE, update_string, update_args)
'''


'''
NOT USED, SO COMMENTED OUT.

def update_inverters_aif_immed_column_by_feed_name(aif_immed_update_column_name_list, aif_immed_update_column_value_list, ip_address):
    # Assumption: the IP has already been validated as multicast.

    update_string = "update %s set " % (DB_INVERTER_NXO_TABLE)
    update_args = []
    # Build the list of parameters:
    for iggy in range (0, len(aif_immed_update_column_name_list)):
        update_string += "%s = %s" % (aif_immed_update_column_name_list[iggy], "%s")
        if iggy < (len(aif_immed_update_column_name_list) -1):
            update_string += ", "
        update_args.append(aif_immed_update_column_value_list[iggy])

    feed_name = get_feed_name_from_multicast_ip(ip_address)
    if feed_name:
        if feed_name == 'A' or feed_name == 'B' or feed_name == 'C':
            update_string += " where %s = %s;" % (INVERTERS_TABLE_FEED_NAME_COLUMN_NAME, "%s")
            update_args.append(feed_name)
            prepared_act_on_database(EXECUTE, update_string, update_args)
        elif feed_name == 'ALL':
            update_string += ";"
            prepared_act_on_database(EXECUTE, update_string, update_args)
        else:
            # ???
            print("UNEXPECTED FEED NAME %s FROM MULTICAST IP %s" % (feed_name, ip_address))

    else:
        print("INVALID FEED NAME FROM MULTICAST IP %s" % ip_address)
'''


def update_universal_aif_table(region_name, power_factor, current_leading, enable):
    """ Updates all values in the table 'universal_aif' """
    update_string= "UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s;" % (DB_UNIVERSAL_AIF_TABLE,
                                                                  UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME, "%s",
                                                                  UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME, "%s",
                                                                  UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME, "%s",
                                                                  UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME, "%s")
    update_args = (region_name, power_factor, current_leading, enable)
    prepared_act_on_database(EXECUTE, update_string, update_args)


def insert_universal_aif_table(region_name, power_factor, current_leading, enable):
    """ Updates all values in the table 'universal_aif' """
    insert_string= "INSERT INTO %s (%s, %s, %s, %s) VALUES (%s, %s, %s, %s);" % (DB_UNIVERSAL_AIF_TABLE,
                                                                                 UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME,
                                                                                 UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME,
                                                                                 UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME,
                                                                                 UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME,
                                                                                 "%s", "%s", "%s", "%s")
    insert_args = (region_name, power_factor, current_leading, enable)
    prepared_act_on_database(EXECUTE, insert_string, insert_args)


def get_universal_aif_db_row():
    select_string = "SELECT * FROM %s" % (DB_UNIVERSAL_AIF_TABLE)
    db_row = prepared_act_on_database(FETCH_ONE, select_string, None)
    return db_row


def is_universal_aif_db_row_valid(u_aif_db_row):
    universal_aif_row_valid = False
    if u_aif_db_row:
        region_name = u_aif_db_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME]
        pf = u_aif_db_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME]
        pf_curr_leading = u_aif_db_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME]
        pf_enable = u_aif_db_row[UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME]

        this_db_tuple = (region_name, pf, pf_curr_leading, pf_enable)

        if this_db_tuple == (REGION_NAME_CA120,      1.0,  0, 0) \
        or this_db_tuple == (REGION_NAME_HI120,      0.95, 0, 1) \
        or this_db_tuple == (REGION_NAME_IEEE,       1.0,  0, 0) \
        or this_db_tuple == (REGION_NAME_NA120,      1.0,  0, 0) \
        or this_db_tuple == (REGION_NAME_MFG120,     1.0,  0, 0):
            universal_aif_row_valid = True

    return universal_aif_row_valid


def is_universal_aif_data_reconfigured():

    # Verifies that the sw_profile is correctly set.
    u_aif_reconfigured = False
    select_string = "SELECT COUNT(*) FROM %s" % (DB_UNIVERSAL_AIF_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_rows = int(select_result['COUNT(*)'])
    if number_of_rows == 1:
        # So far, so good. Is the data valid?
        select_string = "SELECT * FROM %s" % (DB_UNIVERSAL_AIF_TABLE)
        select_db_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        if is_universal_aif_db_row_valid(select_db_row):
            # Data is valid, nothing to do.
            pass
        else:
            # ICK.
            u_aif_reconfigured = True
            update_universal_aif_table(REGION_NAME_MFG120, 1.0, 0, 0)

    elif number_of_rows == 0:
        # Empty? Not good. Fill this table with base default: SW_PROFILE_MFG120 with pf = 1.00/0/0.
        u_aif_reconfigured = True
        insert_universal_aif_table(REGION_NAME_MFG120, 1.00, 0, 0)
    else:
        # Either 0 or more than 1. Also not good. Delete all rows and re-populate with a default.
        u_aif_reconfigured = True
        delete_string = "DELETE FROM %s;" % (DB_UNIVERSAL_AIF_TABLE)
        prepared_act_on_database(EXECUTE, delete_string, )
        insert_universal_aif_table(REGION_NAME_MFG120, 1.00, 0, 0)

    return u_aif_reconfigured


def fix_null_pcs_unit_id_count():
    # Any entries in the inverters table with a NULL pcs unit id?
    select_string = "SELECT COUNT(*) FROM %s WHERE %s IS NULL;" % (DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    null_pcs_unit_id_fix_count = int(select_results["COUNT(*)"])
    if null_pcs_unit_id_fix_count > 0:
        update_string = "UPDATE %s SET %s=0 WHERE %s IS NULL;" % (DB_INVERTER_NXO_TABLE,
                                                                  INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                  INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
        prepared_act_on_database(EXECUTE, update_string, ())
    return null_pcs_unit_id_fix_count


def update_db_pcs_unit_id_by_ip(this_ip, pcs_unit_id):
    update_string = "UPDATE %s set %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME, "%s",
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (pcs_unit_id, this_ip)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def inverters_db_update_all_battery_pcs_unit_id(pcs_unit_id):
    # User wants to update ALL battery inverters in the inverters table with the same pcs_unit_id.
    update_string = "UPDATE %s set %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME, "%s",
                                                          INVERTERS_TABLE_BATTERY_COLUMN_NAME, 1)
    update_args = (pcs_unit_id)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def get_inverters_db_pcs_unit_id(this_ip):
    pcs_unit_id = 0
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (this_ip, )
    pcs_unit_id_row = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if pcs_unit_id_row:
        pcs_unit_id = pcs_unit_id_row[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME]
    return pcs_unit_id


def get_catch_up_reports_data_by_ip(ip_address):

    tlv_id_list = []
    tlv_values_list = []
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GET_CATCH_UP_REPORTS_U16_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:
        # SEND THE PACKET:
        try:
            fancy_sock.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:
            # Now wait for a response:
            try:
                fancy_sock.settimeout(3.0)
                private_port_packet = fancy_sock.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding. A "NO RESPONE" log entry will
                # eventually be written to the results file. So nothing to do here.
                pass

            else:
                # Packet received. Get the payload portion of the packet, and extract all TLVs that have been returned.
                (ip_address, ip_address_port, SG424_data) = get_udp_components(private_port_packet)
                (tlv_id_list, tlv_values_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)

        # Close the socket:
        try:
            fancy_sock.close()
        except Exception: # as error:
            print("close socket failed ...")
        else:
            pass
    return (tlv_id_list, tlv_values_list)

def get_aif_immed_data_by_ip(ip_address):
    tlv_id_list = []
    tlv_values_list = []
    number_of_user_tlv = 1
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GET_AIF_IMMED_GROUP_DATA_U16_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:
        # SEND THE PACKET:
        try:
            fancy_sock.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:
            # Now wait for a response:
            try:
                fancy_sock.settimeout(3.0)
                private_port_packet = fancy_sock.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding. A "NO RESPONE" log entry will
                # eventually be written to the results file. So nothing to do here.
                pass

            else:
                # Packet received. Get the payload portion of the packet, and extract all TLVs that have been returned.
                (ip_address, ip_address_port, SG424_data) = get_udp_components(private_port_packet)
                (tlv_id_list, tlv_values_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)

        # Close the socket:
        try:
            fancy_sock.close()
        except Exception: # as error:
            print("close socket failed ...")
        else:
            pass
    return (tlv_id_list, tlv_values_list)


def get_aif_settings_data_by_ip(ip_address):
    tlv_id_list = []
    tlv_values_list = []
    number_of_user_tlv = 1
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GET_AIF_SETTINGS_GROUP_DATA_U16_TLV_TYPE, 1234)

    # OPEN THE SOCKET:
    try:
        fancy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("socket.socket failed ...")
    else:
        # SEND THE PACKET:
        try:
            fancy_sock.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            print("sendto failed ...")
        else:
            # Now wait for a response:
            try:
                fancy_sock.settimeout(3.0)
                private_port_packet = fancy_sock.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet. Simply means the inverter
                # at that IP is not responding. A "NO RESPOSE" log entry will
                # eventually be written to the results file. So nothing to do here.
                pass

            else:
                # Packet received. Get the payload portion of the packet, and extract all TLVs that have been returned.
                (ip_address, ip_address_port, SG424_data) = get_udp_components(private_port_packet)
                (tlv_id_list, tlv_values_list) = extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)

        # Close the socket:
        try:
            fancy_sock.close()
        except Exception: # as error:
            print("close socket failed ...")
        else:
            pass
        return (tlv_id_list, tlv_values_list)

# -------------------------------------------------------------------------------------------------
#
# Some HTTP utilities:


def send_http_upgrade_reset(inverter_ip_address):
    packet_data = "/upgrade.htm?upgradereset"
    http_serv = httplib.HTTPConnection(inverter_ip_address, 80)
    try:
        http_serv.connect()
    except Exception:
        print("IP %s NOT REACHABLE" % (inverter_ip_address))
    else:
        http_serv.request("GET", packet_data)
        print("IP %s -> SENT HTTP UPGRADE RESET" % (inverter_ip_address))


def send_http_enable_standby(inverter_ip_address):
    packet_data = "/phase.htm?st1"
    http_serv = httplib.HTTPConnection(inverter_ip_address, 80)
    try:
        http_serv.connect()
    except Exception:
        print("IP %s NOT REACHABLE" % (inverter_ip_address))
    else:
        http_serv.request("GET", packet_data)
        print("IP %s -> SENT HTTP ENABLE STANDBY" % (inverter_ip_address))


def send_http_enable_low_power(inverter_ip_address):
    packet_data = "/phase.htm?lp1"
    http_serv = httplib.HTTPConnection(inverter_ip_address, 80)
    try:
        http_serv.connect()
    except Exception:
        print("IP %s NOT REACHABLE" % (inverter_ip_address))
    else:
        http_serv.request("GET", packet_data)
        print("IP %s -> SENT HTTP ENABLE LOWE POWER" % (inverter_ip_address))


# -------------------------------------------------------------------------------------------------
#
# Some NBNS utilities:


def construct_nbns_question(question):

    # Construct the NBNS-formatted byte array, adding the "question" for the NBNS query.

    # TODO: EXPLAIN WHERE THIS DATA CAME FROM. IT'S CRYPTIC. THEN AGAIN, NBNS ITSELF IS CRYPTIC.
    packed_data = struct.pack('!HHHHHHHH', 0x0e97, 0x0000, 0x0001, 0x0000, 0x0000, 0x0000, 0x2043, 0x4b41)
    packed_data += struct.pack('!HHHHHHHH', 0x4141, 0x4141, 0x4141, 0x4141, 0x4141, 0x4141, 0x4141, 0x4141)
    packed_data += struct.pack('!HHHHHHHH', 0x4141, 0x4141, 0x4141, 0x4141, 0x4141, 0x4141, 0x4100, question)
    packed_data += struct.pack('!H', 0x0001)
    return packed_data


def send_and_get_nbns_question(ip_address, nbns_question):

    nbns_response = ""
    if nbns_question == NBNS_NAMESERVICE_QUESTION:
        question = 0x0020
    elif nbns_question == NBNS_NODESTATUS_QUESTION:
        question = 0x0021
    elif nbns_question == NBNS_XETINFO_QUESTION:
        question = 0xE01F
    else:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR)
        return nbns_response

    #
    #  The following questions directed to an SG424/MGI250 return data of the form:
    #
    # NBNS_XETINFO_QUESTION/0xE01F:     MGI250 DEVELOP R302Test5_EP11 ATIRA5544 0 00 3
    # NBNS_NODESTATUS_QUESTION/0x0021:  XET091133000001
    # NBNS_NAMESERVICE_QUESTION/0x0020: 10.252.2.14

    # Create the NBNS-formatted packed data with the question:
    nbns_packed_data = construct_nbns_question(question)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(type=socket.SOCK_DGRAM)
    except Exception as error:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR)

    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(nbns_packed_data, (ip_address, 137))
        except Exception as error:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR)

        else:
            # Now wait for a response:
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(256)
            except Exception:
                # No response. Nothing to do.
                pass

            else:
                # Get here when a packet is received.

                # Get the pointer to the packet payload and the payload length:
                SG424_data = private_port_packet[0]

                xet_info = SG424_data[NBNS_XET_INFO_OPCODE_INFO_LENGTH_RANK:
                                      NBNS_XET_INFO_OPCODE_INFO_LENGTH_RANK + 2]
                (some_length,) = struct.unpack('!H', xet_info)

                for char_char in SG424_data[NBNS_XET_INFO_OPCODE_INFO_DATA_START_RANK:
                        NBNS_XET_INFO_OPCODE_INFO_DATA_START_RANK + some_length]:
                    nbns_response += chr(int(ord(char_char)))

        # Close the socket:
        try:
            fancy_socks.close()
        except Exception as error:
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_RECEIVE_NBNS_QUESTION_ERROR)
        else:
            pass

    return nbns_response


def get_inverter_boot_version_via_nbns(ip_address):

    # Send this inverter an NBNS XET INFO question, examine the response.
    # Determining if this is a TLV-capable boot loader/updater is on the
    # caller, where the boot loader version is to be compared to the
    # FIRST_TLV_CAPABLE_BOOTLOADER_VERSION value.
    #
    # There are no retries in the attempt to query an inverter via NBNS.

    boot_version = 0
    nbns_question = NBNS_XETINFO_QUESTION
    xet_info_string = send_and_get_nbns_question(ip_address, nbns_question)
    if xet_info_string:
        # Check the string to see if it contains "SG424":
        if "SG424 MGIBL" in xet_info_string or "MGI-250 MGIBL" in xet_info_string:
            # It is an SG424 running BOOT LOADER. The XET INFO string is of the form:
            # "SG424 MGIBL R28", where the boot loader version is in rank[2] representing
            # a hexadecimal value.
            xet_info_components = xet_info_string.split(' ')
            boot_loader_as_string = xet_info_components[2]
            boot_loader_as_string = boot_loader_as_string.strip("R")
            boot_version = int(boot_loader_as_string)

        elif "SG424 UPDATER" in xet_info_string:
            # It is an SG424 running BOOT updater. The XET INFO string is of the form:
            # "SG424 UPDATER R28", where the boot updater's version is in rank[2].
            xet_info_components = xet_info_string.split(" ")
            boot_loader_as_string = xet_info_components[2]
            boot_loader_as_string = boot_loader_as_string.strip("R")
            boot_version = int(boot_loader_as_string)

        elif "SG424 DEVELOP" in xet_info_string or "SG424 RELEASE" in xet_info_string or "MGI-250 DEVELOP" in xet_info_string:
            # It is an SG424 running application code. The XET INFO string is of the form:
            # "SG424 DEVELOP R313svn228 ATIRA5488 1 1C 12". The boot loader's version is in
            # rank[5] representing a HEX digit (in this example, hex 1C = 32).
            xet_info_components = xet_info_string.split(' ')
            boot_version = int(xet_info_components[5], 16)
        else:
            # Does not contain the magic word. Migt be an MGI220. But is not an SG424.
            pass

    else:
        # No response.
        pass

    return boot_version


def get_inverter_data_from_xet_info_string(xet_info_string):
    running_mode = RUNMODE_UNKNOWN
    boot_version = 0
    firmware_version = ""

    if xet_info_string:
        # Check the string to see if it contains "SG424":
        xet_info_components = xet_info_string.split(" ")
        if "SG424 MGIBL" in xet_info_string or "MGI-250 MGIBL" in xet_info_string:
            # An SG424 in BOOT LOADER. XET INFO is of the form "SG424 MGIBL R28",
            # with the boot loader version in rank[2] as a decimal value.
            boot_loader_as_string = xet_info_components[2]
            boot_loader_as_string = boot_loader_as_string.strip("R")
            boot_version = int(boot_loader_as_string)
            running_mode = RUNMODE_SG424_BOOT_LOADER

        elif "SG424 UPDATER" in xet_info_string:
            # An SG424 in BOOT UPDATER. XET INFO is of the form "SG424 UPDATER R28",
            # with the boot updater version in rank[2] as a decimal value.
            boot_loader_as_string = xet_info_components[2]
            boot_loader_as_string = boot_loader_as_string.strip("R")
            boot_version = int(boot_loader_as_string)
            running_mode = RUNMODE_SG424_BOOT_UPDATER

        elif "SG424 DEVELOP" in xet_info_string or "SG424 RELEASE" in xet_info_string or "MGI-250 DEVELOP" in xet_info_string:
            # An SG424 in application code. XET INFO is of the form: "SG424 DEVELOP R313svn228 ATIRA5488 1 1C 12".
            # Firmware version is in rank [2], boot loader is in rank[5] as a HEX value. In the case of firmware,
            # the preceeding "R" is removed.
            running_mode = RUNMODE_SG424_APPLICATION
            boot_version = int(xet_info_components[5], 16)
            firmware_version = xet_info_components[2]
            firmware_version = firmware_version.strip("R")
        else:
            # Does not contain the magic word. Might be an MGI220. Hey, could be an A1000! But is not an SG424.
            pass
    else:
        # Pass an empty string? Probably because the inverter did not respond/not in communication.
        pass

    return (running_mode, boot_version, firmware_version)


def is_valid_nbns_node_status_string(nbns_node_status_string):

    # The NBNS node status response is considered valid if:
    #
    # - it is exactly 15 characters long
    # - 1st 3 characters of the node name is XET
    # - the following 12 characters represent 12 decimal digits
    #
    # Get the 15 characters of the string from rank 1 through 16.
    valid_node_status = False
    this_node_status_string = ""
    for char_char in nbns_node_status_string[1:16]:
        this_node_status_string += chr(int(ord(char_char)))

    # Validate the contents of the response.
    if len(this_node_status_string) == 15:
        # Check the 1st 3 characters:
        if "XET" in this_node_status_string:
            # Are the remaining 12 characters a set of decimal digits that look like a serial number?
            this_node_status_string = this_node_status_string.replace("XET", "")
            if len(this_node_status_string) == 12 and this_node_status_string.isdigit() == True:
                # A valid NBNS NODE STATUS string. No attempt is made to determine if the serial number
                # in the string matches what is in the database against this inverter.
                valid_node_status = True

    return valid_node_status


def is_get_nodestatus_indicating_at_least_rev6(ip_address):
    # Is an SG424 inverter at least at rev6 or beyond? If the inverter is in application mode,
    # then an NBNS XETINFO request will return the MGITYPE which contains the indicator. However,
    # if the inverter is stuck in boot loader with no application code available, the MGITYPE in
    # the NBNS XETINFO reply by the boot loader is not available. So it cannot be known if it is
    # a pre-rev6 board or not.
    #
    # So, what to do .... what to do ...
    #
    # Until better information is available:
    #
    # -> as a "best guess" determination, examine digits [2,3] of the serial number, which provides
    #    the date year in which the board was manufactured:
    #
    #        - all boards made in 2017 and beyond will be considered rev6 or beyond boards
    #        - all boards made up to an including 2016 will be considered pre-rev6
    #
    presumed_at_least_rev6 = False
    serial_number_as_string = ""
    node_status = send_and_get_nbns_question(ip_address, NBNS_NODESTATUS_QUESTION)
    if node_status:
        if is_valid_nbns_node_status_string(node_status):
            # -------------------------------------
            # Gets rid of 1st seemingly non-printable [0] character.
            for char_char in node_status[1:16]:
                serial_number_as_string += chr(int(ord(char_char)))
            serial_number_as_string = serial_number_as_string.replace("XET", "")
            # -------------------------------------
            # print("EXAMINE NODESTATUS SN STRING %s" % (serial_number_as_string)
            first_year_digit = int(serial_number_as_string[2])
            second_year_digit = int(serial_number_as_string[3])
            if (first_year_digit == 1) and (second_year_digit > 6):
                # Board presumably manufactured in 2017 or beyond. Use post-rev6 upgrade file.
                presumed_at_least_rev6 = True
            else:
                #
                pass
        else:
            # Not responding. Assume pre-rev6 file.
            pass
    else:
        # Not responding. Assume pre-rev6 file.
        pass

    return (presumed_at_least_rev6, serial_number_as_string)


# That's it for NBNS utilities.
# -------------------------------------------------------------------------------------------------


def is_inverter_board_rev_6_and_beyond(xet_info_string):
    board_type_is_rev6_and_beyond = False
    # In application mode, the  XET INFO string looks like: SG424 DEV R313_1501 ATIRA5486 1 28 10
    #
    # where the 5th element is MGI type, treated as a set of bits: - if the 4th bit is set: board is a rev 6 or beyond
    #                                                              - if the 4th bit is NOT set, it is pre-rev 6
    # In boot mode, the XET INFO string (when available) only has 3 components.
    xet_info_components = xet_info_string.split(' ')

    if len(xet_info_components) > 3:
        # inverter_type = xet_info_components[XET_INFO_INVERTER_TYPE_RANK]                        <- donut care
        # build_type = xet_info_components[XET_INFO_APP_BUILD_TYPE_RANK]                          <- donut care
        # version = xet_info_components[XET_INFO_APP_VERSION_RANK]                                <- donut care
        # product_part_number = xet_info_components[XET_INFO_APP_ATIRA_PART_NO_RANK]              <- donut care
        mgi_type = xet_info_components[XET_INFO_APP_MGI_TYPE_RANK]
        # boot_version = str(int(xet_info_components[XET_INFO_APP_BOOT_LOADER_AS_HEX_RANK], 16))  <- donut care
        # software_profile = xet_info_components[XET_INFO_APP_SOFTWARE_PROFILE_RANK]              <- donut care

        # Examine the mgi type, is the 4th bit set?
        MGI_TYPE_REV6_BIT_RANK = 3
        if int(mgi_type, 16) & (2 ** MGI_TYPE_REV6_BIT_RANK):
            # The bit is set, so it's a 1.
            board_type_is_rev6_and_beyond = True
        else:
            # The bit is not set, so it's a 0.
            board_type_is_rev6_and_beyond = False
    return board_type_is_rev6_and_beyond


def get_inverter_preliminary_upgrade_file_name(preliminary_upgrade_file_type):
    file_name = ""
    select_string = "SELECT %s FROM %s;" % (preliminary_upgrade_file_type, DB_INVERTER_UPGRADE_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    file_name = select_result[preliminary_upgrade_file_type]
    return file_name


def get_boot_version_as_short_from_xet_string(xet_info_string):
    #
    # XET INFO string is in 1 of 2 formats:
    #
    # - if application:           SG424 DEV R313_1511_JEP04 ATIRA5488 1 28 12
    # - if bootloader:            SG424 MGIBL R40
    # - if boot updater:          SG424 UPDATER R40
    #
    # So split the string based on "space", and examine the[1] array element to determine the format.
    # When the format is evaluated, the boot loader version can be extracted.
    boot_version_short = 0
    if xet_info_string:
        xet_info_components = xet_info_string.split(' ')
        if (xet_info_components[XET_INFO_INVERTER_TYPE_RANK] == "SG424") \
        or (xet_info_components[XET_INFO_INVERTER_TYPE_RANK] == "MGI250"):
            # An SG424. Is it running APP or BOOT?
            if (xet_info_components[XET_INFO_APP_BUILD_TYPE_RANK] == "MGIBL") \
            or (xet_info_components[XET_INFO_APP_BUILD_TYPE_RANK] == "UPDATER"):
                # Get the string, strip the R. The boot version is already in decimal/base 10.
                boot_version_string = (xet_info_components[XET_INFO_BOOT_VERSION_RANK]).strip("R")
                boot_version_short = int(boot_version_string)
            else:
                # This inverter is running application code. Access the boot loader rank,
                # it's a string representing a hex value, so convert to base 16.
                boot_version_short = int(xet_info_components[XET_INFO_APP_BOOT_LOADER_AS_HEX_RANK], 16)
    return boot_version_short


def hgn_toss_outliers(array_of_numbers):

    difference = 0
    sum_sq_difference = 0
    interim_list = []
    final_list = []
    # Validate that the array.

    # this_string = "CHECK INPUT ARRAY OF LENGTH %s:" % (len(array_of_numbers))
    # for number in array_of_numbers:
    #     this_string += " %s" % (number)
    # print(this_string)

    # HGN: home-grown numpy?

    '''
    From some web page:
    elements = numpy.array(arr)
    mean = np.mean(elements, axis=0)
    sd = np.std(elements, axis=0)
    final_list = [x for x in arr if (x > mean - 2 * sd)]
    final_list = [x for x in final_list if (x < mean + 2 * sd)]
    print(final_list)
    '''

    if len(array_of_numbers) > 0:

        this_average = sum(array_of_numbers) / len(array_of_numbers)

        for some_number in array_of_numbers:
            difference = some_number - this_average
            sum_sq_difference += difference**2

        variance = sum_sq_difference / len(array_of_numbers)
        standard_deviation = sqrt(variance)

        # Validate and toss as necessary:
        for some_number in array_of_numbers:
            if some_number > (this_average - (2*standard_deviation)):
                interim_list.append(some_number)

        for some_number in interim_list:
            if some_number < (this_average + (2*standard_deviation)):
                final_list.append(some_number)

    return final_list


def is_values_within_range(this_value, that_value, some_percentage):
    # Determine if 2 values are within a percentage of each other.
    values_are_in_range = False
    this_delta = this_value * some_percentage
    hi = this_value + this_delta
    lo = this_value - this_delta
    if (that_value >= lo) and (that_value <= hi):
        values_are_in_range = True
    return values_are_in_range


def is_string_id_looka_lika_string_id(some_string_id):
    # Check that a given "string ID" is of the form: XX:XX:XX where X is any hex character.
    # No need to convert to upper, since upper and lower case hex are included in VALID_HEX.
    string_id_looka_ok = True
    if len(some_string_id) == len(DEFAULT_STRING_ID):
        # Length OK, next: hard-core checking of each character. It's only 8 characters in all.
        if    some_string_id[0] in VALID_HEX \
          and some_string_id[1] in VALID_HEX \
          and some_string_id[2] == ":" \
          and some_string_id[3] in VALID_HEX \
          and some_string_id[4] in VALID_HEX \
          and some_string_id[5] == ":" \
          and some_string_id[6] in VALID_HEX \
          and some_string_id[7] in VALID_HEX:
            # It's a seemingly valid string ID of the form XX:XX:XX
            pass
        else:
            string_id_looka_ok = False
    else:
        string_id_looka_ok = False

    return string_id_looka_ok


def is_ip_as_string_looka_lika_ip_address(some_ip_address):
    # Given an IP as a plain string, does it look like an IP address?
    looka_lika_ip = True
    split_i_pea_soup = some_ip_address.split('.')
    if len(split_i_pea_soup) == 4:
        for x in split_i_pea_soup:
            if not x.isdigit():
                looka_lika_ip = False
                break
            else:
                i = int(x)
                if i < 0 or i > 255:
                    looka_lika_ip = False
                    break
    else:
        looka_lika_ip = False

    return looka_lika_ip


def is_ip_looka_lika_multicast(some_ip_address):
    # An IP looks like a multicast if: - the 1st 3 components are 239 and 192 and 0
    #                                  - 4th component is ... pretty much anything.
    looka_lika_multicast = False
    split_i_pea_soup = some_ip_address.split('.')
    if len(split_i_pea_soup) == 4:
        if split_i_pea_soup[0] == '239' and split_i_pea_soup[1] == '192' and split_i_pea_soup[2] == '0' :
            # Don't bother checking the 4th component - this IP already looka lika multicast anyway.
            looka_lika_multicast = True
    return looka_lika_multicast


def construct_generic_multicast_ip_gen2(inverter_type):
    # Return a multicast IP that targets ALL, SOLAR-only, or BATTERY-only inverters.
    # Create a string of "8 bits as string", then convert that string into a decimal digit.
    forth_octet_as_string = "00"
    if inverter_type == INVERTER_MCAST_SG424_TYPE_ALL:
        forth_octet_as_string = "0" +  MCAST_TYPE_SG424_ALL_BITS + MCAST_PCC_ALL_BITS + MCAST_FEED_ALL_BITS
    elif inverter_type == INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY:
        forth_octet_as_string = "0" +  MCAST_TYPE_SG424_SOLAR_ONLY_BITS + MCAST_PCC_ALL_BITS + MCAST_FEED_ALL_BITS
    elif inverter_type == INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY:
        forth_octet_as_string = "0" +  MCAST_TYPE_SG424_BATTERY_ONLY_BITS + MCAST_PCC_ALL_BITS + MCAST_FEED_ALL_BITS
    elif inverter_type == INVERTER_MCAST_SG424_TYPE_NON_GATEWAY_ONLY_ONLY:
        forth_octet_as_string = "1" + "00" + MCAST_PCC_ALL_BITS + MCAST_FEED_ALL_BITS
    forth_octet_as_int = int(forth_octet_as_string, 2)
    multicast_ip = "%s.%s" % (SG424_MULTICAST_BASE_GEN_2, forth_octet_as_int)
    return multicast_ip

def construct_multicast_ip_gen2(inverter_type, pcc_as_string, feed_as_string):

    # Construct a ("gen 2") multicast IP the form: 239.192.0.XX
    #
    # where XX is the integer value of the 8 bits 0XXYYZZZ of the 4th octat as follows:
    #
    #   0 = bit b7: unused, always 0/zero
    #  XX = bits b6/b5 inverter type: 00 = SOLAR only, 01 = BATTERY only, 10 = ALL
    #  YY = bits b4/b3 PCC: 00 = ALL, 01 = PCC 1 only, 10 = PCC 2 only
    # ZZZ = bits b2/b1/b0 FEED: same as always, has not changed.

    multicast_ip = "00.00.00.00"

    if (inverter_type in valid_inverter_type) and (pcc_as_string in valid_pcc_type) and (feed_as_string in valid_feed):
        # All specs are good. Get the components for the multicast IP from the mini lookup table.
        # When the "4th octet as string of 1 and 0" is constructed, convert it to an integer as binary.
        inverter_type_bits = mcast_by_inverter_type[inverter_type]
        pcc_bits = mcast_by_pcc_type[pcc_as_string]
        feed_bits = mcast_by_feed[feed_as_string]
        forth_octet_as_string = "0" +  inverter_type_bits + pcc_bits + feed_bits
        forth_octet_as_int = int(forth_octet_as_string, 2)
        multicast_ip = "%s.%s" % (SG424_MULTICAST_BASE_GEN_2, forth_octet_as_int)
    return multicast_ip


def is_ip_looka_lika_unicast(some_ip_address):
    # Home-grown method, because I could not find a decent method that determined if a given IP looks "reasonable".
    # Pretty much anything goes, except that the 1st 2 components cannot be the multicast "239.192". Presumably,
    # checking that the 1st 2 components are "10" and "252" would be a good check for unicast, but there would have
    # to be a placeholder for these 2 values, and currently, they are not available.
    looka_lika_unicast = False
    split_i_pea_soup = some_ip_address.split('.')
    if len(split_i_pea_soup) == 4:
        if split_i_pea_soup[0].isdigit() \
        and split_i_pea_soup[1].isdigit() \
        and split_i_pea_soup[2].isdigit() \
        and split_i_pea_soup[3].isdigit() :
            if split_i_pea_soup[0] != "239" and split_i_pea_soup[1] != "192":
                # So far so good, certainly not the gateway multicast. Do some range checking on each
                # IP component: for now, just make sure they are all less than 255.
                if int(split_i_pea_soup[0]) <= 255 \
                and int(split_i_pea_soup[1]) <= 255 \
                and int(split_i_pea_soup[2]) <= 255 \
                and int(split_i_pea_soup[3]) <= 255:
                    looka_lika_unicast = True

    return looka_lika_unicast


def is_mac_looka_lika_mac(some_mac_address):
    # Home-grown method, because I could not find a decent method that determined if a given mac looks "reasonable".
    # 01:02:03:04:05:06
    looka_lika_mac = False
    if len(some_mac_address) == 17:
        split_mac = some_mac_address.split(':')
        if len(split_mac) == 6:
            for this_mac_component in split_mac:
                if this_mac_component[0] in VALID_HEX and this_mac_component[1] in VALID_HEX:
                    # Coming here 6 times is a good thing ...
                    looka_lika_mac = True
                else:
                    # But coming here even once is bad.
                    looka_lika_mac = False
                    break
    return looka_lika_mac


def is_string_looka_lika_hex(some_hex_string): #mac_address):
    # Home-grown method, because I could not find a decent method that determined if a given
    # string looks like pure hex.
    looka_lika_hex = False
    for this_hex in some_hex_string:
        if this_hex in VALID_HEX:
            # Coming here 6 times is a good thing ...
            looka_lika_hex = True
        else:
            # But coming here even once is bad.
            looka_lika_hex = False
            break
    return looka_lika_hex


def get_ip_list_from_comma_separated_list(comma_separated_list):
    # Will extract all the unicast IP from a comma-separated list. Multicast IP are dropped.
    # If the 1st item looks like a string ID, then all items in the list are treated as such
    # (no validation is made to see if they are all in the database). If the list is empty,
    # get all IP from the database.
    ip_list = []

    if not comma_separated_list:
        # Get all IP from the database.
        select_string = "SELECT %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
        total_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_ip in total_ip_list:
            ip_list.append(each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])

    else:

        item_list = comma_separated_list.split(',')
        # Based on the 1st element of the comma-separated list, determine the list type.
        if is_ip_looka_lika_unicast(item_list[0]):
            # 1st item is a unicast-looking IP. The rest of the list will be treated as unicast IP.
            print("TREAT COMMA-SEPARATED LIST (LENGTH %s) AS IP" % (len(item_list)))
            for this_ip in item_list:
                if is_ip_looka_lika_unicast(this_ip):
                    ip_list.append(this_ip)
                else:
                    print("IP %s has a funny odor" % (this_ip))
        elif is_string_id_looka_lika_string_id(item_list[0]):
            # 1st item looks like a string ID. The rest of the list will be treated as string ID.
                print("TREAT COMMA-SEPARATED LIST (LEN %s) AS STRING ID" % (len(item_list)))
                for this_string_id in item_list:
                    if is_string_id_looka_lika_string_id(this_string_id):
                        select_string = "SELECT %s FROM %s WHERE %s=%s ORDER BY %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                                        DB_INVERTER_NXO_TABLE,
                                                                                        INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                                        INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
                        select_args = (this_string_id,)
                        string_id_db_entry = prepared_act_on_database(FETCH_ALL, select_string, (select_args))
                        if string_id_db_entry:
                            for each_inverter in string_id_db_entry:
                                ip_list.append(each_inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
                        else:
                            print("NO IP IN DATABASE BASED ON STRING ID %s" % (this_string_id))

                    else:
                        print("String ID %s has a funny odor" % (this_string_id))

    return ip_list


def gimme_ip_list_from_comma_separated_thingies(comma_separated_thingies):
    ip_list = []

    # Now check the comma-separated list of thingies, which could be: "ALL", "SOLAR", "BATTERY", IP, string ID, mac, serial number, group ID.
    # But not a mix and match, maybe later. Extract and do basic sanity.
    thingies_list = comma_separated_thingies.split(',')
    first_argument = thingies_list[0]
    first_argument = first_argument.upper()

    # Determine what the 1st item in the argument list "looks like".
    # Once determined, treat all arguments in the same way.

    if first_argument == "ALL":
        # Return a 1-item list: the "ALL" multicast.
        multicast_ip = construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL)
        ip_list.append(multicast_ip)

    elif first_argument == "SOLAR":
        # Return a 1-item list: the "SOLAR-ONLY" multicast.
        multicast_ip = construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY)
        ip_list.append(multicast_ip)

    elif first_argument == "BATTERY":
        # Return a 1-item list: the "BATTERY-ONLY" multicast.
        multicast_ip = construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY)
        ip_list.append(multicast_ip)

    elif is_ip_looka_lika_unicast(first_argument):
        # All arguments to be interpreted/treated as IP.
        print("PROCESS COMMA SEPARATED IP LIST LENGTH = %s" % (len(thingies_list)))
        for this_ip in thingies_list:
            if is_ip_looka_lika_unicast(this_ip):
                # Check that this IP is in the inverters table.
                if is_this_ip_in_database(this_ip):
                    # Add it to the list.
                    if this_ip not in ip_list:
                        ip_list.append(this_ip)
                        print("IP %s EXTRACTED, ADDED TO LIST" % (this_ip))
                    else:
                        print("IP %s ALREADY IN LIST" % (this_ip))
                else:
                    print("IP %s NOT IN DATABASE, NOT ADDED TO LIST" % (this_ip))
            else:
                print("IP %s -> YUCK, NOT ADDED TO LIST" % (this_ip))

    # CHECK SERIAL NUMBER BEFORE GROUP ID ... they are both just plain digits after all.
    # But serial number is more specific than a group ID.
    elif len(first_argument) == 12 and first_argument.isdigit():
        # All arguments to be interpreted/treated as serial numbers.
        print("PROCESS COMMA-SEPARATED SERIAL NUMBER ID LIST LENGTH = %s" % (len(thingies_list)))
        for each_serial_number in thingies_list:
            if len(each_serial_number) == 12 and each_serial_number.isdigit():
                # Get the corresponding IP address.
                this_ip = get_ip_from_serial_number(each_serial_number)
                if this_ip:
                    if this_ip not in ip_list:
                        ip_list.append(this_ip)
                        print("SN %s -> IP %s EXTRACTED, ADDED TO LIST" % (each_serial_number, this_ip))
                    else:
                        print("SN %s -> IP %s ALREADY IN LIST" % (each_serial_number, this_ip))
                else:
                    # No IP in database based on this serial number.
                    print("SN %s NOT IN DATABASE, NO IP ADDED TO LIST" % (each_serial_number))
            else:
                print("SN %s -> YUCK, NO IP ADDED TO LIST" % (each_serial_number))

    # Now check if the 1st parameter is a plain digit, treated as a group ID.
    elif first_argument.isdigit():
        # All arguments to be interpreted/treated as group ID.
        print("PROCESS COMMA-SEPARATED GROUP ID LIST LENGTH = %s" % (len(thingies_list)))
        for each_group_id in thingies_list:
            itty_bitty_ip_list = get_ip_list_from_group_id(each_group_id)
            if len(itty_bitty_ip_list) != 0:
                this_string = "GROUP ID %s -> IP =" % (each_group_id)
                for each_ip in itty_bitty_ip_list:
                    if each_ip not in ip_list:
                        ip_list.append(each_ip)
                        this_string += " %s" % (each_ip)
                print(this_string)
            else:
                print("GROUP ID %s HAS NO IP, NO IP ADDED TO LIST" % (each_group_id))

    elif is_string_id_looka_lika_string_id(first_argument):
        # All arguments to be interpreted/treated as string ID.
        print("PROCESS COMMA SEPARATED STRING ID LIST LENGTH = %s" % (len(thingies_list)))
        for this_string_id in thingies_list:
            if is_string_id_looka_lika_string_id(this_string_id):
                select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                    DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
                select_args = (this_string_id,)
                string_id_results = prepared_act_on_database(FETCH_ALL, select_string, (select_args))
                if string_id_results:
                    for each_string_id_result in string_id_results:
                        this_ip = each_string_id_result[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                        if this_ip not in ip_list:
                            ip_list.append(this_ip)
                            print("STRING ID %s -> IP %s , ADDED TO LIST" % (this_string_id, this_ip))
                        else:
                            print("STRING ID %s: IP %s ALREADY IN LIST" % (this_string_id, this_ip))
                else:
                    print("STRING ID %s NOT IN DATABASE, NO IP ADDED TO LIST" % (this_string_id))
            else:
                print("STRING ID %s HAS BAD SMELL, NO IP ADDED TO LIST" % (this_string_id))

    elif is_mac_looka_lika_mac(first_argument):
        # All arguments to be interpreted/treated as mac address.
        print("PROCESS COMMA-SEPARATED MAC ADDRESS LIST LENGTH = %s" % (len(thingies_list)))
        for each_mac_adress in thingies_list:
            if is_mac_looka_lika_mac(each_mac_adress):
                # Get the corresponding IP address.
                this_ip = get_ip_from_mac_address(each_mac_adress)
                if this_ip:
                    if this_ip not in ip_list:
                        ip_list.append(this_ip)
                        print("MAC %s -> IP %s  ADDED TO LIST" % (each_mac_adress, this_ip))
                    else:
                        print("MAC %s -> IP %s ALREADY IN LIST" % (each_mac_adress, this_ip))
                else:
                    # No IP in database based on this mac address.
                    print("MAC %s ... NO IP IN DATABASE, NO IP ADDED TO LIST" % (each_mac_adress))
            else:
                print("MAC %s -> HAS A BAD SMELL, NO IP ADDED TO LIST" % (each_mac_adress))

    else:
        print("1ST ARG ITEM ->%s<- NOT ALL/SOLAR/BATTERY/IP/SN/GROUP ID/STRING ID/MAC - NO IP ADDED TO LIST" % (first_argument))

    return ip_list



def gimme_ip_list_from_inverter_type(inverter_type):
    ip_list = []
    if inverter_type == "ALL":
        select_string = "SELECT %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
        select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_ip in select_results:
                this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                ip_list.append(this_ip)

    elif inverter_type == "SOLAR":
        select_string = "SELECT %s FROM %s WHERE %s=0;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_BATTERY_COLUMN_NAME)
        select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_ip in select_results:
                this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                ip_list.append(this_ip)

    elif inverter_type == "BATTERY":
        select_string = "SELECT %s FROM %s WHERE %s=1;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_BATTERY_COLUMN_NAME)
        select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_ip in select_results:
                this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                ip_list.append(this_ip)

    return ip_list


def get_feed_name_from_multicast_ip(some_multicast_ip):

    # Given an Apparent multicast address, examine the "last 3 bits of the 4th octet"
    # to determine which feed was specified. Feed is specified in the last 3 bits of
    # the 4th octet.
    #
    # Assumption: multicast already validated, so only the 4th octet need be examined.
    #
    # - get the last octet
    # - bit-wise AND that octet with the feed mask
    # - extract the last 3 "characters" as binary/base 2
    # - finally, check the 3 characters that specify the feed
    literal_feed_name = None
    split_i_pea_soup = some_multicast_ip.split('.')
    forth_octet = int(split_i_pea_soup[3])
    forth_octet_masked_off = forth_octet & MCAST_FEED_MASK
    forth_octet_literal = bin(forth_octet_masked_off)[2:].zfill(3)

    # Note: no application has yet multicasted to feeds AB_only, AC_only or BC_ONLY.
    #       So only expect literals for ALL (000), A (001), B (010) and C (100).
    if   forth_octet_literal == MCAST_FEED_ALL_BITS:
        literal_feed_name = FEED_ALL_LITERAL          # "000"
    elif forth_octet_literal == MCAST_FEED_A_BITS:
        literal_feed_name = FEED_A_LITERAL            # "001"
    elif forth_octet_literal == MCAST_FEED_B_BITS:
        literal_feed_name = FEED_B_LITERAL            # "010"
    elif forth_octet_literal == MCAST_FEED_C_BITS:
        literal_feed_name = FEED_C_LITERAL            # "100"
    return literal_feed_name


def set_inverter_upgrade_command(inverter_upgrade_command):
    update_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_UPGRADE_TABLE, INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s")
    upgrade_args = (inverter_upgrade_command,)
    prepared_act_on_database(EXECUTE, update_string, upgrade_args)


def is_forced_inverter_upgrade_enabled():
    fiu_enabled = False
    select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_ENABLE_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    enable_value = select_result[INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_ENABLE_COLUMN_NAME]
    if enable_value == 1:
        fiu_enabled = True
    return fiu_enabled


def is_inverter_upgrade_in_progress():
    upgrade_is_in_progress = True
    select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = select_result[INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME]
    if command == INVERTER_UPGRADE_COMMAND_NO_COMMAND:
        upgrade_is_in_progress = False
    return upgrade_is_in_progress


def is_feed_discovery_in_progress():
    fd_is_in_progress = True
    select_string = "SELECT %s FROM %s;" % (FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME, FEED_DISCOVERY_TABLE)
    fd_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = fd_command[FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME]
    if command == FEED_DISCOVERY_COMMAND_NO_COMMAND:
        fd_is_in_progress = False
    return fd_is_in_progress


def is_fiu_set_against_this_ip(this_ip):
    # - read inverters table for this IP and its group ID
    # - read the groups table based on this IP:
    # - if FU is set: return True
    # - else return False
    fiu_is_set = False
    this_inverter_group_id = db_get_inverter_group_id(this_ip)
    this_group_row_data = db_get_group_id_row_data(this_inverter_group_id)
    if this_group_row_data:
        fiu_param = int(this_group_row_data[GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME])
        if fiu_param == 1:
            fiu_is_set = True
    return fiu_is_set


def is_gateway_control_set_against_this_ip(this_ip):
    # - read inverters table for this IP and its group ID
    # - read the groups table based on this group ID:
    # - if GTW_CTRL is set: return True
    # - else return False
    gateway_control_is_set = False
    this_inverter_group_id = db_get_inverter_group_id(this_ip)
    this_group_row_data = db_get_group_id_row_data(this_inverter_group_id)
    if this_group_row_data:
        gateway_control_param = int(this_group_row_data[GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME])
        if gateway_control_param == 1:
            gateway_control_is_set = True
    return gateway_control_is_set


# -------------------------------------------------------------------------------------------------
# Utilities to support PCS UNIT ID ASSOCIATION and CHARGER-TO-PCS ASSOCIATION processes.
#
def get_pui_assoc_command():
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    command = prepared_act_on_database(FETCH_ONE, select_string)
    disco_command = command[PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    return disco_command


def update_pui_assoc_command(command):
    update_string = "UPDATE %s SET %s = %s;" % (DB_PUI_ASSOCIATION_CONTROL_TABLE,
                                                PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME, "%s")
    args = (command,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def get_pui_assoc_status():
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    command = prepared_act_on_database(FETCH_ONE, select_string)
    pui_status = command[PUI_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME]
    return pui_status


def update_pui_assoc_status(status):
    update_string = "UPDATE %s SET %s = %s;" % (DB_PUI_ASSOCIATION_CONTROL_TABLE,
                                                PUI_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME, "%s")
    args = (status,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def update_pui_assoc_timestamp(date_and_time):
    update_string = "UPDATE %s SET %s = %s;" % (DB_PUI_ASSOCIATION_CONTROL_TABLE,
                                                PUI_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME, "%s")
    args = (date_and_time,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def get_pui_assoc_timestamp():
    pui_timestamp = "0000-00-00 00:00:00"
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string)
    if select_result:
        pui_timestamp = select_result[PUI_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME]
    return pui_timestamp


# Updates one of the counters column in the PUI ASSOC control table.
def update_pui_assoc_counter_column(column_name, counter_value):
    update_string = "UPDATE %s SET %s = %s;" % (DB_PUI_ASSOCIATION_CONTROL_TABLE, column_name, "%s")
    args = (counter_value,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


# Reads/gets the bmu activate/deactive timeouts from the PUI ASSOC control table.
def get_pui_assoc_act_deact_time():
    select_string = "SELECT %s,%s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_ACTIVATE_TIME_COLUMN_NAME,
                                               PUI_ASSOCIATION_CONTROL_TABLE_DEACTIVATE_TIME_COLUMN_NAME,
                                               DB_PUI_ASSOCIATION_CONTROL_TABLE)
    command = prepared_act_on_database(FETCH_ONE, select_string)
    activate_time = command[PUI_ASSOCIATION_CONTROL_TABLE_ACTIVATE_TIME_COLUMN_NAME]
    deactivate_time = command[PUI_ASSOCIATION_CONTROL_TABLE_DEACTIVATE_TIME_COLUMN_NAME]
    return (activate_time, deactivate_time)


def get_database_ordered_aess_bmu_id_list():
    # Return the list of BMU IDs from the AESS BMU Data table, ordered by BMU ID.
    # BMU IDs values are 1 or higher, 0 is not allowed.
    # Note: at some point, AESS is to be renamed to PCS. Might happen one day.
    bmu_id_list = []
    select_string = "SELECT %s FROM %s ORDER BY %s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                        DB_AESS_BMU_DATA_TABLE_NAME,
                                                        AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
    bmu_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_bmu_id in bmu_list:
        this_bmu_id = each_bmu_id[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME]
        if this_bmu_id != 0:
            bmu_id_list.append(this_bmu_id)
        else:
            # Really not expecting a BMU ID of 0. Whine a bit.
            # ALARM
            raise_and_clear_alarm(ALARM_ID_PCS_UNIT_ID_ASSOCIATION_BMU_ID_0_DETECTED)
    return bmu_id_list


def get_bmu_id_and_non_zero_current_from_bmu_data_table():
    bmu_id_list = []
    current_list = []
    # select bmu_id,current from aess_bmu_data where current!=0.00;
    select_string = "SELECT %s,%s FROM %s WHERE %s!=%s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                            AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                                            DB_AESS_BMU_DATA_TABLE_NAME,
                                                            AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, "%s")
    select_args = (0.00, )
    bmu_data_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for this_bmu in bmu_data_list:
        bmu_id_list.append(this_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME])
        current_list.append(this_bmu[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME])
    return (bmu_id_list, current_list)


def get_bmu_current_as_comma_separated_string():
    # Get all "current" readings from the BMU data table as a comma-separated string.
    # Current readings are floats, just get the decimal value.
    comma_separated_string = ""
    # select bmu_id,current from aess_bmu_data order by bmu_id;
    select_string = "SELECT %s FROM %s ORDER BY %s;" % (AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                                        DB_AESS_BMU_DATA_TABLE_NAME,
                                                        AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
    bmu_current_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for this_current_reading in bmu_current_list:
        comma_separated_string = comma_separated_string + str(int(this_current_reading[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME])) + ","
    # Strip off the last character - i.e., the last comma.
    comma_separated_string = comma_separated_string[:-1]
    return comma_separated_string


def is_bmu_data_table_dead_quiet():
    # Return true if all current readings in the bmu_data_table = 0.0amps
    # TODO: consider dead quiet where current readings < some minimum, say: MIN_BMU_CURRENT_THRESHOLD ...
    dead_quiet = False
    # select count(*) from bmu_data_table where current!='0.00';
    select_string = "SELECT COUNT(*) FROM %s WHERE %s!=%s;" % (DB_AESS_BMU_DATA_TABLE_NAME,
                                                               AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, "%s")
    select_args = (0.00, )
    select_results = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    row_count = int(select_results['COUNT(*)'])
    if row_count == 0:
        dead_quiet = True
    return dead_quiet


def get_database_ordered_pcs_units_bmu_id_A_list():
    # Return the list of BMU IDs from the AESS BMU Data table, ordered by BMU ID.
    # BMU IDs values are 1 or higher, 0 is not allowed.
    pcs_unit_bmu_id_A_list = []
    select_string = "SELECT %s FROM %s ORDER BY %s;" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                        DB_PCS_UNITS_TABLE_NAME,
                                                        PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME)
    pcs_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_id in pcs_list:
        this_id = each_id[PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME]
        if this_id != 0:
            pcs_unit_bmu_id_A_list.append(this_id)
        else:
            # Really not expecting a 0. Whine a bit.
            # ALARM
            raise_and_clear_alarm(ALARM_ID_PCS_UNIT_ID_ASSOCIATION_BMU_ID_0_DETECTED)
    return pcs_unit_bmu_id_A_list


# Return the list of IDs from the PCS UNITS table. PCS UNITS IDs values are 1 or higher, 0 is not allowed.
def get_database_pcs_units_id_list():
    pcs_units_id_list = []
    select_string = "SELECT %s FROM %s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
    pcs_units = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_pcs in pcs_units:
        this_pcs_units_id = each_pcs[PCS_UNITS_TABLE_ID_COLUMN_NAME]
        if this_pcs_units_id != 0:
            pcs_units_id_list.append(this_pcs_units_id)
        else:
            # Not expecting 0. Whine a bit.
            # ALARM
            raise_and_clear_alarm(ALARM_ID_PCS_UNIT_ID_ASSOCIATION_BMU_ID_0_DETECTED)
            pass
    return pcs_units_id_list


def db_get_inverters_pcs_unit_id_by_ip(some_ip):
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (some_ip, )
    fetch_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    pcs_unit_id = fetch_result[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME]
    return pcs_unit_id


def db_update_inverters_pui_assoc_status_by_ip(this_ip, this_status):
    # Update the pui_assoc_status of an individual IP.
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                          INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    update_args = (this_status, this_ip)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def db_update_inverters_pui_assoc_status1_to_status2(from_this_status, to_that_status):
    # Update the pui_assoc_status to "that" where pui_assoc_stattus is "this"
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                          INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                          INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s")
    update_args = (to_that_status, from_this_status)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def db_get_inverters_all_pui_waiting_list():
    # Return the *battery* inverters IP address list from the inverters table where status is waiting.
    ip_list = []
    select_string = "SELECT %s FROM %s WHERE %s=1 and %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                 DB_INVERTER_NXO_TABLE,
                                                                 INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                 INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s")
    select_args = (INVERTERS_TABLE_PUI_ASSOC_STATUS_WAITING, )
    waiting_ip_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for each_ip in waiting_ip_list:
        this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        ip_list.append(this_ip)
    return ip_list


def is_pui_assoc_in_progress():
    pui_assoc_in_progress = True
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    pui_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = pui_assoc_command[PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == PUI_ASSOC_NO_COMMAND_COMMAND):
        pui_assoc_in_progress = False
    return pui_assoc_in_progress


def is_pui_assoc_process_cancelled():
    pui_assoc_is_cancelled = False
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    pui_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = pui_assoc_command[PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == PUI_ASSOC_CANCEL_COMMAND):
        pui_assoc_is_cancelled = True
    return pui_assoc_is_cancelled


def is_pui_assoc_process_command_no_command():
    pui_assoc_is_no_command = False
    select_string = "SELECT %s FROM %s;" % (PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_PUI_ASSOCIATION_CONTROL_TABLE)
    pui_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = pui_assoc_command[PUI_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == PUI_ASSOC_NO_COMMAND_COMMAND):
        pui_assoc_is_no_command = True
    return pui_assoc_is_no_command


def get_pui_assoc_impediment_status():
    # Ensure there is no impediment to launching the PCS Unit ID association process, such as:
    # inverter upgrade or feed detection processes. Any other impediment that would prevent
    # the PCS Unit ID association process from becoming fully active go here.
    impediment_to_pui_assoc = ""

    if not is_get_database_ess_battery_control_idle():
        # The battery charge/discharge process is NOT idle.  PUI ASSOC will have to wait.
        impediment_to_pui_assoc = PUI_ASSOC_DELAYED_CONTROL_NOT_IDLE_STATUS
    elif is_inverter_upgrade_in_progress():
        # Active inverter upgrade process. PUI ASSOC will have to wait.
        impediment_to_pui_assoc = PUI_ASSOC_DELAYED_UPGRADE_STATUS
    elif is_feed_discovery_in_progress():
        # Active feed detection process. PUI ASSOC will have to wait.
        impediment_to_pui_assoc = PUI_ASSOC_DELAYED_FEED_DETECT_STATUS
    else:
        # Nothing in the way.
        pass
    return impediment_to_pui_assoc


def is_get_db_ess_units_table_by_id(some_id):
    # Is there a row in the ess_units table indexed by id?
    row_by_id_exists = False
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (ESS_UNITS_ID_COLUMN_NAME, DB_ESS_UNITS_TABLE, ESS_UNITS_ID_COLUMN_NAME, "%s")
    select_arg = (some_id, )
    select_command = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_command:
        row_by_id_exists = True
    return row_by_id_exists


def is_get_db_ess_master_info_table_by_ess_id(some_ess_id):
    # Is there a row in the ess_master_info table indexed by ess_id?
    row_by_ess_id_exists = False
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, DB_ESS_MASTER_INFO_TABLE, ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
    select_arg = (some_ess_id, )
    select_command = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_command:
        row_by_ess_id_exists = True
    return row_by_ess_id_exists


# This function can be used by any process.
def get_database_table_row_count(this_table_name):
    select_string = "SELECT COUNT(*) FROM %s;" % (this_table_name)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    row_count = int(select_results['COUNT(*)'])
    return row_count


def is_get_database_ess_battery_control_idle():
    # Read the "active power command" setting in the ESS master info table:
    # if zero, this indicates that battery control is idle.
    ess_battery_control_idle = False
    select_string = "SELECT %s FROM %s;" % (ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME, DB_ESS_MASTER_INFO_TABLE)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    active_power_command_level = select_results[ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME]
    if active_power_command_level == 0.0:
        ess_battery_control_idle = True
    return ess_battery_control_idle


def get_database_battery_inverters_count():
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=1;" % (DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_BATTERY_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    battery_inverters_count = int(select_results['COUNT(*)'])
    return battery_inverters_count


def get_database_battery_inverters_ip_list():
    # Return the list of *battery* inverters IP addresses from the inverters table.
    ip_list = []
    select_string = "SELECT %s FROM %s WHERE %s=1;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                       DB_INVERTER_NXO_TABLE,
                                                       INVERTERS_TABLE_BATTERY_COLUMN_NAME)
    battery_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    if battery_ip_list:
        print("GET DB BATTERY IP LIST ...")
        for each_ip in battery_ip_list:
            this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            ip_list.append(this_ip)
    else:
        print("BATTERY INVERTERS - YIELDED EMPTY IP LIST")
    return ip_list


def get_not_zero_db_battery_inverters_ip_and_pcs_units_id_list():
    # Return the IP addresses and pcs_unit_id lists from the inverters table
    # for battery inverters only where the pcs_unit_id is NOT ZERO.
    battery_inverters_ip_list = []
    battery_inverters_pcs_unit_id_list = []
    select_string = "SELECT %s,%s FROM %s WHERE %s=1 and %s!=0;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                    INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                    DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                    INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    my_super_duper_results = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_entry in my_super_duper_results:
        this_ip = each_entry[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        this_pcs_unit_id = each_entry[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME]
        battery_inverters_ip_list.append(this_ip)
        battery_inverters_pcs_unit_id_list.append(this_pcs_unit_id)
    return (battery_inverters_ip_list, battery_inverters_pcs_unit_id_list)


def verify_sanity_pcs_unit_id_and_aess_bmu_data():
    #
    # This is a collection of functions that validate the sanity and consistency of
    # both the pcs_units and aess_bmu_data tables. They are used by itca, as well as by
    # the inverter/bmu/pcs and charger/bmu/pcs association processes.
    #
    # All verification functions are collected here and used by both association processes
    # for sanity purpose on startup.
    #
    # If all is well, an empty string is returned. Else return a "something bad detected"
    # string when invalid or inconsistent data is encountered.
    bad_thing_detected_string = detect_null_bmu_id_a_or_b()

    if not bad_thing_detected_string:
        bad_thing_detected_string = detect_zero_bmu_id_a_from_pcs_units_table()

    if not bad_thing_detected_string:
        bad_thing_detected_string = detect_duplicate_bmu_id_from_pcs_units_table()

    if not bad_thing_detected_string:
        bad_thing_detected_string = detect_duplicate_bmu_id_from_bmu_data_table()

    return bad_thing_detected_string


def get_database_battery_inverters_zero_pcs_unit_id_count():
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=1 and %s=0;" % (DB_INVERTER_NXO_TABLE,
                                                                      INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                      INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    zero_pcs_unit_id_count = int(select_results['COUNT(*)'])
    return zero_pcs_unit_id_count


def battery_inverters_null_pcs_unit_id_entries_set_to_zeroO():
    # UPDATE inverters SET pcs_unti_id=0 WHERE battery=1 AND pcs_unit_id IS NULL;
    update_string = "UPDATE %s SET %s=0 WHERE %s=1 AND %s IS NULL;" % (DB_INVERTER_NXO_TABLE,
                                                                       INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                       INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                       INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    prepared_act_on_database(EXECUTE, update_string, ())


# -----------------------------------------------------------------------------
# CHARGER ASSOCIATION STUFF


def is_charger_assoc_process_command_no_command():
    charger_assoc_is_no_command = False
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    charger_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = charger_assoc_command[CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == CHARGER_ASSOC_NO_COMMAND_COMMAND):
        charger_assoc_command = True
    return charger_assoc_command


def is_charger_assoc_process_in_progress():
    charger_assoc_in_progress = True
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    charger_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = charger_assoc_command[CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == CHARGER_ASSOC_NO_COMMAND_COMMAND):
        charger_assoc_in_progress = False
    return charger_assoc_in_progress


def is_charger_assoc_process_cancelled():
    charger_assoc_cancelled = False
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    charger_assoc_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    command = charger_assoc_command[CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    if (command == CHARGER_ASSOC_CANCEL_COMMAND):
        charger_assoc_cancelled = True
    return charger_assoc_cancelled


def get_charger_id_and_module_num_list_by_chassis_id(chassis_id):
    charger_module_num_list = []
    charger_id_list = []
    # select charger_id, module_num from aess_charger_module where chassis_id=1;
    select_string = "SELECT %s,%s FROM %s WHERE %s=%s ORDER BY %s;" % (AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                                       AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME,
                                                                       DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                                       AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME, "%s",
                                                                       AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME)
    select_arg = (chassis_id, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_arg)
    for row_data in select_result:
        charger_module_num_list.append(row_data[AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME])
        charger_id_list.append(row_data[AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME])
    return (charger_module_num_list, charger_id_list)


def get_charger_assoc_timestamp():
    charger_timestamp = "0000-00-00 00:00:00"
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string)
    if select_result:
        charger_timestamp = select_result[CHARGER_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME]
    return charger_timestamp


def get_charger_assoc_command():
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    command = prepared_act_on_database(FETCH_ONE, select_string)
    charger_command = command[CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME]
    return charger_command


def get_charger_association_type():
    select_string = "SELECT %s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_ASSOCIATION_TYPE_COLUMN_NAME,
                                            DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    command = prepared_act_on_database(FETCH_ONE, select_string)
    association_command = command[CHARGER_ASSOCIATION_CONTROL_TABLE_ASSOCIATION_TYPE_COLUMN_NAME]
    return association_command


def update_charger_assoc_association_type(association_type):
    update_string = "UPDATE %s SET %s=%s;" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE,
                                              CHARGER_ASSOCIATION_CONTROL_TABLE_ASSOCIATION_TYPE_COLUMN_NAME, "%s")
    args = (association_type,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def update_charger_assoc_command(command):
    update_string = "UPDATE %s SET %s=%s;" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE,
                                              CHARGER_ASSOCIATION_CONTROL_TABLE_COMMAND_COLUMN_NAME, "%s")
    args = (command,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def update_charger_assoc_timestamp(date_and_time):
    update_string = "UPDATE %s SET %s=%s;" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE,
                                              CHARGER_ASSOCIATION_CONTROL_TABLE_TIMESTAMP_COLUMN_NAME, "%s")
    args = (date_and_time,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def update_charger_assoc_status(status):
    update_string = "UPDATE %s SET %s=%s;" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE,
                                              CHARGER_ASSOCIATION_CONTROL_TABLE_STATUS_COLUMN_NAME, "%s")
    args = (status,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


def get_number_charger_units_to_associate():
    # Return the count of the number of charger units waiting to be associated.
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=1;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                             AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    units_to_associate_count = int(select_result['COUNT(*)'])
    return units_to_associate_count


def get_db_number_rows_in_charger_table():
    select_string = "SELECT COUNT(*) FROM %s" % (DB_AESS_CHARGER_MODULE_TABLE_NAME)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_rows = int(select_result['COUNT(*)'])
    return number_of_rows


def get_charger_association_controller_limits():
    bmu_max_soc = 0
    max_discharge_minutes = 0
    select_string = "SELECT %s,%s FROM %s;" % (CHARGER_ASSOCIATION_CONTROL_TABLE_MAX_SOC_COLUMN_NAME,
                                               CHARGER_ASSOCIATION_CONTROL_TABLE_MAX_DISCHARGE_MINUTES,
                                               DB_CHARGER_ASSOCIATION_CONTROL_TABLE)
    select_result = prepared_act_on_database(FETCH_ONE, select_string)
    if select_result:
        bmu_max_soc = select_result[CHARGER_ASSOCIATION_CONTROL_TABLE_MAX_SOC_COLUMN_NAME]
        max_discharge_minutes = select_result[CHARGER_ASSOCIATION_CONTROL_TABLE_MAX_DISCHARGE_MINUTES]
    return (bmu_max_soc, max_discharge_minutes)


# -------------------------------------------------------------------------------------------------
# These 2 functions support the charger association when launched from the GPTS:
def init_all_assoc_associate_and_status_inactive():
    update_string = "UPDATE %s SET %s=%s,%s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                    AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME, "%s",
                                                    AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s")
    update_arg = (0, AESS_CHARGER_MODULE_ASSOC_INACTIVE_STATUS,)
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def set_charger_to_associate_with_status_waiting(charger_id):
    update_string = "UPDATE %s SET %s=%s,%s=%s WHERE %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                                AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME, "%s",
                                                                AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                                AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    update_arg = (1, AESS_CHARGER_MODULE_ASSOC_WAITING_STATUS, charger_id)
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


# -------------------------------------------------------------------------------------------------




def init_all_assoc_status_inactive():
    update_string = "UPDATE %s SET %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                              AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s")
    update_arg = (AESS_CHARGER_MODULE_ASSOC_INACTIVE_STATUS, )
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def update_associate_all_aess_charger_module(associate_value):
    update_string = "UPDATE %s SET %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                              AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME, "%s")
    update_arg = (associate_value, )
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def set_selected_chargers_with_waiting_status():
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                          AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                          AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME, "%s")
    update_arg = (AESS_CHARGER_MODULE_ASSOC_WAITING_STATUS, 1)
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def update_aess_charger_module_assoc_status(charger_id, status):
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                          AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                          AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    update_arg = (status, charger_id)
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def update_aess_charger_module_functionality(charger_id, functionality):
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                          AESS_CHARGER_MODULE_FUNCTIONALITY_COLUMN_NAME, "%s",
                                                          AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    update_arg = (functionality, charger_id)
    prepared_act_on_database(EXECUTE, update_string, update_arg)
    return


def is_charger_functionality_unequipped(charger_id):
    unequipped = False
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (AESS_CHARGER_MODULE_FUNCTIONALITY_COLUMN_NAME,
                                                        DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                        AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    select_arg = (charger_id, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_result:
        functionality = select_result[AESS_CHARGER_MODULE_FUNCTIONALITY_COLUMN_NAME]
        if functionality == AESS_CHARGER_MODULE_FUNCTIONALITY_UNEQUIPPED:
            unequipped = True
    return unequipped


def db_get_chassis_id_list_from_associate_chargers():
    chassis_id_list = []
    # select distinct chassis_id from aess_charger_module where associate=1;
    select_string = "SELECT DISTINCT %s FROM %s WHERE %s=1" % (AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME,
                                                               DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                               AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_row in select_result:
        chassis_id_list.append(each_row[AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME])
    return chassis_id_list


def db_get_charger_associate_list_from_chassis_id(this_chassis_id):
    # Return the charger ID list that have the "associate" column set to 1 for the chassis ID in question.
    # select charger_id,module_num from aess_charger_module where chassis_id=2 and associate=1 order by charger_id;
    charger_module_list = []
    charger_id_list = []
    select_string = "SELECT %s,%s FROM %s WHERE %s=1 AND %s=%s ORDER BY %s;" % (AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                                                AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME,
                                                                                DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                                                AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME,
                                                                                AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME, "%s",
                                                                                AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME)
    select_arg = (this_chassis_id, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_arg)
    for each_row in select_result:
        charger_module_list.append(each_row[AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME])
        charger_id_list.append(each_row[AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME])
    return (charger_module_list, charger_id_list)


def db_get_charger_module_row_by_module_num_and_chassis_id(module_number, chassis_id):
    select_string = "SELECT * FROM %s WHERE %s=%s and %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                                 AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME, "%s",
                                                                 AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME, "%s")

    select_arg = (module_number, chassis_id)
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    return select_result


def insert_aess_charger_module_by_module_number_chassis_id(module_number, chassis_id):
    # charger_id column is auto-increment.
    insert_string = "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)" \
                                                    % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                       AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_DEVICE_ID_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_MODEL_NUM_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_PCS_UNIT_ID_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_MIN_CHARGING_POWER,
                                                       AESS_CHARGER_MODULE_ASSOCIATE_COLUMN_NAME,
                                                       AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME,
                                                       "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")
    insert_args = (module_number, 1, "", chassis_id, 0, "", 0, AESS_CHARGER_MODULE_ASSOC_INACTIVE_STATUS)
    prepared_act_on_database(EXECUTE, insert_string, insert_args)
    return


def get_module_number_and_chassis_id_from_charger_id(charger_id):
    module_number = 0
    chassis_id = 0
    select_string = "SELECT %s,%s FROM %s WHERE %s=%s;" % (AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME,
                                                           AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME,
                                                           DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                           AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")

    select_arg = (charger_id, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_result:
        module_number = int(select_result[AESS_CHARGER_MODULE_MODULE_NUM_COLUMN_NAME])
        chassis_id = int(select_result[AESS_CHARGER_MODULE_CHASSIS_ID_COLUMN_NAME])
    return (module_number, chassis_id)


def db_get_chassis_id_list():
    chassis_id_list = []
    select_string = "SELECT %s FROM %s;" % (AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME, DB_AESS_CHARGER_CHASSIS_TABLE_NAME)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_chassis_id in select_result:
        chassis_id_list.append(int(each_chassis_id[AESS_CHARGER_CHASSIS_CHASSIS_ID_COLUMN_NAME]))
    return chassis_id_list


def db_get_charger_id_list_from_assoc_status(assoc_status):
    # Return the charger ID list where rows have the specified assoc_status.
    charger_id_list = []
    fetch_string = "SELECT %s FROM %s WHERE %s=%s;" % (AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME,
                                                       DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                       AESS_CHARGER_MODULE_ASSOC_STATUS_COLUMN_NAME, "%s")
    fetch_arg = (assoc_status, )
    fetch_result = prepared_act_on_database(FETCH_ALL, fetch_string, fetch_arg)
    for each_charger_id in fetch_result:
        charger_id_list.append(each_charger_id[AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME])
    return charger_id_list


def get_charger_assoc_impediment_status():
    # Ensure there is no impediment to launching the CHARGER association process, such as:
    # inverter upgrade or feed detection processes. Any other impediment that would prevent
    # the charger association process from becoming fully active go here.

    CHARGER_IMPEDIMENT_ESS_CONTROL_NOT_IDLE = "ESS controller not idle"
    CHARGER_IMPEDIMENT_UPGRADE_IN_PROGRESS = "upgrade in progress"
    CHARGER_IMPEDIMENT_FEED_DETECT_ACTIVE = "feed detect active"
    CHARGER_IMPEDIMENT_PUI_ASSOC_ACTIVE = "PUI association active"

    impediment_to_charger_assoc = None

    if not is_get_database_ess_battery_control_idle():
        # The battery charge/discharge process is NOT idle.
        impediment_to_charger_assoc = CHARGER_IMPEDIMENT_ESS_CONTROL_NOT_IDLE
    elif is_inverter_upgrade_in_progress():
        # Active inverter upgrade process.
        impediment_to_charger_assoc = CHARGER_IMPEDIMENT_UPGRADE_IN_PROGRESS
    elif is_feed_discovery_in_progress():
        # Active feed detection process.
        impediment_to_charger_assoc = CHARGER_IMPEDIMENT_FEED_DETECT_ACTIVE
    elif is_pui_assoc_in_progress():
        # Active PUI ASSOCIATION process.
        impediment_to_charger_assoc = CHARGER_IMPEDIMENT_PUI_ASSOC_ACTIVE
    else:
        # Nothing in the way.
        pass
    return impediment_to_charger_assoc


# Updates one of the counters column in the CHARGER ASSOC control table.
def update_charger_assoc_counter_column(column_name, counter_value):
    update_string = "UPDATE %s SET %s=%s;" % (DB_CHARGER_ASSOCIATION_CONTROL_TABLE, column_name, "%s")
    args = (counter_value,)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


# Used by charger_association, but is available to any entity.
def update_daemon_control_enabled_by_name(this_daemon_name, some_value):
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_DAEMON_CONTROL_TABLE_NAME,
                                                          DAEMON_CONTROL_TABLE_ENABLED_COLUMN_NAME, "%s",
                                                          DAEMON_CONTROL_TABLE_NAME_COLUMN_NAME, "%s")
    args = (some_value, this_daemon_name)
    prepared_act_on_database(EXECUTE, update_string, args)


# End of the section containing the CHARGER-TO-PCS ASSOCIATION processes.
#
# -------------------------------------------------------------------------------------------------


def get_database_solar_inverters_ip_list():
    # Return the list of *solar* inverters IP addresses from the inverters table.
    ip_list = []
    select_string = "SELECT %s FROM %s WHERE %s=0;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                       DB_INVERTER_NXO_TABLE,
                                                       INVERTERS_TABLE_BATTERY_COLUMN_NAME)
    sorta_big_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_ip in sorta_big_ip_list:
        this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        ip_list.append(this_ip)
    return ip_list


def get_database_total_inverters_ip_list():
    # Return the list of *all* inverters IP addresses from the inverters table.
    ip_list = []
    select_string = "SELECT %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
    big_big_very_big_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_ip in big_big_very_big_ip_list:
        this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        ip_list.append(this_ip)
    return ip_list


def is_send_inverter_pcs_unit_id_update(this_ip, pcs_unit_id):
    send_result = False
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_PCS_UNIT_ID_CONFIG_U16_TLV_TYPE, pcs_unit_id)
    if is_send_tlv_packed_data(this_ip, packed_data):
        send_result = True
    return send_result


def get_string_id_and_string_position_by_ip(this_ip):
    string_id = None
    string_position = None
    select_string = "SELECT %s,%s FROM %s where %s=%s;" % (INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                           INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                           DB_INVERTER_NXO_TABLE,
                                                           INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (this_ip, )
    select_results = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_results:
        string_id = select_results[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
        string_position = select_results[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME]
    return (string_id, string_position)


# Send an "AIF IMMED CONNECT" TLV to the prescribed IP (unicast or mcast), for any kind of inverter.
# The connect field could be 1 ("CONNECT") or 0 ("DISCONNECT"). Return True or False send indicator.
def is_send_inverter_connect_to_grid_command(inverter_ip, connect_value):
    send_connect = True
    tlv_pack = add_master_tlv(1)
    tlv_pack = append_user_tlv_16bit_unsigned_short(tlv_pack,
                                                    GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE,
                                                    connect_value)
    if not is_send_multicast_packet_and_close_socket(tlv_pack, inverter_ip, SG424_PRIVATE_UDP_PORT):
        # Wonder what could have happened. Let the caller deal with this mess.
        send_connect = False
    return send_connect


# Another form of the "send inv_ctrl gateway control" function via multicast.
# TODO: review all similar functions, there should only be 1.
def is_pack_and_send_mcast_gateway_control(multicast_ip, gateway_control_value):
    send_result = True
    tlv_pack = add_master_tlv(1)
    tlv_pack = append_user_tlv_16bit_unsigned_short(tlv_pack, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, gateway_control_value)
    if not is_send_multicast_packet_and_close_socket(tlv_pack, multicast_ip, SG424_PRIVATE_UDP_PORT):
        send_result = False
    return send_result


# The following 2 fellas can be anyone's friend to set/get a process "pid":
def set_database_process_pid(table_name, pid_column_name, some_pid_value):
    update_string = "UPDATE %s SET %s = %s;" % (table_name, pid_column_name, "%s")
    args = (str(some_pid_value),)
    prepared_act_on_database(EXECUTE, update_string, args)
    return True


# This function not used, but available in case its use is warranted.
def get_database_process_pid(table_name, pid_column_name):
    select_string = "SELECT %s FROM %s;" % (pid_column_name, table_name)
    command = prepared_act_on_database(FETCH_ONE, select_string, ())
    process_pid = command[pid_column_name]
    return process_pid


# -------------------------------------------------------------------------------------------------
# For use by inverter upgrade, power factor control:

def any_impediment_to_inverter_upgrade():
    # Ensure there is no impediment to launching the inverter upgrade process, such as:
    # active pui_assoc or feed discovery processes. Any other impediment that would
    # prevent inverter upgrade from becoming fully active go here.
    impediment_to_upgrade = ""
    if is_pui_assoc_in_progress():
        # Active PCS Unit ID Association process. Inverter upgrade will have to wait.
        # Note: the database could be read to see if inverters have their association
        #       bit set. That would also mean verifying their string membership.
        impediment_to_upgrade = "PUI_ASSOC"
    elif is_charger_assoc_process_in_progress():
        # Active charger association process. Inverter upgrade will have to wait.
        # Note: this is not really an inverter issue, as inverters are not involved.
        impediment_to_upgrade = "CHARGER_ASSOC"
    elif is_feed_discovery_in_progress():
        # Active feed detection process. Inverter upgrade will have to wait.
        impediment_to_upgrade = "FEED_DETECT"
    else:
        # Nothing in the way.
        pass
    return impediment_to_upgrade


def any_impediment_to_feed_detection():
    # Ensure there is no impediment to launching the feed detection process, such as:
    # active pui_assoc or inverter upgrade processes. Any other impediment that would
    # prevent feed detection from becoming fully active go here.
    impediment_to_feed_detection = ""
    if is_pui_assoc_in_progress():
        # Active PCS Unit ID association process. Feed detection will have to wait.
        impediment_to_feed_detection = "PUI_ASSOC"
    elif is_charger_assoc_process_in_progress():
        # Active charger association process. Inverter upgrade will have to wait.
        impediment_to_upgrade = "CHARGER_ASSOC"
    elif is_inverter_upgrade_in_progress():
        # Active inverter upgrade process. Feed detection will have to wait.
        impediment_to_feed_detection = "UPGRADE"
    else:
        # Nothing in the way.
        pass
    return impediment_to_feed_detection


def is_gateway_operation_enabled():
    # NOTE: "system_enable" in the gateway table is "0" or "1" as a varchar!
    gateway_is_enabled = False
    select_string = "SELECT %s FROM %s;" % (GATEWAY_TABLE_SYSTEM_ENABLE_COLUMN_NAME, DB_GATEWAY_TABLE)
    select_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    if select_command:
        gateway_system_enable = select_command[GATEWAY_TABLE_SYSTEM_ENABLE_COLUMN_NAME]
        if gateway_system_enable == "1":
            gateway_is_enabled = True
    return gateway_is_enabled


#
# -------------------------------------------------------------------------------------------------


def get_boot_rev_from_inverter_upgrade():
    boot_version_short = 0
    select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_BOOT_REV_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
    boot_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    boot_version_short = boot_command[INVERTER_UPGRADE_TABLE_BOOT_REV_COLUMN_NAME]
    return boot_version_short


def get_prefered_svn_version():
    # Used by inverter upgrade: determine if inverter running "old" software first needs to
    # be upgraded to a preliminary version of code, including a boot loader upgrade.
    select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_PREF_SVN_VERSION_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
    select_command = prepared_act_on_database(FETCH_ONE, select_string, ())
    prefered_svn_version_string = select_command[INVERTER_UPGRADE_TABLE_PREF_SVN_VERSION_COLUMN_NAME]
    return prefered_svn_version_string


def get_svn_number_from_string_version(string_version):
    # If an SVN number cannot be gleaned from the file name alone, return 0.
    #
    # The expected formats are: 313svn1234
    #                           313svn1234_xxxx
    #                           313_1234
    #                           313_1234_xxxx
    svn_number = 0
    if string_version:
        # First check if SVN is in the string.
        string_version = string_version.upper()
        string_version_components = string_version.split("SVN")

        if len(string_version_components) > 1:
            # Got something that looks like: "313svn1234" or "313svn1234_xxxx". Split on basis of underscore.
            portion_beyond_svn = string_version_components[1]
            # Now have something that looks like: "1234" or "1234_xxxx". Split on basis of underscore.
            # If the length after splitting is zero, the svn is in the [0] component. If the string does
            # split on underscore, the svn is still in the [0] component.
            svn_components = portion_beyond_svn.split("_")
            svn_portion_as_string = svn_components[0]
            if svn_portion_as_string.isdigit():
                svn_number = int(svn_portion_as_string)
            else:
                # Nope.
                pass

        else:
            # File name without the SVN nomenclature. Most likely a "modern" version format.
            # Expect something like "313_5678" or "313_5678_xxxx". When split on basis of
            # underscore, length is 2 or greater, and the SVN number is in the [1] component.
            string_version_components = string_version.split("_")
            if len(string_version_components) >= 2:
                # Got something like 313_5678 so the SVN number is in the [1] component.
                svn_portion_as_string = string_version_components[1]
                if svn_portion_as_string.isdigit():
                    svn_number = int(svn_portion_as_string)
                else:
                    # Nope.
                    pass
            else:
                # No underscore. The version did not contain SVN or underscore. Odd.
                pass
    else:
        # A null string was passed.
        pass

    return svn_number


def is_forced_upgrade_allowed(some_group_id):
    forced_upgrade_ok = False
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME,
                                                        DB_GROUPS_TABLE,
                                                        GROUPS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
    select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
    if select_result[GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME] == 1:
        forced_upgrade_ok = True
    return forced_upgrade_ok


def get_comm_ip_list_from_not_version(that_which_is_not_this_inverter_version, max_number_of_inverters):
    # Get the list of "communicating" IP addresses from the inverters table (comm == APP or BOOT LOADER/UPDATER)
    # and whose version is NOT that of the specified version passed as parameter.
    ip_list = []
    select_string = "SELECT %s,%s FROM %s WHERE (%s=%s OR %s=%s OR %s=%s) AND %s!=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                                          INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                                                                                          DB_INVERTER_NXO_TABLE,
                                                                                          INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                                                          INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                                                          INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                                                          INVERTERS_TABLE_VERSION_COLUMN_NAME, "%s")
    select_args = (INVERTERS_TABLE_COMM_STRING_SG424_APPLICATION, INVERTERS_TABLE_COMM_STRING_BOOT_LOADER,
                   INVERTERS_TABLE_COMM_STRING_BOOT_UPDATER, that_which_is_not_this_inverter_version,
                   max_number_of_inverters)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    number_of_ip_added = 0
    for each_row in select_result:
        # If this inverter a member of a group where forced upgrade is allowed?
        this_row_ip = each_row[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        this_row_group_id = each_row[INVERTERS_TABLE_GROUP_ID_COLUMN_NAME]
        if is_forced_upgrade_allowed(this_row_group_id):
            # This inverter can be added.
            ip_list.append(this_row_ip)
            number_of_ip_added += 1
            if number_of_ip_added > max_number_of_inverters:
                break
    return ip_list


def get_inverters_version_column_by_ip(inverter_ip_address):
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_VERSION_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (inverter_ip_address, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    inverter_version = select_result[INVERTERS_TABLE_VERSION_COLUMN_NAME]
    return inverter_version


def is_db_hos_inverter_communicating(inverter_string_id):
    # This checks to see if the HOS inverter is declared as being in communication
    # based on the database alone. It does not perform an actual network query. So
    # check the "comm" column of the HOS inverter, which is the row corresponding
    # to the specified string ID and string position = 1.
    hos_is_communicating = True
    select_string = "SELECT %s FROM %s where %s = %s and %s = %s;" % (INVERTERS_TABLE_COMM_COLUMN_NAME,
                                                                      DB_INVERTER_NXO_TABLE,
                                                                      INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME, "%s",
                                                                      INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
    select_args = (1, inverter_string_id)
    select_command = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    database_comm_string = select_command[INVERTERS_TABLE_COMM_COLUMN_NAME]
    if database_comm_string == INVERTERS_TABLE_COMM_STRING_NO_COMM:
        hos_is_communicating = False
    return hos_is_communicating


def execute_on_irradiance_inverters_table(inverter_action, ip_address):
    if inverter_action == "ADD":
        print("INSERT IP %s INTO IRRADIANCE_INVERTERS TABLE" % (ip_address))
        execute_string = "INSERT INTO %s (%s) VALUES(%s);" % (DB_IRRADIANCE_INVERTERS_TABLE,
                                                              DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME, "%s")
    else:
        print("DELETING IP %s FROM IRRADIANCE_INVERTERS TABLE" % (ip_address))
        execute_string = "DELETE FROM %s where %s = %s;" % (DB_IRRADIANCE_INVERTERS_TABLE,
                                                            DB_IRRA_INVERTERS_TBL_IP_ADDRESS_COLUMN_NAME, "%s")
    some_args = (ip_address,)
    prepared_act_on_database(EXECUTE, execute_string, some_args)
    return


def is_send_tlv_packed_data(ip_address, packed_data):
    send_result = False
    this_port = SG424_PRIVATE_UDP_PORT
    # This method sends a packet and returns. No response expected.
    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(type=socket.SOCK_DGRAM)
    except Exception as error:
        raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR)
    else:

        # SEND THE PACKET:
        try:
            fancy_socks.sendto(packed_data, (ip_address, this_port))
        except Exception as error:
            # Has been known to happen. Close the socket.
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR)
            try:
                fancy_socks.close()
            except Exception as unicast_error:
                # Never really expected, but you never know ...
                raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR)
            else:
                # Notice already made that the send failed, the close was OK,
                # so nothing to do here.
                pass

        else:

            # Close the socket.
            try:
                fancy_socks.close()
            except Exception as unicast_error:
                # Never really expected, but you never know ...
                raise_and_clear_alarm(ALARM_ID_GTW_UTILS_SEND_TLV_PACKED_DATA_ERROR)
            else:
                # Send and socket close successful. Nothing else to do.
                send_result = True

    return send_result


def is_send_feed_pcc_command(ip_address, this_pcc, this_feed):

    number_of_user_tlv = 2
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_CONFIG_PCC_U16_TLV_TYPE, this_pcc)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_CONFIG_FEED_U16_TLV_TYPE, this_feed)
    # Upon failure: normally, might want to consider a retry, or returning success/fail indication to caller.

    send_result = is_send_tlv_packed_data(ip_address, packed_data)
    return send_result


def send_fixed_pf_command(ip_address, list_of_tlv, list_of_values):

    number_of_user_tlv = len(list_of_tlv)
    packed_data = add_master_tlv(number_of_user_tlv)
    # for tlv_id, tlv_data in zip(list_of_tlv, list_of_values):
    for iggy in range (0, number_of_user_tlv):

        if list_of_tlv[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE:
            # Add the power factor as a float.
            packed_data = append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE, list_of_values[iggy])

        elif list_of_tlv[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE:
            # Add the power factor current leading paramater as a 16-bit short.
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE, list_of_values[iggy])

        elif list_of_tlv[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE:
            # Add the power factor enable paramater as a 16-bit short.
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE, list_of_values[iggy])

        else:
            # No good
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_INVALID_PF_COMMAND_TLV)
            return

    # Upon failure: normally, might want to consider a retry, or returning success/fail indication to caller.
    donut_care = is_send_tlv_packed_data(ip_address, packed_data)
    return


def is_send_aif_immed_configuration_command(ip_address, tlv_id_list, tlv_values_list):
    # TODO: GET RID OF THIS FUNCTION - NOW IS THE TIME TO USE THE TLV-UTILS PACKER!!!
    pack_and_send_ok = True
    number_of_user_tlv = len(tlv_id_list)
    packed_data = add_master_tlv(number_of_user_tlv)
    # for tlv_id, tlv_data in zip(tlv_id_list, tlv_values_list):
    for iggy in range (0, number_of_user_tlv):

        if tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])

        else:
            # No good
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_INVALID_SET_AIF_IMMED_COMMAND)
            pack_and_send_ok = False
            break

    if pack_and_send_ok:
        pack_and_send_ok = is_send_tlv_packed_data(ip_address, packed_data)
    return pack_and_send_ok


def is_send_aif_settings_configuration_command(ip_address, tlv_id_list, tlv_values_list):
    # TODO: GET RID OF THIS FUNCTION - NOW IS THE TIME TO USE THE TLV-UTILS PACKER!!!
    pack_and_send_ok = True
    number_of_user_tlv = len(tlv_id_list)
    packed_data = add_master_tlv(number_of_user_tlv)
    # for tlv_id, tlv_data in zip(tlv_id_list, tlv_values_list):
    for iggy in range (0, number_of_user_tlv):

        if tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_AC_VOLTAGE_OFFSET_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_IN_EEPROM_U32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_unsigned_long(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE:
            packed_data = append_user_tlv_32bit_float(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_IN_EEPROM_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        else:
            # No good
            print("BAD TLV %s" % tlv_id_list[iggy])
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_INVALID_SET_AIF_SETTINGS_COMMAND)
            pack_and_send_ok = False
            break

    if pack_and_send_ok:
        pack_and_send_ok = is_send_tlv_packed_data(ip_address, packed_data)
    return pack_and_send_ok


def is_send_eerpt_configuration_command(ip_address, tlv_id_list, tlv_values_list):
    # TODO: GET RID OF THIS FUNCTION - NOW IS THE TIME TO USE THE TLV-UTILS PACKER!!!
    pack_and_send_ok = True
    number_of_user_tlv = len(tlv_id_list)
    packed_data = add_master_tlv(number_of_user_tlv)
    # for tlv_id, tlv_data in zip(tlv_id_list, tlv_values_list):
    for iggy in range (0, number_of_user_tlv):
        if tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_ENABLE_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_RATE_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_URL_STRING_TLV_TYPE:
            packed_data = append_user_tlv_string(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_PORT_U16_TLV_TYPE:
            packed_data = append_user_tlv_16bit_unsigned_short(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE:
            packed_data = append_user_tlv_string(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])
        elif tlv_id_list[iggy] == GTW_TO_SG424_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE:
            packed_data = append_user_tlv_string(packed_data, tlv_id_list[iggy], tlv_values_list[iggy])

        else:
            # No good
            # TODO: to be consistent with similar commands, should have its own alarm. But is an alarm warranted?
            print("BAD TLV %s" % tlv_id_list[iggy])
            raise_and_clear_alarm(ALARM_ID_GTW_UTILS_INVALID_SET_AIF_IMMED_COMMAND)
            pack_and_send_ok = False
            break

    if pack_and_send_ok:
        pack_and_send_ok = is_send_tlv_packed_data(ip_address, packed_data)
    return pack_and_send_ok


def is_send_keep_alive_timeout(ip_address, keep_alive_timout):
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_32bit_unsigned_long(packed_data, GTW_TO_SG424_CONFIG_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE, keep_alive_timout)
    send_result = is_send_tlv_packed_data(ip_address, packed_data)
    return send_result


def is_send_upgrade_reset_by_tlv(ip_address):
    dummy_data = 5678
    packed_data = add_master_tlv(1)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE, dummy_data)
    send_result = is_send_tlv_packed_data(ip_address, packed_data)
    return send_result


# -------------------------------------------------------------------------------------------------
#
#
# This set of functions: to support the various auditing/checking of SG424 inverters
#                        and the "inverters" database table.
#
#
def inverters_db_consistency_check_version(results_file_handle):
    # Check "version": do all rows have the same version?
    this_string = "\r\nVERSION AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    fetch_results = []
    fetch_string = "SELECT version, COUNT(*) c FROM inverters GROUP BY version HAVING c > 0;"
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    if len(fetch_results) == 0:
        # There should be at least 1 row. Unless the database is empty?
        this_string = "- THERE ARE NO ROWS WITH ANY VERSION!!!"
        results_file_handle.write(this_string + LINE_TERMINATION)
    else:
        for this_version_results in fetch_results:
            this_row_count = this_version_results["c"]
            this_row_version = this_version_results["version"]
            this_string = "- ROWS WITH VERSION "
            if not this_row_version:
                this_string += "NULL: "
            else:
                this_string += this_row_version + ": "
            this_string += str(this_row_count)
            results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_null_sn(results_file_handle):
    # Check "serialNumber: any "NULL" entries?
    this_string = "\r\nNULL SERIAL NUMBER AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "serialNumber: any "NULL" entries?
    fetch_string = "SELECT COUNT(*) FROM inverters WHERE serialNumber is NULL;"
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, ())

    null_count_string = fetch_results
    this_null_count = int(null_count_string["COUNT(*)"])
    this_string = "- NUMBER OF NULL SERIAL NUMBER ENTRIES = " + str(this_null_count)
    results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_distinct_sn(results_file_handle):
    # Check "serialNumber": all rows are distinct?
    this_string = "\r\nDISTINCT SERIAL NUMBER AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "serialNumber": all rows are distinct?
    fetch_string = "SELECT serialNumber, COUNT(*) c FROM inverters GROUP BY serialNumber HAVING c > 1;"
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    serial_number_length = len(fetch_results)
    duplicate_serial_numbers_found = False
    for this_sn_results in fetch_results:
        this_serial_number_count = this_sn_results["c"]
        this_actual_serial_number = this_sn_results["serialNumber"]
        this_string = "- ROWS WITH VERSION: "
        if not this_actual_serial_number:
            # This is a row with NULL serial number. Needs to be audited
            # against an actual inverter. It is not considered a duplicate.
            this_string += "NULL SERIAL NUMBER:  <- %s entries" % (this_serial_number_count)
            results_file_handle.write(this_string + LINE_TERMINATION)
        else:
            # An actual serial number. The row count is expected to be 1.
            if this_serial_number_count == 1:
                # This is good. Keep quiet about it.
                pass
            else:
                # Count is not 1, there are duplicate serial numbers.
                duplicate_serial_numbers_found = True
                this_string = "- DUPLICATE SERIAL NUMBER: " + this_actual_serial_number \
                              + ": <- " + str(this_serial_number_count) + " ROWS"
                results_file_handle.write(this_string + LINE_TERMINATION)

    if duplicate_serial_numbers_found == False:
        this_string = "- NO OTHER DUPLICATE SN FOUND"
        results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_null_mac_address(results_file_handle):
    # Check "mac_address": any "NULL" entries?
    this_string = "\r\nNULL MAC AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "mac_address": any "NULL" entries?
    fetch_results = []
    fetch_string = "SELECT COUNT(1) FROM inverters WHERE mac_address = 'NULL';"
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, ())

    null_count_string = fetch_results
    this_null_count = int(null_count_string["COUNT(1)"])
    this_string = "- NULL MAC ENTRIES: " + str(this_null_count)
    results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_distinct_mac_address(results_file_handle):
    # Check "mac_address": all rows are distinct?
    this_string = "\r\nDISTINCT MAC AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "mac_address": all rows are distinct?
    fetch_results = []
    fetch_string = "SELECT mac_address, COUNT(*) c FROM inverters GROUP BY mac_address HAVING c > 1;"
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    if len(fetch_results) == 0:
        # A length of zero means there are no duplicated mac addresses.
        this_string = "- DUPLICATED mac_address: NONE"
        results_file_handle.write(this_string + LINE_TERMINATION)

    else:
        count_string = fetch_results[0]
        this_count = str(count_string["c"])
        this_string = "- DUPLICATED mac_address: " + str(this_count)
        results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_null_ip_address(results_file_handle):
    # Check "IP_Address": any "NULL" entries?
    this_string = "\r\nNULL IP AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "IP_Address": any "NULL" entries?
    fetch_results = []
    fetch_string = "SELECT COUNT(1) FROM inverters WHERE IP_Address = 'NULL';"
    fetch_results = prepared_act_on_database(FETCH_ONE, fetch_string, ())

    null_count_string = fetch_results
    this_null_count = int(null_count_string["COUNT(1)"])
    this_string = "- NULL IP ENTRIES: " + str(this_null_count)
    results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_distinct_ip_address(results_file_handle):
    # Check "IP_Address": all rows are distinct?
    this_string = "\r\nDISTINCT IP AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "IP_Address": all rows are distinct?
    fetch_results = []
    fetch_string = "SELECT IP_Address, COUNT(*) c FROM inverters GROUP BY IP_Address HAVING c > 1;"
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    if len(fetch_results) == 0:
        # A length of zero means there are no duplicated ip addresses.
        this_string = "- DUPLICATED IP_Address: NONE"
        results_file_handle.write(this_string + LINE_TERMINATION)

    else:
        count_string = fetch_results[0]
        this_count = str(count_string["c"])
        this_string = "- DUPLICATED IP_Addresses: %s" % (this_count)
        results_file_handle.write(this_string + LINE_TERMINATION)
        # Print/write all rows with duplicate IP addresses.
        tlv_id_list = []
        tlv_data_list = []
        inverters_id_list = []
        for row_data in fetch_results:
            this_duplicate_ip = row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            this_string = "  - CLEANUP REQUIRED FOR ROWS WITH DUPLICATE IP=%s" % (this_duplicate_ip)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            select_string = "SELECT %s, %s, %s, %s from %s where %s=%s;" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                                            INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                            INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                                                                            INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                                                                            DB_INVERTER_NXO_TABLE,
                                                                            INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
            select_args = (this_duplicate_ip,)
            duplicate_row_data = prepared_act_on_database(FETCH_ALL, select_string, select_args)
            if duplicate_row_data:
                for this_duplicate_row in duplicate_row_data:
                    this_string = "    -> ID/IP/MAC/SN = %s, %s, %s, %s" % (this_duplicate_row[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME],
                                                                            this_duplicate_row[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME],
                                                                            this_duplicate_row[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME],
                                                                            this_duplicate_row[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME])
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)
                    inverters_id_list.append(this_duplicate_row[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME])

                    # Prepare a TLV packet in which an inverter will reset by mac AND serial number.
                    if this_duplicate_row[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME]:
                        # There's a valid, non-empty MAC against which an inverter can be reset.
                        tlv_id_list.append(GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE)
                        tlv_data_list.append(this_duplicate_row[INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME])
                    if this_duplicate_row[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME]:
                        # There's a valid, non-empty SN against which an inverter can be reset.
                        tlv_id_list.append(GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE)
                        tlv_data_list.append(this_duplicate_row[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME])

            else:
                # Huh??? This is bad. Really really bad. And unexpected. Break out of the for loop.
                # Who knows what else will happen otherwise!
                this_string = "FOUND DUPLICATE IP ... BUT NOW GONE WHILE SELECTING THEM!!!"
                results_file_handle.write(this_string + LINE_TERMINATION)
                del tlv_id_list[:]
                del tlv_data_list[:]
                break

            if (len(tlv_id_list) == len(tlv_data_list)) and len(tlv_id_list) != 0:
                this_string = "  - PREPARING A SINGLE TLV PACKET WITH THE FOLLOWING RESET TLVs:"
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                number_of_user_tlv = len(tlv_id_list)
                packed_data = add_master_tlv(number_of_user_tlv)

                for iggy in range(0, number_of_user_tlv):
                    this_string = "    - TLV #%s (ID/DATA) = %s %s" % (iggy+1, tlv_id_list[iggy], tlv_data_list[iggy])
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)
                    if tlv_id_list[iggy] == GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE:
                        packed_data = append_user_tlv_mac_as_hex(packed_data, tlv_id_list[iggy], tlv_data_list[iggy])
                    elif tlv_id_list[iggy] == GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE:
                        packed_data = append_user_tlv_string(packed_data, tlv_id_list[iggy], tlv_data_list[iggy])

                multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
                if is_send_tlv_packed_data(multicast_ip, packed_data):
                    this_string = "    - TLV PACKET SEND OK ..."
                else:
                    this_string = "    - TLV PACKET SEND *FAILED*"
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                del tlv_id_list[:]
                del tlv_data_list[:]
        # for ...


            '''
            It was intended on deleting all rows with duplicate IP addresses, but that has repercussions with
            feed name, which is also wiped out against this inverter entry. It can be very tricky afterwards to
            re-establish what feed it was on, especially if it was an HOS inverter. Not impossible, just tricky.
            For now, the database is left untouched.

            this_string = "PREPARING TO DELETE THE FOLLOWING ROWS WITH INVERTERS_ID = "
            for iggy in range(0, len(inverters_id_list)):
                this_string += " %s" % (inverters_id_list[iggy])
                delete_string = "DELETE FROM %s WHERE %s = %s" % (DB_INVERTER_NXO_TABLE,
                                                                  INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME, "%s")
                delete_args = (inverters_id_list[iggy],)
                prepared_act_on_database(EXECUTE, delete_string, delete_args)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            '''


def inverters_db_consistency_check_string_id_and_position(results_file_handle):
    # Check "stringPosition": all rows 0 to 8?
    # -----------------------------------------------------------------------------------------
    # This section of this audit checks the consistency of string ID and string position
    # for each row in the inverters table..
    string_id_position_required_auditing = False
    this_string = "\r\nSTRING ID/POSITION AUDIT PER INVERTER:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check "stringPosition": are they all 0 to 8? The string position is a simple integer,
    # an inverter is in string position 1 through 8 if it is known, 0 otherwise. If the
    # string position is zero, ensure that the string ID is also the 00:00:00 default.
    fetch_string = "SELECT %s,%s, %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                  INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                  INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                  DB_INVERTER_NXO_TABLE)
    fetch_results = None
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    # Check each inverter row.
    for row_data in fetch_results:
        inverter_string_id = row_data[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
        inverter_string_position = str(row_data[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME])
        inverter_ip_address = row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        if inverter_string_position.isdigit():
            string_position_as_integer = int(inverter_string_position)
            # It's a digit. Is is between 1 and 8?
            if (string_position_as_integer >= 1) and (string_position_as_integer <= 8):
                # It's good. No news is good news, so nothing to do here.
                pass
            elif string_position_as_integer == 0:
                # With a string position of 0, then string ID must be the default.
                if inverter_string_id == DEFAULT_STRING_ID:
                    # Both string ID and position are null/empty, this is consistent,
                    # other than feed detect needs to be run. So nothing to do here.
                    pass
                else:
                    # Needs to be corrected.
                    this_string = "- IP %s -> stringPosition = 0, set stringID/position to %s/0" % (inverter_ip_address, DEFAULT_STRING_ID)
                    results_file_handle.write(this_string + LINE_TERMINATION)
                    inverter_string_id = DEFAULT_STRING_ID
                    inverter_string_position = 0
                    update_inverters_string_id_string_position_column_by_ip(inverter_string_id, inverter_string_position, inverter_ip_address)
                    string_id_position_required_auditing = True
                    pass
            else:
                # Invalid value for string position.
                this_string = "- IP %s -> stringPosition = %s NOT 1..8, set stringID/position to %s/0" % (inverter_ip_address, string_position_as_integer, DEFAULT_STRING_ID)
                results_file_handle.write(this_string + LINE_TERMINATION)
                inverter_string_id = DEFAULT_STRING_ID
                inverter_string_position = 0
                update_inverters_string_id_string_position_column_by_ip(inverter_string_id, inverter_string_position, inverter_ip_address)
                string_id_position_required_auditing = True

        else:
            this_string = "- IP %s -> stringPosition = %s NOT A DIGIT, set to %s/0" % (inverter_ip_address, inverter_string_position, DEFAULT_STRING_ID)
            results_file_handle.write(this_string + LINE_TERMINATION)
            inverter_string_id = DEFAULT_STRING_ID
            inverter_string_position = 0
            update_inverters_string_id_string_position_column_by_ip(inverter_string_id, inverter_string_position, inverter_ip_address)

            string_id_position_required_auditing = True

        # If no auditing on string position was required, check the format of the string ID,
        # it must be of the form xx:xx:xx (i.e., the last 3 digits of a mac).
        if string_id_position_required_auditing == False:
            # The string position was valid, now validate the string ID itself.
            # First things first: must be of the form "xx:xx:xx".
            if is_string_id_looka_lika_string_id(inverter_string_id):
                # Seemingly valid string ID. Nothing left to do here.
                pass
            else:
                # Does not look like a valid string ID.
                this_string = "- IP %s stringId incorrect format, set to %s/0" % (inverter_ip_address, DEFAULT_STRING_ID)
                results_file_handle.write(this_string + LINE_TERMINATION)
                inverter_string_id = DEFAULT_STRING_ID
                inverter_string_position = 0
                update_inverters_string_id_string_position_column_by_ip(inverter_string_id, inverter_string_position, inverter_ip_address)
                string_id_position_required_auditing = True

    # for ...

    if string_id_position_required_auditing:
        this_string = "- issues found and corrected - *** RE-RUN STRING DETECTION***"
    else:
        this_string = "- no issues found with string ID/position"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # -----------------------------------------------------------------------------------------
    # This section of this audit checks each seemingly valid string ID, ensuring that
    # there are no more than 8 inverters per string. The select string is of the form:
    #
    # select stringId from inverters where stringId != '00:00:00' group by stringId;

    string_id_aggregate_required_auditing = False
    this_string = "\r\nAGGREGATED STRING ID/POSITION AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    select_string = "select %s from %s where %s != %s group by %s;" %(INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                                      DB_INVERTER_NXO_TABLE,
                                                                      INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                      INVERTERS_TABLE_STRING_ID_COLUMN_NAME)
    select_args = (DEFAULT_STRING_ID,)
    string_id_results = prepared_act_on_database(FETCH_ALL, select_string, select_args)

    for each_string_id in string_id_results:
        this_string_id = each_string_id[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
        # Ensure that there are no more than 8 entries per stringId. The select string is of the form:
        #
        # SELECT COUNT(*) FROM inverters WHERE stringId = '01:10:d9';
        select_count_string = "SELECT COUNT(*) FROM %s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
        select_count_args = (this_string_id,)
        select_count_results = prepared_act_on_database(FETCH_ONE, select_count_string, select_count_args)
        row_count = int(select_count_results['COUNT(*)'])
        if row_count > 8:
            # Bad string ... clear all rows with this string id.
            # UPDATE inverters SET stringId = '00:00:00', stringPosition = 0 WHERE stringId = '01:11:e1';
            clear_string_id_string = "UPDATE %s SET %s=%s,%s=0 WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                                INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s",
                                                                                INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                                INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
            clear_string_id_args = (DEFAULT_STRING_ID, this_string_id)
            prepared_act_on_database(EXECUTE, clear_string_id_string, clear_string_id_args)
            this_string = "- stringId " + this_string_id + " has " \
                          + str(row_count) + " members, set all such stringId to " + str(DEFAULT_STRING_ID) + ",0"
            results_file_handle.write(this_string + LINE_TERMINATION)
            string_id_aggregate_required_auditing = True
        else:
            # 8 or fewer rows with this stringId, life is good. Relax.
            pass

    if string_id_aggregate_required_auditing:
        this_string = "- issues found and corrected with AGGREGATE - *** RE-RUN STRING DETECTION***"
    else:
        this_string = "- no issues found with AGGREGATE string ID/position"
    results_file_handle.write(this_string + LINE_TERMINATION)


def inverters_db_consistency_check_feed_name(results_file_handle):
    # Check FEED_NAME. For all entries in the inverters table, ensure that the feed_name
    # is either A, B or C. If not, write a null FEED_NAME for that row. If the system is
    # "aggregate", there is nothing to audit.


    # ---------------------------------------------------------------------------------------------
    # Local function to support a progress indicator.
    def itca_progress_indicator(next_expected_percent, last_percent, entries_audited_so_far, entries_to_audit):
        percentage_progress = (entries_audited_so_far * 100) / entries_to_audit
        # Print a notice for every 10% of progress.
        if last_percent != percentage_progress:
            # Not the same. Is the percentage just written a multiple of 10?
            this_modulus = percentage_progress % 10
            print_this_progress = False
            if this_modulus == 0:
                # A multiple of 10 percent.
                print_this_progress = True
                last_percent = percentage_progress
            elif percentage_progress > next_expected_percent:
                # Not a multiple of 10%, but it did go past the next expected multiple of 10%.
                # Re-calculate to get and display an exact multiple of 10%.
                next_expected_percentage_for_print = percentage_progress / 10
            if print_this_progress:
                print("%s %% of completion" % (percentage_progress))
        return next_expected_percent, last_percent
    # ---------------------------------------------------------------------------------------------


    this_string = "\r\nFEED NAME AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)

    # Check if this is an aggregate system.
    # Extract the "feed" for all rows from the gateway meter table.
    sql = "SELECT feed_name FROM power_regulators WHERE has_solar=1 GROUP BY feed_name ORDER BY feed_name"
    gateway_meter_row = prepared_act_on_database(FETCH_ALL, sql, [])
    if gateway_meter_row:
        # Check the feed names.
        for feed_name_data in gateway_meter_row:
            this_feed_name = feed_name_data['feed_name']
            if this_feed_name.lower() == "aggregate":
                # Until further clarification, power factor control is inoperative in this configuration.
                this_string = "AGGREGATE CONFIG - NO FEED NAMES TO AUDIT"
                results_file_handle.write(this_string + LINE_TERMINATION)
                return
    else:
        this_string = "GATEWAY METER TABLE EMPTY - NO FEED NAMES TO AUDIT"
        results_file_handle.write(this_string + LINE_TERMINATION)
        return

    # Check "FEED_NAME": are they all NULL, A, B or C?
    fetch_string = "SELECT %s, %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                               INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                               DB_INVERTER_NXO_TABLE)
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())

    next_expected_percent = 0
    last_percent = 0
    entries_audited_so_far = 0
    entries_to_audit = len(fetch_results)

    for row_data in fetch_results:
        inverters_feed_name = row_data[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]
        inverter_ip_address = row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
        if not inverters_feed_name:
            # It's NULL. Feed discovery needs to be run. Set the entry to "NONE".
            this_string = "- FEED NAME NULL: IP " + row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            inverter_feed_name = None
            update_inverters_feed_name_column_by_ip(inverter_feed_name, inverter_ip_address)

        else:
            if (inverters_feed_name == 'A') or (inverters_feed_name == 'B') or (inverters_feed_name == 'C'):
                this_string = "- FEED NAME OK: " + inverters_feed_name
            else:
                # Neither NULL, A, B, or C. Set it to a NULL value.
                this_string = "- FEED NAME INVALID: " + inverters_feed_name + " (FEED DETECT NEEDS TO BE RUN)"
                inverter_feed_name = None
                update_inverters_feed_name_column_by_ip(inverter_feed_name, inverter_ip_address)

        results_file_handle.write(this_string + LINE_TERMINATION)
        # Update progress indicator.
        entries_audited_so_far += 1
        next_expected_percent, last_percent = itca_progress_indicator(next_expected_percent,
                                                                      last_percent,
                                                                      entries_audited_so_far,
                                                                      entries_to_audit)


def battery_tables_db_consistency_check(results_file_handle):
    # 10 items are verified:
    #
    #  1: inverters table BATTERY units: update pcs_unit_id with 0 if NULL
    #  2: inverters table SOLAR units: update pcs_unit_id with 0 if NULL
    #  3: any solar inverters with non-zero pcs_unit_id?
    #  4: inverters table: how many and which battery inverters have pcs_unit_id = 0
    #  5: pcs_unit_id: NULL bmu_id_A or bmu_id_B rows
    #  6: pcs_unit_id: bmu_id_A = 0 entries
    #  7: pcs_unit_id: duplicate bmu ID
    #  8: verify that there are no duplicated IDs in the aess_bmu_data table
    #  9: for each entry in the bmu_data table: which IPs are associated with it
    # 10: is every battery inverter with non-zero pcs_unit_id associaed with an actual BMU DATA ID?

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  1: check NULL pcs_unit_id in inverters table.
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=1 AND %s IS NULL;" % (DB_INVERTER_NXO_TABLE,
                                                                            INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                            INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_null_entries = int(select_results['COUNT(*)'])
    this_string = "\r\nPart 1: BATTERY INVERTERS WITH NULL pcs_unit_id COUNT: "
    if number_of_null_entries == 0:
        # All is good.
        this_string += "0"
    else:
        # Set them to 0
        update_string = "UPDATE %s SET %s=0 WHERE %s IS NULL AND %s=1;" % (DB_INVERTER_NXO_TABLE,
                                                                           INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                           INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                           INVERTERS_TABLE_BATTERY_COLUMN_NAME)
        prepared_act_on_database(EXECUTE, update_string, ())
        this_string += "%s (HAVE BEEN SET TO 0)" % (number_of_null_entries)
    results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  2: inverters table SOLAR units: update pcs_unit_id with 0 if NULL
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=0 AND %s IS NULL;" % (DB_INVERTER_NXO_TABLE,
                                                                            INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                            INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_null_entries = int(select_results['COUNT(*)'])
    this_string = "\r\nPart 2: SOLAR INVERTERS WITH NULL pcs_unit_id COUNT: "
    if number_of_null_entries == 0:
        # All is good.
        this_string += "0"
    else:
        # Set them to 0
        update_string = "UPDATE %s SET %s=0 WHERE %s IS NULL AND %s=0;" % (DB_INVERTER_NXO_TABLE,
                                                                           INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                           INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                           INVERTERS_TABLE_BATTERY_COLUMN_NAME)
        prepared_act_on_database(EXECUTE, update_string, ())
        this_string += "%s (HAVE BEEN SET TO 0)" % (number_of_null_entries)
    results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  3: any solar inverters with non-zero pcs_unit_id?
    select_string = "SELECT COUNT(*) FROM %s WHERE %s=0 AND %s!=0;" % (DB_INVERTER_NXO_TABLE,
                                                                       INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                       INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_null_entries = int(select_results['COUNT(*)'])
    this_string = "\r\nPart 3: SOLAR INVERTERS WITH NON-ZERO pcs_unit_id COUNT: "
    if number_of_null_entries == 0:
        # All is good.
        this_string += "0"
    else:
        # Set them to 0
        update_string = "UPDATE %s SET %s=0 WHERE %s!=0 AND %s=0;" % (DB_INVERTER_NXO_TABLE,
                                                                      INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                      INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                      INVERTERS_TABLE_BATTERY_COLUMN_NAME)
        prepared_act_on_database(EXECUTE, update_string, ())
        this_string += "%s (HAVE BEEN SET TO 0)" % (number_of_null_entries)
    results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  4: inverters table: how many and which battery inverters have pcs_unit_id = 0
    this_string = "\r\nPart 4: BATTERY INVERTERS WITH %s = 0:" % (INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    results_file_handle.write(this_string + LINE_TERMINATION)
    select_string = "SELECT %s FROM %s WHERE %s=1 and %s=0;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                DB_INVERTER_NXO_TABLE,
                                                                INVERTERS_TABLE_BATTERY_COLUMN_NAME,
                                                                INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME)
    select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
    if select_results:
        for each_ip in select_results:
            this_ip = each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            this_string = "- IP %s" % (this_ip)
            results_file_handle.write(this_string + LINE_TERMINATION)
    else:
        # All is good.
        this_string = "- All battery inverters have a non-ZERO ID"
        results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  5: pcs_unit_id: NULL bmu_id_A or bmu_id_B rows
    #
    # select count(*) from pcs_units where bmu_id_A is NULL or bmu_id_B is NULL;
    this_string = "\r\nPart 5: PCS_UNITS AUDIT: NULL BMU_ID_A/B:"
    results_file_handle.write(this_string + LINE_TERMINATION)
    null_items_found = detect_null_bmu_id_a_or_b()
    if null_items_found:
        this_string = "- NULL ITEMS FOUND:"
        results_file_handle.write(this_string + LINE_TERMINATION)
        results_file_handle.write(null_items_found + LINE_TERMINATION)
        this_string = "- NULL ITEMS WERE CORRECTED IN DATABASE"
        results_file_handle.write(this_string + LINE_TERMINATION)
        fix_null_bmu_id_a_or_b_rows()
    else:
        # A length of zero means no NULL entries.
        this_string = "- NULL %s or %s ENTRIES: NONE" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  6: pcs_unit_id: bmu_id_A = 0
    this_string = "\r\nPart 6: PCS_UNITS AUDIT: BMU_ID_A=0 IN %s TABLE:" % (DB_PCS_UNITS_TABLE_NAME)
    results_file_handle.write(this_string + LINE_TERMINATION)
    bmu_id_zero_items_found = detect_zero_bmu_id_a_from_pcs_units_table()
    if bmu_id_zero_items_found:
        this_string = "- ISSUES DETECTED IN %s AND/OR %s TABLES:" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)
        results_file_handle.write(bmu_id_zero_items_found + LINE_TERMINATION)
    else:
        this_string = "- %s = 0 IN %s: NONE" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  7: pcs_unit_id: duplicate bmu ID
    this_string = "\r\nPart 7: PCS_UNITS AUDIT: DUPLICATED BMU ID A/B IN %s TABLE:" % (DB_PCS_UNITS_TABLE_NAME)
    results_file_handle.write(this_string + LINE_TERMINATION)
    bmu_id_zero_items_found = detect_duplicate_bmu_id_from_pcs_units_table()
    if bmu_id_zero_items_found:
        this_string = "- ISSUES DETECTED IN %s AND/OR %s TABLES:" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)
        results_file_handle.write(bmu_id_zero_items_found + LINE_TERMINATION)
    else:
        this_string = "- DUPLICATED %s/%s IN %s: NONE" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  8: verify that there are no duplicated IDs in the aess_bmu_data table
    this_string = "\r\nPart 8: AESS_BMU_DATA AUDIT: DISTINCT BMU_ID:"
    results_file_handle.write(this_string + LINE_TERMINATION)
    duplicate_id_string = detect_duplicate_bmu_id_from_bmu_data_table()
    if duplicate_id_string:
        this_string = "  - CLEANUP WILL BE REQUIRED FOR DUPLICATED BMU ID: "
        results_file_handle.write(this_string + LINE_TERMINATION)
        results_file_handle.write(duplicate_id_string + LINE_TERMINATION)
    else:
        # A length of zero means there are no duplicated bmu_id. Good to go.
        this_string = "- DUPLICATED %s ENTRIES: NONE" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
        results_file_handle.write(this_string + LINE_TERMINATION)

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part  9: for each entry in the pcs_units table: which IPs are associated with it
    this_string = "\r\nPart 9: PCS_UNIT AUDIT: ID -> INVERTER IP ASSOCIATION:"
    results_file_handle.write(this_string + LINE_TERMINATION)
    pcs_units_id_list = db_get_pcs_unit_id_as_list()
    if pcs_units_id_list:
        for this_pcs_unit_id in pcs_units_id_list:
            # select IP_Address from inverters where battery=1 and pcs_unit_id=X;
            select_string = "SELECT %s FROM %s where %s=%s and %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                          DB_INVERTER_NXO_TABLE,
                                                                          INVERTERS_TABLE_BATTERY_COLUMN_NAME, "%s",
                                                                          INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME, "%s")
            select_args = (1, this_pcs_unit_id)
            fetch_results = prepared_act_on_database(FETCH_ALL, select_string, (select_args))
            if fetch_results:
                # Add the list of IP addresses as a comma-separated set, with no comma on 1st entry only.
                first_entry = True
                this_string = "- PCS_UNIT ID %s: ASSOCIATED INVERTERS = " % (this_pcs_unit_id)
                for each_ip in fetch_results:
                    if first_entry:
                        this_string += "%s" % (each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
                        first_entry = False
                    else:
                        this_string += ",%s" % (each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
                results_file_handle.write(this_string + LINE_TERMINATION)
            else:
                this_string = "- PCS UNIT ID %s -> ASSOCIATED WITH INVERTERS = ZERO!!!" % (this_pcs_unit_id)
                results_file_handle.write(this_string + LINE_TERMINATION)

    else:
        this_string = "- PCS_UNITS TABLE AUDIT: PCS_UNITS TABLE IS EMPTY"
        results_file_handle.write(this_string + LINE_TERMINATION)
    # This list name is re-used, so release the memory.
    del pcs_units_id_list[:]

    # ------------------------------------------------------------------------------------------------------------------
    #
    # Part 10: is every battery inverter with non-zero pcs_unit_id associated with an actual PCS_UNITS ID?
    this_string = "\r\nPart 10: BATTERY INVERTERS VALID PCS_UNITS ID AUDIT:"
    results_file_handle.write(this_string + LINE_TERMINATION)
    (ip_list, pcs_units_id_list) = get_not_zero_db_battery_inverters_ip_and_pcs_units_id_list()
    if (len(ip_list) == len(pcs_units_id_list)) and len(ip_list) != 0:
        # Both lists populated in tandem.
        for index, each_ip in enumerate(ip_list):
            this_string = "- BATTERY INVERTER IP %s PCS_UNITS ID %s: " % (ip_list[index], pcs_units_id_list[index])
            select_string = "SELECT %s,%s,%s FROM %s WHERE %s=%s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                                      PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                                      PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME,
                                                                      DB_PCS_UNITS_TABLE_NAME,
                                                                      PCS_UNITS_TABLE_ID_COLUMN_NAME, "%s")
            select_args = (pcs_units_id_list[index], )
            select_results = prepared_act_on_database(FETCH_ALL, select_string, select_args)
            if len(select_results) == 1:
                # This inverter has a pcs_unit_id that indexes an entry in the pcs_units table.
                # Display the BMU IDs (A and B) for giggles.
                this_row = select_results[0]
                bmu_id_A = this_row[PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME]
                bmu_id_B = this_row[PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME]
                this_string += "ASSOCIATED TO TABLE %s -> BMU A/B %s, %s" % (DB_PCS_UNITS_TABLE_NAME, bmu_id_A, bmu_id_B)

            elif len(select_results) == 0:
                # Not good: the pcs_unit_id for this inverter does not index any entry in the pcs_units table.
                this_string += "-> HAS NO ASSOCIATION IN %s TABLE" % (DB_PCS_UNITS_TABLE_NAME)
            else:
                # Morew than 1? What's going on???
                this_string = "CODING ERROR/MAJOR ODDITY: GOT %s ROWS FROM %s BASED ON %s=%s" % (len(select_results),
                                                                                                 DB_PCS_UNITS_TABLE_NAME,
                                                                                                 PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                                                                 pcs_units_id_list[index])
            results_file_handle.write(this_string + LINE_TERMINATION)
    else:
        if len(ip_list) == 0 and len(pcs_units_id_list) == 0:
            # Nothing to audit in this respect.
            this_string = "- THERE ARE NO BATTERY INVERTER ENTRIES WITH NON-ZERO PCS_UNITS ID"
            results_file_handle.write(this_string + LINE_TERMINATION)
        else:
            # ??? It's a coding error, or the impossible happened.
            this_string = "- CODING ERROR: IP_LIST LEN=%s, PCS_UNTIS ID LIKST LEN=%s" % (len(ip_list), len(pcs_units_id_list))
            results_file_handle.write(this_string + LINE_TERMINATION)

    return


def db_get_bmu_id_as_list():
    bmu_id_list = []
    # select bmu_id from aess_bmu_data;
    select_string = "SELECT %s FROM %s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME, DB_AESS_BMU_DATA_TABLE_NAME)
    select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_bmu_id in select_results:
        bmu_id_list.append(int(each_bmu_id[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME]))
    return bmu_id_list


def get_bmu_soc_pair_from_bmu_id_pair(bmu_id_a, bmu_id_b):
    bmu_id_a_soc = 0.00
    bmu_id_b_soc = 0.00
    # select soc from aess_bmu_data where bmu_id=X;
    select_string = "SELECT %s FROM %s WHERE %s=%s" % (AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME,
                                                       DB_AESS_BMU_DATA_TABLE_NAME,
                                                       AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME, "%s")
    # Get the SOC based on the 1st BMU ID:
    select_arg = (bmu_id_a, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_result:
        bmu_id_a_soc = float(select_result[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME])

    # Get the SOC based on the 2nd BMU ID:
    select_arg = (bmu_id_b, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_result:
        bmu_id_b_soc = float(select_result[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME])
    return (bmu_id_a_soc, bmu_id_b_soc)


def db_get_pcs_unit_id_as_list():
    pcs_unit_id_list = []
    # select id from pcs_units;
    select_string = "SELECT %s FROM %s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
    select_results = prepared_act_on_database(FETCH_ALL, select_string, ())
    for each_pcs_unit_id in select_results:
        pcs_unit_id_list.append(each_pcs_unit_id[PCS_UNITS_TABLE_ID_COLUMN_NAME])
    return pcs_unit_id_list


def get_pcs_unit_id_from_bmu_id_pair(bmu_id_one, bmu_id_two):
    pcs_unit_id = 0
    select_string = "SELECT %s FROM %s WHERE (%s=%s AND %s=%s) OR (%s=%s AND %s=%s);" \
                                                                      % (PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                                         DB_PCS_UNITS_TABLE_NAME,
                                                                         PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, "%s",
                                                                         PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, "%s",
                                                                         PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, "%s",
                                                                         PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, "%s")
    select_args = (bmu_id_one, bmu_id_two, bmu_id_two, bmu_id_one)
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    if select_result:
        # Something was retrieved from the database. If the database is "consistent",
        # then only a single pcs unit ID will have been retrieved. Prepare the following
        # string for logging in case more than 1 entry is returned.
        if len(select_result) == 1:
            this_row = select_result[0]
            pcs_unit_id = this_row[PCS_UNITS_TABLE_ID_COLUMN_NAME]
        else:
            # Multiple entries?
            # TODO: SIGNAL BACK TO THE CALLER THAT MORE THAN 1 ENTRY WAS RETURNED
            pass

    else:
        # Zero entries. Yes, this can happen ...
        pass
    return pcs_unit_id


def get_bmu_id_pair_from_pcs_unit_id(pcs_unit_id):
    bmu_id_A = 0
    bmu_id_B = 0
    select_string = "SELECT %s,%s FROM %s WHERE %s=%s;" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                           PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME,
                                                           DB_PCS_UNITS_TABLE_NAME,
                                                           PCS_UNITS_TABLE_ID_COLUMN_NAME, "%s")
    select_args = (pcs_unit_id, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result:
        bmu_id_A = int(select_result[PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME])
        bmu_id_B = int(select_result[PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME])
    return (bmu_id_A, bmu_id_B)


def get_db_pcs_unit_id_from_charger_id(charger_id):
    charger_pcs_unit_id = 0
    # select pcs_unit_id from aess_charger_module where charger_id=1;
    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (AESS_CHARGER_MODULE_PCS_UNIT_ID_COLUMN_NAME,
                                                        DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                        AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    select_args = (charger_id, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result != "NULL": # or NULL
        some_value = select_result[AESS_CHARGER_MODULE_PCS_UNIT_ID_COLUMN_NAME]
        if some_value:
            charger_pcs_unit_id = int(some_value)
    return charger_pcs_unit_id


def db_update_pcs_unit_by_charger_id(some_charger_id, some_pcs_unit_id):
    # update aess_charger_module set pcs_unit_id=5 where charger_id=2;
    update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_AESS_CHARGER_MODULE_TABLE_NAME,
                                                          AESS_CHARGER_MODULE_PCS_UNIT_ID_COLUMN_NAME, "%s",
                                                          AESS_CHARGER_MODULE_CHARGER_ID_COLUMN_NAME, "%s")
    update_args = (some_pcs_unit_id, some_charger_id)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def get_comma_separated_bmu_id_from_non_zero_current(min_threshold):
    # From the BMU data table, return a comma-separated string of BMU ID with non-zero current reading,
    # where a non-zero reading is anything above the min current threshold.
    # select bmu_id from aess_bmu_data where abs(current)>2.0 order by bmu_id;
    comma_separated_bmu_id = ""
    select_string = "SELECT %s FROM %s WHERE abs(%s)>=%s ORDER BY %s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                          DB_AESS_BMU_DATA_TABLE_NAME,
                                                                          AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, "%s",
                                                                          AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
    select_args = (min_threshold, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for this_bmu in select_result:
        comma_separated_bmu_id += ("%s," % this_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME])
    # Remove the very last comma.
    comma_separated_bmu_id = comma_separated_bmu_id[:-1]
    return comma_separated_bmu_id


def get_comma_separated_non_zero_bmu_current_and_id_readings():
    # From the BMU data table, return a comma-separated string of current/bmu id where current is non-zero.
    # select bmu_id,current from aess_bmu_data where current!=0.0;
    bmu_id_and_non_zero_current = ""
    select_string = "SELECT %s,%s FROM %s WHERE %s!=%s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                            AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                                            DB_AESS_BMU_DATA_TABLE_NAME,
                                                            AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, "%s")
    select_args = (0.0, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for this_bmu in select_result:
        bmu_id_and_non_zero_current += ("%s=%s," % (this_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME], this_bmu[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME]))
    # Remove the very last comma.
    bmu_id_and_non_zero_current = bmu_id_and_non_zero_current[:-1]
    return bmu_id_and_non_zero_current


'''
THIS FUNCTION: will be jetisoned in favor of double list rather than string.
'''
def get_comma_separated_bmu_id_and_maxed_soc(max_soc_threshold):
    # From the BMU data table, return a comma-separated string of bmu id/soc
    # where the state-of-charge is above a "max threshold".
    bmu_id_and_maxed_soc = ""
    select_string = "SELECT %s,%s FROM %s WHERE %s>%s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                           AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME,
                                                           DB_AESS_BMU_DATA_TABLE_NAME,
                                                           AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME, "%s")
    select_args = (max_soc_threshold, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for this_bmu in select_result:
        bmu_id_and_maxed_soc += ("%s=%s," % (this_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME], int(this_bmu[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME])))
    bmu_id_and_maxed_soc = bmu_id_and_maxed_soc[:-1] # Remove the very last comma.
    return bmu_id_and_maxed_soc


def get_bmu_id_soc_above_max_threshold(soc_max_threshold):
    bmu_id_max_list = []
    bmu_soc_max_list = []
    select_string = "SELECT %s,%s FROM %s WHERE %s>%s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                           AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME,
                                                           DB_AESS_BMU_DATA_TABLE_NAME,
                                                           AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME, "%s")
    select_args = (soc_max_threshold, )
    select_result = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for this_bmu in select_result:
        bmu_id_max_list.append(this_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME])
        bmu_soc_max_list.append(int(this_bmu[AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME]))
    return (bmu_id_max_list, bmu_soc_max_list)


def detect_duplicate_bmu_id_from_bmu_data_table():
    # Returns comma-separated string of duplicated bmu ID from the aess_bmu_data table.
    duplicate_bmu_id_string = ""
    fetch_string = "SELECT %s, COUNT(*) ccc FROM %s GROUP BY %s HAVING ccc > 1;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                                    DB_AESS_BMU_DATA_TABLE_NAME,
                                                                                    AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())
    if fetch_results:
        # Some duplication - not good. Identify them.
        count_string = fetch_results[0]
        this_count = str(count_string["ccc"])
        duplicate_bmu_id_string += "- DUPLICATED %s ROWS FOUND IN %s, COUNT = %s\r\n" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                                         DB_AESS_BMU_DATA_TABLE_NAME,
                                                                                         this_count)
        for row_data in fetch_results:
            this_duplicate_bmu_id = row_data[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME]
            duplicate_bmu_id_string += "%s," % (this_duplicate_bmu_id)
        # Strip off the last character - i.e., the last comma.
            duplicate_bmu_id_string = duplicate_bmu_id_string[:-1]
    else:
        # A length of zero means there are no duplicated bmu_id. Good to go. Nothing to do. Return an empty string.
        pass

    return duplicate_bmu_id_string


def detect_zero_bmu_id_a_from_pcs_units_table():
    # Get the list of pcs_unit_id where "bmu_id_A is zero".
    zero_bmu_id_a_string = ""
    fetch_string = "SELECT %s FROM %s WHERE %s=%s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                       DB_PCS_UNITS_TABLE_NAME,
                                                       PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, "%s")
    fetch_args = (0, )
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, fetch_args)
    if fetch_results:
        for each_row in fetch_results:
            this_id = each_row[PCS_UNITS_TABLE_ID_COLUMN_NAME]
            zero_bmu_id_a_string += "- ID %s: %s IS ZERO, *** NEEDS FIXING/ASSOCIATION***\r\n" % (this_id, PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME)
    else:
        # No BMU_ID_A = 0. Good.
        pass
    return zero_bmu_id_a_string


def detect_duplicate_bmu_id_from_pcs_units_table():
    duplicate_bmu_id = ""
    fetch_string = "SELECT %s,%s FROM %s;" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                              PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME,
                                              DB_PCS_UNITS_TABLE_NAME)
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())
    if fetch_results:
        bmu_id_list = []
        for each_row in fetch_results:
            # Don't include BMU ID B if it is zero.
            bmu_id_list.append(each_row[PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME])
            if each_row[PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME] != 0:
                bmu_id_list.append(each_row[PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME])

        # Ensure no duplication.
        # any(thelist.count(x) > 1 for x in thelist)
        if any(bmu_id_list.count(xxx) > 1 for xxx in bmu_id_list):
            duplicate_bmu_id += "- %s DUPLICATION DETECTED IN %s TABLE" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, DB_PCS_UNITS_TABLE_NAME)
        else:
            # All were distinct.
            pass
    else:
        # ???
        duplicate_bmu_id = "???FETCH ALL PCS_UNITS TABLE YIELDED ZERO ENTRIES???"

    return duplicate_bmu_id


def detect_null_bmu_id_a_or_b():
    null_items_found_string = ""
    fetch_string = "SELECT COUNT(*) ccc FROM %s WHERE %s IS NULL OR %s IS NULL;" % (DB_PCS_UNITS_TABLE_NAME,
                                                                                    PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                                                    PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME)
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, ())
    if fetch_results:
        # Some NULL entries.
        count_string = fetch_results[0]
        this_count = int(count_string["ccc"])
        if this_count != 0:
            null_items_found_string += "- NULL %s or %s ROWS IN %s, COUNT = %s" % (PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                                                   PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME,
                                                                                   PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                                                   this_count)
        else:
            # Zero count - all is well with this ID A/B.
            pass
    else:
        # A length of zero means no NULL entries.
        pass
    return null_items_found_string


def fix_null_bmu_id_a_or_b_rows():
    update_string = "UPDATE %s SET %s=%s WHERE %s IS NULL" % (DB_PCS_UNITS_TABLE_NAME,
                                                              PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, "%s",
                                                              PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME)
    update_args = (0, )
    prepared_act_on_database(EXECUTE, update_string, update_args)
    update_string = "UPDATE %s SET %s=%s WHERE %s IS NULL" % (DB_PCS_UNITS_TABLE_NAME,
                                                              PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, "%s",
                                                              PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME)
    prepared_act_on_database(EXECUTE, update_string, update_args)
    return


def is_session_title_in_dui_table(some_session_title):
    title_in_table = False
    fetch_string = "SELECT %s FROM %s where %s = %s;" % (DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME,
                                                         DB_DEFERRED_INVERTER_UPGRADE_TABLE,
                                                         DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME, "%s")
    fetch_arg = (some_session_title, )
    fetch_results = prepared_act_on_database(FETCH_ALL, fetch_string, (fetch_arg))
    if fetch_results:
        # The IP is not in the table.
        title_in_table = True
    return title_in_table


def delete_session_title_row_from_dui_table(some_session_title):
    delete_string = "DELETE FROM %s where %s = %s;" % (DB_DEFERRED_INVERTER_UPGRADE_TABLE,
                                                       DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME, "%s")
    delete_arg = (some_session_title, )
    prepared_act_on_database(EXECUTE, delete_string, delete_arg)


def get_number_of_diu_entries():
    number_of_diu_entries = 0
    select_string = "SELECT COUNT(*) FROM %s;" % (DB_DEFERRED_INVERTER_UPGRADE_TABLE)
    select_results = prepared_act_on_database(FETCH_ONE, select_string, ())
    number_of_diu_entries = int(select_results['COUNT(*)'])
    return number_of_diu_entries


def get_diu_data_by_session_title(session_title_as_arg):
    session_title = ""
    upgrade_filename = ""
    comma_separated_arg = ""
    select_string = "SELECT %s,%s,%s FROM %s WHERE %s=%s;" % (DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME,
                                                              DEFERRED_INVERTER_UPGRADE_TABLE_INVERTER_UPGRADE_FILE_COLUMN_NAME,
                                                              DEFERRED_INVERTER_UPGRADE_TABLE_COMMA_SEPARATED_ITEMS_COLUMN_NAME,
                                                              DB_DEFERRED_INVERTER_UPGRADE_TABLE,
                                                              DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME, "%s")
    select_arg = (session_title_as_arg, )
    select_results = prepared_act_on_database(FETCH_ONE, select_string, select_arg)
    if select_results:
        session_title = select_results[DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME]
        upgrade_filename = select_results[DEFERRED_INVERTER_UPGRADE_TABLE_INVERTER_UPGRADE_FILE_COLUMN_NAME]
        comma_separated_arg = select_results[DEFERRED_INVERTER_UPGRADE_TABLE_COMMA_SEPARATED_ITEMS_COLUMN_NAME]
    return (session_title, upgrade_filename, comma_separated_arg)


def insert_into_dui_table(session_title, inverter_upgrade_file, comma_separated_items):
    insert_string = "INSERT INTO %s (%s,%s,%s) VALUES (%s,%s,%s);" % (DB_DEFERRED_INVERTER_UPGRADE_TABLE,
                                                                      DEFERRED_INVERTER_UPGRADE_TABLE_SESSION_TITLE_COLUMN_NAME,
                                                                      DEFERRED_INVERTER_UPGRADE_TABLE_INVERTER_UPGRADE_FILE_COLUMN_NAME,
                                                                      DEFERRED_INVERTER_UPGRADE_TABLE_COMMA_SEPARATED_ITEMS_COLUMN_NAME,
                                                                      "%s", "%s", "%s")
    insert_args = (session_title, inverter_upgrade_file, comma_separated_items)
    prepared_act_on_database(EXECUTE, insert_string, insert_args)
    return


def get_ip_list_from_group_id(some_group_id):
    some_ip_list = []
    select_string = "SELECT %s from %s WHERE %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_GROUP_ID_COLUMN_NAME, "%s")
    select_args = (some_group_id, )
    this_funky_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
    for each_ip in this_funky_list:
        some_ip_list.append(each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
    return some_ip_list


def get_ip_from_serial_number(some_serial_number):
    some_ip_address = ""
    select_string = "SELECT %s from %s WHERE %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME, "%s")
    select_args = (some_serial_number, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result:
        some_ip_address = select_result[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
    return some_ip_address

def get_serial_number_from_ip(some_ip_address):
    some_serial_number = ""
    select_string = "SELECT %s from %s WHERE %s=%s;" % (INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
    select_args = (some_ip_address, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result:
        some_serial_number = select_result[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME]
    return some_serial_number


def get_ip_from_mac_address(some_mac_address):
    some_ip_address = ""
    select_string = "SELECT %s from %s WHERE %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                        DB_INVERTER_NXO_TABLE,
                                                        INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, "%s")
    select_args = (some_mac_address, )
    select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)
    if select_result:
        some_ip_address = select_result[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
    return some_ip_address


def get_inverter_debug_counters_data(inverter_ip_address):
    # Synopsis: get inverter debug counter data based on an IP address:
    #
    #           - construct a TLV-encoded DEBUG-COUNTERS data packet
    #           - open a UDP socket
    #           - send the UDP packet
    #           - wait for a response
    #           - validate and extract data from the response
    #           - close the socket
    #           - call it quits
    #
    coma_separated_debug_counters = ""

    # Construct the GET DEBUG COUNTERS TLV data that will be inserted into the UDP payload.
    # The data consists of the MASTER TLV (mandatory and must be the 1st TLV),
    # followed by the DEBUG COUNTERS TLV.
    number_of_user_tlv = 1
    dummy_short_data = 1234
    packed_data = add_master_tlv(number_of_user_tlv)
    packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_DEBUG_COUNTERS_QUERY_U16_TLV_TYPE, dummy_short_data)

    # OPEN THE SOCKET:
    try:
        fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception: # as error:
        print("OPEN SOCKET FAILED ...")
    else:

        # Send the packet. If the "sendto" facility fails, it is a sign of a
        # lack of system resources. No point trying again in this case.
        try:
            fancy_socks.sendto(packed_data, (inverter_ip_address, SG424_PRIVATE_UDP_PORT))
        except Exception: # as error:
            # This is not good. Nothing to do with the inverter. Bail out of the loop.
            print("SENDTO FAILED - SYSTEM RESOURCE ISSUE ...")

        else:

            # Now wait for a response.
            try:
                fancy_socks.settimeout(3.0)
                private_port_packet = fancy_socks.recvfrom(1024)
            except Exception:
                # Timed out waiting to receive a packet.
                pass
            else:
                # Response received, extract the data from the packet. The raw hex data is in the SG424_data variable.
                SG424_data = []
                (ip_address, ip_address_port, SG424_data) = get_udp_components(private_port_packet)

                # The Master TLV is mandatory, must be at the beginning of the TLV set so validate it.
                (tlv_rank, number_of_user_tlv) = evaluate_master_tlv(SG424_data)
                if tlv_rank == 0:
                    # Master TLV is missing or mal-formed.
                    print("GET DEBUG COUNTERS RESPONSE: MISSING MASTER TLV/MALFORMED TLV PACKET ...")
                else:
                    # There is only 1 user TLV in this response:
                    if number_of_user_tlv == 1:
                        (user_tlv_type, tlv_rank) = get_tlv_type(SG424_data, tlv_rank)

                        if user_tlv_type == SG424_TO_GTW_DEBUG_COUNTERS_U32_ARRAY_TLV_TYPE:
                            # The debug counters is an array of 32-bit unsigned LONGs. The number of LONGs can vary
                            # depending on the inverter software version. The TLV's Length field contains the number
                            # of bytes in the Value portion of the TLV, so the number of counters is the Length / 4.
                            #
                            # The tlv_rank variable is currently indexing the Length field. Extract this length field
                            # from the TLV header as a 16-bit unsigned short value, evaluate the number of LONGs and
                            # extrat that many from the packet.
                            (tlv_length, tlv_rank) = get_tlv_header_length(SG424_data, tlv_rank)
                            number_of_counters = tlv_length / 4
                            for iggy in range(0, number_of_counters):
                                (long_counter, tlv_rank) = get_32bit_unsigned_long_from_hex_data(SG424_data, tlv_rank)
                                coma_separated_debug_counters += "," + str(long_counter)

                        else:
                            print("GET DEBUG COUNTERS RESPONSE: unexp'd TLV type " + str(user_tlv_type))

                    else:
                        print("GET DEBUG COUNTERS RESPONSE: expected 1 TLV but instead got " + str(number_of_user_tlv))

            # Close the socket:
            try:
                fancy_socks.close()
            except Exception: # as error:
                print("SOCKET CLOSE FAILED ...")
            else:
                # Life is good. Knock back another cold one.
                pass
    return coma_separated_debug_counters


#
#
# END OF THAT SET OF AUDITING FUNCTIONS.
#
#
# -------------------------------------------------------------------------------------------------


def get_feed_name_as_string(feed_name_as_integer):
    # Given FEED number 1, 2, 4 or (bit encoding b001, b010 and b100), return the literal FEED name A, B, C or ALL.
    if feed_name_as_integer == int(FEED_A_MULTICAST):
        feed_name_as_string = FEED_A_LITERAL
    elif feed_name_as_integer == int(FEED_B_MULTICAST):
        feed_name_as_string = FEED_B_LITERAL
    elif feed_name_as_integer == int(FEED_C_MULTICAST):
        feed_name_as_string = FEED_C_LITERAL
    else:
        # Assume no other value, so ...
        feed_name_as_string = FEED_ALL_LITERAL
    return feed_name_as_string


def get_feed_name_as_integer(feed_name_as_string):
    # Given the literal FEED name A, B, C or ALL, return the FEED number 1, 2, 4 or 0.
    if feed_name_as_string == FEED_A_LITERAL:
        feed_name_as_integer = int(FEED_A_MULTICAST)
    elif feed_name_as_string == FEED_B_LITERAL:
        feed_name_as_integer = int(FEED_B_MULTICAST)
    elif feed_name_as_string == FEED_C_LITERAL:
        feed_name_as_integer = int(FEED_C_MULTICAST)
    else:
        # Assume no other feed name, so ...
        feed_name_as_integer = int(FEED_ALL_MULTICAST)
    return feed_name_as_integer

def gimme_pcc_value_from_feed_value(feed_as_integer):
    pcc_value = 0
    if feed_as_integer == 0:
        # This is the feed name "ALL", so:
        pcc_value = 0
    else:
        # For all other feed name (A, B, C), pcc is 1.
        pcc_value = 1
    return pcc_value


# Kill a linux process by its name. Return the OS command as evidence of the crime.
def kill_process_by_name(linux_process_name):
    process_kill_command_string = None
    grep_command = "ps -aux | grep %s" % (linux_process_name)
    for line in os.popen(grep_command):
        if linux_process_name in line:
            pgrep_components_list = line.split()
            pid_to_kill = pgrep_components_list[1]
            process_kill_command_string = "sudo kill -9 %s" % (pid_to_kill)
            donut_care_return = os.system(process_kill_command_string)
            break
    return process_kill_command_string

def send_artesyn_charger_cli_command(cli_command, sleep_time):
    # Add PYTHONPATH to environment:
    my_env = os.environ.copy()
    my_env["PYTHONPATH"] = PYTHONPATH

    # Execute the cli command.
    artesyn_cli_command = "%s/ArtesynChargerCLI.py %s" % (ARTESYN_PATH, cli_command)
    subprocess.Popen(artesyn_cli_command, env=my_env, shell=True)

    # Sleep if caller asked.
    if sleep_time > 0:
        time.sleep(sleep_time)
    return
