import socket

from lib.gateway_constants.InverterConstants import *
from lib.logging_setup import *
from lib.model import Model
from gateway_utils import construct_multicast_ip_gen2


class Inverter(Model):
    sql_table = "inverters"
    id_field = "inverters_id"
    logger = setup_logger(__name__)

    MAX_KW_POWER = 0.250

    def __init__(self, **kwargs):
        self.logger = self.__class__.logger
        self.inverters_id = kwargs.get('inverters_id', None)
        self.serial_number = kwargs.get('serialNumber', None)
        self.group_id = kwargs.get('group_id', None)
        self.mac_address = kwargs.get('mac_address', None)
        self.ip_address = kwargs.get('IP_Address', None)
        self.version = kwargs.get('version', None)
        self.label = kwargs.get('label', None)
        self.string_id = kwargs.get('stringId', None)
        self.string_position = kwargs.get('stringPosition', None)
        self.export_status = kwargs.get('export_status', None)
        self.software_profile = kwargs.get('sw_profile', None)
        self.prod_part_number = kwargs.get('prodPartNo', None)
        self.output_status = kwargs.get('output_status', None)
        self.controlled_by_gateway = kwargs.get('controlledByGateway', None)
        self.feed_name = kwargs.get('FEED_NAME', None)
        self.inverter_lock = kwargs.get('inverter_lock', None)
        self.status = kwargs.get('status', None)
        self.comm = kwargs.get('comm', None)
        self.connect = kwargs.get('connect', None)
        self.upgrade_status = kwargs.get('upgrade_status', None)
        self.upgrade_progress = kwargs.get('upgrade_progress', None)
        self.das_boot = kwargs.get('das_boot', None)
        self.resets = kwargs.get('resets', None)
        self.uptime = kwargs.get('uptime', None)
        self.watts = kwargs.get('watts', None)
        self.aess_group_id = kwargs.get('aess_group_id', None)
        self.battery = kwargs.get('battery', None)
        self.last_resp = kwargs.get('last_resp', None)

    def send_unicast(self, tlv):
        try:
            _socket = socket.socket(type=socket.SOCK_DGRAM)
            _socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 3)
            _socket.sendto(tlv, (self.ip_address, SG424_PRIVATE_UDP_PORT))
            _socket.close()
        except Exception as ex:
            self.logger.exception("Multicast Error: ip: %s\n command: %s" %
                                  (repr(self.ip_address), repr(tlv)))

    @classmethod
    def solar_feed_size(cls, feed_name='Aggregate'):
        if feed_name.lower() in ("delta", "aggregate"):
            return cls.count(battery=0) * cls.MAX_KW_POWER
        else:
            return cls.count(battery=0, FEED_NAME=feed_name) * cls.MAX_KW_POWER

    @classmethod
    def get_multicast_ip(cls, inverter_type, feed_name):
        feed_to_multicast = {
            FEED_A_LITERAL: FEED_A_MULTICAST,
            FEED_B_LITERAL: FEED_B_MULTICAST,
            FEED_C_LITERAL: FEED_C_MULTICAST,
            'aggregate': FEED_ALL_MULTICAST
        }

        cls.logger.debug('feed_name = %s' % feed_name)
        if feed_name.lower() == 'aggregate':
            multicast_feed = FEED_ALL_MULTICAST
            multicast_pcc = PCC_ALL_MULTICAST
        else:
            multicast_feed = feed_to_multicast[feed_name.upper()]
            multicast_pcc = PCC_1_MULTICAST

        multicast_ip = construct_multicast_ip_gen2(inverter_type,
                                                   multicast_pcc,
                                                   multicast_feed)

        return multicast_ip


