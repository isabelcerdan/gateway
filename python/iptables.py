#!/usr/share/apparent/.py2-virtualenv/bin/python
# This script is called by the Configurator when a new mac address is white listed.
# White listing a mac address updates the firewall settings to allow for the device
# on the gateway LAN with this mac address to access the internet (ports 80, 443).

from lib.gateway_constants.DBConstants import *
import logging

logging.basicConfig()

from lib.db import prepared_act_on_database
from jinja2 import Environment, FileSystemLoader
import sys
import os


def get_context():

    sql = "SELECT * FROM inverters WHERE version IS NOT NULL"
    inverters = prepared_act_on_database(FETCH_ALL, sql, [])

    sql = "SELECT * FROM mac_address_white"
    mac_address_white = prepared_act_on_database(FETCH_ALL, sql, [])

    return {'inverters': inverters, 'mac_address_white': mac_address_white}


if __name__ == "__main__":
    try:
        path = "/var/lib/apparent"
        template = "iptables.j2"
        outpath = os.path.join(path, "iptables")
        backup = os.path.join(path, "iptables.bak")

        # write new iptables file that includes latest white listed mac addresses
        env = Environment(
            autoescape=False,
            loader=FileSystemLoader(path),
            trim_blocks=False)

        with open(outpath, 'w') as f:
            text = env.get_template(template).render(get_context())
            f.write(text)
            f.write("\n")

        # save a backup of current iptable settings
        os.system('sudo iptables-save > %s' % backup)

        # apply new iptables settings

        #
        # TODO: May need to install iptables-persistent for these changes to persist across reboot.
        #
        os.system('sudo iptables-restore %s' % outpath)

        sys.exit(0)

    except Exception as ex:
        sys.stderr.write('ERROR: %s' % str(ex))
        sys.exit(1)
