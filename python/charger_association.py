#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

# Summary: CHARGER TO PCS ASSOCIATION.
#
#          CHARGER ASSOC ...
#
# Copyright: Apparent Inc. 2018/2019
#
# author: Ed

import time
from datetime import datetime, timedelta
# import traceback
import sys
import os
import subprocess
from lib.logging_setup import *

import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
import logging.handlers
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.ArtesynConstants import *

import gateway_utils as gu
import setproctitle
from lib.db import prepared_act_on_database

from multiprocessing import active_children
from daemon.daemon_monitor import DaemonMonitor
from daemon.daemon import Daemon

from Artesyn.ArtesynChassisRelayDaemon import ArtesynChassisRelayDaemon
from Artesyn.ArtesynControllerDaemon import ArtesynControllerDaemon
from ess.ess_unit import *

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_CHARGER_ASSOC_UNEXP_EXEC_EVENT

# The charger association process drives the chassis controller with a subset of
# the following artesyn CLI commands:
NUMBER_OF_USEFUL_CLI_COMMANDS = 6
INDEX_OF_CLI_COMMAND_POWER_UP = 0
INDEX_OF_CLI_COMMAND_SET_VREF_CHASSIS = 1
INDEX_OF_CLI_COMMAND_SET_VREF = 2
INDEX_OF_CLI_COMMAND_WRITE_PROTECT_MODULE = 3
INDEX_OF_CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL = 4
INDEX_OF_CLI_COMMAND_POWER_DOWN = 5

# These 2 support the charger association process when starting up or finishing,
# as the daemon environment must be setup to interface with the artesyn controller.
CHARGER_ASSOCIATION_PROCESS_START = 1
CHARGER_ASSOCIATION_PROCESS_END = 2

logger = setup_logger('chargerAssoc')

def instantiate_chargerassocscript():
    setproctitle.setproctitle('%s-chargerassoc*' % GATEWAY_PROCESS_PREFIX)
    ChargerAssociationProcess()

class ChargerAssociationProcess(object):

    # The CHARGER_ASSOC process will perform an association between selected chargers and BMUs by
    # commanding a single charger to start charging, then reading the current values in the BMU
    # data table. Once the process is complete, some back-tracking is done to correlate each BMU
    # that was charged on a per-charger basis to the pcs_units table: each BMU is correlated to an
    # "id" in the pcs_units table, and the pcs_units/id is updated in the aess_charger_module table.
    #
    # SPECIAL NOTE:
    #
    # This process requires an interaction with chargers on an individual basis, performed using a
    # special set of CLI commands.
    #
    # In order to interact with chargers on an individual basis, the processing environment first
    # requires that several of the artesyn/ess daemons be disabled during the overall process, the
    # artesyn chassis relay daemon must be killed/re-started, and the artesyn controller is started.
    # When the overall process is complete, the system is restored: the artesyn/ess daemons that were
    # initially disabled are re-enabled, the artesyn controller is disabled/stopped, and the chassis
    # relay is again killed and restarted.

    def __init__(self, _=None):

        self.charger_assoc_pid = 0
        self.charger_assoc_command = None
        self.current_date_time = "00-00-00 00:00:00"
        self.current_utc = 0
        self.start_utc = 0
        self.results_file_handle = 0
        self.process_was_canceled = False
        self.bmu_maximum_soc = 0
        self.max_discharge_minutes = 0
        self.number_charger_units_to_associate = 0
        self.charger_units_completed = 0
        self.charger_association_type = CHARGER_ASSOC_UNKNOWN_ASSOCIATE_TYPE
        self.charger_id = 0
        self.charger_module = 0
        self.all_chargers_involved = False
        self.cli_command = None
        self.cli_parameter = None
        self.cli_command_list = []

        # These lists support control of activating the chassis and chargers
        self.chassis_id_list = []
        self.charger_id_list = []
        self.charger_module_list = []

        # These 2 lists populated in tandem, for all chargers that are to be associated.
        # They are used for the final evaluation and auditing when the process is done.
        self.final_charger_id_to_associate_list = []
        self.final_comma_separated_bmu_id_list = []

        # -----------------------------------------------------------------------------------------
        # ArtesynChassisRelayDaemon not included here; it is stopped/killed via os interaction.
        some_daemon_class = ArtesynControllerDaemon
        daemon_monitor = DaemonMonitor.find_or_create_all(some_daemon_class)
        daemon_monitor[0].save(enabled=True)
        self.daemon_monitor = daemon_monitor[0]
        # -----------------------------------------------------------------------------------------

        # --------------------------
        self.run_charger_association()
        # --------------------------

    # LOCAL FUNCTIONS


    #
    #
    # Utility to open the results file for the charger association dae,mon,
    # and write a terse message that a new process has started.
    #
    def is_open_charger_association_results_file(self):
        open_ok = True
        try:
            self.results_file_handle = open(CHARGER_ASSOCIATION_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception:  # as err_string:
            # Think long and hard about an alarm. Well, not too long, not too hard.
            open_ok = False
        return open_ok

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)


    def log_stuff(self, crazy_string):
        # In case we decide to control what is logged and where.
        hms_current_time = time.strftime("%H:%M:%S")
        self.results_file_handle.write(hms_current_time + " " + crazy_string + LINE_TERMINATION)
        logger.info(crazy_string)


    def is_charger_assoc_database_table_start_ok(self):
        start_ok = True
        # The charger start command must be "start your engines".
        self.charger_assoc_command = gu.get_charger_assoc_command()
        if self.charger_assoc_command == CHARGER_ASSOC_START_COMMAND:
            self.charger_association_type = gu.get_charger_association_type()
            if (self.charger_association_type != CHARGER_ASSOC_FULL_ASSOCIATE_TYPE) \
            and (self.charger_association_type!= CHARGER_ASSOC_SELECT_ASSOCIATE_TYPE):
                self.log_stuff("*** CHARGER_ASSOC START FAILED: INVALID ASSOCIATION TYPE = %s ***" % (self.charger_association_type))
                start_ok = False
        else:
            self.log_stuff("*** CHARGER_ASSOC START FAILED: CONTROL COMMAND (%s) NOT %s ***" % (self.charger_assoc_command, CHARGER_ASSOC_START_COMMAND))
            start_ok = False
        return start_ok

    def is_any_impediments_to_charger_assoc(self):
        # Ensure there are no impediments to allow the CHARGER_ASSOC to proceed.
        # Unlike the PUI ASSOCIATION, this one bails without waiting.
        impediments_exist = False
        impediment_status_string = gu.get_charger_assoc_impediment_status()
        if impediment_status_string:
            # Something is preventing PUI_ASSOC from becoming active. Set the status in the database.
            gu.update_pui_assoc_status(CHARGER_ASSOC_START_FAILED_STATUS)
            gu.update_charger_assoc_status(CHARGER_ASSOC_START_FAILED_STATUS)
            # There are no impediments. Proceed with PUI_ASSOC.
            self.log_stuff("*** CHARGER_ASSOC: IMPEDIMENTS PREVENT CHARGER ASOC: %s ***" % (impediment_status_string))
            impediments_exist = True
        return impediments_exist

    def get_bmu_id_pair(self, associated_bmu_id):
        bmu_id_A = 0
        bmu_id_B = 0

        if associated_bmu_id:
            # At least 1 or more BMU IDs are indicated. There should be either 1 or 2.
            this_bmu_id_list = associated_bmu_id.split(",")
            comma_separated_length = len(this_bmu_id_list)
            if comma_separated_length == 1:
                bmu_id_A = int(this_bmu_id_list[0])
            elif comma_separated_length == 2:
                bmu_id_A = int(this_bmu_id_list[0])
                bmu_id_B = int(this_bmu_id_list[1])
            else:
                # Nope. Should not have detected more than 2 BMUs on any charger.
                pass
        else:
            # The comma-separated list does not contain anything.
            pass
        return (bmu_id_A, bmu_id_B)

    def charger_assoc_final_assessment(self):
        # The charger association process is entering the final assessment and resolution phase.
        # Chargers have been associated to 0, 1 or 2 BMU IDs, now they need to be correlated and
        # associated to PCS UNIT IDs.
        # Provide overall basic stats regarding the chargers, BMUs, PCS UNITS.

        if len(self.final_charger_id_to_associate_list) == len(self.final_comma_separated_bmu_id_list):
            if len(self.final_charger_id_to_associate_list) != 0:
                self.log_stuff("ASSOCIATION RESULTS OF SELECTED CHARGERS BASED ON CHASSIS/MODULE/CHARGER_ID/DB_PCS_UNIT_ID:")
                for this_charger_id, each_bmu_id_string in zip(self.final_charger_id_to_associate_list, self.final_comma_separated_bmu_id_list):
                    (this_module_number, this_chassis_id) = gu.get_module_number_and_chassis_id_from_charger_id(this_charger_id)

                    if this_charger_id != 0:
                        # For this charger: get the bmu ID A/B pair from the comma-separated BMU ID list.
                        # Then associate that A/B pair to an id in the pcs unit table.
                        (bmu_id_one, bmu_id_two) = self.get_bmu_id_pair(each_bmu_id_string)
                        associated_pcs_unit_id = gu.get_pcs_unit_id_from_bmu_id_pair(bmu_id_one, bmu_id_two)

                        # Get this charger's PCS UNIT ID from the charger table.
                        db_charger_pcs_unit_id = gu.get_db_pcs_unit_id_from_charger_id(this_charger_id)
                        # Compare the PCS UNIT ID for this charger in the charger to the corelated PCS UNIT ID.
                        # There are several possibilities:
                        #
                        # - if the charger table has a non-zero PCS UNIT ID
                        #   then: if correlated PCS UNIT ID is ZERO
                        #         then: this indicates a failure to interact with a charger, no database change
                        #         else: - if association process evaluated the same PCS UNIT ID
                        #                 then no change/no database update required
                        #                 else: update the PCS NIT ID the charger table
                        # - else: if the association process evaluated a non-zero PCS UNIT ID
                        #         then: update the PCS NIT ID the charger table
                        #         else: this indicates a continued failure to interact with a charger, no database change
                        this_string = ""
                        if db_charger_pcs_unit_id != 0:
                            if associated_pcs_unit_id != 0:
                                if db_charger_pcs_unit_id == associated_pcs_unit_id:
                                    # Non-zero and the same. Database update not required.
                                    this_string = "CHARGER %s/%s/%s/%s = ACTIVATED BMU ID %s/%s/*SAME* PCS UNIT ID %s - OPERATIONAL" \
                                                % (this_chassis_id, this_module_number, this_charger_id, db_charger_pcs_unit_id, bmu_id_one, bmu_id_two, associated_pcs_unit_id)
                                else:
                                    # Non-zero and they differ. Update the database.
                                    gu.db_update_pcs_unit_by_charger_id(this_charger_id, associated_pcs_unit_id)
                                    this_string = "CHARGER %s/%s/%s/*%s* = ACTIVATED BMU ID %s/%s/*CHANGED* PCS UNIT ID %s - OPERATIONAL" \
                                                % (this_chassis_id, this_module_number, this_charger_id, db_charger_pcs_unit_id, bmu_id_one, bmu_id_two, associated_pcs_unit_id)
                                # Either way, the association on this charger was successful.
                                gu.update_aess_charger_module_assoc_status(this_charger_id, AESS_CHARGER_MODULE_ASSOC_SUCCESS_STATUS)
                                gu.update_aess_charger_module_functionality(this_charger_id, AESS_CHARGER_MODULE_FUNCTIONALITY_OPERATIONAL)
                            else:
                                # Most likely a chassis/charger failure during interaction with the chassis.
                                # The PCS UNIT ID in the charger table is left un-molested.
                                gu.update_aess_charger_module_assoc_status(this_charger_id, AESS_CHARGER_MODULE_ASSOC_FAILED_STATUS)
                                this_string = "CHARGER %s/%s/%s/%s = ASSOCIATION FAILED BASED ON BMU_ID %s - FAULTY" \
                                            % (this_chassis_id, this_module_number, this_charger_id, db_charger_pcs_unit_id, each_bmu_id_string)
                                if not gu.is_charger_functionality_unequipped(this_charger_id):
                                    gu.update_aess_charger_module_functionality(this_charger_id, AESS_CHARGER_MODULE_FUNCTIONALITY_FAULTY)
                        else:
                            if associated_pcs_unit_id != 0:
                                # Update the database.
                                gu.db_update_pcs_unit_by_charger_id(this_charger_id, associated_pcs_unit_id)
                                gu.update_aess_charger_module_assoc_status(this_charger_id, AESS_CHARGER_MODULE_ASSOC_SUCCESS_STATUS)
                                this_string = "CHARGER %s/%s/%s *FIRST TIME* ACTIVATED BMU ID %s/%s/PCS UNIT ID %s - OPERATIONAL" \
                                                        % (this_chassis_id, this_module_number, this_charger_id, bmu_id_one, bmu_id_two, associated_pcs_unit_id)
                                gu.update_aess_charger_module_functionality(this_charger_id, AESS_CHARGER_MODULE_FUNCTIONALITY_OPERATIONAL)
                            else:
                                # Most likely a chassis/charger failure during interaction with the chassis. Possibly unequipped.
                                gu.update_aess_charger_module_assoc_status(this_charger_id, AESS_CHARGER_MODULE_ASSOC_FAILED_STATUS)
                                this_string = "CHARGER %s/%s/%s/%s = ASSOCIATION FAILED BASED ON BMU_ID %s - FAULTY TOWERS" \
                                            % (this_chassis_id, this_module_number, this_charger_id, db_charger_pcs_unit_id, each_bmu_id_string)

                                if not gu.is_charger_functionality_unequipped(this_charger_id):
                                    gu.update_aess_charger_module_functionality(this_charger_id, AESS_CHARGER_MODULE_FUNCTIONALITY_FAULTY)

                        self.log_stuff(this_string)
                    else:
                        # This charger must have been excluded due to multiple chargers causing the same BMU excitation.
                        pass
            else:
                # Should not have come here: this process would have bailed when first launched.
                self.raise_and_clear_alarm(ALARM_ID_CHARGER_ASSOC_UNEXP_EXEC_EVENT)
                self.log_stuff("THERE WERE ZERO CHARGERS TO ASSOCIATE")
        else:
            # Both should have the same length since they are populated in tandem.
            # In other words: the impossible happened! Or maybe a coding error error!
            # This merits an alarm.
            self.raise_and_clear_alarm(ALARM_ID_CHARGER_ASSOC_UNEXP_EXEC_EVENT)
            self.log_stuff("FINAL CHARGER ID/BMU ID LISTS OF UNEVEN LENGTH %s/%s" % (len(self.final_charger_id_to_associate_list),
                                                                                     len(self.final_comma_separated_bmu_id_list)))

    def associate_one_charger_unit(self):

        # This is the fundamental function to associate a single charger: set the output current to 3amps,
        # wait a bit to allow the electrons to get all giddy, get the current reading from the BMU data table,
        # and then reset the charger's output current to zero. Set the charger's status to active upon entry,
        # and to "done" upon leaving. The final charger status will be update with good/bad when the overall
        # results are assessed.
        gu.update_aess_charger_module_assoc_status(self.charger_id, AESS_CHARGER_MODULE_ASSOC_ACTIVE_STATUS)
        this_string = "ASSOCIATE CHARGER %s/%s/%s: " % (self.chassis_id, self.charger_module, self.charger_id)

        # Send a "-set-vref" command with 3 parameters: chassis ID, module number, 56.8volts.
        self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_SET_VREF]
        self.cli_parameter = "%s %s 56.8" % (self.chassis_id, self.charger_module)
        full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
        this_string += "CLI(%s) " % (full_cli_command)
        gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

        # Send a "--set-output-current-level" command with 3 parameters: chassis ID, module number, 3 amps.
        self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL]
        self.cli_parameter = "%s %s 3.0" % (self.chassis_id, self.charger_module)
        full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
        this_string += "CLI(%s) " % (full_cli_command)
        gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

        # ---------------------------------------------------------------------
        # From the BMU data table, get the list of BMU IDs based on non-zero current readings.
        comma_separated_bmu_id = gu.get_comma_separated_bmu_id_from_non_zero_current(MIN_BMU_CURRENT_THRESHOLD)
        this_string += "(RESULT=BMU ID "
        if comma_separated_bmu_id:
            # Check: what if there were more than 2 BMUs? This is not correct. Sometimes things go wonky.
            # split
            this_active_list = comma_separated_bmu_id.split(",")
            if len(this_active_list) > MAX_BMU_PER_CHARGER:
                # Sometimes things go wonky. At one time, observation included a number of BMUs with
                # very low current readings, which is considered to be somewhat unexpected . This may
                # be the case. For now, check but display only. In the future, it may be desirable to
                # modify the above function to extract active current readings whose absolute values are
                # outside som range, something like +/-1amp.
                this_string += "WONKY:%s)" % (comma_separated_bmu_id)
                comma_separated_bmu_id = None
                non_zero_bmu_current_and_id = gu.get_comma_separated_non_zero_bmu_current_and_id_readings()
                this_string += "WITH NON-ZERO READINGS %s)" % (non_zero_bmu_current_and_id)
            else:
                this_string += "%s)" % (comma_separated_bmu_id)
        else:
            this_string += "NONE)"

        # Save the comma-separated BMU ID for this particular charger ID.
        # These 2 lists are populated in tandem.
        self.final_charger_id_to_associate_list.append(self.charger_id)
        self.final_comma_separated_bmu_id_list.append(comma_separated_bmu_id)
        # ---------------------------------------------------------------------

        # Send a "--set-output-current-level" command with 3 parameters: chassis ID, charger ID, 0 amps:
        self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL]
        self.cli_parameter = "%s %s 0.0" % (self.chassis_id, self.charger_module)
        full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
        this_string += " CLI(%s)" % (full_cli_command)
        gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

        # Done. Log all that occured with this 1 charger.
        self.log_stuff(this_string)

        # Another one bites the dust ...
        self.charger_units_completed += 1
        gu.update_charger_assoc_counter_column(CHARGER_ASSOCIATION_CONTROL_TABLE_CHARGER_UNITS_COMPLETED_COLUMN_NAME, self.charger_units_completed)
        gu.update_aess_charger_module_assoc_status(self.charger_id, AESS_CHARGER_MODULE_ASSOC_DONE_STATUS)

        return

    def set_zero_output_current_on_this_chassis(self):
        # For each charger on this chassis, disable write-protect and set output current to 0amps,
        # identified by charger module number. For logging purpose, the charger ID is also gotten.
        (charger_module_num_list, charger_id_list) = gu.get_charger_id_and_module_num_list_by_chassis_id(self.chassis_id)
        if len(charger_module_num_list) == 0:
            # Odd - no other charger on this chassis?
            self.log_stuff("THERE ARE NO CHARGERS ON THIS CHASSIS TO SET ZERO OUTPUT CURRENT")
        else:
            # NOTE: sending write_protect_module_all to the chassis did not work. Each charger unit required
            #       its own individual write_protect_module, those nasty selfish buggers ...
            self.log_stuff("CHASSIS %s: WRITE PROTECT/DISABLE + OUTPUT CURRENT/0.0 ALL CHARGERS BY MOD_NUM/CHARGER_ID" % (self.chassis_id))
            for module_num, charger_id in zip(charger_module_num_list, charger_id_list):
                # write protect module "disable":
                self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_WRITE_PROTECT_MODULE]
                self.cli_parameter = "%s %s disable" % (self.chassis_id, module_num)
                full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
                gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

                this_string = "CHARGER %s/%s/%s: CLI(%s %s) " % (self.chassis_id, module_num, charger_id, self.cli_command, self.cli_parameter)
                # set output current "0.0":
                self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL]
                self.cli_parameter = "%s %s 0.0" % (self.chassis_id, module_num)
                full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
                gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

                this_string += " CLI(%s %s)" % (self.cli_command, self.cli_parameter)
                self.log_stuff(this_string)

        del charger_id_list[:]
        del charger_module_num_list[:]

        return

    def prepare_artesyn_processes_for_charger_association(self, association_start_or_end):

        # Preamble:
        #
        # The association process requires that during the interaction with the chargers,
        # the following 4 daemon processes be stopped on a per-ESS, namely:
        #     - artesyncontrollerfactoryd
        #     - dischargercontrollerfactoryd
        #     - essbmulimitsd
        #     - esscontrollerd
        #
        # and that the artesynchassisrelayd daemon be re-started. Then the chargers can be
        # individually commanded by the set of CLI artesyn charger commands.
        #
        # When the association process is complete, "cleanup" those same processes.

        number_of_ess_units = gu.get_database_table_row_count(DB_ESS_UNITS_TABLE)
        if association_start_or_end == CHARGER_ASSOCIATION_PROCESS_START:
            # The association process is starting. Setup the daemons for charger interaction.

            for iggy in range (0, number_of_ess_units):
                # Kill the artesyn chassis relay daemons on a per-ESS basis. They will be immediately re-launched,
                # and a new queuing environment setup will be setup.
                artesyn_chassis_relay_name_by_ess = "%s-%s" % (ARTESYN_CHASSIS_RELAY_DAEMON_NAME, (iggy+1))
                os_command_string = gu.kill_process_by_name(artesyn_chassis_relay_name_by_ess)
                if os_command_string:
                    self.log_stuff("PROCESS NAME %s KILLED BY OS COMMAND: %s" % (artesyn_chassis_relay_name_by_ess, os_command_string))
                else:
                    # Something not right here ...
                    self.log_stuff("PROCESS NAME %s KILLED *FAILED* -> NO PROCESS BY THAT NAME" % (artesyn_chassis_relay_name_by_ess))

                # The following daemons should not be running during the association process.
                for index, this_daemon_name in enumerate(DAEMONS_FOR_ASSOCIATION_CONTROL):
                    daemon_name_by_ess = "%s-%s" % (this_daemon_name, (iggy+1))
                    self.log_stuff("CLEAR ENABLED BIT FOR %s ..." % (daemon_name_by_ess))
                    gu.update_daemon_control_enabled_by_name(daemon_name_by_ess, 0)

            # Launching the ArtesynControllerDaemon for charger interaction.
            self.log_stuff(crazy_string = "START ArtesynControllerDaemon")
            gu.update_daemon_control_enabled_by_name("artesyncontrollerd", 1)
            d = self.daemon_monitor
            d.start()
            active_children()

            # Give a reasonable amount of time for both daemons to be fully launched and running.
            time.sleep(10)

            if self.charger_association_type == CHARGER_ASSOC_FULL_ASSOCIATE_TYPE:
                self.log_stuff("ENTERING ACTIVE *FULL* ASSOCIATION PHASE")
            else:
                self.log_stuff("ENTERING ACTIVE *SELECT* ASSOCIATION PHASE")

        else:
            # The association process is done. Process cleanup:
            # #
            # - disable/stop the artesyn controller daemon
            # - re-enable the processes that were disabled at the begining of the process
            # - kill the artesyn chassis relay daemon

            self.log_stuff("DISABLE/STOP ArtesynControllerDaemon")
            gu.update_daemon_control_enabled_by_name("artesyncontrollerd", 0)
            d = self.daemon_monitor
            d.stop()

            for iggy in range(0, number_of_ess_units):
                for index, this_daemon_name in enumerate(DAEMONS_FOR_ASSOCIATION_CONTROL):
                    daemon_name_by_ess = "%s-%s" % (this_daemon_name, (iggy+1))
                    self.log_stuff("SET ENABLED FOR %s ..." % (daemon_name_by_ess))
                    gu.update_daemon_control_enabled_by_name(daemon_name_by_ess, 1)

                artesyn_chassis_relay_name_by_ess = "%s-%s" % (ARTESYN_CHASSIS_RELAY_DAEMON_NAME, (iggy+1))
                os_command_string = gu.kill_process_by_name(artesyn_chassis_relay_name_by_ess)
                if os_command_string:
                    self.log_stuff("PROCESS NAME %s KILLED BY OS COMMAND: %s" % (artesyn_chassis_relay_name_by_ess, os_command_string))
                else:
                    # Something not right here ...
                    self.log_stuff("PROCESS NAME %s KILLED *FAILED* -> NO PROCESS BY THAT NAME" % (artesyn_chassis_relay_name_by_ess))

            # Give a reasonable amount of time for all daemons to restore themselves.
            time.sleep(10)
        return

    def verify_all_bmu_id_association(self):
        # All chargers were involved in the association process, so the expectation
        # is that all BMUs were excited to have been invited to the party. This function
        # logs those BMUs that did not come to the party.
        #
        # Create a list of all BMU IDs from the database, and remove those BMUs from that
        # list that were in the final comma-separated list. When done, the total BMU ID list
        # should be empty. Or not.
        total_bmu_id_list = gu.db_get_bmu_id_as_list()
        for each_bmu_id_string in self.final_comma_separated_bmu_id_list:
            # Each string is a comma-separated list of BMU ID. Split and check
            # membership in the total BMU ID list.
            if each_bmu_id_string:
                # This BMU was associated to a charger. Remove it from the total list.
                this_bmu_id_list = each_bmu_id_string.split(",")
                for each_bmu_id in this_bmu_id_list:
                    total_bmu_id_list.remove(int(each_bmu_id))
            else:
                # This charger entry did not have any associated BMU.
                pass

        if len(total_bmu_id_list) == 0:
            self.log_stuff("ALL CHARGERS WERE INVOLVED, ALL BMUs WERE ASSOCIATED")
        else:
            this_string = "ALL CHARGERS WERE INVOLVED, THESE BMUs NOT ASSOCIATED:"
            for each_missing_bmu in total_bmu_id_list:
                this_string += " %s" % (each_missing_bmu)
                self.log_stuff(this_string)

        return

    def is_duplicate_associations_detected(self):
        bmu_id_duplications_detected = False
        bmu_id_duplicated_list = []
        bmu_fount_once_list = []

        # If a BMU is found to have been associated to more than 1 charger ID,
        # then reject/remove that association.
        for this_charger_id, each_bmu_id_string in zip(self.final_charger_id_to_associate_list, self.final_comma_separated_bmu_id_list):
            if each_bmu_id_string:
                this_bmu_id_list = each_bmu_id_string.split(",")
                for each_bmu_id in this_bmu_id_list:
                    if each_bmu_id in bmu_fount_once_list:
                        # Add this BMU ID to the duplicated list, it will be used to cycle through the
                        # charger list to determine which will be removed for final assessment.
                        self.log_stuff("BMU ID %s (CHARGER ID %s) ASSOCIATED AT LEAST A SECOND TIME" % (each_bmu_id, this_charger_id))
                        bmu_id_duplicated_list.append(each_bmu_id)
                    else:
                        # Not found. Add it to the duplicate list, to make sure it's not found subsequently.
                        bmu_fount_once_list.append(each_bmu_id)
        if len(bmu_id_duplicated_list) == 0:
            self.log_stuff("ALL BMU->CHARGER ASSOCIATIONS WERE DISTINCT")
        else:
            # There was duplication of BMU IDs across 1 or more chargers.
            # Exclude such entries from the final lists where BMU IDs were in the duplicated list.
            # First: identify them, if any.
            bmu_id_duplications_detected = True
            ranks_to_exclude_list = []
            self.log_stuff("DETECTED MULTIPLE BMU ASSOCIATIONS, THE FOLLOWINGS CHARGERS ARE EXCLUDED:")
            for iggy, this_duplicated_bmu_id in enumerate(bmu_id_duplicated_list):
                # Go through the final list searching for any entry with this BMU ID.
                for pop, each_bmu_id_string in enumerate(self.final_comma_separated_bmu_id_list):
                    this_bmu_id_list = each_bmu_id_string.split(",")
                    for each_bmu_id in this_bmu_id_list:
                        if each_bmu_id == this_duplicated_bmu_id:
                            ranks_to_exclude_list.append(pop)

            # Second: if any, exclude by setting the charger ID to 0.
            for iggy in range (0, len(ranks_to_exclude_list)):
                exclude_rank = ranks_to_exclude_list[iggy]
                this_charger_id = self.final_charger_id_to_associate_list[exclude_rank]
                this_bmu_id_string = self.final_comma_separated_bmu_id_list[exclude_rank]
                self.log_stuff("EXCLUDE CHARGER/BMU ID: %s/%s FROM FINAL LISTS [RANK %s]" % (this_charger_id, this_bmu_id_string, exclude_rank))
                self.final_charger_id_to_associate_list[exclude_rank] = 0
                self.final_comma_separated_bmu_id_list[exclude_rank] = ""
        return bmu_id_duplications_detected

    def is_setup_association_for_all_chargers_ok(self):
        setup_ok = True
        self.chassis_id_list = gu.db_get_chassis_id_list()
        if len(self.chassis_id_list) > 0:
            for each_chassis_id in self.chassis_id_list:
                # Each chassis: up to 8 chargers. Or 8 "modules".
                for module_number in range (1, 9):
                    charger_module_row = gu.db_get_charger_module_row_by_module_num_and_chassis_id(module_number, each_chassis_id)
                    if charger_module_row:
                        # NO: self.log_stuff("CHARGER MODULE: ROW EXISTS BASED ON MODULE NUM %s/CHASSIS %s/PCS UNIT %s" % (module_number,
                        #                                                                                               each_chassis_id,
                        #                                                                                               charger_module_row[AESS_CHARGER_MODULE_PCS_UNIT_ID_COLUMN_NAME]))
                        pass
                    else:
                        # Missing entry in aess charger module table. Insert it on the basis of module number/chassis ID.
                        self.log_stuff("CHARGER MODULE: NEW ROW INSERTED, MOD_NUM/CHASSIS/PCS UNIT %s/%s/0" % (module_number, each_chassis_id))
                        gu.insert_aess_charger_module_by_module_number_chassis_id(module_number, each_chassis_id)
                # for iggy ...
            # for each_chassis ...
        else:
            self.log_stuff("NO CHASSIS DEVICES IN SYSTEM - %s TABLE WAS EMPTY" % (DB_AESS_CHARGER_CHASSIS_TABLE_NAME))
            setup_ok = False

        return setup_ok

    def is_charger_association_good_to_go(self):
        #  1: get and store the pid in the control table
        self.charger_assoc_pid = os.getpid()
        if not gu.set_database_process_pid(DB_CHARGER_ASSOCIATION_CONTROL_TABLE, CHARGER_ASSOCIATION_CONTROL_TABLE_PID_COLUMN_NAME, self.charger_assoc_pid):
            crazy_string = "*** CHARGER_ASSOC START FAILED: COULD NOT SET DB CHARGER ASSOC PID ***"
            self.results_file_handle.write(crazy_string + LINE_TERMINATION)
            logger.critical(crazy_string)
            return False

        #  2: set the timestamp in the control table
        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.current_utc = int(time.time())
        self.start_utc = self.current_utc
        gu.update_charger_assoc_timestamp(self.current_date_time)

        #  3: check database tables for sanity
        if not self.is_charger_assoc_database_table_start_ok():
            # Reason for insanity will have been logged and/or alarmed.
            return False

        #  4: verify that the pcs_units and aess_bmu_data tables are consistent.
        bad_things_detected_string = gu.verify_sanity_pcs_unit_id_and_aess_bmu_data()
        if bad_things_detected_string:
            self.log_stuff("***CHARGER ASSOCIATION DATABASE ISSUES: %s" % (bad_things_detected_string))
            return False

        #  5: ensure there are no impediments to allow the PUI_ASSOC to proceed
        if self.is_any_impediments_to_charger_assoc():
            # Several causes can impede the association process, and whatever they may be, this process has timed out
            # waiting for them to pass. The status will have been updated in the database.
            return False

        #  6: is there at least 1 charger to associate?
        if self.charger_association_type == CHARGER_ASSOC_SELECT_ASSOCIATE_TYPE:
            self.number_charger_units_to_associate = gu.get_number_charger_units_to_associate()
            if self.number_charger_units_to_associate == 0:
                self.log_stuff("***CHARGER ASSOCIATION NOT STARTED: ZERO CHARGER UNITS BY SELECT COMMAND")
                gu.update_charger_assoc_command(CHARGER_ASSOC_NO_COMMAND_COMMAND)
                gu.update_charger_assoc_status(CHARGER_ASSOC_START_FAILED_STATUS)
                return False
        else:
            # Perform association for ALL chargers in the system. For this, the charger module table must
            # be fully populated based on the charger chassis table.
            if not self.is_setup_association_for_all_chargers_ok():
                self.log_stuff("***CHARGER ASSOCIATION NOT STARTED: SETUP FAILED FOR ALL ASSOCIATION")
                gu.update_charger_assoc_command(CHARGER_ASSOC_NO_COMMAND_COMMAND)
                gu.update_charger_assoc_status(CHARGER_ASSOC_START_FAILED_STATUS)
                return False
            # Set the associate column to 1.
            gu.update_associate_all_aess_charger_module(1)
            self.number_charger_units_to_associate = gu.get_number_charger_units_to_associate()

        #  7: set the association status to "waiting" for all chargers to be associated.
        #     All statuses are 1st initialzed with "inactive".
        gu.init_all_assoc_status_inactive()
        gu.set_selected_chargers_with_waiting_status()

        #  8: setup max SOC threshold and max discharge time.
        (self.bmu_maximum_soc, self.max_discharge_minutes) = gu.get_charger_association_controller_limits()

        # Did we get here? Then we are good to go ...
        return True

    def perform_mass_bmu_discharge(self):
        #  Verify that all BMUs have a SOC that is below a certain max threshold. If not, the association results will
        #  be compromised.
        #
        #  Typically, an SOC of 97% or less will ensure successful association results. If 1 or more BMUs
        #  has an SOC greater than that max threshold, the process enters into a period of inverter discharge
        #  to bring the SOC down on all BMUs. If discharge is necessary, all inverters are commanded to discharge,
        #  as the assumption is: the inverter->pcs association itself may not have been performed.
        discharge_period_minutes = 0

        bmu_id_and_maxed_soc_string = gu.get_comma_separated_bmu_id_and_maxed_soc(self.bmu_maximum_soc)
        self.log_stuff("BMU SOC ABOVE MAX THRESHOLD - ENTER FORCED DISCHARGE PERIOD (MAX=%sMINUTES)" % (self.max_discharge_minutes))
        donut_care = gu.is_pack_and_send_mcast_gateway_control(SG424_MULTICAST_GENERIC_BATTERY, 0)

        while bmu_id_and_maxed_soc_string:
            # Command all inverters to discharge by taking them out of gateway control.
            # Check again in a few minutes. This stage typically takes about 10-15 minutes.
            self.log_stuff("BMU ID SOC MAX'D: %s" % (bmu_id_and_maxed_soc_string))
            gu.update_charger_assoc_status(CHARGER_ASSOC_SOC_DISCHARGING_STATUS)
            self.log_stuff("WILL CHECK AGAIN IN %s MINUTES ..." % INVERTER_DISCHARGE_WAIT_PERIOD_MINUTES)
            time.sleep(INVERTER_DISCHARGE_WAIT_PERIOD_MINUTES * 60)
            discharge_period_minutes += INVERTER_DISCHARGE_WAIT_PERIOD_MINUTES
            if discharge_period_minutes > self.max_discharge_minutes:
                # This process has been discharging for much longer than is tolerable. Go home.
                gu.update_charger_assoc_command(CHARGER_ASSOC_NO_COMMAND_COMMAND)
                gu.update_charger_assoc_status(CHARGER_ASSOC_EXCESS_DISCHARGE_PERIOD_STATUS)
                gu.update_charger_assoc_timestamp(self.current_date_time)
                elapsed_seconds = self.current_utc - self.start_utc
                self.log_stuff("***CHARGER ASSOCIATION ABANDONED AT %s (UTC = %s, ELAPSED = %s SECS)" % (self.current_date_time, self.current_utc, elapsed_seconds))
                break
            else:
                # The limit of this daemon's patience has not been reached. Re-read the BMU data table
                # to see if 1 or more or all BMU SOC has been lowered.
                bmu_id_and_maxed_soc_string = gu.get_comma_separated_bmu_id_and_maxed_soc(self.bmu_maximum_soc)

            # Is the user fed up?
            if gu.is_charger_assoc_process_cancelled():
                self.process_was_cancelled = True
                break
        # while ...

        # Place all inverters back under gateway control. Sleep to allow the BMUs to settle back down.
        donut_care = gu.is_pack_and_send_mcast_gateway_control(SG424_MULTICAST_GENERIC_BATTERY, 1)
        self.log_stuff("BMU SOC OK NOW - INVERTERS RETURNED TO GATEWAY CONTROL")
        time.sleep(20)

        #  This process is now ready to perform association for all selected chargers. Non-zero
        #  current readings in the BMU data table will compromise the results. Normally, this isn't
        #  observed, but continue anyway if so. On the other hand, if there was a period of inverter
        #  discharge due to too-high an SOC on any BMU, perhaps the return-to-gateway-control multicast
        #  was not received by all inverters. In which case, send 1 more, wait a bit, and then
        #  continue regardless.
        if not gu.is_bmu_data_table_dead_quiet():
            self.log_stuff("BMU DATA TABLE NOT DEAD-QUIET - RESULTS MAY BE COMPROMISED")
            # Re-send another gateway control, sleep a bit, check the readongs one more time.
            self.log_stuff("RE-SEND INV_CTRL/GTW_CTRL + SLEEP ...")
            donut_care = gu.is_pack_and_send_mcast_gateway_control(SG424_MULTICAST_GENERIC_BATTERY, 1)
            time.sleep(10)
            if gu.is_bmu_data_table_dead_quiet():
                self.log_stuff("NOW THE BMU DATA TABLE NOW DEAD-QUIET ...")
            else:
                self.log_stuff("BMU DATA TABLE STILL NOT DEAD-QUIET - PROCEEDING NEVERTHELESS ...")
        return

    def setup_charger_association_commands(self):
        for iggy in range(0, NUMBER_OF_USEFUL_CLI_COMMANDS):
            self.cli_command_list.append("")
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_POWER_UP,                 CLI_COMMAND_POWER_UP)
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_SET_VREF_CHASSIS,         CLI_COMMAND_SET_VREF_CHASSIS)
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_SET_VREF,                 CLI_COMMAND_SET_VREF)
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_WRITE_PROTECT_MODULE,     CLI_COMMAND_WRITE_PROTECT_MODULE)
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL, CLI_COMMAND_SET_OUTPUT_CURRENT_LEVEL)
        self.cli_command_list.insert(INDEX_OF_CLI_COMMAND_POWER_DOWN,               CLI_COMMAND_POWER_DOWN)
        return

    def perform_association_on_selected_chargers(self):

        # Get the chassis ID list against which chargers are to be associated.
        self.chassis_id_list = gu.db_get_chassis_id_list_from_associate_chargers()
        self.process_was_cancelled = False
        for this_chassis_id in self.chassis_id_list:

            if gu.is_charger_assoc_process_cancelled():
                # The cancel request may have been detected in the inner loop for loop of each charger,
                # or when going from one chassis to the next.
                self.process_was_cancelled = True
                break

            # For this chassis, get the charger ID/module list to be associated.
            self.chassis_id = int(this_chassis_id)
            (self.charger_module_list, self.charger_id_list) = gu.db_get_charger_associate_list_from_chassis_id(
                self.chassis_id)
            this_string = "CHASSIS %s: PROCESS START, ASSOCIATE THESE CHASSIS/MODULE/CHARGER_ID:" % (self.chassis_id)
            for pop in range(0, len(self.charger_id_list)):
                this_string += " %s/%s/%s" % (self.chassis_id, self.charger_module_list[pop], self.charger_id_list[pop])
            self.log_stuff(this_string)

            # ---------------------------------------------------------------------
            #
            # For each chassis:
            #
            # Send a chassis "--power-up" command with 1 parameter: chassis ID.
            self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_POWER_UP]
            self.cli_parameter = "%s" % (self.chassis_id)
            full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
            this_string = "CHASSIS %s CLI(%s) " % (self.chassis_id, full_cli_command)
            self.log_stuff(this_string)
            gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_POWER_ON_SLEEP)

            # Send a chassis "--set-vref-chassis" command with 2 parameters: chassis ID, fixed 56.8v:
            self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_SET_VREF_CHASSIS]
            self.cli_parameter = "%s 56.8" % (self.chassis_id)
            full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
            this_string = "CLI(%s)" % (full_cli_command)
            gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)
            self.log_stuff(this_string)

            # The chassis is powered up and ready for charger interaction. Before interacting
            # with individual chargers on this chassis, set the output current for each to zero.
            self.set_zero_output_current_on_this_chassis()

            # ---------------------------------------------------------------------
            # Now associate each charger unit with 0, 1 or 2 BMUs. More than 2 is considered an error.
            self.log_stuff("ENTERING PER_CHARGER INTERACTION PHASE (CHASSIS/MODULE/CHARGER)")

            for this_charger_module, this_charger_id in zip(self.charger_module_list, self.charger_id_list):
                self.charger_id = int(this_charger_id)
                self.charger_module = int(this_charger_module)

                # -----------------------------------------
                # PERFORM ASSOCIATION FOR ONE CHARGER.
                self.associate_one_charger_unit()
                # -----------------------------------------

                if gu.is_charger_assoc_process_cancelled():
                    # All results so far will be dropped. Breaking out of this loop
                    # will shot down the chassis and go to the outter chassis loop,
                    # and the process will also bail at that level.
                    self.process_was_cancelled = True
                    break
            #
            # This is the end of the per-charger section.
            # ---------------------------------------------------------------------

            # All done with this chassis. There may be more chargers on another chassis,
            # so send this chassis a "--power-down" command with 1 parameter: chassis ID.
            self.cli_command = self.cli_command_list[INDEX_OF_CLI_COMMAND_POWER_DOWN]
            self.cli_parameter = "%s" % (self.chassis_id)
            full_cli_command = "%s %s" % (self.cli_command, self.cli_parameter)
            this_string = "CHASSIS %s CLI(%s) " % (self.chassis_id, full_cli_command)
            self.log_stuff(this_string)
            gu.send_artesyn_charger_cli_command(full_cli_command, CLI_CHARGER_COMMAND_GENERIC_SLEEP)

            self.log_stuff("CHASSIS %s: ASSOCIATION COMPLETE ON THIS CHASSIS" % (self.chassis_id))
            #
            # This is it for this chassis.
            # ---------------------------------------------------------------------

        return

    def run_charger_association(self):

        # Process goal: associate 1 or more chassis/charger to a PCS unit.
        #
        # This process involves sending commands to interact with the artesyn controller to
        # activate the charger/chassis interface as follows:
        #
        # - a few Artysen/battery based gateway daemons must be disabled
        # - stop/re-launch the ArtesynChassisRelayDaemon daemon
        # - launch the ArtesynControllerDaemon daemon
        # - for each chassis/charger, interact with the controller with the following CLI commanded actions:
        #       1: send a chassis power_up command
        #       2: wait to give the controller time to come up
        #       3: send a set_vref_chassis command
        #       4: disable chargers on a chassis with write protect module "disable" and set output current = 0.0 amps
        #       5: for each charger in question:
        #          5.1: send a set_output_current_level command (3amps) to cause charging
        #          5.2: read the BMU DATA table to determine which BMU (if any) has been a ctivated
        #          5.3: send a set_output_current_level command (param: chassis ID + charger ID + 0 amps) to stop discharge
        #       6: when done with all chargers on this chassis, send a power_off command

        #  1: open the results file into which associaiton activity will be written.
        if not self.is_open_charger_association_results_file():
            logger.critical("CHARGER ASSOCIATION FAILED TO OPEN THE LOG RESULTS FILE")
            return

        #  2: is everything good to go? Are there any impediments to prevent the process from starting?
        if not self.is_charger_association_good_to_go():
            # Reason for insanity will have been logged and/or alarmed.
            gu.update_charger_assoc_command(CHARGER_ASSOC_NO_COMMAND_COMMAND)
            gu.update_charger_assoc_status(CHARGER_ASSOC_NO_START_IMPEDIMENTS)
            self.results_file_handle.close()
            return

        #  3: setup the sub-set of CLI charger commands used by this process.
        self.setup_charger_association_commands()

        #  4: write a start-up notice to the log\
        self.log_stuff("***CHARGER ASSOCIATION STARTED (PID %s): %s (UTC = %s)" % (self.charger_assoc_pid, self.current_date_time, self.current_utc))
        gu.update_charger_assoc_command(CHARGER_ASSOC_RUNNING_COMMAND)
        gu.update_charger_assoc_status(CHARGER_ASSOC_STARTED_STATUS)

        #  5: update the charger association table for GUI progress display
        self.charger_units_completed = 0
        gu.update_charger_assoc_counter_column(CHARGER_ASSOCIATION_CONTROL_TABLE_NUM_OF_CHARGER_UNITS_COLUMN_NAME, self.number_charger_units_to_associate)
        gu.update_charger_assoc_counter_column(CHARGER_ASSOCIATION_CONTROL_TABLE_CHARGER_UNITS_COMPLETED_COLUMN_NAME, self.charger_units_completed)
        if self.number_charger_units_to_associate == gu.get_db_number_rows_in_charger_table():
            # Looks like all chargers are involved.
            self.all_chargers_involved = True

        #  6: several of the artesyn processes need to be stopped, and some started/re-started.
        self.prepare_artesyn_processes_for_charger_association(CHARGER_ASSOCIATION_PROCESS_START)
        gu.update_charger_assoc_status(CHARGER_ASSOC_IN_PROGRESS_STATUS)

        #  7: as the process prepares to enter an association cycle, verify if any BMUs have an SOC above a
        #     max threshold. BMUs above such a threshold will not charge, and there will not be any discernible
        #     amperage readings to establish a charger/BMU/PCS association. In which case, the process enters
        #     into a mass discharge period to bring down all SOC below the threshold.
        (bmu_id_max_list, bmu_soc_max_list) = gu.get_bmu_id_soc_above_max_threshold(self.bmu_maximum_soc)
        if len(bmu_id_max_list) > 0:
            self.log_stuff("%s BMUs WITH SOC >%s MAX THRESHOLD, EXPECT EXTENDED ASSOCIATION PROCESS TIME" % (len(bmu_id_max_list), self.bmu_maximum_soc))
            bmu_id_soc_list_string = "LIST WITH HIGH SOC: "
            for this_bmu_id, this_soc in zip(bmu_id_max_list, bmu_soc_max_list):
                bmu_id_soc_list_string += "%s=%s, " % (this_bmu_id, this_soc)
            self.log_stuff(bmu_id_soc_list_string)

            # Enter into a period of mass discharge.
            self.log_stuff("ENTERING MASS BMU DISCHARGE PERIOD BEFORE COMMENCING ASSOCIATION")
            self.perform_mass_bmu_discharge()

        #  8: from the previous step above, BMUs may have had to go through a discharge period to get their
        #     SOC below MAX threshold. The user may have lost patience in the meantime and canceled the
        #     association. Check and bail as necessary.
        if gu.is_charger_assoc_process_cancelled():
            # Go home. Return the Artesyn processes to their normal condition.
            self.prepare_artesyn_processes_for_charger_association(CHARGER_ASSOCIATION_PROCESS_END)
            self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
            self.current_utc = int(time.time())
            gu.update_charger_assoc_timestamp(self.current_date_time)
            elapsed_seconds = self.current_utc - self.start_utc
            self.log_stuff("CHARGER ASSOCIATION CANCELLED AT %s (UTC = %s, ELAPSED= %s SECS)" % (self.current_date_time, self.current_utc, elapsed_seconds))
            self.results_file_handle.close()
            return

        # -----------------------------------------------------------------------------------------
        #
        #   9. associate all selected chargers.
        self.perform_association_on_selected_chargers()
        #
        # -----------------------------------------------------------------------------------------

        # 10: check if the process terminated normally. If so, perform the final assessment.
        if not self.process_was_cancelled:
            self.log_stuff("ENTER FINAL EVALUATION AND ASSESSMENT PHASE")
            # Normal termination. Of all the BMUs that were associated against one or more chargers,
            # were any BMUs duplicated across chargers? If so, remove them from the "final" assessment
            # lists. They will be considered as failed.
            if not self.is_duplicate_associations_detected():
                if self.all_chargers_involved:
                    # A small logging blurb: since all chargers were involved in this association process,
                    # the expectation is that all BMUs/PCS units were associated to at least 1 charger.
                    # This function doesn't do anything except log if all BMUs were activated, and if not,
                    # which BMUs did not get activated during the process.
                    self.verify_all_bmu_id_association()
            else:
                # Duplicated BMUs were detected across chargers, they would have been removed from the final
                # evaluation list.
                pass

            # 11. final results/assessment and some logging results.
            self.charger_assoc_final_assessment()
        #
        # -----------------------------------------------------------------------------------------

        # 12: restore the artesyn processes as they were when this association process was started.
            self.log_stuff("ALL INTERACTION WITH CHASSIS/CHARGERS COMPLETE: RESTORE ARTESYN DEAMONS")
        self.prepare_artesyn_processes_for_charger_association(CHARGER_ASSOCIATION_PROCESS_END)

        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.current_utc = int(time.time())
        gu.update_charger_assoc_command(CHARGER_ASSOC_NO_COMMAND_COMMAND)
        gu.update_charger_assoc_status(CHARGER_ASSOC_COMPLETED_STATUS)
        gu.update_charger_assoc_timestamp(self.current_date_time)
        elapsed_seconds = self.current_utc - self.start_utc
        if self.process_was_cancelled:
            completion_notice_string = "***CHARGER ASSOCIATION CANCELLED AT "
        else:
            completion_notice_string = "***CHARGER ASSOCIATION COMPLETED AT "
        self.log_stuff("%s %s (UTC = %s, ELAPSED= %s SECS)" % (completion_notice_string, self.current_date_time, self.current_utc, elapsed_seconds))
        self.results_file_handle.close()

        return


def main():
    instantiate_chargerassocscript()
    return
    
if __name__ == '__main__':
    main()
    exit(0)
