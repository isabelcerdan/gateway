#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import socket
import struct
import time

from lib.db import prepared_act_on_database
import MySQLdb as MyDB
from contextlib import closing
import MySQLdb.cursors as my_cursor

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from gateway_utils import *

#
# Over-all process description:
#
# Purpose: Inverter Preparation For Turnip:
#
# IPFT = Inverter Preparation For Turnip
#
# The goal is to "prepare" SG424 inverters for system turnup, where preparation means to get the inverters
# into a state where they will not produce any power, not before the Gateway itself has been configured to
# control the field of inverters.
#
# - read the "new inverter ip" table, for each IP in the table:
#       - query the inverter via TLV
#       - wait for response
#       - if response is received, check "gateway control":
#             - if UNDER GATEWAY CONTROL = NO
#               then: - send gateway control command
#                     - query the inverter one more time
#
# This script does NOT run as a daemon, it is a script only: it is launched from the
# terminal, and runs to completion and terminates.
#
# Assumption: that the field of inverters may or may not be running with the latest inverter software.
#             Inverters can be running with an OLD version of software that does not have the TLV interface.

def execute_inverter_preparation_for_turnup_process():
    InverterPreparationForTurnupScript()

class InverterPreparationForTurnupScript:
    def __init__(self):

        self.SG424_data = []

        # These fields are populated based on the data returned by the inverter in response to
        # a query command.

        self.list_of_tlv = []
        self.list_of_tlv_values = []

        self.title_output_string = None
        self.version_string = None
        self.ppn_string = None
        self.sn_string = None
        self.mac_string = None
        self.pcc_value = 0
        self.feed_value = 0
        self.runmode = RUNMODE_UNKNOWN
        self.boot_version_string = None
        self.xet_info_string = None
        self.catch_up_reports_active_count = 0
        self.catch_up_reports_both_count = 0
        self.catch_up_reports_info_count = 0
        self.number_of_inverter_resets = 0
        self.sw_profile = None
        self.inv_ctrl = 0
        self.aif_immed_connect = 0
        self.keep_alive_mili_seconds = 0
        self.uptime_seconds = 0
        self.inverter_erc = 0
        self.dc_sm_output = None
        self.aif_immed_output_power_factor = 0
        self.aif_immed_output_power_factor_leading = 0
        self.aif_immed_output_power_factor_enable = 0
        self.dc_sm_instantaneous_watts = 0
        self.dc_sm_instantaneous_volts = 0
        self.dc_sm_instantaneous_amps = 0
        self.curtail_packet_count = 0
        self.regulate_packet_count = 0
        self.keep_alive_gone = 0
        self.keep_alive_back = 0
        self.keep_alive_late = 0
        self.seq_num_repeat = 0
        self.seq_num_oos = 0
        self.upstream_link_toggle_count = 0
        self.downstream_link_toggle_count = 0
        self.battery_discharge_type = 0

        self.version_string_reported = False
        self.ppn_string_reported = False
        self.sn_string_reported = False
        self.mac_string_reported = False
        self.pcc_value_reported = False
        self.feed_value_reported = False
        self.runmode_reported = False
        self.boot_version_string_reported = False
        self.xet_info_string_reported = False
        self.catch_up_reports_active_count_reported = False
        self.catch_up_reports_both_count_reported = False
        self.catch_up_reports_info_count_reported = False
        self.number_of_inverter_resets_reported = False
        self.sw_profile_reported = False
        self.inv_ctrl_reported = False
        self.aif_immed_connect_reported = False
        self.keep_alive_mili_seconds_reported = False
        self.uptime_seconds_reported = False
        self.inverter_erc_reported = False
        self.dc_sm_output_reported = False
        self.aif_immed_output_power_factor_reported = False
        self.aif_immed_output_power_factor_leading_reported = False
        self.aif_immed_output_power_factor_enable_reported = False
        self.dc_sm_instantaneous_watts_reported = False
        self.dc_sm_instantaneous_volts_reported = False
        self.dc_sm_instantaneous_amps_reported = False
        self.curtail_packet_count_reported = False
        self.regulate_packet_count_reported = False
        self.keep_alive_gone_reported = False
        self.keep_alive_back_reported = False
        self.keep_alive_late_reported = False
        self.seq_num_repeat_reported = False
        self.seq_num_oos_reported = False
        self.upstream_link_toggle_count_reported = False
        self.downstream_link_toggle_count_reported = False
        self.battery_discharge_type_reported = False

        # ---------------------------------------
        self.run_main_control_script_process()  # <- CALL THE SCRIPT'S MAIN PROCESS!!!
        # ---------------------------------------


    def validate_query_data(self):

        # Parse and validate the list of TLVs that were returned by the inverter in question.
        tlv_list_length = len(self.list_of_tlv)
        tlv_value_length = len(self.list_of_tlv_values)

        # Pre-initialize the xxx_reported field. Some inverters, if running old
        # code, do not necessarily return all expected TLVs in the QUERY response.
        self.version_string_reported = False
        self.ppn_string_reported = False
        self.sn_string_reported = False
        self.mac_string_reported = False
        self.pcc_value_reported = False
        self.feed_value_reported = False
        self.runmode_reported = False
        self.boot_version_string_reported = False
        self.xet_info_string_reported = False
        self.catch_up_reports_active_count_reported = False
        self.catch_up_reports_both_count_reported = False
        self.catch_up_reports_info_count_reported = False
        self.number_of_inverter_resets_reported = False
        self.sw_profile_reported = False
        self.inv_ctrl_reported = False
        self.aif_immed_connect_reported = False
        self.keep_alive_mili_seconds_reported = False
        self.uptime_seconds_reported = False
        self.inverter_erc_reported = False
        self.dc_sm_output_reported = False
        self.aif_immed_output_power_factor_reported = False
        self.aif_immed_output_power_factor_leading_reported = False
        self.aif_immed_output_power_factor_enable_reported = False
        self.dc_sm_instantaneous_watts_reported = False
        self.dc_sm_instantaneous_volts_reported = False
        self.dc_sm_instantaneous_amps_reported = False
        self.curtail_packet_count_reported = False
        self.regulate_packet_count_reported = False
        self.keep_alive_gone_reported = False
        self.keep_alive_back_reported = False
        self.keep_alive_late_reported = False
        self.seq_num_repeat_reported = False
        self.seq_num_oos_reported = False
        self.upstream_link_toggle_count_reported = False
        self.downstream_link_toggle_count_reported = False
        self.battery_discharge_type_reported = False

        if (tlv_list_length == tlv_value_length) and tlv_list_length > 0:

            # The majority of TLVs are ignored.

            for iggy in range(0, tlv_list_length):

                if self.list_of_tlv[iggy] == SG424_TO_GTW_VERSION_STRING_TLV_TYPE:
                    self.version_string = self.list_of_tlv_values[iggy]
                    self.version_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_PRODUCT_PART_NUMBER_STRING_TLV_TYPE:
                    self.ppn_string = self.list_of_tlv_values[iggy]
                    self.ppn_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_SERIAL_NUMBER_STRING_TLV_TYPE:
                    self.sn_string = self.list_of_tlv_values[iggy]
                    self.sn_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_MAC_ADDRESS_STRING_TLV_TYPE:
                    self.mac_string = self.list_of_tlv_values[iggy]
                    self.mac_string = self.mac_string.lower()
                    self.mac_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE:
                    self.pcc_value = self.list_of_tlv_values[iggy]
                    self.pcc_value_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE:
                    self.feed_value = self.list_of_tlv_values[iggy]
                    self.feed_value_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_RUNMODE_U16_TLV_TYPE:
                    self.runmode = self.list_of_tlv_values[iggy]
                    self.runmode_reported = True
                    # RUNMODE_SG424_APPLICATION = 1
                    # RUNMODE_SG424_BOOT_LOADER = 2
                    # RUNMODE_SG424_BOOT_UPDATER = 3
                    if self.runmode != RUNMODE_SG424_APPLICATION:
                        this_string = "INVERTER %s NOT IN STANDARD APP MODE, IS IN %s" % (self.inverter_ip_address, self.runmode)
                        print(this_string)

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_BOOT_VERSION_STRING_TLV_TYPE:
                    self.boot_version_string = self.list_of_tlv_values[iggy]
                    self.boot_version_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_XET_INFO_STRING_TLV_TYPE:
                    self.xet_info_string = self.list_of_tlv_values[iggy]
                    self.xet_info_string_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_ACTIVE_U16_TLV_TYPE:
                    self.catch_up_reports_active_count = self.list_of_tlv_values[iggy]
                    self.catch_up_reports_active_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_BOTH_U16_TLV_TYPE:
                    self.catch_up_reports_both_count = self.list_of_tlv_values[iggy]
                    self.catch_up_reports_both_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CATCH_UP_REPORTS_INFO_U16_TLV_TYPE:
                    self.catch_up_reports_info_count = self.list_of_tlv_values[iggy]
                    self.catch_up_reports_info_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_NUMBER_OF_RESETS_U16_TLV_TYPE:
                    self.number_of_inverter_resets = self.list_of_tlv_values[iggy]
                    self.number_of_inverter_resets_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_SW_PROFILE_U16_TLV_TYPE:
                    self.sw_profile = self.list_of_tlv_values[iggy]
                    self.sw_profile_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:
                    self.inv_ctrl = self.list_of_tlv_values[iggy]
                    self.inv_ctrl_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE:
                    self.aif_immed_connect = self.list_of_tlv_values[iggy]
                    self.aif_immed_connect_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_KEEP_ALIVE_TIMEOUT_U32_TLV_TYPE:
                    # Get the KEEP ALIVE TIMEOUT, mili-seconds. It's a 32-bit long.
                    self.keep_alive_mili_seconds = self.list_of_tlv_values[iggy]
                    self.keep_alive_mili_seconds_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_UPTIME_SECS_U32_TLV_TYPE:
                    # Get the inverter's UPTIME, seconds. It's a 32-bit quantity.
                    self.uptime_seconds = self.list_of_tlv_values[iggy]
                    self.uptime_seconds_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_ENERGY_REPORT_CONTROL_U16_TLV_TYPE:
                    self.inverter_erc = self.list_of_tlv_values[iggy]
                    self.inverter_erc_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_DC_SM_OUTPUT_STRING_TLV_TYPE:
                    self.dc_sm_output = self.list_of_tlv_values[iggy]
                    self.dc_sm_output_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE:
                    self.aif_immed_output_power_factor = self.list_of_tlv_values[iggy]
                    self.aif_immed_output_power_factor_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE:
                    self.aif_immed_output_power_factor_leading = self.list_of_tlv_values[iggy]
                    self.aif_immed_output_power_factor_leading_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE:
                    self.aif_immed_output_power_factor_enable = self.list_of_tlv_values[iggy]
                    self.aif_immed_output_power_factor_enable_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_WATTS_U16_TLV_TYPE:
                    self.dc_sm_instantaneous_watts = self.list_of_tlv_values[iggy]
                    self.dc_sm_instantaneous_watts_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_VOLTS_F32_TLV_TYPE:
                    self.dc_sm_instantaneous_volts = self.list_of_tlv_values[iggy]
                    self.dc_sm_instantaneous_volts_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_DC_SM_INSTANTANEOUS_AMPS_F32_TLV_TYPE:
                    self.dc_sm_instantaneous_amps = self.list_of_tlv_values[iggy]
                    self.dc_sm_instantaneous_amps_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_CURTAILMENT_PACKETS_U32_TLV_TYPE:
                    self.curtail_packet_count = self.list_of_tlv_values[iggy]
                    self.curtail_packet_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_REGULATION_PACKETS_U32_TLV_TYPE:
                    self.regulate_packet_count = self.list_of_tlv_values[iggy]
                    self.regulate_packet_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_KEEP_ALIVE_GONE_U32_TLV_TYPE:
                    self.keep_alive_gone = self.list_of_tlv_values[iggy]
                    self.keep_alive_gone_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_KEEP_ALIVE_BACK_U32_TLV_TYPE:
                    self.keep_alive_back = self.list_of_tlv_values[iggy]
                    self.keep_alive_back_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_KEEP_ALIVE_LATE_U32_TLV_TYPE:
                    self.keep_alive_late = self.list_of_tlv_values[iggy]
                    self.keep_alive_late_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_SEQUENCE_NUMBER_REPEAT_U32_TLV_TYPE:
                    self.seq_num_repeat = self.list_of_tlv_values[iggy]
                    self.seq_num_repeat_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_SEQUENCE_NUMBER_OOS_U32_TLV_TYPE:
                    self.seq_num_oos = self.list_of_tlv_values[iggy]
                    self.seq_num_oos_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_UPSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:
                    self.upstream_link_toggle_count = self.list_of_tlv_values[iggy]
                    self.upstream_link_toggle_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_DOWNSTREAM_LINK_TOGGLE_COUNT_U32_TLV_TYPE:
                    self.downstream_link_toggle_count = self.list_of_tlv_values[iggy]
                    self.downstream_link_toggle_count_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_BATTERY_U16_TLV_TYPE:
                    self.battery_discharge_type = self.list_of_tlv_values[iggy]
                    self.battery_discharge_type_reported = True

                elif self.list_of_tlv[iggy] == SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE:
                    print("HOS TLV IGNORED ...")

                # TODO: allow for other TLV types, even if they are irrelevant to the
                #       GET RUNMODE response.
                else:
                    # No other TLV type is expected when processing a QUERY response.
                    this_string = "UNEXP'D TLV TYPE %s IN QUERY RESPONSE" % (self.list_of_tlv[iggy])
                    print(this_string)
                    break
            # end ... for loop
        else:
            # Something wrong: a packet was received, so the length of both lists should be equal AND not zero.
            this_string = "ODDITY: query response received, tlv list/value lengths = %s/%s" % (tlv_list_length, tlv_value_length)
            print(this_string)

    def ipft_query_one_inverter(self):

        # Synopsis: get inverter QUERY data based on an IP address:
        #
        #           - send/receive a query to the targeted inverter
        #           - if a response is received, process the data

        reply_received = False

        self.SG424_data = send_and_get_query_data(self.inverter_ip_address)

        if self.SG424_data:
            # Packet received. Extract the data into a list of TLVs and associated data.
            reply_received = True
            (self.list_of_tlv, self.list_of_tlv_values) = extract_list_of_tlv_id_and_data_from_packed_data(self.SG424_data)
            self.validate_query_data()

        return reply_received

    def verify_inverter_data_for_turnup(self):

        # Got here because the inverter responded to a TLV QUERY. The goal is get the inverter under "gateway control",
        # in standby mode, and low power mode.
        #
        # - if AIF IMMED CONNECT is not reported, then it is running old software,
        #   so bit [7] = "STANDBY" of INV CTRL must be set even though in the latest version of inverter code,
        #   this bit has been deprecated in favor of AIF IMMED "CONNECT" variable.
        #
        # - on the other hand, if AIF IMMED "CONNECT" is reported, then only need to set 2 of the bits
        #   in INV CTRL and verify that AIF IMMED CONNECT = 0 (i.e., the new equivalent of the now-deprecated STANDBY).
        #
        # So check that the inverter is in: - gateway control = YES
        #                                   - in power control mode
        #                                   - AIF IMMED CONNECT = 0
        #
        # Rather than check a hard-coded value for INV_CTRL, check the 2 bits of interest, and check the
        # AIF SETTINGS "CONNECT" parameter.
        #
        # NOTE: "STANDBY" bit setting in INV_CTRL has been deprecated, in favor of AIF SETTINGS "CONNECT".
        if self.inv_ctrl_reported:
            # Display/verify.
            inverter_control_hex_as_string = hex(self.inv_ctrl)[2:].zfill(2)
            inverter_control_hex_as_bits = "{0:16b}".format(int(inverter_control_hex_as_string, 16))

            # inverter_control_hex_as_bits[0] = gateway control
            # inverter_control_hex_as_bits[1] = DEPRECATED
            # inverter_control_hex_as_bits[2] = DEPRECATED
            # inverter_control_hex_as_bits[3] = low power
            # inverter_control_hex_as_bits[4] = DEPRECATED
            # inverter_control_hex_as_bits[5] = sleep
            # inverter_control_hex_as_bits[6] = DEPRECATED
            # inverter_control_hex_as_bits[7] = DEPRECATED
            if self.version_string_reported:
                formatted_version_string = self.version_string
            else:
                formatted_version_string = "VERSION_NOT_REPORTED"

            # Check the following 2 bits, they must be set if the inverter is to be considered safe for turn-up.
            #
            # bits[0] = gateway control
            # bits[3] = low power
            if inverter_control_hex_as_bits[0] == "1" and inverter_control_hex_as_bits[3] == "1":
                this_string = "%s -> OK FOR TURNUP (INV_CTRL = 0x%s S/W version = %s)" % (self.title_output_string,
                                                                                          inverter_control_hex_as_string,
                                                                                          formatted_version_string)
                print(this_string)
            else:
                # This inverter may need fixing. Check the AIF IMMED CONNECT bit.
                if self.aif_immed_connect_reported:
                    this_string = "%s -> AIF IMMED CONNECT REPORTED AS " % (self.inverter_ip_address)

                    if self.aif_immed_connect:
                        # It's been reported, and it is set to connect to the grid. Disable it. Also include the TLV
                        # to set the "under gateway control" bit in INV_CTRL.
                        packed_data = add_master_tlv(2)
                        packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE, 0)
                        packed_data = append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, 1)
                        send_ok = is_send_tlv_packed_data(self.inverter_ip_address, packed_data)
                        if send_ok:
                            this_string += "ENABLED, WAS DISABLED VIA TLV"
                        else:
                            this_string += "ENABLED ** BUT TLV SEND FAILED **"
                    else:
                        # Reported but disabled - good.
                        this_string += "DISABLED - INVERTER CONSIDERED SAFE FOR TURNUP"
                    this_string += ", RE-VERIFY ..."
                    print(this_string)
                else:
                    this_string = "%s -> ** N O K **  FOR TURNUP (INV_CTRL = 0x%s S/W version = %s)" % (self.title_output_string,
                                                                                                        inverter_control_hex_as_string,
                                                                                                        formatted_version_string)
                    print(this_string)

                    # Send the following 3 CLI commands, in this order:
                    #
                    # - set the inverter under gateway control YES: "aif gtw_on"
                    # - set power control to 10%: "sp 10" *** NOT LONGER AVAILABLE ON THE SG424***
                    # - set low power control: "stby lp ena"
                    cli_command_string = "aif gtw_on"
                    send_cli_command(self.inverter_ip_address, cli_command_string)
                    time.sleep(1)

                    # cli_command_string = "sp 10"
                    # send_cli_command_by_tlv(self.inverter_ip_address, cli_command_string)
                    # time.sleep(1)

                    cli_command_string = "stby lp ena"
                    send_cli_command(self.inverter_ip_address, cli_command_string)
                    time.sleep(1)

                    this_string = "%s -> INVERTER COMMANDED VIA CLI FOR TURNUP, RE-VERIFY" % (self.inverter_ip_address)
                    print(this_string)

        else:
            # The INV_CTRL was NOT reported. Inverter may be in BOOT mode, or running old code where
            # this variable is not returned in the query response. Send the following 2 commands via HTTP.
            send_http_enable_standby(self.inverter_ip_address)
            time.sleep(1)

            send_http_enable_low_power(self.inverter_ip_address)
            time.sleep(1)

            this_string = "%s -> INV_CTRL NOT REPORTED - SENDING STBY/LOW POWER VIA HTTP, RE-VERIFY" % (self.inverter_ip_address)
            print(this_string)

    def verify_inverter_via_nbns(self):
        # Send/get the XET info question.
        nbns_question = NBNS_XETINFO_QUESTION
        xet_info_string = send_and_get_nbns_question(self.inverter_ip_address, nbns_question)
        if len(xet_info_string) != 0:
            # Check the string to see if it contains "SG424":
            self.title_output_string += xet_info_string
        else:
            self.title_output_string += " NO NBNS RESPONSE"
        print(self.title_output_string)

    def run_main_control_script_process(self):

        current_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        this_string = "INVERTER PREP SCRIPT LAUNCHED AT " + str(current_dt)
        print(this_string)

        # Extract all rows from the new ip table for inverter preparation.
        select_string = "SELECT %s,%s,%s,%s FROM %s;" % (DB_INVERTER_DISCOVERY_TEMP_TBL_IP_ADDRESS_COLUMN_NAME,
                                                         DB_INVERTER_DISCOVERY_TEMP_TBL_MAC_ADDRESS_COLUMN_NAME,
                                                         DB_INVERTER_DISCOVERY_TEMP_TBL_BREAKER_NUMBER_COLUMN_NAME,
                                                         DB_INVERTER_DISCOVERY_TEMP_TBL_LAST_UPDATE_COLUMN_NAME,
                                                         DB_INVERTER_DISCOVERY_TEMP_TABLE)

        inverters_ip_list = []
        try:
            inverters_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())

        except MyDB.Error as error:
            this_string = ("IGPC[MAJOR]: CANNOT CONNECT TO NEW IP DNS DB FOR IP LIST", error)

        else:

            print("EXTRACTED " + str(len(inverters_ip_list)) + " IPs FROM " + DB_INVERTER_DISCOVERY_TEMP_TABLE)

            for each_inverter in inverters_ip_list:
                self.inverter_ip_address = each_inverter[DB_INVERTER_DISCOVERY_TEMP_TBL_IP_ADDRESS_COLUMN_NAME]
                self.inverter_mac = each_inverter[DB_INVERTER_DISCOVERY_TEMP_TBL_MAC_ADDRESS_COLUMN_NAME]
                self.inverter_breaker_number = each_inverter[DB_INVERTER_DISCOVERY_TEMP_TBL_BREAKER_NUMBER_COLUMN_NAME]
                self.inverter_last_update = each_inverter[DB_INVERTER_DISCOVERY_TEMP_TBL_LAST_UPDATE_COLUMN_NAME]

                # Precaution:
                if not self.inverter_breaker_number:
                    self.inverter_breaker_number = "NO_BR_NUM"

                self.title_output_string = "VERIFY " + self.inverter_ip_address + "\tMAC=" + self.inverter_mac \
                                           + " br # = " + self.inverter_breaker_number \
                                           + " last update = " + str(self.inverter_last_update)
                # For the inverter at this IP, send a general purpose TLV query, and wait for the response.
                if self.ipft_query_one_inverter():
                    # A response was received from the inverter, verify the returned data.
                    self.verify_inverter_data_for_turnup()

                else:
                    # No response received from this inverter. Hey, was it an MGI-220? More than likely,
                    # it's an SG424 in boot loader mode, or running pre-TLV code.
                    self.title_output_string += " -> **NO RESPONSE** NBNS: "
                    self.verify_inverter_via_nbns()

        current_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        this_string = "INVERTER PREP SCRIPT COMPLETED AT " + str(current_dt)
        print(this_string)

# CORE OF THE SCRIPT. THIS SCRIPT IS NOT A DAEMON.

# ----------------------------------
execute_inverter_preparation_for_turnup_process()   # <- IT'S ALL ABOUT ME, ME, ME!!!
# ----------------------------------
