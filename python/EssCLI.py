#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
@module: EssCLI.py

@description: EssCLI is a command-line-interface module for the EssController.

@created: February 4, 2019

@author:     steve

@copyright:  2019 Apparent Energy, Inc. All rights reserved.
'''
import argparse
import zmq
import time
import json

from lib.gateway_constants.EssControllerConstants import ess_controller_command_input_dict
from lib.exceptions import ParameterError
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.helpers import get_args, extract_cmd


def publish_message(ess_unit_id, cmd):
    context = zmq.Context(1)
    pub_socket = context.socket(zmq.PUB)
    pub_socket.hwm = 100
    pub_socket.setsockopt(zmq.LINGER, 0)
    pub_socket.connect(MSG_BROKER_XSUB_URL)

    time.sleep(PUB_CONNECT_WAIT_TIME)

    topic = "essid%d-controller" % int(ess_unit_id)
    print 'Sending [%s, %s] to the Message Broker...' % (topic, cmd)
    pub_socket.send_multipart([topic, cmd])
    pub_socket.close()
    context.term()


"""
Time to figure out what the user wants...
"""
def get_cmd():
    parser = argparse.ArgumentParser(description='CLI for the ESS Controller')
    clargs = get_args(ess_controller_command_input_dict, parser)
    ess_unit_id, command, param = extract_cmd(clargs)
    if command is None:
        raise ParameterError("None or invalid command given")
    if param is True:
        param_str = 'None'
    else:
        param_str = str(param)

    cmd_str = "|".join((str(command), param_str))
    return ess_unit_id, json.dumps(cmd_str)

if __name__ == '__main__':
    ess_unit_id, cmd = get_cmd()
    publish_message(ess_unit_id, cmd)
    exit(0)

