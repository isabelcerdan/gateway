#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import time

import setproctitle
from daemon.daemon import Daemon
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_GROUPS_DAEMON_RESTARTED, \
                                  ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RESET_DEFAULTS, \
                                  ALARM_ID_GROUPS_DAEMON_GROUP_ID_1_RE_CREATED, \
                                  ALARM_ID_GROUPS_DAEMON_INVERTERS_GROUP_ID_MISSING_FROM_GROUPS, \
                                  ALARM_ID_GROUPS_DAEMON_IRRADIANCE_GROUP_INCONSISTENT, \
                                  ALARM_ID_GROUPS_DAEMON_GROUPS_TABLE_INCONSISTENT, \
                                  ALARM_ID_GROUPS_DAEMON_INVERTER_ASSIGNED_TO_NULL_GROUP, \
                                  ALARM_ID_GROUPS_DAEMON_ENERGY_REPORT_CONFIG_INCONSISTENT, \
                                  ALARM_ID_GROUPS_DAEMON_INVERTERS_NULL_GROUP_ID

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.db import prepared_act_on_database
from lib.logging_setup import *
import gateway_utils as gu

# A few useful definitions:
INTERVAL_LOOP_ITTY_BITTY_TIMEOUT = 10


# A small dictionary to support the effort to verify that none of the inverter-centric data in the
# groups table is NULL. These fields must be properly configured for all rows in the groups table.
GROUPS_COLUMNS_TO_VALIDATE = { 1 : (DB_GROUPS_TABLE, GROUPS_TABLE_FALLBACK_COLUMN_NAME,        GROUPS_TABLE_GROUP_ID_1_FALLBACK_DEFAULT),
                               2 : (DB_GROUPS_TABLE, GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME,   GROUPS_TABLE_GROUP_ID_1_NTP_URL_DEFAULT),
                               3 : (DB_GROUPS_TABLE, GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME,  GROUPS_TABLE_GROUP_ID_1_FORCED_UPGRADE_DEFAULT),
                               4 : (DB_GROUPS_TABLE, GROUPS_TABLE_IRRADIANCE_COLUMN_NAME,      GROUPS_TABLE_GROUP_ID_1_IRRADIANCE_DEFAULT),
                               5 : (DB_GROUPS_TABLE, GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME, GROUPS_TABLE_GROUP_ID_1_GATEWAY_CONTROL_DEFAULT),
                               6 : (DB_GROUPS_TABLE, GROUPS_TABLE_SLEEP_COLUMN_NAME,           GROUPS_TABLE_GROUP_ID_1_SLEEP_DEFAULT),
                               7 : (DB_GROUPS_TABLE, GROUPS_TABLE_ER_SERVER_HTTP_COLUMN_NAME,  GROUPS_TABLE_GROUP_ID_1_ER_SERVER_HTTP_DEFAULT),
                               8 : (DB_GROUPS_TABLE, GROUPS_TABLE_ER_GTW_TLV_COLUMN_NAME,      GROUPS_TABLE_GROUP_ID_1_ER_GTW_TLV_DEFAULT),
                               9 : (DB_GROUPS_TABLE, GROUPS_TABLE_AIF_ID_COLUMN_NAME,          GROUPS_TABLE_GROUP_ID_1_AIF_ID_DEFAULT)
                             }


class GroupsDaemon(Daemon):


    def __init__(self, **kwargs):
        super(GroupsDaemon, self).__init__(**kwargs)

        self.inverters_to_audit = 0
        self.audited_so_far = 0
        self.ip_to_audit = None
        self.inverter_group_id = 0

        self.inverter_fallback_value_from_groups = 0
        self.inverter_ntp_url_value_from_groups = None
        self.inverter_forced_upgraded_value_from_groups = 0
        self.inverter_feed_value_value_from_groups = 0
        self.inverter_pcc_value_value_from_groups = 0
        self.inverter_inv_ctrl_gtw_ctrl_value_from_groups = 0
        self.inverter_inv_ctrl_sleep_value_from_groups = 0
        self.inverter_er_server_http_value_from_groups = 0
        self.inverter_er_gateway_tlv_value_from_groups = 0
        self.inverter_irradiance_from_groups = 0
        self.inverter_aif_id_from_groups = 0
        self.inverter_er_derived_enable_value = 0

        # These come from the energy report config table:
        self.inverter_er_rate_value_from_erc = 0
        self.inverter_er_url_value_from_erc = None
        self.inverter_er_port_value_from_erc = 0
        self.inverter_er_user_name_value_from_erc = None
        self.inverter_er_password_value_from_erc = None

        self.reported_group_id = 0
        self.reported_fallback = 0
        self.reported_inv_ctrl = 0
        self.reported_ntp_server = None
        self.reported_pcc = 0
        self.reported_feed = 0
        self.reported_server_enable = 0
        self.reported_er_rate = 0
        self.reported_er_url = None
        self.reported_er_port = 0
        self.reported_er_user_name = None
        self.reported_er_password = None


        self.interval_timeout = 0
        self.query_timeout = 0
        self.loop_start_utc = 0
        self.my_logger = None
        self.results_file_handle = 0
        self.alarm_id = 0
        self.missing_groups_list = []

        self.setup_logger()

        self.current_interval = 0


    #
    # Logging functions
    #
    def setup_logger(self):
        self.my_logger = setup_logger("groupsctrl")

    def log_info(self, message):
        self.my_logger.info(message)


    #
    # Alarm functions
    #
    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)


    #
    # Get Groups Control table data from the database
    #  -- data could change randomly, so check at beginning of query loop
    #
    def refresh_groups_control_parameters(self):
        select_string = "SELECT * FROM %s;" % DB_GROUPS_CONTROL_TABLE
        select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
        if select_result:
            self.interval_timeout = select_result[GROUPS_CONTROL_TABLE_INTERVAL_TIMEOUT_COLUMN_NAME]
            self.query_timeout = select_result[GROUPS_CONTROL_TABLE_QUERY_TIMEOUT_COLUMN_NAME]
        else:
            # Sanity was verified before coming here, so this should not happen.
            self.log_info("REFRESH FAILED TO READ ITS CONTROL TABLE")


    #
    # Facility to update 2 counts in the control table. Useful if a progress measure
    # Possibly useful when a progress measure is desirable.
    # Possibly useless otherwise.
    #
    def db_update_groups_table_counters(self):
        update_string = "UPDATE %s SET %s=%s,%s=%s;" % (DB_GROUPS_CONTROL_TABLE,
                                                        GROUPS_CONTROL_TABLE_INVERTERS_TO_AUDIT_COLUMN_NAME, "%s",
                                                        GROUPS_CONTROL_TABLE_AUDITED_SO_FAR_COLUMN_NAME, "%s")
        update_arg = (self.inverters_to_audit, self.audited_so_far)
        prepared_act_on_database(EXECUTE, update_string, update_arg)


    #
    # Utility function to set this deamon as being updated.
    #
    def updateRunning(self):
        self.update()


    #
    # Utility function: create a packed data block with data for inverter group audit.
    #
    def build_group_audit_tlv_packed_data(self):
        number_of_user_tlv = 13
        packed_data = gu.add_master_tlv(number_of_user_tlv)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_GROUP_ID_CONFIG_U16_TLV_TYPE, self.inverter_group_id)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE, self.inverter_fallback_value_from_groups)
        packed_data = gu.append_user_tlv_string(packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, self.inverter_ntp_url_value_from_groups)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_CONFIG_FEED_U16_TLV_TYPE, self.inverter_feed_value_value_from_groups)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_CONFIG_PCC_U16_TLV_TYPE, self.inverter_pcc_value_value_from_groups)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE, self.inverter_inv_ctrl_gtw_ctrl_value_from_groups)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_UPDATE_INV_CTRL_SLEEP_U16_TLV_TYPE, self.inverter_inv_ctrl_sleep_value_from_groups)

        # Energy reporting parameters. The "server enable" is a 2-bit U16 derived from the groups table.
        # The remaining parameters come from the energy_report_config table.:

        server_enable = 0
        if self.inverter_er_server_http_value_from_groups == 1:
            server_enable = ERPT_XET_SERVER_ENABLE_BITS
        if self.inverter_er_gateway_tlv_value_from_groups == 1:
            server_enable += ERPT_GTW_SERVER_ENABLE_BITS

        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_EERPT_SERVER_ENABLE_U16_TLV_TYPE, server_enable)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_EERPT_SERVER_RATE_U16_TLV_TYPE, self.inverter_er_rate_value_from_erc)
        packed_data = gu.append_user_tlv_string(packed_data, GTW_TO_SG424_EERPT_SERVER_URL_STRING_TLV_TYPE, self.inverter_er_url_value_from_erc)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_EERPT_SERVER_PORT_U16_TLV_TYPE, self.inverter_er_port_value_from_erc)
        packed_data = gu.append_user_tlv_string(packed_data, GTW_TO_SG424_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE, self.inverter_er_user_name_value_from_erc)
        packed_data = gu.append_user_tlv_string(packed_data, GTW_TO_SG424_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE, self.inverter_er_password_value_from_erc)

        return packed_data


    #
    # Utility function: validate the data returned by the inverter for group audit purpose.
    #
    def is_validate_inverter_data_against_group(self):
        audit_successful = False
        inverter_requires_re_audit = False
        # NOT YET: this_string = None
        # Did the inverter report its correct group ID?
        if self.reported_group_id == self.inverter_group_id:
            # Group id correct. Check the rest of the returned data against this group.
            # All data from the inverter is expected to be as per group specification.

            if self.reported_fallback != self.inverter_fallback_value_from_groups:
                inverter_requires_re_audit = True
            if self.reported_ntp_server != self.inverter_ntp_url_value_from_groups:
                inverter_requires_re_audit = True

            # Check both the "under gateway control" and "sleep" bits that are contained in
            # the reported hex-based inv_ctrl:
            reported_inv_ctrl_as_hex_string = "0x%s" % (hex(self.reported_inv_ctrl)[2:].zfill(2))

            # UNDER GATEWAY CONTROL:
            if gu.is_gateway_control_set_in_inv_ctrl(reported_inv_ctrl_as_hex_string):
                reported_gtw_ctrl_from_inv_ctrl = 1
            else:
                reported_gtw_ctrl_from_inv_ctrl = 0
            if reported_gtw_ctrl_from_inv_ctrl != self.inverter_inv_ctrl_gtw_ctrl_value_from_groups:
                inverter_requires_re_audit = True

            # SLEEP:
            if gu.is_sleep_set_in_inv_ctrl(reported_inv_ctrl_as_hex_string):
                reported_sleep_from_inv_ctrl = 1
            else:
                reported_sleep_from_inv_ctrl = 0
            if reported_sleep_from_inv_ctrl != self.inverter_inv_ctrl_sleep_value_from_groups:
                inverter_requires_re_audit = True

            # Reported pcc/feed: if NOT under gateway control, validate non-gateway pcc/feed settings.
            if not self.inverter_inv_ctrl_gtw_ctrl_value_from_groups:
                # Inverter is NOT under gateway control, so its pcc/feed are set to the non-PCC/FEED values.
                if (self.reported_pcc != self.inverter_pcc_value_value_from_groups) or (self.reported_feed != self.inverter_feed_value_value_from_groups):
                    inverter_requires_re_audit = True

            # Energy Reporting configuration:
            # Server enable requires special processing, it's a packed value.
            if (self.reported_server_enable & ERPT_XET_SERVER_ENABLE_BITS) != 0:
                # The bit is set, inverter is reporting to the server via HTTP:
                if self.inverter_er_server_http_value_from_groups == 1:
                    pass
                else:
                    inverter_requires_re_audit = True
            else:
                # The bit is cleared, inverter is NOT reporting to the server via HTTP:
                if self.inverter_er_server_http_value_from_groups == 0:
                    pass
                else:
                    inverter_requires_re_audit = True

            if (self.reported_server_enable & ERPT_GTW_SERVER_ENABLE_BITS) != 0:
                # The bit is set, inverter is reporting to the gateway via TLV:
                if self.inverter_er_gateway_tlv_value_from_groups == 1:
                    pass
                else:
                    inverter_requires_re_audit = True
            else:
                # The bit is cleared, inverter is NOT reporting to the gateway via TLV:
                if self.inverter_er_gateway_tlv_value_from_groups == 0:
                    pass
                else:
                    inverter_requires_re_audit = True

            if self.reported_er_rate != self.inverter_er_rate_value_from_erc:
                inverter_requires_re_audit = True

            if self.reported_er_url != self.inverter_er_url_value_from_erc:
                inverter_requires_re_audit = True

            if self.reported_er_port != self.inverter_er_port_value_from_erc:
                inverter_requires_re_audit = True

            if self.reported_er_user_name != self.inverter_er_user_name_value_from_erc:
                inverter_requires_re_audit = True

            if self.reported_er_password != self.inverter_er_password_value_from_erc:
                inverter_requires_re_audit = True

        else:
            # Not expected: this inverter responded to a group ID audit, yet returned data
            # indicating it is in some other group?
            this_string = "IP %s: IS IN GROUP ID %s BUT REPORTED %s" % (self.ip_to_audit, self.inverter_group_id, self.reported_group_id)
            self.log_info(this_string)
            self.results_file_handle.write(this_string + LINE_TERMINATION)
            inverter_requires_re_audit = True

        if inverter_requires_re_audit:
            gu.db_update_inverter_group_audit_required(self.ip_to_audit, 1)
        else:
            audit_successful = True
            gu.db_update_inverter_group_audit_required(self.ip_to_audit, 0)

        return audit_successful


    #
    # Utility function: create a packed data block with data for inverter group audit.
    #
    def is_process_inverter_group_audit_response(self, SG424_data):
        inverter_audit_success = False
        (tlv_id_list, tlv_values_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
        if len(tlv_values_list) != 0:
            # Extract the relevant data for this audit. Items of interest:
            #
            # - group ID
            # - fallback
            # - INV_CTRL
            # - PCC
            # - FEED
            # - NTP URL
            # - ER SERVER ENABLE
            # - ER SERVER RATE
            # - ER SERVER URL
            # - ER SERVER PORT
            # - ER SERVER USER NAME
            # - ER SERVER PASSWORD
            extracted_tlv_of_interest = 0
            for this_tlv_id, this_tlv_value in zip(tlv_id_list, tlv_values_list):
                if this_tlv_id == SG424_TO_GTW_GROUP_ID_U16_TLV_TYPE:
                    self.reported_group_id = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE:
                    self.reported_fallback = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_INV_CTRL_U16_TLV_TYPE:
                    self.reported_inv_ctrl = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE:
                    self.reported_ntp_server = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_CONFIG_PCC_U16_TLV_TYPE:
                    self.reported_pcc = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_CONFIG_FEED_U16_TLV_TYPE:
                    self.reported_feed = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_ENABLE_U16_TLV_TYPE:
                    self.reported_server_enable = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_RATE_U16_TLV_TYPE:
                    self.reported_er_rate = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_URL_STRING_TLV_TYPE:
                    self.reported_er_url = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_PORT_U16_TLV_TYPE:
                    self.reported_er_port = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE:
                    self.reported_er_user_name = this_tlv_value
                    extracted_tlv_of_interest += 1
                elif this_tlv_id == SG424_TO_GTW_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE:
                    self.reported_er_password = this_tlv_value
                    extracted_tlv_of_interest += 1

                else:
                    # All others of no interest here.
                    pass

            if extracted_tlv_of_interest == 12:
                # Now the data can be validated.
                inverter_audit_success = self.is_validate_inverter_data_against_group()
                # TODO: ANY LOGGING?
            else:
                # Not expected. A response to audit from valid inverters expects at least these TLVs.
                this_string = "- IP %s: RESPONDED TO GROUP AUDIT BUT ONLY %s TLVs EXTRACTED" % (self.ip_to_audit, extracted_tlv_of_interest)
                self.log_info(this_string)
                self.results_file_handle.write(this_string + LINE_TERMINATION)
        else:
            # No response from inverter in question. Logged in handler file only.
            this_string = "- IP %s -> NO RESPONSE" % (self.ip_to_audit)
            self.results_file_handle.write(this_string + LINE_TERMINATION)

        return inverter_audit_success


    #
    # Utility function to query and audit an inverter's group-based data.
    #
    def is_group_data_audit_this_inverter_ok(self):

        inverter_audit_ok = False
        # Send this inverter a group ID command. First build the list of tlv/data to be sent,
        # consisting of: fallback, NTP URL, pcc/feed, GTW-CTRL/INV_CTRL, and ER booleans.
        this_group_row_data = gu.db_get_group_id_row_data(self.inverter_group_id)

        if this_group_row_data:

            self.inverter_fallback_value_from_groups = this_group_row_data[GROUPS_TABLE_FALLBACK_COLUMN_NAME]
            self.inverter_ntp_url_value_from_groups = this_group_row_data[GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME]
            self.inverter_forced_upgraded_value_from_groups = this_group_row_data[GROUPS_TABLE_FORCED_UPGRADE_COLUMN_NAME]
            self.inverter_er_server_http_value_from_groups = this_group_row_data[GROUPS_TABLE_ER_SERVER_HTTP_COLUMN_NAME]
            self.inverter_er_gateway_tlv_value_from_groups = this_group_row_data[GROUPS_TABLE_ER_GTW_TLV_COLUMN_NAME]
            self.inverter_irradiance_from_groups = this_group_row_data[GROUPS_TABLE_IRRADIANCE_COLUMN_NAME]
            self.inverter_inv_ctrl_gtw_ctrl_value_from_groups = this_group_row_data[GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME]
            self.inverter_inv_ctrl_sleep_value_from_groups = this_group_row_data[GROUPS_TABLE_SLEEP_COLUMN_NAME]
            self.inverter_aif_id_from_groups = this_group_row_data[GROUPS_TABLE_AIF_ID_COLUMN_NAME]

            # Sometimes a group is not properly setup:
            '''
            WHY DID THIS NOT WORK?
            if (not self.inverter_fallback_value_from_groups) \
            or (not self.inverter_ntp_url_value_from_groups) \
            or (not self.inverter_forced_upgraded_value_from_groups) \
            or (not self.inverter_er_server_http_value_from_groups) \
            or (not self.inverter_er_gateway_tlv_value_from_groups) \
            or (not self.inverter_irradiance_from_groups) \
            or (not self.inverter_inv_ctrl_gtw_ctrl_value_from_groups) \
            or (not self.inverter_aif_id_from_groups):
            '''
            if (self.inverter_fallback_value_from_groups == None) \
            or (self.inverter_ntp_url_value_from_groups == None) \
            or (self.inverter_forced_upgraded_value_from_groups == None) \
            or (self.inverter_er_server_http_value_from_groups == None) \
            or (self.inverter_er_gateway_tlv_value_from_groups == None) \
            or (self.inverter_irradiance_from_groups == None) \
            or (self.inverter_inv_ctrl_gtw_ctrl_value_from_groups == None) \
            or (self.inverter_inv_ctrl_sleep_value_from_groups == None) \
            or (self.inverter_aif_id_from_groups == None):
                this_string = "- FOR IP %s/GROUP ID %s -> SOME GROUPS FIELDS ARE NULL" % (self.ip_to_audit, self.inverter_group_id)
                self.log_info(this_string)
                self.results_file_handle.write(this_string + LINE_TERMINATION)
                gu.db_update_inverter_group_audit_required(self.ip_to_audit, 1)
                self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_GROUPS_TABLE_INCONSISTENT)
            else:
                # PCC/FEED to send depends on INV_CTRL
                if self.inverter_inv_ctrl_gtw_ctrl_value_from_groups == 0:
                    # This is a non-gateway group, pcc/feed need to be set:
                    # - fallback retrieved/written, should be 0 but meaningless for non-gateway
                    # - feed/pcc are set to the non-gateway values
                    # - gateway_control/INV_CTRL is cleared
                    self.inverter_pcc_value_value_from_groups = GATEWAY_PCC_NON_GATEWAY
                    self.inverter_feed_value_value_from_groups = GATEWAY_FEED_NON_GATEWAY

                else:
                    # This is a gateway-controlled group, may or may not be irradiance.
                    # - fallback is already specified
                    # - feed obtained from the inverters table, pcc derived from feed
                    # - under gateway control/INV_CRL will be updated
                    # - sleep/INV_CRL will be updated
                    (this_pcc_value, this_feed_value) = gu.db_get_pcc_and_feed_value_by_ip(self.ip_to_audit)
                    self.inverter_pcc_value_value_from_groups = this_pcc_value
                    self.inverter_feed_value_value_from_groups = this_feed_value

                # NOT READY TO INCLUDE "GROUP AIF_ID"

                packed_data = self.build_group_audit_tlv_packed_data()
                SG424_data = gu.send_and_get_group_audit_command(self.ip_to_audit, packed_data)
                inverter_audit_ok = self.is_process_inverter_group_audit_response(SG424_data)

        else:
            # This inverter to be re-audited upon the next cycle, where the group ID will be
            # validated and caught, such that its group ID will be set to group 1.
            self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_INVERTER_ASSIGNED_TO_NULL_GROUP)
            this_string = "IP %s ASIGNED TO NULL GROUP ID %s" % (self.ip_to_audit, self.inverter_group_id)
            self.log_info(this_string)
            self.results_file_handle.write(this_string)
            gu.db_update_inverter_group_audit_required(self.ip_to_audit, 1)

        return inverter_audit_ok


    #
    # If any group is defined as irradiance, verify that fallback is 100%.
    # Not a hard rule, just generally expected of an irradiance group.
    #
    def irradiance_verify_veracity(self):
        # select group_id,fallback,irradiance,gateway_control from groups where irradiance=1 and (fallback!=100 or gateway_control=1 or sleep=1);
        select_string = "SELECT %s,%s,%s,%s,%s FROM %s WHERE %s=%s AND (%s!=%s OR %s=%s OR %s=%s);" % (GROUPS_TABLE_GROUP_ID_COLUMN_NAME,
                                                                                                       GROUPS_TABLE_FALLBACK_COLUMN_NAME,
                                                                                                       GROUPS_TABLE_IRRADIANCE_COLUMN_NAME,
                                                                                                       GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME,
                                                                                                       GROUPS_TABLE_SLEEP_COLUMN_NAME,
                                                                                                       DB_GROUPS_TABLE,
                                                                                                       GROUPS_TABLE_IRRADIANCE_COLUMN_NAME, "%s",
                                                                                                       GROUPS_TABLE_FALLBACK_COLUMN_NAME, "%s",
                                                                                                       GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME, "%s",
                                                                                                       GROUPS_TABLE_SLEEP_COLUMN_NAME, "%s")
        select_args = (1, 100,1,1)
        select_results = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        if select_results:
            # At least 1 row with irradiance has a fallback other than 100% or still under gateway control,
            # or still going to sleep at night. Fix and complain.
            self.log_info("1 OR MORE IRRADIANCE GROUPS HAS INCONSISTENT DATA")
            self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_IRRADIANCE_GROUP_INCONSISTENT)
            # Update groups: if irradiance is set and (fallback not 100 or gateway control is set or sleep is set) ...
            update_string = "UPDATE %s SET %s=%s,%s=%s,%s=%s WHERE %s=%s AND (%s!=%s OR %s=%s OR %s=%s)" % (DB_GROUPS_TABLE,
                                                                                                            GROUPS_TABLE_FALLBACK_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_SLEEP_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_IRRADIANCE_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_FALLBACK_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_GATEWAY_CONTROL_COLUMN_NAME, "%s",
                                                                                                            GROUPS_TABLE_SLEEP_COLUMN_NAME, "%s")
            update_arg = (100, 0, 0, 1, 100,1,1)
            prepared_act_on_database(EXECUTE, update_string, update_arg)

        else:
            # All entries where irradiance set have a 100% fallback. Nothing to do here.
            pass
        return


    #
    # For all rows in the groups table, verify that none of the inverter-centric columns
    # have NULL data. If so: update with the groups 1 default. An intsy-tinsy, itty-bitty
    # data dictionary is defined to traverse the relevant columns of the groups table.
    #
    def fix_inverter_centric_data_in_groups_table(self):
        fixit_string = ""
        for iggy in GROUPS_COLUMNS_TO_VALIDATE:
            (table_name, column_name, default_value) = GROUPS_COLUMNS_TO_VALIDATE[iggy]
            null_count = gu.database_gimme_row_null_count(table_name, column_name)
            if null_count != 0:
                gu.database_fix_row_null_data(table_name, column_name, default_value)
                fixit_string = "%s%s: %s ROWS HAD NULL DATA, " % (fixit_string, column_name, null_count)
        if fixit_string:
            # Remove the very last "space comma" (i.e., the last 2 trailing characters).
            fixit_string = fixit_string[:-2]
            self.log_info("GROUPS TABLE REQUIRED FIXING: %s" % (fixit_string))


    #
    # Verify the sanity of group ID:
    #
    # - rule 1: group ID 1 exists, and with default data
    # - rule 2: all inverter group IDs are represented in the groups table
    # - rule 3: for all groups, if any have non-gateway set, then irradiance and fallback must = 0
    # - rule 4: all inverter-centric columns in the groups table is data filled.
    # - rule 5: if irradiance is set, fallback is forced to 100%
    def verify_groups_table_data_sanity(self):

        # Rule 1: verify the sanity of groups table entry 1.
        if not gu.is_group_id_1_sanity_ok():
            # Group ID 1 is either missing, or default data inadvertently changed.
            self.alarm_id = gu.rectify_group_id_1_data()
            self.log_info("GROUP 1 DATA REQUIRED FIXING, SET TO DEFAULT PARAMETERS")

        # Rule 2: AIF_ID parameter - not implemented, not yet anyway. But cannot be NULL.
        #         In fact, none of the inverter-centric parameters can be NULL.
        self.fix_inverter_centric_data_in_groups_table()

        # Rule 3: verify that all inverters have a non-NULL group ID.
        null_group_id_count = gu.get_inverters_null_group_id_count()
        if null_group_id_count > 0:
            gu.rectify_inverters_null_group_id()
            self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_INVERTERS_NULL_GROUP_ID)
            self.log_info("%s INVERTERS HAD NULL GROUP ID - ARE ASSIGNED TO GROUP 1" % (null_group_id_count))

        # Rule 4: verify that all inverters have a group ID contained in the groups table.
        self.missing_groups_list = gu.get_inverters_group_id_missing_from_groups_table()
        if self.missing_groups_list:
            gu.rectify_inverters_missing_group_id(self.missing_groups_list)
            self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_INVERTERS_GROUP_ID_MISSING_FROM_GROUPS)
            self.log_info("%s INVERTERS HAD GROUP ID NOT IN GROUPS TABLE - ARE ASSIGNED TO GROUP 1" % (len(self.missing_groups_list)))

        # Rule 5: anything specific about the non-gateway inverter group? Like: can't be a battery inverter?

        # Rule 6: irrradiance: such groups are expected to have 100% fallaback,
        #         and NOT under gateway control. Check please.
        self.irradiance_verify_veracity()

        # Rule 7: until we need more.

        return


    #
    # Verify the sanity of group ID:
    #
    # - rule 1: group ID 1 exists, and with default data
    # - rule 2: all inverter group IDs are represented in the groups table
    # - rule 3: for all groups, if any have non-gateway set, then irradiance and fallback must = 0
    def db_get_erc_parameters(self):
        select_string = "SELECT * FROM %s;" % DB_ENERGY_REPORT_CONFIG_TABLE
        select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
        if select_result:
            self.inverter_er_rate_value_from_erc = int(select_result[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_RATE_COLUMN_NAME])
            self.inverter_er_url_value_from_erc = select_result[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_URL_COLUMN_NAME]
            self.inverter_er_port_value_from_erc = int(select_result[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_PORT_COLUMN_NAME])
            self.inverter_er_user_name_value_from_erc = select_result[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_USER_NAME_COLUMN_NAME]
            self.inverter_er_password_value_from_erc = select_result[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_PASSWORD_COLUMN_NAME]

            # Verify those fields:
            if (self.inverter_er_rate_value_from_erc == None) \
            or (self.inverter_er_url_value_from_erc == None) \
            or (self.inverter_er_port_value_from_erc == None) \
            or (self.inverter_er_user_name_value_from_erc == None) \
            or (self.inverter_er_password_value_from_erc == None):
                this_string = "- 1 OR MORE FIELDS FROM %s TABLE HAVE NULL DATA" % (DB_ENERGY_REPORT_CONFIG_TABLE)
                self.log_info(this_string)
                self.results_file_handle.write(this_string + LINE_TERMINATION)
                self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_ENERGY_REPORT_CONFIG_INCONSISTENT)
            else:
                pass
        else:
            this_string = "- NULL DATA READ OUT FROM %s TABLE" % (DB_ENERGY_REPORT_CONFIG_TABLE)
            self.log_info(this_string)
            self.results_file_handle.write(this_string + LINE_TERMINATION)
            self.raise_and_clear_alarm(ALARM_ID_GROUPS_DAEMON_ENERGY_REPORT_CONFIG_INCONSISTENT)

        return


    #
    # Audit inverters if their group membership requires it.
    #
    def audit_all_inverter_for_groups(self):

        # Get the list of inverter IP/group ID that require group auditing.
        (ip_list, group_id_list) = gu.get_group_audit_required_ip_and_id_list()

        self.inverters_to_audit = len(ip_list)
        self.audited_so_far = 0
        self.db_update_groups_table_counters()
        if self.inverters_to_audit > 0:
            this_string = "- %s INVERTERS WILL REQUIRE GROUP AUDITING:\r\n" % (self.inverters_to_audit)
            self.results_file_handle.write(this_string)
            failed_audit_ip_list = []

            # Get the energy report related parameters from the energy report config table.
            self.db_get_erc_parameters()

            # TODO: consider if the list of audited IPs (failed/success) should be included in the results file.
            #       On a large site (such as PVUSA), the list can be quite long. So should logging include IPs?

            for this_ip, this_group_id in zip(ip_list, group_id_list):
                self.ip_to_audit = this_ip
                self.inverter_group_id = this_group_id
                if not self.is_group_data_audit_this_inverter_ok():
                    failed_audit_ip_list.append(self.ip_to_audit)
                time.sleep(self.query_timeout)
                self.updateRunning()
                self.audited_so_far += 1
                self.db_update_groups_table_counters()

            # Done auditing all inverters that required it. Were they all successful?
            if len(failed_audit_ip_list) > 0:
                # 1 or more inverters that required auditing failed for one reason or another.
                this_string = "- %s INVERTERS FAILED GROUPS AUDIT" % (len(failed_audit_ip_list))
            else:
                # All inverters that required auditing responded.
                this_string = "- GROUP AUDIT SUCCESSFUL AGAINST THOSE INVERTERS THAT REQUIRED IT"
            self.results_file_handle.write(this_string + LINE_TERMINATION)
        else:
            # Nothing to audit, nothing to do. So doing plenty of nothing. Note that the length
            # of both lists is not validated for equality: both are populated in tandem, so it
            # is a valid assumption that they will always be equal.
            this_string = "- 0 INVERTERS REQUIRED GROUP AUDITING\r\n"
            self.results_file_handle.write(this_string)
        return


    #
    #
    # Utility to open the results file for the groups audit daemon,
    # and write a terse message that a new auditing loop has started.
    #
    def open_and_write_results_file_start_notice(self):
        try:
            self.results_file_handle = open(GROUPS_AUDIT_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception:  # as err_string:
            # Think long and hard about an alarm. Well, not too long, not too hard.
            self.log_info("FAILED TO OPEN %s FILE" % (GROUPS_AUDIT_RESULTS_PATH_AND_FILE_NAME))

        else:
            current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
            self.loop_start_utc = int(time.time())
            dashes_string = "---------------------------------------------------------------\r\n"
            this_string = "GROUPS AUDIT START %s (UTC %s)" % (current_date_time, self.loop_start_utc)
            self.results_file_handle.write(dashes_string + this_string + LINE_TERMINATION)
            self.log_info(this_string)
        return


    #
    #
    # Utility to write a completion notice to the results file, and close it.
    #
    def write_completion_notice_and_close_results_file(self):
        current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        current_utc = int(time.time())
        elapsed = current_utc - self.loop_start_utc
        this_string = "GROUPS AUDIT COMPLETE %s (UTC %s ELAPSED %s)\r\n" % (current_date_time, current_utc, elapsed)
        self.results_file_handle.write(this_string + LINE_TERMINATION)
        self.results_file_handle.close()
        self.log_info(this_string)
        return


    #
    #
    # Main loop
    #
    def run(self):
        # ---------------------------------------------------------------------
        #
        # Run forever and don't stop until eternity is reached.
        #
        # The groups controller is a very slow rate audit-oriented daemon, whose sole function
        # is to traverse the inverters table, seeking those entries with the ig_audit_required
        # bit set. Those inverters are audited.
        #
        # Run forever and don't stop until you've reached eternity.
        while True:
            self.updateRunning()

            # Ensures consistency of group-related data.
            self.verify_groups_table_data_sanity()

            # In case something changed on-the-fly.
            self.refresh_groups_control_parameters()

            loop_interval = self.interval_timeout
            while (loop_interval > 0):
                # Interval sleep before auditing groups. But don't forget to update.
                time.sleep(INTERVAL_LOOP_ITTY_BITTY_TIMEOUT)
                loop_interval -= INTERVAL_LOOP_ITTY_BITTY_TIMEOUT
                self.updateRunning()
            # while ...

           # Open and write a start notice to the results file.
            self.open_and_write_results_file_start_notice()

            # Audit those all inverters whose group-requires-auditing is set.
            self.audit_all_inverter_for_groups()

            # Write a completion notice to the log file and close it.
            self.write_completion_notice_and_close_results_file()

        # while ...

    # end run function


if __name__ == '__main__':
    GroupsDaemon()
