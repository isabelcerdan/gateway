'''
Created on Feb 14, 2017

HammerGateway is a load-generating program designed to test the StringMon Daemon's
ability to handle HOS response bursts.  The program is command line configurable
(see Usage for parameter definitions.)

@author: steve
'''
import os, signal
import time
import getopt
import sys
import socket
import fcntl
import struct
import Queue
import random
import argparse
import binascii
from datetime import datetime
from threading import Thread, current_thread
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.StringMonConstants import *
from gateway_utils import *


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg
        
        
class HammerThread(Thread):
    def __init__(self, queue, ipaddr, port, delay):
        Thread.__init__(self)
        self.sock = None
        self.ipaddr = ipaddr
        self.queue = queue
        self.port = port
        self.packet = []
        self.LAN_address = None
        self.delay = delay
        self.create_socket()


    def create_socket(self):
        self.LAN_address = get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except:
            print "Unable to create socket - exiting"
            os._exit()
        '''
        else:
            # Bind the socket to the port
            inverter_address = (self.LAN_address, SG424_PRIVATE_UDP_PORT)
            print 'starting up on',  repr(inverter_address)
            self.sock.bind(inverter_address)
           '''

    def run(self):
#        print "Starting %s..."  % Thread.getName(self)
        while not self.queue.empty():
            self.packet = []
            self.packet = struct.pack('!HHH', SG424_TO_GTW_MASTER_U16_TLV_TYPE,
                                MASTER_TLV_DATA_FIELD_LENGTH, 8)
            for i in range(8):
                self.packet += struct.pack('!HH', SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE, 6)
                try:
                    mac_str = self.queue.get()
                    mac_addr = int(mac_str, 16)
                except Exception as err:
                    print "Queue error:", repr(err)
                    break
                if mac_addr == 0:
                    print "Queue empty..."
                    break
                try:
                    self.packet += binascii.unhexlify(mac_str)
                except Exception as err:
                    # If we can't pack the mac_address, just skip to the next entry.
                    print "Oh foo!", repr(err)
                    continue 
            try:
                self.sock.sendto(self.packet,(self.ipaddr, STRINGMON_HOS_DISCOVERY_PORT))
            except Exception as err:
                print "Socket error:", repr(err)
                os._exit(1)
            try:
                self.queue.task_done()
            except Exception as err:
                self.log_error(repr(err))
            time.sleep(self.delay)
#        print "%s done..." % Thread.getName(self)
        
    
def generate_hos_response(queue):
    response = 'e01f0a' + '%02x%02x%02x' % \
               (random.randint(0x00, 0x7f), 
               random.randint(0x00, 0xff),
               random.randint(0x00, 0xff))
    queue.put(response)


def main(args):
    queue = Queue.Queue(maxsize=0)

    ipaddr = str(args.address[0])
    port = int(args.port[0])
    num_responses = int(args.num_responses[0])
    num_threads = int(args.threads[0])
    delay = float(int(args.delay_ms[0])) / 1000.0
    #print "delay = %f" % delay

    # Fill the output queue with lots of bogus HOS responses.
    print "Generating %d MAC addresses for %d messages..." % (num_responses * 8, num_responses)
    for i in range(num_responses * 8):
        generate_hos_response(queue)
    
    for i in range(num_threads):
        ht = HammerThread(queue, ipaddr, port, delay)
        ht.setDaemon(True)
        ht.start()
        
    while not queue.empty():
        time.sleep(1)

    return

    
if __name__ == '__main__':
    
#    signal.signal(signal.SIGINT, sig_handler)
#    signal.signal(signal.SIGTERM, sig_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--address', nargs=1, required=True)
    parser.add_argument('-p', '--port', nargs=1, required=True)
    parser.add_argument('-n', '--num_responses', nargs=1, required=True)
    parser.add_argument('-t', '--threads', nargs=1, required=True)
    parser.add_argument('-d', '--delay_ms', nargs=1, required=True)
    args = parser.parse_args()
    print args
    main(args)
     
    sys.exit(0)

        