'''
@omdule: MODbusTestClient.py

@description: This module is a test client for the MODbusTestServer.  Different
              configurations of the test server can be verified using this tool.

@copyright: Apparent Energy Inc.  2018

@author: Steve
'''
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.exceptions import ModbusException, ModbusIOException
import logging

FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

UNIT = 0x1


def run_sync_client():
    client = ModbusClient('localhost', port=5020)
    client.connect()

    log.debug("Reading Coils")
    rr = client.read_coils(1, 1, unit=UNIT)
    log.debug(rr)

    log.debug("Write to a Coil and read back")
    try:
        rq = client.write_coil(0, True, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on write_coil')
        exit
        
    try:
        rr = client.read_coils(0, 1, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read_coil')
        exit
        
    assert(rr.bits[0] == True)          # test the expected value

    log.debug("Write to multiple coils and read back- test 1")
    try:
        rq = client.write_coils(1, [True]*8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on write to multiple coils')
        exit
        
    try:
        rr = client.read_coils(1, 21, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read multiple coils')
        exit

    resp = [True]*21

    '''
    If the returned output quantity is not a multiple of eight, the
    remaining bits in the final data byte will be padded with zeros
    (toward the high order end of the byte).
    '''
    resp.extend([False]*3)
    assert(rr.bits == resp)         # test the expected value

    log.debug("Write to multiple coils and read back - test 2")
    try:
        rq = client.write_coils(1, [False]*8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read multiple coils')
        exit
    try:
        rr = client.read_coils(1, 8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read multiple coils')
        exit
        
    assert(rr.bits == [False]*8)         # test the expected value

    log.debug("Read discrete inputs")
    try:
        rr = client.read_discrete_inputs(0, 8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read discrete inputs')
        exit
        
    log.debug("Write to a holding register and read back")
    try:
        rq = client.write_register(1, 10, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on write register')
        exit
    try:
        rr = client.read_holding_registers(1, 1, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read multiple coils')
        exit
        
    assert(rr.registers[0] == 10)       # test the expected value

    log.debug("Write to multiple holding registers and read back")
    try:
        rq = client.write_registers(1, [10]*8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on write to multiple holding registers')
        exit
    try:
        rr = client.read_holding_registers(1, 8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read holding registers')
        exit
        
    assert(rr.registers == [10]*8)      # test the expected value

    log.debug("Read input registers")
    try:
        rr = client.read_input_registers(1, 8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read input registers')
        exit
        
    arguments = {
        'read_address':    1,
        'read_count':      8,
        'write_address':   1,
        'write_registers': [20]*8,
    }
    log.debug("Read/write registers simulataneously")
    try:
        rq = client.readwrite_registers(unit=UNIT, **arguments)
        rr = client.read_holding_registers(1, 8, unit=UNIT)
    except (ModbusException, ModbusIOException):
        log.error('Modbus failure on read/write registers simulataneously')
        exit
        
    assert(rq.registers == [20]*8)      # test the expected value
    assert(rr.registers == [20]*8)      # test the expected value

    # ----------------------------------------------------------------------- #
    # close the client
    # ----------------------------------------------------------------------- #
    client.close()


if __name__ == "__main__":
    run_sync_client()
