#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
Created on Jan 18, 2017

@author: steve
'''
import os
import socket
import struct
import binascii
import sys
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.NetMonConstants import *

APPARENT_MAC_HEADER = 'E01F0A'

class FakeInverter(object):
    def __init__(self):
        self.sock = None
        self.response_data = None
        self.number_of_user_tlv = 0
        self.mac_string_length = 0  # MAC string length
        self.mac_string = None  # MAC string
        self.recv_packet = None
        
        self.SG424_data = []
        self.SG424_data_length = 0
        
        self.create_socket()
        
        self.run_unified_daemon()

    
    def create_HOS_response(self):
        mac_list = ['112233', '445566', '778899', 'aabbcc', 'ddeeff', '000000', '000000', '000000']
        
        self.SG424_data = struct.pack('!HHH', SG424_TO_GTW_MASTER_U16_TLV_TYPE,
                                         MASTER_TLV_DATA_FIELD_LENGTH, 8)
        print 'Initial Packed Value   :', binascii.hexlify(self.SG424_data)
        for mac in mac_list:
            if '000000' == mac:
                big_mac = mac + '000000'
            else:
                big_mac = APPARENT_MAC_HEADER + mac
            print "big_mac = %s..." % big_mac
            self.SG424_data += struct.pack('!HH', SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE, 6)
            toggle = 0
            piggy = ''
            for iggy in big_mac:
                if (toggle):
                    piggy = piggy + iggy
                    print 'piggy + iggy = %s' % piggy
                    toggle = 0
                else:
                    piggy = iggy
                    toggle = 1
                    continue
                print "piggy = %x..." % int(piggy, 16)
                self.SG424_data += struct.pack('!B', int(piggy, 16))
                        
        print "self.SG424_data len = %d" % len(self.SG424_data)
        print 'Packed Value   :', binascii.hexlify(self.SG424_data)
        
    def prepare_hos_response(self):
        mac_list = [0xE01F0A112233, 0xE01F0A445566, 0xE01F0A778899, 0xE01F0Aaabbcc, 0xE01F0Addeeff]
        #mac_list = [0xE01F0A445566, 0xE01F0A778899, 0xE01F0Aaabbcc, 0xE01F0Addeeff]

        self.SG424_data = struct.pack('!HHH', SG424_TO_GTW_MASTER_U16_TLV_TYPE,
                                         MASTER_TLV_DATA_FIELD_LENGTH, 5)
        for mac in mac_list:
            self.SG424_data += struct.pack('!HH', SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE, 6)
            print "MAC = %x" % mac
            self.SG424_data += struct.pack('!Q', mac)

        print "self.SG424_data len = %d" % len(self.SG424_data)
        print 'Packed Value   :', binascii.hexlify(self.SG424_data)
        
               
    def create_socket(self):
        print "Creating socket..."
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except:
            print "Unable to create socket - exiting"
            os._exit()
        else:
            # Bind the socket to the port
            inverter_address = ('localhost', SG424_PRIVATE_UDP_PORT)
            print 'starting up on %s port %s' % inverter_address
            self.sock.bind(inverter_address)


    def run_unified_daemon(self):
        while True:
            print '\nwaiting to receive message'
            self.recv_packet, address = self.sock.recvfrom(512)
            
            print 'received %s bytes from %s' % (len(self.recv_packet), address)
            print "Here it is:", self.recv_packet

            if self.recv_packet == str(GTW_TO_SG424_HOS_QUERY_U16_TLV_TYPE):
                self.prepare_hos_response()
                print self.SG424_data
                sent = self.sock.sendto(str(self.SG424_data), address)
                print 'sent %s bytes back to %s' % (sent, address)
            else:
                print "received = %s" % self.recv_packet
                    

def main():
    FakeInverter()
    
if __name__ == '__main__':
    main()

