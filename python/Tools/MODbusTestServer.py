'''
@module: MODbusTestServer.py

@description: This module creates a MODbus/TCP server that can be used as a
              communications test tool.

@copyright: Apparent Energy Inc.  2018

@author: Steve
'''
from pymodbus.server.sync import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
import logging

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)


def run_server():
    '''
    Step 1: Initialize data store.
    The datastores only respond to the addresses that they are initialized
    to.  Therefore, if you initialize a DataBlock to addresses of 0x00 to
    0xFF, a request to 0x100 will respond with an invalid address exception.
    
        block = ModbusSequentialDataBlock(0x00, [0]*0xff)
    
    Step 2:
    You can choose to use a sequential or a sparse DataBlock in your data context.
    The difference is that the sequential block has no gaps in the data while the
    sparse block can.
    
        block = ModbusSparseDataBlock({0x00: 0, 0x05: 1})
        block = ModbusSequentialDataBlock(0x00, [0]*5)
    
    Alternately, you can use the factory methods to initialize the DataBlocks
    or simply do not pass them to have them initialized to 0x00 on the full
    address range::
    
        store = ModbusSlaveContext(di = ModbusSequentialDataBlock.create())
        store = ModbusSlaveContext()
    
    Step 3:
    You are allowed to use the same DataBlock reference for every table
    or you may use a separate DataBlock for each table.  This depends 
    whether you would like functions to be able to access and modify
    the same data or not:
    
        block = ModbusSequentialDataBlock(0x00, [0]*0xff)
        store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block)
    
    The server makes use of a server context that allows it to respond with
    different slave contexts for different unit ids. By default, it will return
    the same context for every unit id supplied (broadcast mode).  However, this
    can be overloaded by setting the single flag to False and then supplying a
    dictionary of unit id to context mapping::
    
        slaves  = {
            0x01: ModbusSlaveContext(...),
            0x02: ModbusSlaveContext(...),
            0x03: ModbusSlaveContext(...),
        }
        context = ModbusServerContext(slaves=slaves, single=False)
    
    The slave context can also be initialized in zero_mode, which means that
    a request to address(0-7) will map to the address (0-7). The default is
    False, so address(0-7) will map to (1-8)::
    
        store = ModbusSlaveContext(..., zero_mode=True)
    '''
    
    log.debug('Initializing data store...')
    store = ModbusSlaveContext(
        di=ModbusSequentialDataBlock(0, [17]*100),
        co=ModbusSequentialDataBlock(0, [17]*100),
        hr=ModbusSequentialDataBlock(0, [17]*100),
        ir=ModbusSequentialDataBlock(0, [17]*100))

    context = ModbusServerContext(slaves=store, single=True)

    log.debug('Initializing server information...')
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'Apparent Energy'
    identity.ProductCode = 'AEMS'
    identity.VendorUrl = 'http://apparent.com'
    identity.ProductName = 'MODbus Server'
    identity.ModelName = 'MODbus Server'
    identity.MajorMinorRevision = '0.1'

    log.debug('Starting MODbus server...')
    StartTcpServer(context, identity=identity, address=("localhost", 5020))


if __name__ == "__main__":
    run_server()
