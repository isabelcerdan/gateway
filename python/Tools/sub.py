import zmq

xpub_url = "tcp://127.0.0.1:60887"

def subscriber(ctx):
    sub = ctx.socket(zmq.SUB)
    sub.connect(xpub_url)
    sub.setsockopt(zmq.SUBSCRIBE, "")
    while True:
        topic, data = sub.recv_multipart()
        print "received %s|%s" % (topic, data)

if __name__ == '__main__':
    ctx = zmq.Context()
    subscriber(ctx)
