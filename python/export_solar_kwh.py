#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
    scripts/export_readings.py

    This script exports solar production in kwh for a given date range.

    $ python export_solar_kwh.py --begin="2017-07-07 00:00:00" --end="2017-07-07 23:59:59" --tables=pcc_reading_1

    --begin     Time of first reading (format "YYYY-MM-DD mm:hh:ss") to export to CSV. Required.
    --end       Time of last reading (format "YYYY-MM-DD mm:hh:ss") to export to CSV. Default is current time.
"""

from optparse import OptionParser
from datetime import datetime

from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database

fields = [
    "Accumulated_Real_Energy_Net_FP",
    "Accumulated_Real_Energy_A_Import_FP",
    "Accumulated_Real_Energy_B_Import_FP",
    "Accumulated_Real_Energy_C_Import_FP",
    "Accumulated_Real_Energy_A_Export_FP",
    "Accumulated_Real_Energy_B_Export_FP",
    "Accumulated_Real_Energy_C_Export_FP",
    "Total_Net_Instantaneous_Real_Power_FP"
]

def get_solar_reading(timestamp):
    select = "SELECT %s FROM solar_readings_1" % (",".join(fields))
    where = "WHERE timestamp = '%s'" % timestamp
    result = prepared_act_on_database(FETCH_ONE, " ".join([select, where]), [])
    if not result:
        print "No solar readings available for %s" % timestamp
    return result

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-b", "--begin", dest="begin",
                      help="Date and time of first reading to evaluate (format \"YYYY-MM-DD mm:hh:ss\").")
    parser.add_option("-e", "--end", dest="end", default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                      help="Date and time of last reading to evaluate (format \"YYYY-MM-DD mm:hh:ss\"). Default is current time.")
    (options, args) = parser.parse_args()

    begin = get_solar_reading(options.begin)
    end   = get_solar_reading(options.end)

    if begin and end:
        for field, value in begin.iteritems():
            diff = end[field] - begin[field]
            print "%s = %s" % (field, diff)
