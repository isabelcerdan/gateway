#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import os
import time
import math
import socket
import struct
import calendar
from datetime import datetime

import logging
import logging.handlers

import MySQLdb as MyDB
from contextlib import closing
import MySQLdb.cursors as my_cursor

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.StringMonConstants import *
from lib.gateway_constants.GatewayConstants import *


from lib.db import prepared_act_on_database
from lib.logging_setup import *
from lib.helpers import datetime_to_utc

import gateway_utils as gu
import tlv_utils


from threading import Thread
import Queue
from Queue import Empty

from power_status.psa_data_handler import *
from power_status.psa_util import *


# Globals
queue = Queue.Queue(maxsize=0)

PSA_RECEIVE_THREAD_ALIVENESS_CNT = 6
PSA_RECEIVE_SOCKET_TIMEOUT = 5.0
PSA_WAIT_FOR_ENTRY_SECONDS = 30.0
PSA_EMPTY_QUEUE_DEBOUNCE = (PSA_RECEIVE_SOCKET_TIMEOUT * PSA_RECEIVE_THREAD_ALIVENESS_CNT) + 1

# Class to receive the response to the multicast messages.  The responses are pushed on to the queue for handling
# by the process thread class
class PsaReceiveThread(Thread):
    def __init__(self, logger):
        Thread.__init__(self)
        self.psa_recv_socket = None
        self.LAN_address = None
        self.logger = logger

    def setup_recv_socket(self):
        # Initialize the socket. SOCK_DGRAM specifies a UDP datagram.
        self.LAN_address = gu.get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        if self.LAN_address is None:
            self.logger.error('Unable to get LAN IP address to bind to.')
            return False

        try:
            self.psa_recv_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            self.psa_recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.psa_recv_socket.settimeout(PSA_RECEIVE_SOCKET_TIMEOUT)

        except Exception as error:
            err_string = "init socket failed - %s" % error
            self.logger.error(err_string)
            return False
        else:
            # Bind socket to local host and port
            try:
                self.logger.debug("Binding to %s, %d" % (self.LAN_address,
                                                         PSA_HOS_DATA_PORT))
                self.psa_recv_socket.bind((self.LAN_address,
                                           PSA_HOS_DATA_PORT))

            except socket.error, msg:
                err_string = "Bind failed. Error Code : " + str(msg[0]) + " Message " + msg[1]
                self.logger.error(err_string)
                self.close_recv_socket()
                return False

        return True


    def close_recv_socket(self):
        #self.logger.info('Closing Socket')
        error_encountered = False

        if self.psa_recv_socket is None:
            pass
        else:
            try:
                self.psa_recv_socket.close()
            except Exception as error:
                error_string = 'Error closing receive socket - %s' % repr(error)
                self.logger.error(error_string)
                error_encountered = True

        return error_encountered

    def run(self):
        if not self.setup_recv_socket():
            self.logger.error("Unable to set up receive socket for HOS audit thread.")
            return False

        rv = True
        '''
        Start out with three measures of aliveness.  Each measure corresponds
        to one socket timeout or one trip through the message reception loop.
        '''
        aliveness = PSA_RECEIVE_THREAD_ALIVENESS_CNT
        while aliveness > 0:
            try:
                my_data, my_addr = self.psa_recv_socket.recvfrom(1024)
                data_packet = [my_data, my_addr, int(time.time())]

            except socket.timeout:
                '''
                No messages received during socket timeout period.
                Lose one measure of aliveness.
                '''
                aliveness -= 1

                continue
            except Exception as error:
                self.logger.error('Socket receive error - %s' % error)
                rv = False
                break

            try:
                queue.put(data_packet)
            except Exception as err:
                self.logger.error('Error putting message data on queue - %s' & err)
                rv = False
                break

            else:
                '''
                Successful queue put - gain one measure of aliveness.
                '''
                aliveness += 1
            '''
            Each trip through the loop costs one measure of aliveness.
            '''
            aliveness -= 1

        self.close_recv_socket()
        return rv

# This class processes the response message put on the queue by the receive thread. The PsaProcessData class does the
# actual work of unpacking the response and building the display string
class PsaProcessThread(Thread):
    def __init__(self, logger, fp, utils):
        Thread.__init__(self)
        self.outputFP = fp
        self.logger = logger
        self.psa_util = utils

    def handleResponse(self, ipAddr, tlvRsp, msgRxTime):

        sn = gu.get_serial_number_from_ip(ipAddr)

        lt = time.localtime(msgRxTime)
        rspString = time.strftime("%Y-%m-%d %H:%M:%S", lt)
        rspString += " {:<15s}".format(ipAddr)
        rspString += " {:<12s}".format(sn)

        pd = PsaProcessData(ipAddr, tlvRsp, self.logger)
        rspString += " {:02d} ".format(RUNMODE_SG424_APPLICATION)
        pd.processData(msgRxTime)
        rspString += pd.shortDump()

        self.outputFP.write(rspString + LINE_TERMINATION)
        self.outputFP.flush()

        return True

    def run(self):
        queue_err_cnt = 0
        queue_empty_cnt = PSA_EMPTY_QUEUE_DEBOUNCE

        while True:
            try:
                port_packet = queue.get(timeout=PSA_WAIT_FOR_ENTRY_SECONDS)

            except Empty:
                '''
                Timeout on waiting for an entry to be placed on the queue.  
                TODO: ADD 'ALIVENESS' LOGIC HERE SO THE THREAD DOESN'T SHUT DOWN PREMATURELY.
                '''
                self.logger.info('Timeout waiting for a PSA response')
                return True

            except Exception as error:
                error_string = 'queue get failed - %s' % repr(error)
                self.logger.error(error_string)
                queue_err_cnt += 1
                if queue_err_cnt > 5:
                    self.logger.error('Unable to retrieve events from queue.')
                    return False
                continue

            try:
                queue_err_cnt = 0

                # arguments are ip address, response data, and received time
                self.handleResponse(port_packet[1][0], port_packet[0], port_packet[2])

                queue.task_done()
                if queue.empty():
                    queue_empty_cnt = PSA_EMPTY_QUEUE_DEBOUNCE

                    '''
                    If we haven't received another response for 5000ms, we probably
                    won't get any more.  Consider our work here done.
                    '''
                    while queue_empty_cnt > 0:
                        queue_empty_cnt -= 1
                        time.sleep(1)
                        if not queue.empty():
                            break

                        # nothing for PSA_EMPTY_QUEUE_DEBOUNCE seconds
                        if queue_empty_cnt == 0:
                            #self.logger.info('Done waiting for queue to empty')
                            return True

            except Exception as err:
                error_string = 'Unable to process HOS responses: %s' % repr(err)
                self.logger.error(error_string)
                return False


# This class sends the message
class PsaSendMulticastMsg(object):

    def __init__(self, logger, runMode, msgTlv):

        self.SG424_data = []
        self.psa_send_socket = None
        self.msgTlv = msgTlv
        self.runMode = runMode
        self.logger = logger

    def setup_stringmon_send_socket(self):
        try:
            self.psa_send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ttl = struct.pack('b', 1)
            self.psa_send_socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

        except socket.error as err:
            err_string = "Send socket programming failed %s" % repr(err)
            self.logger.error(err_string)
            return False

        else:
            return True

    def close_send_socket(self):
        error_encountered = False

        if self.psa_send_socket is None:
            pass

        else:
            try:
                self.psa_send_socket.close()

            except Exception as error:
                error_string = 'Error closing send socket - %s' % repr(error)
                self.logger.error(error_string)
                error_encountered = True

        return error_encountered

    def send(self):
        if not self.setup_stringmon_send_socket():
            critical_string = "Unable to setup send socket for HOS audit thread - exiting."
            print(critical_string)
            self.logger.error(critical_string)
            return  # Thread will enter the join() in the main thread.

        while True:
            user_tlvs = 1

            if (self.runMode == PSA_RUN_MODE_MCAST_ALL):
                tlvData = PSA_MCAST_ALL
            else:
                tlvData = PSA_MCAST_HOS

            self.logger.debug("Sending PSA TLV msg=%d, data=%d" % (self.msgTlv, tlvData))
            self.SG424_data = gu.add_master_tlv(user_tlvs)
            self.SG424_data = gu.append_user_tlv_16bit_unsigned_short(self.SG424_data,
                                                                      self.msgTlv,
                                                                      tlvData)
            try:
                self.psa_send_socket.sendto(self.SG424_data,
                                            (HOS_QUERY_MULTICAST_ADDRESS,
                                             SG424_PRIVATE_UDP_PORT))

            except socket.error:
                err_string = "socket error detected sending psa discovery request"
                self.logger.error(err_string)
                return False

            break

        self.close_send_socket()
        return True

# this is the main class for managing sending the multicast message and waiting for the responses.
class PsaMulticastMessage(object):

    def __init__(self, logger, fp, runMode, msgTlv, utils):
        self.logger = logger
        self.outputFP = fp
        self.runMode = runMode
        self.msgTlv = msgTlv
        self.dateString = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        self.psa_util = utils

    def send(self):
        recv_thread_alive = False
        process_thread_alive = False

        self.PsaReceiveThread = PsaReceiveThread(self.logger)

        self.PsaProcessThread = PsaProcessThread(self.logger,
                                                 self.outputFP,
                                                 self.psa_util)

        self.PsaSendMsg = PsaSendMulticastMsg(self.logger,
                                              self.runMode,
                                              self.msgTlv)

        try:
            self.logger.debug("Starting PSA response processing thread...")
            self.PsaProcessThread.daemon = True
            self.PsaProcessThread.start()
        except:
            self.logger.error("Unable to start response processing thread.")
            return False

        try:
            self.logger.debug("Starting PSA Receive thread...")
            self.PsaReceiveThread.daemon = True
            self.PsaReceiveThread.start()
        except:
            self.logger.error("Unable to start receive thread.")
            return False

        try:
            waitCnt = 0
            while ((not self.PsaReceiveThread.is_alive()) and
                       (not self.PsaProcessThread.is_alive()) and
                       (waitCnt < 2)):
                time.sleep(1)
                waitCnt+=1

            if (waitCnt < 2):
                recv_thread_alive = True
                process_thread_alive = True
                self.PsaSendMsg.send()

            else:
                self.logger.error("Process and/or Rx threads did not start")

        except Exception:
            self.logger.error("Unable to start multicast thread.")
            return False

        # check if receive and msg process threads are still running
        # update the PSA control 'still running' epoch in Db
        while True:
            if recv_thread_alive:
                self.PsaReceiveThread.join(0.5)
                if not self.PsaReceiveThread.is_alive():
                    self.logger.debug('PSA Receive thread completed.')
                    recv_thread_alive = False

            if process_thread_alive:
                self.PsaProcessThread.join(0.5)
                if not self.PsaProcessThread.is_alive():
                    self.logger.debug('PSA Process thread completed.')
                    process_thread_alive = False

            if not (process_thread_alive or recv_thread_alive):
                break

            self.psa_util.updateRunning()

        if (self.runMode == PSA_RUN_MODE_MCAST_ALL):
            didNotRespondList = self.psa_util.create_list_not_resp_before_timeString(self.dateString)
        else:
            didNotRespondList = self.psa_util.create_hos_list_not_resp_before_timeString(self.dateString)

        rspTimeStr = time.strftime("%Y-%m-%d %H:%M:%S")

        for sn in didNotRespondList:
            rspString = rspTimeStr
            rspString += " {:<15s}".format(didNotRespondList[sn])
            if sn > 0:
                rspString += " {:<12d}".format(sn)
            else:
                rspString += " {:<12s}".format("None")
            rspString += " {:02d}  | No_Response".format(RUNMODE_UNKNOWN)
            self.outputFP.write(rspString + LINE_TERMINATION)
            gu.update_inverters_comm_column_by_ip(INVERTERS_TABLE_COMM_STRING_NO_COMM,
                                              didNotRespondList[sn])
            self.outputFP.flush()

        return True


