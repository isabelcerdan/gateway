#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import socket
import struct
import time
import datetime
import os
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.GatewayConstants import *
from gateway_utils import *
import tlv_utils

from lib.db import prepared_act_on_database
from lib.logging_setup import *

from collections import namedtuple


def _build_fake_psa_power_data(dcWatts):
    fake = []
    powerData = bytearray(24)

    '''
    Create TLV Header, Type and Length
    '''
    fake = struct.pack("!HH", SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE, 24)

    '''
    Create Data
    '''
    struct.pack_into("!BBBBBB", powerData, psaPowerDataMac.offset, 0xe0, 0x1f, 0x0a, 0x01,0x58, 0x67)
    struct.pack_into("!I", powerData, psaPowerDataUptime.offset, 0xabcdef55)
    struct.pack_into("!H", powerData, psaPowerDataCurtail.offset, 0x1234)
    struct.pack_into("!I", powerData, psaPowerDataRampUp.offset, 0x01234567)
    struct.pack_into("!I", powerData, psaPowerDataRampDown.offset, 0x01234567)
    struct.pack_into("!H", powerData, psaPowerDataFallback.offset, 0xaabb)
    struct.pack_into("!H", powerData, psaPowerDataDcWatts.offset, dcWatts)

    fake += powerData

    return fake


def _build_fake_psa_current_status():
    fake = []
    currentStatus = bytearray(20)

    fake = struct.pack("!HH", SG424_TO_GTW_CURRENT_STATUS_STRUCTURED_TLV_TYPE, 20)

    struct.pack_into("!B", currentStatus, psaStatusDataVersion.offset, 0x01)
    struct.pack_into("!B", currentStatus, psaStatusDataStates.offset, 0x02)
    struct.pack_into("!B", currentStatus, psaStatusDataAdmin.offset, 0x03)
    struct.pack_into("!B", currentStatus, psaStatusDataFill.offset, 0x00)
    struct.pack_into("!I", currentStatus, psaStatusDataOperational.offset, 0x01234567)
    struct.pack_into("!I", currentStatus, psaStatusDataAvailability.offset, 0x0000aabb)
    struct.pack_into("!I", currentStatus, psaStatusDataPower.offset, 0x0000ccdd)
    struct.pack_into("!I", currentStatus, psaStatusDataInfo.offset, 0x00001234)

    fake += currentStatus

    return fake


def _build_fake_psa_latched_status():
    fake = []
    latchedStatus = bytearray(20)

    fake = struct.pack("!HH", SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE, 20)

    struct.pack_into("!B", latchedStatus, psaStatusDataVersion.offset, 0x01)
    struct.pack_into("!B", latchedStatus, psaStatusDataStates.offset, 0x02)
    struct.pack_into("!B", latchedStatus, psaStatusDataAdmin.offset, 0x03)
    struct.pack_into("!B", latchedStatus, psaStatusDataFill.offset, 0x00)
    struct.pack_into("!I", latchedStatus, psaStatusDataOperational.offset, 0x01234567)
    struct.pack_into("!I", latchedStatus, psaStatusDataAvailability.offset, 0x0000aabb)
    struct.pack_into("!I", latchedStatus, psaStatusDataPower.offset, 0x0000ccdd)
    struct.pack_into("!I", latchedStatus, psaStatusDataInfo.offset, 0x00001234)

    fake += latchedStatus

    return fake

# ------------------------------------------------------------------------------------------------------
def get_fake_psa_power_status_response(mac):
    fake = []

    fake = struct.pack("!HHH", SG424_TO_GTW_MASTER_U16_TLV_TYPE, MASTER_TLV_DATA_FIELD_LENGTH, 3)

    fake += _build_fake_psa_power_data(int(mac[15], 16) + int(mac[16], 16))
    fake += _build_fake_psa_current_status()
    fake += _build_fake_psa_latched_status()

    return fake
