#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8


import gateway_utils as gu
import tlv_utils
from inverterMessages.tlvMsgPeek import *
from inverterMessages.tlvObjectList import *
from inverterMessages.tlvMasterSG424 import *
from inverterMessages.tlvPsaPower import *
from inverterMessages.tlvPsaStatusCurrent import *
from inverterMessages.tlvPsaStatusLatched import *



# process the power and status responses to TLVs POWER_STATUS and POWER_STATUS_ALL from
# the inverter and build a decoded string for display
class PsaProcessData(object):
    def __init__(self, ipAddr, rspData, logger):
        self.rspData = rspData
        self.ipAddr = ipAddr
        self.powerObj = None
        self.latchedStausObj = None
        self.currentStatusObj = None
        self.logger = logger

    def processData(self, rxTime):
        try:
            tlvList = msgPeek(False, self.rspData)
            msgs = tlvList.getTlvDict()

            for key in msgs:
                try:
                    obj = TlvObjects(key, self.ipAddr)
                    tlvObj = obj.getObj()
                    #print(tlvObj)
                    tlvObj.unpackDataStruct(msgs[key])

                    if tlvObj.getTlvId() == SG424_TO_GTW_MASTER_U16_TLV_TYPE:
                        #print("master count = %u" % tlvObj.count)
                        pass
                    elif tlvObj.getTlvId() == SG424_TO_GTW_POWER_STATUS_STRUCTURED_TLV_TYPE:
                        self.powerObj = tlvObj
                        self.powerObj.persistData(rxTime)
                    elif tlvObj.getTlvId() == SG424_TO_GTW_LATCHED_STATUS_STRUCTURED_TLV_TYPE:
                        self.latchedStausObj = tlvObj
                    elif tlvObj.getTlvId() == SG424_TO_GTW_CURRENT_STATUS_STRUCTURED_TLV_TYPE:
                        self.currentStatusObj = tlvObj
                    else:
                        self.logger.error("Unknown response TLV, id=%u" % tlvObj.tlvId())

                except  Exception as e:
                    self.logger.error(("PsaProcessData, Can't parse TLV id=%u,  " % key) + str(e))

        except Exception as e:
            errorStr =  "Unable to parse response: " + str(e)
            self.logger.error(errorStr)

    def longDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.powerObj is not None:
                rspString += self.powerObj.dumpDecodedBuf()

            if self.latchedStausObj is not None:
                rspString += self.latchedStausObj.dumpDecodedBuf()

            if self.currentStatusObj is not None:
                rspString += self.currentStatusObj.dumpDecodedBuf()

        except Exception as e:
            errorStr =  "Unable to build long response buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)

        return rspString

    def shortDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.powerObj is not None:
                rspString += " | " + self.powerObj.dumpShortDecodedBuf()

            if self.latchedStausObj is not None:
                rspString += "|" + self.latchedStausObj.dumpShortDecodedBuf()

            if self.currentStatusObj is not None:
                rspString += " |" + self.currentStatusObj.dumpShortDecodedBuf()

        except Exception as e:
            errorStr =  "Unable to build short response buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)

        return rspString

    def getShortHeaders(self):
        labelString = ""
        itemsString = ""

        try:
            #Build response string in following order
            if self.powerObj is not None:
                str1, str2 = self.powerObj.dumpHeaderStrings()
                labelString += " | " + str1
                itemsString += " | " + str2

            if self.latchedStausObj is not None:
                str1, str2 = self.latchedStausObj.dumpHeaderStrings()
                labelString += " | " + str1
                itemsString += " | " + str2

            if self.currentStatusObj is not None:
                str1, str2 = self.currentStatusObj.dumpHeaderStrings()
                labelString += " | " + str1
                itemsString += " | " + str2


        except Exception as e:
            errorStr =  "Unable to build short response buffer: " + str(e)
            labelString += errorStr
            itemsString += errorStr
            self.logger.error(errorStr)

        return labelString, itemsString

    def hexDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.powerObj is not None:
                rspString += self.powerObj.dumpDataBuf()

            if self.latchedStausObj is not None:
                rspString += self.latchedStausObj.dumpDataBuf()

            if self.currentStatusObj is not None:
                rspString += self.currentStatusObj.dumpDataBuf()

        except Exception as e:
            errorStr =  "Unable to build short response buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)

        return rspString

#
# process the query response to QUERY_INVERTER TLV from the inverters
class PsaProcessQueryData(object):
    def __init__(self, ipAddr, rspData):
        self.rspData = rspData
        self.ipAddr = ipAddr

    def process(self):
        this_database_row_list = gu.get_db_inverters_row_data(self.ipAddr)

        # Response received from the inverter. Compare the data against that in the database.

        (tlv_id_list, tlv_values_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(self.rspData)
        tlv_utils.TLVTranslator.verify_psa_query_data_against_db(tlv_id_list,
                                                                         tlv_values_list,
                                                                         this_database_row_list,
                                                                         self.ipAddr)

        return tlv_id_list, tlv_values_list



