#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
import sys

# Writing the auditor's results file:
LINE_TERMINATION = "\r\n"

# -------------------------------------------------------------------------
#

PSA_DECODE_COLUMN_WIDTH = 15

# list of items in input string
PSA_DECODE_DATE                 = 0
PSA_DECODE_TIME                 = 1
PSA_DECODE_IP_ADDR              = 2
PSA_DECODE_SN                   = 3
PSA_DECODE_RUN_MODE             = 4
PSA_DECODE_SEP_1                = 5
PSA_DECODE_POWER_MAC            = 6
PSA_DECODE_POWER_UPTIME         = 7
PSA_DECODE_POWER_DC_VOLTS       = 8
PSA_DECODE_POWER_DC_CURR        = 9
PSA_DECODE_POWER_DC_WATTS       = 10
PSA_DECODE_POWER_PHASE_ANGLE    = 11
PSA_DECODE_POWER_AC_VRMS        = 12
PSA_DECODE_POWER_AC_CURR        = 13
PSA_DECODE_POWER_WAC            = 14
PSA_DECODE_POWER_VAR_REACT_PWR  = 15
PSA_DECODE_POWER_FREQ_LINE      = 16
PSA_DECODE_POWER_TEMP_C         = 17
PSA_DECODE_SEP_2                = 18
PSA_DECODE_LATCHED_VER          = 19
PSA_DECODE_LATCHED_USAGE_STATE  = 20
PSA_DECODE_LATCHED_OP_STATE     = 21
PSA_DECODE_LATCHED_ADMIN_STATE  = 22
PSA_DECODE_LATCHED_AVAILABLE_SB = 23
PSA_DECODE_LATCHED_POWER_SB     = 24
PSA_DECODE_LATCHED_INFO_SB      = 25
PSA_DECODE_LATCHED_ADMIN_SB     = 26
PSA_DECODE_LATCHED_OPER_SB      = 27
PSA_DECODE_SEP_3                = 28
PSA_DECODE_CURRENT_AVAILABLE_SB = 29
PSA_DECODE_CURRENT_POWER_SB     = 30
PSA_DECODE_CURRENT_INFO_SB      = 31
PSA_DECODE_CURRENT_ADMIN_SB     = 32
PSA_DECODE_CURRENT_OPER_SB      = 33

# for bad response on up to SN is in string to decode
PSA_DECODE_SIZE_HEADER_DATA     = PSA_DECODE_SEP_1 + 1
PSA_DECODE_SIZE_BAD_RESPONSE    = PSA_DECODE_SIZE_HEADER_DATA + 1

# decoder strings for enum states and bitfields
PSA_TOKEN_DECODER_TYPE = 0
PSA_TOKEN_DECODER_LIST = 1

PSA_DECODE_USAGE_STATE_STRINGS = [
    "ENUM",
    [ "Idle",
      "Active" ]
]

PSA_DECODE_OPER_STATE_STRINGS = [
    "ENUM",
    [ "Enabled",
      "Disabled" ]
]

PSA_DECODE_ADMIN_STATE_STRINGS = [
    "ENUM",
    [ "Unlocked",
      "Locked" ]
]

PSA_DECODE_AVAILABLE_BF_STRINGS = [
    "BF",
    [ "IDLE",
      "ACTIVE",
      "STARTUP",
      "NIGHT",
      "SLEEP",
      "GW_CTRL",
      "GW_GONE"]
]

PSA_DECODE_POWER_BF_STRINGS = [
    "BF",
    [ "MPP",
      "MAX",
      "LIMITED",
      "GW_CURT",
      "GW_REG",
      "GW_FB",
      "IMMED",
      "VW",
      "FW",
      "VAR_PRI",
      "IOUT" ]
]

PSA_DECODE_INFO_BF_STRINGS = [
    "BF",
    [ "TRO"]
]

PSA_DECODE_ADMIN_BF_STRINGS = [
    "BF",
    [ "UNLOCKED",
      "LOCKED",
      "LOCKED_LP"]
]


PSA_DECODE_OPER_BF_STRINGS = [
    "BF",
    [ "ENABLED",
      "DISABLED",
      "UNINIT - TBD)",
      "DIAG_FAIL",
      "TEMP_STBY",
      "BOB",
      "DCOVP",
      "OCP",
      "TROV2",
      "NO_IGN",
      "HIGH_TEMP",
      "LOW_TEMP",
      "DCIN_P",
      "DCIN_V",
      "DCIN_I",
      "ACOV_1",
      "ACOV_2",
      "ACOV_3",
      "PLL_LOL",
      "PLL_NOLOK",
      "PLL_ISLAND",
      "BAD_PROFILE" ]
]

# Array of toke definations for items to display
PSA_TOKEN_ENUM    = 0
PSA_TOKEN_STRING  = 1
PSA_TOKEN_DISPLAY = 2
PSA_TOKEN_TYPE    = 3
PSA_TOKEN_DECODER = 4


tokDefination = [
    [PSA_DECODE_DATE,                 "Date",         1, "S", None],
    [PSA_DECODE_TIME,                 "Time",         1, "S", None],
    [PSA_DECODE_IP_ADDR,              "IP Addr",      1, "S", None],
    [PSA_DECODE_SN,                   "Serial Num",   1, "S", None],
    [PSA_DECODE_RUN_MODE,             "Mode",         0, "S", None],
    [PSA_DECODE_SEP_1,                "Sep",          0, "S", None],
    [PSA_DECODE_POWER_MAC,            "MAC",          1, "S", None],
    [PSA_DECODE_POWER_UPTIME,         "Uptime",       1, "D", None],
    [PSA_DECODE_POWER_DC_VOLTS,       "DC Volts",     1, "F", None],
    [PSA_DECODE_POWER_DC_CURR,        "DC Curr",      1, "F", None],
    [PSA_DECODE_POWER_DC_WATTS,       "DC Watts",     1, "D", None],
    [PSA_DECODE_POWER_PHASE_ANGLE,    "Phase Angle",  1, "D", None],
    [PSA_DECODE_POWER_AC_VRMS,        "AC Vrms",      1, "F", None],
    [PSA_DECODE_POWER_AC_CURR,        "AC Curr",      1, "F", None],
    [PSA_DECODE_POWER_WAC,            "WAC",          1, "F", None],
    [PSA_DECODE_POWER_VAR_REACT_PWR,  "Var React Pwr",1, "F", None],
    [PSA_DECODE_POWER_FREQ_LINE,      "Freq Hz",      1, "F", None],
    [PSA_DECODE_POWER_TEMP_C,         "Temp C",       1, "F", None],
    [PSA_DECODE_SEP_2,                "Sep",          0, "S", None],
    [PSA_DECODE_LATCHED_VER,          "Version",      1, "D", None],
    [PSA_DECODE_LATCHED_USAGE_STATE,  "Usage Enum (L)",   1, "E", PSA_DECODE_USAGE_STATE_STRINGS],
    [PSA_DECODE_LATCHED_OP_STATE,     "Oper Enum (L)",    1, "E", PSA_DECODE_OPER_STATE_STRINGS],
    [PSA_DECODE_LATCHED_ADMIN_STATE,  "Admin Enum (L)",   1, "E", PSA_DECODE_ADMIN_STATE_STRINGS],
    [PSA_DECODE_LATCHED_AVAILABLE_SB, "Available Bits (L)", 1, "H", PSA_DECODE_AVAILABLE_BF_STRINGS],
    [PSA_DECODE_LATCHED_POWER_SB,     "Power Bits (L)",   1, "H", PSA_DECODE_POWER_BF_STRINGS],
    [PSA_DECODE_LATCHED_INFO_SB,      "Info Bits (L)",    1, "H", PSA_DECODE_INFO_BF_STRINGS],
    [PSA_DECODE_LATCHED_ADMIN_SB,     "Admin Bits (L)",   1, "H", PSA_DECODE_ADMIN_BF_STRINGS],
    [PSA_DECODE_LATCHED_OPER_SB,      "Oper Bits (L)",    1, "H", PSA_DECODE_OPER_BF_STRINGS],
    [PSA_DECODE_SEP_3,                "Sep",              0, "S", None],
    [PSA_DECODE_CURRENT_AVAILABLE_SB, "Available Bits (C)", 1, "H", PSA_DECODE_AVAILABLE_BF_STRINGS],
    [PSA_DECODE_CURRENT_POWER_SB,     "Power Bits (C)",   1, "H", PSA_DECODE_POWER_BF_STRINGS],
    [PSA_DECODE_CURRENT_INFO_SB,      "Info Bits (C)",    1, "H", PSA_DECODE_INFO_BF_STRINGS],
    [PSA_DECODE_CURRENT_ADMIN_SB,     "Admin Bits (C)",   1, "H", PSA_DECODE_ADMIN_BF_STRINGS],
    [PSA_DECODE_CURRENT_OPER_SB,      "Oper Bits (C)",    1, "H", PSA_DECODE_OPER_BF_STRINGS]
]




class PsaDecode(object):

    def __init__(self):
        self.dataStr = ""
        self.outputStr  = ""
        self.tokenList = []

    def psa_decode_cmd_line(self):
        if len(sys.argv) <= 1:
            print("No args")
        else:
            self.psa_decode_input_str(str(sys.argv[1]))

    def psa_decode_print_tokens(self):
        print("Input Str tokens=%s" % str(self.tokenList))

    def psa_decode_output(self):
        return self.outputStr

    def psa_decode_input_str(self, dataStr):
        self.dataStr = dataStr
        self.tokenList = [str(x) for x in dataStr.split(' ') if x.strip()]

        # setup some column spacing
        leftJustifiedColumn = ("{:>%ds}" % PSA_DECODE_COLUMN_WIDTH)
        rightJustifiedColumn = ("{:<%ds}" % PSA_DECODE_COLUMN_WIDTH)

        # loop through the input
        for x in range(0, min(len(tokDefination), len(self.tokenList))):
            tok = tokDefination[x]

            if tok[PSA_TOKEN_DISPLAY] == 1:

                # get the display string and display value and build a string
                ts = rightJustifiedColumn.format(tok[PSA_TOKEN_STRING]) + \
                     "= " + \
                     self.tokenList[tok[PSA_TOKEN_ENUM]]

                # bit of a hack here - older inverter applications will not get
                # all data - so display the inverter mode as the last entry
                if  x == (len(self.tokenList) - 1):
                    if len(self.tokenList) == PSA_DECODE_SIZE_BAD_RESPONSE:
                        ts = rightJustifiedColumn.format(tokDefination[PSA_DECODE_RUN_MODE][PSA_TOKEN_STRING]) + \
                         "= " + \
                         self.tokenList[tok[PSA_TOKEN_ENUM]]

                # output non-decoded line
                if ((tok[PSA_TOKEN_TYPE] != "E") and
                    (tok[PSA_TOKEN_TYPE] != "H") ):
                    self.outputStr += ts + LINE_TERMINATION

                # output decode item
                elif (tok[PSA_TOKEN_DECODER] != None):
                    decoderType = tok[PSA_TOKEN_DECODER][PSA_TOKEN_DECODER_TYPE]
                    decoderList = tok[PSA_TOKEN_DECODER][PSA_TOKEN_DECODER_LIST]
                    val = int(self.tokenList[tok[0]], 16)

                    if (decoderType == "ENUM"):
                        if (val < len(decoderList)):
                            ts += " (" + decoderList[val] + ")"
                        else:
                            ts = "(Unknown)"

                        self.outputStr += ts + LINE_TERMINATION

                    elif (decoderType == "BF"):
                        ts = rightJustifiedColumn.format(tok[PSA_TOKEN_STRING]) + \
                             "= 0x" + \
                             self.tokenList[tok[PSA_TOKEN_ENUM]]
                        self.outputStr += ts + LINE_TERMINATION

                        checkedBits = 0

                        # output bit field items
                        for x in range(0, len(decoderList)):
                            mask = (1 << x)
                            checkedBits += mask

                            if (mask & val):
                                bs = "Set"
                            else:
                                bs = "Clear"

                            ts = leftJustifiedColumn.format(" ") + \
                                 "  [{:>02d}] ".format(x) + \
                                 rightJustifiedColumn.format(decoderList[x]) + \
                                 "= " + \
                                 bs
                            self.outputStr += ts + LINE_TERMINATION

                        # output a line item if any unknown bits are set
                        if ((~checkedBits & val) > 0):
                            ts = leftJustifiedColumn.format(" ") + \
                                 "  " + \
                                 rightJustifiedColumn.format("Unknown Bits") + \
                                 "= "
                            ts += "0x{:x}".format((val & (~checkedBits)))
                            self.outputStr += ts + LINE_TERMINATION

                    else:
                        self.outputStr += ts + LINE_TERMINATION
                else:
                    self.outputStr += ts + LINE_TERMINATION





def main():
    bb = PsaDecode()
    bb.psa_decode_cmd_line()
    print(bb.psa_decode_output())

if __name__ == '__main__':
    #print("__name__= __main__ psa_decode.py")
    main()

'''

