#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import time

import gateway_utils as gu
from lib.db import prepared_act_on_database
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *

INTERRUPTION_REASON_NONE = "none"
INTERRUPTION_REASON_UPGRADE = "Inverter_upgrade"
INTERRUPTION_REASON_FEED_DISCOVERY = "Feed_discovery"
INTERRUPTION_REASON_DELAY = "Delay"
INTERRUPTION_REASON_GTW_IN_OBSERVATION_MODE = "Observation_mode"

class PsaUtil(object):
    def __init__(self, logger, daemon=None):
        self.post_interruption_delay = 0
        self.isInterrupted = False
        self.interrupt_reason = INTERRUPTION_REASON_NONE
        self.last_update_time = 0

        self.logger = logger
        self.daemon = daemon

    def get_last_time(self):
        return self.last_update_time

    def set_last_time(self, now):
        self.last_update_time = now

    #
    # Utility function to call the PSA update epoch function if it has not been called
    # in the last SET_UPDATE_BIT_FREQUENCY (5) seconds
    #
    def updateRunning(self):
        if self.daemon is not None:
            now = int(time.time())

            if (self.get_last_time() + SET_UPDATE_BIT_FREQUENCY) <= now:
                self.daemon.update()
                self.set_last_time(now)

    def psa_get_run_now(self):
        run_now = False
        select_string = "SELECT %s FROM %s;" % (DB_PSA_TBL_RUN_NOW_COLUMN_NAME, DB_PSA_CONTROL_TABLE)
        db_data = prepared_act_on_database(FETCH_ONE, select_string, ())
        run_now_column = db_data[DB_PSA_TBL_RUN_NOW_COLUMN_NAME]

        if run_now_column == PSA_RUN_NOW_COLUMN_YES:
            run_now = True

        return run_now

    def psa_reset_run_now(self):
        update_string = "UPDATE %s SET %s = %d;" % (DB_PSA_CONTROL_TABLE,
                                                    DB_PSA_TBL_RUN_NOW_COLUMN_NAME,
                                                    PSA_RUN_NOW_COLUMN_NO)
        prepared_act_on_database(EXECUTE, update_string, ())

    # Check if the Query loop should be interrupted
    #
    def check_for_interruption_reason(self):
        wasInterrupted = self.isInterrupted

        self.isInterrupted = True

        if gu.is_inverter_upgrade_in_progress():
            ts = "PSA interrupted -> upgrade"
            self.post_interruption_delay = 60
            self.interrupt_reason = INTERRUPTION_REASON_UPGRADE

        elif gu.is_feed_discovery_in_progress():
            ts = "PSA interrupted -> feed discovery"
            self.post_interruption_delay = 60
            self.interrupt_reason = INTERRUPTION_REASON_FEED_DISCOVERY

        elif gu.is_gateway_in_observation_mode():
            ts = "PSA interrupted -> observation mode"
            self.interrupt_reason = INTERRUPTION_REASON_GTW_IN_OBSERVATION_MODE

        elif (self.post_interruption_delay > 0):
            self.post_interruption_delay -= 1
            ts = "PSA Delay, time left=%s sec" % self.post_interruption_delay
            self.interrupt_reason = INTERRUPTION_REASON_DELAY

        else:
            self.isInterrupted = False
            self.interrupt_reason = INTERRUPTION_REASON_NONE

        if self.isInterrupted:
            if not wasInterrupted:
                self.logger.info(ts)
        else:
            if wasInterrupted:
                self.logger.info("Interruption cleared")

        return self.isInterrupted

    def log_if_interrupted(self):
        ts = "PSA interrupted due to "

        if  ((self.interrupt_reason == INTERRUPTION_REASON_UPGRADE) or
             (self.interrupt_reason == INTERRUPTION_REASON_FEED_DISCOVERY) or
             (self.interrupt_reason == INTERRUPTION_REASON_GTW_IN_OBSERVATION_MODE)):
            ts += self.interrupt_reason
            self.logger.info(ts)

        elif self.interrupt_reason == INTERRUPTION_REASON_DELAY:
            ts += self.interrupt_reason
            ts += " time left=%s sec" % self.post_interruption_delay
            self.logger.info(ts)


    #
    #  Wait loop
    #  -- used between inverter queries and at end of query loop
    #  -- minimum delay is 100 msec
    #  -- check for changes once per second
    #
    def wait_for_timeout(self, next_start):
        need_to_interrupt = False

        updateCheckCntr = 1
        while True:
            if time.time() >= next_start:
                break
            time.sleep(0.100)

            if updateCheckCntr >= 10:
                updateCheckCntr = 0
                self.updateRunning()

                if self.check_for_interruption_reason():
                    need_to_interrupt = True

                if self.psa_get_run_now():
                    self.psa_reset_run_now()
                    break

            updateCheckCntr += 1

        return need_to_interrupt

    def set_psa_run_start_time(self):
        set_string = "UPDATE %s SET %s=%s;" % (DB_PSA_CONTROL_TABLE,
                                               DB_PSA_TBL_RUN_START_TIME_COLUMN_NAME, "%s")

        set_args = (time.strftime("%Y-%m-%d %H:%M:%S"),)
        prepared_act_on_database(EXECUTE, set_string, set_args)
        return

    def set_psa_run_end_time(self):
        set_string = "UPDATE %s SET %s=%s;" % (DB_PSA_CONTROL_TABLE,
                                               DB_PSA_TBL_RUN_END_TIME_COLUMN_NAME, "%s")

        set_args = (time.strftime("%Y-%m-%d %H:%M:%S"),)
        prepared_act_on_database(EXECUTE, set_string, set_args)
        return

    def set_psa_last_response_time(self, ipAddr):
        this_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        gu.set_inverter_last_response_timestamp(ipAddr, this_date_time)

    def psa_get_run_mode(self):
        run_mode = PSA_RUN_MODE_UNKNOWN
        select_string = "SELECT %s FROM %s;" % (DB_PSA_TBL_RUN_MODE_COLUMN_NAME, DB_PSA_CONTROL_TABLE)
        db_data = prepared_act_on_database(FETCH_ONE, select_string, ())
        run_mode_column = db_data[DB_PSA_TBL_RUN_MODE_COLUMN_NAME]

        if run_mode_column == PSA_RUN_MODE_IDLE:
            run_mode = PSA_RUN_MODE_IDLE
        elif run_mode_column == PSA_RUN_MODE_UNICAST:
            run_mode = PSA_RUN_MODE_UNICAST
        elif run_mode_column == PSA_RUN_MODE_MCAST_ALL:
            run_mode = PSA_RUN_MODE_MCAST_ALL
        elif run_mode_column == PSA_RUN_MODE_MCAST_HOS:
            run_mode = PSA_RUN_MODE_MCAST_HOS

        return run_mode

    #
    # Create a list of the inverters in the system
    #
    # This may not be the 1st time through the list of inverters. Membership in the inverters
    # table may change for each new PSA loop. So empty the inverters_id_list if necessary.
    # Not doing so could result in interesting and fun side effects. Like a memory leak.
    #
    @staticmethod
    def create_inverters_id_list():
        inverter_list = []

        select_string = "SELECT %s FROM %s ORDER BY %s;" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                            DB_INVERTER_NXO_TABLE,
                                                            INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
        all_inverters_id = prepared_act_on_database(FETCH_ALL, select_string, ())
        count = len(all_inverters_id)
        if count > 0:
            # Add the inverters_id to the list.
            for each_inverters_id in all_inverters_id:
                this_inverter_id = each_inverters_id[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME]
                inverter_list.append(this_inverter_id)

        return (count, inverter_list)

    @staticmethod
    def create_inverters_id_list_from_list(input_list):
        inverter_list = []

        select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                            DB_INVERTER_NXO_TABLE,
                                                            INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                            "%s")
        for ipAddr in input_list:
            select_args = (ipAddr,)
            select_result = prepared_act_on_database(FETCH_ONE, select_string, select_args)

            if select_result:
                id = select_result[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME]
                inverter_list.append(id)
            else:
                print("IP %s NOT IN THE DATABASE" % (ipAddr))

        return (len(inverter_list), inverter_list)

    def create_list_not_resp_before_timeString(self, timeString):
        inverter_list = {}

        select_string = "SELECT %s,%s FROM %s WHERE %s < %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                 INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                                                                 DB_INVERTER_NXO_TABLE,
                                                                 INVERTERS_TABLE_LAST_RESP_COLUMN_NAME,
                                                                 "%s")

        select_args = (timeString,)
        all_inverters_id = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        count = len(all_inverters_id)
        if count > 0:
            # if there is no serial numbers then set key to a negative value
            badSn = -1

            # Add the inverters_id to the list.
            for inverter in all_inverters_id:
                if inverter[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME] is not None:
                    inverter_list[int(inverter[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME])] = \
                    inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                else:
                    inverter_list[badSn] = inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                    badSn -= 1

        return (inverter_list)

    def create_hos_list_not_resp_before_timeString(self, timeString):
        inverter_list = {}

        select_string = "SELECT %s,%s FROM %s WHERE %s = 1 AND %s < %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                            INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME,
                                                                            DB_INVERTER_NXO_TABLE,
                                                                            INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                            INVERTERS_TABLE_LAST_RESP_COLUMN_NAME,
                                                                            "%s")

        select_args = (timeString,)
        all_inverters_id = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        count = len(all_inverters_id)
        if count > 0:
            # if there is no serial numbers then set key to a negative value
            badSn = -1

            # Add the inverters_id to the list.
            for inverter in all_inverters_id:
                if inverter[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME] is not None:
                    inverter_list[int(inverter[INVERTERS_TABLE_SERIAL_NUMBER_COLUMN_NAME])] = \
                                  inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                else:
                    inverter_list[badSn] = inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                    badSn -= 1

        return (inverter_list)

    def create_hos_inverters_id_list(self):
        inverter_list = []

        select_string = "SELECT %s FROM %s WHERE %s <= 1 ORDER BY %s;" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                                        DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                        INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
        all_inverters_id = prepared_act_on_database(FETCH_ALL, select_string, ())
        count = len(all_inverters_id)
        if count == 0:
            # Empty database?
            self.logger.info("SEEMINGLY EMPTY INVERTERS TABLE")
        else:
            # Add the inverters_id to the list.
            for each_inverters_id in all_inverters_id:
                this_inverter_id = each_inverters_id[INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME]
                inverter_list.append(this_inverter_id)

        return (count, inverter_list)



