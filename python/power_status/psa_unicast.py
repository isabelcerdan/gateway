#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import os
import time
import math
import socket
import struct
import calendar
from datetime import datetime

import logging
import logging.handlers

import MySQLdb as MyDB
from contextlib import closing
import MySQLdb.cursors as my_cursor

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.StringMonConstants import *
from lib.gateway_constants.GatewayConstants import *


from lib.db import prepared_act_on_database
from lib.logging_setup import *
from lib.helpers import datetime_to_utc

import gateway_utils as gu
import tlv_utils

from power_status.psa_data_handler import *
from power_status.psa_util import *

# class for sending unicast message
class PsaUnicastMessage(object):
    def __init__(self, logger, fp, inverter_id_list, delayTime, tlvMsgId, utils, longDump):
        self.tlvMsgId = tlvMsgId
        self.outputFP = fp
        self.logger = logger
        self.inverter_id_list = inverter_id_list
        self.query_delay = delayTime/ 1000.0
        self.psa_util = utils
        self.inverter_id_not_responding = []
        self.longDump = longDump

    def send_and_get_psa_query_data(self, ip_addr, tlvMsgId):

        # Assumption: the IP has been validated to the extent that it looks like an IP.
        private_port_packet = None
        SG424_data = None
        number_of_user_tlv = 1
        packed_data = gu.add_master_tlv(number_of_user_tlv)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, tlvMsgId, 1234)

        #print("TLV query = %d" % tlvMsgId)

        # OPEN THE SOCKET:
        try:
            fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        except Exception:  # as error:
            print("socket.socket failed ...")

        else:
            # SEND THE PACKET:
            try:
                fancy_socks.sendto(packed_data, (ip_addr, SG424_PRIVATE_UDP_PORT))

            except Exception:  # as error:
                print("sendto failed ...")

            # Now wait for a response:
            else:
                try:
                    fancy_socks.settimeout(3.0)
                    packet = fancy_socks.recvfrom(1024)

                except Exception:
                    # Timed out waiting to receive a packet. Simply means the inverter
                    # at that IP is not responding.
                    pass

                else:
                    # Packet received, data is in private_port_packet field.
                    (addr, port, SG424_data) = gu.get_udp_components(packet)

                    '''
                    print("SG424_data")
                    print(len(SG424_data))
                    for i in range(0, len(SG424_data)):
                       print("[%03d] %s" % (i, hex(ord(SG424_data[i]))))
                    '''

            # Close the socket:
            try:
                fancy_socks.close()

            except Exception:  # as error:
                print("close socket failed ...")

            else:
                pass

        return SG424_data


    def send_psa_message(self, inverter_id, ip_addr, sn, tlvMsgId):

        query_successful = False

        # For the results file:
        self.logger.debug("quering IP=%s" % ip_addr)

        rspString = time.strftime("%Y-%m-%d %H:%M:%S")
        rspString += " {:<15s}".format(ip_addr)
        rspString += " {:<12s}".format(sn)

        for iggy in range(0, 2):
            psa_data = self.send_and_get_psa_query_data(ip_addr, tlvMsgId)

            if psa_data:
                query_successful = True
                pd = PsaProcessData(ip_addr, psa_data, self.logger)
                rspString += " {:02d} ".format(RUNMODE_SG424_APPLICATION)
                pd.processData(int(time.time()))
                rspString += pd.shortDump()

                self.outputFP.write(rspString + LINE_TERMINATION)
                self.outputFP.flush()

                if (self.longDump):
                    print(pd.longDump())

                break
            else:
                self.psa_util.updateRunning()

        if not query_successful:
            self.inverter_id_not_responding.append(inverter_id)

        return query_successful, rspString

    def send_query_message(self, inverter_id, ip_addr, sn, tlvMsgId):

        query_successful = False

        # For the results file:
        self.logger.debug("quering IP=%s" % ip_addr)

        rspString = time.strftime("%Y-%m-%d %H:%M:%S")
        rspString += " {:<15s}".format(ip_addr)
        rspString += " {:<12s}".format(sn)

        for iggy in range(0, 2):
            psa_data = self.send_and_get_psa_query_data(ip_addr, tlvMsgId)

            if psa_data:
                query_successful = True
                pd = PsaProcessQueryData(ip_addr, psa_data)
                tlvList, tlvValuesList = pd.process()
                self.psa_util.set_psa_last_response_time(ip_addr)

                if (SG424_TO_GTW_RUNMODE_U16_TLV_TYPE in tlvList):
                    idx = tlvList.index(SG424_TO_GTW_RUNMODE_U16_TLV_TYPE)
                    rspString += " {:02d} ".format(tlvValuesList[idx])

                    if tlvValuesList[idx] == RUNMODE_SG424_APPLICATION:
                        rspString += " | SG424_Application_Mode "
                    elif tlvValuesList[idx] == RUNMODE_SG424_BOOT_LOADER:
                        rspString += " | SG424_Boot_Loader_Mode "
                    elif tlvValuesList[idx] == RUNMODE_SG424_BOOT_UPDATER:
                        rspString += " | SG424_Boot_Updater_Mode "
                    elif tlvValuesList[idx] == RUNMODE_LEGACY_SG424_APPLICATION:
                        rspString += " | SG424_Legacy_Mode "
                    elif tlvValuesList[idx] == RUNMODE_LEGACY_SG424_BOOT_LOADER:
                        rspString += " | SG424_Legacy_Loader_Mode "
                    elif tlvValuesList[idx] == RUNMODE_LEGACY_SG424_BOOT_UPDATER:
                        rspString += " | SG424_Legacy_Updater_Mode "
                    else:
                        rspString += " | SG424_UnKnown_Mode "

                else:
                    rspString += " {:02d} ".format(RUNMODE_UNKNOWN)
                    rspString += " | Unknown_Mode "

                self.outputFP.write(rspString + LINE_TERMINATION)
                self.outputFP.flush()

                #for i in range(0, len(tlvList)):
                #    print ("[%02d] TLV[%04d] = %s" % (i, tlvList[i], tlvValuesList[i]))

                break
            else:
                self.psa_util.updateRunning()

        if not query_successful:
            # 2 tries, no response. Write NO RESPONSE against this inverter. Set the database COMM field to NO_COMM.
            rspString += " {:02d}  | No_Response".format(RUNMODE_UNKNOWN)
            self.outputFP.write(rspString + LINE_TERMINATION)
            gu.update_inverters_comm_column_by_ip(INVERTERS_TABLE_COMM_STRING_NO_COMM, ip_addr)
            self.outputFP.flush()

        return query_successful, rspString

    # send unicast PSA query to each device in turn.
    # if there is no response to the new message then the older style QUERY message is sent.
    def startSendAll(self):
        interrupted = False

        for inverters_list_index in range(0, len(self.inverter_id_list)):
            self.psa_util.updateRunning()
            inverter_id = self.inverter_id_list[inverters_list_index]

            (ip_addr, mac, sn, string_id, string_position) = \
                gu.base_inverter_info_from_inverters_id(inverter_id)

            # Get the counters and data of interest:
            if ip_addr:
                # ----------------------------------------
                self.send_psa_message(inverter_id, ip_addr, sn, self.tlvMsgId)
                # ----------------------------------------
            else:
                self.logger.info("PASSING OVER NULL IP FROM INVERTERS ID " + str(inverter_id))

            # also check for interruption
            if self.psa_util.wait_for_timeout(time.time() + self.query_delay):
                interrupted = True
                break
        # end query loop

        # if the loop was not interrupted, and there are devices which did not respond to PSA query,
        # then send old style query - backward compatibility
        if not interrupted:
            for inverters_list_index in range(0, len(self.inverter_id_not_responding)):
                inverter_id = self.inverter_id_not_responding[inverters_list_index]

                (ip_addr, mac, sn, string_id, string_position) = \
                    gu.base_inverter_info_from_inverters_id(inverter_id)

                # Get the counters and data of interest:
                if ip_addr:
                    # ----------------------------------------
                    self.send_query_message(inverter_id, ip_addr, sn, GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE)
                    # ----------------------------------------
                else:
                    self.logger.info("PASSING OVER NULL IP FROM INVERTERS ID " + str(inverter_id))


    # send PSA query to one device and return the formatted response string
    # if new message fails, send the old style query message
    def startSendOne(self, inverter_id):
        ok = False
        rspString = ""

        self.psa_util.updateRunning()

        (ip_addr, mac, sn, string_id, string_position) = \
            gu.base_inverter_info_from_inverters_id(inverter_id)

        # Get the counters and data of interest:
        if ip_addr:
            ok, rspString = self.send_psa_message(inverter_id, ip_addr, sn, self.tlvMsgId)

            if not ok:
                ok, rspString = self.send_query_message(inverter_id, ip_addr, sn, GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE)

        else:
            self.logger.info("PASSING OVER NULL IP FROM INVERTERS ID " + str(inverter_id))

        return ok, rspString
