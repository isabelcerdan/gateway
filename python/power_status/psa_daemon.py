#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_PSA_DAEMON_RESTARTED

from power_status.psa_multicast import *
from power_status.psa_unicast import *
from power_status.psa_util import *
from inverterMessages.tlvPsaPower import *
from inverterMessages.tlvPsaStatusCurrent import *
from inverterMessages.tlvPsaStatusLatched import *




# A few useful definitions:
INTERRUPTION_REASON_NONE = "NO_INTERRUPTION"
INTERRUPTION_REASON_UPGRADE = "INVERTER_UPGRADE"
INTERRUPTION_REASON_FEED_DISCOVERY = "FEED_DISCOVERY"
INTERRUPTION_REASON_SUSPENSION = "SUSPENSION"
INTERRUPTION_REASON_GTW_IN_OBSERVATION_MODE = "OBSERVATION"
INTERRUPTION_REASON_DELAY = "DELAY"


class PsaDaemon(Daemon):

    alarm_id = ALARM_ID_PSA_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(PsaDaemon, self).__init__(**kwargs)
        self.inverters_to_query = 0         # number of queries to do in query period
        self.my_logger = None               # logging handle
        self.detail_log = 0                 # detailed INFO log info flag, read from the Db
        self.inverter_rank = 0              # index into list of inverters, range 0 -> self.inverters_to_query
        self.inter_query_delay = 1000       # delay between the inverter queries
        self.inter_loop_delay = 60          # delay between query loops
        self.wait_period_B4_start = 0       # delay at start up
        self.run_mode = PSA_RUN_MODE_UNICAST

        self.interruption_reason = INTERRUPTION_REASON_NONE   # reason to block the query from running
        self.isInterrupted = False                            # True if query loop not querying

        self.loco_loop_timer = 0                # used to limit updating of Db control table time to >= SET_UPDATE_BIT_FREQUENCY seconds
        self.string_id_list = []                # lists of inverter strings re-determined at start of query loop
        self.inverters_id_list = []             # list of inverter found - updated on each loop
        self.inverters_id = 0                   # index of current ID in inverters_id_list[], used as Db Index

        # data read from base_inverter_info_from_inverters_id(self.inverters_id)
        # updated on each interaction of the loop
        self.inverters_ip = None                # IP address of inverter
        self.mac_address = None                 # MAC address of the inverter
        self.serial_number = None               # serial number of the inverter
        self.string_id = None                   # string ID of the inverter
        self.string_position = None             # position index in string

        self.get_next_inverter_status_utc = 0   # epoch when next inverter should be queried, forces delay btwn queries

        self.setup_logger()
        self.psa_util = PsaUtil(self.my_logger, self)

    #
    # Logging functions
    #
    def setup_logger(self):
        self.my_logger = setup_logger("psactrl")

    def detail_log_info(self, message):
        if self.detail_log == 1:
            self.log_info("Detail: " + message)

    def log_info(self, message):
        self.my_logger.info(message)

    def log_error(self, message):
        self.my_logger.error(message)

    #
    # For all inverters in DB
    # -- set comm status to UNKNOWN
    # -- set DC Watts to 0
    #
    def set_all_comm_to_unknown(self):
        update_string = "UPDATE %s SET %s = %s, %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                             INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                             INVERTERS_TABLE_WATTS_COLUMN_NAME, "%s")
        update_args = (INVERTERS_TABLE_COMM_STRING_UNKNOWN, 0)
        prepared_act_on_database(EXECUTE, update_string, update_args)


    #
    # At PSA start up check if inverter comm'd to gateway in last PSA run.
    # -- set comm status to UNKNOWN
    # -- set DC Watts to 0
    def set_non_responsive_comm_to_unknown(self):
        select_string = "SELECT %s,%s from %s;" % (DB_PSA_TBL_RUN_START_TIME_COLUMN_NAME,
                                                   DB_PSA_TBL_RUN_END_TIME_COLUMN_NAME,
                                                   DB_PSA_CONTROL_TABLE)

        string_id_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        last_run_start = string_id_row[DB_PSA_TBL_RUN_START_TIME_COLUMN_NAME]
        last_run_end = string_id_row[DB_PSA_TBL_RUN_END_TIME_COLUMN_NAME]

        update_string = "UPDATE %s SET %s = %s, %s = %s WHERE last_resp < %s;" % (DB_INVERTER_NXO_TABLE,
                                                             INVERTERS_TABLE_COMM_COLUMN_NAME, "%s",
                                                             INVERTERS_TABLE_WATTS_COLUMN_NAME, "%s", "%s")

        # if first or last not set, then reset all
        # can occur if database not set correctly
        if ((last_run_start is None) or (last_run_end is None)):
            update_args = (INVERTERS_TABLE_COMM_STRING_UNKNOWN, 0, time.strftime("%Y-%m-%d %H:%M:%S"))
            self.log_info("last_run_start or last_run_end is None")

        else:
            self.detail_log_info("last_run_start=%s, last_run_end=%s" % (last_run_start, last_run_end))
            if last_run_end < last_run_start:
                update_args = (INVERTERS_TABLE_COMM_STRING_UNKNOWN, 0, last_run_end)
            else:
                update_args = (INVERTERS_TABLE_COMM_STRING_UNKNOWN, 0, last_run_start)

        prepared_act_on_database(EXECUTE, update_string, update_args)


    def is_psa_first_start(self):
        time_is_null = False
        select_string = "SELECT %s from %s;" % (DB_PSA_TBL_RUN_START_TIME_COLUMN_NAME,
                                                DB_PSA_CONTROL_TABLE)

        string_id_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        self.log_info("is_psa_first_start() string_id_row=%s" % string_id_row)

        first_time = string_id_row[DB_PSA_TBL_RUN_START_TIME_COLUMN_NAME]
        self.log_info("is_psa_first_start() first_time=%s" % first_time)

        if not first_time:
            time_is_null = True
        return time_is_null

    #
    # Get PSA Control table data from the database
    #  -- data could change randomly, so check at beginning of query loop
    #
    def retrieve_psa_parameters(self):
        select_string = "SELECT * FROM %s;" % DB_PSA_CONTROL_TABLE
        psa_control_row = prepared_act_on_database(FETCH_ONE, select_string, ())

        self.detail_log = psa_control_row[DB_PSA_TBL_DETAIL_LOG_COLUMN_NAME]
        self.inter_query_delay = psa_control_row[DB_PSA_TBL_INTER_QUERY_DELAY_COLUMN_NAME]
        self.inter_loop_delay = psa_control_row[DB_PSA_TBL_INTER_LOOP_DELAY_COLUMN_NAME]
        self.wait_period_B4_start = psa_control_row[DB_PSA_TBL_WAIT_B4_START_COLUMN_NAME]
        self.run_mode = psa_control_row[DB_PSA_TBL_RUN_MODE_COLUMN_NAME]

        # The queries is updated through every loop based on the size of the inverters table,
        # and index is updated after each query. It provides an psa progress indication.
        self.inverters_to_query = 0
        self.inverter_rank = 0

        # -----------------------------------------------------------------
        # The list of data may change from loop to loop, so it is re-created
        # each time thru an PSA loop.
        self.inverters_to_query, self.inverters_id_list = self.psa_util.create_inverters_id_list()
        self.number_of_string_ids, self.string_id_list = self.psa_util.create_hos_inverters_id_list()

    #
    #
    # Main loop
    #
    def run(self):
        # ---------------------------------------------------------------------
        # Retrieve running parameters from the psa table. Update the total
        # number of queries and the query rank.
        self.retrieve_psa_parameters()

        # Wait until the "wait period before start" expires.
        this_string = "Wait before start=%d sec" % (self.wait_period_B4_start)
        self.log_info(this_string)

        for _ in range(0, self.wait_period_B4_start):
            time.sleep(1)
            self.psa_util.updateRunning()

        # Before doing anything useful, set the comm field to unknown for inverters.
        # that did not respond to PSA during the last query loop. This will apply
        # the big black dot in the comm column for all inverters in the system. The
        # Watts field is also set to 0.
        gu.update_all_inverter_status_in_db(INVERTERS_TABLE_STATUS_STRING_NOT_USED)

        if self.is_psa_first_start():
            self.log_info("PSA First Start")
            self.set_all_comm_to_unknown()
        else:
            self.set_non_responsive_comm_to_unknown()

        # Run forever until eternity is reached.
        while True:
            time.sleep(1)
            self.psa_util.updateRunning()

            if self.psa_util.check_for_interruption_reason():
                pass
            else:
                self.psa_util.set_psa_run_start_time()
                self.retrieve_psa_parameters()

                this_string = "LOOP start: Run Mode=%s" % (self.run_mode)

                if not ((self.run_mode == "multicast_hos") or (self.run_mode == "multicast_all")):
                    this_string += " LoopDelay=%smSecs, IntervalDelay=%ssecs, Inverters=%d" \
                              % (self.inter_loop_delay,
                                 self.inter_query_delay,
                                 self.inverters_to_query)

                self.log_info(this_string)

                #setup time logging and inter query delay
                loop_start_time_utc = int(time.time())
                self.get_next_inverter_status_utc = loop_start_time_utc

                # Next open the PSA audit file into which the results of the audit will be written.
                try:
                    self.results_file_handle = open(PSAD_RESULTS_PATH_AND_FILE_NAME, 'a')
                except Exception:  # as err_string:
                    # TODO: Not seen during testing. Alarm maybe?
                    print("FAILED TO OPEN %s FILE" % (PSAD_RESULTS_PATH_AND_FILE_NAME))
                    continue

                powerLabelStr, powerItemStr = TlvPsaPower.dumpHeaderStrings()
                latchedLabelStr, latchedItemStr = TlvPsaStatusLatched.dumpHeaderStrings()

                hdrTimeString = time.strftime("%Y-%m-%d %H:%M:%S") + " "
                hdrString = hdrTimeString + \
                            "{:<33}".format(self.run_mode) + \
                            powerLabelStr + \
                            latchedLabelStr
                self.results_file_handle.write(hdrString + LINE_TERMINATION)

                hdrString = hdrTimeString + \
                            "{:<16s}".format("IP Address") + \
                            "{:<13s}".format("serial num") + \
                            "{:<4s}".format("mode") + \
                            powerItemStr + \
                            latchedItemStr
                self.results_file_handle.write(hdrString + LINE_TERMINATION)

                if self.run_mode == PSA_RUN_MODE_UNICAST:
                    ucastMsg = PsaUnicastMessage(self.my_logger,
                                                 self.results_file_handle,
                                                 self.inverters_id_list,
                                                 self.inter_query_delay,
                                                 GTW_TO_SG424_PSA_POWER_STATUS_U16_TLV_TYPE,
                                                 self.psa_util,
                                                 False)
                    ucastMsg.startSendAll()

                elif (self.run_mode == PSA_RUN_MODE_MCAST_HOS or
                      self.run_mode == PSA_RUN_MODE_MCAST_ALL):
                    mcastMsg = PsaMulticastMessage(self.my_logger,
                                              self.results_file_handle,
                                              self.run_mode,
                                              GTW_TO_SG424_PSA_POWER_STATUS_U16_TLV_TYPE,
                                              self.psa_util)
                    mcastMsg.send()

                self.log_info("LOOP Done, inverters queried=%d time=%s secs: " % \
                              (self.inverters_to_query,
                               (int(time.time()) - loop_start_time_utc)))

                self.results_file_handle.close()
                self.psa_util.updateRunning()
                self.psa_util.log_if_interrupted()

            # end of run

            if len(self.inverters_id_list) > 0:
                del self.inverters_id_list[:]

            if len(self.string_id_list) > 0:
                del self.string_id_list[:]

            self.psa_util.set_psa_run_end_time()
            next_start = int(time.time()) + self.inter_loop_delay
            self.log_info("Next loop start in %d sec at %s" % (self.inter_loop_delay, time.ctime(next_start)))
            self.psa_util.wait_for_timeout(next_start)

        # end while

    # end run function


if __name__ == '__main__':
    PsaDaemon()
