#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import time
import math
from datetime import datetime

import setproctitle

from lib.helpers import datetime_to_utc
import gateway_utils as gu

from daemon.daemon import Daemon

from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.db import prepared_act_on_database

from alarm.alarm import Alarm
from alarm.alarm_constants import *


# Readings from the pcc_readings_1 table can be logged for display/viewing/study,
# but they are very messy. So normally, such logging is turned off.
ENABLE_MESSY_MESSY_LOGGING_OF_PCC_METER_READINGS = False

# For logging function:
LOGGING_FOR_INFO_ONLY = 1
LOGGING_COMMANDED_PF = 2

# Requirements must be met in order to bother inverters with a commanded power factor.
MIN_REQ_ALL_HAVE_BEEN_MET_FULL = 1
MIN_REQ_ALL_HAVE_BEEN_MET_REDUCED = 2
MIN_REQ_PCC_LOW = 3
MIN_REQ_SOLAR_LOW = 4
MIN_REQ_PCC_AND_SOLAR_LOW = 5
MIN_REQ_SOLAR_MAXED_OUT = 6
MIN_REQ_PF_SAME_NO_CHANGE = 7
MIN_REQ_PF_FLAT_SPOT_NO_CHANGE = 8
MIN_REQ_Q_SHEDDING = 9
MIN_REQ_PREF_PF_RE_APPLIED = 10
MIN_REQ_PREF_PF_QUAD_SAME = 11
MIN_REQ_PREF_PF_QUAD_CHANGE = 12
MIN_REQ_PCC_READINGS_REJECT = 13
MIN_REQ_SOLAR_READINGS_REJECT = 14
MIN_REQ_PCC_SOLAR_READINGS_REJECT = 15
MIN_REQ_NOT_DETERMINED = 16
MIN_REQ_DATA_LOGGING_ONLY = 17

# A cute define to remain in the daemon's primary while loop.
TO_INFINITY_AND_BEYOND = True

# This script was launched by the monitor to provide power factor control. There is a control sequence between
# the monitor and power factor control ("updated") to keep this daemon going.
#
# This script is a single-process daemon, and provides power factor control for all configured feeds.
# Hence the rather extensive use of lists for the manipulation of variables across feeds, as well as a
# goodly number of for loops to cycle through the feeds.

# ---------------------------------------------------------------------------------------------------------------------
#
# CONVENTION, CONVENTION, CONVENTION GALORE ...
#
# The following convention is adhered to for polarity regarding PCC meter readings and GUI display of real power
# and reactive power:
#
# -> real power readings from the pcc_readings_1 table:
#    - positive value = importing real energy on the PCC ... CONSUMPTION
#    - negative value = exporting real energy on the PCC ... PRODUCTION
#
#    NOTE: when PCC real power readings are received by the EnergyReview server, the display has the polarity reversed.
#
#
# -> real power readings from the solar_readings_1 table:
#    - negative value = inverters are producing energy ... PRODUCTION
#    - positive value = inverters are consuming energy ... CONSUMPTION
#
#
# For the benefit of the user, the GUI will display:
#
#
# - "PCC Meter" column may be +ve or -ve for consumption (import) or production (export) depending on FEED configuration
# - "Power Generation" column is positive when the inverters are producing power
# - at night: the "Power Generation" column would be negative, meaning the inverters are in low power, not producing
#             any power. The power consumption by such a field of inverters can be relatively high, hence the desire
#             to be able to put them into "sleep mode"; this is a whole other issue.
#
#
# POWER QUADRANT:
#
# There is a 4-quadrant (I, II, III, IV) power indicator in the GUI for each configured feed.
# Based on PCC meter readings, power at the PCC can be in 1 or 4 quadrants:
#
#
#
#                        II              |                  I
#                                        |
#                        INDUCTIVE       |    INDUCTIVE
#                        P-              |    P+
#                        Q+              |    Q+
#                        EXPORT          |    IMPORT
#                                        |
#                     ---------------------------------------
#                        CAPACITIVE      |    CAPACITIVE
#                                        |
#                        P-              |    P+
#                        Q-              |    Q-
#                        EXPORT          |    IMPORT
#                                        |
#                        III             |                 IV
#
#
# where: INDUCTIVE = current lagging,
#        CAPACITIVE = current leading
#        quadrant II and III -> power EXPORTED on the PCC
#        quadrant  I and  IV -> power IMPORTED on the PCC
#
# With "pure" NXO control, the system should always be in either quadrant 1 or 4.
#
#
# ---------------------------------------------------------------------------------------------------------------------


class PowerFactorControlDaemon(Daemon):

    base_name = "pfcd"
    alarm_id = ALARM_ID_POWER_FACTOR_SANITY

    def __init__(self, **kwargs):
        super(PowerFactorControlDaemon, self).__init__(**kwargs)

        self.number_of_pcc_readings_1 = 1
        self.last_gui_readings_utc = 0
        self.last_inverter_update_utc = 0
        self.current_utc = 0

        self.wait_period_B4_start = 0

        self.pf_command_frequency = 0
        self.last_meter_readings_rejected = False
        self.percent_reactive_power_to_harvest = 0.0
        self.minimum_reactive_power_threshold = 0.0
        self.minimum_real_power_threshold = 0.0
        self.num_readings_for_pf_upd = 0

        # When a list of reactive power meter readings is extracted from the database, the readings are accepted
        # if the number of readings above or below zero exceed the following percentage:
        self.minimum_acceptable_meter_readings_percent = 0.0

        # When a preferred power factor condition is active, the system is in a steady state where reactive power at
        # the PCC and SOLAR do not waver more than the following percentage:
        self.preferred_q_condition_waver_percent = 0.0

        # When PCC reactive power flips quadrant, the system may enter into a preferred Pf condition. If the
        # reactive power at the PCC and SOLAR have changed by more than the following percentage, the change
        # is considered large, and preferred Pf condition does not apply:
        self.q_change_no_preef_q_condition_percent = 0.0

        # When PCC reactive power is being brought down towards its target, if the new target would bring the PCC down
        # below the minimal Q threshold, then the amount of reactive power to harvest must be reduced by the following:
        self.reduced_harvest_percent = 0.0

        self.log_power_factor_data_frequency = 65535
        self.last_log_power_factor_control_utc = 0
        self.first_time_capability_calculated = False
        self.var_capability_calculated_at_least_once = False
        self.feed_name_list = []
        self.power_factor_control_enable_list = []
        self.power_factor_control_command_logging_list = []
        self.power_factor_bounce_data_logging_list = []
        self.power_factor_control_data_logging_list = []

        self.num_dynamic_pf_commands = 1
        self.logging_number = 1
        self.multicast_all_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)

        self.latest_commanded_pf_list = []
        self.latest_commanded_pf_c_lead_list = []

        self.preferred_pf_activated_list = []
        self.preferred_pf_pcc_reactive_power_list = []
        self.preferred_pf_pcc_quadrant_list = []
        self.preferred_pf_solar_reactive_power_list = []
        self.preferred_pf_candidate_list = []
        self.preferred_pf_c_lead_candidate_list = []
        self.previous_commanded_pf_list = []
        self.previous_commanded_pf_c_lead_list = []

        # TODO: might want to rename all xxx_reactive_power_xxx to: xxx_vars_xxx

        self.pcc_real_power_fp_name_list = []
        self.pcc_amperage_fp_name_list = []
        self.pcc_reactive_power_fp_name_list = []

        self.solar_real_power_fp_name_list = []
        self.solar_amperage_fp_name_list = []
        self.solar_reactive_power_fp_name_list = []

        self.pcc_real_power_average_list = []
        self.pcc_amperage_average_list = []
        self.pcc_reactive_power_average_list = []
        self.pcc_last_quadrant_list = []
        self.pcc_reactive_power_readings_is_valid_list = []

        self.solar_real_power_average_list = []
        self.solar_amperage_average_list = []
        self.solar_reactive_power_average_list = []
        self.solar_q_to_harvest_list = []
        self.solar_q_target_list = []
        self.solar_reactive_power_readings_is_valid_list = []

        # These variables support meter reading for GUI power factor/quadrant display. No amperage values.
        self.gui_samples_per_interval = 0
        self.gui_max_size = 0
        self.gui_frequency = 0
        self.gui_samples = 0
        self.gui_logging = GUI_LOG_NO

        self.gui_pcc_readings_real_power_list = []
        self.gui_pcc_readings_reactive_power_list = []

        self.gui_pcc_averages_real_power_name_list = []
        self.gui_pcc_averages_reactive_power_name_list = []
        self.gui_pcc_averages_quadrant_name_list = []

        self.gui_pcc_averages_real_power_list = []
        self.gui_pcc_averages_reactive_power_list = []

    def log_info(self, message):
        self.logger.info(message)

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)

    def gui_pf_window_logging(self):
        # This method, invoked regularly in this daemon's main loop, has nothing to do with PF control.
        # Its sole purpose is to read the pcc_readings_1 table and populate the interval averages table,
        # to support the GUI's display of the "PCC reactive power quadrant I, II, III or IV".
        if self.is_gui_update_meter_readings_timer_expired():
            # On a regular basis, retrieve a number of PCC readings and average them for
            # the "power factor meter" display. These readings are taken whether or not
            # power factor control is running.
            self.gui_read_pcc_readings_1_for_display()

            # -------------------------------------------------------------------------------------
            if ENABLE_MESSY_MESSY_LOGGING_OF_PCC_METER_READINGS:
                # The following logging functions are useful in a development environment.
                # When the above macro is set to True, the logging of the data can be quite messy.
                self.log_messy_pcc_real_power_meter_readings()
                self.log_messy_pcc_reactive_power_meter_readings()
            # -------------------------------------------------------------------------------------

            self.gui_calculate_average_real_power()
            self.gui_calculate_average_reactive_power()

            # -------------------------------------------------------------------------------------
            if ENABLE_MESSY_MESSY_LOGGING_OF_PCC_METER_READINGS:
                self.log_messy_interval_averages_real_power()
                self.log_messy_interval_averages_reactive_power()
            # -------------------------------------------------------------------------------------

            self.gui_write_all_pcc_averages()

    def get_power_factor_control_parameters(self):
        # Reads the power factor control table to retrieve the latest set of power factor control running parameters.
        #
        # This method invoked through each pass in the main loop so that any change to these parameters during normal
        # operation can take effect without having to stop/start normal nxo control, or without having to force kill
        # the power factor control daemon. Since this database read is occuring once per second, it has negligible
        # impact on system resources.
        select_string = "SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s FROM %s;" \
                                                    % (POWER_FACTOR_CONTROL_TABLE_NUM_READINGS_FOR_PF_UPD_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_ACCEPT_METER_READS_PCT_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_PREF_Q_WAVER_PCT_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_Q_CHANGE_NO_PREF_PCT_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_REDUCED_HARVEST_PCT_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_PF_COMMAND_FREQ_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_PERCENT_Q_TO_HARVEST_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_MINIMUM_Q_THRESHOLD_COLUMN_NAME,
                                                       POWER_FACTOR_CONTROL_TABLE_MINIMUM_P_THRESHOLD_COLUMN_NAME,
                                                       POWER_FACTOR_ENABLE_A,
                                                       POWER_FACTOR_ENABLE_B,
                                                       POWER_FACTOR_ENABLE_C,
                                                       POWER_FACTOR_LOG_A,
                                                       POWER_FACTOR_LOG_B,
                                                       POWER_FACTOR_LOG_C,
                                                       POWER_FACTOR_BOUNCE_DATA_LOG_A,
                                                       POWER_FACTOR_BOUNCE_DATA_LOG_B,
                                                       POWER_FACTOR_BOUNCE_DATA_LOG_C,
                                                       POWER_FACTOR_DATA_LOG_A,
                                                       POWER_FACTOR_DATA_LOG_B,
                                                       POWER_FACTOR_DATA_LOG_C,
                                                       POWER_FACTOR_DATA_LOG_FREQUENCY,
                                                       POWER_FACTOR_CONTROL_TABLE_GUI_LOG_COLUMN_NAME,
                                                       DB_POWER_FACTOR_CONTROL_CONTROL_TABLE)
        pfc_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        if pfc_row:
            self.num_readings_for_pf_upd = pfc_row[POWER_FACTOR_CONTROL_TABLE_NUM_READINGS_FOR_PF_UPD_COLUMN_NAME]
            self.minimum_acceptable_meter_readings_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_ACCEPT_METER_READS_PCT_COLUMN_NAME]
            self.preferred_q_condition_waver_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_PREF_Q_WAVER_PCT_COLUMN_NAME]
            self.q_change_no_preef_q_condition_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_Q_CHANGE_NO_PREF_PCT_COLUMN_NAME]
            self.reduced_harvest_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_REDUCED_HARVEST_PCT_COLUMN_NAME]
            self.pf_command_frequency = pfc_row[POWER_FACTOR_CONTROL_TABLE_PF_COMMAND_FREQ_COLUMN_NAME]
            self.percent_reactive_power_to_harvest = (pfc_row[POWER_FACTOR_CONTROL_TABLE_PERCENT_Q_TO_HARVEST_COLUMN_NAME] * 1.0) / 100
            self.minimum_reactive_power_threshold = (pfc_row[POWER_FACTOR_CONTROL_TABLE_MINIMUM_Q_THRESHOLD_COLUMN_NAME] * 1.0) / 1000
            self.minimum_real_power_threshold = (pfc_row[POWER_FACTOR_CONTROL_TABLE_MINIMUM_P_THRESHOLD_COLUMN_NAME] * 1.0) / 1000
            self.power_factor_control_enable_list[0] = pfc_row[POWER_FACTOR_ENABLE_A]
            self.power_factor_control_enable_list[1] = pfc_row[POWER_FACTOR_ENABLE_B]
            self.power_factor_control_enable_list[2] = pfc_row[POWER_FACTOR_ENABLE_C]
            self.power_factor_control_command_logging_list[0] = pfc_row[POWER_FACTOR_LOG_A]
            self.power_factor_control_command_logging_list[1] = pfc_row[POWER_FACTOR_LOG_B]
            self.power_factor_control_command_logging_list[2] = pfc_row[POWER_FACTOR_LOG_C]
            self.power_factor_bounce_data_logging_list[0] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_A]
            self.power_factor_bounce_data_logging_list[1] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_B]
            self.power_factor_bounce_data_logging_list[2] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_C]
            self.power_factor_control_data_logging_list[0] = pfc_row[POWER_FACTOR_DATA_LOG_A]
            self.power_factor_control_data_logging_list[1] = pfc_row[POWER_FACTOR_DATA_LOG_B]
            self.power_factor_control_data_logging_list[2] = pfc_row[POWER_FACTOR_DATA_LOG_C]
            self.log_power_factor_data_frequency = pfc_row[POWER_FACTOR_DATA_LOG_FREQUENCY]
            self.gui_logging = pfc_row[POWER_FACTOR_CONTROL_TABLE_GUI_LOG_COLUMN_NAME]

    def is_gui_update_meter_readings_timer_expired(self):
        readings_expired = False
        if self.current_utc - self.last_gui_readings_utc >= self.gui_frequency:
            # The "gui frequency" for update period has expired, time to gather up and calculate
            # averages for the GUI's reactive power and "power quadrant" display.
            readings_expired = True
        return readings_expired

    def is_pf_command_frequency_time_expired(self):
        inverter_update_expired = False
        if self.current_utc - self.last_inverter_update_utc >= self.pf_command_frequency:
            # Time to read meters to calculate and determine if new power factor should be commanded.
            inverter_update_expired = True
        elif self.last_meter_readings_rejected:
            # The last readings were rejected. Take another stab, but only if the
            # normal command frequency is greater than the AFTER_REJECT_TIMEOUT.
            if PFC_METER_RE_READ_AFTER_REJECT_TIMEOUT < self.pf_command_frequency:
                if self.current_utc - self.last_inverter_update_utc >= PFC_METER_RE_READ_AFTER_REJECT_TIMEOUT:
                    inverter_update_expired = True
                    self.last_meter_readings_rejected = False

        return inverter_update_expired

    def is_pf_logging_timer_expired(self):
        pf_logging_expired = False
        if self.current_utc - self.last_log_power_factor_control_utc >= self.log_power_factor_data_frequency:
            # It is time to log a new set of power factor data.
            pf_logging_expired = True
            self.last_log_power_factor_control_utc = self.current_utc
        return pf_logging_expired

    def gui_read_pcc_readings_1_for_display(self):

        self.last_gui_readings_utc = self.current_utc
        self.number_of_pcc_readings_1 += 1

        for iggy in range(0, len(self.feed_name_list)):
            # Retrieve the last set of PCC Pf/real/reactive power readings from the pcc_readings_1
            # table. The table's entry_id and row_id are not extracted.
            select_string = "SELECT %s,%s FROM %s WHERE %s='%s' AND %s=%s;" % (self.pcc_real_power_fp_name_list[iggy],
                                                                             self.pcc_reactive_power_fp_name_list[iggy],
                                                                             DB_METER_READINGS_TABLE,
                                                                             METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                                                                             METER_ROLE_PCC1,
                                                                             METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                                                                             METER_DATA_TYPE_LAST_VALUE)

            pcc_readings_1_rows = prepared_act_on_database(FETCH_ALL, select_string, ())
            if pcc_readings_1_rows:
                pop = 0
                for pcc_readings_1 in pcc_readings_1_rows:
                    self.gui_pcc_readings_real_power_list[iggy][pop] = pcc_readings_1[self.pcc_real_power_fp_name_list[iggy]]
                    self.gui_pcc_readings_reactive_power_list[iggy][pop] = pcc_readings_1[self.pcc_reactive_power_fp_name_list[iggy]]
                    pop += 1

    # -----------------------------------------------------------------------------------
    # MESSY MESSY MESSY ...
    #
    # The following set of functions: useful during development, but comes with messy output to the log.
    # Generally and normally not used.
    def log_messy_pcc_real_power_meter_readings(self):
        for iggy in range(0, len(self.feed_name_list)):
            this_string = "REAL POWER METER READINGS # %s FEED %s -> %s SAMPLES:" % (self.number_of_pcc_readings_1,
                                                                                     self.feed_name_list[iggy],
                                                                                     self.gui_samples_per_interval)
            for pop in range(0, self.gui_samples_per_interval):
                this_string += "%s," % (self.gui_pcc_readings_real_power_list[iggy][pop])
            self.logger.info(this_string)

    def log_messy_pcc_reactive_power_meter_readings(self):
        for iggy in range(0, len(self.feed_name_list)):
            this_string = "REACTIVE POWER METER READINGS # %s FEED %s -> %s SAMPLES:" % (self.number_of_pcc_readings_1,
                                                                                         self.feed_name_list[iggy],
                                                                                         self.gui_samples_per_interval)
            for pop in range(0, self.gui_samples_per_interval):
                this_string += "%s," % (self.gui_pcc_readings_reactive_power_list[iggy][pop])
            self.logger.info(this_string)

    def log_messy_interval_averages_real_power(self):
        this_string = "INTERVAL AVERAGE REAL POWER:"
        for iggy in range(0, len(self.feed_name_list)):
            this_string += " FEED %s/%s" % (self.feed_name_list[iggy], self.gui_pcc_averages_real_power_list[iggy])
        self.logger.info(this_string)

    def log_messy_interval_averages_reactive_power(self):
        this_string = "INTERVAL AVERAGE REACTIVE POWER:"
        for iggy in range(0, len(self.feed_name_list)):
            this_string += " FEED %s/%s" % (self.feed_name_list[iggy], self.gui_pcc_averages_reactive_power_list[iggy])
        self.logger.info(this_string)
    #
    # -----------------------------------------------------------------------------------

    def log_tossed_raw_q_readings(self, **kwargs):

        db_q_list = kwargs['db_q_list']
        tossed_q_list = kwargs['tossed_q_list']
        pcc_or_solar_string = kwargs['pcc_or_solar_string']
        pos_or_neg_string = kwargs['pos_or_neg_string']
        feed_name = kwargs['feed_name']

        # This function was useful when analyzing and validating if any reactive power readings (PCC or SOLAR)
        # were tossed because they were "outliers" among a set of readings. It can make the log a bit messy.
        # It was quite useful during development, but once validated, no longer used.
        #
        # - db_q_list: list of raw reactive power readings from the database
        # - tossed_q_list: list of readings after removing "outliers" from the datasbse list
        #
        # If any readings from the database loist were tossed, then log it.
        # Include in the log: - feed name
        #                     - "PCC" or "SOLAR" indicator
        #                     - "+VE" or "-VE" reactive power indicator
        if len(tossed_q_list) == len(db_q_list):
            # No readings were tossed.
            no_toss_string = "FEED %s %s Q RAW READINGS (%s, NO TOSS): " % (feed_name, pcc_or_solar_string, pos_or_neg_string)
            for pop in range(0, len(tossed_q_list)):
                no_toss_string += " %s " % (tossed_q_list[pop])
            self.logger.info(no_toss_string)
        else:
            # 1 or more values tossed. Prepare and log the string that had all the raw database readings.
            toss_string = "FEED %s %s Q RAW READINGS (%s, B4 TOSS LEN %s): " % (feed_name,
                                                                                pcc_or_solar_string,
                                                                                pos_or_neg_string,
                                                                                len(db_q_list))
            for pop in range(0, len(db_q_list)):
                toss_string += " %s " % (db_q_list[pop])
            self.logger.info(toss_string)

            # Now prepare and log the string with outliers removed from the database readings list.
            toss_string = "FEED %s %s Q RAW READINGS (%s, AFTER TOSS LEN %s): " % (feed_name,
                                                                                   pcc_or_solar_string,
                                                                                   pos_or_neg_string,
                                                                                   len(tossed_q_list))
            for pop in range(0, len(tossed_q_list)):
                toss_string += " %s " % (tossed_q_list[pop])
            self.logger.info(toss_string)

        return

    # TODO: read_pcc_readings_1_and_calculate_meter_averages_one_feed and read_solar_readings_1_and_calculate_meter_averages_one_feed
    # TODO: functions are so similar, they should be combined into 1 function with a parameter to differentiate.

    def read_pcc_readings_1_and_calculate_meter_averages_one_feed(self, feed_name_rank):
        # For "this feed", get pcc_readings_1 meter values from the database, and calculate averages.
        #
        # Of special note for reactive power: as reactive power is brought down on the PCC by compensating on
        # the solar side, when getting close to the zero point, meter readings go wonky, and will often show
        # readings that instantaneously are above/below zero. This is accounted for.

        raw_pcc_q_readings_list = [0.0] * self.num_readings_for_pf_upd
        self.pcc_reactive_power_readings_is_valid_list[feed_name_rank] = True

        # -------------------------------------------------------------------------------------
        # Retrieve the last set of PCC Pf/real/reactive power readings from the pcc_readings_1 table,
        # and calculate the average for each. The table's entry_id and row_id are not extracted.
        select_string = "SELECT %s,%s,%s FROM %s WHERE %s='%s' AND %s=%s;" % (self.pcc_real_power_fp_name_list[feed_name_rank],
                                                                                self.pcc_amperage_fp_name_list[feed_name_rank],
                                                                                self.pcc_reactive_power_fp_name_list[feed_name_rank],
                                                                                DB_METER_READINGS_TABLE,
                                                                                METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                                                                                METER_ROLE_PCC1,
                                                                                METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                                                                                METER_DATA_TYPE_LAST_VALUE)

        pcc_readings_1_rows = prepared_act_on_database(FETCH_ALL, select_string, ())
        if pcc_readings_1_rows:
            pcc_real_power_sum = 0.0
            pcc_amperage_sum = 0.0

            q_readings_above_zero = 0
            q_readings_below_zero = 0

            pop = 0
            for pcc_readings_1 in pcc_readings_1_rows:
                # No issues found with real power and ameperage readings. Reactive power on the other hand,
                # needs massaging. The individual readings are stored in a separate list for further analysis.
                pcc_real_power_sum += pcc_readings_1[self.pcc_real_power_fp_name_list[feed_name_rank]]
                pcc_amperage_sum += pcc_readings_1[self.pcc_amperage_fp_name_list[feed_name_rank]]
                reactive_power_reading = pcc_readings_1[self.pcc_reactive_power_fp_name_list[feed_name_rank]]
                raw_pcc_q_readings_list[pop] = reactive_power_reading

                pop += 1
                if reactive_power_reading > 0.0:
                    q_readings_above_zero += 1
                else:
                    q_readings_below_zero += 1

            # No issue observed with real power and amperage, calculate average for this feed directly.
            self.pcc_real_power_average_list[feed_name_rank] = round((pcc_real_power_sum / self.num_readings_for_pf_upd), 3)
            self.pcc_amperage_average_list[feed_name_rank] = round((pcc_amperage_sum / self.num_readings_for_pf_upd), 2)

            # Average for reactive power however, needs to account for bouncing around zero when really low.
            if (q_readings_above_zero == self.num_readings_for_pf_upd) and (q_readings_below_zero == 0):
                # All reactive power readings were above zero, consistent. Simple case # 1.
                # Toss out "outliers" that deviate significantly from the mean. The average
                # is then calculated on the basis of the remaining raw readings.
                above_zero_q_readings_list = gu.hgn_toss_outliers(raw_pcc_q_readings_list)

                # This observe during development:
                if len(above_zero_q_readings_list) == 0:
                    if self.power_factor_control_command_logging_list[feed_name_rank]:
                        self.log_tossed_raw_q_readings(db_q_list = raw_pcc_q_readings_list,
                                                       tossed_q_list = above_zero_q_readings_list,
                                                       pcc_or_solar_string = "PCC",
                                                       pos_or_neg_string = "+VE",
                                                       feed_name = self.feed_name_list[feed_name_rank])
                else:
                    self.pcc_reactive_power_average_list[feed_name_rank] = round((sum(above_zero_q_readings_list) / len(above_zero_q_readings_list)), 3)

            elif (q_readings_below_zero == self.num_readings_for_pf_upd) and (q_readings_above_zero == 0):
                # All reactive power readings were below zero, consistent. Simple case # 2.
                # Toss out "outliers" that deviate significantly from the mean. The average
                # is then calculated on the basis of the remaining raw readings.
                below_zero_q_readings_list = gu.hgn_toss_outliers(raw_pcc_q_readings_list)

                # Did not see this one during development, but added just in case.
                if len(below_zero_q_readings_list) == 0:
                    if self.power_factor_control_command_logging_list[feed_name_rank]:
                        self.log_tossed_raw_q_readings(db_q_list = raw_pcc_q_readings_list,
                                                       tossed_q_list = below_zero_q_readings_list,
                                                       pcc_or_solar_string = "PCC",
                                                       pos_or_neg_string = "-VE",
                                                       feed_name = self.feed_name_list[feed_name_rank])
                else:
                    self.pcc_reactive_power_average_list[feed_name_rank] = round((sum(below_zero_q_readings_list) / len(below_zero_q_readings_list)), 3)

            else:
                # Reactive power sometimes above zero ... sometimes below. First perform a basic sanity test.
                if (q_readings_above_zero + q_readings_below_zero) == self.num_readings_for_pf_upd:
                    # For readings that were both above/below zero, apply the 1st level criteria:
                    # - if 75% or more of the readings were ABOVE zero, accept only those readings,
                    # - else if 75% or more of the readings were BELOW zero, accept only those readings
                    #
                    # Of the remaining readings, toss outliers, and get the average from that.
                    minimum_acceptable_count = int(self.num_readings_for_pf_upd * self.minimum_acceptable_meter_readings_percent)

                    acceptable_q_readings_list = []
                    if q_readings_above_zero >= minimum_acceptable_count:
                        # Use only those readings ABOVE zero.
                        for iggy in range (0, self.num_readings_for_pf_upd):
                            if raw_pcc_q_readings_list[iggy] >= 0.0:
                                acceptable_q_readings_list.append(raw_pcc_q_readings_list[iggy])

                    elif q_readings_below_zero >= minimum_acceptable_count:
                        # Use only those readings BELOW zero.
                        for iggy in range (0, self.num_readings_for_pf_upd):
                            if raw_pcc_q_readings_list[iggy] < 0.0:
                                acceptable_q_readings_list.append(raw_pcc_q_readings_list[iggy])

                    else:
                        # A complete mish-mash of reactive power readings. No averaging can be performed
                        # for this cycle. Log this if bounce is enabled.
                        self.pcc_reactive_power_readings_is_valid_list[feed_name_rank] = False
                        # ----------------------------------------------------------------------------------------------
                        # Conditional logging:
                        if self.power_factor_bounce_data_logging_list[feed_name_rank]:
                            # Display the raw readings.
                            this_string = "FEED %s PCC Q MISH MASH READINGS REJECTED (ABOVE/BELOW %s/%s count), RAW READINGS: " \
                                                                                 % (self.feed_name_list[feed_name_rank],
                                                                                    q_readings_above_zero,
                                                                                    q_readings_below_zero)
                            for iggy in range(0, len(raw_pcc_q_readings_list)):
                                this_string += " %s " % (raw_pcc_q_readings_list[iggy])
                            self.logger.info(this_string)
                        # ----------------------------------------------------------------------------------------------

                    acceptable_q_readings_after_toss_list = []
                    if len(acceptable_q_readings_list) != 0:
                        # Perform reactive power averaging based on this list once outliers have been rejected.
                        acceptable_q_readings_after_toss_list = gu.hgn_toss_outliers(acceptable_q_readings_list)

                        # Did not see this one during development, but added just in case.
                        if len(acceptable_q_readings_after_toss_list) == 0:
                            if self.power_factor_control_command_logging_list[feed_name_rank]:
                                self.log_tossed_raw_q_readings(db_q_list = acceptable_q_readings_list,
                                                               tossed_q_list = acceptable_q_readings_after_toss_list,
                                                               pcc_or_solar_string = "PCC",
                                                               pos_or_neg_string = "-VE",
                                                               feed_name = self.feed_name_list[feed_name_rank])
                        else:
                            self.pcc_reactive_power_average_list[feed_name_rank] = round((sum(acceptable_q_readings_after_toss_list) \
                                                                                   / len(acceptable_q_readings_after_toss_list)), 3)

                        # ----------------------------------------------------------------------------------------------
                        # Conditional logging:
                        if self.power_factor_bounce_data_logging_list[feed_name_rank]:
                            # Display the raw readings.
                            this_string = "FEED %s PCC Q BOUNCE (ABOVE/BELOW %s/%s count) Q_AVG=%s (%s/%s) RAW READINGS: " \
                                             % (self.feed_name_list[feed_name_rank],
                                                q_readings_above_zero,
                                                q_readings_below_zero,
                                                '{0:+.3f}'.format(self.pcc_reactive_power_average_list[feed_name_rank]),
                                                len(acceptable_q_readings_list),
                                                len(acceptable_q_readings_after_toss_list))
                            for iggy in range(0, len(raw_pcc_q_readings_list)):
                                this_string += " %s " % (raw_pcc_q_readings_list[iggy])
                            self.logger.info(this_string)
                        # ----------------------------------------------------------------------------------------------

                else:
                    # Someone has really lost their sanity. Or mind. This was encountered during development
                    # due to coding error, but not seen since appropriate fixes were made.
                    self.logger.info("PCC SANITY COUNT: %s %s %s" % (q_readings_above_zero,
                                                                     q_readings_below_zero,
                                                                     self.num_readings_for_pf_upd))
                    self.pcc_reactive_power_average_list[feed_name_rank] = 0.0
                    self.raise_and_clear_alarm(ALARM_ID_POWER_FACTOR_SANITY)

        else:
            # Read out nothing from the database.
            self.logger.info("NOTHING READ OUT FROM THE PCC READINGS1 TABLE")

    def read_solar_readings_1_and_calculate_meter_averages_one_feed(self, feed_name_rank):
        # For "this feed", get solar_readings_1 meter values from the database, and calculate averages.
        #
        # Of special note for reactive power: as reactive power is brought down on the PCC by compensating on
        # the solar side, when getting close to the reactive power target, meter readings go wonky, and will
        # often show readings that flip instantaneously above and below zero. This is accounted for.

        raw_solar_q_readings_list = [0.0] * self.num_readings_for_pf_upd
        self.solar_reactive_power_readings_is_valid_list[feed_name_rank] = True

        # -------------------------------------------------------------------------------------
        # Retrieve the last set of SOLAR Pf/real/reactive power readings from the solar_readings_1 table,
        # and calculate the average for each. The table's entry_id and row_id are not extracted.
        select_string = "SELECT %s,%s,%s FROM %s WHERE %s='%s' AND %s=%s;" % (self.solar_real_power_fp_name_list[feed_name_rank],
                                                                                self.solar_amperage_fp_name_list[feed_name_rank],
                                                                                self.solar_reactive_power_fp_name_list[feed_name_rank],
                                                                                DB_METER_READINGS_TABLE,
                                                                                METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                                                                                METER_ROLE_SOLAR1,
                                                                                METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                                                                                METER_DATA_TYPE_LAST_VALUE)

        solar_readings_1_rows = prepared_act_on_database(FETCH_ALL, select_string, ())
        if solar_readings_1_rows:
            solar_real_power_sum = 0.0
            solar_amperage_sum = 0.0

            q_readings_above_zero = 0
            q_readings_below_zero = 0

            pop = 0
            for solar_readings_1 in solar_readings_1_rows:
                # No issues found with real power and ameperage readings. Reactive power on the other hand,
                # needs massaging. The individual readings are stored in a separate list for further analysis.
                solar_real_power_sum += round((solar_readings_1[self.solar_real_power_fp_name_list[feed_name_rank]]), 3)
                solar_amperage_sum += round((solar_readings_1[self.solar_amperage_fp_name_list[feed_name_rank]]), 2)
                reactive_power_reading = solar_readings_1[self.solar_reactive_power_fp_name_list[feed_name_rank]]
                raw_solar_q_readings_list[pop] = reactive_power_reading
                pop += 1

                if reactive_power_reading > 0.0:
                    q_readings_above_zero += 1
                else:
                    q_readings_below_zero += 1

            # No issue observed with real power and amperage, calculate average for this feed directly.
            self.solar_real_power_average_list[feed_name_rank] = solar_real_power_sum / self.num_readings_for_pf_upd
            self.solar_amperage_average_list[feed_name_rank] = solar_amperage_sum / self.num_readings_for_pf_upd

            # Average for reactive power however, needs to account for bouncing around zero when really low.
            if (q_readings_above_zero == self.num_readings_for_pf_upd) and (q_readings_below_zero == 0):
                # All reactive power readings were above zero, consistent. Simple case # 1.
                # Toss out "outliers" that deviate significantly from the mean. The average
                # is then calculated on the basis of the remaining raw readings.
                above_zero_q_readings_list = gu.hgn_toss_outliers(raw_solar_q_readings_list)

                # This observe during development:
                if len(above_zero_q_readings_list) == 0:
                    if self.power_factor_control_command_logging_list[feed_name_rank]:
                        self.log_tossed_raw_q_readings(db_q_list = raw_solar_q_readings_list,
                                                       tossed_q_list = above_zero_q_readings_list,
                                                       pcc_or_solar_string = "SOLAR",
                                                       pos_or_neg_string = "+VE",
                                                       feed_name = self.feed_name_list[feed_name_rank])
                else:
                    self.solar_reactive_power_average_list[feed_name_rank] = round((sum(above_zero_q_readings_list) / len(above_zero_q_readings_list)), 3)

            elif (q_readings_below_zero == self.num_readings_for_pf_upd) and (q_readings_above_zero == 0):
                # All reactive power readings were below zero, consistent. Simple case # 2.
                # Toss out "outliers" that deviate significantly from the mean. The average
                # is then calculated on the basis of the remaining raw readings.
                below_zero_q_readings_list = gu.hgn_toss_outliers(raw_solar_q_readings_list)

                # Did not see this one during development, but added just in case.
                if len(below_zero_q_readings_list) == 0:
                    if self.power_factor_control_command_logging_list[feed_name_rank]:
                        self.log_tossed_raw_q_readings(db_q_list = raw_solar_q_readings_list,
                                                       tossed_q_list = below_zero_q_readings_list,
                                                       pcc_or_solar_string = "SOLAR",
                                                       pos_or_neg_string = "-VE",
                                                       feed_name =self.feed_name_list[feed_name_rank])
                else:
                    self.solar_reactive_power_average_list[feed_name_rank] = round((sum(below_zero_q_readings_list) / len(below_zero_q_readings_list)), 3)

            else:
                # Reactive power readings sometimes above zero ... sometimes below. First perform a basic sanity test.
                if (q_readings_above_zero + q_readings_below_zero) == self.num_readings_for_pf_upd:
                    # For readings that were both above/below zero, apply the 1st level criteria:
                    # - if 75% or more of the readings were ABOVE zero, accept only those readings,
                    # - else if 75% or more of the readings were BELOW zero, accept only those readings
                    #
                    # Of the remaining readings, toss outliers, and get the average from that.
                    minimum_acceptable_count = int(self.num_readings_for_pf_upd * self.minimum_acceptable_meter_readings_percent)

                    acceptable_q_readings_list = []
                    if q_readings_above_zero >= minimum_acceptable_count:
                        # Use only those readings ABOVE zero.
                        for iggy in range (0, self.num_readings_for_pf_upd):
                            if raw_solar_q_readings_list[iggy] >= 0.0:
                                acceptable_q_readings_list.append(raw_solar_q_readings_list[iggy])

                    elif q_readings_below_zero >= minimum_acceptable_count:
                        # Use only those readings BELOW zero.
                        for iggy in range (0, self.num_readings_for_pf_upd):
                            if raw_solar_q_readings_list[iggy] < 0.0:
                                acceptable_q_readings_list.append(raw_solar_q_readings_list[iggy])

                    else:
                        # A complete mish-mash of reactive power readings. No averaging can be performed
                        # for this cycle. Not observed during testing.
                        self.solar_reactive_power_readings_is_valid_list[feed_name_rank] = False
                        # ----------------------------------------------------------------------------------------------
                        # Conditional logging:
                        if self.power_factor_bounce_data_logging_list[feed_name_rank]:
                            # Display the raw readings.
                            this_string = "FEED %s SOLAR Q MISH MASH READINGS REJECTED (ABOVE/BELOW %s/%s count), RAW READINGS: " \
                                          % (self.feed_name_list[feed_name_rank],
                                             q_readings_above_zero,
                                             q_readings_below_zero)
                            for iggy in range(0, len(raw_solar_q_readings_list)):
                                this_string += " %s " % (raw_solar_q_readings_list[iggy])
                            self.logger.info(this_string)
                        # ----------------------------------------------------------------------------------------------

                    acceptable_q_readings_after_toss_list = []
                    if len(acceptable_q_readings_list) != 0:
                        # Perform reactive power averaging based on this list once outliers have been rejected.
                        acceptable_q_readings_after_toss_list = gu.hgn_toss_outliers(acceptable_q_readings_list)

                        # Did not see this one during development, but added just in case.
                        if len(acceptable_q_readings_after_toss_list) == 0:
                            if self.power_factor_control_command_logging_list[feed_name_rank]:
                                self.log_tossed_raw_q_readings(db_q_list = acceptable_q_readings_list,
                                                               tossed_q_list = acceptable_q_readings_after_toss_list,
                                                               pcc_or_solar_string = "SOLAR",
                                                               pos_or_neg_string = "-VE",
                                                               feed_name = self.feed_name_list[feed_name_rank])
                        else:
                            self.solar_reactive_power_average_list[feed_name_rank] = round((sum(acceptable_q_readings_after_toss_list) \
                                                                                   / len(acceptable_q_readings_after_toss_list)), 3)

                        # ----------------------------------------------------------------------------------------------
                        # Conditional logging:
                        if self.power_factor_bounce_data_logging_list[feed_name_rank]:
                            # Display the raw readings.
                            this_string = "FEED %s SOLAR Q BOUNCE (ABOVE/BELOW %s/%s count) Q_AVG=%s (%s/%s) RAW READINGS: " \
                                             % (self.feed_name_list[feed_name_rank],
                                                q_readings_above_zero,
                                                q_readings_below_zero,
                                                '{0:+.3f}'.format(self.solar_reactive_power_average_list[feed_name_rank]),
                                                len(acceptable_q_readings_list),
                                                len(acceptable_q_readings_after_toss_list))
                            for iggy in range(0, len(raw_solar_q_readings_list)):
                                this_string += " %s " % (raw_solar_q_readings_list[iggy])
                            self.logger.info(this_string)
                        # ----------------------------------------------------------------------------------------------

                else:
                    # Someone has really lost their sanity. Or mind. This was encountered during development
                    # due to coding error, but not seen since appropriate fixes were made.
                    self.logger.info("SOLAR SANITY COUNT: %s %s %s" % (q_readings_above_zero,
                                                                    q_readings_below_zero,
                                                                    self.num_readings_for_pf_upd))
                    self.solar_reactive_power_average_list[feed_name_rank] = 0.0
                    self.raise_and_clear_alarm(ALARM_ID_POWER_FACTOR_SANITY)

        else:
            # Read out nothing from the database.
            self.logger.info("NOTHING READ OUT FROM THE SOLAR READINGS1 TABLE")

    def super_duper_meter_logging(self, reason_for_logging, feed_index, min_requirements_met_flag):

        # -----------------------------------------------------------------------------------------
        #
        # Logging: all floats are rounded to 2 or 3 decimal places for logging/display purpose.
        #          Using "round(some_float,3)" to log/display to 3 decimal places causes the
        #          trailing zero to be dropped, so instead, use "format(some_float, '.+3f')"
        #          (omit + sign for no signage).
        #
        # Prepare the string for logging for debug/analysis/study. It will look something like:
        #                                                                    SOLAR(kW/A/Qs/Qh/Qt
        # EXCEL,XXX,YYY,A,PCC(kW/A/Qp/quad),-0.079,+2.02,+0.224,Q1,SOLAR(kW/A/Qs/Qh/Qt),+0.746,+6.98,-0.404,+0.097,-0.501,ZZZ,WWW,0.830,0
        #                 |                                       |                                                              |commanded
        #                 |<------------- PCC info -------------->|<------------------------ SOLAR info ------------------------>|  value
        #
        # where: XXX = LOG if this is simple data logging (when logging is enabled), or
        #              CMD is this is the commanded dynamic power factor for the feed in question.
        #        YYY = command power factor number. If data logging or there is no command, xx is displayed
        #        Qp = reactive power reading average at the PCC
        #        Qs = reactive power reading average at the SOLAR
        #        Qh = reactive power to harvest (kVAr) by the inverters on the feed in question
        #        ZZZ = "CMD" if there was a commanded power factor else "NO_CMD"
        #        WWW = power factor condition indicator string
        #
        # Values are averages of a set of readings from the pcc_readings_1 and solar_readings_1 tables, and the last
        # 2 values are the commanded Pf/current_leading for the feed in question.
        # -------------------------------------------------------------------------------

        # Is logging enabled on this feed?
        if (self.power_factor_control_command_logging_list[feed_index] == LOG_PF_CONTROL_DATA_ENABLED)\
                or (self.power_factor_control_data_logging_list[feed_index] == LOG_PF_STUDY_DATA_ENABLED):
            # Logging might be for data-only or commanded power factor, but is enabled.

            # PCC and SOLAR meter data has aleady been extracted, averages already calculated. On a per-feed basis,
            # formulate the string for logging with indication if the var capability on the solar side is sufficient
            # to compensate for the reactive power on the PCC side.

            # --------------------------------------------
            # TODO: PUT THIS SECTION IN A FUNCTION/METHOD.
            solar_capability = "Ed_lost_his_mind"
            if min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_FULL:
                solar_capability = "CAPABLE_FULL"
            elif min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_REDUCED:
                solar_capability = "CAPABLE_REDUCED"
            elif min_requirements_met_flag == MIN_REQ_PCC_LOW:
                solar_capability = "PCC_LOW" # PCC reactive power below threshold of meter sensitivity.
            elif min_requirements_met_flag == MIN_REQ_SOLAR_LOW:
                solar_capability = "SOLAR_LOW"  # SOLAR real power below threshold of meter sensitivity.
            elif min_requirements_met_flag == MIN_REQ_PCC_AND_SOLAR_LOW:
                solar_capability = "PCC_SOLAR_LOW"  # Both PCC and SOLAR real power below threshold of meter sensitivity.
            elif min_requirements_met_flag == MIN_REQ_SOLAR_MAXED_OUT:
                solar_capability = "SOLAR_MAXED_OUT" # SOLAR real power cannot meet reactive power demand, so MAX'd out.
            elif min_requirements_met_flag == MIN_REQ_PF_SAME_NO_CHANGE:
                solar_capability = "PF_SAME" # Previous->next Pf would not make any change in reactive power.
            elif min_requirements_met_flag == MIN_REQ_PF_FLAT_SPOT_NO_CHANGE:
                solar_capability = "PF_FLAT_SPOT" # Previous->next Pf in inverter's flat spot, so no Q change.
            elif min_requirements_met_flag == MIN_REQ_Q_SHEDDING:
                solar_capability = "Q_SHEDDING" # Reactive power shedding event.
            elif min_requirements_met_flag == MIN_REQ_PREF_PF_RE_APPLIED:
                solar_capability = "PREF_PF_RE_APPLIED" # The (previous) "preferred" Pf was re-applied.
            elif min_requirements_met_flag == MIN_REQ_PREF_PF_QUAD_SAME:
                solar_capability = "PREF_PF_QUAD_SAME" # No Pf applied, system in steady state, PCC still in same quadrant.
            elif min_requirements_met_flag == MIN_REQ_PREF_PF_QUAD_CHANGE:
                solar_capability = "PREF_PF_QUAD_CHANGE" # No Pf applied, system in steady state, PCC in other quadrant.
            elif min_requirements_met_flag == MIN_REQ_PCC_READINGS_REJECT:
                solar_capability = "PCC_READ_REJECT" # No Pf applied, PCC meter readings unacceptable.
            elif min_requirements_met_flag == MIN_REQ_SOLAR_READINGS_REJECT:
                solar_capability = "SOLAR_READ_REJECT" # No Pf applied, SOLAR meter readings unacceptable.
            elif min_requirements_met_flag == MIN_REQ_PCC_SOLAR_READINGS_REJECT:
                solar_capability = "PCC_SOLAR_READS_REJECT" # No Pf applied, PCC and SOLAR meter readings unacceptable.
            else:
                if min_requirements_met_flag != MIN_REQ_DATA_LOGGING_ONLY:
                    # TODO: CHANGE DONUT_CARE -> DATA_LOGGING_ONLY
                    # Plain ol' data logging.
                    pass
                else:
                    # Maybe Ed did lose his mind afterall ...
                    pass
            # --------------------------------------------

            # Now compose the string for logging. Include the feed name in the log data. The log data
            # contains 'EXCEL', to facilitate culling and entering data into an excel spreadsheet.
            # Log all floats to 3 decimal places including its sign.
            if reason_for_logging == LOGGING_COMMANDED_PF:
                # Logging an actual commanded power factor. The last commanded power factor is not included, available in
                # the self.previous_commanded_pf_list[xxx] and self.previous_commanded_pf_c_lead_list[xxx]
                # lists variables.
                if (min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_FULL) \
                  or (min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_REDUCED) \
                  or (min_requirements_met_flag == MIN_REQ_Q_SHEDDING) \
                  or (min_requirements_met_flag == MIN_REQ_SOLAR_MAXED_OUT) \
                  or (min_requirements_met_flag == MIN_REQ_PREF_PF_RE_APPLIED):
                    super_duper_string = "EXCEL,CMD,%s,%s" % (self.num_dynamic_pf_commands, self.feed_name_list[feed_index])
                else:
                    # Commanded Pf will not be made - don't display command number.
                    super_duper_string = "EXCEL,CMD,xx,%s" % (self.feed_name_list[feed_index])

                # PCC(Pf/kW/A/Q/Qtgt/quad) ...
                super_duper_string += ",PCC(kW/A/Qp/quad),%s,%s,%s,Q%s" \
                                      % ('{0:+.3f}'.format(self.pcc_real_power_average_list[feed_index]),
                                         '{0:+.2f}'.format(self.pcc_amperage_average_list[feed_index]),
                                         '{0:+.3f}'.format(self.pcc_reactive_power_average_list[feed_index]),
                                         self.get_power_quadrant(self.pcc_real_power_average_list[feed_index],
                                                                 self.pcc_reactive_power_average_list[feed_index]))
                if (min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_FULL) \
                  or (min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_REDUCED) \
                  or (min_requirements_met_flag == MIN_REQ_Q_SHEDDING) \
                  or (min_requirements_met_flag == MIN_REQ_SOLAR_MAXED_OUT)\
                  or (min_requirements_met_flag == MIN_REQ_PREF_PF_RE_APPLIED) \
                  or (min_requirements_met_flag == MIN_REQ_PF_SAME_NO_CHANGE) \
                  or (min_requirements_met_flag == MIN_REQ_PF_FLAT_SPOT_NO_CHANGE):
                    # Normally, this section for actual commanded power factor, but includes the 2 "no change"
                    # (since the last commanded Pf is to be displayed).
                    # SOLAR(kW/A/Qs/Qh/Qt/"CMD or NO_CMD"/solar capability string/commanded Pf+current_leading bool)
                    command_or_no_command = "CMD"
                    if (min_requirements_met_flag == MIN_REQ_PF_SAME_NO_CHANGE) \
                      or (min_requirements_met_flag == MIN_REQ_PF_FLAT_SPOT_NO_CHANGE):
                        command_or_no_command = "NO_CMD"
                    super_duper_string += ",SOLAR(kW/A/Qs/Qh/Qt),%s,%s,%s,%s,%s,%s,%s,LATEST,%s,%s" \
                                    % ('{0:+.3f}'.format(self.solar_real_power_average_list[feed_index]),
                                       '{0:+.2f}'.format(self.solar_amperage_average_list[feed_index]),
                                       '{0:+.3f}'.format(self.solar_reactive_power_average_list[feed_index]),
                                       '{0:+.3f}'.format(self.solar_q_to_harvest_list[feed_index]),
                                       '{0:+.3f}'.format(self.solar_q_target_list[feed_index]),
                                       command_or_no_command,
                                       solar_capability,
                                       '{0:.3f}'.format(self.latest_commanded_pf_list[feed_index]),
                                       self.latest_commanded_pf_c_lead_list[feed_index])
                else:
                    # Min requirements not met, or bad meter readings. So no power factor command.
                    # SOLAR(kW/A/Qs/"NO_CMD",PREV",power factor)
                    super_duper_string += ",SOLAR(kW/A/Qs),%s,%s,%s,,,NO_CMD,%s,PREV,%s,%s" \
                                    % ('{0:+.3f}'.format(self.solar_real_power_average_list[feed_index]),
                                       '{0:+.2f}'.format(self.solar_amperage_average_list[feed_index]),
                                       '{0:+.3f}'.format(self.solar_reactive_power_average_list[feed_index]),
                                       solar_capability,
                                       '{0:.3f}'.format(self.previous_commanded_pf_list[feed_index]),
                                       self.previous_commanded_pf_c_lead_list[feed_index])
            else:
                # DATA LOGGING ONLY.
                # PCC(Pf/kW/A/Q/Qtgt/quad)
                super_duper_string = "EXCEL,LOG,%s,%s" % (self.logging_number, self.feed_name_list[feed_index])
                self.logging_number += 1
                super_duper_string += ",PCC(kW/A/Qp/quad),%s,%s,%s,Q%s" \
                                    % ('{0:+.3f}'.format(self.pcc_real_power_average_list[feed_index]),
                                       '{0:+.2f}'.format(self.pcc_amperage_average_list[feed_index]),
                                       '{0:+.3f}'.format(self.pcc_reactive_power_average_list[feed_index]),
                                       self.get_power_quadrant(self.pcc_real_power_average_list[feed_index],
                                                               self.pcc_reactive_power_average_list[feed_index]))
                # SOLAR(Pf/kW/A/Q)
                super_duper_string += ",SOLAR(kW/A/Qs/),%s,%s,%s,,,LOG,,PREV,%s,%s" \
                                    % ('{0:+.3f}'.format(self.solar_real_power_average_list[feed_index]),
                                       '{0:+.2f}'.format(self.solar_amperage_average_list[feed_index]),
                                       '{0:+.3f}'.format(self.solar_reactive_power_average_list[feed_index]),
                                       '{0:.3f}'.format(self.latest_commanded_pf_list[feed_index]),
                                       self.latest_commanded_pf_c_lead_list[feed_index])

            self.logger.info(super_duper_string)

    def is_minimum_threshold_requirements_met(self, pcc_reactive_power, solar_real_power):

        minimum_power_requirements_met = MIN_REQ_NOT_DETERMINED
        pcc_low = False
        solar_low = False

        if abs(pcc_reactive_power) < self.minimum_reactive_power_threshold:
            pcc_low = True
        if abs(solar_real_power) <= self.minimum_real_power_threshold:
            solar_low = True

        if (not pcc_low) and (not solar_low):
            minimum_power_requirements_met = MIN_REQ_ALL_HAVE_BEEN_MET_FULL
        else:
            # One or the other or both are low.
            if pcc_low and solar_low:
                minimum_power_requirements_met = MIN_REQ_PCC_AND_SOLAR_LOW
            elif pcc_low:
                minimum_power_requirements_met = MIN_REQ_PCC_LOW
            else:
                minimum_power_requirements_met = MIN_REQ_SOLAR_LOW

        return minimum_power_requirements_met

    def log_this_hex_data(self, some_hex_data):
        # Will log the raw hex data in packed data:
        this_string = "HEX DATA: "
        if some_hex_data:
            for char_char in some_hex_data:
                this_string += hex(ord(char_char))[2:].zfill(2) + " "
        self.logger.info(this_string)

    def calc_solar_target_from_harvest(self, q_pcc_target, q_solar, q_to_harvest):
        q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_TRUE
        if q_pcc_target > 0.0 and q_solar > 0.0:
            # Both are positive values, both are in the same quadrant (1 or 2). To move q_pcc "downwards"
            # towards the target (Pf 1.0), push the q_solar positive value towards PF of 1.0 (i.e., make it
            # less positive) by subtracting the amount to harvest (a positive value).
            # NOTE: with PCC and SOLAR in the same quadrant, this may be a reactive power shedding condition,
            #       which is accounted for separately.
            q_solar_target = q_solar - q_to_harvest
            # Did the solar target end up in the other quadrant? It was positive, now it is ...
            if q_solar_target <= 0.0:
                # Yes, moved from quadrant 2/1 into 3/4.
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_TRUE
            else:
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_FALSE

        elif q_pcc_target <= 0.0 and q_solar <= 0.0:
            # Both are negative values, both are in the same quadrant (3 or 4). To move q_pcc "upwards"
            # towards the target (Pf 1.0), push q_solar negative value towards Pf of 1 (i.e., make it
            # less negative) by adding the amount to harvest (a positive value).
            # NOTE: with PCC and SOLAR in the same quadrant, this may be a reactive power shedding condition,
            #       which is accounted for separately.
            q_solar_target = q_solar + q_to_harvest
            # Did the solar target end up in the other quadrant? It was negative, now it is ...
            if q_solar_target > 0.0:
                # Yes, moved from quadrant 3/4 into 2/1.
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_FALSE
            else:
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_TRUE

        else:
            # The PCC and SOLAR measurements are in opposite quadrants.
            if q_solar > 0.0:
                # q_solar is positive, q_pcc is negative: to move q_pcc "upwards" towards the
                # target (Pf 1.0), increase the q_solar positive value by the harvest amount.
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_FALSE
                q_solar_target = q_solar + q_to_harvest

            else:
                # q_solar is negative, q_pcc is positive: to move q_pcc "downwards" towards the
                # target (Pf 1.0), increase the q_solar negative value by subtracting the harvest
                # amount (a positive value).
                q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_TRUE
                q_solar_target = q_solar - q_to_harvest

        return (q_solar_target, q_solar_current_leading_target)

    def calculate_solar_reactive_power_target(self, q_pcc, q_solar):
        # Calculate the reactive power required on the solar side to bring
        # the PCC reactive power down to a desired level.
        #
        # - q_pcc   -> current average reactive power on the PCC side
        # - q_solar -> current average reactive power on the solar side
        #
        # To pull the PCC reactive power down towards its target (typically "zero VAr" or Pf of 1.0),
        # the PCC reactive power target is based on a percentage of its current value, so:
        full_vars_reduction = True
        q_pcc_target = q_pcc - (q_pcc * self.percent_reactive_power_to_harvest)
        q_to_harvest = abs(q_pcc) - abs(q_pcc_target)
        if abs(q_pcc_target) <= self.minimum_reactive_power_threshold:
            # The target is below the "meter sensitivity threshold" range, so somewhere in the zone where the meter
            # cannot provide reliable values. Set the target to a value that is just slightly below the threshold.
            # If set to the actual value that is below, reactive power on the PCC will swing and oscillate between
            # quadrants (although the swings are close to the threshold, but will oscillate).
            full_vars_reduction = False
            q_to_harvest = (abs(q_pcc) - abs(self.minimum_reactive_power_threshold)) \
                             + (self.minimum_reactive_power_threshold * self.reduced_harvest_percent)
            if q_pcc > 0.0:
                # To pull the PCC target "dowwnard" from a positive value: subtract the amount to harvest.
                q_pcc_target = q_pcc - q_to_harvest
            else:
                # To pull the PCC target "upward" from a negative value: add the amount to harvest.
                q_pcc_target = q_pcc + q_to_harvest

        # Calculate the reactive power target on the solar side. The reactive power TARGET on the solar side is a
        # function of its present reading and the amount to harvest, accounting for which quadrant reactive power
        # on both the PCC and solar are in.
        q_solar_target = q_solar
        q_solar_current_leading_target = POWER_FACTOR_CURRENT_LEADING_TRUE
        (q_solar_target, q_solar_current_leading_target) = self.calc_solar_target_from_harvest(q_pcc_target, q_solar, q_to_harvest)

        return (q_to_harvest, q_solar_target, q_solar_current_leading_target, full_vars_reduction)

    def is_both_q_readings_in_same_quadrant(self, this_q_reading, that_q_reading):
        same_quadrant = False
        if (this_q_reading > 0.0 and that_q_reading > 0.0) or (this_q_reading <= 0.0 and that_q_reading <= 0.0):
            same_quadrant = True
        return same_quadrant

    # Unused function, but kept around, just in case.
    def is_both_pf_the_same(self, this_pf_as_float, that_pf_as_float, rounding_value):
        # Compare 2 floats. Not a good idea to compare floats directly, due to the long
        # train of decimals (especially when calculated from real and reactive power).
        # So compare them when rounded.
        both_pf_are_the_same = False
        this_pf_as_float = round(this_pf_as_float, rounding_value)
        that_pf_as_float = round(that_pf_as_float, rounding_value)
        if this_pf_as_float == that_pf_as_float:
            both_pf_are_the_same = True
        return both_pf_are_the_same

    def new_pf_effectiveness(self, this_pf_as_float, this_pf_current_leading, that_pf_as_float, that_pf_current_leading):
        # Given the last Pf with which inverters were commanded with, and a "next" potential Pf, will the
        # next commanded power factor result in an effective change in reactive power? This requires that previous
        # and next Pf be compared.
        #
        # ASSUMPTION: if 2 calculated floats are compared, then a match is highly unlikely due to trailing
        #             decimal digits being all over the map. The assumption is that this function is called
        #             when Pf values have been pre-rounded to 2 decimal places.
        #
        # No effective change in reactive power by an SG424 inverter will occur if previous/next Pf is:
        #
        # - both are the same
        # - 0.95 .. 0.96       <- inverter's "flat spot"
        # - 0.86 .. 0.87       <- inverter's "flat spot"
        # - 0.77 .. 0.78       <- inverter's "flat spot"
        #

        reactive_power_change = NEW_PF_CHANGE_IN_Q_WILL_OCCUR

        if this_pf_current_leading == that_pf_current_leading:
            # Next power factor update will keep reactive power in the same quadrant.
            if this_pf_as_float == that_pf_as_float:
                # Both are the same.
                reactive_power_change = NEW_PF_NO_Q_CHANGE_FOR_SAME_PF
            elif (this_pf_as_float == 0.95 or this_pf_as_float == 0.96) \
                and (that_pf_as_float == 0.95 or that_pf_as_float == 0.96):
                reactive_power_change = NEW_PF_NO_Q_CHANGE_INVERTER_FLAT_SPOT
            elif (this_pf_as_float == 0.86 or this_pf_as_float == 0.87) \
                and (that_pf_as_float == 0.86 or that_pf_as_float == 0.87):
                reactive_power_change = NEW_PF_NO_Q_CHANGE_INVERTER_FLAT_SPOT
            elif (this_pf_as_float == 0.77 or this_pf_as_float == 0.78) \
                and (that_pf_as_float == 0.77 or that_pf_as_float == 0.78):
                reactive_power_change = NEW_PF_NO_Q_CHANGE_INVERTER_FLAT_SPOT
            else:
                pass
        return reactive_power_change

    def command_inverters_with_dynamic_pf(self):

        # The number of TLVs depends on the number of feeds configured, and if power factor control is enabled
        # on each feed.
        tlv_packed_data = ""
        number_of_user_tlv = 0
        self.last_inverter_update_utc = self.current_utc

        # First read the latest PCC and SOLAR meter readings and calculate averages to guide power factor control.

        # On a per-feed basis, check that:
        #
        # - power factor enabled on the feed
        # - power is imported on the PCC side
        # - there is sufficient reactive power at the PCC for meter accuracy
        # - calculate the PCC reactive power target
        # - calculate the reactive power to be harvested on the solar side
        # - ensure there is sufficient solar energy to produce and reach the reactive power to harvest
        # - evaluate the power factor that the inverters will be commanded on that feed
        #
        for iggy in range(0, len(self.feed_name_list)):

            # Is power factor control enabled for this feed?
            if self.power_factor_control_enable_list[iggy] == POWER_FACTOR_CONTROL_ENABLE_ENABLED:
                # Power control is enabled on this feed.

                # Read the PCC and SOLAR meter readings for this feed and calculate averages for power factor control.
                # Retain the previous reactive power at both the PCC and SOLAR (required to support "preferred PF if
                # reactive power flipped").
                retained_last_pcc_q = self.pcc_reactive_power_average_list[iggy]
                retained_last_solar_q = self.solar_reactive_power_average_list[iggy]

                self.read_pcc_readings_1_and_calculate_meter_averages_one_feed(iggy)
                self.read_solar_readings_1_and_calculate_meter_averages_one_feed(iggy)

                # TODO: INCLUDE/INCORPORATE THIS SECTION WITH THE SAME CHECK BELOW, I.E., DO THIS CHECK ONCE ONLY.
                if self.pcc_reactive_power_readings_is_valid_list[iggy] == True \
                        and self.solar_reactive_power_readings_is_valid_list[iggy] == True:

                    # Check if preferred power factor is activated.
                    if self.preferred_pf_activated_list[iggy]:
                        # A preferred Pf has been re-applied. Check the current readings, they would be expected
                        # to be in the range of the preferred values, and the PCC quadrant should also have reverted
                        # back to the preferred one. It is OK if reactive power (especially the the PCC) changed sign.

                        # TODO: FOR SOLAR, RATHER THAN REACTIVE POWER CHANGE, SHOULD IT NOT BE REAL POWER CHANGE???

                        pcc_q_ok = gu.is_values_within_range(abs(self.preferred_pf_pcc_reactive_power_list[iggy]),
                                                             abs(self.pcc_reactive_power_average_list[iggy]),
                                                             self.preferred_q_condition_waver_percent)
                        solar_q_ok = gu.is_values_within_range(abs(self.preferred_pf_solar_reactive_power_list[iggy]),
                                                               abs(self.solar_reactive_power_average_list[iggy]),
                                                              self.preferred_q_condition_waver_percent)
                        present_pcc_quadrant = self.get_power_quadrant(self.pcc_real_power_average_list[iggy],
                                                                       self.pcc_reactive_power_average_list[iggy])
                        if self.preferred_pf_pcc_quadrant_list[iggy] == present_pcc_quadrant:
                            min_requirements_quandrant = MIN_REQ_PREF_PF_QUAD_SAME
                        else:
                            min_requirements_quandrant = MIN_REQ_PREF_PF_QUAD_CHANGE
                        if pcc_q_ok and solar_q_ok: # and quadrant_ok:
                            # The re-applied "preferred power factor" sent the system back to its, well,
                            # preferred power factor. Leave the system alone. The quadrant may have shifted,
                            # but as long as the reactive power at the PCC and SOLAR are within range, then
                            # the system can continue to run with the same preferred power factor. If it did
                            # flip, it's very likely that the next time around, the power factor at the PCC
                            # will flip quadrant yet again.

                            # ----- PF LOGGING -------------------------------------------------------------------------
                            self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_quandrant)
                            # ------------------------------------------------------------------------------------------

                            continue

                        else:
                            # Either PCC reactive power or solar real power move significantly enough that the
                            # preferred power factor condition no longer applies. Re-do power factor control.
                            self.preferred_pf_activated_list[iggy] = False
                    else:
                        # PCC reactive power "is on the move", preferred power factor is not active. Check if PCC
                        # reactive power flipped quadrant. Might it be time to active a preferred PF condition?
                        if self.pcc_last_quadrant_list[iggy] == POWER_QUADRANT_UNKNOWN:
                            # Must be 1st time here.
                            self.pcc_last_quadrant_list[iggy] = self.get_power_quadrant(self.pcc_real_power_average_list[iggy],
                                                                                        self.pcc_reactive_power_average_list[iggy])
                        else:
                            # Known quadrant. Check if the PCC fliped quadrant.
                            present_pcc_quadrant = self.get_power_quadrant(self.pcc_real_power_average_list[iggy],
                                                                           self.pcc_reactive_power_average_list[iggy])
                            if present_pcc_quadrant != self.pcc_last_quadrant_list[iggy]:
                                # Reactive power at the PCC flipped quadrant.
                                if self.previous_commanded_pf_list[iggy] == BASELINE_GENERIC_PF_DEFAULT:
                                    # This is not a "flip" per se, reactive power just went to where it needed to be.
                                    self.preferred_pf_activated_list[iggy] = False
                                else:
                                    # PCC flipped quadrant. Presumably, a small change in power factor caused the flip.
                                    # Would the previous power factor be preferred?
                                    #
                                    # If the absolute value of the PCC and SOLAR reactive power "before" and "now" are
                                    # within a specified percentage of each other, then the previous power factor is
                                    # preferred, and the system should stay there, as long as reactive power does not
                                    # wander much.
                                    if (gu.is_values_within_range(abs(retained_last_pcc_q),
                                                                  abs(self.pcc_reactive_power_average_list[iggy]),
                                                                 self.q_change_no_preef_q_condition_percent)) \
                                      and (gu.is_values_within_range(abs(retained_last_solar_q),
                                                                     abs(self.solar_reactive_power_average_list[iggy]),
                                                                     self.q_change_no_preef_q_condition_percent)):
                                        # The preferred power factor candidate is the desired one. Re-apply it, and
                                        # setup the data to monitor this condition, to avoid applying a cyclic Pf
                                        # between these 2 values. Save the reactive power at the PCC and solar sides
                                        # for subsequent checking, and set the flag that "preferred Pf condition" has
                                        # been applied".
                                        self.preferred_pf_activated_list[iggy] = True
                                        self.preferred_pf_pcc_reactive_power_list[iggy] = retained_last_pcc_q
                                        self.preferred_pf_pcc_quadrant_list[iggy] = self.pcc_last_quadrant_list[iggy]
                                        self.preferred_pf_solar_reactive_power_list[iggy] = retained_last_solar_q
                                        self.latest_commanded_pf_list[iggy] = self.preferred_pf_candidate_list[iggy]
                                        self.latest_commanded_pf_c_lead_list[iggy] = self.preferred_pf_c_lead_candidate_list[iggy]
                                        # self.preferred_pf_candidate_list[iggy]                 <- don't touch
                                        # self.preferred_pf_c_lead_candidate_list[iggy]          <- don't touch

                                        # ----- PF LOGGING -------------------------------------------------------------------
                                        self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, MIN_REQ_PREF_PF_RE_APPLIED)
                                        # ------------------------------------------------------------------------------------

                                        # Add the both the power factor and current_leading TLVs for this feed.
                                        number_of_user_tlv += 2
                                        tlv_packed_data = gu.append_dynamic_power_factor(tlv_packed_data,
                                                                                         self.latest_commanded_pf_tlv_list[iggy],
                                                                                         self.latest_commanded_pf_list[iggy],
                                                                                        self.latest_commanded_pf_c_lead_tlv_list[iggy],
                                                                                         self.latest_commanded_pf_c_lead_list[iggy])
                                        # Save the last commanded power factor.
                                        self.previous_commanded_pf_list[iggy] = self.latest_commanded_pf_list[iggy]
                                        self.previous_commanded_pf_c_lead_list[iggy] = self.latest_commanded_pf_c_lead_list[iggy]

                                        # Now that the TLV for power factor has been setup for this feed: CONTINUE!!!
                                        # Yes, continue to the end of this for loop FOR THIS FEED ONLY. Continue and
                                        # process PFC for any remaining feeds.
                                        continue

                                    else:
                                        # Yes, the PCC flipped quadrant, but it was a large swing.
                                        self.preferred_pf_activated_list[iggy] = False

                                self.pcc_last_quadrant_list[iggy] = present_pcc_quadrant

                else:
                    # Average reactive power readings not acceptable. No power factor command to be sent.
                    # Restore the reactive power averages, they are used for "preferred power factor".
                    self.pcc_reactive_power_average_list[iggy] = retained_last_pcc_q
                    self.solar_reactive_power_average_list[iggy] = retained_last_solar_q

                # With reactive power readings taken on the PCC and SOLAR sides, were the readings trustworthy?
                # If not, no power factor can be commanded during this cycle.
                if self.pcc_reactive_power_readings_is_valid_list[iggy] == True \
                    and self.solar_reactive_power_readings_is_valid_list[iggy] == True:

                    # First: have the minimum requirements for Pf control been met? This includes the minimum
                    # reactive power on the PCC, and the minimum real power on the solar side.
                    min_requirements_met_flag = self.is_minimum_threshold_requirements_met(self.pcc_reactive_power_average_list[iggy],
                                                                                           self.solar_real_power_average_list[iggy])
                    if min_requirements_met_flag == MIN_REQ_ALL_HAVE_BEEN_MET_FULL:
                        # There is sufficient reactive power on the PCC side, and sufficient real power on the solar
                        # side to exceeed "meter sensitivity" and read back meaningful values.
                        #
                        # Next: on the basis of real solar power, calculate solar reactive power capability at a max of 0.707 Pf.
                        #
                        # Generic Formula:
                        #     Q = sqrt( ((P/Pf)**2)       - (P**2) )
                        # or: Q = sqrt( (((P**2)/(Pf**2)) - (P**2) )   <- use this one
                        # or: Q = sqrt( (P**2) * ( 1/(Pf**2) - 1 ) )
                        #
                        # where Pf_square = 0.707**2 = 0.499849
                        #
                        # At the max power factor of 0.707, the reactive power VAr = real power W, so there is no actual
                        # calculation required. So the solar reactive power capability is simply the solar real power.
                        self.solar_reactive_power_capability_list[iggy] = self.solar_real_power_average_list[iggy]

                        # ---------------------------------------------------------
                        # This "ditty" helps avoid logging NOT_CAPABLE prematurely.
                        if not self.var_capability_calculated_at_least_once:
                            self.var_capability_calculated_at_least_once = True
                        # ---------------------------------------------------------

                        # Calculate the PCC reactive power target based on PCC real power and "target power factor".
                        #
                        # NOTE: in the initial implementation of this module, the notion of a "target PCC power factor"
                        #       other than 1.0 was proposed. Since then, this notion has been removed, and the target
                        #       Pf is always unity (1.0), which means (as per the math) that the reactive power target
                        #       is 0.

                        (solar_q_to_harvest,
                         solar_reactive_power_target,
                         target_pf_current_leading,
                         full_vars_reduction) = self.calculate_solar_reactive_power_target(self.pcc_reactive_power_average_list[iggy],
                                                                                           self.solar_reactive_power_average_list[iggy])
                        if full_vars_reduction:
                            min_requirements_met_flag = MIN_REQ_ALL_HAVE_BEEN_MET_FULL
                        else:
                            min_requirements_met_flag = MIN_REQ_ALL_HAVE_BEEN_MET_REDUCED

                        # REACTIVE POWER SHED DETECTION.
                        # If reactive power from the load has been removed, then the system will inch its way back
                        # to a power factor of 1 very very slowly. This is detected by:
                        power_factor_command_already_prepared = False
                        if self.is_both_q_readings_in_same_quadrant(self.solar_reactive_power_average_list[iggy],
                                                                    self.pcc_reactive_power_average_list[iggy]):
                            # Both readings in same quadrant. Check the difference, check for reactive power shedding.
                            q_diff = abs(abs(self.solar_reactive_power_average_list[iggy]) - abs(self.pcc_reactive_power_average_list[iggy]))
                            if q_diff <= self.minimum_reactive_power_threshold:
                                # Send 1.0/0 Pf. Check if it was already sent, as it can take time for the
                                # inverters to bring reactive power down.
                                min_requirements_met_flag = MIN_REQ_NOT_DETERMINED
                                if self.previous_commanded_pf_list[iggy] != BASELINE_GENERIC_PF_DEFAULT:
                                    # Send BASELINE TARGET Pf please.
                                    power_factor_command_already_prepared = True
                                    self.latest_commanded_pf_list[iggy] = BASELINE_GENERIC_PF_DEFAULT
                                    self.latest_commanded_pf_c_lead_list[iggy] = BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT
                                    min_requirements_met_flag = MIN_REQ_Q_SHEDDING
                                    number_of_user_tlv += 2
                                    tlv_packed_data = gu.append_dynamic_power_factor(tlv_packed_data,
                                                                                     self.latest_commanded_pf_tlv_list[iggy],
                                                                                    self.latest_commanded_pf_list[iggy],
                                                                                     self.latest_commanded_pf_c_lead_tlv_list[iggy],
                                                                                    self.latest_commanded_pf_c_lead_list[iggy])
                                    # Save the last commanded power factor.
                                    self.preferred_pf_candidate_list[iggy] = self.previous_commanded_pf_list[iggy]
                                    self.preferred_pf_c_lead_candidate_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                                    self.previous_commanded_pf_list[iggy] = self.latest_commanded_pf_list[iggy]
                                    self.previous_commanded_pf_c_lead_list[iggy] = self.latest_commanded_pf_c_lead_list[iggy]

                                    # Preferred power factor conditions ... no more.
                                    self.preferred_pf_activated_list[iggy] = False

                                else:
                                    # It was already sent.
                                    min_requirements_met_flag = MIN_REQ_PF_SAME_NO_CHANGE
                                    power_factor_command_already_prepared = True

                                # ----- PF LOGGING ---------------------------------------------------------------------
                                self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                                # --------------------------------------------------------------------------------------

                        if not power_factor_command_already_prepared:

                            self.solar_q_to_harvest_list[iggy] = solar_q_to_harvest
                            self.solar_q_target_list[iggy] = solar_reactive_power_target
                            self.latest_commanded_pf_c_lead_list[iggy] = target_pf_current_leading

                            # Does solar reactive power capability exceed that to be harvested by the solar field?
                            if (abs(self.solar_reactive_power_capability_list[iggy]) > abs(self.solar_q_target_list[iggy])):
                                # Yes, sufficient real power on the solar side. Calculate Pf to command inverters
                                # with, based on formula: Pf = P / ( sqrt(sqr(P) + sqr(Q) ) )
                                # Real and reactive power can be +ve/-ve, but Pf is always treated as a +ve value only.
                                # So there are several ways to arrive at a caculated (but positive) value. Here, Pf will
                                # be arrived at in 3 steps:
                                #
                                # 1. P squared = absolute(P) ** 2
                                # 2. Q squared = absolute(Q) ** 2
                                # 3. Pf = absolute(P) / square root (P squared + Q squared)
                                p_square = abs(self.solar_real_power_average_list[iggy]) ** 2
                                q_square = abs(self.solar_q_target_list[iggy]) ** 2
                                pf_as_float = abs(self.solar_real_power_average_list[iggy]) / math.sqrt(p_square + q_square)
                                # Round power factor to 2 decimal places. Do it this way since straight rounding of
                                # the calculated value does not round up/down the 3rd decimal as expected (it just
                                # truncates it).
                                pf_as_string = ("%.2f" % pf_as_float)
                                pf_as_rounded_float = float(pf_as_string)
                                self.latest_commanded_pf_list[iggy] = pf_as_rounded_float

                                # Will the current Pf compared to the last one effect any change in Q by the inverters?

                                new_pf_effect = self.new_pf_effectiveness(self.latest_commanded_pf_list[iggy],
                                                                          self.latest_commanded_pf_c_lead_list[iggy],
                                                                          self.previous_commanded_pf_list[iggy],
                                                                          self.previous_commanded_pf_c_lead_list[iggy])
                                if new_pf_effect == NEW_PF_CHANGE_IN_Q_WILL_OCCUR:
                                    # The Pf update to be commanded will result in some reactive power change on the
                                    # solar side. Add the both the power factor and current_leading TLVs for this feed.
                                    number_of_user_tlv += 2
                                    tlv_packed_data = gu.append_dynamic_power_factor(tlv_packed_data,
                                                                                     self.latest_commanded_pf_tlv_list[iggy],
                                                                                     self.latest_commanded_pf_list[iggy],
                                                                                     self.latest_commanded_pf_c_lead_tlv_list[iggy],
                                                                                     self.latest_commanded_pf_c_lead_list[iggy])
                                    # Save the last commanded power factor.
                                    self.preferred_pf_candidate_list[iggy] = self.previous_commanded_pf_list[iggy]
                                    self.preferred_pf_c_lead_candidate_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                                    self.previous_commanded_pf_list[iggy] = self.latest_commanded_pf_list[iggy]
                                    self.previous_commanded_pf_c_lead_list[iggy] = self.latest_commanded_pf_c_lead_list[iggy]

                                else:
                                    # The calculated Pf for the desired change in reactive power will not result in
                                    # any change, so the inverters are not commanded. The Pf change (assuming current
                                    # leading value did not flip) could be because it is in one of the inverter's 4
                                    # "flat spots" where no effective change will occur. The "latest" PF needs to
                                    # revert to what it was previously, since the "latest" is not used.
                                    self.latest_commanded_pf_list[iggy] = self.previous_commanded_pf_list[iggy]
                                    self.latest_commanded_pf_c_lead_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                                    if new_pf_effect == NEW_PF_NO_Q_CHANGE_FOR_SAME_PF:
                                        min_requirements_met_flag = MIN_REQ_PF_SAME_NO_CHANGE
                                    else:
                                        min_requirements_met_flag = MIN_REQ_PF_FLAT_SPOT_NO_CHANGE
                                # ----- PF LOGGING -----------------------------------------------------------------
                                self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                                # ----------------------------------------------------------------------------------

                            else:
                                # Not enough power. Inverters will need to be pegged to the max power factor value.
                                self.latest_commanded_pf_list[iggy] = MAX_SG424_POWER_FACTOR
                                self.latest_commanded_pf_c_lead_list[iggy] = BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT

                                new_pf_effect = self.new_pf_effectiveness(self.latest_commanded_pf_list[iggy],
                                                                          self.latest_commanded_pf_c_lead_list[iggy],
                                                                          self.previous_commanded_pf_list[iggy],
                                                                          self.previous_commanded_pf_c_lead_list[iggy])
                                if new_pf_effect == NEW_PF_CHANGE_IN_Q_WILL_OCCUR:
                                    # The Pf update to be commanded will result in some reactive power change on the
                                    # solar side.
                                    min_requirements_met_flag = MIN_REQ_SOLAR_MAXED_OUT
                                    number_of_user_tlv += 2
                                    tlv_packed_data = gu.append_dynamic_power_factor(tlv_packed_data,
                                                                                     self.latest_commanded_pf_tlv_list[iggy],
                                                                                     self.latest_commanded_pf_list[iggy],
                                                                                     self.latest_commanded_pf_c_lead_tlv_list[iggy],
                                                                                     self.latest_commanded_pf_c_lead_list[iggy])
                                    # Save the last commanded power factor.
                                    self.preferred_pf_candidate_list[iggy] = self.previous_commanded_pf_list[iggy]
                                    self.preferred_pf_c_lead_candidate_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                                    self.previous_commanded_pf_list[iggy] = self.latest_commanded_pf_list[iggy]
                                    self.previous_commanded_pf_c_lead_list[iggy] = self.latest_commanded_pf_c_lead_list[iggy]

                                else:
                                    # No power factor command to be sent. The Pf change (assuming current
                                    # leading value did not flip) could be because it is in one of the inverter's 4
                                    # "flat spots" where no effective change will occur. The "latest" PF needs to
                                    # revert to what it was previously, since the "latest" is not used.
                                    self.latest_commanded_pf_list[iggy] = self.previous_commanded_pf_list[iggy]
                                    self.latest_commanded_pf_c_lead_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                                    if new_pf_effect == NEW_PF_NO_Q_CHANGE_FOR_SAME_PF:
                                        min_requirements_met_flag = MIN_REQ_PF_SAME_NO_CHANGE
                                    else:
                                        min_requirements_met_flag = MIN_REQ_PF_FLAT_SPOT_NO_CHANGE

                                # ----- PF LOGGING ------------------------------------------------------------------
                                self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                                # -----------------------------------------------------------------------------------

                    else:
                        # Insufficient reactive power on the PCC side, or real power on the solar sides.
                        # ----- PF LOGGING ------------------------------------------------------------------
                        self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                        # -----------------------------------------------------------------------------------

                        # As minimum threshold requirements have not been met:
                        #   if  PCC reactive power below threshold or SOLAR real power below threshold
                        #   AND if the last commanded power factor was not unity:
                        #       -> then: issue such a power factor command.
                        if (  (min_requirements_met_flag == MIN_REQ_PCC_AND_SOLAR_LOW) \
                          and (self.previous_commanded_pf_list[iggy] != BASELINE_GENERIC_PF_DEFAULT)):
                            # Send Pf of 1.0 please.
                            self.latest_commanded_pf_list[iggy] = BASELINE_GENERIC_PF_DEFAULT
                            self.latest_commanded_pf_c_lead_list[iggy] = BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT
                            number_of_user_tlv += 2
                            tlv_packed_data = gu.append_dynamic_power_factor(tlv_packed_data,
                                                                             self.latest_commanded_pf_tlv_list[iggy],
                                                                             self.latest_commanded_pf_list[iggy],
                                                                             self.latest_commanded_pf_c_lead_tlv_list[iggy],
                                                                             self.latest_commanded_pf_c_lead_list[iggy])
                            # Save the last commanded power factor.
                            self.preferred_pf_activated_list[iggy] = False
                            self.preferred_pf_candidate_list[iggy] = self.previous_commanded_pf_list[iggy]
                            self.preferred_pf_c_lead_candidate_list[iggy] = self.previous_commanded_pf_c_lead_list[iggy]
                            self.previous_commanded_pf_list[iggy] = self.latest_commanded_pf_list[iggy]
                            self.previous_commanded_pf_c_lead_list[iggy] = self.latest_commanded_pf_c_lead_list[iggy]

                            # ----- PF LOGGING ------------------------------------------------------------------
                            self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                            # -----------------------------------------------------------------------------------

                else:
                    # PCC and/or SOLAR reactive power readings were not trustworthy during this cycle.
                    self.last_meter_readings_rejected = True

                    # ----- PF LOGGING -------------------------------------------------------------------------
                    if self.power_factor_control_command_logging_list[iggy] == LOG_PF_CONTROL_DATA_ENABLED:
                        if self.pcc_reactive_power_readings_is_valid_list[iggy] == False \
                                and self.solar_reactive_power_readings_is_valid_list[iggy] == True:
                            min_requirements_met_flag = MIN_REQ_PCC_READINGS_REJECT
                        elif self.pcc_reactive_power_readings_is_valid_list[iggy] == True \
                                and self.solar_reactive_power_readings_is_valid_list[iggy] == False:
                            min_requirements_met_flag = MIN_REQ_SOLAR_READINGS_REJECT
                        else:
                            # In the very rare case, both meters gave a complete mish-mash of readings.
                            min_requirements_met_flag =  MIN_REQ_PCC_SOLAR_READINGS_REJECT

                        self.super_duper_meter_logging(LOGGING_COMMANDED_PF, iggy, min_requirements_met_flag)
                    # ----------------------------------------------------------------------------------------------

            else:
                # Power factor control is disabled for this feed.
                pass

        # for iggy in ... self.feed_name_list

        # --------------------------------------------------------------------------------------------------------------
        #
        # POWER FACTOR SENDING SECTION.
        #
        # Dynamic power factor control is sent to the field of inverter on a per-feed basis. It is sent in a single
        # TLV-encoded packet containing the values for all configured feeds that are to be updated.
        if number_of_user_tlv > 0:
            # Create the master TLV, then add the user-specified TLVs.
            self.num_dynamic_pf_commands += 1
            packed_data = gu.add_master_tlv(number_of_user_tlv)
            packed_data += tlv_packed_data
            # self.log_this_hex_data(packed_data) # <- TO LOG PACKET'S TLV HEX-ENCODED TLV DATA

            # -------------------------------------------------------------------------
            donut_care = gu.is_send_tlv_packed_data(self.multicast_all_ip, packed_data)
            # -------------------------------------------------------------------------
        else:
            # None of the feeds are to be commanded.
            pass

    def gui_calculate_average_real_power(self):
        for iggy in range(0, len(self.feed_name_list)):
            real_power_sum = 0
            for pop in range(0,self.gui_samples_per_interval):
                real_power_sum += self.gui_pcc_readings_real_power_list[iggy][pop]
            average = real_power_sum / self.gui_samples_per_interval
            self.gui_pcc_averages_real_power_list[iggy] = average

    def gui_calculate_average_reactive_power(self):
        for iggy in range(0, len(self.feed_name_list)):
            reactive_power_sum = 0
            for pop in range(0, self.gui_samples_per_interval):
                reactive_power_sum += self.gui_pcc_readings_reactive_power_list[iggy][pop]
            average = reactive_power_sum / self.gui_samples_per_interval
            self.gui_pcc_averages_reactive_power_list[iggy] = average

    def get_power_quadrant(self, real_power_float, reactive_power_float):
        power_quadrant = 0
        if real_power_float >= 0.0:
            # IMPORTING power on the PCC. It's in quadrant I or IV depending on reactive power.
            if reactive_power_float >= 0.0:
                power_quadrant = POWER_QUADRANT_1
            else:
                power_quadrant = POWER_QUADRANT_4
        else:
            # EXPORTING power on the PCC. It's in quadrant II or III depending on reactive power.
            if reactive_power_float >= 0.0:
                power_quadrant = POWER_QUADRANT_2
            else:
                power_quadrant = POWER_QUADRANT_3
        return power_quadrant

    def gui_write_all_pcc_averages(self):
        # Based on the set of instantaneous power factor and reactive power readings from the pcc_readings_1 meter,
        # calculate the average and write them to the gui pcc_averages_XXX tables. Without any other constraint, simply
        # adding a new entry to this table would cause it to grow until system resources are totally consumed. So the
        # number of rows in this table must be constrained.
        #
        # A mysql "replace/select/mod" feature is used, and as with the NXO meter table, this link provides an example
        # of how to accomplish this:
        #
        # http://dt.deviantart.com/journal/Build-Your-Own-Circular-Log-with-MySQL-222550965
        # REPLACE INTO circular_log_table
        # SET row_id = (SELECT COALESCE(MAX(log_id), 0) % MAX_CIRCULAR_LOG_ROWS + 1
        # FROM circular_log_table AS t),
        # payload = 'I like turtles.'
        # yaddy yaddy yaddy ...
        args_list = []

        # If logging of this data was requested:
        this_log_string = "EXCEL,GUI"

        # Build the main portion of the replace string.
        #replace_string = "REPLACE INTO %s SET %s = (SELECT MOD(COALESCE(MAX(%s), 0),%s)+1 FROM %s AS t), " \
        replace_string = "REPLACE INTO %s SET %s = (SELECT MOD(COALESCE(MAX(%s), 0),%s) FROM %s AS t), " \
                                                                        % (DB_PCC_INTERVAL_AVERAGES_TABLE,
                                                                           DB_PCC_INTERVAL_AVERAGES_ROW_ID_COLUMN_NAME,
                                                                           DB_PCC_INTERVAL_AVERAGES_READ_ID_COLUMN_NAME,
                                                                           self.gui_max_size,
                                                                           DB_PCC_INTERVAL_AVERAGES_TABLE)

        # Now build/add the string and argument list based on configured feeds.
        for iggy in range(0, len(self.feed_name_list)):
            replace_string += "%s = %s, %s = %s, %s = %s, " \
                                                     % (self.gui_pcc_averages_real_power_name_list[iggy], "%s",
                                                        self.gui_pcc_averages_reactive_power_name_list[iggy], "%s",
                                                        self.gui_pcc_averages_quadrant_name_list[iggy], "%s")
            power_quadrant = self.get_power_quadrant(self.gui_pcc_averages_real_power_list[iggy],
                                                     self.gui_pcc_averages_reactive_power_list[iggy])
            args_list.append(str(self.gui_pcc_averages_real_power_list[iggy]))
            args_list.append(str(self.gui_pcc_averages_reactive_power_list[iggy]))
            args_list.append(str(power_quadrant))

            this_log_string += "," + self.feed_name_list[iggy] + "," \
                               + str(self.gui_pcc_averages_real_power_list[iggy]) + ","  \
                               + str(self.gui_pcc_averages_reactive_power_list[iggy]) + "," \
                               + str(power_quadrant)

        # And finally, add the interval time.
        interval_time = time.strftime("%Y-%m-%d %H:%M:%S")
        replace_string += "%s = %s;" % (DB_PCC_INTERVAL_AVERAGES_INTERVAL_TIME_COLUMN_NAME, "%s")
        args_list.append(str(interval_time))

        # Now insert/replace into the database:
        prepared_act_on_database(EXECUTE, replace_string, args_list)

        # If the user enabled logging of interval data:
        if self.gui_logging == GUI_LOG_YES:
            self.logger.info(this_log_string)

    def is_build_feed_names_lists_ok(self):

        build_feed_names_ok = True

        self.feed_name_list = []

        self.latest_commanded_pf_list = []
        self.latest_commanded_pf_c_lead_list = []
        self.latest_commanded_pf_tlv_list = []
        self.latest_commanded_pf_c_lead_tlv_list = []

        # For "preferred power factor" support:
        self.preferred_pf_activated_list = []
        self.preferred_pf_pcc_reactive_power_list = []
        self.preferred_pf_pcc_quadrant_list = []
        self.preferred_pf_solar_reactive_power_list = []
        self.preferred_pf_candidate_list = []
        self.preferred_pf_c_lead_candidate_list = []
        self.previous_commanded_pf_list = []
        self.previous_commanded_pf_c_lead_list = []

        # For power factor control and command:
        self.pcc_real_power_fp_name_list = []
        self.pcc_amperage_fp_name_list = []
        self.pcc_reactive_power_fp_name_list = []

        self.solar_real_power_fp_name_list = []
        self.solar_amperage_fp_name_list = []
        self.solar_reactive_power_fp_name_list = []

        self.pcc_real_power_average_list = []
        self.pcc_amperage_average_list = []
        self.pcc_reactive_power_average_list = []
        self.pcc_last_quadrant_list = []
        self.pcc_reactive_power_readings_is_valid_list = []

        self.solar_real_power_average_list = []
        self.solar_amperage_average_list = []
        self.solar_reactive_power_average_list = []
        self.solar_reactive_power_readings_is_valid_list = []
        self.solar_q_to_harvest_list = []
        self.solar_q_target_list = []
        self.solar_reactive_power_capability_list = []

        # For power factor/quadrant GUI display:
        self.gui_pcc_readings_real_power_list = []
        self.gui_pcc_readings_reactive_power_list = []
        self.gui_pcc_averages_real_power_list = []
        self.gui_pcc_averages_reactive_power_list = []
        self.gui_pcc_averages_real_power_name_list = []
        self.gui_pcc_averages_reactive_power_name_list = []
        self.gui_pcc_averages_quadrant_name_list = []

        # Extract the "feed" for all rows from the gateway meter table.
        sql = "SELECT feed_name FROM power_regulators WHERE has_solar=1 GROUP BY feed_name ORDER BY feed_name"
        gateway_meter_row = prepared_act_on_database(FETCH_ALL, sql, ())
        if gateway_meter_row:
            # Construct the list of feed names.
            for feed_name_data in gateway_meter_row:
                self.gui_pcc_averages_real_power_list.append(0.0)
                self.gui_pcc_averages_reactive_power_list.append(0.0)
                this_feed_name = feed_name_data['feed_name']

                self.latest_commanded_pf_list.append(BASELINE_GENERIC_PF_DEFAULT)
                self.latest_commanded_pf_c_lead_list.append(BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT)

                self.preferred_pf_activated_list.append(False)
                self.preferred_pf_pcc_reactive_power_list.append(0.0)
                self.preferred_pf_pcc_quadrant_list.append(POWER_QUADRANT_UNKNOWN)
                self.preferred_pf_solar_reactive_power_list.append(0.0)
                self.preferred_pf_candidate_list.append(BASELINE_GENERIC_PF_DEFAULT)
                self.preferred_pf_c_lead_candidate_list.append(BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT)

                self.previous_commanded_pf_list.append(BASELINE_GENERIC_PF_DEFAULT)
                self.previous_commanded_pf_c_lead_list.append(BASELINE_GENERIC_PF_CURR_LEAD_DEFAULT)

                self.pcc_real_power_average_list.append(0.0)
                self.pcc_amperage_average_list.append(0.0)
                self.pcc_reactive_power_average_list.append(0.0)
                self.pcc_last_quadrant_list.append(POWER_QUADRANT_UNKNOWN)
                self.pcc_reactive_power_readings_is_valid_list.append(True)

                self.solar_real_power_average_list.append(0.0)
                self.solar_amperage_average_list.append(0.0)
                self.solar_reactive_power_average_list.append(0.0)
                self.solar_reactive_power_readings_is_valid_list.append(True)
                self.solar_q_to_harvest_list.append(0.0)
                self.solar_q_target_list.append(0.0)
                self.solar_reactive_power_capability_list.append(0.0)

                self.latest_commanded_pf_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_F32_TLV_TYPE)
                self.latest_commanded_pf_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_F32_TLV_TYPE)
                self.latest_commanded_pf_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_F32_TLV_TYPE)
                self.latest_commanded_pf_c_lead_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_LEADING_U16_TLV_TYPE)
                self.latest_commanded_pf_c_lead_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_LEADING_U16_TLV_TYPE)
                self.latest_commanded_pf_c_lead_tlv_list.append(GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_LEADING_U16_TLV_TYPE)

                if this_feed_name == "A":
                    self.feed_name_list.append(this_feed_name)
                    self.pcc_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_A_FP_COLUMN_NAME)
                    self.pcc_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_A_FP_COLUMN_NAME)
                    self.pcc_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_A_FP_COLUMN_NAME)

                    self.solar_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_A_FP_COLUMN_NAME)
                    self.solar_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_A_FP_COLUMN_NAME)
                    self.solar_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_A_FP_COLUMN_NAME)

                    self.gui_pcc_averages_real_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REAL_POWER_A_COLUMN_NAME)
                    self.gui_pcc_averages_reactive_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REACTIVE_POWER_A_COLUMN_NAME)
                    self.gui_pcc_averages_quadrant_name_list.append(DB_PCC_INTERVAL_AVERAGES_QUAD_FEED_A_COLUMN_NAME)

                elif this_feed_name == "B":
                    self.feed_name_list.append(this_feed_name)

                    self.pcc_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_B_FP_COLUMN_NAME)
                    self.pcc_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_B_FP_COLUMN_NAME)
                    self.pcc_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_B_FP_COLUMN_NAME)

                    self.solar_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_B_FP_COLUMN_NAME)
                    self.solar_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_B_FP_COLUMN_NAME)
                    self.solar_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_B_FP_COLUMN_NAME)

                    self.gui_pcc_averages_real_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REAL_POWER_B_COLUMN_NAME)
                    self.gui_pcc_averages_reactive_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REACTIVE_POWER_B_COLUMN_NAME)
                    self.gui_pcc_averages_quadrant_name_list.append(DB_PCC_INTERVAL_AVERAGES_QUAD_FEED_B_COLUMN_NAME)

                elif this_feed_name == "C":
                    self.feed_name_list.append(this_feed_name)

                    self.pcc_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_C_FP_COLUMN_NAME)
                    self.pcc_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_C_FP_COLUMN_NAME)
                    self.pcc_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_C_FP_COLUMN_NAME)

                    self.solar_real_power_fp_name_list.append(METER_READINGS_TABLE_REAL_POWER_C_FP_COLUMN_NAME)
                    self.solar_amperage_fp_name_list.append(METER_READINGS_TABLE_AMPERAGE_C_FP_COLUMN_NAME)
                    self.solar_reactive_power_fp_name_list.append(METER_READINGS_TABLE_REACTIVE_POWER_C_FP_COLUMN_NAME)

                    self.gui_pcc_averages_real_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REAL_POWER_C_COLUMN_NAME)
                    self.gui_pcc_averages_reactive_power_name_list.append(DB_PCC_INTERVAL_AVERAGES_REACTIVE_POWER_C_COLUMN_NAME)
                    self.gui_pcc_averages_quadrant_name_list.append(DB_PCC_INTERVAL_AVERAGES_QUAD_FEED_C_COLUMN_NAME)

                elif this_feed_name == "Aggregate":
                    # Until further clarification, power factor control is inoperative in this configuration.
                    this_string = "AGGREGATE CONFIGURATION NOT SUPPORTED"
                    self.logger.info(this_string)

                else:
                    # This is not good. Feed name *must* be one of "A", "B", "C" or "Aggregate". Empty all lists that
                    # may have been populated, log a critical notice and don't let the power factor control daemon do
                    # anything useful until the situation has been corrected. The system will have to be restarted.
                    build_feed_names_ok = False
                    this_string = "INVAL FEED NAME %s IN GATEWAY METER TABLE - PFCTRL INOPERATIVE" % (this_feed_name)
                    self.logger.critical(this_string)

                    del self.feed_name_list[:]
                    del self.latest_commanded_pf_list[:]
                    del self.latest_commanded_pf_c_lead_list[:]
                    del self.preferred_pf_activated_list[:]
                    del self.preferred_pf_pcc_reactive_power_list[:]
                    del self.preferred_pf_pcc_quadrant_list[:]
                    del self.preferred_pf_solar_reactive_power_list[:]
                    del self.preferred_pf_candidate_list[:]
                    del self.preferred_pf_c_lead_candidate_list[:]
                    del self.previous_commanded_pf_list[:]
                    del self.previous_commanded_pf_c_lead_list[:]
                    del self.pcc_real_power_average_list[:]
                    del self.pcc_amperage_average_list[:]
                    del self.pcc_reactive_power_average_list[:]
                    del self.pcc_last_quadrant_list[:]
                    del self.pcc_reactive_power_readings_is_valid_list[:]
                    del self.solar_real_power_average_list[:]
                    del self.solar_amperage_average_list[:]
                    del self.solar_reactive_power_average_list[:]
                    del self.gui_pcc_averages_real_power_list[:]
                    del self.gui_pcc_averages_reactive_power_list[:]

                    del self.solar_reactive_power_readings_is_valid_list[:]
                    del self.solar_q_to_harvest_list[:]
                    del self.solar_q_target_list[:]
                    del self.solar_reactive_power_capability_list[:]
                    del self.pcc_real_power_fp_name_list[:]
                    del self.pcc_amperage_fp_name_list[:]
                    del self.pcc_reactive_power_fp_name_list[:]
                    del self.solar_real_power_fp_name_list[:]
                    del self.solar_amperage_fp_name_list[:]
                    del self.solar_reactive_power_fp_name_list[:]
                    del self.latest_commanded_pf_tlv_list[:]
                    del self.latest_commanded_pf_c_lead_tlv_list[:]
                    del self.gui_pcc_averages_real_power_name_list[:]
                    del self.gui_pcc_averages_reactive_power_name_list[:]
                    del self.gui_pcc_averages_quadrant_name_list[:]
                    return build_feed_names_ok

            # Declare these arrays to hold the PCC Pf/real/reactive power and power factor readings for all feeds.
            self.gui_pcc_readings_real_power_list = [[0 for y in range(self.gui_samples_per_interval)] \
                                                 for x in range(len(self.feed_name_list))]

            self.gui_pcc_readings_reactive_power_list = [[0 for y in range(self.gui_samples_per_interval)] \
                                                     for x in range(len(self.feed_name_list))]
        return build_feed_names_ok

    def setup_running_parameters(self):
        select_string = "SELECT * FROM %s;" % DB_POWER_FACTOR_CONTROL_CONTROL_TABLE
        pfc_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        if pfc_row:

            # ---------------------------------------------------------------------------------------------------------
            # This section creates empty lists for all necessary parameters for the power factor control daemon,
            # based on 3 feeds. The system may or many not be configured with all 3 feeds, or the system may be
            # a Y-connected system with essentially only a single feed.
            self.power_factor_control_enable_list = []
            self.power_factor_control_command_logging_list = []
            self.power_factor_bounce_data_logging_list = []
            self.power_factor_control_data_logging_list = []

            self.power_factor_control_enable_list.append(POWER_FACTOR_CONTROL_ENABLE_DISABLED)
            self.power_factor_control_enable_list.append(POWER_FACTOR_CONTROL_ENABLE_DISABLED)
            self.power_factor_control_enable_list.append(POWER_FACTOR_CONTROL_ENABLE_DISABLED)
            self.power_factor_control_command_logging_list.append(LOG_PF_CONTROL_DATA_DISABLED)
            self.power_factor_control_command_logging_list.append(LOG_PF_CONTROL_DATA_DISABLED)
            self.power_factor_control_command_logging_list.append(LOG_PF_CONTROL_DATA_DISABLED)
            self.power_factor_bounce_data_logging_list.append(LOG_POWER_FACTOR_BOUNCE_DATA_DISABLED)
            self.power_factor_bounce_data_logging_list.append(LOG_POWER_FACTOR_BOUNCE_DATA_DISABLED)
            self.power_factor_bounce_data_logging_list.append(LOG_POWER_FACTOR_BOUNCE_DATA_DISABLED)
            self.power_factor_control_data_logging_list.append(LOG_PF_STUDY_DATA_DISABLED)
            self.power_factor_control_data_logging_list.append(LOG_PF_STUDY_DATA_DISABLED)
            self.power_factor_control_data_logging_list.append(LOG_PF_STUDY_DATA_DISABLED)
            # ---------------------------------------------------------------------------------------------------------

            # Populate the fields based on data from the database's power_factor_control table. The 2 thresholds
            # from the database are in VAr and watts, when read out, convert to kVAr and kw.
            self.wait_period_B4_start =  pfc_row[POWER_FACTOR_CONTROL_TABLE_WAIT_B4_START_COLUMN_NAME]
            self.pf_command_frequency = pfc_row[POWER_FACTOR_CONTROL_TABLE_PF_COMMAND_FREQ_COLUMN_NAME]
            self.num_readings_for_pf_upd = pfc_row[POWER_FACTOR_CONTROL_TABLE_NUM_READINGS_FOR_PF_UPD_COLUMN_NAME]
            self.minimum_acceptable_meter_readings_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_ACCEPT_METER_READS_PCT_COLUMN_NAME]
            self.preferred_q_condition_waver_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_PREF_Q_WAVER_PCT_COLUMN_NAME]
            self.q_change_no_preef_q_condition_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_Q_CHANGE_NO_PREF_PCT_COLUMN_NAME]
            self.reduced_harvest_percent = pfc_row[POWER_FACTOR_CONTROL_TABLE_REDUCED_HARVEST_PCT_COLUMN_NAME]

            self.minimum_reactive_power_threshold = (pfc_row[POWER_FACTOR_CONTROL_TABLE_MINIMUM_Q_THRESHOLD_COLUMN_NAME] * 1.0) / 1000
            self.minimum_real_power_threshold = (pfc_row[POWER_FACTOR_CONTROL_TABLE_MINIMUM_P_THRESHOLD_COLUMN_NAME] * 1.0) / 1000

            self.power_factor_control_enable_list[0] = pfc_row[POWER_FACTOR_ENABLE_A]
            self.power_factor_control_enable_list[1] = pfc_row[POWER_FACTOR_ENABLE_B]
            self.power_factor_control_enable_list[2] = pfc_row[POWER_FACTOR_ENABLE_C]
            self.power_factor_control_command_logging_list[0] = pfc_row[POWER_FACTOR_LOG_A]
            self.power_factor_control_command_logging_list[1] = pfc_row[POWER_FACTOR_LOG_B]
            self.power_factor_control_command_logging_list[2] = pfc_row[POWER_FACTOR_LOG_C]
            self.power_factor_bounce_data_logging_list[0] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_A]
            self.power_factor_bounce_data_logging_list[1] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_B]
            self.power_factor_bounce_data_logging_list[2] = pfc_row[POWER_FACTOR_BOUNCE_DATA_LOG_C]
            self.power_factor_control_data_logging_list[0] = pfc_row[POWER_FACTOR_DATA_LOG_A]
            self.power_factor_control_data_logging_list[1] = pfc_row[POWER_FACTOR_DATA_LOG_B]
            self.power_factor_control_data_logging_list[2] = pfc_row[POWER_FACTOR_DATA_LOG_C]
            self.log_power_factor_data_frequency = pfc_row[POWER_FACTOR_DATA_LOG_FREQUENCY]
            self.gui_logging = pfc_row[POWER_FACTOR_CONTROL_TABLE_GUI_LOG_COLUMN_NAME]
            self.gui_frequency = pfc_row[POWER_FACTOR_CONTROL_TABLE_GUI_FREQ_COLUMN_NAME]
            self.gui_samples = pfc_row[POWER_FACTOR_CONTROL_TABLE_GUI_SAMPLES_COLUMN_NAME]
            self.gui_max_size = pfc_row[POWER_FACTOR_CONTROL_TABLE_GUI_MAX_SIZE_COLUMN_NAME]

            self.gui_samples_per_interval = self.gui_samples * METER_READINGS_SAMPLES_PER_SECOND

    def allow_wait_period_before_start_to_expire(self):
        # The Gateway has just started. Or, the monitor has restarted this power factor control daemon.
        # Either way, allow the "wait period before start" period to expire. This gives the inverters
        # time to ramp up to normal power production level.
        loco_loop_timer = 0
        for iggy in range(0, self.wait_period_B4_start):
            time.sleep(1)
            loco_loop_timer += 1
            if loco_loop_timer >= SET_UPDATE_BIT_FREQUENCY:
                self.update()
                loco_loop_timer = 0

        # With the initial wait period expired, now time to return and do usefull work.
        # Display a terse message with the power factor with which the inverters should
        # have been commanded by the NXO inverter daemon upon its startup.
        self.current_utc = int(time.time())
        this_time = time.strftime("%I:%M:%S")
        this_string = "***PFCTRL WAIT-B4_START EXPIRED; MIN Q/P THRESHOLD: %skVAr/%skW (AT %s)***" % \
                                                          ('{0:.3f}'.format(self.minimum_reactive_power_threshold),
                                                           '{0:.3f}'.format(self.minimum_real_power_threshold),
                                                           this_time)
        self.logger.info(this_string)

    def is_pf_data_logging_enabled_any_feed(self):
        data_logging_enabled = False
        for iggy in range(0, len(self.feed_name_list)):
            if self.power_factor_control_data_logging_list[iggy] == LOG_PF_STUDY_DATA_ENABLED:
                data_logging_enabled = True
                break
        return data_logging_enabled

    def log_power_factor_control_data(self):
        # First read and calculate averages for PCC and solar readings on all feeds.
        for iggy in range(0, len(self.feed_name_list)):
            if self.power_factor_control_data_logging_list[iggy] == LOG_PF_STUDY_DATA_ENABLED:
                self.read_pcc_readings_1_and_calculate_meter_averages_one_feed(iggy)
                self.read_solar_readings_1_and_calculate_meter_averages_one_feed(iggy)
                self.super_duper_meter_logging(LOGGING_FOR_INFO_ONLY, iggy, MIN_REQ_DATA_LOGGING_ONLY)

    def run(self):

        # Synopsis: - write a terse info message to the log indicating that this daemon has started
        #           - set the started field in the power_factor_control table to 1
        #           - wait before doing any actual useful work. As the gateway just started,
        #             keepAlive will soon start, and then inverters will start making power.
        #             So no use doing any power factor control until then.
        #           - enter into an infinite loop:
        #                 - sleep a wee bit
        #                 - regularly set the updated field in the auditor_control table
        #                 - for GUI meter update:
        #                       - take pcc readings (power factor, reactive power)
        #                       - when enough readings for an interval have been taken,
        #                         average and write them to the gui pcc_averages_XXX table
        #                 - for power factor control:
        #                       - when the power factor control cycle expires, calculate the reactive
        #                         power and power factor required of the inverter to compensate that on
        #                         the PCC, and command the inverters.

        # ---------------------------------------------------------------------
        # Get the current (utc) time, and prepare a startup banner message.
        this_time = time.strftime("%I:%M:%S")
        this_time_utc = int(time.time())
        self.current_utc = this_time_utc
        self.last_gui_readings_utc = self.current_utc
        self.last_inverter_update_utc = self.current_utc
        self.last_log_power_factor_control_utc = self.current_utc
        startup_banner_string = ""

        # ---------------------------------------------------------------------
        # Retrieve running parameters from the power_factor_control table.
        self.setup_running_parameters()

        # Determine the feeds and the lists to be managed. Complete and log the PFC startup banner.
        if self.is_build_feed_names_lists_ok():
            if len(self.feed_name_list) > 0:
                startup_banner_string += "- WILL MANAGE FEEDS "
                for iggy in range (0, len(self.feed_name_list)):
                    startup_banner_string += self.feed_name_list[iggy]
                    if (iggy != (len(self.feed_name_list) - 1)):
                        startup_banner_string += ", "
                startup_banner_string += "***"
                self.logger.info(startup_banner_string)
            else:
                startup_banner_string += " - NO CONFIGURED FEEDS TO MANAGE***"
                self.logger.info(startup_banner_string)

                # TODO: no feeds because system not configured? If eventually configured, will gateway be restarted?
                # TODO  If restart not required, then this loop never exited. Look into this.

                # Power factor will not be doing anything useful; set the update bit periodically, as the monitor
                # needs to know if this process is running. Since this daemon will forever be doing lots of nothing
                # when in this configuration, the sleep can be quite the long slumber.
                loco_loop_timer = 0
                while TO_INFINITY_AND_BEYOND:
                    time.sleep(SET_UPDATE_BIT_FREQUENCY)
                    self.update()

        else:
            # Something very bad happened. Bail.
            exit(99)

        # ---------------------------------------------------------------------
        # Before doing any actual useful work, wait until the initial "wait period start" expires.
        # This allows the inverters and nxo control to settle down.
        self.allow_wait_period_before_start_to_expire()

        # Control variable, for regular monitor update.
        loco_loop_timer = 0

        # Run forever until eternity is reached.
        while TO_INFINITY_AND_BEYOND:

            # Keep it as simple as possible:
            #
            # - check that the update is update at a regular interval
            # - check that nothing is in the way for power factor control:
            #       - upgrade?
            #       - feed discoveryh?
            #       - gateway is enabled?
            # - gather data for GUI window quadrant update
            # - get parameter from power_factor_control table (allows on-the-fly parameter changes)
            # - take care of data logging if enabled
            # - if power factor control timer expired: send dynamic Pf if required to the field of inverters

            time.sleep(1)
            self.current_utc = int(time.time())

            # Always do this, even if suspended. The monitor needs to know if this process is running or not.
            loco_loop_timer += 1
            if loco_loop_timer >= SET_UPDATE_BIT_FREQUENCY:
                self.update()
                loco_loop_timer = 0

            # -------------------------------------------------------------------------------------
            #
            # The actual power factor control portion.

            if gu.is_inverter_upgrade_in_progress():
                # TODO: consider having the upgrade (or monitor) daemon suspend power factor control.
                # Upgrade in progress. Any number of inverters could be in upgrade mode. When they reset
                # and return to application mode, they will wait 5 minutes before starting to make power.
                # When upgrades are complete, then upon the last pass through this section of the code,
                # the 6 minutes added to the last_inverter_update_utc variable will provide all inverters
                # with at least 5 minutes before they start making power, and an extra minute to reach
                # their max power making capability.
                self.last_inverter_update_utc = self.current_utc + PFC_RESUMPTION_AFTER_INVERTER_UPGRADE

            elif gu.is_feed_discovery_in_progress():
                # TODO: consider having feed discovery (or monitor) daemon suspend power factor control.
                # Feed discovery in progress. Inverters are running standard application, but are in one
                # of several feed detect states, and hence, not producing power. No point commanding them
                # with a power factor. When feed detect is complete, power factor control will resume. So
                # add 1 minute to the last_inverter_update_utc variable, this will give the inverters time
                # to settle down and ramp to maximum power making capability.
                self.last_inverter_update_utc = self.current_utc + PFC_RESUMPTION_AFTER_FEED_DISCOVERY

            elif not gu.is_gateway_operation_enabled():
                # TODO: consider having the monitor daemon suspend power factor control.
                # Gateway control is not enabled. When enabled, power factor control will resume. So add
                # 6 minutes added to the last_inverter_update_utc variable will provide all inverters
                # with at least 5 minutes before they start making power, and an extra minute to reach
                # their max power making capability.
                self.last_inverter_update_utc = self.current_utc + PFC_RESUMPTION_AFTER_GATEWAY_ENABLED

            # elif nxo_is_interrupted or curtailment is at 0%:
            #
            # TODO: consider having the NXO meter and NXO inverter daemons suspend power factor control.
            # ... or any other reason why power factor control should be interrupted. This daemon can't check
            #     everything in order to self-suspend power factor control, since much of the data in NXO control
            #     (nxo_inverter_daemon.py and meter_daemon.py) is based on run-time local variables not
            #     maintained in the database. Those daemons however, could set the suspended field in the
            #     power-factor_control table as necessary.

            else:

                # -------------------------------------------------------------------------------------
                # This method: nothing to do with power factor control. Its sole purpose is to support
                #              a GUI display, "PCC reactive power indicator, quadrant I, II, III or IV".
                self.gui_pf_window_logging()
                # -------------------------------------------------------------------------------------

                # Get the running parameters. This is done each time through the loop. The main
                # parameter of interest is the "pf_enable_x" parameter (enables/disables PF
                # on a per-feed basis). It also allows for the enabling/disabling of logging
                # without having to restart this daemon.
                self.get_power_factor_control_parameters()

                # -------------------------------------------------------------------------------------------
                #
                # Logging: take care of logging if enabled. Logging is outside of actual power factor control.
                #          Its use is primarilly for debug/study purpose.
                if self.is_pf_data_logging_enabled_any_feed() and self.is_pf_logging_timer_expired():
                    self.log_power_factor_control_data()
                # -------------------------------------------------------------------------------------------

                # Ready to apply power factor control?
                if self.is_pf_command_frequency_time_expired():
                    self.command_inverters_with_dynamic_pf()
                    # after returning, self.pf_command_frequency may have been shortened due to rejected meter readings!

                else:
                    # Power Factor Control always runs. Control is enabled on a per-feed basis.
                    pass
            # -------------------------------------------------------------------------------------
        # ... while


if __name__ == '__main__':
    pfcd = PowerFactorControlDaemon()
    pfcd.run()
    exit(0)
