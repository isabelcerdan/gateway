#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
'''
@summary: String Monitor Daemon for inverter string position and identification

          StringMon multicasts a request for neighbor information from all the 
          inverters that have identified themselves as a 'head-of-string'.  
          Responses from the 'head-of-string' inverters are used to update the
          inverters table with stringId and stringPosition information.
  
@copyright: Apparent Inc. 2017

@author: steve
'''
import time
from datetime import datetime, timedelta
import traceback
import os
import sys
import socket
import fcntl
import Queue
from Queue import Empty
from lib.logging_setup import *
from threading import Thread
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
from contextlib import closing
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.StringMonConstants import *
from lib.gateway_constants.GatewayConstants import *
from gateway_utils import *
import setproctitle
import re
from alarm.alarm import Alarm
from alarm.alarm_constants import *
from gateway_utils import *
from lib.db import prepared_act_on_database


logger = setup_logger('StringAudit')

import ctypes
class ifreq(ctypes.Structure):
    _fields_ = [("ifr_ifrn", ctypes.c_char * 16),
                ("ifr_flags", ctypes.c_short)]

# Globals
queue = Queue.Queue(maxsize=0)

STRINGMON_RECEIVE_SOCKET_TIMEOUT = 5.0
STRINGMON_WAIT_FOR_ENTRY_SECONDS = 30.0
'''
@attention: queue debounce dictates how long the script will wait for the next response
            to be placed on the queue.  Touch this very carefully.
'''
STRINGMON_EMPTY_QUEUE_DEBOUNCE = 5.0
response_cnt = 0


def instantiate_stringmonscript():
    setproctitle.setproctitle('%s-stringaudit*' % GATEWAY_PROCESS_PREFIX)
    try:
        if StringMonScript():
            logger.info('Processed %d responses.' % response_cnt)
            exit(0)
        else:
            logger.error('Problems encountered.')
            Alarm.occurrence(ALARM_ID_UNSUCCESSFUL_STRING_AUDIT)
            exit(1)
    except Exception as error:
        critical_string = 'Unhandled exception - exiting: %s' % repr(error)
        logger.critical(critical_string)
        Alarm.occurrence(ALARM_ID_STRINGMON_UNHANDLED_EXCEPTION)
    os._exit(1)

    
class HOSMulticastThread(Thread):
    '''
    This thread sends a multicast to the heat-of-string inverters
    asking them for information on their neighbors. 
    '''
    def __init__(self):
        '''
        Constructor
        '''
        Thread.__init__(self)
        
        self.SG424_data = []
        self.SG424_string = ''
        self.hos_send_socket = None


    def setup_stringmon_send_socket(self):
        try:
            self.hos_send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ttl = struct.pack('b', 1)
            self.hos_send_socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        except socket.error as err:
            err_string = "Send socket programming failed %s" % repr(err)
            logger.error(err_string)
            return False
        else:
            return True
            
    def close_send_socket(self):
        error_encountered = False
        if self.hos_send_socket is None:
            pass
        else:
            try:
                self.hos_send_socket.close()
            except Exception as error:
                error_string = 'Error closing send socket - %s' % repr(error)
                logger.error(error_string)
                error_encountered = True
    
        return error_encountered

    def wait_to_run_audit(self):
        mysql_string = 'select %s from %s;' % (INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, 
                                               DB_INVERTER_UPGRADE_TABLE)
        args = []
        cmd = prepared_act_on_database(FETCH_ONE, mysql_string, args)
        if cmd['upgrade_command'] == INVERTER_UPGRADE_COMMAND_NO_COMMAND:
            return False
        else:
            return True
                                                                      
    def run(self):
        DUMMY_SEQUENCE_NUMBER = 12345678
        
        if not self.setup_stringmon_send_socket():
            critical_string = "Unable to setup send socket for HOS audit thread - exiting."
            logger.critical(critical_string)
            return    # Thread will enter the join() in the main thread.

        while True:
            if self.wait_to_run_audit():
                time.sleep(30)
                continue
            
            user_tlvs = 1
            dummy_data = 0x12
            dummy_data = int(dummy_data)
            logger.info("Sending HOS discovery message...")
            self.SG424_data = add_master_tlv(user_tlvs)
            self.SG424_data = append_user_tlv_16bit_unsigned_short(self.SG424_data, GTW_TO_SG424_HOS_QUERY_U16_TLV_TYPE, dummy_data)
            multicast_all = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            try:
                self.hos_send_socket.sendto(self.SG424_data,
                                            (multicast_all,
                                             SG424_PRIVATE_UDP_PORT))
            except socket.error:
                err_string = "socket error detected sending HOS discovery request"
                logger.error(err_string)
                return False
            break
        self.close_send_socket()
        logger.info('Multicast thread completed...')
        return True
    

class HOSReceiveThread(Thread):
    '''
    This thread receives the HOS response messages from the 'head-of-string' 
    inverters and places them on a queue for the processing thread.
    '''
    def __init__(self):
        '''
        Constructor
        '''        
        Thread.__init__(self)
        
        self.terminal_debug_enabled = False
        self.LAN_address = None
        self.hos_recv_socket = None
        
    def setup_stringmon_recv_socket(self):
        # Initialize the socket. SOCK_DGRAM specifies a UDP datagram.
        self.LAN_address = get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        if self.LAN_address is None:
            logger.error('Unable to get LAN IP address to bind to.')
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            return False

        try:
            self.hos_recv_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            self.hos_recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.hos_recv_socket.settimeout(STRINGMON_RECEIVE_SOCKET_TIMEOUT)
        except Exception as error:
            err_string = "init socket failed - %s" % error
            logger.critical(err_string)
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            return False
        else:
            # Bind socket to local host and port
            try:
                logger.info("Binding to %s, %d" % (self.LAN_address,
                                                STRINGMON_HOS_DISCOVERY_PORT))
                self.hos_recv_socket.bind((self.LAN_address, STRINGMON_HOS_DISCOVERY_PORT))
            except socket.error, msg:
                err_string = "Bind failed. Error Code : " + str(msg[0]) + " Message " + msg[1]
                logger.critical(err_string)
                self.close_recv_socket()
                self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
                return False                
        return True

    def close_recv_socket(self):
        error_encountered = False
        if self.hos_recv_socket is None:
            pass
        else:
            try:
                self.hos_recv_socket.close()
            except Exception as error:
                error_string = 'Error closing receive socket - %s' % repr(error)
                logger.error(error_string)
                error_encountered = True
    
        return error_encountered

    def set_audit_status(self, status=None):
        update_string = 'update %s set %s = %s' % (DB_STRINGMON_CONTROL_TABLE, 
                                                   STRINGMON_CTL_TBL_AUDIT_STATUS_COLUMN_NAME,
                                                   '%s')    
        args = [status,]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True
    
    def zero_string_count(self):
        update_string = 'UPDATE %s SET %s = %s;' % (DB_STRINGMON_CONTROL_TABLE,
                                                   STRINGMON_CTL_TBL_STRINGS_DETECTED_COLUMN_NAME,
                                                   "%s")
        args = [0,]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True
    
    def run(self):        
        if not self.setup_stringmon_recv_socket():
            logger.error("Unable to set up receive socket for HOS audit thread.")
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            self.zero_string_count()
            return False

        rv = True
        '''
        Start out with three measures of aliveness.  Each measure corresponds
        to one socket timeout or one trip through the message reception loop.
        '''
        aliveness = 3
        while aliveness > 0:
            try:
                my_data, my_addr = self.hos_recv_socket.recvfrom(4096)
                data_packet = [my_data, my_addr]
            except socket.timeout:
                '''
                No messages received during socket timeout period.
                Lose one measure of aliveness.
                '''
                aliveness -= 1
                continue
            except Exception as error:
                logger.error('Socket receive error - %s' % error)
                rv = False
                break
            try:
                queue.put(data_packet)
            except Exception as err:
                logger.error('Error putting message data on queue - %s' & err)  
                rv = False
                break
            else:
                '''
                Successful queue put - gain one measure of aliveness.
                '''
                aliveness += 1
            '''
            Each trip through the loop costs one measure of aliveness.
            '''
            aliveness -= 1

                
        self.close_recv_socket()
        return rv

                              
class HOSProcessThread(Thread):
    '''
    This thread processes the HOS response messages from the 'head-of-string' 
    inverters.  StringMon expects 1 to 8 TLVs containing the MAC addresses
    of the 'head-of-string' and its neighbors (if any.)
    '''

    def __init__(self):
        '''
        Constructor
        '''        
        Thread.__init__(self)
        
        # For setting up and reading from the database:
        self.terminal_debug_enabled = False
        self.LAN_address = None
        self.port_packet = None
        self.SG424_data = []
        self.SG424_ip_address = None
        self.SG424_ip_address_port = None
        self.next_tlv_rank = 0
        self.number_of_user_tlvs = 0
        self.hos_mac_addr = ""
        self.hos_position = 0
        self.sid = ""

    def process_hos_response(self):
        self.SG424_data = self.port_packet[0]
        try:
            # The Master TLV is mandatory, must be at the beginning of the TLV set so validate it.
            (self.next_tlv_rank, self.number_of_user_tlvs) = evaluate_master_tlv(self.SG424_data)
        except Exception:
            logger.error('Unhandled exception in evaluate_master_tlv()')
            return False
        
        if self.next_tlv_rank == 0:
            # Master TLV is missing or mal-formed.
            logger.error("invalid master TLV detected, discarding message.")
            return False
        
        # Assumption is that the content of the packet is the set of mac addresses
        # returned by a presumed HOS inverter. Extract that set.
        if not self.extract_mac_addresses():
            logger.error("failed to extract MAC addresses")
            return False

        return True
    
    '''    
    Extract the MAC address from the user TLV, format it for processing, and
    send it on to the inverter table update function. 
    '''
    def extract_mac_addresses(self):
        for hos_resp in range(1, self.number_of_user_tlvs + 1):

            (hos_tlv_type, self.next_tlv_rank) = get_tlv_type(self.SG424_data, self.next_tlv_rank)
            if hos_tlv_type == SG424_TO_GTW_HOS_RESPONSE_6HEX_BYTES_TLV_TYPE:
                # This is the expected TLV TYPE. Validate the length.
                (hos_tlv_length, self.next_tlv_rank) = get_tlv_header_length(self.SG424_data, self.next_tlv_rank)
                if hos_tlv_length == 6:
                    # Now extract the next 6 bytes of the MAC:
                    try:
                        hex_pair1, hex_pair2, hex_pair3, self.next_tlv_rank = get_funky_packed_mac(self.SG424_data, self.next_tlv_rank)
                    except Exception as err:
                        logger.error(repr(err))
                        return False
                    try:
                        addr = '%04x%04x%04x' % (hex_pair1, hex_pair2, hex_pair3)
                    except Exception as err:
                        logger.error(repr(err))
                        return False

                    self.hos_position = hos_resp
                    if addr == '000000000000':
                        return self.truncate_string()

                    self.hos_mac_addr = ':'.join([addr[i:i+2] for i in range(0, len(addr), 2)])

                    if not self.update_inverter_table():
                        err_string = "failed to update database for HOS %s position %d" % \
                                     (self.SG424_ip_address, self.hos_position)
                        logger.error(err_string)
                else:
                    err_string = "from %s: unexpected MAC data length = %d" % \
                                 (self.SG424_ip_address, hos_tlv_length)
                    logger.error(err_string)

            else:
                err_string = "invalid tlv type = %x" % hos_tlv_type
                logger.error(err_string)

        return  True

    def truncate_string(self):        
        if self.hos_position < self.number_of_user_tlvs:
            '''
            We need to truncate the string. Remove all the members (set stringID and
            stringPosition = NULL) starting with the current position and going to 
            the end of the string.
            '''
            update_string = "update %s set %s = %s, %s = 0 where stringId = %s and " \
                            "stringPosition >= %s" % \
                            (DB_INVERTER_NXO_TABLE, 
                             INVERTERS_TABLE_STRING_ID_COLUMN_NAME, '%s', 
                             INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                             "%s", "%s")
            data_string = ('00:00:00', self.sid, self.hos_position)
            prepared_act_on_database(EXECUTE, update_string, data_string)
        return True

    def macToStringId(self, mac_addr):
        string_id = mac_addr[-8:]
        return string_id
    
    def update_inverter_table(self):        
        '''
        Get the stringId and stringPosition fields for this inverter.  If they
        are 0, this is the first response from the unit.
        '''
        mysql_query = "SELECT * from %s WHERE %s = %s;" % \
                      (DB_INVERTER_NXO_TABLE, INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, "%s")
        query_data = [self.hos_mac_addr,]
        row_contents = prepared_act_on_database(FETCH_ALL, mysql_query, query_data)
        if row_contents is None or len(row_contents) == 0:
            if self.SG424_ip_address is None:
                self.SG424_ip_address = '0.0.0.0'
            if self.hos_position == 1:
                # New inverter is a HOS!
                self.sid = self.macToStringId(self.hos_mac_addr)
                mysql_update = "insert into %s (%s, %s, %s, %s) values (%s, %s, %s, %s) " % \
                            (DB_INVERTER_NXO_TABLE,
                             INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                             INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                             INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                             INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                             "%s", "%s", "%s", "%s")
                update_data = (self.sid,
                               str(self.hos_position),
                               self.hos_mac_addr,
                               self.SG424_ip_address)
                prepared_act_on_database(EXECUTE, mysql_update, update_data)
                return True
            else:
                # New inverter is not a HOS.
                mysql_update = "insert into %s (%s, %s, %s) values (%s, %s, %s) " % \
                            (DB_INVERTER_NXO_TABLE,
                             INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                             INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                             INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
                             "%s", "%s", "%s")
                update_data = (self.sid, int(self.hos_position), self.hos_mac_addr)
                prepared_act_on_database(EXECUTE, mysql_update, update_data)
                return True
        else:
            '''
            MAC for this inverter is already in the database. Note that it
            doesn't mean there is any string position information present.
            Check for existing string that has been truncated.
            '''
            if self.hos_position == 1:
                if not self.check_head_of_string(row_contents):
                    err_string = "Failed to process audit for inverter %s at head of " \
                                 "string" % self.hos_mac_addr
                    logger.error(err_string)
                    return False
            else:
                self.check_string_member(row_contents)
                
        return True

    def stringId_is_valid(self, db_stringId=None):
        if db_stringId is None:
            return False
        if not isinstance(db_stringId, str) or not isinstance(db_stringId, unicode):
            return False
        
        octets = re.split('[\:\-]', db_stringId)
        if len(octets) != 3:
            return False
        for i in octets:
            try:
                if int(i, 16) > 255:
                    return False
            except:
                return False
        return True
 
    def check_head_of_string(self, row_contents):
        update_needed = False
        
        # The inverter claims to be a HOS.  See if the database corroborates his story.
        for row in row_contents:
            db_stringPosition = row['stringPosition']
            db_stringId = row['stringId']
            db_mac_addr = row['mac_address']
        
        if db_stringPosition is None:
            # No string position for this inverter in the database.  We'll treat it as a new string.
            self.sid = self.macToStringId(db_mac_addr)
            logger.info("Existing inverter %s now a HOS for stringId = %s" % \
                                   (self.hos_mac_addr, self.sid))
            update_needed = True
        elif db_stringPosition == self.hos_position:
            # Looks like we have a real HOS here.  All copacetic.  Maybe.
            if not self.stringId_is_valid(db_stringId):
                self.sid = self.macToStringId(db_mac_addr)
                update_needed = True
            else:
                self.sid = db_stringId
        else:
            # Looks like the existing inverter has been promoted to HOS.  Get him a 
            # new stringId.   
            self.sid = self.macToStringId(db_mac_addr)
            # Note: stringId is a ... string, not an integer.
            logger.info("Existing inverter %s moved to be HOS for stringId = %s" % \
                                   (self.hos_mac_addr, self.sid))
            update_needed = True
            
            # We need to remove the old HOS from the string.
            self.remove_position_from_string(db_mac_addr, self.hos_position)
                        
        if update_needed:
            self.update_existing_inverter()
        return True
    
    def check_string_member(self, row_contents):        
        update_needed = False
        for row in row_contents:
            db_stringPosition = row['stringPosition']
            db_stringId = row['stringId']
            db_mac_addr = row['mac_address']
            
        # Compare the string position in the message to the string position in the database.
        if db_stringPosition is None:
            # No existing string Position for this inverter. Skip the position change logic.
            logger.info("No position data for existing inverter %s" % self.hos_mac_addr)
            update_needed = True
        elif self.hos_position != db_stringPosition:
            '''
            When an existing inverter changes position on a string, or moves to a new string 
            it constitutes an info event.  Note: db_stringId is a string, not an integer.
            '''
            info_string = "check_string_member: %s in stringId %s stringPosition changed %d -> %d" % \
                    (self.hos_mac_addr, db_stringId, db_stringPosition, self.hos_position)
            logger.info(info_string)
            update_needed = True

            # We also need to remove the 'old' position from the string.
            self.remove_position_from_string(db_mac_addr, self.hos_position)
        elif db_stringId is None:
            # We have found an inverter in the database that doesn't belong to
            # any string.  Treat it like a new inverter.
            logger.info("No stringId data for existing inverter %s" % self.hos_mac_addr)
            update_needed = True            
        elif self.sid != db_stringId:
            # The head-of-string's ID changed, so the members must change, also.
            logger.info("stringId changed from %s to %s for %s..." % \
                                   (db_stringId, self.sid, self.hos_mac_addr))          
            update_needed = True

        if update_needed is True:    
            return self.update_existing_inverter()
        else:
            return True 
                    
    def remove_position_from_string(self, mac_addr, old_string_position):        
        logger.info("removing %s from string position %d..." % \
                               (mac_addr, old_string_position))
        remove_string = "update %s set %s = %s, %s = 0 where %s = %s;" % \
                        (DB_INVERTER_NXO_TABLE,
                         INVERTERS_TABLE_STRING_ID_COLUMN_NAME, '%s',
                         INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                         INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, '%s')
        remove_data = ['00:00:00', mac_addr]
        prepared_act_on_database(EXECUTE, remove_string, remove_data)
        return True
                             
    def update_existing_inverter(self):
        mysql_update = "update %s set %s = %s, %s = %s where %s = %s; " % \
            (DB_INVERTER_NXO_TABLE,
             INVERTERS_TABLE_STRING_ID_COLUMN_NAME, '%s',
             INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME, '%s',
             INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME, '%s')
        update_data = (self.sid, self.hos_position, self.hos_mac_addr)
        prepared_act_on_database(EXECUTE, mysql_update, update_data)
        return True

    def set_audit_status(self, status=None):
        update_string = 'update %s set %s = %s' % (DB_STRINGMON_CONTROL_TABLE, 
                                                   STRINGMON_CTL_TBL_AUDIT_STATUS_COLUMN_NAME,
                                                   '%s')    
        args = [status,]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True
            
    def run(self):
        global response_cnt
                
        queue_err_cnt = 0
        process_err_cnt = 0
        while True:
            try:
                self.port_packet = queue.get(timeout=STRINGMON_WAIT_FOR_ENTRY_SECONDS)
            except Empty:
                '''
                Timeout on waiting for an entry to be placed on the queue.  
                TODO: ADD 'ALIVENESS' LOGIC HERE SO THE THREAD DOESN'T SHUT DOWN PREMATURELY.
                '''
                logger.info('Timeout waiting for a HOS response')
                self.set_audit_status(STRINGMON_AUDIT_COMPLETE_STATUS)
                return True
            except Exception as error:
                error_string = 'queue get failed - %s' % repr(error)
                logger.error(error_string)
                queue_err_cnt += 1
                if queue_err_cnt > 5:
                    logger.critical('Unable to retrieve events from queue.')
                    self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
                    return False
                continue
            try:
                queue_err_cnt = 0
                if not self.process_hos_response():
                    logger.error('Failed to process HOS response.')
                    process_err_cnt += 1
                    if process_err_cnt > 10:
                        logger.critical('Too many failures processing HOS responses.')
                        self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
                        return False
                else:
                    response_cnt += 1
                queue.task_done()
                if queue.empty():
                    # Debounce - pause for a moment.
                    time.sleep(STRINGMON_EMPTY_QUEUE_DEBOUNCE)
                    if queue.empty():
                        '''
                        If we haven't received another response for 5000ms, we probably
                        won't get any more.  Consider our work here done.
                        '''
                        self.set_audit_status(STRINGMON_AUDIT_COMPLETE_STATUS)
                        return True
                    else:
                        continue
            except Exception as err:
                error_string = 'Unable to process HOS responses: %s' % repr( err)
                logger.error(error_string)
                self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
                return False
                    
                                        
class StringMonScript(object):
    '''
    StringMon multicasts a request for neighbor information from all the inverters 
    that have identified themselves as a 'head-of-string'.  Responses from the
    'head-of-string' inverters are used to update the inverters table with stringId
    and stringPosition information.
    '''
    def __init__(self, _=None):
        self.start_time = datetime.datetime.now()
        self.terminal_debug_enabled = True # False

        # Threads
        self.HOSReceiveThread = None
        self.HOSProcessThread = None
        self.HOSMulticastThread = None

        self.SG424_data = []
        self.SG424_string = ''
                
        self.run_script()

    def set_pid(self):        
        mysql_string = "update %s set %s = %s;" % (DB_STRINGMON_CONTROL_TABLE,
                                                   STRINGMON_CTL_TBL_PID_COLUMN_NAME,
                                                   "%s")
        args = [str(os.getpid()),]
        prepared_act_on_database(EXECUTE, mysql_string, args)
        return True
                                              
    def set_audit_status(self, status=None):
        update_string = 'update %s set %s = %s' % (DB_STRINGMON_CONTROL_TABLE, 
                                                   STRINGMON_CTL_TBL_AUDIT_STATUS_COLUMN_NAME,
                                                   '%s')    
        args = [status,]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True
    
    def wait_to_run_audit(self):
        mysql_string = 'select %s from %s;' % (INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, 
                                               DB_INVERTER_UPGRADE_TABLE)
        args = []
        cmd = prepared_act_on_database(FETCH_ONE, mysql_string, args)
        if cmd['upgrade_command'] == INVERTER_UPGRADE_COMMAND_NO_COMMAND:
            return False
        else:
            return True
                                           
    def update_string_count(self, count=None):
        if count is None:
            query_string = 'SELECT COUNT(*) FROM %s WHERE %s = 1;' % \
                           (DB_INVERTER_NXO_TABLE,
                            INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
            args = []
            results = prepared_act_on_database(FETCH_ALL, query_string, args)
        else:
            results = [{'COUNT(*)' : count},]
        
        for result in results:
            update_string = 'UPDATE %s SET %s = %s;' % (DB_STRINGMON_CONTROL_TABLE,
                                                       STRINGMON_CTL_TBL_STRINGS_DETECTED_COLUMN_NAME,
                                                       '%s')
            args = [result['COUNT(*)']]
            prepared_act_on_database(EXECUTE, update_string, args)
        return True
            
    def run_script(self):    
        global response_cnt
        
        # Get the pid and store it in the control table.
        if not self.set_pid():
            logger.critical("Unable to set PID in database.")
            return False
        
        # Can we actually run the audit right now?
        if self.wait_to_run_audit():
            logger.warning('Inverter upgrade in progress - unable to run string ID audit.')
            self.set_audit_status(STRINGMON_AUDIT_DELAYED_STATUS)
            self.update_string_count(0)
            return False

        # Set audit status field to 'audit_started'.
        self.set_audit_status(STRINGMON_AUDIT_STARTED_STATUS)
        
        # Start the audit and response processing threads.
        self.HOSMulticastThread = HOSMulticastThread()
        self.HOSReceiveThread   = HOSReceiveThread()
        self.HOSProcessThread   = HOSProcessThread()
        
        try:
            logger.info("Starting HOS Receive thread...")
            self.HOSReceiveThread.daemon = True
            self.HOSReceiveThread.start()
        except:
            logger.critical("Unable to start receive thread.")
            self.update_string_count(0)
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            return False
        try:
            logger.info("Starting HOS response processing thread...")
            self.HOSProcessThread.daemon = True
            self.HOSProcessThread.start()
        except:
            logger.critical("Unable to start response processing thread.")
            self.update_string_count(0)
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            return False
        try:
            time.sleep(0.250)  # hang out for 250ms to let the other threads spool up.
            logger.info("Starting HOS multicast thread...")
            self.HOSMulticastThread.daemon = True
            self.HOSMulticastThread.start()
        except Exception:
            logger.critical("Unable to start multicast thread.")
            self.update_string_count(0)
            self.set_audit_status(STRINGMON_AUDIT_FAILED_STATUS)
            return False

        self.set_audit_status(STRINGMON_AUDIT_IN_PROGRESS_STATUS)
        recv_thread_alive = True
        process_thread_alive = True
        self.HOSMulticastThread.join(2.0)
        while True:
            if recv_thread_alive:
                self.HOSReceiveThread.join(10.0)
                if not self.HOSReceiveThread.is_alive():
                    logger.info('HOS Receive thread completed.')
                    recv_thread_alive = False
            if process_thread_alive:
                self.HOSProcessThread.join(10.0)
                if not self.HOSProcessThread.is_alive():
                    logger.info('HOS Process thread completed.')
                    process_thread_alive = False
            if not(process_thread_alive or recv_thread_alive):
                break
           
        self.update_string_count(response_cnt)
        
        return True


def main():
    instantiate_stringmonscript()
    return
    
if __name__ == '__main__':
    main()
    exit(0)

