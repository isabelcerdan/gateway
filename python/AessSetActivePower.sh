#!/bin/sh
# Set the active power for the Pimentel ESS-One.
#
# usage: AessSetActivePower.sh <active_power>  [+discharge / -charge]
#
# copyright Apparent Inc. 2019
#

active_power=$1

# System must be running for the command to have any effect.
/usr/share/apparent/python/DbCompare.py --table ess_master_info \
                                        --column unit_work_state \
                                        --where-column ess_id \
                                        --where-value 1 \
                                        --compare-value Running

if [ $? -ne 0 ]
then
  echo "System not running - command rejected."
  exit
fi

# The system must be in manual mode for the command to have any effect.
/usr/share/apparent/python/DbCompare.py --table ess_units \
                                        --column mode \
                                        --where-column id \
                                        --where-value 1 \
                                        --compare-value manual

if [ $? -eq 0 ]
then
  sudo /usr/share/apparent/.py2-virtualenv/bin/python /usr/share/apparent/python/EssCLI.py --active-power ${active_power}
  echo "Active power set to ${active_power}"
else
  echo "System not in manual mode - command rejected."
  exit
fi

