#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
    scripts/export_readings.py

    This stand-alone script zips CSV data to a file and sends to AWS

    $ python export_readings.py

    --cron      Not Used
    --test      Skips sending data to AWS

    Steps:
    1) Change to the readings directory
    2) Make a temporary directory with today's date in the sync directory
    2a) Delete the temporary directory if it happens to exist
    3) Move all the *.csv files to the temporary directory
    4) Zip all files in temporary directory into zip file with named with today's date (ex: 2019-03-13.zip)
    4a) if a same named zip file exists, extend the file name (ex 2019-030-13_00.zip or 2019_03-13_01.zip ...)
    5) Get a count of files in zip and in the temporary directory
    6) if the counts are equal,
    6a) delete the temporary directory (and files)
    6b) Build a gateway ID
    6c) Send *.zip file to AWS
    6c1) if send to AWS OK
    6c1a) Delete older *zip.sent files from sync directory, only keeping last 31 (a month if all is well)

"""
import subprocess
import boto3
from lib.db import prepared_act_on_database, FETCH_ALL, FETCH_ONE, EXECUTE
from optparse import OptionParser
import datetime
import sys
from lib.gateway_constants.vault import *
import shutil, os
from datetime import datetime, timedelta
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_EXPORT_READINGS_ERROR

parser = OptionParser()
parser.add_option("-c", "--cron",
                  dest="cron", default=False,
                  help="Run as cron job to export and archive last full days worth of enabled readings.\"")
parser.add_option("-T", "--test",
                  dest="test", default=False,
                  help="Does all the work execpt sending files to AWS.")
(options, args) = parser.parse_args()



readings_sync_data_path = "/var/lib/apparent/readings_data"
readings_raw_data_path = "/var/lib/apparent/readings_raw"

fileTypeToCompress = '*.csv'
syncDirPath = readings_sync_data_path
srcFilePath = os.path.join(readings_raw_data_path, fileTypeToCompress)

def raise_and_clear_alarm(this_alarm):
    Alarm.occurrence(this_alarm)
    Alarm.request_clear(this_alarm)

def send_to_aws(gateway_id):
    '''
    Sends all *.zip files in the reading directory to AWS
    Limits the number of files in AWS to 30

    :param gateway_id:
    :return: success or failure boolean
    '''
    retVal = False

    bucket = "gateway-readings"

    cmd = "ls -lt"
    print("\nFiles to sync")
    print("executing cmd = " + str(cmd))
    ls = subprocess.check_output(cmd, shell=True)
    print(ls)

    cmd = "/usr/bin/aws s3 sync --sse %s s3://%s/%s/" % (syncDirPath, bucket, gateway_id)
    print("executing cmd = " + str(cmd))

    if options.test == False:
        try:
            subprocess.call(["/usr/bin/aws", "s3", "sync", "--sse", syncDirPath, ("s3://%s/%s/" % (bucket, gateway_id))])

            client = boto3.client('s3')

            # list objects at s3://gateway-readings/08-00-27-BD-12-0A/
            result = client.list_objects(Bucket=bucket, Prefix=gateway_id + '/', Delimiter='/')

            # grab the relevant backup contents from the returned JSON object
            backups = result.get('Contents')
            sorted_backups = sorted(backups, key=lambda k: k['Key'])

            # lets keep 30 days worth of backups on aws s3
            keep = 30
            num_remove = len(sorted_backups) - keep
            if num_remove > 0:
                # create dictionary object with just keys of objects to be removed
                old_backups = {'Objects': [{'Key': o.get('Key')} for o in sorted_backups[:num_remove]]}

                # delete old backups
                client.delete_objects(Bucket=bucket, Delete=old_backups)

            retVal = True

        except Exception as error:
            raise_and_clear_alarm(ALARM_ID_EXPORT_READINGS_ERROR)
            print("Error sending data to AWS")

    else:
        print("Skipping push to AWS")
        retVal = True

    return retVal

def getGatewayId():
    '''
    Build the Id to send to AWS
    :return:
    '''
    from uuid import getnode as get_mac
    mac = '-'.join('%02X' % ((get_mac() >> 8 * i) & 0xff) for i in reversed(xrange(6)))

    sql = "SELECT gateway_base_sn FROM energy_report_config"
    results = prepared_act_on_database(FETCH_ONE, sql, [])
    if results and results['gateway_base_sn'] is not None and len(results['gateway_base_sn']) > 0:
        gateway_id = '-'.join([results['gateway_base_sn'], mac[-8:]])
    else:
        gateway_id = mac

    return gateway_id

def deleteOldLocalFiles():
    '''
    delete old zip files that were sent to AWS
    :return:
    '''
    # Change to readings directory
    os.chdir(syncDirPath)
    print("changed to directory =" + str(os.getcwd()))

    cmd = ("ls -t *.zip | tail -n +31")
    print("executing cmd = " + str(cmd))
    deleteList = subprocess.check_output(cmd, shell=True)
    print("Files to Delete:")
    print(deleteList)
    cmd = ("ls -t *.zip | tail -n +31 | xargs -I {} rm -rf -- {}")
    print("executing cmd = " + str(cmd))
    subprocess.check_output(cmd, shell=True)


def buildZipFile():
    retVal = False
    filesZipped = 0

    try:
        # Make temporary directory based upon yesterdays date
        d = datetime.today() - timedelta(days=1)
        date = d.strftime("%Y-%m-%d")
        print("date=" + str(date))
        target_dir = os.path.join(syncDirPath, date)
        if os.path.exists(target_dir):
            cmd = "rm -rf %s  " % target_dir
            print("executing cmd = " + str(cmd))
            os.system(cmd)

        print("creating directory =" + str(target_dir))
        os.makedirs(target_dir)

        # Move files to the temporary directory
        cmd = "mv %s %s" % (srcFilePath, date)
        print("executing cmd = " + str(cmd))
        os.system(cmd)

        # Zip all the files in the temp directory
        targetFile = ("%s.zip" % date)
        loopCnt = 0
        while os.path.exists(targetFile) == True:
            targetFile = ("%s_%02u.zip" % (date, loopCnt))
            if loopCnt > 40:
                print("*** Too many zip files, stepping on file %s" % targetFile)
                break
            loopCnt = loopCnt + 1

        print("targetFile = " + str(targetFile))
        cmd = "zip -r %s %s" % (targetFile, date)
        print("executing cmd = " + str(cmd))
        os.system(cmd)

        # Get a count of files in zip
        cmd = "unzip -l %s  | grep csv | wc -l" % targetFile
        print("executing cmd = " + str(cmd))
        filesZipped = int(subprocess.check_output(cmd, shell=True))
        print("filesZipped = " + str(filesZipped))

        # Get a count of files that should have been zipped
        cmd = "ls -l %s  | grep csv | wc -l" % date
        print("executing cmd = " + str(cmd))
        filesCount = int(subprocess.check_output(cmd, shell=True))
        print("fileCount = " + str(filesCount))

        # remove files that were added to the zip file
        if filesCount == filesZipped:
            cmd = "rm -rf %s  " % date
            print("executing cmd = " + str(cmd))
            os.system(cmd)

            retVal = True

    except Exception as error:
        raise_and_clear_alarm(ALARM_ID_EXPORT_READINGS_ERROR)
        print("Error Building zip file")

    return filesZipped, retVal


startDir = os.getcwd()
print("cwd = " + str(startDir))

# Change to sync directory
os.chdir(syncDirPath)
print("changed to directory =" + str(os.getcwd()))

# Build the zip file
filesZipped, zipOK = buildZipFile()
print("filesZipped = %u,  zipOk = %s" % (filesZipped, str(zipOK)))

if filesZipped > 0 and zipOK:
    # Build Id string
    gateway_id = getGatewayId()
    print("gateway_id=" + str(gateway_id))

    # push backup to s3 (sse=server-side encryption)
    pushOk = send_to_aws(gateway_id)
    print("push to ASW OK = %s" % str(pushOk))

    if pushOk:
        # Get rid of file/directory older then 31 days
        # This assume that the script is run daily
        deleteOldLocalFiles()

# Go back to starting directory
os.chdir(startDir)
print("changed to directory =" + str(os.getcwd()))
