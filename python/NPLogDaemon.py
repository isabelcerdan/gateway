#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: NPLogDaemon.py

@description: Read the current database values for the discrete inputs of the
              PET7202 and write them to a circular database file.  

@copyright: Apparent Inc.  2017

@created: Aug 29, 2017

@author: steve
"""

from lib.logging_setup import *
from lib.gateway_constants.DBConstants import *
from daemon.daemon import Daemon
from alarm.alarm import ALARM_ID_NPLOG_DAEMON_RESTARTED
from readings_logger.readings_logger import ReadingsLogger
import collections


class NPLogDaemon(Daemon):
    LOG_CYCLE = 2592000
    polling_freq_sec = 0.500    # 500 ms
    alarm_id = ALARM_ID_NPLOG_DAEMON_RESTARTED
    NPLOG_MAX_DB_ERRORS = 5

    def __init__(self, **kwargs):
        super(NPLogDaemon, self).__init__(**kwargs)
        self.db_errors = 0
        self.log_cycle = self.LOG_CYCLE
        self.readings_logger = ReadingsLogger(basename="net_prot_log")

    def work(self):
        mysql_string = 'select %s, %s, %s, %s, %s, %s from %s' % \
                       (ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME,
                        ICPCON_DEVICES_TBL_DI0_VOLATILE_REGISTER_NAME,
                        ICPCON_DEVICES_TBL_DI1_VOLATILE_REGISTER_NAME,
                        ICPCON_DEVICES_TBL_DI2_VOLATILE_REGISTER_NAME,
                        ICPCON_DEVICES_TBL_DI3_VOLATILE_REGISTER_NAME,
                        ICPCON_DEVICES_TBL_TIMESTAMP_COLUMN_NAME,
                        DB_ICPCON_DEVICES_TABLE)
        args = []
        try:
            devices = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            err_string = 'DB select failed for %s - %s' % (DB_ICPCON_DEVICES_TABLE, repr(error))
            self.logger.exception(err_string)
            self.db_errors += 1
            if self.db_errors > self.NPLOG_MAX_DB_ERRORS:
                self.logger.critical('Maximum database errors exceeded.')
            return False

        if len(devices) == 0:
            self.logger.warn("No network protector devices found to monitor.")
            return

        for registers in devices:
            dict = collections.OrderedDict(registers)
            self.readings_logger.write_reading(dict)
