from multiprocessing import Process, Event
import setproctitle
import time

from lib.logging_setup import *
from lib.gateway_constants.DBConstants import GATEWAY_PROCESS_PREFIX

"""
    PeriodicTask
    
    Launches a given task at regular intervals. Allows for
    stopping the periodic task by calling stop. The interval 
    should be a floating point number specifying number of 
    seconds between launching the task.
    
    'on_interval' allows for the task to time align with the
    interval. For example, an interval value of 60 seconds
    with on_interval set to True would ensure the task is
    run at the beginning of each minute:
        1:00:00pm
        1:01:00pm
        ...
"""


class PeriodicTask(Process):
    def __init__(self, name, target, interval=0, on_interval=False, args=[], kwargs={}):
        super(PeriodicTask, self).__init__()
        self.name = name
        self.target = target
        self.interval = interval
        self.on_interval = on_interval
        self.args = args
        self.kwargs = kwargs
        self.daemon = True

        self.finished = Event()
        self.logger = setup_logger(name)

    def stop(self):
        """Stop the timer if it hasn't finished yet"""
        self.finished.set()
        self.join(timeout=3)

    def wait_time(self):
        if self.on_interval:
            return self.interval - (time.time() % self.interval)
        else:
            return self.interval

    def run(self):
        setproctitle.setproctitle("%s-%s" % (GATEWAY_PROCESS_PREFIX, self.name))
        try:
            while not self.finished.is_set():
                self.finished.wait(self.wait_time())
                if not self.finished.is_set():
                    self.target(*self.args, **self.kwargs)

        except Exception as e:
            self.logger.exception("Unhandled exception in %s" % self.name)
