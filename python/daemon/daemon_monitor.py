#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: DaemonMonitor

@description: DaemonMonitor class used for starting/stopping/monitoring daemons.

@copyright: Apparent Inc. 2018

@reference:

@author: patrice

@created: November 30, 2018
"""

import setproctitle
import os
import signal
from multiprocessing import Process, Event, active_children
import subprocess
from datetime import datetime
import time

from alarm.alarm import Alarm
from lib.db import EXECUTE, get_default
from lib.logging_setup import *
from lib.helpers import datetime_to_utc


class MissingControlTableRecord(Exception):
    pass


class DaemonMonitor(object):
    """
    The DaemonMonitor class is used to manage and monitor each daemon that is
    run in the gateway. The DaemonMonitor class makes use of the daemon_control
    table to manage the state of a given daemon. Each daemon is given a unique
    name which is used to update and retrieve the daemon_control record.

    The gateway_monitor.py and ess/ess_manager.py scripts make use of the
    method DaemonMonitor.find_or_create_all(<daemon_class>) to instantiate
    the appropriate DaemonMonitors for a given Daemon class.
    """
    def __init__(self, daemon_class, **kwargs):
        self.daemon_class = daemon_class
        self.name = kwargs.get('name', self.daemon_class.get_base_name())
        self.id = get_default(kwargs, 'source_id', self.name)
        self.type = self.daemon_class.get_base_name()
        self.logger = setup_logger(self.name)
        if self.daemon_class.check_finished:
            self.finished = Event()
        else:
            self.finished = None
        self.enabled = kwargs.get('enabled', 0) == 1
        self.started = kwargs.get('started', 0) == 1
        self.updated = kwargs.get('updated', 0)
        self.suspended = kwargs.get('suspended', 0) == 1
        self.no_restarts = kwargs.get('no_restarts', False)
        if self.daemon_class.polling_freq_sec > self.daemon_class.update_timeout:
            raise Exception("Restart timeout must be greater than daemon polling frequency: daemon=%s" % self.name)
        self.pid = None
        self.process = None
        self.daemon = None
        self.stop()

    def has_updated(self):
        return self.no_restarts or \
               self.updated >= (datetime_to_utc(datetime.utcnow()) - self.daemon_class.update_timeout)

    def update(self):
        sql = "UPDATE daemon_control SET updated = %s WHERE name=%s" % ("%s", "%s")
        args = [int(datetime_to_utc(datetime.utcnow())), self.name]
        prepared_act_on_database(EXECUTE, sql, args)

    def start(self):
        try:
            if self.finished:
                self.finished.clear()
            self.process = Process(target=self.target)
            self.process.daemon = self.daemon_class.daemon
            self.update()
            self.process.start()
            self.started = self.process.is_alive()
            self.pid = self.process.pid
        except Exception as ex:
            self.logger.exception("Unable to start process %s" % self.name)
            self.started = False
        finally:
            self.save(started=self.started, pid=self.pid)

    def stop(self):
        if self.process and self.process.is_alive():
            if self.finished:
                self.finished.set()
                self.process.join(3)
                if self.process.is_alive():
                    self.process.terminate()
            else:
                self.process.terminate()

            # calling active_children has side effect of "joining" processes which have finished
            active_children()
        elif self.pid_exists():
            self.logger.info("Killing old %s process (old-pid=%s)..." % (self.name, self.pid))
            try:
                os.kill(int(self.pid), signal.SIGKILL)
            except Exception as ex:
                self.logger.exception("Unable to kill pid=%s" % self.pid)
        else:
            subprocess.Popen(["pkill", self.name])

        self.started = False
        self.pid = None
        self.save(started=self.started, pid=self.pid)

    def monitor(self):
        """
        Reload the control table from the database and then check the following:
        1. If the daemon is not enabled and it is started, stop the daemon.
        2. If the daemon is enabled and not started, start the daemon.
        3. If the daemon is enabled and started but is not alive (pid not running), restart the daemon.
        4. If the daemon is enabled, started and alive, but has not updated in update_timeout seconds,
           stop and start the daemon.
        :return:
        """
        self.reload()
        if self.enabled:
            if self.started:
                if self.process and self.process.is_alive():
                    if not self.has_updated():
                        self.logger.warn('%s process has not updated... restarting.' % self.name)
                        if type(self.daemon_class.alarm_id) is dict:
                            Alarm.occurrence(self.daemon_class.alarm_id[self.id])
                        elif type(self.daemon_class.alarm_id) is int:
                            Alarm.occurrence(self.daemon_class.alarm_id)
                        self.stop()
                        self.start()
                else:
                    self.logger.warn('%s process marked as "started" was not started... starting now.' % self.name)
                    self.start()
            else:
                self.start()
        else:
            if self.started:
                self.logger.warn('%s process was disabled... stopping process.' % self.name)
                self.stop()

    def target(self):
        """
        This is the entry point given to the Process class for the daemon.
        It instantiates the daemon class and then calls the run method on it.
        :return:
        """
        try:
            setproctitle.setproctitle('apparent-%s' % self.name)
            self.logger.info('***%s STARTED (PID=%s) AT %s (UTC=%s).' % (self.name.upper(), self.process.pid,
                                                                         time.strftime("%I:%M:%S %p"), int(time.time())))
            daemon = self.daemon_class(
                            name=self.name,
                            finished=self.finished,
                            id=self.id,
                            enabled=self.enabled)
            return daemon.run()
        except Exception as ex:
            self.logger.exception('Unhandled exception was caught in %s.' % self.name)

            # NOTE: It seems to speed things up to have the daemon exit right away
            # without waiting for gateway monitor to kill us.

            # time.sleep(self.daemon_class.update_timeout)
            # self.logger.critical('%s daemon not killed by Gateway monitor - exiting!' % self.name)
            # os._exit(1)

    def pid_exists(self):
        """ Check For the existence of a unix pid. """
        if self.pid and int(self.pid) != 0:
            try:
                os.kill(int(self.pid), 0)
            except Exception as ex:
                return False
            else:
                return True
        else:
            return False

    def reload(self):
        """
        Override Model.reload to prevent stopping the process by calling __init__.
        :return:
        """
        if self.daemon_class.source_table is not None:
            self.daemon_class.sync_with_source_table()
        record = self.fetch_control_record()
        self.enabled = record.get('enabled', 0) == 1
        self.started = record.get('started', 0) == 1
        self.suspended = record.get('suspended', 0) == 1
        self.updated = record.get('updated', 0)

    def fetch_control_record(self):
        sql = "SELECT * FROM daemon_control WHERE name = %s" % "%s"
        record = prepared_act_on_database(FETCH_ONE, sql, [self.name])
        if record is None:
            raise MissingControlTableRecord("Missing control table record in daemon_control: sql=%s." % sql)
        return record

    @classmethod
    def find_or_create_all(cls, daemon_class, **kwargs):
        """
        This method uses the daemon_class to instantiate the corresponding DaemonMonitor objects
        used to instantiate and monitor daemons. All daemons are managed using the daemon_control
        table.

        :param daemon_class:
        :param kwargs:
            parent_table: parent table is used to derive number of daemons to instantiate
            parent_id: parent id is used to derive number of daemons to instantiate
        :return:
        List of DaemonMonitor objects that can be used to instantiate and monitor daemons.
        """
        parent_table = kwargs.get('parent_table', None)
        parent_id = kwargs.get('parent_id', None)
        extra = kwargs.get('extra', None)

        if daemon_class.source_table:
            daemon_class.sync_with_source_table()
        else:
            control_records = daemon_class.control_records(source_id=parent_id)
            if len(control_records) == 0:
                sql = "INSERT INTO daemon_control (name, enabled, source_table, source_id) " \
                      "VALUES (%s, %s, %s, %s)" % ("%s", "%s", "%s", "%s")
                args = [daemon_class.get_name(parent_id), 0, parent_table, parent_id]
                prepared_act_on_database(EXECUTE, sql, args)

        daemon_monitors = []

        for control_record in daemon_class.control_records(source_id=parent_id):
            if extra is not None:
                control_record['no_restarts'] = extra.get('no_restarts', False)
            daemon_monitor = cls(daemon_class, **control_record)
            if daemon_class.always_enabled and not daemon_monitor.enabled:
                daemon_monitor.save(enabled=True)
            daemon_monitors.append(daemon_monitor)

        return daemon_monitors

    def save(self, **kwargs):
        keys, args = [], []
        for k, v in kwargs.iteritems():
            keys.append(k)
            args.append(v)
        params = ", ".join(["%s=%s" % (k, "%s") for k in keys])
        sql = "UPDATE daemon_control SET %s WHERE name=%s" % (params, "%s")
        args.append(self.name)
        prepared_act_on_database(EXECUTE, sql, args)

    def __hash__(self):
        return hash(self.name)
