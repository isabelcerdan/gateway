#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: Daemon

@description: Abstract Daemon class used to create new Daemons.

@copyright: Apparent Inc. 2018

@reference:

@author: patrice

@created: November 2, 2018
"""
from datetime import datetime
import time

from lib.db import EXECUTE, FETCH_ALL, prepared_act_on_database
from lib.helpers import datetime_to_utc
from lib.logging_setup import *


class Daemon(object):
    """
    Below is the bare minimum of whats required when creating a Daemon subclass:

        class MyDaemon(Daemon):
            alarm_id = ALARM_ID_MY_DAEMON_RESTARTED

            def __init__(self, **kwargs):
                super(MyDaemon, self).__init__(**kwargs)

            def work(self):
                print("doing some work...")

    For this daemon to be launched, the subclass must be added to the list
    of Daemon classes that will be monitored by the gateway_monitor.py or
    ess_manager.py. A DaemonMonitor object will be created to monitor/start/stop
    your Daemon subclass.

    The following facilities are handled by the DaemonMonitor/Daemon classes:
        1. Daemon: Instantiate a daemon specific self.logger.
        2. DaemonMonitor: Output initial logging header message to this logger.
        3. DaemonMonitor: Catch and log unhandled exceptions.
        4. DaemonMonitor: Raise alarm when daemon needs to be restarted.
        5. DaemonMonitor: Create a record in the daemon_control table if one
            does not yet exist.

    RUN METHOD:

    You can optionally override the run instance method if the work instance
    method is too simplistic for your needs, but be sure to call self.update()
    so that the monitoring process does not restart your daemon.

        EXAMPLE:
        def run(self):
            while True:
                self.update()
                self.do_some_stuff()
                time.sleep(self.polling_freq_sec)


    CUSTOMIZATION:

    The following Daemon class variables can be used to customize the
    behavior of your Daemon subclass:

    param: alarm_id default=None
        Set the alarm_id class variable to the alarm id you want to have
        raised if your daemon does not update in time and needs to be
        restarted.
    param: update_timeout default=5.0
        Override the update_timeout class variable if your daemon takes
        longer than 5.0 seconds to update.
    param: polling_freq_sec default=60.0
        Override the polling_freq_sec class variable if you are using
        the work instance method and need to run more often than once
        every 60 seconds.
    param: daemon default=False
        Override the daemon class variable and set it to True if your
        Daemon subclass needs to launch child processes. This is used
        in the EssManager.
    param: base_name default=<daemon_class_name.lower() + 'd'>
        Override the base_name class variable if you want the name of
        your daemon to be something other than the name of your Daemon
        subclass appended with the letter 'd' (used when naming the
        daemon using setproctitle and when populating the daemon_control
        table with a record for your daemon).
    params: always_enabled default=False
        When set to True, the daemon is always enabled regardless of
        what state the enabled flag is in on the daemon_control record.

    EXAMPLE OF CUSTOMIZATION:

        class MyDaemon(Daemon):
            alarm_id = ALARM_ID_MY_DAEMON_RESTARTED
            base_name = "somethingFunky"
            polling_freq_sec = 60.0
            update_timeout = 180.0
            daemon = False
    """
    base_name = None
    polling_freq_sec = 5.0
    alarm_id = None
    daemon = True
    update_timeout = 30.0
    check_finished = False
    always_enabled = False

    # source_table:
    # Used when the number of daemons to instantiate is based on user managed table.
    # The source_table tells the DaemonMonitor where to look when determining the
    # number of daemons to instantiate. For example, each meter needs to have
    # a meter daemon instantiated for every meter defined by the user in the
    # configurator, therefore the source_table is set to 'energy_meter' for the
    # MeterDaemon.
    source_table = None
    source_id_field = None

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', self.__class__.get_base_name())
        self.id = kwargs.get('id', None)
        self.enabled = kwargs.get('enabled', False)
        self.logger = setup_logger(self.name)

    def update(self):
        sql = "UPDATE daemon_control SET updated = %s WHERE name=%s" % ("%s", "%s")
        args = [int(datetime_to_utc(datetime.utcnow())), self.name]
        prepared_act_on_database(EXECUTE, sql, args)

    def run(self):
        """
        This method can be overridden in subclass if the looping structure
        does not conform to what is needed by the daemon. Make sure to call
        self.update() to prevent the monitor from restarting your daemon.
        :return:
        """
        while True:
            self.update()
            self.work()
            time.sleep(self.__class__.polling_freq_sec)

    def work(self):
        raise Exception("Method must be overridden in subclass.")

    @classmethod
    def get_base_name(cls):
        if cls.base_name is not None:
            return cls.base_name
        else:
            return cls.__name__.lower().replace('daemon', 'd')

    @classmethod
    def get_name(cls, _id=None):
        base_name = cls.get_base_name()
        if _id is not None:
            return "-".join([base_name, str(_id)])
        else:
            return base_name

    @classmethod
    def control_records(cls, **kwargs):
        sql = "SELECT * FROM daemon_control WHERE name LIKE %s" % "%s"
        args = ["%s%%" % cls.get_base_name()]
        source_id = kwargs.get('source_id', None)
        if source_id is not None:
            where_clause = "AND source_id = %s" % "%s"
            sql = " ".join([sql, where_clause])
            args.append(source_id)
        control_records = prepared_act_on_database(FETCH_ALL, sql, args)
        if control_records:
            return control_records
        else:
            return []

    @classmethod
    def source_records(cls):
        sql = "SELECT * FROM %s" % cls.source_table
        source_records = prepared_act_on_database(FETCH_ALL, sql, [])
        return source_records

    @classmethod
    def sync_with_source_table(cls):
        """
        This method will ensure the daemon_control table contains corresponding
        records for each record in the user managed source_table and that the
        enabled flag is synchronized from source table to daemon_control table.

        For example, each meter needs to have a meter daemon instantiated for every
        meter defined by the user in the configurator. The MeterDaemon class sets
        source_table='energy_meter' so that the DaemonMonitor knows to instantiate
        a separate daemon for each record in the energy_meter table.
        """

        # build a dictionary of {<daemon-name>: 'id', <daemon-source-id>} from records in source_table
        # e.g. for meter daemons, source_table="energy_meter" and source_id_field="meter_role"
        # so source_records will be something like {'meterd-pcc': 'pcc', 'meterd-solar': 'solar'}
        # where 'meterd-pcc' is the daemon-name and 'pcc' is the daemon-id.
        source_records = {}
        for r in cls.source_records():
            source_records[cls.get_name(r[cls.source_id_field])] = \
                { 'id': r[cls.source_id_field], 'enabled': r['enabled'] }

        # extract daemon-names from source_table and daemon-names from daemon_control table
        source_names = set(source_records.keys())
        control_names = set([r['name'] for r in cls.control_records()])

        # remove daemon_control names that don't exist in source_table
        sql = "DELETE FROM daemon_control WHERE name IN (%s)" % "%s"
        prepared_act_on_database(EXECUTE, sql, [",".join(control_names - source_names)])

        # add daemon_control names for source_table records that don't exist
        for name in (source_names - control_names):
            sql = "INSERT INTO daemon_control (name, enabled, source_table, source_id) " \
                  "VALUES (%s, %s, %s, %s)" % ("%s", "%s", "%s", "%s")
            prepared_act_on_database(EXECUTE, sql, [name, source_records[name]['enabled'], cls.source_table, source_records[name]['id']])

        # make sure enabled flag is in sync with source table
        for name, source_record in source_records.iteritems():
            sql = "UPDATE daemon_control SET enabled = %s WHERE name = %s" % ("%s", "%s")
            args = [source_record['enabled'], name]
            prepared_act_on_database(EXECUTE, sql, args)