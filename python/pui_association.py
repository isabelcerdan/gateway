#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

# Summary: PCS UNIT INVERTER ASSOCIATION.
#
#          PUI ASSOC ...
#
# Copyright: Apparent Inc. 2018/2019
#
# author: Ed

import time
from datetime import datetime, timedelta
import traceback
import os
import sys
from lib.logging_setup import *

import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
import logging.handlers
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *

import gateway_utils as gu
import setproctitle

from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_INVERTER_ASSOCIATION_INTERNAL_ERROR

from lib.db import prepared_act_on_database

# Globals
# There aren't any.

# -----------------------------------------------------------------------------
WAIT_FOR_IDLE_MAX_LENGTH_SECONDS = 300
SLEEP_BEFORE_CHECKING_AGAIN_SECONDS = 60
NUMBER_OF_WAITS_BEFORE_GIVING_UP = WAIT_FOR_IDLE_MAX_LENGTH_SECONDS / SLEEP_BEFORE_CHECKING_AGAIN_SECONDS

MINIMUM_DC_VOLTAGE_FOR_BATTERY_OPERATION = 10.0
# -----------------------------------------------------------------------------


logger = setup_logger('puiAssoc')


def instantiate_puiassocscript():
    setproctitle.setproctitle('%s-puiassoc*' % GATEWAY_PROCESS_PREFIX)
    PcsUnitIdAssociationProcess()

class PcsUnitIdAssociationProcess(object):

    # The PUI_ASSOC process will perform an association between selected battery inverters and
    # BMU ID by causing a BMU to discharge. Once the process is complete, then some back-tracking
    # is required to correlate each BMU ID that was discharged on a per-inverter basis to the
    # pcs_units table: each BMU ID is correlated to an "id" in the pcs_units table, and it is the
    # pcs_units/id that is written/updated in the inverters table, and then propagated by TLV to
    # the inverter in question.

    def __init__(self, _=None):

        self.pui_assoc_pid = 0
        self.pui_assoc_is_super_busy = True
        self.pui_assoc_command = None
        self.pui_assoc_status = None
        self.current_date_time = "00-00-00 00:00:00"
        self.current_utc = 0
        self.start_utc = 0
        self.inverter_ip_to_associate = ZERO_IP_ADDRESS
        self.active_time = 0
        self.deactive_time = 0
        self.associated_pcs_unit_id = 0
        self.keep_alive_timeout_seconds = 0

        # Lists:

        # To support interactions with the inverters.
        self.SG424_packed_data = []

        # These 2 lists will be populated in tandem, designed to be of the same length:
        #
        # - the 1st list contains the BMU ID from the aess_bmu_data table. Since the id column
        #   is the primary key, no duplications are expected.
        # - the 2nd list (on a 1-to-1 basis with the 1st list) is initialized 0. It will contain the inverters_id from
        #   the inverters table when an inverter is associated with the bmu id in question.
        self.bmu_data_id_list = []
        self.bmu_data_id_associated_inverter_ip_list = []

        # These following 2 lists will be populated in tandem, so they are expected to be of the same length:
        #
        # - the 1st list contains the IP address of the battery inverters to be associated to a PCS UNIT by the
        #   association process.
        # - the 2nd (on a 1-to-1 basis with the 1st list) is initialized with 0/zero. It will contain the PCS UNIT ID
        #   that each inverter is associated with. It will be used when the association process is complete to note
        #   any changes.
        #
        # DO NOT CONFUSE: inverters/pcs_unit_id ... pcs_unit/id,bmu_id_A,bmu_ib_B ... aess_bmu_data/bmu_id
        #
        self.inverters_ip_list = []
        self.inverters_ip_associated_bmu_data_id_list = []
        self.number_inverters = 0

        # --------------------------
        self.run_pui_assoc_script()
        # --------------------------


    # LOCAL FUNCTIONS

    def raise_and_clear_alarm(self, this_alarm):
        Alarm.occurrence(this_alarm)
        Alarm.request_clear(this_alarm)

    def is_pui_assoc_database_tables_sanity_ok(self):

        # 1: audit the pui_association_control table: command must be "start your engines".
        self.pui_assoc_command = gu.get_pui_assoc_command()
        if self.pui_assoc_command != PUI_ASSOC_START_COMMAND:
            logger.info("*** PUI_ASSOC START FAILED: CONTROL COMMAND (%s) NOT %s ***" % (self.pui_assoc_command, PUI_ASSOC_START_COMMAND))
            return False

        # 2: audit the AESS BMU DATA table: there should be at least 1 row in the table.
        aess_bmu_data_row_count = gu.get_database_table_row_count(DB_AESS_BMU_DATA_TABLE_NAME)
        if aess_bmu_data_row_count == 0:
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_STATUS)
            logger.info("*** PUI_ASSOC START FAILED: AESS_BMU_DATA TABLE WAS EMPTY ***")
            return False

        # 3: adit the inverters table: there should be 1 or more battery inverters in the system.
        inverters_to_associate = gu.get_database_battery_inverters_count()
        if inverters_to_associate == 0:
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_STATUS)
            logger.info("*** PUI_ASSOC START FAILED: SYSTEM HAS NO BATTERY INVERTERS ***")
            return False

        # 4: verify that the pcs_units and aess_bmu_data tables are consistent.
        bad_things_detected_string = gu.verify_sanity_pcs_unit_id_and_aess_bmu_data()
        if bad_things_detected_string:
            logger.info("*** PUI_ASSOC START FAILED: %s" % (bad_things_detected_string))
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_STATUS)
            return False

        # PUI_ASSOC is ready to run.
        return True

    def is_build_bmu_data_id_list(self):
        build_list_ok = True
        # Get the list of BMU IDs from the AESS BMU DATA table.
        # Based on sanity, there should be at least 1 BMU.
        self.bmu_data_id_list = gu.get_database_ordered_aess_bmu_id_list()
        if len(self.bmu_data_id_list) == 0:
            # Funny. Earlier sanity checking indicated the table was not empty.
            # But the list of BMU ID is now zero. Alarm and log.
            self.raise_and_clear_alarm(ALARM_ID_INVERTER_ASSOCIATION_INTERNAL_ERROR)
            logger.info("*** PUI_ASSOC START FAILED: AESS BMU DATA table populated, but BMU ID list length 0 ***")
            build_list_ok = False
        else:
            for each_bmu in self.bmu_data_id_list:
                self.bmu_data_id_associated_inverter_ip_list.append(None)
        return build_list_ok

    def is_build_inverters_ip_list(self):
        # Build the list of BATTERY inverters IP that are to be associated to pcs units.
        build_list_ok = True
        self.inverters_ip_list = gu.db_get_inverters_all_pui_waiting_list()
        if len(self.inverters_ip_list) == 0:
            # Process should have bailed earlier, should not have come here. Either it is a coding error,
            # or a "wee window of opportunity" presented itself for this to happen. Vote for coding error.
            # Alarm and log.
            self.raise_and_clear_alarm(ALARM_ID_INVERTER_ASSOCIATION_INTERNAL_ERROR)
            logger.info("*** PUI_ASSOC START FAILED: NO BATTERY INVERTERS SELECTED FOR ASSOCIATION ***")
            build_list_ok = False
        else:
            # The battery inverters IP address list has been populated. Build the list of those IP addresses,
            # and for each such IP, init the table that will contain the associated bmu id when pui assoc is run.
            self.number_inverters = len(self.inverters_ip_list)
            for each_ip in self.inverters_ip_list:
                self.inverters_ip_associated_bmu_data_id_list.append(None)
        return build_list_ok

    def is_any_impediments_to_pui_assoc(self):
        # Ensure there are no impediments to allow the PUI_ASSOC to proceed.
        # If there are any, wait for some amount of time to pass before bailing.

        # Itty-bitty dictionary to correlate DELAYED->FAILED STATUSES:
        delay_to_fail_status = {PUI_ASSOC_DELAYED_CONTROL_NOT_IDLE_STATUS : PUI_ASSOC_FAILED_CONTROL_NOT_IDLE_STATUS,
                                PUI_ASSOC_DELAYED_UPGRADE_STATUS          : PUI_ASSOC_FAILED_UPGRADE_STATUS,
                                PUI_ASSOC_DELAYED_FEED_DETECT_STATUS      : PUI_ASSOC_FAILED_FEED_DETECT_STATUS
                               }

        first_time_through = True
        number_of_loops = 0
        impediments_exist = True
        while impediments_exist:
            impediment_status_string = gu.get_pui_assoc_impediment_status()
            if impediment_status_string:
                # Something is preventing PUI_ASSOC from becoming active. Set the status in the database,
                # and wait for some amount of time until they pass before bailing.
                gu.update_pui_assoc_status(impediment_status_string)
                if first_time_through:
                    logger.info("*** PUI_ASSOC START DELAYED DUE TO %s ***" % (impediment_status_string))
                time.sleep(SLEEP_BEFORE_CHECKING_AGAIN_SECONDS)
                first_time_through = False
                number_of_loops += 1
                if number_of_loops > NUMBER_OF_WAITS_BEFORE_GIVING_UP:
                    failed_status = delay_to_fail_status[impediment_status_string]
                    gu.update_pui_assoc_status(failed_status)
                    logger.info("*** PUI_ASSOC FAILED TO START: %s NOW %s ***" % (impediment_status_string, failed_status))
                    break
            else:
                # There are no impediments. Proceed with PUI_ASSOC.
                if first_time_through:
                    # There were no impediments when PUI_ASSOC was launched. No news is good news.
                    pass
                else:
                    # All impediments have pased. Log this notion.
                    logger.info("*** PUI_ASSOC: PRIOR IMPEDIMENTS HAVE PASSED ***")
                impediments_exist = False

        return impediments_exist

    def is_validate_inverters_for_comm(self):
        # Verify communication with the battery inverters that will be associated.
        #
        # Since this phase can go on for a while, set the status to validating.
        logger.info("PUI_ASSOC: ENTER VALIDATION PHASE FOR %s BATTERY INVERTERS" % (self.number_inverters))
        gu.update_pui_assoc_status(PUI_ASSOC_VALIDATING_STATUS)

        validation_ok = True
        number_inverters_responded = 0
        number_inverters_no_response = 0
        rank = 0

        # Validate communication to each battery inverter in the system.
        for this_ip in self.inverters_ip_list:
            this_string = "IP %s [%s] -> " % (this_ip, rank)
            rank += 1
            (runmode, dc_volts, dc_amps, dc_watts, dc_gwa_string, under_gateway_control, keep_alive_timeout) = gu.get_inverter_status_for_association(this_ip)
            self.keep_alive_timeout_seconds = keep_alive_timeout / 1000
            if runmode == RUNMODE_SG424_APPLICATION:
                number_inverters_responded += 1
                dc_sm_state_string = gu.extract_dc_sm_state_string(dc_gwa_string)
                dc_sm_state_string = dc_sm_state_string.upper()
                this_string += "APP MODE WITH DC %sv/%sA/%sW/DCsm=%s" % ('{0:.1f}'.format(dc_volts),
                                                                         '{0:.1f}'.format(dc_amps),
                                                                         dc_watts,
                                                                         dc_sm_state_string)
                poor_dc_info_string = None
                not_operational_info_string = None
                not_under_gateway_control_string = None
                if dc_volts < MINIMUM_DC_VOLTAGE_FOR_BATTERY_OPERATION:
                    poor_dc_info_string = "*LOW DC*"
                    this_string += poor_dc_info_string
                if (dc_sm_state_string != "WOPR") and (dc_sm_state_string != "OPER"):
                    not_operational_info_string = "*DC NOT OPERATIONAL*"
                    this_string += not_operational_info_string
                if under_gateway_control == False:
                    not_under_gateway_control_string = "*NOT UNDER GTW_CTRL*"
                    this_string += not_under_gateway_control_string

                if poor_dc_info_string or not_operational_info_string or not_under_gateway_control_string:
                    this_string += " - EXPECT ASSOC FAILURE"

            else:
                # Most likely: no response.
                number_inverters_no_response += 1
                this_string += "*NO_RESPONSE TO QUERY*"
            logger.info(this_string)

        if self.number_inverters == (number_inverters_responded + number_inverters_no_response):
            # Of course.
            if number_inverters_no_response == 0:
                logger.info("*ALL* SELECTED BATTERY INVERTERS ARE IN APP MODE")
            elif number_inverters_responded == 0:
                # No inverters responded. This is one of the few conditions in which the PUI_ASSOC process
                # will refuse to get out of bed.
                validation_ok = False
                logger.info("ZERO BATTERY INVERTERS ARE IN COMMUNICATION - NO PUI_ASSOC TONIGHT")
            else:
                # Some inverters did not respond. Proceed anyway. One day, a criteria might be established.
                logger.info("%s BATTERY INVERTERS: %s IN APP MODE AND %s ARE *NOT* COMMUNICATING - PROCEEDING NEVERTHELESS"
                                                                                 % (self.number_inverters,
                                                                                    number_inverters_responded,
                                                                                    number_inverters_no_response))
        else:
            # Either a coding error or the impossible occured. I vote for the impossible. Log it.
            self.raise_and_clear_alarm(ALARM_ID_INVERTER_ASSOCIATION_INTERNAL_ERROR)
            logger.info("UNEXPECTED LISTS LENGTHS: %s SHOULD EQUAL %s + %s" % (self.number_inverters,
                                                                               number_inverters_responded,
                                                                               number_inverters_no_response))
            validation_ok = False
        return validation_ok

    def get_ordered_bmu_id_and_current_readings(self):
        # Get the list of all BMU ID and current values ordered by BMJU ID.
        bmu_id_list = []
        bmu_id_current_list = []
        select_string = "SELECT %s,%s FROM %s ORDER BY %s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                               AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                                               DB_AESS_BMU_DATA_TABLE_NAME,
                                                               AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME)
        select_result = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_bmu in select_result:
            bmu_id_list.append(each_bmu[AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME])
            bmu_id_current_list.append(each_bmu[AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME])
        return (bmu_id_list, bmu_id_current_list)

    def get_pcs_unit_id_from_inverter(this_ip):
        # Query the inverter.
        inverter_pcs_unit_id = 0
        inverter_replied = False
        SG424_data = gu.send_and_get_query_data(this_ip)
        (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
        if tlv_id_list:
            for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
                # Only the PCS_UNIT_ID is of interest.
                if tlv_id == SG424_TO_GTW_PCS_UNIT_ID_U16_TLV_TYPE:
                    inverter_pcs_unit_id = tlv_data
                    inverter_replied = True
                    break
            # ... for
        else:
            # Zero-length list, so no response.
            logger.info("-> GATEWAY-CONTROL/INV_CTRL QUERY RESPONSE NOT RETURNED")
        return (inverter_replied, inverter_pcs_unit_id)

    def is_temporary_bmu_id_list_match(self, this_bmu_id_list, that_bmu_id_list):
        # Lists length must match, and each ID in each list must match on a 1-to-1 basis.
        # Surely there is a better way to check this. But don't rely on "cmp()", it's been deprecated.
        lists_id_match = True
        if len(this_bmu_id_list) == len(that_bmu_id_list):
            for iggy in range(0, len(this_bmu_id_list)):
                if this_bmu_id_list[iggy] != that_bmu_id_list[iggy]:
                    lists_id_match = False
                    logger.info("- BMU ID LISTS MATCH CHECK FAILED: INDEX %s: %s vs %s" % (iggy, this_bmu_id_list[iggy], that_bmu_id_list[iggy]))
                    break
        else:
            lists_id_match = False
        return lists_id_match

    def is_all_bmu_devices_quiet(self):
        select_string = "SELECT %s,%s FROM %s WHERE %s!=%s;" % (AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME,
                                                                AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME,
                                                                DB_AESS_BMU_DATA_TABLE_NAME,
                                                                AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, "%s")
        select_args = (0.00, )
        bmu_data_list = prepared_act_on_database(FETCH_ALL, select_string, select_args)
        if bmu_data_list:
            return False
        return True

    def associate_inverter_to_bmu_id(self, inverter_index):
        # Include the string ID/position in the log. Get it now based on IP.
        # TODO: consider getting string ID/position in their own list ...
        (string_id, string_position) = gu.get_string_id_and_string_position_by_ip(self.inverter_ip_to_associate)
        logger.info("ASSOCIATING IP %s stringId/Pos=%s/%s (%s of %s)" % (self.inverter_ip_to_associate,
                                                                         string_id,
                                                                         string_position,
                                                                         inverter_index+1,
                                                                         self.number_inverters))
        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        gu.update_pui_assoc_timestamp(self.current_date_time)
        gu.db_update_inverters_pui_assoc_status_by_ip(self.inverter_ip_to_associate, INVERTERS_TABLE_PUI_ASSOC_STATUS_IN_PROGRESS)

        # -----------------------------------------------------------------------------------------
        # All BMU units are expected to be quiet. Check.
        (bmu_id_list,current_list) = gu.get_bmu_id_and_non_zero_current_from_bmu_data_table()
        if bmu_id_list:
            # Hmmmmm ... not quiet? Notify, wait a bit, try again.
            this_string = "-> %s BMU DEVICES STILL RUNNING, THEY ARE:" % (len(bmu_id_list))
            for bmu_id, current in zip(bmu_id_list, current_list):
                # Should something be done other than just logging?
                this_string += " BMU ID %s/%sA" % (bmu_id, current)
            logger.info(this_string)
            del bmu_id_list[:]
            del current_list[:]
            time.sleep(self.deactive_time)

            (bmu_id_list,current_list) = gu.get_bmu_id_and_non_zero_current_from_bmu_data_table()
            if bmu_id_list:
                # Still not quiet!
                # As Elmer Fudd would say: expecting a "vewy vewy quiet" system ...
                this_string = "-> SOME BMU DEVICES *STILL* RUNNING, THEY ARE:"
                # for index, bmu_id, current in enumerate(bmu_id_list, current_list):
                #for index, (bmu_id, current) in enumerate(bmu_id_list, current_list):
                for bmu_id, current in zip(bmu_id_list, current_list):
                    # Should something be done other than just logging?
                    this_string += " BMU ID %s/%sA" % (bmu_id, current)
                logger.info(this_string)
                del bmu_id_list[:]
                del current_list[:]
                logger.info("-> FAILED TO GET BMU DEVICES QUIET - ASSOCIATION FOR THIS INVERTER NOT PERFORMED")
                gu.db_update_inverters_pui_assoc_status_by_ip(self.inverter_ip_to_associate, INVERTERS_TABLE_PUI_ASSOC_STATUS_DONE)
                gu.update_pui_assoc_counter_column(PUI_ASSOCIATION_CONTROL_TABLE_INVERTERS_COMPLETED_COLUMN_NAME, (inverter_index + 1))
                return
            else:
                logger.info("-> ALL BMU DEVICES IDLE AFTER EXTRA WAIT")
        else:
            # The more typical condition encountered.
            logger.info("-> ALL BMU DEVICES IDLE")

        # -----------------------------------------------------------------------------------------
        # Force the inverter to discharge. This will cause the BMU to which it is connected to
        # discharge. Wait a wee bit for electrons to be herded.
        #
        # Go through an itty bity loop sending 100% curtailment commands to the inverter in
        # question every second or so for the "activation period" as per the PUI control table.
        # This should be sufficient to cause BMU discharge. If the gateway goes away, the inverter
        # is still under gatewqay control and will soon lose its keep alive.
        number_of_curt_packets = self.active_time
        logger.info("-> SENDING SERIES OF 100% CURTAIL TO INVERTER")
        for iggy in range (0, number_of_curt_packets):
            i_dont_care_much = gu.is_send_simple_curtail_packet(100, self.inverter_ip_to_associate)
            self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
            gu.update_pui_assoc_timestamp(self.current_date_time)
            if i_dont_care_much == False:
                logger.info("-> FAILED TO SEND 100% CURTAIL TO INVERTER")
            time.sleep(1)

        # -----------------------------------------------------------------------------------------
        # Re-read the BMU ID table to see which BMU device(s) is(are) discharging.
        # This is done immediately after sending the above series of 100% curtail commands,
        # so the inverter will not declare its keep alive missing just yet.
        # Perform basic sanity to ensure a consistent set of BMU ID.
        (temporary_bmu_id_list, temporary_bmu_id_current_list) = self.get_ordered_bmu_id_and_current_readings()

        if self.is_temporary_bmu_id_list_match(temporary_bmu_id_list, self.bmu_data_id_list):
            number_of_discharging_bmu = 0
            # TODO: IN THIS SECTION, BETTER MANAGEMENT/ACCESS TO THE VARIOUS TABLES: - USE zip please!!!
            for index, each_current_reading in enumerate(temporary_bmu_id_current_list):
                this_bmu_id = self.bmu_data_id_list[index]
                if each_current_reading != 0.00:
                    number_of_discharging_bmu += 1
                    logger.info("-> DISCHARGE CURRENT DETECTED ON BMU_ID %s = %s" % (this_bmu_id, each_current_reading))
                    # --------------------------------------------------------------------------------------------------
                    # Update the "associated" BMU ID -> IP list as a comma-separated IP list).
                    this_bmu_ip_list = self.bmu_data_id_associated_inverter_ip_list[index]
                    if this_bmu_ip_list:
                        # String already contains 1 or more IPs, so add a comma and then this IP.
                        this_bmu_ip_list += ",%s" % (self.inverter_ip_to_associate)
                    else:
                        # 1st IP to add: no comma.
                        this_bmu_ip_list = "%s" % (self.inverter_ip_to_associate)
                    self.bmu_data_id_associated_inverter_ip_list[index] = this_bmu_ip_list

                    # --------------------------------------------------------------------------------------------------
                    # Update the IP -> BMU ID association list.
                    # Find the index into self.inverters_ip_list based on the IP, and based on THAT index,
                    # write the BMU ID into the ip_associated_bmu list as a comma-separated list.
                    rank_into_ip = self.inverters_ip_list.index(self.inverter_ip_to_associate)
                    this_inverter_bmu_list = self.inverters_ip_associated_bmu_data_id_list[rank_into_ip]
                    if this_inverter_bmu_list:
                        # String already contains 1 or more IPs, so add a comma and then this IP.
                        this_inverter_bmu_list += ",%s" % (this_bmu_id)
                    else:
                        # 1st BMU to add: co comma.
                        this_inverter_bmu_list = "%s" % (this_bmu_id)
                    self.inverters_ip_associated_bmu_data_id_list[rank_into_ip] = this_inverter_bmu_list

            if number_of_discharging_bmu == 0:
                logger.info("-> IP %s DID NOT CAUSE ANY BMU DEVICES TO DISCHARGE" % (self.inverter_ip_to_associate))
        else:
            # Right in the middle of the pui association process, the aess_bmu_data table has changed.
            logger.info("*** AESS BMU DATA TABLE HAS CHANGED - bmu_id CHANGED ***")

        # Send the inverter a curtail-to-zero to get it to shut down power making as fast as possible.
        i_dont_care_much = gu.is_send_simple_curtail_packet(0, self.inverter_ip_to_associate)
        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        gu.update_pui_assoc_timestamp(self.current_date_time)
        if i_dont_care_much == True:
            logger.info("-> ZERO % CURTAIL SENT TO INVERTER")
        if i_dont_care_much == False:
            logger.info("-> FAILED TO SEND ZERO % CURTAIL TO INVERTER")

        # Wait a bit so that the BMU in question can settle down before going to the next inverter.
        # Must wait for at least the inverter's keep-alive timeout to expire before it disconnects,
        # then a small additional amount of time for the BMUs themselves to settle out.
        extra_settling_time = self.keep_alive_timeout_seconds + self.deactive_time
        logger.info("-> WAIT %s+%s SECS TO ALLOW BMU UNIT(S) TO SETTLE BACK DOWN" % (self.keep_alive_timeout_seconds, self.deactive_time))

        time.sleep(extra_settling_time)
        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        gu.update_pui_assoc_timestamp(self.current_date_time)

        # Done with his inverter.
        logger.info("-> ASSOCIATION PROCESS ON THIS INVERTER = DONE")
        gu.db_update_inverters_pui_assoc_status_by_ip(self.inverter_ip_to_associate, INVERTERS_TABLE_PUI_ASSOC_STATUS_DONE)
        gu.update_pui_assoc_counter_column(PUI_ASSOCIATION_CONTROL_TABLE_INVERTERS_COMPLETED_COLUMN_NAME, (inverter_index+1))

        # Deallocate the lists to release the memory. Not that it is necessary since it gets released when
        # exiting this function, but it's just good practice.
        del temporary_bmu_id_list[:]
        del temporary_bmu_id_current_list[:]

    def is_create_pcs_units_table_from_scratch(self):
        table_creation_ok = True
        # Remember, for now: only 1 ESS system!!!!!
        logger.info("CREATING THE %s TABLE FROM SCRATCH" % (DB_PCS_UNITS_TABLE_NAME))
        for each_bmu_id in self.bmu_data_id_list:
            # INSERT INTO pcs_units (bmu_id_A,ess_unit_id) VALUES (xxx,1);
            insert_string = "INSERT INTO %s (%s,%s) VALUES (%s,%s);" % (DB_PCS_UNITS_TABLE_NAME,
                                                                        PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                                        PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME,
                                                                        "%s", "%s")
            insert_args = (each_bmu_id, 1)
            prepared_act_on_database(EXECUTE, insert_string, insert_args)

        # Validate the insert: number of rows must match, and bmu_id_A must match 1-to-1 with the bmu_data_id_list.
        number_pcs_units_rows = gu.get_database_table_row_count(DB_PCS_UNITS_TABLE_NAME)
        if number_pcs_units_rows == len(self.bmu_data_id_list):
            # Check the bmu id on a 1-to-1 basis:
            pcs_units_bmu_id_A_list = gu.get_database_ordered_pcs_units_bmu_id_A_list()
            for iggy in range (0, len(self.bmu_data_id_list)):
                if pcs_units_bmu_id_A_list[iggy] == self.bmu_data_id_list[iggy]:
                    pass
                else:
                    logger.info("PCS UNIT CREATE ERROR - ID NO MATCH")
                    table_creation_ok = False
                    break
        else:
            table_creation_ok = False
        return table_creation_ok

    def pui_assoc_final_assessment(self):

        # The pui association process is entering the final assessment and resolution phase.
        # Provide overall basic stats regarding the BMUs, then regarding the inverters.
        this_string = "ENTERING FINAL ASSESSMENT AND RESOLUTION PHASE"
        logger.info(this_string)

        # -----------------------------------------------------------------------------------------
        #
        # This section of code is informational-only.
        # TODO: consider placing this section under control of a log yes/no constraint from ther PUI control table.
        #
        this_string = "BMU_ID->INVERTERS ASSOCIATION INFORMATIONAL RESULTS:"
        logger.info(this_string)

        # Some basic info and stats regarding the BMUs:
        for iggy in range (0, len(self.bmu_data_id_list)):
            this_string = "- BMU ID %s CORRELATED TO " % (self.bmu_data_id_list[iggy])
            if self.bmu_data_id_associated_inverter_ip_list[iggy]:
                number_of_inverters = gu.get_comma_separated_list_length(self.bmu_data_id_associated_inverter_ip_list[iggy])
                this_string += "%s INVERTERS, IP %s " % (number_of_inverters, self.bmu_data_id_associated_inverter_ip_list[iggy])
            else:
                this_string += "*NO INVERTER*"
            logger.info(this_string)

        # Some basic info and stats regarding the inverters:
        this_string = "INVERTERS->BMU_ID ASSOCIATION INFORMATIONAL RESULTS:"
        logger.info(this_string)
        for iggy in range (0, len(self.inverters_ip_list)):
            this_string = "- IP %s CORRELATED TO: " % (self.inverters_ip_list[iggy])
            if self.inverters_ip_associated_bmu_data_id_list[iggy]:
                number_of_bmu = gu.get_comma_separated_list_length(self.inverters_ip_associated_bmu_data_id_list[iggy])
                this_string += "%s BMUs, ID %s" % (number_of_bmu, self.inverters_ip_associated_bmu_data_id_list[iggy])
            else:
                this_string += "*NO BMU*"
            logger.info(this_string)
        #
        #
        # -----------------------------------------------------------------------------------------

        # Now is the time to resolve and close the great divide between:
        #     - inverters:pcs_unit_id         -> pcs_units:id
        #     - pcs_units:bmu_id_A + bmu_ib_B -> aess_bmu_data:bmu_id
        #
        # FOR NOW, ASSUME: 1 ESS ONLY.

        this_string = "INVERTERS->PCS_UNITS FINAL ASSOCIATION, RESULTS AND ACTIONS:"
        logger.info(this_string)
        for iggy, this_ip in enumerate(self.inverters_ip_list):
            self.associated_pcs_unit_id = 0
            bmu_id_one = 0
            bmu_id_two = 0
            bmu_id_list = []
            bmu_id_list_length = 0
            if  self.inverters_ip_associated_bmu_data_id_list[iggy]:
                bmu_id_list =  self.inverters_ip_associated_bmu_data_id_list[iggy].split(',')
                bmu_id_list_length = len(bmu_id_list)

            if bmu_id_list_length == 1:
                bmu_id_one = int(bmu_id_list[0])
            elif bmu_id_list_length == 2:
                bmu_id_one = int(bmu_id_list[0])
                bmu_id_two = int(bmu_id_list[1])

            self.associated_pcs_unit_id = gu.get_pcs_unit_id_from_bmu_id_pair(bmu_id_one, bmu_id_two)
            database_pcs_unit_id = gu.get_inverters_db_pcs_unit_id(this_ip)

            if self.associated_pcs_unit_id != 0:
                # Update the PUI status bit ion the inverter table.
                gu.db_update_inverters_pui_assoc_status_by_ip(this_ip, INVERTERS_TABLE_PUI_ASSOC_STATUS_COMPLETE_SUCCESS)

                # Check if database/inverter update is required.
                this_string = "- IP %s ASSOCIATION OK: " % (this_ip)
                if self.associated_pcs_unit_id == database_pcs_unit_id:
                    # No change detected.
                    this_string += "DATABASE pcs_unit_id = %s <- NO UPDATES REQUIRED" % (database_pcs_unit_id)
                else:
                    this_string += "DATABASE pcs_unit_id UPDATED FROM %s -> %s" % (database_pcs_unit_id, self.associated_pcs_unit_id)
                    # Update the database:
                    gu.update_db_pcs_unit_id_by_ip(this_ip, self.associated_pcs_unit_id)

                    # Update the inverter:
                    send_inverter_result = gu.is_send_inverter_pcs_unit_id_update(this_ip, self.associated_pcs_unit_id)
                    if not send_inverter_result:
                        this_string += " ***BUT INVERTER SEND FAILED ***"
                logger.info(this_string)
            else:
                # The process failed to associate an inverter to anything in the pcs_units table.
                gu.db_update_inverters_pui_assoc_status_by_ip(this_ip, INVERTERS_TABLE_PUI_ASSOC_STATUS_COMPLETE_FAILED)
                (db_bmu_A, db_bmu_B) = gu.get_bmu_id_pair_from_pcs_unit_id(database_pcs_unit_id)
                this_string = "- IP %s ASSOCIATION FAILED (DB pcs_units id=%s): " % (this_ip, database_pcs_unit_id)
                this_string += "EXPECTED BMU"
                if db_bmu_A != 0:
                    this_string += " %s" % (db_bmu_A)
                if db_bmu_B != 0:
                    this_string += " %s" % (db_bmu_B)
                if (bmu_id_one == 0) and (bmu_id_two == 0):
                    this_string += " BUT NONE WERE ACTIVE "
                else:
                    this_string += " BUT ONLY SAW"
                    if bmu_id_one != 0:
                        this_string += " %s" % (bmu_id_one)
                    if bmu_id_two != 0:
                        this_string += " %s" % (bmu_id_two)
                this_string += "*** RE-RUN PUI ASSOC ON THIS INVERTER ***"
                logger.info(this_string)

            del bmu_id_list[:]

        logger.info("FINAL ASSESSMENT AND RESOLUTION PHASE COMPLETE")

        return

    # ------------------------------------------------------------------------------------------------------------------
    #
    # The main entry point to the PCS UNIT ID Association ("PUI_ASSOC") process.
    #
    # The process goes through several steps before entering the actual association phase:
    #
    #  1: get and store the pid in the control table
    #  2: set the timestamp in the control table
    #  3: check database tables for sanity
    #  4: build the BMU ID list from the database
    #  5: build the IP list of battery inverters to associate
    #  6: ensure there are no impediments to allow the _to proceed
    #  7: write a start-up notice to the log
    #  8: update the pui association table for GUI progress display
    #  9: log a few stats (number of inverters involved, pcs_units table)
    # 10: retrieve the BMU activate/deactivate timers
    # 11: verify that ALL battery inverters to be associated are in communication
    # 12: multicast a curtail-to-zero command
    #
    # If all preparations are good-to-go, then enter the association phase on a per-inverter basis.
    #
    def run_pui_assoc_script(self):

        #  1: get and store the pid in the control table
        self.pui_assoc_pid = os.getpid()
        if not gu.set_database_process_pid(DB_PUI_ASSOCIATION_CONTROL_TABLE, PUI_ASSOCIATION_CONTROL_TABLE_PID_COLUMN_NAME, self.pui_assoc_pid):
            logger.critical("*** PUI_ASSOC START FAILED: COULD NOT SET DB PUI ASSOC PID ***")
            return

        #  2: set the timestamp in the control table
        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.current_utc = int(time.time())
        self.start_utc = self.current_utc
        gu.update_pui_assoc_timestamp(self.current_date_time)

        #  3: check database tables for sanity
        if not self.is_pui_assoc_database_tables_sanity_ok():
            # Reason for insanity will have been logged and/or alarmed.
            return

        #  4: build the BMU DATA ID list from the database
        if not self.is_build_bmu_data_id_list():
            # Reason for returning from here will have been logged and/or alarmed.
            # Most likely due to an empty BMU DATA table.
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_STATUS)
            return

        #  5: build the IP list of battery inverters to associate. This will also build a list of the same size
        # to contain the PCS_UNITS_ID that each inverter is associated with (eventually to be compared to that
        # in the database and updated as necessary).
        #
        # NOTE: pcs_unit_id is an index into the pcs_units table, which contains bmu_id_A/B that are indices into
        #       the aess_bmu_data table.
        if not self.is_build_inverters_ip_list():
            # Reason for returning from here will have been logged and/or alarmed.
            # Most likely due to no battery inverters in the system.
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_STATUS)
            return

        #  6: ensure there are no impediments to allow the PUI_ASSOC to proceed
        if self.is_any_impediments_to_pui_assoc():
            # Several causes can impede the association process, and whatever they may be, this process has timed out
            # waiting for them to pass. The status will have been updated in the database.
            return

        #  7: write a start-up notice to the log
        this_string = "***PUI ASSOCIATION STARTED (PID %s): %s (UTC = %s)" % (self.pui_assoc_pid, self.current_date_time, self.current_utc)
        logger.info(this_string)
        gu.update_pui_assoc_command(PUI_ASSOC_RUNNING_COMMAND)
        gu.update_pui_assoc_status(PUI_ASSOC_STARTED_STATUS)

        # 8: update the pui association table for GUI progress display
        gu.update_pui_assoc_counter_column(PUI_ASSOCIATION_CONTROL_TABLE_NUM_OF_INVERTERS_COLUMN_NAME, self.number_inverters)
        gu.update_pui_assoc_counter_column(PUI_ASSOCIATION_CONTROL_TABLE_INVERTERS_COMPLETED_COLUMN_NAME, 0)

        #  9: log a few stats (number of inverters involved, pcs_units table)
        this_string = "PUI_ASSOC: %s INVERTERS TO ASSOCIATE AGAINST %s BMU DEVICES" % (len(self.inverters_ip_list),
                                                                                       len(self.bmu_data_id_list))
        logger.info(this_string)

        # 10: retrieve the BMU activate/deactivate timers
        (self.active_time, self.deactive_time) = gu.get_pui_assoc_act_deact_time()
        logger.info("PUI_ASSOC: ACTIVATE AND DEACTIVATE WAIT TIMERS(SECS) %s %s" % (self.active_time, self.deactive_time))

        # 11: verify that ALL battery inverters to be associated are in communication
        if not self.is_validate_inverters_for_comm():
            # Come here only if zero battery inverters responded to query. Although 1 or more battery
            # inverters may not be communicating, if none are in communication, then all bets are off.
            logger.info("***PUI_ASSOC: NO BATTERY INVERTERS ARE IN COMMUNICATION, FAILED: %s (UTC = %s)***" % (self.current_date_time, self.current_utc))
            gu.update_pui_assoc_command(PUI_ASSOC_NO_COMMAND_COMMAND)
            gu.update_pui_assoc_status(PUI_ASSOC_FAILED_DISCONNECT_SETUP_STATUS)
            return

        # 12: multicast a curtail-to-zero command
        # This will help individual inverters to start making power when they are commanded
        # to 100% when association is being determined.
        if gu.is_send_simple_curtail_packet(0, SG424_MULTICAST_GENERIC_BATTERY):
            logger.info("PUI_ASSOC: 0% CURTAILMENT MCAST ... OK")
        else:
            logger.info("PUI_ASSOC: 0% CURTAILMENT MCAST ... *FAILED*")

        # This process now ready to perform association for all selected inverters. All battery inverters
        # in the inverters table that are "waiting" are now placed "in queue".
        logger.info("PUI_ASSOC: NOW ENTERING ACTIVE ASSOCIATION PHASE")
        gu.update_pui_assoc_status(PUI_ASSOC_IN_PROGRESS_STATUS)
        gu.db_update_inverters_pui_assoc_status1_to_status2(INVERTERS_TABLE_PUI_ASSOC_STATUS_WAITING, INVERTERS_TABLE_PUI_ASSOC_STATUS_IN_QUEUE)

        # -----------------------------------------------------------------------------------------
        # The core of the association process.
        completion_notice_string = "***PCS UNIT ID ASSOCIATION COMPLETED AT"
        while self.pui_assoc_is_super_busy:

            # For each inverter/IP in the list:
            for index, self.inverter_ip_to_associate in enumerate(self.inverters_ip_list):

                # -----------------------------------------
                # PERFORM ASSOCIATION FOR ONE INVERTER.
                self.associate_inverter_to_bmu_id(index)
                # -----------------------------------------

                if gu.is_pui_assoc_process_cancelled():
                    completion_notice_string = "***PCS UNIT ID ASSOCIATION CANCELLED AT"
                    break
            self.pui_assoc_is_super_busy = False
        # -----------------------------------------------------------------------------------------

        # Final results/assessment and some logging results.
        self.pui_assoc_final_assessment()

        self.current_date_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.current_utc = int(time.time())
        gu.update_pui_assoc_command(PUI_ASSOC_NO_COMMAND_COMMAND)
        gu.update_pui_assoc_status(PUI_ASSOC_COMPLETED_STATUS)
        gu.update_pui_assoc_timestamp(self.current_date_time)
        elapsed_seconds = self.current_utc - self.start_utc
        logger.info("%s (UTC = %s, ELAPSED= %s SECS)" % (completion_notice_string, self.current_date_time, self.current_utc, elapsed_seconds))
        return


def main():
    instantiate_puiassocscript()
    return
    
if __name__ == '__main__':
    main()
    exit(0)
