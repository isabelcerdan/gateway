from datetime import datetime

from lib.gateway_constants.DBConstants import *
from lib.logging_setup import setup_logger
from lib.db import prepared_act_on_database

devices = [
    {'name': 'inverter',
     'mac_prefix': INVERTERS_MAC_PREFIX,
     'table': DB_INVERTER_NXO_TABLE,
     'mac_field': INVERTERS_TABLE_MAC_ADDRESS_COLUMN_NAME,
     'ip_field': INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME},
    {'name': 'brainbox',
     'mac_prefix': BRAINBOXES_MAC_PREFIX,
     'table': DB_TUNNEL_TABLE,
     'mac_field': 'mac_address',
     'ip_field': 'IP_Address'},
    {'name': 'icpcon',
     'mac_prefix': ICPCON_MAC_PREFIX,
     'table': DB_ICPCON_DEVICES_TABLE,
     'mac_field': ICPCON_DEVICES_TBL_MAC_ADDR_COLUMN_NAME,
     'ip_field': ICPCON_DEVICES_TBL_IP_ADDR_COLUMN_NAME}
]


class DhcpClient(object):
    """
     Use this class to add/update/delete records from a devices table. 
     This is used when scanning the DHCP leases file or when receiving 
     DHCP lease notifications from dnsmasq.
    """

    def __init__(self, mac_address, ip_address=''):
        self.logger = setup_logger(__name__)

        device = DhcpClient.find_device(mac_address)
        self.mac_field = device.get('mac_field', None)
        self.mac_address = mac_address
        self.ip_field = device.get('ip_field', None)
        self.ip_address = ip_address
        self.table = device.get('table', None)

    @staticmethod
    def find_device(mac_address):
        for device in devices:
            if device['mac_prefix'] in mac_address:
                return device

        return {}

    def is_supported_device(self):
        return self.table is not None

    def find_by_mac_address(self):
        """
        Find inverter/tunnel record(s) by MAC address
        """
        if not self.is_supported_device():
            self.logger.info("Unsupported device %s; no record exists in the database." % self.mac_address)
            return None

        query = "SELECT * FROM %s WHERE %s = %s;" % (self.table, self.mac_field, "%s")
        params = (str(self.mac_address),)

        return prepared_act_on_database(FETCH_ONE, query, params)

    def save(self):
        """ Save new IP address for DHCP client """
        record = self.find_by_mac_address()
        if record:
            # If IP address is already in the database, no need to save
            if record[self.ip_field] == self.ip_address:
                return False

            # Otherwise, update IP address
            query = "UPDATE %s SET %s = %s WHERE %s = %s;" % \
                    (self.table, self.ip_field, "%s", self.mac_field, "%s")
        else:
            query = "INSERT INTO %s (%s, %s) VALUES (%s, %s);" % \
                    (self.table, self.ip_field, self.mac_field, "%s", "%s")

        params = (str(self.ip_address), str(self.mac_address))
        result = prepared_act_on_database(EXECUTE, query, params)
