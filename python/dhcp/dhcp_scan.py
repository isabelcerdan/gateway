# encoding: utf-8
import os
import re
import time
from datetime import datetime
import MySQLdb as MyDB

from lib.db import prepared_act_on_database
from dhcp_client import DhcpClient
from lib.gateway_constants.DBConstants import *
import setproctitle
from lib.logging_setup import *

def dhcp_scan(discovery_type='inverters', leases_file_path='/var/lib/misc/dnsmasq.leases'):
    setproctitle.setproctitle('%s-dhcp-scand' % GATEWAY_PROCESS_PREFIX)
    DhcpScan(discovery_type, leases_file_path)

class DhcpScan(object):
    def __init__(self, discovery_type='inverters', leases_file_path='/var/lib/misc/dnsmasq.leases'):
        self.discovery_type = discovery_type
        self.leases_file_path = leases_file_path
        self.exit_state = 'failed'
        self.inverters_discovered = 0
        self.logger = setup_logger('DHCP-SCAN')
        self.scan_utc_start_time = int(time.time())
        self.begin_scan()
        self.scan_dhcp_leases_file()
        self.end_scan()

    def begin_scan(self):
        query = "UPDATE %s SET %s = %s, %s = %s, %s = %s " \
                "WHERE %s = %s;" % (DB_INVERTERS_STATUS_NXO_TABLE,
                                    'running', "%s", 'pid', "%s", 'inverters_discovered', "%s",
                                    'discovery_type', "%s")
        params = ('1', str(os.getpid()), '0', self.discovery_type)
        try:
            prepared_act_on_database(EXECUTE, query, params)
        except MyDB.Error as error:
            self.logger.error("Can't connect to DB to set as running!!", error)

    # Scan dnsmasq leases file:
    #
    # Format for /var/lib/misc/dnsmasq.leases:
    #     <expiry>   <mac-addr>        <ip-addr>   <host-name> <client-id = intefacer+mac-addr>
    #     -------------------------------------------------------------------------------------
    #     1489770186 d8:cb:8a:84:a1:f9 10.252.2.13 Laptop-PC   01:d8:cb:8a:84:a1:f9
    def scan_dhcp_leases_file(self):
        pattern = re.compile("^([0-9]+)\s+([:a-f0-9]+)\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+([^\s]+)\s+([:a-f0-9|\*]+)\s+$")
        _mac = 2
        _ip = 3
        with open(self.leases_file_path) as leases_file:
            for line in leases_file:
                try:
                    match = pattern.search(line)
                    if match:
                        dhcp_client = DhcpClient(match.group(_mac), match.group(_ip))
                        if dhcp_client.is_supported_device():
                            if dhcp_client.save():
                                self.inverters_discovered += 1;
                except Exception as ex:
                    self.logger.exception("Error occurred while scanning DHCP leases file: %s" % ex)
                    self.exit_state = "failed"
        self.exit_state = "completed"

    def end_scan(self):
        formatted_start_time = datetime.utcfromtimestamp(self.scan_utc_start_time)
        formatted_end_time = datetime.utcfromtimestamp(int(time.time()))
        query = "UPDATE %s SET %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s " \
                "WHERE %s = %s" \
                ";" % (DB_INVERTERS_STATUS_NXO_TABLE,
                       self.exit_state, "%s",
                       'requested_scan_time', "%s",
                       'last_time_scanned', "%s",
                       'running', "%s",
                       'pid', "%s",
                       'inverters_discovered', "%s",
                       'discovery_type', "%s")
        params = ('1', formatted_start_time, formatted_end_time, '0', 'NULL',
                  self.inverters_discovered, self.discovery_type)
        prepared_act_on_database(EXECUTE, query, params)
        end_log = "%s SCAN COMPLETED AT UTC %s, %s AT UTC %s" % (self.discovery_type, formatted_start_time,
                                                                 self.exit_state, formatted_end_time)
        self.logger.info(end_log)

        return True

if __name__ == '__main__':
  dhcp_scan()
