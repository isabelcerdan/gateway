#!/usr/bin/env bash
# Set the Pimentel ESS-One in manual mode, stopping the battery.
#
# copyright Apparent Inc. 2019

# Set the Apparent battery mode to 'manual'.
/usr/share/apparent/python/DbUpdate.py --table ess_units \
                                       --column mode \
                                       --column-value manual \
                                       --where-column-1 id \
                                       --where-value-1 1

# Just in case we are transitioning from 'auto' mode, stop the battery.
sudo /usr/share/apparent/.py2-virtualenv/bin/python /usr/share/apparent/python/EssCLI.py --stop

/usr/share/apparent/python/DbCompare.py --table ess_units \
                                        --column mode \
                                        --where-column id \
                                        --where-value 1 \
                                        --compare-value manual
if [ $? -eq 0 ]
then
  echo "Successful database comparison"
else
  echo "Comparison failed - check log file" >&2
fi
