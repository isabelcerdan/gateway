import time
from multiprocessing import active_children

from task import *
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_SCHEDULER_DAEMON_RESTARTED


class SchedulerDaemon(Daemon):
    polling_freq_sec = 60.0
    update_timeout = 120.0
    alarm_id = ALARM_ID_SCHEDULER_DAEMON_RESTARTED
    always_enabled = True

    def __init__(self, **kwargs):
        super(SchedulerDaemon, self).__init__(**kwargs)
        self.on_interval = kwargs.get('on_interval', True)

    def wait_time(self):
        if self.on_interval:
            return self.polling_freq_sec - (time.time() % self.polling_freq_sec)
        else:
            return self.polling_freq_sec

    def run(self):
        while True:
            self.update()
            time.sleep(self.wait_time())
            for task in Task.find_all():
                task.monitor()
            active_children()
