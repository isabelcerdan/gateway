import subprocess
import os
from datetime import timedelta
from signal import SIG_DFL

from lib.logging_setup import *
from recurrence import *


class Task(object):
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.name = kwargs.get('name', 'UNKNOWN')
        self.start_cmd = kwargs['start_cmd']
        self.start_date = kwargs['start_date']
        self.stop_date = self.start_date + timedelta(seconds=59)
        self.enabled = kwargs.get('enabled', 1) == 1
        self.running = kwargs.get('running', 0) == 1
        self.last_started = kwargs.get('last_started', None)
        self.last_stopped = kwargs.get('last_stopped', None)
        self.pid = kwargs.get('pid', None)
        self.process = None
        self.recurrence_id = kwargs.get('recurrence_id', None)
        self.recurrence = Recurrence.find(self.recurrence_id)
        self.logger = setup_logger("scheduler")

    def reload(self):
        sql = "SELECT * FROM tasks WHERE id=%s" % "%s"
        record = prepared_act_on_database(FETCH_ONE, sql, [self.id])
        if record:
            self.__init__(**record)
        else:
            raise Exception("Unable to find scheduled task '%s' in task table." % self.name)

    def save(self):
        sql = "UPDATE tasks SET running=%s,pid=%s,last_started=%s,last_stopped=%s WHERE id=%s" % \
              ("%s", "%s", "%s", "%s", "%s")
        if isinstance(self.last_started, datetime):
            last_started = self.last_started.strftime("%Y-%m-%d %H:%M:%S")
        else:
            last_started = None

        if isinstance(self.last_stopped, datetime):
            last_stopped = self.last_stopped.strftime("%Y-%m-%d %H:%M:%S")
        else:
            last_stopped = None

        args = [self.running, self.pid, last_started, last_stopped, self.id]
        prepared_act_on_database(EXECUTE, sql, args)

    def pid_running(self):
        """
        Check For the existence of a unix pid.
        :return:
        """
        if self.pid:
            try:
                os.kill(int(self.pid), SIG_DFL)
            except OSError:
                return False
            else:
                return True
        else:
            return False

    def run_time(self):
        return self.last_stopped - self.last_started

    def start(self):
        try:
            self.process = subprocess.Popen(self.start_cmd.split(' '),
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
            self.pid = self.process.pid
            self.last_started = datetime.now()
            self.running = True
            self.logger.info("[%s]: Task start command was run (pid=%s): %s" %
                             (self.name, self.pid, self.start_cmd))
        except Exception as ex:
            self.logger.exception("[%s]: Task start command raised exception: %s!\n" % (self.name, self.start_cmd))
            self.pid = None
            self.running = False
        self.save()

    def update(self):
        """
        Check if task has finished running.
        :return:
        """
        if self.pid_running() or (self.process and self.process.is_alive):
            self.running = True
            self.save()
            return

        # clean up process
        if self.process:
            self.process.terminate()
            self.process = None

        # clean up state if task still marked as running
        if self.running:
            self.running = False
            self.pid = None
            self.last_stopped = datetime.now()
            self.save()
            self.logger.info("[%s]: Task start command finished running in approx. %s: %s" %
                             (self.name, self.run_time(), self.start_cmd))

    def recur(self, now):
        return self.recurrence.recur(self.start_date, now, self.stop_date)

    def time_to_start(self):
        now = datetime.now()
        if self.recurrence:
            return self.recur(now)
        else:
            return self.start_date <= now < self.stop_date

    def monitor(self):
        if self.enabled:
            if self.time_to_start():
                self.start()
            else:
                self.update()

    @staticmethod
    def find(id):
        sql = "SELECT * FROM tasks WHERE id = %s" % "%s"
        record = prepared_act_on_database(FETCH_ONE, sql, [id])
        if record:
            return Task(**record)
        else:
            return None

    @staticmethod
    def find_all():
        sql = "SELECT * FROM tasks"
        records = prepared_act_on_database(FETCH_ALL, sql, [])
        tasks = []
        for record in records:
            tasks.append(Task(**record))
        return tasks
