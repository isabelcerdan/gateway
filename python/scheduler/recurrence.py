from datetime import datetime
from lib.db import *


class Recurrence(object):

    class Interval(object):
        def __init__(self, **kwargs):
            self.begin = kwargs.get('begin', None)
            self.end = kwargs.get('end', None)

        def within(self, value):
            if not value:
                return False
            return self.begin <= value <= self.end

        def without(self, value):
            return not self.within(value)

    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.period = kwargs.get('period', 1)
        self.unit_of_time = kwargs.get('unit', 'day')
        self.weekday = []
        self.weekday.append(kwargs.get('mo', 0) == 1)
        self.weekday.append(kwargs.get('tu', 0) == 1)
        self.weekday.append(kwargs.get('we', 0) == 1)
        self.weekday.append(kwargs.get('th', 0) == 1)
        self.weekday.append(kwargs.get('fr', 0) == 1)
        self.weekday.append(kwargs.get('sa', 0) == 1)
        self.weekday.append(kwargs.get('su', 0) == 1)
        self.ends = kwargs.get('ends', 'never')
        self.ends_on = kwargs.get('ends_on', None)
        self.ends_after = kwargs.get('ends_after', None)

    def occurrences(self, time):
        return time / self.period

    def month_diff(self, a, b):
        if a > b:
            months = a.month - b.month
        else:
            months = b.month - a.month
        if months < 0:
            months += 12
        return months

    def recurring_units(self, interval, time_units, now):
        if (time_units % self.period) != 0:
            return False
        if self.ended(interval, time_units):
            return False
        return interval.begin <= now <= interval.end

    def ended(self, interval, time_units):
        if self.ends == 'on':
            return interval.begin > self.ends_on
        elif self.ends == 'after':
            return self.occurrences(time_units) > self.ends_after

    def recur(self, start_date, now, stop_date):
        # don't recur until we've passed the start date
        if now < start_date:
            return False

        if self.unit_of_time == 'minute':
            begin = datetime(now.year, now.month, now.day, now.hour, now.minute)
            time_units = (now - start_date).seconds / 60
        elif self.unit_of_time == 'hour':
            begin = datetime(now.year, now.month, now.day, now.hour, start_date.minute)
            time_units = (now - start_date).seconds / (60 * 60)
        elif self.unit_of_time == 'day':
            begin = datetime(now.year, now.month, now.day, start_date.hour, start_date.minute)
            time_units = (now - start_date).days
        elif self.unit_of_time == 'week':
            if not self.weekday[now.weekday()]:
                return False
            begin = datetime(now.year, now.month, now.day, start_date.hour, start_date.minute)
            time_units = (now - start_date).days / 7
        elif self.unit_of_time == 'month':
            begin = datetime(now.year, now.month, start_date.day, start_date.hour, start_date.minute)
            time_units = self.month_diff(now, start_date)
        elif self.unit_of_time == 'year':
            begin = datetime(now.year, start_date.month, start_date.day, start_date.hour, start_date.minute)
            time_units = now.year - start_date.year
        else:
            return False

        duration = stop_date - start_date
        interval = Recurrence.Interval(begin=begin, end=(begin+duration))
        return self.recurring_units(interval, time_units, now)

    @staticmethod
    def find(id):
        sql = "SELECT * FROM recurrences WHERE id = %s" % id
        record = prepared_act_on_database(FETCH_ONE, sql, [])
        if record:
            return Recurrence(**record)
        else:
            return None
