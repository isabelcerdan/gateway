#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: ESS Limits 

@description: ESS Limits monitors the master info table, looking for values exceeding the
              configured limits for the system.  If found, the 'capability' field for the
              unit is updated from 'Unrestricted' to {'Charge only', 'Discharge only',
              'Fault'} appropriately.

@copyright: Apparent Inc. 2018

@author: steve

@created: October 19, 2018
"""
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_ESS_LIMITS_RESTARTED, \
                                  ALARM_ID_ESS_LIMITS_BATTERY_SOC_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_BATTERY_SOC_UNDER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_UNDER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_CELL_TEMPERATURE_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_MOSFET_TEMPERATURE_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_ENVIRONMENT_TEMPERATURE_OVER_LIMIT
from ess.ess_daemon import EssDaemon
from ess.ess_unit import EssUnit

ESS_LIMITS_MAX_DB_ERRORS = 5
ESS_LIMITS_DEFAULT_POLLING_SECONDS = 1

"""
ESS Limits Exceptions
"""
class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class ParameterError(Error):
    """
    Exception raised when an invalid parameter is encountered.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ProcessingError(Error):
    """
    Exception raised when an internal error is encountered.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DatabaseError(Error):
    """
    Exception raised when database error maximum is exceeded.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class EssLimitsDaemon(EssDaemon):
    alarm_id = ALARM_ID_ESS_LIMITS_RESTARTED
    update_timeout = 60.0
    polling_freq_sec = ESS_LIMITS_DEFAULT_POLLING_SECONDS

    def __init__(self, **kwargs):
        super(EssLimitsDaemon, self).__init__(**kwargs)
        self.db_errors = 0

    def work(self):
        # if no limits breached, set capability to 'Unrestricted'
        capability = EssUnit.CAPABILITY_UNRESTRICTED
        capability_reason = ''

        self.ess_unit.reload()
        self.ess_unit.reload_master_info()

        """ Look for temperature faults FIRST. Temperature faults will eventually stop the system. """
        if self.ess_unit.battery_max_cell_temperature >= self.ess_unit.battery_max_cell_temperature_limit:
            self.logger.error('Breached maximum cell temperature limit')
            self.logger.info('Cell temperature = %sC' % self.ess_unit.battery_max_cell_temperature)
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_CELL_TEMPERATURE_OVER_LIMIT)
            capability = EssUnit.CAPABILITY_FAULT
            capability_reason = "Breached maximum cell temperature limit.  Cell temperature = %sC" \
                                % self.ess_unit.battery_max_cell_temperature

        elif self.ess_unit.battery_max_mosfet_temperature_limit is not None and \
                        self.ess_unit.max_mosfet_temperature >= self.ess_unit.battery_max_mosfet_temperature_limit:
            self.logger.error('Breached maximum MOSFET temperature limit')
            self.logger.info('MOSFET temperature = %sC' % self.ess_unit.max_mosfet_temperature)
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_MOSFET_TEMPERATURE_OVER_LIMIT)
            capability = EssUnit.CAPABILITY_FAULT
            capability_reason = "Breached maximum MOSFET temperature limit.  MOSFET temperature = %sC" \
                                % self.ess_unit.max_mosfet_temperature

        elif self.ess_unit.battery_max_environment_temperature_limit is not None and \
                        self.ess_unit.max_environment_temperature >= self.ess_unit.battery_max_environment_temperature_limit:
            self.logger.error('Breached maximum environment temperature limit')
            self.logger.info('Environment temperature = %sC' % self.ess_unit.max_environment_temperature)
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_ENVIRONMENT_TEMPERATURE_OVER_LIMIT)
            capability = EssUnit.CAPABILITY_FAULT
            capability_reason = "Breached maximum environment temperature limit.  " \
                                "Environment temperature = %sC" % self.ess_unit.max_environment_temperature

        elif self.ess_unit.min_cell_voltage <= self.ess_unit.cell_voltage_limits.min_hard_limit:
            self.logger.warning("Breached minimum cell voltage hard-limit.")
            self.logger.info("Battery min cell voltage: %s <= %s (hard-limit)" %
                         (self.ess_unit.min_cell_voltage, self.ess_unit.cell_voltage_limits.min_hard_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_UNDER_LIMIT)
            capability = EssUnit.CAPABILITY_CHARGE_ONLY
            capability_reason = "Breached minimum cell voltage hard-limit.  Battery min cell voltage:" \
                              "%s <= %s (hard-limit)" % (self.ess_unit.min_cell_voltage,
                                                         self.ess_unit.cell_voltage_limits.min_hard_limit)

        elif self.ess_unit.stack_soc <= self.ess_unit.stack_soc_limits.min_hard_limit:
            self.logger.warning("Breached minimum SOC hard-limit.")
            self.logger.info("Battery SOC: %s <= %s (hard-limit)" % (self.ess_unit.stack_soc,
                                                                 self.ess_unit.stack_soc_limits.min_hard_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_BATTERY_SOC_UNDER_LIMIT)
            capability = EssUnit.CAPABILITY_CHARGE_ONLY
            capability_reason = "Breached minimum SOC hard-limit.  Battery SOC: %s <= %s (hard-limit)" \
                              % (self.ess_unit.stack_soc, self.ess_unit.stack_soc_limits.min_hard_limit)
        
        elif self.ess_unit.max_cell_voltage >= self.ess_unit.cell_voltage_limits.max_hard_limit:
            self.logger.warning("Breached maximum cell voltage hard-limit")
            self.logger.info("Battery max cell voltage: %s >= %s (hard-limit)" % \
                        (self.ess_unit.max_cell_voltage, self.ess_unit.cell_voltage_limits.max_hard_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_OVER_LIMIT)
            capability = EssUnit.CAPABILITY_DISCHARGE_ONLY
            capability_reason = "Breached maximum cell voltage hard-limit.  Battery max cell voltage: "\
                              "%s >= %s (hard-limit)" % (self.ess_unit.max_cell_voltage,
                                                         self.ess_unit.cell_voltage_limits.max_hard_limit)

        elif self.ess_unit.stack_soc >= self.ess_unit.stack_soc_limits.max_hard_limit:
            self.logger.warning("Breached maximum SOC hard-limit")
            self.logger.info("Battery SOC: %s >= %s (hard-limit)" %
                         (self.ess_unit.stack_soc, self.ess_unit.stack_soc_limits.max_hard_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_BATTERY_SOC_OVER_LIMIT)
            capability = EssUnit.CAPABILITY_DISCHARGE_ONLY
            capability_reason = "Breached maximum SOC hard-limit.  Battery SOC: %s >= %s (hard-limit)" % \
                              (self.ess_unit.stack_soc, self.ess_unit.stack_soc_limits.max_hard_limit)

            """ Soft limit checks. """
        elif self.ess_unit.min_cell_voltage <= self.ess_unit.cell_voltage_limits.min_soft_limit:
            self.logger.warning("Breached minimum cell voltage soft-limit.")
            self.logger.info("Battery min cell voltage: %s <= %s (soft-limit)" %
                         (self.ess_unit.min_cell_voltage, self.ess_unit.cell_voltage_limits.min_soft_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_CELL_VOLTAGE_UNDER_LIMIT)
            capability = EssUnit.CAPABILITY_CHARGE_ONLY
            capability_reason = "Breached minimum cell voltage soft-limit. Battery min cell voltage:" \
                              "%s <= %s (soft-limit)" % (self.ess_unit.min_cell_voltage,
                                                         self.ess_unit.cell_voltage_limits.min_soft_limit)

        elif self.ess_unit.stack_soc <= self.ess_unit.stack_soc_limits.min_soft_limit:
            self.logger.warning("Breached minimum SOC soft-limit.")
            self.logger.info("Battery SOC: %s <= %s (soft-limit)" %
                         (self.ess_unit.stack_soc, self.ess_unit.stack_soc_limits.min_soft_limit))
            Alarm.occurrence(ALARM_ID_ESS_LIMITS_BATTERY_SOC_UNDER_LIMIT)
            capability = EssUnit.CAPABILITY_CHARGE_ONLY
            capability_reason = "Breached minimum SOC soft-limit.  Battery SOC: %s <= %s (soft-limit)" \
                              % (self.ess_unit.stack_soc, self.ess_unit.stack_soc_limits.min_soft_limit)

        self.ess_unit.transition_capability_to(capability, capability_reason)

if __name__ == '__main__':
    ess_limits_daemon = EssLimitsDaemon()
    ess_limits_daemon.run()
