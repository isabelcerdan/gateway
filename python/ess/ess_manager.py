"""
@module: ESS Manager

@description: Manager ESS daemons

@copyright: Apparent Inc. 2019

@reference:

@author: patrice

@created: April 20, 2018
"""
from multiprocessing import active_children

from daemon.daemon_monitor import DaemonMonitor
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_ESS_MANAGER_DAEMON_RESTARTED

from ess_controller import EssControllerDaemon
from ess_monitor import EssMonitorDaemon
from ess_model import *
from ess_data_logger import EssDataLoggerDaemon
from EVE.EveBmuDataPublisherFactory import EveBmuDataPublisherFactoryDaemon
from EVE.EveBmuDataSubscriberFactory import EveBmuDataSubscriberFactoryDaemon
from EVE.EveBmuDataAggregator import EveBmuDataAggregatorDaemon
from EVE.EveBmuStatusSubscriberFactory import EveBmuStatusSubscriberFactoryDaemon
from EVE.EveBmuControllerDaemon import EveBmuControllerDaemon
from EVE.AessMasterDataLogger import AessMasterDataLoggerDaemon
from Artesyn.ArtesynControllerFactoryDaemon import ArtesynControllerFactoryDaemon
#from Artesyn.ArtesynControllerDaemon import ArtesynControllerDaemon
from ess.ess_bmu_limits import EssBmuLimitsDaemon
from ess.apparent.discharger_controller import DischargerControllerFactoryDaemon
from ess_limits import EssLimitsDaemon
from EVE.AessBmuDataLogger import AessBmuDataLoggerDaemon
from Artesyn.ArtesynChassisRelayDaemon import ArtesynChassisRelayDaemon
from Artesyn.ArtesynChassisRelayTimeoutDaemon import ArtesynChassisRelayTimeoutDaemon
from EVE.BrokerMonitor import MsgBrokerMonitor
from ess.ess_unit import *


class EssManagerDaemon(Daemon):
    source_table = "ess_units"
    source_id_field = "id"
    daemon = False
    alarm_id = ALARM_ID_ESS_MANAGER_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(EssManagerDaemon, self).__init__(**kwargs)
        self.ess_unit = EssUnit.find_by(id=self.id)
        ess_daemon_map = {
            ESS_MODEL_ID_BYD_CESS_40: [
                EssControllerDaemon,
                EssMonitorDaemon,
                EssLimitsDaemon,
                EssDataLoggerDaemon
            ],
            ESS_MODEL_ID_BYD_CESS_400: [
                EssControllerDaemon,
                EssMonitorDaemon,
                EssLimitsDaemon,
                EssDataLoggerDaemon
            ],
            ESS_MODEL_ID_APPARENT_BATTERY: [
                EssControllerDaemon,
                EssMonitorDaemon,
                EssLimitsDaemon,
                EssDataLoggerDaemon
            ],
            ESS_MODEL_ID_APPARENT_EVE_ESS: [
                ArtesynChassisRelayDaemon,
                ArtesynChassisRelayTimeoutDaemon,
                ArtesynControllerFactoryDaemon,
                EveBmuDataPublisherFactoryDaemon,
                EveBmuDataSubscriberFactoryDaemon,
                EveBmuStatusSubscriberFactoryDaemon,
                EveBmuDataAggregatorDaemon,
                DischargerControllerFactoryDaemon,
                AessBmuDataLoggerDaemon,
                AessMasterDataLoggerDaemon,
                EssBmuLimitsDaemon,
                EssControllerDaemon,
                MsgBrokerMonitor
            ]
        }
        self.daemon_monitors = []
        for daemon_class in ess_daemon_map[self.ess_unit.model.id]:
            daemon_monitors = DaemonMonitor.find_or_create_all(
                daemon_class,
                parent_table='ess_units',
                parent_id=self.id
            )
            for d in daemon_monitors:
                d.save(enabled=self.enabled)
                self.daemon_monitors.append(d)
            self.update()

    def run(self):
        try:
            while True:
                for d in self.daemon_monitors:
                    d.monitor()
                self.update()
                active_children()
                time.sleep(self.polling_freq_sec)
        except Exception as ex:
            self.logger.exception("ESS manager just crashed...")
