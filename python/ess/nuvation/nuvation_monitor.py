import time
import requests
import json

from lib.db import prepared_act_on_database, EXECUTE
from lib.logging_setup import setup_logger


class NuvationMonitor(object):
    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs['ess_unit']
        self.ip_address = self.ess_unit.ip_address
        self.registers = [
            "4129",   "151553", "151569",  "4128",
            "65536",  "65537",  "147457",  "147473",
            "90112",  "90113",  "90114",   "90115",
            "28674",  "28676",  "28672",   "28675",
            "28673",  "139265", "139281",  "139313",
            "139329", "28690",  "28692",   "28688",
            "28691",  "28689",  "143361",  "143377",
            "143425", "143441", "131072",  "131077",
            "4592",   "200705"
        ]

    def get_run_time_parameters(self):
        pass

    def get_string_status(self):
        pass

    def monitor(self):
        callback = '{"method":"readRegisters","params":[%s],"id":0}' % ",".join(self.registers)
        callback = callback.replace('"', "%22")
        timestamp = int(time.time())
        qry = "?callback=&%s&_=%s" % (callback, timestamp)
        url = "http://%s/api.jsonp" % self.ip_address
        self.logger.debug(url)

        try:
            # nuvation server doesn't like when '{' and '}' are URL encoded
            s = requests.Session()
            req = requests.Request(method='GET', url=url)
            prep = req.prepare()
            prep.url = url + qry
            self.logger.debug(prep.url)
            response = s.send(prep)
            r = response.content
            self.logger.debug(r)

            r = r.replace("JSONP_Callback", "").replace("('", "").replace("')", "").replace(";", "")
            response = json.loads(r)
            self.logger.debug(response)
            battery_stack_voltage = response['result'][0] / 1000
            battery_stack_soc = response['result'][8]
            battery_stack_current = response['result'][3] / 1000
            battery_max_cell_voltage = response['result'][12] / 1000
            battery_min_cell_voltage = response['result'][14] / 1000
            battery_max_cell_temperature = response['result'][21]
            battery_min_cell_tempreature = response['result'][23]

            sql = "UPDATE ess_master_info SET battery_stack_voltage=%s, battery_stack_soc = %s, " \
                  "battery_stack_current= %s, battery_max_cell_voltage=%s, battery_min_cell_voltage=%s," \
                  "battery_max_cell_temperature=%s,battery_min_cell_tempreature= %s WHERE ess_id = %s" % \
                  ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")
            args = [battery_stack_voltage, battery_stack_soc, battery_stack_current, battery_max_cell_voltage,
                    battery_min_cell_voltage, battery_max_cell_temperature, battery_min_cell_tempreature,
                    self.ess_unit.id]
            prepared_act_on_database(EXECUTE, sql, args)
        except Exception as ex:
            self.logger.error("Error monitoring Nuvation BMS: %s" % repr(ex))

    def update_master_from_unit_work_state(self):
        pass

    def update_master_from_unit_cntrl_mode(self):
        pass

    def update_master_from_unit_work_mode(self):
        pass

    def update_master_from_reconnect_state(self):
        pass

    def update_master_from_battery_stack_voltage(self):
        pass

    def update_master_from_battery_stack_current(self):
        pass

    def update_master_from_battery_stack_power(self):
        pass

    def update_master_from_battery_stack_soc(self):
        pass

    def update_master_from_battery_stack_soh(self):
        pass

    def update_master_from_battery_max_cell_voltage(self):
        pass

    def update_master_from_battery_min_cell_voltage(self):
        pass

    def update_master_from_battery_max_cell_temperature(self):
        pass

    def update_master_from_battery_min_cell_temperature(self):
        pass

    def update_master_from_dc_voltage(self):
        pass

    def update_master_from_dc_current(self):
        pass

    def update_master_from_dc_power(self):
        pass

    def update_master_from_frequency(self):
        pass

    def update_master_from_power_factor(self):
        pass

    def update_master_from_work_state_control_cmd(self):
        pass

    def update_master_from_active_power_cmd(self):
        pass

    def update_master_from_reactive_power_cmd(self):
        pass

    def update_master_from_voltage_control_cmd(self):
        pass

    def update_master_from_frequency_control_cmd(self):
        pass


if __name__ == '__main__':
    m = NuvationMonitor(ip_address='192.168.10.186:8888')
    m.monitor()
