#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: ESS Monitor

@description: Continuously poll BMS for system values.

@copyright: Apparent Inc. 2018

@reference:

@author: steve

@created: February 20, 2018
"""
import zmq
from zmq.backend.cython.utils import ZMQError
import json

from lib.gateway_constants.BydCess400Constants import ConnectError, \
    DatabaseError, ParameterError, WriteError
from ess.nuvation.nuvation_monitor import NuvationMonitor
from ess.byd.BydCess40Monitor import BydCess40Monitor
from ess.byd.BydCess400Monitor import BydCess400Monitor
from ess.ess_daemon import EssDaemon
from alarm.alarm_constants import ALARM_ID_ESS_MONITOR_DAEMON_RESTARTED
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XPUB_PORT
from ess.ess_model import *

CESS_DEFAULT_POLLING_FREQ = 5
CESS_MAX_DB_ERRORS = 5


class EssMonitorDaemon(EssDaemon):
    polling_freq_sec = CESS_DEFAULT_POLLING_FREQ
    alarm_id = ALARM_ID_ESS_MONITOR_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(EssMonitorDaemon, self).__init__(**kwargs)
        self.client = None
        self.db_errors = 0
        self.num_strings = 0
        if self.ess_unit.model.id == ESS_MODEL_ID_BYD_CESS_400:
            self.delegate = BydCess400Monitor(ess_unit=self.ess_unit)
        elif self.ess_unit.model.id == ESS_MODEL_ID_APPARENT_BATTERY:
            self.delegate = NuvationMonitor(ess_unit=self.ess_unit)
        elif self.ess_unit.model.id == ESS_MODEL_ID_BYD_CESS_40:
            self.delegate = BydCess40Monitor(ess_unit=self.ess_unit)
        else:
            raise Exception("Unsupported ESS model id %s" % self.ess_unit.model.id)

        self.command_dict = {'refetch': self.delegate.get_run_time_parameters,
                             'string_status': self.delegate.get_string_status
                             }

    def run(self):
        self.update()

        # Get the run-time parameters.
        try:
            self.delegate.get_run_time_parameters()
        except Exception as ex:
            self.logger.error('Failed to get run-time parameters')
            raise

        try:
            zctx = zmq.Context()
            subscriber = zctx.socket(zmq.SUB)
            subscriber.connect("tcp://localhost:%s" % MSG_BROKER_XPUB_PORT)
            poller = zmq.Poller()
            poller.register(subscriber, zmq.POLLIN)
        except ZMQError:
            self.logger.error('Failed to instantiate ZMQ object')
            raise

        topic = "essid%d-monitor" % self.ess_unit.id
        subscriber.setsockopt(zmq.SUBSCRIBE, topic)

        """
        Main monitor loop.
        """
        while True:
            self.update()

            try:
                zsocks = dict(poller.poll(self.polling_freq_sec * 1000))
            except Exception as error:
                str_err = repr(error)
                self.logger.error(str_err)
                continue

            if subscriber in zsocks and zsocks[subscriber] == zmq.POLLIN:
                [topic, data] = subscriber.recv_multipart()
                if topic is None or data is None:
                    self.logger.info('Received a message with no topic or data frame.')
                    continue
                request = json.loads(data)
                command, parameter = request.split("|")
                try:
                    if parameter == 'None':
                        self.command_dict[command]()
                    else:
                        self.command_dict[command](parameter)
                except KeyError as error:
                    str_err = 'Invalid key: %s' % repr(error.message)
                    self.logger.error(str_err)
                    continue
                except (ConnectError, DatabaseError, ParameterError, WriteError) as error:
                    str_err = repr(error.message)
                    self.logger.error(str_err)
                    break
                except TypeError as error:
                    self.logger.error(error)
                    continue
                except Exception as error:
                    self.logger.error('Unable to process command - %s' % error)
                    continue

            try:
                self.delegate.monitor()
            except Exception as ex:
                break

        subscriber.close()
        zctx.term()

