import zmq
import time

from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
import lib.model as model
from lib.logging_setup import setup_logger
import json
from zmq.backend.cython.utils import ZMQError
from lib.gateway_constants.ArtesynConstants import ARTESYN_VREF_MAX
from lib.exceptions import ZmqError


class ArtesynChargerModule(model.Model):
    sql_table = "aess_charger_module"

    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs.get('ess_unit', None)
        self.charger_id = kwargs.get('charger_id', None)
        self.module_num = kwargs.get('module_num', None)
        self.device_id = kwargs.get('device_id', None)
        self.model_num = kwargs.get('model_num', None)
        self.chassis_id = kwargs.get('chassis_id', None)
        self.pcs_unit_id = kwargs.get('pcs_unit_id', None)
        self.vref = ARTESYN_VREF_MAX
        self.pub_socket = None
        self.ctx = None
        self.topic = 'chassis%d#' % self.chassis_id
        '''
            @note: When setting up the publisher socket, set the LINGER option
            to zero, to avoid having the process hanging around
            trying to deliver messages after it's socket has been closed.
            
            Set the high water mark for the publisher socket to 100
            messages, to avoid gobbling up memory if one or more of the
            subscriber processes fails and is not restarted.
        '''
        self.ctx = None            
        self.pub_socket = None

    def set_active_power(self, active_power):
        """
            negative active power means charge; multiply by 1000 to convert from kW to W

            limit oc_level to 45A
            limit vref to MAX_VREF
        :param active_power: +discharge/-charge in kW
        :return:
        """

        '''
        if active_power >= 0:
            self.send(["operation_module", str(self.chassis_id), str(self.module_num), ARTESYN_ISOCOMM_DISABLE_PARAM])
        else:
            self.send(["operation_module", str(self.chassis_id), str(self.module_num), ARTESYN_ISOCOMM_ENABLE_PARAM])
        '''
        active_power = -active_power * 1000     # convert kW to W
        oc_level = active_power / self.vref
        oc_level = max(oc_level, 0.0)
        self.send(["set_output_current_level", str(self.module_num), str(oc_level)])
        self.logger.debug('Sending set_output_current_level = %f for %s/%s...' % (float(oc_level),
                          str(self.module_num), self.topic))

    def start(self):
        pass
        '''
        self.send(["set_vref", str(self.chassis_id), str(self.module_num), str(self.vref)])
        self.send(["set_output_current_level", str(self.module_num)])
        '''

    def send(self, cmd):
        """
        :param cmd: a list of strings starting with command followed by parameters.
        :return:
        """
        if self.ctx is None:
            try:
                self.ctx = zmq.Context(1)            
            except ZMQError as error:
                self.logger.exception(error)
                raise ZmqError(error)
            try:
                self.pub_socket = self.ctx.socket(zmq.PUB)
            except ZMQError as error:
                self.ctx.term()
                self.logger.exception(error)
                raise ZmqError(error)
            self.pub_socket.hwm = 100
            self.pub_socket.setsockopt(zmq.LINGER, 0)
            try:
                self.pub_socket.connect(MSG_BROKER_XSUB_URL)
                time.sleep(PUB_CONNECT_WAIT_TIME)
            except ZMQError as error:
                self.logger.exception(error)
                self.pub_socket.close()
                self.ctx.term()
                raise ZmqError(error)
        
        try:
            cmd_str = "|".join([cmd[0], " ".join(cmd[1:])])
            self.pub_socket.send_multipart([self.topic, json.dumps(cmd_str)])
        except ZMQError as error:
            self.logger.exception(error)
            self.pub_socket.close()
            self.ctx.term()
            raise ZmqError(error)
        except Exception as error:
            self.logger.exception(error)
            self.pub_socket.close()
            self.ctx.term()
            raise ZmqError(error)


