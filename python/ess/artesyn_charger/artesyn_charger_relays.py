"""
@module:      Artesyn Charger Controller

@description: (Very) basic controller for the Artesyn charger unit, limited to
              controlling the on/off state of the individual charger modules
              using relays.

              In effect, charging control is limited to a step function, from
              all off, to one/two/three/all on.

@copyright:   Apparent Inc. 2018

@author:      steve

@created:     January 26, 2018
"""
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Defaults
from lib.logging_setup import *
from lib.helpers import minmax
from lib.model import Model

ARTESYN_MODBUS_PORT = 502
ARTESYN_MODBUS_UNIT = 1
ARTESYN_MAX_MODBUS_ERRORS = 10
ARTESYN_MODBUS_DELAY_TIME = 1.0
ARTESYN_MAX_MODULES = 4

ARTESYN_RELAY_0_REG_ADDR = 0x0
ARTESYN_RELAY_1_REG_ADDR = 0x1
ARTESYN_RELAY_2_REG_ADDR = 0x2
ARTESYN_RELAY_3_REG_ADDR = 0x3
ARTESYN_RELAY_4_REG_ADDR = 0x4
ARTESYN_RELAY_5_REG_ADDR = 0x5

relay_number_to_address = [ARTESYN_RELAY_0_REG_ADDR,
                           ARTESYN_RELAY_1_REG_ADDR,
                           ARTESYN_RELAY_2_REG_ADDR,
                           ARTESYN_RELAY_3_REG_ADDR,
                           ARTESYN_RELAY_4_REG_ADDR,
                           ARTESYN_RELAY_5_REG_ADDR]
MAX_DB_ERRORS = 5


class ArtesynChargerRelays(Model):
    sql_table = "artesyn_charger_relays"

    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.modbus_ip_addr = kwargs.get('ip_addr', None)
        self.modbus_unit = kwargs.get('modbus_unit', -1)
        self.module_relay_list = [
            kwargs.get('module_0_relay', 0),
            kwargs.get('module_1_relay', 0),
            kwargs.get('module_2_relay', 0),
            kwargs.get('module_3_relay', 0),
        ]
        self.module_status = [None, None, None, None]
        self.client = None

    def client_connect(self):
        """
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of the caller of this
               function to close the connection.
        """
        modbus_errs = 0
        Defaults.Timeout = 0.75
        while True:
            try:
                self.client = ModbusClient(self.modbus_ip_addr, port=ARTESYN_MODBUS_PORT)
            except ValueError as error:
                modbus_errs += 1
                err_string = "Unable to create MODbus client at Artysen ip address %s - %s" % \
                             (ARTESYN_MODBUS_UNIT, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= ARTESYN_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    raise Exception('Maximum MODbus errors exceeded trying to connect')
                continue
            break

        modbus_errs = 0
        while True:
            if not self.client.connect():
                err_string = "MODbus connect failed for Artysen charger unit at %s" % self.modbus_ip_addr
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= ARTESYN_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus connection errors exceeded.')
                    self.client.close()
                    raise Exception('Maximum MODbus errors exceeded trying to connect')
                continue
            break
        return True

    def write_relay_register(self, relay_num=None, value=None):
        """
        Perform a MODbus write_coil to set/reset a single relay on the PET-7060.
        """
        if value is None:
            raise Exception('Missing charger module setting')

        self.client_connect()

        try:
            self.client.write_coil(relay_number_to_address[relay_num], value, unit=self.modbus_unit)
        except Exception as ex:
            raise ex
        finally:
            self.client.close()

    def set_charger_module(self, mod, value):
        try:
            if self.module_status[mod] != value:
                self.write_relay_register(self.module_relay_list[mod], value)
            self.module_status[mod] = value
            self.save()
        except Exception as ex:
            self.logger.exception("Unable to write relay register on charger.")

    def activate_all_charger_modules(self):
        for mod in range(0, ARTESYN_MAX_MODULES):
            self.set_charger_module(mod, True)

    def deactivate_all_charger_modules(self):
        for mod in range(0, ARTESYN_MAX_MODULES):
            self.set_charger_module(mod, False)

    CHARGE_PER_RELAY = 1.2  # kW AC

    def set_active_power(self, active_power):
        """
        :param active_power:   (+discharge/-charge)
        :return:
        """

        charge_power = -float(active_power)
        charge_power = minmax(0, charge_power, ArtesynChargerRelays.CHARGE_PER_RELAY * ARTESYN_MAX_MODULES)

        self.logger.debug("charge_power=%s" % charge_power)
        for mod in range(0, ARTESYN_MAX_MODULES):
            if charge_power >= ArtesynChargerRelays.CHARGE_PER_RELAY:
                self.logger.debug("activate_charger_module(mod=%s)" % mod)
                self.set_charger_module(mod, True)
                charge_power -= ArtesynChargerRelays.CHARGE_PER_RELAY
            else:
                self.logger.debug("deactivate_charger_module(mod=%s)" % mod)
                self.set_charger_module(mod, False)

    def start(self):
        pass

    def stop(self):
        self.deactivate_all_charger_modules()
