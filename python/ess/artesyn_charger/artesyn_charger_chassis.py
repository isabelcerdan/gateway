"""
@module: artesyn_charger_chassis.py

@description:

@copyright: Apparent Energy Inc. 2018

@created:

@author: patrice
"""
import zmq
import time
import json

from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.logging_setup import setup_logger
from lib.db import prepared_act_on_database, FETCH_ALL
from lib.model import Model
from zmq.backend.cython.utils import ZMQError
from lib.exceptions import ZmqError
from lib.helpers import merge_dicts

class ArtesynChargerChassis(Model):
    sql_table = "aess_charger_chassis"
    id_field = "chassis_id"

    REQUEST_RETRIES = 3
    REQUEST_TIMEOUT = 5000

    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs.get('ess_unit', None)
        self.chassis_id = kwargs.get('chassis_id', None)
        self.model_num = kwargs.get('model_num', None)
        self.status = kwargs.get('status', None)
        self.ip_addr = kwargs.get('ip_addr', None)
        self.mac_addr = kwargs.get('mac_addr', None)
        self.rack_id = kwargs.get('rack_id', None)
        self.topic = 'chassis%d#' % self.chassis_id
        self.ctx = None
        self.pub_socket = None
        self.powered_up = False 
    
    def start(self):
        """
        self.logger.info('Sending power-up command to chassis %d...' % self.chassis_id)
        self.send(["power_up", str(self.chassis_id)])
        self.powered_up = True
        """
        pass
        
    def stop(self):
        """
        self.logger.info('Sending power-down command to chassis %d...' % self.chassis_id)
        self.send(["power_down", str(self.chassis_id)])
        self.powered_up = False
        """
        pass
    
    def enable(self):
        """
        self.send(["operation", str(self.chassis_id), "enable"])
        self.send(["operation_module_all", str(self.chassis_id), "enable"])
        """
        pass
    
    def disable(self):
        """
        self.send(["operation", str(self.chassis_id), "disable"])
        self.send(["operation_module_all", str(self.chassis_id), "disable"])
        """
        pass
    
    def send(self, cmd=None):
        """
        :param cmd: a list of strings starting with command followed by parameters.
        :return
        """
        if self.ctx is None:
            try:
                """
                    @note: When setting up the publisher socket, set the LINGER option
                    to zero, to avoid having the process hanging around
                    trying to deliver messages after it's socket has been closed.
                    
                    Set the high water mark for the publisher socket to 100
                    messages, to avoid gobbling up memory if one or more of the
                    subscriber processes fails and is not restarted.
                """
                self.ctx = zmq.Context(1)            
            except ZMQError as error:
                self.logger.exception(error)
                raise
            try:
                self.pub_socket = self.ctx.socket(zmq.PUB)
            except ZMQError as error:
                self.ctx.term()
                self.logger.exception(error)
                raise ZmqError(error)
            self.pub_socket.hwm = 100
            self.pub_socket.setsockopt(zmq.LINGER, 0)
            try:
                self.pub_socket.connect(MSG_BROKER_XSUB_URL)
                time.sleep(PUB_CONNECT_WAIT_TIME)
            except ZMQError as error:
                self.logger.exception(error)
                self.pub_socket.close()
                self.ctx.term()
                raise

        cmd_dict = {}
        try:
            cmd_dict[cmd[0]] = " ".join(cmd[1:])
            self.logger.debug('Sending %s - %s' % (self.topic, repr(cmd_dict)))
            self.pub_socket.send_multipart([self.topic, json.dumps(cmd_dict)])
        except ZMQError as error:
            self.logger.exception(error)
            self.pub_socket.close()
            self.ctx.term()
            raise
        except TypeError as error:
            self.logger.exception(error)
            self.pub_socket.close()
            self.ctx.term()
            raise

    @classmethod
    def find_all_by(cls, **kwargs):
        """
        Find all charger chassis tied to the specified ESS unit.
        :param kwargs:
        :return:
        """
        ess_unit = kwargs.get('ess_unit', None)
        sql = "SELECT acc.* FROM aess_charger_chassis acc " \
              "INNER JOIN aess_charger_module acm ON acm.chassis_id = acc.chassis_id " \
              "INNER JOIN pcs_units pcs ON pcs.id = acm.pcs_unit_id " \
              "INNER JOIN ess_units ess ON ess.id = pcs.ess_unit_id " \
              "WHERE ess.id = %s" % "%s"
        results = prepared_act_on_database(FETCH_ALL, sql, [ess_unit.id])
        models = []
        for result in results:
            result = merge_dicts(result, kwargs)
            models.append(ArtesynChargerChassis(**result))

        return models
