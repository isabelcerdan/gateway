#!/usr/share/apparent/.py2-virtualenv/bin/python"""
"""
@module: ESS BMU Limits 

@description: ESS BMU Limits monitors the aess_bmu_data table, looking for values exceeding
              the configured limits for the BMUs.  If found, the 'state' field for the BMU
              is updated to {'Fault', 'Discharge rtc', 'Charged rtd'} as appropriate.

@copyright: Apparent Inc. 2018

@author: steve

@created: November 2, 2018
"""
from ess.ess_daemon import EssDaemon
from ess.apparent.pcs_unit import PcsUnit


ESS_LIMITS_DEFAULT_POLLING_SECONDS = 1.0  # 25 ms


class EssBmuLimitsDaemon(EssDaemon):
    polling_freq_sec = ESS_LIMITS_DEFAULT_POLLING_SECONDS

    def __init__(self, **kwargs):
        super(EssBmuLimitsDaemon, self).__init__(**kwargs)
        # pass in ESS unit object so that it only needs to be reloaded once per call to run daemon method
        self.pcs_units = PcsUnit.find_all_by(ess_unit_id=self.ess_unit.id, extra={'ess_unit': self.ess_unit})

    NOMINAL_CHARGE = -0.100
    NOMINAL_DISCHARGE = 0.100

    def update_capability(self):
        """
        Update the ESS unit capability based on the PCS units available.
        :return:
        """
        available_charge_units, _ = PcsUnit.units_available_for(self.pcs_units, self.NOMINAL_CHARGE)
        available_discharge_units, _ = PcsUnit.units_available_for(self.pcs_units, self.NOMINAL_DISCHARGE)

        new_capability = None
        if len(available_charge_units) > 0:
            if len(available_discharge_units) > 0:
                new_capability = self.ess_unit.CAPABILITY_UNRESTRICTED
            else:
                new_capability = self.ess_unit.CAPABILITY_CHARGE_ONLY
        else:
            if len(available_discharge_units) > 0:
                new_capability = self.ess_unit.CAPABILITY_DISCHARGE_ONLY

        if new_capability is not None:
            self.ess_unit.transition_capability_to(new_capability)

    def work(self):
        self.ess_unit.reload()
        self.ess_unit.reload_master_info()

        for pcs_unit in self.pcs_units:
            pcs_unit.reload()
            pcs_unit.check_bmu_limits()

        self.update_capability()

