from lib.logging_setup import setup_logger
from lib.db import prepared_act_on_database, EXECUTE
from lib.gateway_constants.DBConstants import DB_ESS_MASTER_INFO_TABLE


class EssDriver(object):
    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs['ess_unit']

    def set_work_state_stop(self):
        raise Exception("Method must be overridden in subclass.")

    def set_work_state_run(self):
        raise Exception("Method must be overridden in subclass.")

    def set_active_power(self, active_power=None):
        raise Exception("Method must be overridden in subclass.")

    def set_reactive_power(self, power=None):
        raise Exception("Method must be overridden in subclass.")

    def set_voltage_control(self, voltage=None):
        raise Exception("Method must be overridden in subclass.")

    def set_frequency_control(self, frequency=None):
        raise Exception("Method must be overridden in subclass.")

    def set_work_state_fault(self):
        self.set_work_state_stop()
        self.save(unit_work_state="Fault")

    def set_battery_mode(self, mode=None):
        raise Exception("Method must be overridden in subclass.")

    def save(self, **kwargs):
        keys, args = [], []
        for k, v in kwargs.iteritems():
            keys.append(k)
            args.append(v)
        params = ", ".join(["%s=%s" % (k, "%s") for k in keys])
        sql = "UPDATE %s SET %s" % (DB_ESS_MASTER_INFO_TABLE, params)
        sql += " WHERE ess_id=%s" % "%s"
        args.append(self.ess_unit.id)
        prepared_act_on_database(EXECUTE, sql, args)


