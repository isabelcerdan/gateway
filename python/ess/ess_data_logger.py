#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: BydCess400DataLogger.py

@description: Read the current database values for selected fields of the
              master info table and write them to a circular database file.  

@copyright: Apparent Inc.  2018

@created: March 20, 2018

@author: steve
"""

from datetime import datetime
from lib.db import *
from alarm.alarm_constants import ALARM_ID_ESS_DATA_LOGGER_RESTARTED
from lib.gateway_constants.BydCess400Constants import DatabaseError
from ess.ess_daemon import EssDaemon

DB_ERR_MAX = 5
CESS_DEFAULT_POLLING_FREQ_SEC = 5.0
CESS_DEFAULT_LOG_CYCLE = 2592000
CESS_MAX_DB_ERRORS = 5
CESS_UPDATE_INTERVAL = 5

master_log_string_fields = [CESS_400_MASTER_LOG_UNIT_WORK_STATE_COLUMN_NAME,
                            CESS_400_MASTER_LOG_UNIT_CTRL_MODE_COLUMN_NAME,
                            CESS_400_MASTER_LOG_UNIT_WORK_MODE_COLUMN_NAME,
                            CESS_400_MASTER_LOG_RECONNECT_STATE_COLUMN_NAME,
                            CESS_400_MASTER_LOG_WORK_STATE_CONTROL_CMD_COLUMN_NAME,
                            CESS_400_MASTER_LOG_TIMESTAMP_COLUMN_NAME]

master_info_select_statement = "SELECT %s, %s, %s, %s, %s, %s, %s, %s, " \
                               "%s, %s, %s, %s, %s, %s, %s, %s, " \
                               "%s, %s, %s, %s, %s, %s, %s, %s, " \
                               "%s, %s, %s, %s, %s, %s, %s, %s from %s;" % \
                               (ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME,
                                ESS_MASTER_INFO_UNIT_CTRL_MODE_COLUMN_NAME,
                                ESS_MASTER_INFO_UNIT_WORK_MODE_COLUMN_NAME,
                                ESS_MASTER_INFO_RECONNECT_STATE_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STACK_VOLTAGE_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STACK_CURRENT_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STACK_POWER_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STACK_SOH_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_MIN_CELL_TEMPREATURE_COLUMN_NAME,
                                ESS_MASTER_INFO_DC_VOLTAGE_COLUMN_NAME,
                                ESS_MASTER_INFO_DC_CURRENT_COLUMN_NAME,
                                ESS_MASTER_INFO_DC_POWER_COLUMN_NAME,
                                ESS_MASTER_INFO_FREQUENCY_COLUMN_NAME,
                                ESS_MASTER_INFO_POWER_FACTOR_COLUMN_NAME,
                                ESS_MASTER_INFO_WORK_STATE_CONTROL_CMD_COLUMN_NAME,
                                ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME,
                                ESS_MASTER_INFO_REACTIVE_POWER_CMD_COLUMN_NAME,
                                ESS_MASTER_INFO_VOLTAGE_CONTROL_CMD_COLUMN_NAME,
                                ESS_MASTER_INFO_FREQUENCY_CONTROL_CMD_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_1_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_2_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_3_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_4_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_5_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_6_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_7_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_BATTERY_STRING_8_SOC_COLUMN_NAME,
                                ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME,
                                DB_ESS_MASTER_INFO_TABLE)


class EssDataLoggerDaemon(EssDaemon):
    polling_freq_sec = CESS_DEFAULT_POLLING_FREQ_SEC
    alarm_id = ALARM_ID_ESS_DATA_LOGGER_RESTARTED

    def __init__(self, **kwargs):
        super(EssDataLoggerDaemon, self).__init__(**kwargs)
        self.log_cycle = CESS_DEFAULT_LOG_CYCLE
        self.next_heartbeat = datetime.now()
        self.db_errors = 0

    def work(self):
        args = []
        registers = prepared_act_on_database(FETCH_ALL, master_info_select_statement, args)
        register_value_dict = registers[0]

        replace_list = ['REPLACE INTO %s SET %s = (SELECT MOD(COALESCE(MAX(%s), 0), %d)' 
                        ' + 1 FROM %s AS t), ' %
                        (DB_BYD_CESS_400_MASTER_DATA_LOG_TABLE,
                         CESS_400_MASTER_LOG_ROW_ID_COLUMN_NAME,
                         CESS_400_MASTER_LOG_ENTRY_ID_COLUMN_NAME,
                         self.log_cycle,
                         DB_BYD_CESS_400_MASTER_DATA_LOG_TABLE)]
        set_list = []
        for register_name, register_value in register_value_dict.iteritems():
            if register_name in master_log_string_fields:
                set_list.append('%s="%s"' % (register_name, register_value))
            else:
                set_list.append('%s=%s' % (register_name, register_value))
            set_list.append(', ')
        set_list.pop()  # pop the final comma
        set_list.append(';')
        replace_list.append(''.join(set_list))
        replace_string = ''.join(replace_list)
        args = []
        try:
            prepared_act_on_database(EXECUTE, replace_string, args)
        except Exception as error:
            err_string = 'Unable to update %s - %s' % (DB_BYD_CESS_400_MASTER_DATA_LOG_TABLE, repr(error))
            self.logger.error(err_string)
            self.db_errors += 1
            if self.db_errors > CESS_MAX_DB_ERRORS:
                raise DatabaseError('Maximum database errors exceeded.')
            return False
        return True
