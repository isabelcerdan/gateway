from lib.model import Model

ESS_MODEL_ID_BYD_CESS_400 = 1
ESS_MODEL_ID_APPARENT_BATTERY = 2
ESS_MODEL_ID_BYD_CESS_40 = 3
ESS_MODEL_ID_APPARENT_EVE_ESS = 4


class EssModel(Model):
    sql_table = "ess_models"

    def __init__(self, **kwargs):
        self.id = int(kwargs.get('id', 0))
        self.manufacturer = kwargs.get('manufacturer', None)
        self.model = kwargs.get('model', None)
        self.charge_limit = kwargs.get('charge_limit', None)
        self.discharge_limit = kwargs.get('discharge_limit', None)
        self.feed_control = kwargs.get('feed_control', None)

    def full_name(self):
        return " ".join([self.manufacturer, self.model])
