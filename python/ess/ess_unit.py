import zmq
import json
import time
from multiprocessing import Lock
from zmq.backend.cython.utils import ZMQError

import lib.model
from lib.db import *
from ess.ess_model import EssModel
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import setup_logger
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from ess.apparent.bmu_data import BmuData

REQUEST_TIMEOUT = 200
REQUEST_RETRIES = 3

ESS_MODEL_ID_BYD_CESS_400 = 1
ESS_MODEL_ID_APPARENT_BATTERY = 2
ESS_MODEL_ID_BYD_CESS_40 = 3
ESS_MODEL_ID_APPARENT_EVE_ESS = 4


class OperatingLimits(object):
    def __init__(self, **kwargs):
        self.min_soft_limit = None
        self.min_hard_limit = None
        self.max_hard_limit = None
        self.reload(**kwargs)

    def reload(self, **kwargs):
        self.min_soft_limit = kwargs.get('min_soft_limit', None)
        self.min_hard_limit = kwargs.get('min_hard_limit', None)
        self.max_hard_limit = kwargs.get('max_hard_limit', None)


class EssUnit(lib.model.Model):
    sql_table = "ess_units"
    id_field = "id"

    BATTERY_MODE_AUTO = 'auto'
    BATTERY_MODE_MANUAL = 'manual'

    CAPABILITY_UNRESTRICTED = "Unrestricted"
    CAPABILITY_CHARGE_ONLY = "Charge only"
    CAPABILITY_DISCHARGE_ONLY = "Discharge only"
    CAPABILITY_FAULT = "Fault"

    BATTERY_STRATEGY_SETPOINT = 'setpoint'
    BATTERY_STRATEGY_VARIABLE_CHARGE = 'variable_charge'
    BATTERY_STRATEGY_VARIABLE_DISCHARGE = 'variable_discharge'

    SOC_SOFT_LIMIT_OFFSET = 0.01
    CELL_VOLTAGE_SOFT_LIMIT_OFFSET = 0.1
    locks = {}

    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.name = kwargs.get('name', None)
        model_id = kwargs.get('model_id', None)
        self.model = EssModel.find_by(id=model_id) if model_id else None
        if self.id not in EssUnit.locks:
            EssUnit.locks[self.id] = Lock()
        self.lock = EssUnit.locks[self.id]
        self.feed_name = 'Aggregate'
        self.ip_address = kwargs.get('ip_address', None)
        self.mac_address = kwargs.get('mac_address', None)
        self.mode = kwargs.get('mode', EssUnit.BATTERY_MODE_MANUAL)
        self.fixed_charge_rate = kwargs.get('fixed_charge_rate', None)
        self.fixed_discharge_rate = kwargs.get('fixed_discharge_rate', None)
        self.fixed_charge_soc_stop_limit = kwargs.get('fixed_charge_soc_stop_limit', None)
        self.fixed_discharge_soc_stop_limit = kwargs.get('fixed_discharge_soc_stop_limit', None)
        self.enabled = kwargs.get('enabled', False)
        self.port = kwargs.get('port', None)
        self.charge_rate_limit = kwargs.get('charge_rate_limit', None)
        self.discharge_rate_limit = kwargs.get('discharge_rate_limit', None)
        self.soc_charge_limit = kwargs.get('soc_charge_limit', 0.99)
        self.soc_charge_limit_response = kwargs.get('soc_charge_limit_response', None)
        self.soc_discharge_limit = kwargs.get('soc_discharge_limit', 0.01)
        self.soc_discharge_limit_response = kwargs.get('soc_discharge_limit_response', None)
        self.battery_max_cell_voltage_limit = kwargs.get('battery_max_cell_voltage_limit', 4.0)
        self.battery_max_cell_voltage_limit_response = kwargs.get('battery_max_cell_voltage_limit_response', None)
        self.battery_min_cell_voltage_limit = kwargs.get('battery_min_cell_voltage_limit', 2.9)
        self.battery_min_cell_voltage_limit_response = kwargs.get('battery_min_cell_voltage_limit_response', None)
        self.auto_shutdown_behavior = kwargs.get('auto_shutdown_behavior', 1) == 1
        
        self.charge_priority = kwargs.get('charge_priority', 1)
        self.discharge_priority = kwargs.get('discharge_priority', 1)

        # preserve operational_active_power during reload
        if not hasattr(self, 'operational_active_power'):
            self.operational_active_power = 0
        
        self.cell_voltage_limits = OperatingLimits()
        self.stack_soc_limits = OperatingLimits()
        min_hard_limit = self.battery_min_cell_voltage_limit
        self.cell_voltage_limits.reload(
            min_hard_limit=min_hard_limit,
            min_soft_limit=(min_hard_limit + EssUnit.CELL_VOLTAGE_SOFT_LIMIT_OFFSET),
            max_hard_limit=self.battery_max_cell_voltage_limit)

        min_hard_limit = max(0.0, self.soc_discharge_limit)
        self.stack_soc_limits.reload(
            min_hard_limit=min_hard_limit,
            min_soft_limit=min_hard_limit + EssUnit.SOC_SOFT_LIMIT_OFFSET,
            max_hard_limit=min(1.0, self.soc_charge_limit))

        self.battery_max_cell_temperature_limit = kwargs.get(ESS_UNITS_TABLE_MAX_CELL_TEMPERATURE_COLUMN_NAME, None)
        self.battery_max_mosfet_temperature_limit = kwargs.get(ESS_UNITS_TABLE_MAX_MOSFFET_TEMPERATURE_COLUMN_NAME, None)
        self.battery_max_environment_temperature_limit = kwargs.get(ESS_UNITS_TABLE_MAX_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, None)

        self.active_power = None
        self.stack_soc = None
        self.unit_work_state = None
        self.min_cell_voltage = None
        self.max_cell_voltage = None
        self.capability = None
        self.capability_reason = None
        self.max_mosfet_temperature = None
        self.battery_max_cell_temperature = None
        self.max_environment_temperature = None
        self.startup_delay_active = False
        self.step_percent = kwargs.get('step_percent', 3.0)
        self.bmu_data = None
        self.reload_master_info()

        self.logger = setup_logger(__name__)
        self.restart_pending = False
        self.chassis_list = []
        
        ''' Cruft required to talk directly to the ArtesynController '''
        self.context = None
        self.pub_socket = None
        
    def reload(self, **kwargs):
        old_mode = self.mode
        sql = "SELECT * FROM ess_units WHERE id = %s" % "%s"
        result = prepared_act_on_database(FETCH_ONE, sql, [self.id])
        self.__init__(**result)
        if self.mode != old_mode:
            self.handle_mode_transition(old_mode)

    def handle_mode_transition(self, old_mode):
        """
        If transitioning from manual mode to auto mode, ensure operational
        active power is set to active power that was being used in manual
        mode. This addresses an issue where a stale operational active power
        was used when auto mode was re-engaged.

        Also, turn off auto shutdown behavior when in auto mode since it
        creates a delay in charge demanded and actual charge seen which
        throws off the battery regulator timing.

        :param old_mode:
        :return:
        """
        if self.mode != self.BATTERY_MODE_MANUAL and old_mode == self.BATTERY_MODE_MANUAL:
            self.operational_active_power = self.active_power
            self.auto_shutdown_behavior = 0
            self.save(auto_shutdown_behavior=self.auto_shutdown_behavior)

        if self.mode == self.BATTERY_MODE_MANUAL and old_mode != self.BATTERY_MODE_MANUAL:
            self.auto_shutdown_behavior = 1
            self.save(auto_shutdown_behavior=self.auto_shutdown_behavior)

    def reload_master_info(self):
        sql = "SELECT * FROM %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE, 'ess_id', "%s")
        result = prepared_act_on_database(FETCH_ONE, sql, [self.id])
        if result is None:
            raise Exception("No ess_master_info record found for ess_id = %s" % self.id)
        self.active_power = result.get(ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME, 0)
        self.stack_soc = result.get(ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME, -1) / 100.0
        self.unit_work_state = result.get(ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME, "Stop")
        self.min_cell_voltage = result.get(ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME, None)
        self.max_cell_voltage = result.get(ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME, None)
        self.capability = result.get(ESS_MASTER_INFO_TABLE_CAPABILITY_COLUMN_NAME, None)
        self.capability_reason = result.get(ESS_MASTER_INFO_TABLE_CAPABILITY_REASON_COLUMN_NAME, None)
        self.battery_max_cell_temperature = result.get(ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME, None)
        self.max_mosfet_temperature = result.get(ESS_MASTER_INFO_TABLE_MAX_MOSFET_TEMPERATURE_COLUMN_NAME, None)
        self.max_environment_temperature = result.get(ESS_MASTER_INFO_TABLE_MAX_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, None)
        self.startup_delay_active = result.get(ESS_MASTER_INFO_TABLE_STARTUP_DELAY_ACTIVE_COLUMN_NAME, False)

    def get_bmu_list(self):
        sql = "SELECT bmu_id FROM aess_battery_module_unit bmu " \
              "INNER JOIN pcs_units pcs ON pcs.bmu_id_A = bmu.bmu_id OR pcs.bmu_id_B = bmu.bmu_id " \
              "INNER JOIN ess_units ess ON ess.id = pcs.ess_unit_id " \
              "WHERE ess.id = %s" % "%s"
        results = prepared_act_on_database(FETCH_ALL, sql, [self.id])
        if results:
            return [r['bmu_id'] for r in results]
        else:
            return []

    def get_bmu_ids(self):
        return ", ".join([str(bmu_id) for bmu_id in self.get_bmu_list()])

    def get_bmu_data(self):
        if self.bmu_data is None:
            self.bmu_data = BmuData.find_by_ess_unit(self)
        return self.bmu_data

    def is_running(self):
        return self.unit_work_state == "Running"

    def is_stopped(self):
        return self.unit_work_state == "Stopped" or \
               self.unit_work_state == "Stop"

    def is_standby(self):
        return self.unit_work_state == "Standby"

    def start(self):
        self.logger.debug('Sending Controller RUN command...')
        self._send_controller_cmd("run")

    def stop(self):
        return self._send_controller_cmd("stop")

    def send_fault(self):
        return self._send_controller_cmd("fault")

    def is_auto_mode(self):
        return self.mode != self.BATTERY_MODE_MANUAL

    def is_capable_of(self, active_power):
        """
        Check if the ESS Unit is capable of the given active power.
        For example, if the ESS Unit is charged beyond its SOC charge
        limit, then it is capable of "Charge only".
        :param active_power:
        :return:
        """
        if self.capability == self.CAPABILITY_UNRESTRICTED:
            return True
        elif self.capability == self.CAPABILITY_CHARGE_ONLY:
            return float(active_power) <= 0.0
        elif self.capability == self.CAPABILITY_DISCHARGE_ONLY:
            return float(active_power) >= 0.0
        elif self.capability == self.CAPABILITY_FAULT:
            return False

    def fulfillment_capability(self, battery_strategy, requested_active_power):
        """
        How much of the requested active power can this ESS unit fulfill? This
        is primarily based on the ESS unit charge and discharge rate limits.
        :param battery_strategy:
        :param requested_active_power:
        :return: active power that can be fulfilled.
        """
        if battery_strategy == self.BATTERY_STRATEGY_SETPOINT:
            active_power = max(min(requested_active_power, self.discharge_rate_limit), self.charge_rate_limit)
        elif battery_strategy == self.BATTERY_STRATEGY_VARIABLE_CHARGE:
            active_power = max(min(requested_active_power, 0), self.charge_rate_limit)
        elif battery_strategy == self.BATTERY_STRATEGY_VARIABLE_DISCHARGE:
            active_power = min(max(requested_active_power, 0), self.discharge_rate_limit)
        else:
            active_power = 0

        return active_power

    def fault_detected(self):
        return self.capability == "Fault"

    def actively_charging(self):
        bmu_ids = [str(b) for b in self.get_bmu_list()]
        sql = "SELECT * FROM aess_bmu_data WHERE bmu_id IN (%s)" % ",".join(bmu_ids)
        results = prepared_act_on_database(FETCH_ALL, sql, [])
        if results is not None:
            for result in results:
                if result['current'] > 0.0:
                    return True
        return False

    def set_active_power(self, active_power):
        # attempt to send active_power to battery controller daemon
        if self._send_controller_cmd('active_power', active_power):
            self.logger.debug("Sent \"active_power=%s\"" % active_power)
            self.active_power = active_power
            return active_power
        else:
            self.logger.warning("Failure sending active_power command to controller: %s" % active_power)
            return 0

    def _send_controller_cmd(self, cmd_str=None, param=None):
        topic = "essid%d-controller" % self.id
        return self._send(topic, cmd_str, param)

    def _send(self, topic, cmd_str=None, param=None):
        success = True
        param_str = str(param)
        cmd = "|".join([cmd_str, param_str])

        if self.context is None:
            try:
                self.context = zmq.Context(1)
            except ZMQError as error:
                self.logger.exception(error)
                return False

        pub_socket = self.context.socket(zmq.PUB)
        pub_socket.setsockopt(zmq.LINGER, 0)
        pub_socket.hwm = 100
        pub_socket.connect(MSG_BROKER_XSUB_URL)

        time.sleep(PUB_CONNECT_WAIT_TIME)

        self.logger.debug('Sending %s|%s...' % (topic, repr(cmd)))
        pub_socket.send_multipart([topic, json.dumps(cmd)])
        pub_socket.close()

        return success

    def step_size(self):
        """
        The step size is used in ramping charge/discharge setting up/down
        for a given ESS unit when balancing solar and battery resources.
        :return: float value adjustment representing active power in kW.
        """
        rate_limit = min(abs(self.charge_rate_limit), abs(self.discharge_rate_limit))
        return rate_limit * (self.step_percent / 100.0)

    def try_active_power_delta(self, battery_strategy, requested_delta):
        """
        Apply a delta active power to this ESS unit. Each ESS unit manages an
        operational active power that a delta active power can be applied to.

        :param battery_strategy:
        :param requested_delta:
        :return: Applied delta active power.
        """
        applied_delta = 0.0
        new_operational_active_power = self.try_active_power(battery_strategy, self.operational_active_power + requested_delta)
        if not self.startup_delay_active:
            applied_delta = new_operational_active_power - self.operational_active_power
            self.operational_active_power = new_operational_active_power
        return applied_delta

    def try_active_power(self, battery_strategy, requested_active_power):
        requested_active_power = self.fulfillment_capability(battery_strategy, requested_active_power)
        applied_active_power = 0
        if self.is_running():
            if self.is_capable_of(requested_active_power):
                self.logger.debug("ESS Unit is capable of active-power=%s" % requested_active_power)
                applied_active_power = self.set_active_power(requested_active_power)
            else:
                if self.fault_detected():
                    self.logger.debug("ESS Unit detected a fault with the following "
                                      "reason: %s" % self.capability_reason)
                    self.send_fault()
                else:
                    self.logger.debug("ESS Unit is being stopped because not capable "
                                      "of requested active power=%s" % requested_active_power)
                    self.stop()
                self.logger.debug("ESS Unit is NOT capable of requested active power=%s" % requested_active_power)
        elif self.is_stopped() or self.is_standby():
            if self.is_auto_mode() and self.is_capable_of(requested_active_power) and requested_active_power != 0:
                self.start()

        return applied_active_power

    capability_reason_map = {
        CAPABILITY_UNRESTRICTED: '',
        CAPABILITY_CHARGE_ONLY: "No more BMUs available for discharge.",
        CAPABILITY_DISCHARGE_ONLY: "No more BMUs available for charge."
    }

    def transition_capability_to(self, new_capability, new_capability_reason=None):
        if self.capability == self.CAPABILITY_FAULT:
            return

        if new_capability != self.capability:
            self.logger.info("ESS unit capability changing from %s to %s" % (self.capability, new_capability))
            self.capability = new_capability
            if new_capability_reason is None:
                self.capability_reason = self.capability_reason_map[new_capability]
            else:
                self.capability_reason = new_capability_reason
            sql = "UPDATE ess_master_info SET capability=%s, capability_reason=%s WHERE ess_id=%s" % \
                  ("%s", "%s", "%s")
            prepared_act_on_database(EXECUTE, sql, [self.capability, self.capability_reason, self.id])
