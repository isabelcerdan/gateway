#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: BYD CESS 400 Controller

@description: Translate GUI command strings into MODbus register settings.

@copyright: Apparent Inc. 2018

@reference: "CESS-P40B40-I-E-R1 Installation User Manual"
            "BYD Modbus TCP/IP Protocol"X
            "Remote Control Manual of Cabinet Energy Storage System (CESS)"
            
@author: steve

@created: February 21, 2018
"""
from multiprocessing import Lock
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Defaults
from pymodbus.register_read_message import ReadHoldingRegistersResponse
import signal

from event.event_constants import EVENT_TAG_CESS_400
from lib.gateway_constants.BydCess400Constants import * 
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
from lib.db import prepared_act_on_database, FETCH_ONE
from ess.ess_driver import EssDriver
import numpy as np


MAX_DB_ERRORS = 5
CONTROLLER_UPDATE_INTERVAL = 5

ZERO_CROSSING_DELAY = 0.250 #250ms


class BydCess400Driver(EssDriver):

    def __init__(self, **kwargs):
        super(BydCess400Driver, self).__init__(**kwargs)
        # self.logger.setLevel(logging.DEBUG)
        self.logger.info("Instantiating BYD CESS-400 Driver...")
        self.ip_address = self.ess_unit.ip_address
        self.port = self.ess_unit.port
        self.client = None
        self.reg_access_lock = self.ess_unit.lock

    def client_connect(self):
        """
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of  the caller of this
               function to close the connection.
        """
        modbus_errs = 0
        Defaults.Timeout = 0.75
        self.reg_access_lock.acquire()
        while True:
            try:
                self.client = ModbusClient(self.ip_address, port=self.port)
            except ValueError as error:
                modbus_errs += 1
                err_string = "Unable to create MODbus client at CESS address %s - %s" % \
                             (self.ip_address, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    self.reg_access_lock.release()
                    raise ConnectError('Maximum ModBus errors exceeded.')
                continue
            break
        
        modbus_errs = 0
        while True:
            try:
                if not self.client.connect():
                    err_string = "MODbus connect failed for CESS unit at %s" % self.ip_address
                    self.logger.error(err_string)
                    modbus_errs += 1
                    if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus connection errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ConnectError('Maximum MODbus connection errors exceeded.')
                    continue
                break
            except Exception as error:
                raise ConnectError('Unable to connect to MODbus slave.')
                
        return True
    
    def test_bit(self, register=None, bit_pos=None):
        if (register & (1 << bit_pos)):
            return True
        else:
            return False
    
    def set_bit(self, register=0, bit_pos=None):
        if bit_pos is None:
            raise ParameterError("Bit position not provided.")
        return (register | (1 << bit_pos))
    
    def clear_bit(self, register=0, bit_pos=None):
        if bit_pos is None:
            raise ParameterError("Bit position not provided.")
        return register & ~(1 << bit_pos)

    def read_holding_registers(self, offset=None, count=0):
        """
        @return: register list, or raise exception on error
        @exceptions: ParameterError, ConnectError, ExceptionResponse
        """
        if offset is None or count == 0:
            raise ParameterError('Invalid input parameters.')
    
        try:
            self.client_connect()
        except:
            raise ConnectError('read_holding_registers: Unable to connect to MODbus client')
        
        modbus_errs = 0
        while True:
            try:
                response = self.client.read_holding_registers(offset,
                                                              count=count, 
                                                              unit=CESS_400_MODBUS_ID)
            except Exception as error:
                err_string = 'read_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus read errors exceeded.')
                    self.client.close()
                    self.reg_access_lock.release()
                    raise ReadError('Maximum MODbus read errors per transaction exceeded.')
            else:
                if isinstance(response, ReadHoldingRegistersResponse):
                    break
                else:
                    """
                    Empty response from the unit.  Usually signifies that the  
                    operation has not had time to complete.
                    """
                    empty = repr(response)
                    self.logger.warning('Empty response from the unit when reading register 0x%x - %s' \
                                   % (offset, empty))
                    modbus_errs += 1
                    if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus read errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ReadError('Maximum MODbus read errors per transaction exceeded.')

                    """
                    Close the connection, re-connect and try again.
                    """
                    try:
                        self.client.close()
                        self.reg_access_lock.release()
                        self.client_connect()
                    except:
                        raise ConnectError('Unable to reconnect to unit after MODbus error')
                    
        try:
            self.client.close()
            self.reg_access_lock.release()
        except ValueError as error:
            raise ProcessingError(error)

        return response
            
    def write_holding_registers(self, offset=None, contents=None):
        if offset is None or contents is None:
            raise ParameterError('Invalid input parameters.')
        
        try:
            self.client_connect()
        except:
            raise ConnectError('Unable to connect to MODbus slave for write operation.')
        
        modbus_errs = 0
        while True:
            try:
                self.client.write_register(offset,
                                           count=1,
                                           value=contents,
                                           unit=CESS_400_MODBUS_ID)
            except Exception as error:
                err_string = 'write_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus write errors exceeded.')
                    self.client.close()
                    self.reg_access_lock.release()
                    raise WriteError('Maximum MODbus write errors per transaction exceeded.')
                
                """
                Close the connection, re-connect and try again.
                """
                try:
                    self.client.close()
                    self.reg_access_lock.release()
                    self.client_connect()
                except:
                    raise ConnectError('Unable to reconnect to unit after MODbus write error')
            else:
                break

        try:
            self.client.close()
            self.reg_access_lock.release()
        except Exception as error:
            raise ProcessingError(error)

        return True
    
    def work_state_running(self):
        query_string = "SELECT %s from %s;" % (ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME,
                                               DB_ESS_MASTER_INFO_TABLE)
        args = []
        try:
            result = prepared_act_on_database(FETCH_ONE, query_string, args)
        except Exception:
            err_string = 'Unable to fetch %s.%s from the database.' % \
                         (DB_ESS_MASTER_INFO_TABLE, ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME)
            raise DatabaseError(err_string)
        
        if result[ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME] == 'Running':
            return True
        else:
            return False
            
    def set_work_state_stop(self):
        
        retries = CESS_400_CMD_RETRIES
        while retries > 0:
            try:
                self.logger.info('Setting work state = STOP',
                            extra={'event_tags': [EVENT_TAG_CESS_400]})
                self.write_holding_registers(CESS_400_WORK_STATE_CONTROL_COMMAND_REG_OFFSET,
                                             CESS_400_WORK_STATE_CONTROL_STOP_VALUE)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue
            
            try:
                response = self.read_holding_registers(CESS_400_WORK_STATE_CONTROL_COMMAND_REG_OFFSET, count=1)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue
        
            if response.registers[0] != CESS_400_WORK_STATE_CONTROL_STOP_VALUE:
                retries -= 1
                if retries == 0:
                    raise WriteError('Verify STOP command failed')
                else:
                    continue
            else:
                break
            
        return True
    
    def set_work_state_run(self):
        retries = CESS_400_CMD_RETRIES
        while retries > 0:
            try:
                self.logger.info('Setting work state = RUN',
                            extra={'event_tags': [EVENT_TAG_CESS_400]})
                self.write_holding_registers(CESS_400_WORK_STATE_CONTROL_COMMAND_REG_OFFSET,
                                             CESS_400_WORK_STATE_CONTROL_RUN_VALUE)
            except Exception as error:
                #self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise ProcessingError('Failed to write work state to RUN')
                else:
                    continue
        
            try:
                response = self.read_holding_registers(CESS_400_WORK_STATE_CONTROL_COMMAND_REG_OFFSET, count=1)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise ProcessingError('Failed to read work state after RUN command')
                else:
                    continue
        
            if response.registers[0] != CESS_400_WORK_STATE_CONTROL_RUN_VALUE:
                retries -= 1
                if retries == 0:
                    raise WriteError('Verify RUN command failed')
                else:
                    continue
            else:
                break
        
        return True
    
    def zero_cross_required(self, proposed_setting=None):
        """
        Retrieve active power command setting to determine the charge/discharge
        state of the unit. 
        """
        query_string = "SELECT %s from %s;" % (CESS_400_ACTIVE_POWER_CMD_COLUMN_NAME,
                                               DB_CESS_400_ESS_VALUES_TABLE)
        args = []
        try:
            result = prepared_act_on_database(FETCH_ONE, query_string, args)
        except Exception:
            err_string = 'Unable to fetch %s.%s from the database.' % \
                         (DB_CESS_400_ESS_VALUES_TABLE, CESS_400_ACTIVE_POWER_CMD_COLUMN_NAME)
            raise DatabaseError(err_string)

        if result[CESS_400_ACTIVE_POWER_CMD_COLUMN_NAME] == 0:
            """
            System is idle - zero crossing not required in either direction.
            """
            return False
        elif proposed_setting == 0:
            self.logger.debug('Proposed power setting is zero...')
            return False
            
        active_power_setting = np.int16(result[CESS_400_ACTIVE_POWER_CMD_COLUMN_NAME])
        self.logger.debug('active power setting = %d' % active_power_setting)
        if active_power_setting > 0:
            if proposed_setting < 0:
                self.logger.debug('Active > 0, proposed < 0 ZCR...')
                return True
            else:
                self.logger.debug('Active > 0, proposed > 0...')
                return False
        else:
            if proposed_setting < 0:
                self.logger.debug('active < 0, proposed < 0...')
                return False
            else:
                self.logger.debug('Active < 0, proposed > 0 ZCR...')
                return True
    
    def set_active_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing active power parameter')
        
        if not self.work_state_running():
            """
            If the unit isn't running, it will ignore commands, so don't bother trying.
            """
            self.logger.warning('Attempt to set ACTIVE POWER when system is not running',
                           extra={'event_tags': [EVENT_TAG_CESS_400]})
            return

        if not self.ess_unit.is_capable_of(power):
            self.logger.debug("Requested active_power=%s cannot be fulfilled: capability='%s'" %
                              (power, self.ess_unit.capability))
            return

        try:
            ipwr = int(round(float(power)))
        except Exception as error:
            raise ParameterError(error)
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(CESS_400_ACTIVE_POWER_MIN) or ipwr > np.int16(CESS_400_ACTIVE_POWER_MAX):
            raise ParameterError('Out-of-range parameter')              

        """        
        @note: Jess says the 400kW unit doesn't require zero-crossing support.
        if self.zero_cross_required(ipwr):
            self.logger.info('Setting active power to 0kW for zero-crossing')
            try:
                self.write_holding_registers(CESS_400_ACTIVE_POWER_COMMAND_REG_OFFSET, 0)
            except Exception as error:
                self.logger.error(error)
                raise WriteError('Failed to write to Active Power register.')
            time.sleep(ZERO_CROSSING_DELAY) # Let machine settle
        """
        retries = CESS_400_CMD_RETRIES
        while retries > 0:
            try:
                self.logger.info('Setting active power to %dkW' % ipwr,
                            extra={'event_tags': [EVENT_TAG_CESS_400]})
                self.write_holding_registers(CESS_400_ACTIVE_POWER_COMMAND_REG_OFFSET, pwr)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue

            try:
                response = self.read_holding_registers(CESS_400_ACTIVE_POWER_COMMAND_REG_OFFSET, count=1)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue
        
            if response.registers[0] != pwr:
                retries -= 1
                if retries == 0:
                    raise WriteError('Verify active power command failed')
                else:
                    continue
            else:
                break
        
        return True
    
    def set_reactive_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing reactive power parameter')

        if not self.work_state_running():
            """
            If the unit isn't running, it will ignore commands, so don't bother trying.
            """
            self.logger.warning('Attempt to set REACTIVE POWER when system is not running',
                           extra={'event_tags': [EVENT_TAG_CESS_400]})
            return
        
        try:
            ipwr = int(round(float(power)))
        except Exception as error:
            raise ParameterError(error)
        
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(CESS_400_REACTIVE_POWER_MIN) or ipwr > np.int16(CESS_400_REACTIVE_POWER_MAX): 
            raise ParameterError('Out-of-range reactive power parameter') 
                     
        self.logger.info('Setting reactive power to %dkW' % ipwr,
                    extra={'event_tags': [EVENT_TAG_CESS_400]})
        retries = CESS_400_CMD_RETRIES
        while retries > 0:
            try:
                self.write_holding_registers(CESS_400_REACTIVE_POWER_COMMAND_REG_OFFSET, pwr)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue
    
            try:
                response = self.read_holding_registers(CESS_400_REACTIVE_POWER_COMMAND_REG_OFFSET, count=1)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue
        
            if response.registers[0] != pwr:
                retries -= 1
                if retries == 0:
                    raise WriteError('Verify reactive power command failed')
                else:
                    continue
            else:
                break
        
        return True
    
    def set_voltage_control(self, voltage=None):
        if voltage is None or voltage == '':
            raise ParameterError('Missing voltage control parameter')
    
        if not self.work_state_running():
            """
            If the unit isn't running, it will ignore commands, so don't bother trying.
            """
            self.logger.warning('Attempt to set VOLTAGE CONTROL when system is not running')
            return
        
        try:
            ivoltage = int(round(float(voltage)))
        except Exception as error:
            raise ParameterError(error)
        
        vltg = (ivoltage & 0xFFFF)
        if ivoltage < np.int16(CESS_400_VOLTAGE_MIN) or ivoltage > np.int16(CESS_400_VOLTAGE_MAX): 
            raise ParameterError('Out-of-range voltage control parameter')              

        self.logger.info('Setting voltage control to %dV' % ivoltage,
                    extra={'event_tags': [EVENT_TAG_CESS_400]})
        retries = CESS_400_CMD_RETRIES
        while retries > 0:
            try:
                self.write_holding_registers(CESS_400_VOLTAGE_CONTROL_COMMAND_REG_OFFSET, vltg)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue

            try:
                response = self.read_holding_registers(CESS_400_VOLTAGE_CONTROL_COMMAND_REG_OFFSET, count=1)
            except Exception as error:
                self.logger.warning(error)
                retries -= 1
                if retries == 0:
                    raise
                else:
                    continue

            if response.registers[0] != vltg:
                retries -= 1
                if retries == 0:
                    raise WriteError('Verify SET VOLTAGE command failed')
                else:
                    continue
            else:
                break
        
    def set_frequency_control(self, frequency=None):
        if frequency is None or frequency == '':
            raise ParameterError('Missing frequency control parameter')
        
        raise NotSupportedError('Frequency control is not yet supported')
