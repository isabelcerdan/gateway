#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: BYD CESS Monitor

@description: Continuously poll the BYD CESS 40 unit for specified system values.

@copyright: Apparent Inc. 2017

@reference: "CESS-P40B40-I-E-R1 Installation User Manual"
            "BYD Modbus TCP/IP Protocol"
            "Remote Control Manual of Cabinet Energy Storage System (CESS)"
            
@author: steve

@created: October 5, 2017
"""
from lib.logging_setup import *
from datetime import datetime
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.pdu import ExceptionResponse
from pymodbus.register_read_message import ReadHoldingRegistersResponse
from pymodbus.constants import Defaults
from lib.gateway_constants.BydCess40Constants import *
import numpy as np
from ess.ess_master_info import EssMasterInfo

CESS_DEFAULT_POLLING_FREQ = 10
CESS_MAX_DB_ERRORS = 5


class BydCess40Monitor(object):
    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs['ess_unit']
        self.ip_address = self.ess_unit.ip_address
        self.port = self.ess_unit.port
        self.reg_access_lock = self.ess_unit.lock
        if self.reg_access_lock is None:
            raise ParameterError('Missing mutex parameter')
        self.client = None
        self.db_errors = 0
        self.ess_master_info = EssMasterInfo.find_by(ess_id=self.ess_unit.id)

        """
        register-to-field update dictionary:
        { register offset : update function }
        """ 
        self.uc_pcs_unit_field_dict = \
        {
            CESS_UNIT_WORK_STATE_REG_OFFSET: self.update_unit_work_state,
            CESS_UNIT_CTRL_MODE_REG_OFFSET: self.update_unit_cntrl_mode,
            CESS_UNIT_WORK_MODE_REG_OFFSET: self.update_unit_work_mode
        }
        
        self.cess_pcs_unit_power_field_dict = \
        {
            DC_VOLTAGE_INFO_REG_OFFSET: self.update_dc_voltage,
            DC_CURRENT_INFO_REG_OFFSET: self.update_dc_current,
            DC_POWER_INFO_REG_OFFSET: self.update_dc_power,
            CESS_UNIT_FREQUENCY_REG_OFFSET: self.update_frequency
        }
    
        self.bms_info_field_dict = \
        {
            CESS_BATTERY_STRING_TOTAL_VOLTAGE_REG_OFFSET: self.update_battery_stack_voltage,
            CESS_BATTERY_STRING_TOTAL_CURRENT_REG_OFFSET: self.update_battery_stack_current,
            CESS_BATTERY_STRING_OUTPUT_POWER_REG_OFFSET: self.update_battery_stack_power,
            CESS_BATTERY_STRING_SOC_REG_OFFSET: self.update_battery_stack_soc,
            CESS_BATTERY_STRING_SOH_REG_OFFSET: self.update_battery_stack_soh,
            CESS_BATTERY_STRING_MAX_CELL_VOLTAGE_REG_OFFSET: self.update_battery_max_cell_voltage,
            CESS_BATTERY_STRING_MIN_CELL_VOLTAGE_REG_OFFSET: self.update_battery_min_cell_voltage,
            CESS_BATTERY_STRING_MAX_CELL_TEMPERATURE_REG_OFFSET: self.update_battery_max_cell_temperature,
            CESS_BATTERY_STRING_MIN_CELL_TEMPERATURE_REG_OFFSET: self.update_battery_min_cell_temperature,
        }
        
        self.uc_pcs_command_field_dict = \
        {
            CESS_WORK_STATE_CONTROL_CMD_REG_OFFSET: self.update_work_state_control_cmd,
            CESS_ACTIVE_POWER_CMD_REG_OFFSET: self.update_active_power_cmd,
            CESS_REACTIVE_POWER_CMD_REG_OFFSET: self.update_reactive_power_cmd
        }
        
        """
        Unit register dictionary definition:
        {register offset : cess_register_value}
        """
        self.register_value_dict = {}
        self.polling_freq_sec = CESS_DEFAULT_POLLING_FREQ
        self.command_dict = { 'refetch': self.get_run_time_parameters,
                              'string_status': self.get_string_status
                            }

    def client_connect(self):
        """
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of  the caller of this
               function to close the connection.
        """
        modbus_errs = 0
        Defaults.Timeout = 1.0
        while True:
            try:
                self.client = ModbusClient(self.ip_address, port=self.port)
            except ValueError as error:
                modbus_errs += 1
                err_string = "MODbus error on CESS address %s - %s" % \
                             (self.ip_address, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    raise ConnectError('Maximum MODbus errors exceeded.')
                continue
            break
        
        modbus_errs = 0
        while True:
            if not self.client.connect():
                err_string = "MODbus connect failed for CESS unit at %s" % self.ip_address
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus connection errors exceeded.')
                    self.client.close()
                    raise ConnectError('Maximum MODbus errors exceeded.')
                continue
            break
        return True
    
    def test_bit(self, register=None, bit_pos=None):
        if (register & (1 << bit_pos)):
            return True
        else:
            return False
        
    def read_holding_registers(self, offset=None, count=0):
        """
        @return: register list, or raise exception on error
        @exceptions: ParameterError, ConnectError, ExceptionResponse
        """
        if offset is None or count == 0:
            raise ParameterError('Invalid input parameters.')
        
        try:
            self.client_connect()
        except:
            raise
        
        modbus_errs = 0
        while True:
            try:
                self.reg_access_lock.acquire()
                response = self.client.read_holding_registers(offset,
                                                              count=count, 
                                                              unit=CESS_MODBUS_ID)
            except (ExceptionResponse, Exception) as error:
                err_string = 'read_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
            else:
                if isinstance(response, ReadHoldingRegistersResponse):
                    break
                else:
                    """
                    Empty response from the unit.  Usually signifies that the  
                    operation has not had time to complete.
                    """
                    empty = repr(response)
                    self.logger.warning('Empty response from the unit when reading register 0x%x - %s' \
                                   % (offset, empty))
                    modbus_errs += 1
                    if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus read errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ReadError('Maximum MODbus read errors per transaction exceeded.')

                    """
                    Close the connection, re-connect and try again.
                    """
                    try:
                        self.client.close()
                        self.reg_access_lock.release()
                        self.client_connect()
                    except:
                        raise ConnectError('Unable to reconnect to unit after MODbus error')
                
        try:
            self.client.close()
            self.reg_access_lock.release()
        except Exception as error:
            raise ProcessingError(error)

        return response
    
    def print_register_dictionary(self):
        for offset, value in self.register_value_dict.iteritems():
            print '0x%x' % offset, ':', np.int16(value)
    
    def get_string_status(self, string_num=None):
        pass

    def read_uc_pcs_unit_field_registers(self):
        for offset in self.uc_pcs_unit_field_dict.iterkeys():
            try:
                response = self.read_holding_registers(offset, count=1)
            except Exception as error:
                self.logger.error('Error reading UC PCS register 0x%x - %s' % (offset, error))
                raise
            self.register_value_dict[offset] = response.registers[0]
        return True

    def update_master_info_from_pcs_unit_fields(self):
        for offset in self.uc_pcs_unit_field_dict.iterkeys():
            try:
                self.uc_pcs_unit_field_dict[offset](self.register_value_dict[offset])
            except:
                raise

    def update_unit_work_state(self, register_value=None):
        unit_work_state = 'Unknown'
        for bit in range(UNIT_WORK_STATE_STOP_BIT, UNIT_WORK_STATE_DEBUG_BIT):
            if self.test_bit(register_value, bit):
                if uc_pcs_unit_work_state_dict[bit] is None:
                    continue
                else:
                    unit_work_state = uc_pcs_unit_work_state_dict[bit]
                    break
        self.ess_master_info.save(unit_work_state=unit_work_state)

    def update_unit_cntrl_mode(self, register_value=None):
        unit_cntrl_mode = 'Unknown'
        for bit in range(UNIT_CONTROL_MODE_REMOTE_BIT, UNIT_CONTROL_MODE_LOCAL_AUTO_BIT+1):
            if self.test_bit(register_value, bit):
                if uc_pcs_unit_cntrl_mode_dict[bit] is None:
                    continue
                else:
                    unit_cntrl_mode = uc_pcs_unit_cntrl_mode_dict[bit]
                    break

        self.ess_master_info.save(unit_cntrl_mode=unit_cntrl_mode)

    def update_unit_work_mode(self, register_value=None):
        unit_work_mode = 'Unknown'
        for bit in range(UNIT_WORK_MODE_DIESEL_BIT, UNIT_WORK_MODE_LOAD_SHIFTING_BIT+1):
            if self.test_bit(register_value, bit):
                if uc_pcs_unit_work_mode_dict[bit] is None:
                    continue
                else:
                    unit_work_mode = uc_pcs_unit_work_mode_dict[bit]
                    break
        self.ess_master_info.save(unit_work_mode=unit_work_mode)

    def read_pcs_unit_power_registers(self):
        for offset in self.cess_pcs_unit_power_field_dict.iterkeys():
            try:
                response = self.read_holding_registers(offset, count=1)
            except Exception as error:
                self.logger.error('Error reading unit power register 0x%x - %s' % (offset, error))
                raise
            self.register_value_dict[offset] = response.registers[0]
        return True                
        
    def update_master_info_from_pcs_unit_power_fields(self):
        for offset in self.cess_pcs_unit_power_field_dict.iterkeys():
            self.cess_pcs_unit_power_field_dict[offset](self.register_value_dict[offset])

    def update_dc_voltage(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * DC_VOLTAGE_INFO_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_DC_VOLTAGE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_dc_current(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * DC_CURRENT_INFO_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_DC_CURRENT_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_dc_power(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * DC_POWER_INFO_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_DC_POWER_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_frequency(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * CESS_UNIT_FREQUENCY_REG_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_FREQUENCY_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def read_bms_info_registers(self):
        for offset in self.bms_info_field_dict.iterkeys():
            try:
                response = self.read_holding_registers(offset, count=1)
                self.register_value_dict[offset] = response.registers[0]
            except Exception as error:
                self.logger.error('Error reading BMS info register 0x%x - %s' % (offset, error))
                raise
        return True
            
    def update_master_info_from_bms_info_fields(self):
        for offset in self.bms_info_field_dict.iterkeys():
            self.bms_info_field_dict[offset](self.register_value_dict[offset])
        
    def update_battery_stack_voltage(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.uint16(register_value * CESS_BATTERY_STRING_TOTAL_VOLTAGE_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_STACK_VOLTAGE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)
    
    def update_battery_stack_current(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * CESS_BATTERY_STRING_TOTAL_CURRENT_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_STACK_CURRENT_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)
    
    def update_battery_stack_power(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * CESS_BATTERY_STRING_OUTPUT_POWER_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_STACK_POWER_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_battery_stack_soc(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.uint16(register_value * CESS_BATTERY_STRING_SOC_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME,
                                          "%s")
        try:
            prepared_act_on_database(EXECUTE, sql, args)
        except:
            raise
    
    def update_battery_stack_soh(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.uint16(register_value * CESS_BATTERY_STRING_SOH_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_STACK_SOH_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def  update_battery_max_cell_voltage(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.uint16(register_value * CESS_BATTERY_STRING_MAX_CELL_VOLTAGE_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def  update_battery_min_cell_voltage(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.uint16(register_value * CESS_BATTERY_STRING_MIN_CELL_VOLTAGE_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_battery_max_cell_temperature(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * CESS_BATTERY_STRING_MAX_CELL_TEMPERATURE_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_battery_min_cell_temperature(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value * CESS_BATTERY_STRING_MIN_CELL_TEMPERATURE_SCALING_FACTOR)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE,
                                          ESS_MASTER_INFO_BATTERY_MIN_CELL_TEMPREATURE_COLUMN_NAME,
                                          "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def read_command_registers(self):
        for offset in self.uc_pcs_command_field_dict.iterkeys():
            try:
                response = self.read_holding_registers(offset, count=1)
            except Exception as error:
                self.logger.error('Error reading PCS command register 0x%x - %s' % (offset, error))
                raise
            self.register_value_dict[offset] = response.registers[0]
        return True                
        
    def update_master_info_from_command_registers(self):
        for offset in self.uc_pcs_command_field_dict.iterkeys():
            self.uc_pcs_command_field_dict[offset](self.register_value_dict[offset])
    
    def update_work_state_control_cmd(self, register_value=None):
        if register_value is None:
            register_value = 0
            
        if register_value == 64:
            work_state = 'Running'
        elif register_value == 16:
            work_state = 'Stopped'
        else:
            work_state = 'Unknown(0x%x)' % register_value
            
        args = [work_state,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE, ESS_MASTER_INFO_WORK_STATE_CONTROL_CMD_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_active_power_cmd(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE, ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def update_reactive_power_cmd(self, register_value=None):
        if register_value is None:
            register_value = 0.0
            
        scaled_value = np.int16(register_value)
        
        args = [scaled_value,]
        sql = "UPDATE %s SET %s = %s;" % (DB_ESS_MASTER_INFO_TABLE, ESS_MASTER_INFO_REACTIVE_POWER_CMD_COLUMN_NAME, "%s")
        prepared_act_on_database(EXECUTE, sql, args)

    def monitor(self):
        self.read_uc_pcs_unit_field_registers()
        self.update_master_info_from_pcs_unit_fields()

        self.read_pcs_unit_power_registers()
        self.update_master_info_from_pcs_unit_power_fields()

        self.read_bms_info_registers()
        self.update_master_info_from_bms_info_fields()

        self.read_command_registers()
        self.update_master_info_from_command_registers()

        """
        @note: Timestamping the scan should be the last operation of a scan cycle,
               for obvious reasons.  Just saying.
        """
        self.timestamp_scan()

        # Clear the dictionary for the next iteration.
        self.register_value_dict.clear()

    def timestamp_scan(self):
        update_string = "UPDATE %s set %s = %s;" %  (DB_ESS_MASTER_INFO_TABLE,
                                                     ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME,
                                                     "%s")
        args = [datetime.now(),]
        prepared_act_on_database(EXECUTE, update_string, args)

    def get_run_time_parameters(self):
        pass
