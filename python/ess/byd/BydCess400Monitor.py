#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: BYD CESS 400 Monitor

@description: Continuously poll the BYD CESS 400 unit for specified system values.

@copyright: Apparent Inc. 2018

@reference: 

@author: steve

@created: February 20, 2018
"""
from datetime import datetime
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Defaults
from pymodbus.pdu import ExceptionResponse
from pymodbus.register_read_message import ReadHoldingRegistersResponse
from lib.gateway_constants.BydCess400Constants import *
from lib.gateway_constants.DBConstants import *
from lib.logging_setup import *
import numpy as np

DB_ERR_MAX = 5
CESS_DEFAULT_POLLING_FREQ = 5
CESS_MAX_DB_ERRORS = 5
CESS_UPDATE_INTERVAL = 5


class BydCess400Monitor(object):
    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs['ess_unit']
        self.reg_access_lock = self.ess_unit.lock
        self.ip_address = self.ess_unit.ip_address
        self.port = self.ess_unit.port
        self.client = None
        self.db_errors = 0
        self.ess_unit_id = self.ess_unit.id
        self.num_strings = 0

        self.init_battery_string_table(self.ess_unit.id)

        """
        Unit register dictionary definition:
        {register offset : cess_register_value}
        """
        self.register_value_dict = {}
        self.polling_freq_sec = CESS_DEFAULT_POLLING_FREQ
        self.cess_400_bms_register_dict = {
            CESS_400_BATTERY_STRING_SOC_1_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_1_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_2_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_2_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_3_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_3_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_4_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_4_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_5_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_5_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_6_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_6_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_7_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_7_SOC_COLUMN_NAME,
            CESS_400_BATTERY_STRING_SOC_8_REG_OFFSET: ESS_MASTER_INFO_BATTERY_STRING_8_SOC_COLUMN_NAME
        }
        self.command_dict = {'refetch': self.get_run_time_parameters,
                             'string_status': self.get_string_status
                             }
        self.cess_400_max_allowable_register_dict = {
            CESS_400_MAX_ALLOWABLE_CHARGE_ACTIVE_POWER_REG_OFFSET: ESS_MASTER_INFO_MAX_ALLOWABLE_CHARGE_ACTIVE_POWER_COLUMN_NAME,
            CESS_400_MAX_ALLOWABLE_DISCHARGE_ACTIVE_POWER_REG_OFFSET: ESS_MASTER_INFO_MAX_ALLOWABLE_DISCHARGE_ACTIVE_POWER_COLUMN_NAME,
            CESS_400_MAX_ALLOWABLE_APPARENT_POWER_REG_OFFSET: ESS_MASTER_INFO_MAX_ALLOWABLE_APPARENT_POWER_COLUMN_NAME,
            CESS_400_MAX_ALLOWABLE_REACTIVE_POWER_REG_OFFSET: ESS_MASTER_INFO_MAX_ALLOWABLE_REACTIVE_POWER_COLUMN_NAME
        }
        self.cess_400_transformer_temperatures_register_dict = {
            CESS_400_IPM_PHASE_A_TEMPERATURE_REG_OFFSET: ESS_MASTER_INFO_IPM_PHASE_A_TEMPERATURE_COLUMN_NAME,
            CESS_400_IPM_PHASE_B_TEMPERATURE_REG_OFFSET: ESS_MASTER_INFO_IPM_PHASE_B_TEMPERATURE_COLUMN_NAME,
            CESS_400_IPM_PHASE_C_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_IPM_PHASE_C_TEMPERATURE_COLUMN_NAME,
            CESS_400_MASTER_INFO_LC_FILTER_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_LC_FILTER_TEMPERATURE_COLUMN_NAME,
            CESS_400_MASTER_INFO_DC_EMI_FILTER_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_DC_EMI_FILTER_TEMPERATURE_COLUMN_NAME,
            CESS_400_MASTER_INFO_AC_EMC_FILTER_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_AC_EMC_FILTER_TEMPERATURE_COLUMN_NAME,
            CESS_400_MASTER_INFO_INT_ENV_TRANSFORMER_TEMP_REG_OFFSET : ESS_MASTER_INFO_INT_ENV_TRANSFORMER_TEMP_COLUMN_NAME,
            CESS_400_MASTER_INFO_INT_ENV_33KW_PCS_TEMP_REG_OFFSET : ESS_MASTER_INFO_INT_ENV_33KW_PCS_TEMP_COLUMN_NAME,
            CESS_400_MASTER_INFO_152KVA_TRANSFORMER_CORE_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_152KVA_TRANSFORMER_CORE_TEMPERATURE_COLUMN_NAME,
            CESS_400_MASTER_INFO_152KVA_TRANSFORMER_WINDING_TEMPERATURE_REG_OFFSET : ESS_MASTER_INFO_152KVA_TRANSFORMER_WINDING_TEMPERATURE_COLUMN_NAME
        }
        self.cess_400_ess_register_dict = {
            CESS_400_UNIT_WORK_STATE_REG_OFFSET: self.update_master_from_unit_work_state,
            CESS_400_UNIT_CTRL_MODE_REG_OFFSET: self.update_master_from_unit_cntrl_mode,
            CESS_400_UNIT_WORK_MODE_REG_OFFSET: self.update_master_from_unit_work_mode,
            CESS_400_BATTERY_RECONNECT_STATE_REG_OFFSET: self.update_master_from_reconnect_state,
            CESS_400_BATTERY_STACK_VOLTAGE_REG_OFFSET: self.update_master_from_battery_stack_voltage,
            CESS_400_BATTERY_STACK_CURRENT_REG_OFFSET: self.update_master_from_battery_stack_current,
            CESS_400_BATTERY_STACK_POWER_REG_OFFSET: self.update_master_from_battery_stack_power,
            CESS_400_BATTERY_STACK_SOC_REG_OFFSET: self.update_master_from_battery_stack_soc,
            CESS_400_BATTERY_STACK_SOH_REG_OFFSET: self.update_master_from_battery_stack_soh,
            CESS_400_BATTERY_MAX_CELL_VOLTAGE_REG_OFFSET: self.update_master_from_battery_max_cell_voltage,
            CESS_400_BATTERY_MIN_CELL_VOLTAGE_REG_OFFSET: self.update_master_from_battery_min_cell_voltage,
            CESS_400_BATTERY_MAX_CELL_TEMPERATURE_REG_OFFSET: self.update_master_from_battery_max_cell_temperature,
            CESS_400_BATTERY_MIN_CELL_TEMPERATURE_REG_OFFSET: self.update_master_from_battery_min_cell_temperature,
            CESS_400_DC_VOLTAGE_REG_OFFSET: self.update_master_from_dc_voltage,
            CESS_400_DC_CURRENT_REG_OFFSET: self.update_master_from_dc_current,
            CESS_400_DC_POWER_REG_OFFSET: self.update_master_from_dc_power,
            CESS_400_FREQUENCY_REG_OFFSET: self.update_master_from_frequency,
            CESS_400_POWER_FACTOR_REG_OFFSET: self.update_master_from_power_factor,
            CESS_400_WORK_STATE_CONTROL_COMMAND_REG_OFFSET: self.update_master_from_work_state_control_cmd,
            CESS_400_ACTIVE_POWER_COMMAND_REG_OFFSET: self.update_master_from_active_power_cmd,
            CESS_400_REACTIVE_POWER_COMMAND_REG_OFFSET: self.update_master_from_reactive_power_cmd,
            CESS_400_VOLTAGE_CONTROL_COMMAND_REG_OFFSET: self.update_master_from_voltage_control_cmd,
            CESS_400_FREQUENCY_CONTROL_COMMAND_REG_OFFSET: self.update_master_from_frequency_control_cmd
        }

    def init_battery_string_table(self, ess_unit_id):
        """
        @note: This function only works for a single-battery scenario.
        """
        self.logger.debug('Initializing battery_strings table for ESS Unit ID = %d...' % int(ess_unit_id))
        count_query = "SELECT COUNT(*) from %s WHERE %s = %s" % (DB_BATTERY_STRINGS_TABLE,
                                                                  BATTERY_STRING_STATUSES_ESS_UNIT_ID_COLUMN_NAME,
                                                                  "%s")
        args = [ess_unit_id,]
        response = prepared_act_on_database(FETCH_ONE, count_query, args)
        if response:
            if response.get('COUNT(*)') > 0:
                self.logger.debug('Battery string table already initialized...')
                return
        """
        Get the number of strings - map the battery id to the model id first.
        """
        model_query = "SELECT %s FROM %s WHERE %s = %s" % (ESS_UNITS_MODEL_ID_COLUMN_NAME,
                                                            DB_ESS_UNITS_TABLE,
                                                            ESS_UNITS_ID_COLUMN_NAME, "%s")
        args = [ess_unit_id,]
        response = prepared_act_on_database(FETCH_ONE, model_query, args)
        if response is None:
            self.logger.error('Unable to get battery model for battery_strings table initialization')
            return False
        model_id = response.get(ESS_UNITS_MODEL_ID_COLUMN_NAME)
        self.logger.debug('Using %d for battery model...' % int(model_id))

        num_query = "SELECT %s FROM %s WHERE %s = %s" % (ESS_MODEL_NUM_STRINGS_COLUMN_NAME,
                                                          DB_ESS_MODEL_TABLE,
                                                          ESS_MODEL_ID_COLUMN_NAME, "%s")
        args = [model_id,]
        response = prepared_act_on_database(FETCH_ONE, num_query, args)
        if response is None:
            self.logger.error('Unable to get number of strings for battery_strings table initialization')
            return False

        num_strings = response.get(ESS_MODEL_NUM_STRINGS_COLUMN_NAME)
        self.logger.debug('Initializing %d rows in battery_strings table...' % int(num_strings))
        insert_str= "INSERT INTO %s (%s, %s) VALUES (" % (DB_BATTERY_STRINGS_TABLE,
                                                          BATTERY_STRING_STATUSES_ESS_UNIT_ID_COLUMN_NAME,
                                                          BATTERY_STRING_STATUSES_STRING_NUMBER_COLUMN_NAME)
        for num in range(1, num_strings+1):
            insert_cmd = insert_str + "%s, " + "%s)"
            args = [ess_unit_id, num]
            try:
                prepared_act_on_database(EXECUTE, insert_cmd, args)
            except Exception as error:
                self.logger.error('Database failure on %s table initialization.' % DB_BATTERY_STRINGS_TABLE)
            del insert_cmd


    def client_connect(self):
        """
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of  the caller of this
               function to close the connection.
        @note: calling client_connect() acquires the lock for communication to the
               MODbus server.  It is up to the caller to release the lock.
        """
        modbus_errs = 0
        Defaults.Timeout = BYD_CESS_400_MODBUS_TIMEOUT
        self.reg_access_lock.acquire()
        while True:
            try:
                self.client = ModbusClient(self.ip_address, port=self.port)
            except ValueError as error:
                modbus_errs += 1
                err_string = "MODbus error on CESS address %s - %s" % \
                             (self.ip_address, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    self.reg_access_lock.release()
                    raise ConnectError('Maximum ModBus errors exceeded.')
                continue
            break

        modbus_errs = 0
        while True:
            try:
                if not self.client.connect():
                    err_string = "MODbus connect failed for CESS unit at %s" % self.ip_address
                    self.logger.error(err_string)
                    modbus_errs += 1
                    if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus connection errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ConnectError('Maximum MODbus connection errors exceeded.')
                    continue
                break
            except Exception as error:
                raise ProcessingError(error)
        return True

    def test_bit(self, register=None, bit_pos=None):
        if register & (1 << bit_pos):
            return True
        else:
            return False

    def read_holding_registers(self, offset=None, count=0):
        """
        @return: register list, or raise exception on error
        @exceptions: ParameterError, ConnectError, ExceptionResponse
        """
        if offset is None or count == 0:
            raise ParameterError('Invalid input parameters.')

        try:
            self.client_connect()
        except:
            raise ConnectError('read_holding_registers: Unable to connect to MODbus client')

        modbus_errs = 0
        while True:
            try:
                response = self.client.read_holding_registers(offset,
                                                              count=count,
                                                              unit=CESS_400_MODBUS_ID)
            except (ExceptionResponse, Exception) as error:
                err_string = 'read_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus read errors exceeded.')
                    self.client.close()
                    self.reg_access_lock.release()
                    raise ReadError('Maximum MODbus read errors per transaction exceeded.')
            else:
                if isinstance(response, ReadHoldingRegistersResponse):
                    break
                else:
                    """
                    Empty response from the unit.  Usually signifies that the  
                    operation has not had time to complete.
                    """
                    empty = repr(response)
                    self.logger.warning('Empty response from the unit when reading register 0x%x - %s'
                                   % (offset, empty))
                    modbus_errs += 1
                    if modbus_errs >= CESS_400_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus read errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ReadError('Maximum MODbus read errors per transaction exceeded.')

                    """
                    Close the connection, re-connect and try again.
                    """
                    try:
                        self.client.close()
                        self.reg_access_lock.release()
                        self.client_connect()
                    except:
                        raise ConnectError('Unable to reconnect to unit after MODbus error')

        try:
            self.client.close()
            self.reg_access_lock.release()
        except Exception as error:
            raise ProcessingError(error)

        return response

    def print_register_dictionary(self):
        for offset, value in self.register_value_dict.iteritems():
            print '0x%x' % offset, ':', np.int16(value)

    def monitor(self):
        """
        Read selected ESS registers.
        """
        try:
            self.read_ess_registers()
            self.update_master_info_from_ess_registers()
        except:
            self.logger.error('Error processing ESS registers')
            raise

        """
        Read selected BMS registers.
        """
        try:
            self.read_bms_registers()
            self.update_master_info_from_bms_registers()
        except:
            self.logger.error('Error processing BMS registers')
            raise

        """
        Read the 'maximum allowable' register values.
        """
        try:
            self.read_max_allowable_registers()
            self.update_master_info_from_max_allowable_registers()
        except:
            self.logger.error('Error processing MAX ALLOWABLE registers')
            raise

        try:
            self.read_transformer_temperatures()
            self.update_master_info_from_transformer_temperatures()
        except:
            raise
        
        """
        Get the individual string statuses.
        """
        try:
            self.get_battery_string_running_state()
            self.get_battery_string_enabled_state()
            self.get_battery_string_isolated_switch_state_state()
        except:
            self.logger.error('Error processing battery string registers')
            raise

        """
        @note: Timestamping the scan should be the last operation of a scan cycle,
               for obvious reasons.  Just saying.
        """
        try:
            self.timestamp_scan()
        except:
            raise

        # Clear the dictionary for the next iteration.
        self.register_value_dict.clear()

    def timestamp_scan(self):
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [datetime.now(), self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to timestamp latest scan')

    def read_ess_registers(self):
        for offset in self.cess_400_ess_register_dict.iterkeys():
            modbus_errs = 0
            while True:
                try:
                    response = self.read_holding_registers(offset, count=1)
                except Exception as error:
                    self.logger.error(
                        'Exception reading ESS register at 0x%x- %s' % (offset, error))
                    raise
                if response.function_code > 0x80:
                    # MODbus error occurred
                    modbus_errs += 1
                    self.logger.error('MODbus error %x detected reading address 0x%x' %
                                 (response.function_code, offset))
                    if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                        raise ReadError('Maximum MODbus errors exceeded')
                else:
                    break
            self.register_value_dict[offset] = response.registers[0]

        return True

    def update_master_info_from_ess_registers(self):
        for offset, update_func in self.cess_400_ess_register_dict.iteritems():
            try:
                update_func(self.register_value_dict[offset])
            except:
                raise
        return True

    def get_battery_string_running_state(self):
        self.logger.debug('Get battery string running state...')
        modbus_errs = 0
        while True:
            try:
                response = self.read_holding_registers(
                    CESS_400_BATTERY_STRING_RUNNING_STATE_REG_OFFSET, count=1)
            except:
                self.logger.error('Exception reading ESS register at 0x%x' %
                             CESS_400_BATTERY_STRING_RUNNING_STATE_REG_OFFSET)
                raise
            if response.function_code > 0x80:
                # MODbus error occurred
                modbus_errs += 1
                self.logger.error('MODbus error %x detected reading address 0x%x' %
                             (response.function_code, CESS_400_BATTERY_STRING_RUNNING_STATE_REG_OFFSET))
                if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            else:
                break
        register_content = response.registers[0]
        self.logger.debug('running state register contents = 0x%x...' %
                     register_content)
        for string_num in range(0, self.num_strings):
            if self.test_bit(register_content, string_num) == BATTERY_STRING_RUNNING_STATE_RUNNING:
                state = "running"
            else:
                state = "stop"

            update_string = "UPDATE %s SET %s = %s WHERE %s = %s AND %s = %s" % \
                            (DB_BATTERY_STRINGS_TABLE,
                             BATTERY_STRING_STATUSES_RUNNING_STATE_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_STRING_NUMBER_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_ESS_UNIT_ID_COLUMN_NAME, "%s")
            args = [state, string_num + 1, self.ess_unit_id]
            try:
                prepared_act_on_database(EXECUTE, update_string, args)
            except:
                raise

        return True

    def get_battery_string_enabled_state(self):
        modbus_errs = 0
        while True:
            try:
                response = self.read_holding_registers(
                    CESS_400_BATTERY_STRING_ENABLED_STATE_REG_OFFSET, count=1)
            except:
                self.logger.error('Exception reading ESS register at 0x%x' %
                             CESS_400_BATTERY_STRING_ENABLED_STATE_REG_OFFSET)
                raise
            if response.function_code > 0x80:
                # MODbus error occurred
                modbus_errs += 1
                self.logger.error('MODbus error %x detected reading address 0x%x' %
                             (response.function_code, CESS_400_BATTERY_STRING_ENABLED_STATE_REG_OFFSET))
                if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            else:
                break
        register_content = response.registers[0]
        for string_num in range(0, self.num_strings):
            if self.test_bit(register_content, string_num) == BATTERY_STRING_ENABLED_STATE_ENABLED:
                state = "enabled"
            else:
                state = "disabled"

            update_string = "UPDATE %s SET %s = %s WHERE %s = %s AND %s = %s" % \
                            (DB_BATTERY_STRINGS_TABLE,
                             BATTERY_STRING_STATUSES_ENABLED_STATE_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_STRING_NUMBER_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_ESS_UNIT_ID_COLUMN_NAME, "%s")
            args = [state, string_num + 1, self.ess_unit_id]
            try:
                prepared_act_on_database(EXECUTE, update_string, args)
            except:
                raise

        return True

    def get_battery_string_isolated_switch_state_state(self):
        modbus_errs = 0
        while True:
            try:
                response = self.read_holding_registers(
                    CESS_400_BATTERY_STRING_ISOLATED_SWITCH_STATE_REG_OFFSET, count=1)
            except:
                self.logger.error('Exception reading ESS register at 0x%x' %
                             CESS_400_BATTERY_STRING_ISOLATED_SWITCH_STATE_REG_OFFSET)
                raise
            if response.function_code > 0x80:
                # MODbus error occurred
                modbus_errs += 1
                self.logger.error('MODbus error %x detected reading address 0x%x' %
                             (response.function_code, CESS_400_BATTERY_STRING_ISOLATED_SWITCH_STATE_REG_OFFSET))
                if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                    raise ReadError('Maximum MODbus errors exceeded')
            else:
                break
        register_content = response.registers[0]
        for string_num in range(0, self.num_strings):
            if self.test_bit(register_content, string_num) == BATTERY_STRING_ISOLATED_SWITCH_STATE_CLOSED:
                state = "closed"
            else:
                state = "open"

            update_string = "UPDATE %s SET %s = %s WHERE %s = %s AND %s = %s" % \
                            (DB_BATTERY_STRINGS_TABLE,
                             BATTERY_STRING_STATUSES_ISOLATED_SWITCH_STATE_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_STRING_NUMBER_COLUMN_NAME, "%s",
                             BATTERY_STRING_STATUSES_ESS_UNIT_ID_COLUMN_NAME, "%s")
            args = [state, string_num + 1, self.ess_unit_id]
            try:
                prepared_act_on_database(EXECUTE, update_string, args)
            except:
                raise

        return True

    def not_implemented(self, field_name=None, reg_value=None):
        #self.logger.info('Update function not implemented for %s' % field_name)
        pass

    def read_bms_registers(self):
        for offset in self.cess_400_bms_register_dict.iterkeys():
            modbus_errs = 0
            while True:
                try:
                    response = self.read_holding_registers(offset, count=1)
                except Exception as error:
                    self.logger.error('Exception reading ESS register at 0x%x- %s' % (offset, error))
                    raise

                if response.function_code > 0x80:
                    # MODbus error occurred
                    modbus_errs += 1
                    self.logger.error('MODbus error %x detected reading address 0x%x' %
                                 (response.function_code, offset))
                    if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                        raise ReadError('Maximum MODbus errors exceeded')
                else:
                    break
            self.register_value_dict[offset] = response.registers[0]

        return True

    def update_master_info_from_bms_registers(self):
        for register_offset, column_name in self.cess_400_bms_register_dict.iteritems():
            update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                        column_name, "%s", 
                                                        ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
            args = [self.register_value_dict[register_offset], self.ess_unit.id]
            try:
                prepared_act_on_database(EXECUTE, update_string, args)
            except:
                raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                                column_name))

    def get_string_status(self, string_num=None):
        if string_num is None:
            raise ParameterError('String number parameter missing')

        # { register offset : register value }
        string_status_dict = {}
        string_offset = cess_400_bms_string_offset_list[int(string_num) - 1]
        for register_offset in cess_400_string_status_register_dict.iterkeys():
            modbus_errs = 0
            reg_addr = string_offset + register_offset
            while True:
                try:
                    response = self.read_holding_registers(reg_addr, count=1)
                except Exception as error:
                    self.logger.error('Exception reading BMS register at 0x%x- %s' % (reg_addr, error))
                    raise

                if response.function_code > 0x80:
                    # MODbus error occurred
                    modbus_errs += 1
                    self.logger.error('MODbus error %x detected reading address 0x%x' %
                                 (response.function_code, reg_addr))
                    if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                        raise ReadError('Maximum MODbus errors exceeded')
                else:
                    break
            string_status_dict[register_offset] = response.registers[0]

        insert_list = ["INSERT INTO %s (%s, " % (DB_BYD_CESS_400_STRING_STATUS_TABLE,
                                                 CESS_400_STRING_STATUS_STRING_NUM_COLUMN_NAME)]
        for register_offset in string_status_dict.iterkeys():
            insert_list.append(
                cess_400_string_status_register_dict[register_offset])
            insert_list.append(', ')
        insert_list.pop()  # get rid of the last ', '
        insert_list.append(') VALUES (%s, ' % string_num)
        for register_offset in string_status_dict:
            scaled_value = string_status_dict[register_offset] * \
                cess_400_string_status_scaling_factor_dict[register_offset]
            insert_list.append(str(scaled_value))
            insert_list.append(', ')
        insert_list.pop()  # get rid of the last ', '
        insert_list.append(');')
        insert_string = ''.join(insert_list)
        args = []
        try:
            prepared_act_on_database(EXECUTE, insert_string, args)
        except:
            raise

    def update_master_from_unit_work_state(self, reg_value=None):
        if reg_value is None:
            str_value = "Unknown"
        elif int(reg_value) == UNIT_WORK_STATE_STOP_VALUE:
            str_value = 'Stop'
        elif int(reg_value) == UNIT_WORK_STATE_STARTING_VALUE or int(reg_value) == UNIT_WORK_STATE_STANDBY_VALUE:
            #            str_value = 'Starting'
            str_value = 'Standby'
        elif int(reg_value) == UNIT_WORK_STATE_RUNNING_VALUE:
            str_value = 'Running'
        elif int(reg_value) == UNIT_WORK_STATE_FAULT_VALUE:
            str_value = 'Fault'
        else:
            str_value = 'Unknown'

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                    ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME, "%s",
                                                    ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [str_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_UNIT_WORK_STATE_COLUMN_NAME))
        return True

    def update_master_from_unit_cntrl_mode(self, reg_value=None):
        if reg_value == UNIT_CONTROL_MODE_REMOTE_VALUE:
            str_value = 'Remote'
        elif reg_value == UNIT_CONTROL_MODE_LOCAL_MANUAL_VALUE:
            str_value = 'Local manual'
        else:
            str_value = "Unknown"

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                        ESS_MASTER_INFO_UNIT_CTRL_MODE_COLUMN_NAME, "%s",
                                                        ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [str_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_UNIT_CTRL_MODE_COLUMN_NAME))
        return True

    def update_master_from_unit_work_mode(self, reg_value=None):
        if reg_value is None:
            str_value = 'Unknown'
        elif int(reg_value) == UNIT_WORK_MODE_PQ_VALUE:
            str_value = 'P/Q'
        elif int(reg_value) == UNIT_WORK_MODE_ISLAND_VALUE:
            str_value = 'Island'
        else:
            str_value = 'Unknown'

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_UNIT_WORK_MODE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [str_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_UNIT_WORK_MODE_COLUMN_NAME))
        return True

    def update_master_from_reconnect_state(self, reg_value=None):
        if reg_value is None:
            str_value = 'Unknown'
        elif int(reg_value) == RECONNECT_STATE_COMPLETE_OR_PAUSED_VALUE:
            str_value = 'Complete/Paused'
        elif int(reg_value) == RECONNECT_STATE_RECONNECTING_VALUE:
            str_value = 'Reconnecting'
        else:
            str_value = 'Unknown'

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                    ESS_MASTER_INFO_RECONNECT_STATE_COLUMN_NAME, "%s",
                                                    ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [str_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_RECONNECT_STATE_COLUMN_NAME))
        return True

    def update_master_from_battery_stack_voltage(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(
                reg_value) * CESS_400_BATTERY_STACK_VOLTAGE_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                    ESS_MASTER_INFO_BATTERY_STACK_VOLTAGE_COLUMN_NAME, "%s",
                                                    ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_STACK_VOLTAGE_COLUMN_NAME))
        return True

    def update_master_from_battery_stack_current(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(
                reg_value) * CESS_400_BATTERY_STACK_CURRENT_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_STACK_CURRENT_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")

        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_STACK_CURRENT_COLUMN_NAME))
        return True

    def update_master_from_battery_stack_power(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value) * \
                CESS_400_BATTERY_STACK_POWER_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_STACK_POWER_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_STACK_POWER_COLUMN_NAME))
        return True

    def update_master_from_battery_stack_soc(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value)
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME))
        return True

    def update_master_from_battery_stack_soh(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value)
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_STACK_SOH_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_STACK_SOH_COLUMN_NAME))
        return True

    def update_master_from_battery_max_cell_voltage(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(
                reg_value) * CESS_400_BATTERY_MAX_CELL_VOLTAGE_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME))
        return True

    def update_master_from_battery_min_cell_voltage(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(
                reg_value) * CESS_400_BATTERY_MIN_CELL_VOLTAGE_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME))
        return True

    def update_master_from_battery_max_cell_temperature(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value)
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_MAX_CELL_TEMPERATURE_COLUMN_NAME))
        return True

    def update_master_from_battery_min_cell_temperature(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value)
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_BATTERY_MIN_CELL_TEMPREATURE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_BATTERY_MIN_CELL_TEMPREATURE_COLUMN_NAME))
        return True

    def update_master_from_dc_voltage(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.uint16(reg_value) * \
                CESS_400_DC_VOLTAGE_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_DC_VOLTAGE_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_DC_VOLTAGE_COLUMN_NAME))
        return True

    def update_master_from_dc_current(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value) * \
                CESS_400_DC_CURRENT_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_DC_CURRENT_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_DC_CURRENT_COLUMN_NAME))
        return True

    def update_master_from_dc_power(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value) * \
                CESS_400_DC_POWER_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_DC_POWER_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True

    def update_master_from_frequency(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value) * \
                CESS_400_FREQUENCY_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_FREQUENCY_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True

    def update_master_from_power_factor(self, reg_value=None):
        if reg_value is None:
            update_value = 0
        else:
            update_value = np.int16(reg_value) * \
                CESS_400_POWER_FACTOR_SCALING_FACTOR
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_POWER_FACTOR_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [update_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True

    def update_master_from_work_state_control_cmd(self, reg_value=None):
        if reg_value == CESS_400_WORK_STATE_CONTROL_RUN_VALUE:
            str_value = "Run"
        elif reg_value == CESS_400_WORK_STATE_CONTROL_RUN_VALUE:
            str_value = "Stop"
        else:
            str_value = "Unknown"
            #self.logger.warning('Unknown value in Work State Control Command register')

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_WORK_STATE_CONTROL_CMD_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [str_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True

    def update_master_from_active_power_cmd(self, reg_value=None):
        if reg_value is None:
            reg_value = 0
        if reg_value > 32767:
            signed_value = reg_value - 65536
        else:
            signed_value = reg_value

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [signed_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)

        return True

    def update_master_from_reactive_power_cmd(self, reg_value=None):
        if reg_value is None:
            reg_value = 0
        if reg_value > 32767:
            signed_value = reg_value - 65536
        else:
            signed_value = reg_value

        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_REACTIVE_POWER_CMD_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [signed_value, self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)

        return True

    def update_master_from_voltage_control_cmd(self, reg_value=None):
        if reg_value is None:
            reg_value = 0
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_VOLTAGE_CONTROL_CMD_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        args = [int(reg_value), self.ess_unit.id]
        prepared_act_on_database(EXECUTE, update_string, args)
        return True

    def update_master_from_frequency_control_cmd(self, reg_value=None):
        if reg_value is None:
            reg_value = 0
        update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                 ESS_MASTER_INFO_FREQUENCY_CONTROL_CMD_COLUMN_NAME, "%s",
                                                                 ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
        update_value = np.int16(reg_value) * \
            CESS_400_FREQUENCY_CONTROL_CMD_SCALING_FACTOR
        args = [update_value, self.ess_unit.id]
        try:
            prepared_act_on_database(EXECUTE, update_string, args)
        except:
            raise DatabaseError('Unable to update %s.%s' % (DB_ESS_MASTER_INFO_TABLE,
                                                            ESS_MASTER_INFO_FREQUENCY_CONTROL_CMD_COLUMN_NAME))
        return True

    def read_max_allowable_registers(self):
        for offset in self.cess_400_max_allowable_register_dict.iterkeys():
            modbus_errs = 0
            while True:
                try:
                    response = self.read_holding_registers(offset, count=1)
                except Exception as error:
                    self.logger.error(
                        'Exception reading ESS register at 0x%x- %s' % (offset, error))
                    raise

                if response.function_code > 0x80:
                    # MODbus error occurred
                    modbus_errs += 1
                    self.logger.error('MODbus error %x detected reading address 0x%x' %
                                 (response.function_code, offset))
                    if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                        raise ReadError('Maximum MODbus errors exceeded')
                else:
                    break
            self.register_value_dict[offset] = response.registers[0]

        return True

    def update_master_info_from_max_allowable_registers(self):
        for register_offset, column_name in self.cess_400_max_allowable_register_dict.iteritems():
            unsigned_value = self.register_value_dict[register_offset] 
            if unsigned_value > 32767:
                signed_value = unsigned_value - 65536
            else:
                signed_value = unsigned_value

            update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                     column_name, "%s",
                                                                     ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
            args = [signed_value, self.ess_unit_id]
            prepared_act_on_database(EXECUTE, update_string, args)

    def read_transformer_temperatures(self):
        for offset in self.cess_400_transformer_temperatures_register_dict.iterkeys():
            modbus_errs = 0
            while True:
                try:
                    response = self.read_holding_registers(offset, count=1)
                except Exception as error:
                    self.logger.error(
                        'Exception reading temperature register at 0x%x- %s' % (offset, error))
                    raise

                if response.function_code > 0x80:
                    # MODbus error occurred
                    modbus_errs += 1
                    self.logger.error('MODbus error %x detected reading address 0x%x' %
                                 (response.function_code, offset))
                    if modbus_errs > CESS_400_MAX_MODBUS_ERRORS:
                        raise ReadError('Maximum MODbus errors exceeded')
                else:
                    break
            self.register_value_dict[offset] = response.registers[0]

        return True
    
    def update_master_info_from_transformer_temperatures(self):
        for register_offset, column_name in self.cess_400_transformer_temperatures_register_dict.iteritems():
            update_string = "UPDATE %s SET %s = %s WHERE %s = %s" % (DB_ESS_MASTER_INFO_TABLE,
                                                                     column_name, "%s",
                                                                     ESS_MASTER_INFO_ESS_ID_COLUMN_NAME, "%s")
            args = [self.register_value_dict[register_offset], self.ess_unit.id]
            prepared_act_on_database(EXECUTE, update_string, args)

    def get_run_time_parameters(self):
        query_string = "SELECT * FROM %s WHERE %s = %s" % (DB_ESS_UNITS_TABLE,
                                                           ESS_UNITS_ID_COLUMN_NAME, "%s")
        param_dict = {}
        args = [self.ess_unit.id]
        try:
            response = prepared_act_on_database(
                FETCH_ALL, query_string, args)
        except Exception:
            self.logger.critical('Unable to fetch run-time parameters.')
            raise DatabaseError('Failed to fetch run-time parameters.')
        try:
            param_dict.clear()
            param_dict = response[0]
            self.ess_unit_id = param_dict[ESS_UNITS_ID_COLUMN_NAME]
            self.logger.info("Battery: ID = %s" % self.ess_unit_id)
        except Exception as error:
            self.logger.error('Unable to get battery model data')
            raise DatabaseError(error)

        query_string = "SELECT %s FROM %s" % (ESS_MODEL_NUM_STRINGS_COLUMN_NAME, DB_ESS_MODEL_TABLE)
        args = []
        response = prepared_act_on_database(FETCH_ONE, query_string, args)

        try:
            self.num_strings = response.get(ESS_MODEL_NUM_STRINGS_COLUMN_NAME)
        except Exception as error:
            self.logger.error(error)
            raise DatabaseError('Unable to extract number-of-strings parameter')

