#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: BYD CESS 40 Controller

@description: Translate GUI command strings into MODbus register settings.

@copyright: Apparent Inc. 2017

@reference: "CESS-P40B40-I-E-R1 Installation User Manual"
            "BYD Modbus TCP/IP Protocol"
            "Remote Control Manual of Cabinet Energy Storage System (CESS)"
            
@author: steve

@created: October 16, 2017
"""
import time
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.register_read_message import ReadHoldingRegistersResponse
from pymodbus.constants import Defaults
from lib.gateway_constants.BydCess40Constants import *
from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database, EXECUTE, FETCH_ONE
from ess.ess_driver import EssDriver
from dateutil import parser
import numpy as np


CESS_MAX_DB_ERRORS = 5
CONTROLLER_SOCKET_TIMEOUT = 5000 #ms

POWER_RISE_TIME_MIN_SEC = 180
POWER_RISE_TIME_MAX_SEC = 540
POWER_DECREASE_TIME_MIN_SEC = 360         
POWER_DECREASE_TIME_MAX_SEC = 1080
CAPACITY_TEST_TARGET_POWER_MIN = 0xffd8      
CAPACITY_TEST_TARGET_POWER_MAX = 40
DIESEL_TARGET_POWER_MIN = 0xffd8
DIESEL_TARGET_POWER_MAX = 0
DISCHARGE_POWER_MIN = 0
DISCHARGE_POWER_MAX = 40
CHARGE_POWER_MIN = 0xffd8
CHARGE_POWER_MAX = 0
ZERO_CROSSING_DELAY = 0.250 #250ms
ACTIVE_POWER_MIN = 0xffd8
ACTIVE_POWER_MAX = 40
REACTIVE_POWER_MIN = 0xffd8
REACTIVE_POWER_MAX = 40


def sig_handler(signum, frame):
    print "Caught signal:", signum
    exit(1)
    

class BydCess40Driver(EssDriver):
    def __init__(self, **kwargs):
        super(BydCess40Driver, self).__init__(**kwargs)
        self.feed_name = self.ess_unit.feed_name
        self.logger.info("Instantiating BYD CESS-40 Controller...")
        if self.ess_unit.lock is None:
            raise ParameterError('Missing mutex parameter')        
        self.reg_access_lock = self.ess_unit.lock
        self.ip_address = self.ess_unit.ip_address
        self.port = self.ess_unit.port
        self.db_errors = 0
        self.client = None

    def client_connect(self):
        """
        @note: calling client_connect() creates the MODbus client object and connects
               to the external unit.  It is the responsibility of  the caller of this
               function to close the connection.
        """
        modbus_errs = 0
        Defaults.Timeout = 0.75
        self.reg_access_lock.acquire()
        while True:
            try:
                self.client = ModbusClient(self.ip_address, port=self.port)
            except ValueError as error:
                modbus_errs += 1
                err_string = "Unable to create MODbus client at CESS address %s - %s" % \
                             (self.ip_address, repr(error))
                self.logger.error(err_string)
                if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum ModBus errors exceeded.')
                    self.reg_access_lock.release()
                    raise ConnectError('Maximum ModBus errors exceeded.')
                continue
            break
        
        modbus_errs = 0
        while True:
            if not self.client.connect():
                err_string = "MODbus connect failed for CESS unit at %s" % self.ip_address
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus connection errors exceeded.')
                    self.client.close()
                    self.reg_access_lock.release()
                    raise ConnectError('Maximum MODbus connection errors exceeded.')
                continue
            break
        return True
    
    def test_bit(self, register=None, bit_pos=None):
        if (register & (1 << bit_pos)):
            return True
        else:
            return False
    
    def set_bit(self, register=0, bit_pos=None):
        if bit_pos is None:
            raise ParameterError("Bit position not provided.")
        return (register | (1 << bit_pos))
    
    def clear_bit(self, register=0, bit_pos=None):
        if bit_pos is None:
            raise ParameterError("Bit position not provided.")
        return register & ~(1 << bit_pos)

    def read_holding_registers(self, offset=None, count=0):
        """
        @return: register list, or raise exception on error
        @exceptions: ParameterError, ConnectError, ExceptionResponse
        """
        if offset is None or count == 0:
            raise ParameterError('Invalid input parameters.')
        
        try:
            self.client_connect()
        except:
            raise ConnectError('read_holding_registers: Unable to connect to MODbus client')
        
        modbus_errs = 0
        while True:
            try:
                response = self.client.read_holding_registers(offset,
                                                              count=count, 
                                                              unit=CESS_MODBUS_ID)
            except Exception as error:
                err_string = 'read_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
            else:
                if isinstance(response, ReadHoldingRegistersResponse):
                    break
                else:
                    """
                    Empty response from the unit.  Usually signifies that the  
                    operation has not had time to complete.
                    """
                    empty = repr(response)
                    self.logger.warning('Empty response from the unit when reading register 0x%x - %s' \
                                   % (offset, empty))
                    modbus_errs += 1
                    if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                        self.logger.critical('Maximum MODbus read errors exceeded.')
                        self.client.close()
                        self.reg_access_lock.release()
                        raise ReadError('Maximum MODbus read errors per transaction exceeded.')

                    """
                    Close the connection, re-connect and try again.
                    """
                    try:
                        self.client.close()
                        self.reg_access_lock.release()
                        self.client_connect()
                    except:
                        raise ConnectError('Unable to reconnect to unit after MODbus error')
            finally:
                self.reg_access_lock.release()
                    
            if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                self.logger.critical('Maximum MODbus read errors exceeded.')
                self.client.close()
                raise ReadError('MaximumMODbus read errors exceeded.')
            time.sleep(CESS_MODBUS_DELAY_TIME)
            
        try:
            self.client.close()
            self.reg_access_lock.release()
        except ValueError as error:
            raise ProcessingError(error)

        return response
            
    def write_holding_registers(self, offset=None, contents=None):
        if offset is None or contents is None:
            raise ParameterError('Invalid input parameters.')
        
        try:
            self.client_connect()
        except:
            raise ConnectError('Unable to connect to MODbus slave.')
        
        modbus_errs = 0
        while True:
            try:
                self.client.write_register(offset,
                                           count=1,
                                           value=contents,
                                           unit=CESS_MODBUS_ID)
            except Exception as error:
                err_string = 'write_holding_registers() failed - %s' % repr(error)
                self.logger.error(err_string)
                modbus_errs += 1
                if modbus_errs >= CESS_MAX_MODBUS_ERRORS:
                    self.logger.critical('Maximum MODbus write errors exceeded.')
                    self.client.close()
                    self.reg_access_lock.release()
                    raise WriteError('Maximum MODbus write errors exceeded.')

                """
                Close the connection, re-connect and try again.
                """
                try:
                    self.client.close()
                    self.reg_access_lock.release()
                    self.client_connect()
                except:
                    raise ConnectError('Unable to reconnect to unit after MODbus write error')
            else:
                break

        try:
            self.client.close()
            self.reg_access_lock.release()
        except Exception as error:
            raise ProcessingError(error)

        return True

    def set_voltage_control(self, voltage=None):
        self.logger.debug('set_voltage_control command received...')
    
    def set_frequency_control(self, frequency=None):
        self.logger.debug('set_frequency_control command received...')
        
    def set_work_state_stop(self):
        reg_val = 16 # Jess says so...
        try:
            self.write_holding_registers(CESS_WORK_STATE_CONTROL_CMD_REG_OFFSET, reg_val)
        except:
            raise WriteError('Failed to write to Work State Control register.')
        self.logger.info('Setting work state = STOP')
            
    def set_work_state_run(self):
        reg_val = 64 # Jess says so...
        try:
            self.write_holding_registers(CESS_WORK_STATE_CONTROL_CMD_REG_OFFSET, reg_val)
        except:
            raise WriteError('Failed to write to Work State Control register.')
        self.logger.info('Setting work state = RUN')

    def zero_cross_required(self, proposed_setting=None):
        """
        Retrieve active power command setting to determine the charge/discharge
        state of the unit. 
        """
        query_string = "SELECT %s from %s;" % (ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME,
                                               DB_ESS_MASTER_INFO_TABLE)
        args = []
        try:
            result = prepared_act_on_database(FETCH_ONE, query_string, args)
        except Exception:
            err_string = 'Unable to fetch %s from the database.' % \
                         ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME
            raise DatabaseError(err_string)

        #print 'active power setting = %d' % result[CESS_40_ACTIVE_POWER_CMD_SETTING_COLUMN_NAME]
        
        if result[ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME] == 0:
            """
            System is idle - zero crossing not required in either direction.
            """
            self.logger.debug('System is idle...')
            return False
        elif proposed_setting == 0:
            self.logger.debug('Proposed power setting is zero...')
            return False
            
        active_power_setting = np.int16(result[ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME])
        self.logger.debug('active power setting = %d' % active_power_setting)
        if active_power_setting > 0:
            if proposed_setting < 0:
                self.logger.debug('Active > 0, proposed < 0 ZCR...')
                return True
            else:
                self.logger.debug('Active > 0, proposed > 0...')
                return False
        else:
            if proposed_setting < 0:
                self.logger.debug('active < 0, proposed < 0...')
                return False
            else:
                self.logger.debug('Active < 0, proposed > 0 ZCR...')
                return True
    
    def set_active_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing active power parameter')
        try:
            ipwr = int(round(float(power)))
        except Exception as error:
            raise ParameterError(error)

        if not self.ess_unit.is_capable_of(power):
            self.logger.debug("Requested active_power=%s cannot be fulfilled: capability='%s'" %
                              (power, self.ess_unit.capability))
            return

        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(ACTIVE_POWER_MIN) or ipwr > np.int16(ACTIVE_POWER_MAX):
            raise ParameterError('Out-of-range parameter')


        if self.zero_cross_required(ipwr):
            """
            @note: we don't bother to update the database for a zero-crossing operation.
                   After all, the zero is only going to be there for 250ms.
            """
            self.logger.info('Setting active power to 0kW for zero-crossing')
            try:
                self.write_holding_registers(CESS_ACTIVE_POWER_CMD_REG_OFFSET, 0)
            except Exception as error:
                self.logger.error(error)
                raise WriteError('Failed to write to Active Power register.')
            time.sleep(ZERO_CROSSING_DELAY) # Let machine settle
            
        self.logger.info('Setting active power to %dkW' % ipwr)
        try:
            self.write_holding_registers(CESS_ACTIVE_POWER_CMD_REG_OFFSET, pwr)
        except Exception as error:
            self.logger.error(error)
            raise WriteError('Failed to write to Active Power register.')

    def set_reactive_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing reactive power parameter')
        try:
            ipwr = int(round(float(power)))
        except Exception as error:
            raise ParameterError(error)
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(REACTIVE_POWER_MIN) or ipwr > np.int16(REACTIVE_POWER_MAX): 
            raise ParameterError('Out-of-range reactive power parameter')              
        try:
            self.write_holding_registers(CESS_REACTIVE_POWER_CMD_REG_OFFSET, pwr)
        except:
            raise WriteError('Failed to write to Reactive Power register.')
        self.logger.info('Setting reactive power to %dkW' % ipwr)
    
    def set_diesel_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing Diesel mode parameter')
        self.logger.info('Setting Diesel mode %s' % mode)
        if mode.lower() == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for Diesel mode')
        try:
            self.set_work_mode_control_cmd_reg(DIESEL_MODE_CMD_BIT, bit_value)
        except:
            raise

    def set_smooth_pv_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing smooth PV mode parameter')
        self.logger.info('Setting smooth PV mode %s' % mode)
        if mode.lower() == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for smooth PV mode')
        try:
            self.set_work_mode_control_cmd_reg(SMOOTH_PV_CMD_BIT, bit_value)
        except:
            raise
    
    def set_gmlo_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing GMLO mode parameter')
        self.logger.info('Setting GMLO mode %s' % mode)
        if mode.lower() == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for GMLO mode')
        try:
            self.set_work_mode_control_cmd_reg(GMLO_CMD_BIT, bit_value)
        except:
            raise
    
    def set_gmlp_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing GMLP mode parameter')
        self.logger.info('Setting GMLP mode %s' % mode)
        if mode == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for GMLP mode')
        try:
            self.set_work_mode_control_cmd_reg(GMLP_CMD_BIT, bit_value)
        except:
            raise
    
    def set_dispatch_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing Dispatch mode parameter')
        self.logger.info('Setting Dispatch mode %s' % mode)
        if mode == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for dispatch mode')
        try:
            self.set_work_mode_control_cmd_reg(DISPATCH_CMD_BIT, bit_value)
        except:
            raise
    
    def set_capacity_testing_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing capacity testing mode parameter')
        self.logger.info('Setting Capacity Testing mode %s' % mode)
        if mode == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for capacity testing mode')
        try:
            self.set_work_mode_control_cmd_reg(CAPACITY_TESTING_CMD_BIT, bit_value)
        except:
            raise
    
    def set_load_shifting_mode(self, mode=None):
        if mode is None or mode == '':
            raise ParameterError('Missing load shifting mode parameter')
        self.logger.info('Setting Load Shifting mode %s' % mode)
        if mode == 'on':
            bit_value = 1
        elif mode.lower() == 'off':
            bit_value = 0
        else:
            raise ParameterError('Invalid setting for load shifting mode')
        try:
            self.set_work_mode_control_cmd_reg(LOAD_SHIFTING_CMD_BIT, bit_value)
        except:
            raise
    
    def set_work_mode_control_cmd_reg(self, bit_pos, bit_value):
        try:
            response = self.read_holding_registers(CESS_WORK_MODE_CONTROL_CMD_REG_OFFSET, count=1)
        except Exception as error:
            self.logger.error('Error reading work mode control command registers - %s' % error)
            raise
        if bit_value == 1:
            new_reg_val = self.set_bit(response.registers[0], bit_pos)
        else:
            new_reg_val = self.clear_bit(response.registers[0], bit_pos)
        try:
            self.write_holding_registers(CESS_WORK_MODE_CONTROL_CMD_REG_OFFSET, new_reg_val)
        except:
            raise WriteError('Failed to write to Work Mode Control register.')
        
    def clear_alerts(self):
        self.logger.info('Clearing alerts.')
        try:
            self.write_holding_registers(CESS_CLEAR_ABNORMITY_REG_OFFSET, 1)
        except:
            raise WriteError('Failed to clear alerts.')
    
    def set_power_decrease_time(self, seconds=None):
        if seconds is None or seconds == '':
            raise ParameterError('Missing power decrease time parameter')
        try:
            secs = int(seconds)
        except ValueError as error:
            raise ParameterError(str(error))
        if secs < POWER_DECREASE_TIME_MIN_SEC or secs > POWER_DECREASE_TIME_MAX_SEC:
            raise ParameterError('Out-of-range parameter')              
        self.logger.info('Setting power decrease time to %d seconds' % secs)
        try:
            self.write_holding_registers(CESS_POWER_DECREASE_TIME_CMD_REG_OFFSET, secs)
        except:
            raise WriteError('Failed to write to power decrease time register.')
    
    def set_power_rise_time(self, seconds=None):
        if seconds is None or seconds == '':
            raise ParameterError('Missing power rise time parameter')
        try:
            secs = int(seconds)
        except ValueError as error:
            raise ParameterError(str(error))
        if secs < POWER_RISE_TIME_MIN_SEC or secs > POWER_RISE_TIME_MAX_SEC:
            raise ParameterError('Out-of-range parameter')  
        self.logger.info('Setting power rise time to %d seconds' % secs)
        try:
            self.write_holding_registers(CESS_POWER_RISE_TIME_CMD_REG_OFFSET, secs)
        except:
            raise WriteError('Failed to write to power rise time register.')

    def set_gmlo_gmlp_start_soc(self, percentage=None):
        if percentage is None or percentage == '':
            raise ParameterError('Missing GMLO/GMLP start SOC percentage parameter')
        try:
            percent = float(percentage)/100.0
        except ValueError as error:
            raise ParameterError(str(error))
        usint = int(percent)
        self.logger.info('Setting gmlo/gmlp SOC to %2.2f' % percent)
        try:
            self.write_holding_registers(CESS_GMLO_GMLP_START_SOC_REG_OFFSET, usint)
        except:
            raise WriteError('Failed to write to gmlo/gmlp SOC register.')
    
    def set_gmlp_power_control_soc(self, percentage=None):
        if percentage is None or percentage == '':
            raise ParameterError('Missing GMLP power control SOC percentage parameter')
        try:
            percent = float(percentage)/100.0
        except ValueError as error:
            raise ParameterError(str(error))
        usint = int(percent)
        self.logger.info('Setting GMLP power control SOC to %2.2f' % percent)
        try:
            self.write_holding_registers(CESS_GMLP_POWER_CNTRL_SOC_CMD_REG_OFFSET, usint)
        except:
            raise WriteError('Failed to write to gmlp power control SOC register.')
    
    def set_capacity_test_target_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing capacity test target power parameter')
        try:
            ipwr = int(power)
        except Exception as error:
            raise ParameterError(error)
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(CAPACITY_TEST_TARGET_POWER_MIN) or \
           ipwr > np.int16(CAPACITY_TEST_TARGET_POWER_MAX):
            raise ParameterError('Out-of-range parameter')  
        self.logger.info('Setting Capacity test power to %dkW' % ipwr)
        try:
            self.write_holding_registers(CESS_CAPACITY_TEST_TARGET_POWER_CMD_REG_OFFSET, pwr)
        except:
            raise WriteError('Failed to write to CApacity Test Target Power register.')

    def set_diesel_target_power(self, power=None):
        if power is None or power == '':
            raise ParameterError('Missing Diesel target power parameter')
        try:
            ipwr = int(power)
        except Exception as error:
            raise ParameterError(error)
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(DIESEL_TARGET_POWER_MIN) or ipwr > np.int16(DIESEL_TARGET_POWER_MAX):
            raise ParameterError('Out-of-range parameter')  
        self.logger.info('Setting Diesel power to %dkW' % pwr)
        try:
            self.write_holding_registers(CESS_DIESEL_TARGET_POWER_CMD_REG_OFFSET, pwr)
        except:
            raise WriteError('Failed to write to Diesel Power register.')

    def set_discharge_begin_time(self, time_param=None):
        if time_param is None or time_param == '':
            raise ParameterError('Missing discharge begin time parameter')
        try:
            time = parser.parse(time_param)
        except ValueError as error:
            self.logger.warning('Invalid time parameter - %s' % error)
            raise ParameterError('Invalid time parameter.')
        
        info_string = 'Setting discharge begin time to %s:%s' % (time.hour, time.minute)
        self.logger.info(info_string)
        try:
            self.write_holding_registers(CESS_DISCHARGE_BEGIN_TIME_HOUR_CMD_REG_OFFSET,
                                         int(time.hour))
        except:
            raise WriteError('Failed to write to Discharge Begin Time hour register.')
        try:
            self.write_holding_registers(CESS_DISCHARGE_BEGIN_TIME_MIN_CMD_REG_OFFSET,
                                         int(time.minute))
        except:
            raise WriteError('Failed to write to Discharge Begin Time minute register.')

    def set_discharge_end_time(self, time_param=None):
        if time_param is None or time_param == '':
            raise ParameterError('Missing discharge end time parameter')
        try:
            time = parser.parse(time_param)
        except ValueError as error:
            self.logger.warning('Invalid time parameter - %s' % error)
            raise ParameterError('Invalid time parameter.')            
        info_string = 'Setting discharge end time to %s:%s' % (time.hour, time.minute)
        self.logger.info(info_string)
        try:
            self.write_holding_registers(CESS_DISCHARGE_END_TIME_HOUR_CMD_REG_OFFSET,
                                         int(time.hour))
        except:
            raise WriteError('Failed to write to Discharge End Time hour register.')
        try:
            self.write_holding_registers(CESS_DISCHARGE_END_TIME_MIN_CMD_REG_OFFSET,
                                         int(time.minute))
        except:
            raise WriteError('Failed to write to Discharge End Time minute register.')

    def set_timed_discharge_power(self, power):
        if power is None or power == '':
            raise ParameterError('Missing discharge power parameter')
        try:
            pwr = int(power)
        except ValueError as error:
            raise ParameterError(str(error))
        if pwr < DISCHARGE_POWER_MIN or pwr > DISCHARGE_POWER_MAX:
            raise ParameterError('Out-of-range parameter')  
        self.logger.info('Setting discharge power to %dkW' % pwr)
        try:
            self.write_holding_registers(CESS_DISCHARGE_POWER_CMD_REG_OFFSET, pwr)
        except:
            raise WriteError('Failed to write to Discharge Power register.')
        
    def set_charge_begin_time(self, time_param=None):
        if time_param is None or time_param == '':
            raise ParameterError('Missing charge begin time parameter')
        try:
            time = parser.parse(time_param)
        except ValueError as error:
            self.logger.warning('Invalid time parameter - %s' % error)
            raise ParameterError('Invalid time parameter.')
        info_string = 'Setting charge begin time to %s:%s' % (time.hour, time.minute)
        self.logger.info(info_string)
        try:
            self.write_holding_registers(CESS_CHARGE_BEGIN_TIME_HOUR_CMD_REG_OFFSET,
                                         int(time.hour))
        except:
            raise WriteError('Failed to write to Charge Begin Time hour register.')
        try:
            self.write_holding_registers(CESS_CHARGE_BEGIN_TIME_MIN_CMD_REG_OFFSET,
                                         int(time.minute))
        except:
            raise WriteError('Failed to write to Charge Begin Time minute register.')

    def set_charge_end_time(self, time_param=None):
        if time_param is None or time_param == '':
            raise ParameterError('Missing charge end time parameter')
        try:
            time = parser.parse(time_param)
        except ValueError as error:
            self.logger.warning('Invalid time parameter - %s' % error)
            raise ParameterError('Invalid time parameter.')
        info_string = 'Setting charge end time to %s:%s' % (time.hour, time.minute)
        self.logger.info(info_string)
        try:
            self.write_holding_registers(CESS_CHARGE_END_TIME_HOUR_CMD_REG_OFFSET,
                                         int(time.hour))
        except:
            raise WriteError('Failed to write to Charge End Time hour register.')
        try:
            self.write_holding_registers(CESS_CHARGE_END_TIME_MIN_CMD_REG_OFFSET,
                                         int(time.minute))
        except:
            raise WriteError('Failed to write to Charge End Time minute register.')

    def set_timed_charge_power(self, power):
        if power is None or power == '':
            raise ParameterError('Missing charge power parameter')
        try:
            ipwr = int(power)
        except Exception as error:
            raise ParameterError(error)
        pwr = (ipwr & 0xFFFF)
        if ipwr < np.int16(CHARGE_POWER_MIN) or ipwr > np.int16(CHARGE_POWER_MAX):
            raise ParameterError('Out-of-range parameter')
        self.logger.info('Setting charge power to %dkW' % ipwr)
        try:
            self.write_holding_registers(CESS_CHARGE_POWER_CMD_REG_OFFSET, pwr)
        except:
            raise WriteError('Failed to write to Charge Power register.')

    def enable_timed_operation(self, enable=None):
        if enable is None or enable == '':
            raise ParameterError('Missing enable? parameter')
        ienable = int(enable)
        self.logger.info('Setting CfgConfig to %s' % enable)
        try:
            self.write_holding_registers(CESS_CFG_CONFIG_CMD_REG_OFFSET, ienable)
        except:
            raise WriteError('Failed to write to CfgConfig register.')
        
    def write_work_state_control_cmd(self):
        old_register = 0
        new_register = self.set_bit(old_register, CESS_WORK_STATE_STOP_CMD_BIT)
        try:
            self.write_holding_registers(CESS_WORK_STATE_CONTROL_CMD_REG_OFFSET,
                                         new_register)
        except:
            raise WriteError('Failed to write to Work State Control register.')
        
    def get_active_power_setting(self):
        query_string = 'SELECT %s FROM %s;' % (ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME,
                                               DB_ESS_MASTER_INFO_TABLE)
        args = []
        try:
            result = prepared_act_on_database(FETCH_ONE, query_string, args)
        except:
            raise
        
        return result[ESS_MASTER_INFO_ACTIVE_POWER_CMD_COLUMN_NAME]
    
    def set_battery_mode(self, mode=None):
        mode_list = ['manual', 'auto']
        
        if mode not in mode_list:
            mode = 'manual' 
        sql = "UPDATE %s SET %s = %s WHERE %s = %s;" % \
              (DB_ESS_UNITS_TABLE, ESS_UNITS_MODE_COLUMN_NAME, "%s",
               ESS_UNITS_NAME_COLUMN_NAME, "%s")
        args = [mode,'BYD-CESS 40']
        try:
            prepared_act_on_database(EXECUTE, sql, args)
        except:
            raise
