#!/usr/share/apparent/.py2-virtualenv/bin/python
"""

DEPRECATED

@module: ESS Alarm

@description: Process alarms for the ESS unit.

@copyright: Apparent Inc. 2018

@reference:

@author: steve

@created: February 20, 2018
"""
from datetime import datetime
from dateutil import parser
from zmq.backend.cython.utils import ZMQError
import zmq
import time

from alarm.alarm import Alarm
from alarm.alarm_constants import \
    ALARM_ID_CESS_400_MAX_CELL_VOLTAGE_ALARM, \
    ALARM_ID_CESS_400_MIN_CELL_VOLTAGE_ALARM, \
    ALARM_ID_BYD_CESS_400_DISCHARGE_LIMIT_REACHED, \
    ALARM_ID_BYD_CESS_400_CHARGE_LIMIT_REACHED
from lib.gateway_constants.BydCess400Constants import *
from lib.gateway_constants.ZmqConstants import ESS_ALARM_URL
from lib.db import prepared_act_on_database, FETCH_ONE

import subprocess as sub
from event.event_constants import EVENT_TAG_CESS_400
from ess.ess_daemon import EssDaemon

CESS_MAX_DB_ERRORS = 5
CESS_UPDATE_INTERVAL = 5
CESS_DEFAULT_POLLING_FREQ = 10


class EssAlarmDaemon(EssDaemon):
    polling_freq_sec = CESS_DEFAULT_POLLING_FREQ

    def __init__(self, **kwargs):
        super(EssAlarmDaemon, self).__init__(**kwargs)
        self.next_heartbeat = datetime.now()
        self.db_errors = 0
        self.next_scan = datetime.now()
        self.soc_charge_limit = None
        self.soc_discharge_limit = None
        self.soc_charge_limit_response = None
        self.soc_discharge_limit_response = None
        self.max_cell_voltage_limit = None
        self.min_cell_voltage_limit = None
        self.max_cell_voltage_limit_response = None
        self.min_cell_voltage_limit_response = None
        self.limit_response_suppressed = False
        self.max_soc_response_suppressed = False
        self.min_soc_response_suppressed = False
        self.last_master_info_timestamp = datetime.now()

        self.command_dict = {'refetch': self.get_run_time_parameters,
                             'suppress': self.suppress_limit_responses,
                             'unsuppress': self.unsuppress_limit_responses,
                             'suppress_min_soc' : self.suppress_min_soc_response,
                             'suppress_max_soc' : self.suppress_max_soc_response,
                             'unsuppress_min_soc' : self.unsuppress_min_soc_response,
                             'unsuppress_max_soc' : self.unsuppress_max_soc_response }

    def get_run_time_parameters(self):
        self.logger.debug('Fetching run-time parameters...')
        self.ess_unit.reload()

    def data_is_new(self):
        sql = "SELECT %s FROM %s;" % (ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME,
                                      DB_ESS_MASTER_INFO_TABLE)
        args = []
        result = prepared_act_on_database(FETCH_ONE, sql, args)

        if result is None or result[ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME] is None:
            # Assume there is no valid data yet.
            return False

        master_timestamp = parser.parse(result[ESS_MASTER_INFO_TIMESTAMP_COLUMN_NAME])
        if self.last_master_info_timestamp < master_timestamp:
            self.last_master_info_timestamp = master_timestamp
            return True
        else:
            return False
        
    def soc_discharge_limit_active(self):
        return self.soc_discharge_limit is not None and self.soc_discharge_limit_response is not None

    def soc_charge_limit_active(self):
        return self.soc_charge_limit is not None and self.soc_charge_limit_response is not None

    def monitor_soc(self):
        sql = "SELECT %s from %s;" % (ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME,
                                      DB_ESS_MASTER_INFO_TABLE)
        args = []
        result = prepared_act_on_database(FETCH_ONE, sql, args)

        soc_percentage = result[ESS_MASTER_INFO_BATTERY_STACK_SOC_COLUMN_NAME] / 100.0
        if self.soc_discharge_limit_active() and soc_percentage <= self.soc_discharge_limit:
            if self.min_soc_response_suppressed:
                # The soft-limit for discharge has been suppressed.  Nothing else to do.
                return True
            """
            We are at the discharge limit - send the provisioned response to the Controller.
            """
            alarm = Alarm.find(ALARM_ID_BYD_CESS_400_DISCHARGE_LIMIT_REACHED)
            if not isinstance(alarm, Alarm):
                self.logger.warning('Discharge limit reached, sending provisioned response.',
                                    extra={'event_tags': [EVENT_TAG_CESS_400]})
                Alarm.occurrence(ALARM_ID_BYD_CESS_400_DISCHARGE_LIMIT_REACHED)

            if not self.limit_response_suppressed:
                p = sub.Popen(self.soc_discharge_limit_response.split(" "), stdout=sub.PIPE, stderr=sub.PIPE)
                output, errors = p.communicate()

        elif self.soc_charge_limit_active() and soc_percentage >= self.soc_charge_limit:
            if self.max_soc_response_suppressed:
                # The soft-limit for charge has been suppressed.  Nothing else to do.
                return True
            """
            We are at the charge limit - send the provisioned response to the Controller.
            """
            alarm = Alarm.find(ALARM_ID_BYD_CESS_400_CHARGE_LIMIT_REACHED)
            if not isinstance(alarm, Alarm):
                # Alarm does not exist, therefore it has not been raised.
                self.logger.warning('Charge limit reached, sending provisioned response.',
                                    extra={'event_tags': [EVENT_TAG_CESS_400]})
                Alarm.occurrence(ALARM_ID_BYD_CESS_400_CHARGE_LIMIT_REACHED)

            if not self.limit_response_suppressed:
                p = sub.Popen(self.soc_charge_limit_response.split(" "), stdout=sub.PIPE, stderr=sub.PIPE)
                output, errors = p.communicate()
        else:
            '''
            We are in a safe SOC range.  Reset max/min_soc_response_suppressed variables.
            '''
            if self.max_soc_response_suppressed or self.min_soc_response_suppressed:
                self.logger.debug('Reseting max/min_soc_response_suppressed variables.')
                self.max_soc_response_suppressed = False
                self.min_soc_response_suppressed = False

        return True

    def suppress_limit_responses(self):
        self.limit_response_suppressed = True

    def unsuppress_limit_responses(self):
        self.limit_response_suppressed = False
    
    def min_cell_voltage_limit_active(self):
        return self.min_cell_voltage_limit is not None and self.min_cell_voltage_limit_response is not None

    def max_cell_voltage_limit_active(self):
        return self.max_cell_voltage_limit is not None and self.max_cell_voltage_limit_response is not None

    def suppress_min_soc_response(self):
        self.logger.debug('Minimum SOC limit suppressed!')
        self.min_soc_response_suppressed = True
        
    def suppress_max_soc_response(self):
        self.max_soc_response_suppressed = True

    def unsuppress_min_soc_response(self):
        self.min_soc_response_suppressed = False
        
    def unsuppress_max_soc_response(self):
        self.max_soc_response_suppressed = False

    def monitor_cell_voltages(self):
        """
        @note: this function gives preference to the min voltage
        """
        sql = "SELECT %s, %s from %s;" % \
              (ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME,
               ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME,
               DB_ESS_MASTER_INFO_TABLE)
        args = []
        results = prepared_act_on_database(FETCH_ALL, sql, args)

        results_dict = results[0]
        max_cell_voltage = results_dict[ESS_MASTER_INFO_BATTERY_MAX_CELL_VOLTAGE_COLUMN_NAME]
        min_cell_voltage = results_dict[ESS_MASTER_INFO_BATTERY_MIN_CELL_VOLTAGE_COLUMN_NAME]
        if self.min_cell_voltage_limit_active() and min_cell_voltage <= self.min_cell_voltage_limit:
            alarm = Alarm.find(ALARM_ID_CESS_400_MIN_CELL_VOLTAGE_ALARM)
            if not isinstance(alarm, Alarm):
                # Alarm does not exist, therefore it has not been raised.
                self.logger.warning('Minimum cell voltage limit reached, sending provisioned response.',
                                    extra={'event_tags': [EVENT_TAG_CESS_400]})
                Alarm.occurrence(ALARM_ID_CESS_400_MIN_CELL_VOLTAGE_ALARM)

            if not self.limit_response_suppressed:
                p = sub.Popen(self.min_cell_voltage_limit_response.split(" "))
                output, errors = p.communicate()

        elif self.max_cell_voltage_limit_active() and max_cell_voltage >= self.max_cell_voltage_limit:
            alarm = Alarm.find(ALARM_ID_CESS_400_MAX_CELL_VOLTAGE_ALARM)
            if not isinstance(alarm, Alarm):
                # Alarm does not exist, therefore it has not been raised.
                self.logger.warning('Maximum cell voltage limit reached, sending provisioned response.',
                                    extra={'event_tags': [EVENT_TAG_CESS_400]})
                Alarm.occurrence(ALARM_ID_CESS_400_MAX_CELL_VOLTAGE_ALARM)

            if not self.limit_response_suppressed:
                p = sub.Popen(self.max_cell_voltage_limit_response.split(" "), stdout=sub.PIPE, stderr=sub.PIPE)
                output, errors = p.communicate()

        else:
            pass

    def process_alarms(self):
        # if not self.data_is_new():
        #    return True

        self.monitor_soc()
        self.monitor_cell_voltages()

    def run(self):
        self.update()

        # Get the run-time parameters.
        self.get_run_time_parameters()

        try:
            zctx = zmq.Context()
            server = zctx.socket(zmq.REP)
            server.bind(ESS_ALARM_URL)
            poller = zmq.Poller()
            poller.register(server, zmq.POLLIN)
        except ZMQError:
            raise

        """
        Main monitor loop.
        """
        while True:
            start = time.time()
            self.update()

            # check if we need to suppress or unsuppress responses
            self.ess_unit.reload()
            if self.ess_unit.mode == "auto":
                # we suppress limit responses when in auto mode
                # because battery regulator watches limits
                self.suppress_limit_responses()
            else:
                self.unsuppress_limit_responses()

            try:
                zsocks = dict(poller.poll(self.polling_freq_sec * 1000))
            except Exception as error:
                str_err = repr(error)
                self.logger.error(str_err)
                continue

            if server in zsocks and zsocks[server] == zmq.POLLIN:
                request = server.recv()
                command, parameter = request.split("|")
                try:
                    if parameter == 'None':
                        self.command_dict[command]()
                    else:
                        self.command_dict[command](parameter)
                except KeyError as error:
                    str_err = 'Invalid key: %s' % repr(error.message)
                    self.logger.error(str_err)
                    server.send(str_err)
                    continue
                except ParameterError as error:
                    str_err = repr(error.message)
                    self.logger.error(str_err)
                    server.send(str_err)
                    raise
                except WriteError as error:
                    self.logger.error('MODbus write failed.')
                    str_err = repr(error.message)
                    server.send(str_err)
                    continue
                except TypeError as error:
                    self.logger.error(error)
                    str_err = str(error)
                    server.send(str_err)
                    continue
                except DatabaseError as error:
                    self.logger.error(error)
                    str_err = str(error)
                    server.send(str_err)
                    raise
                except NotSupportedError as error:
                    self.logger.warning(error)
                    str_err = str(error)
                    server.send(str_err)
                    continue
                except Exception as error:
                    self.logger.error('Unable to process command - %s' % error)
                    server.send("0")
                    continue
                # Return the command to the sender to signify all is well.
                server.send(command)

            self.process_alarms()

            done = time.time()
            elapsed = done - start
            print('End of main loop', elapsed)

        server.close()
        zctx.term()
