#!/usr/share/apparent/.py2-virtualenv/bin/python

"""
@module: dischargerCLI - Discharger Command Line Interface

@description: This module is a script designed to send a command to Battery Inverters.  
              It is invoked from the GUI and requires the following syntax:
              
              DischargerCLI --pcs-unit-id [<parameter>]
                            --active-power [<parameter>]
                            --help will print the usage information

@copyright: Apparent Inc. 2018

@author: Patrice

@created: October 9, 2018
"""

import argparse
import zmq
import time
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME


def extract_cmd(clargs):
    args_dict = vars(clargs)
    pcs_unit_id = args_dict.pop('pcs_unit_id')
    for command, param in args_dict.iteritems():
        if param is not None and param is not False:
            return pcs_unit_id, command, param
    return pcs_unit_id, None, None


"""
Time to figure out what the user wants...
"""
parser = argparse.ArgumentParser(description='CLI for the Discharger Controller')
parser.add_argument("--pcs-unit-id", help="PCS unit id")
parser.add_argument("--active-power", nargs=2, help="Set active power <feed-name> <active_power:float>")
clargs = parser.parse_args()

pcs_unit_id, command, param = extract_cmd(clargs)
if command is None or param is None:
    print 'None or invalid command given'
    exit(1)
param_str = " ".join(param)
cmd_str = str(command)
cmd = "|".join([cmd_str, param_str])
print 'CLI command: %s' % cmd

context = zmq.Context(1)
pub_socket = context.socket(zmq.PUB)
pub_socket.hwm = 100
pub_socket.setsockopt(zmq.LINGER, 0)
pub_socket.connect(MSG_BROKER_XSUB_URL)

time.sleep(PUB_CONNECT_WAIT_TIME)

topic = "pcs_unit%d#" % pcs_unit_id
pub_socket.send_multipart([topic, cmd])
pub_socket.close()
context.term()

exit(0)
