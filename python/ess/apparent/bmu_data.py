from lib.model import Model
from lib.gateway_constants.DBConstants import DB_AESS_BMU_DATA_TABLE_NAME, \
                                              AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_REMAINING_CAPACITY_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_FULL_CHARGED_CAPACITY_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_DESIGN_CAPACITY_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_CYCLE_COUNT_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME, \
                                              AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME

from lib.gateway_constants.EveBmuConstants import EVE_BMU_UNINITIALIZED_STATE
from lib.db import prepared_act_on_database, FETCH_ALL, get_default


class BmuData(Model):
    sql_table = DB_AESS_BMU_DATA_TABLE_NAME
    id_field = AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME

    def __init__(self, **kwargs):
        self.bmu_id = kwargs.get(AESS_BMU_DATA_TABLE_BMU_ID_COLUMN_NAME, None)
        self.state = get_default(kwargs, AESS_BMU_DATA_TABLE_STATE_COLUMN_NAME, EVE_BMU_UNINITIALIZED_STATE)
        self.current = kwargs.get(AESS_BMU_DATA_TABLE_CURRENT_COLUMN_NAME, None)
        self.voltage = kwargs.get(AESS_BMU_DATA_TABLE_VOLTAGE_COLUMN_NAME, None)
        self.soc = kwargs.get(AESS_BMU_DATA_TABLE_SOC_COLUMN_NAME, None)
        self.soh = kwargs.get(AESS_BMU_DATA_TABLE_SOH_COLUMN_NAME, None)
        self.remaining_capacity = kwargs.get(AESS_BMU_DATA_TABLE_REMAINING_CAPACITY_COLUMN_NAME, None)
        self.full_charged_capacity = kwargs.get(AESS_BMU_DATA_TABLE_FULL_CHARGED_CAPACITY_COLUMN_NAME, None)
        self.design_capacity = kwargs.get(AESS_BMU_DATA_TABLE_DESIGN_CAPACITY_COLUMN_NAME, None)
        self.cycle_count = kwargs.get(AESS_BMU_DATA_TABLE_CYCLE_COUNT_COLUMN_NAME, None)
        self.cell_voltages = []
        for cell in range(1, 17):
            self.cell_voltages.append(kwargs.get('cell_voltage_%s' % cell))
        self.cell_temperatures = []
        for cell in range(1, 5):
            self.cell_temperatures.append(kwargs.get('cell_temperature_%s' % cell))
        self.mosfet_temperature = kwargs.get(AESS_BMU_DATA_TABLE_MOSFET_TEMPERATURE_COLUMN_NAME, None)
        self.environment_temperature = kwargs.get(AESS_BMU_DATA_TABLE_ENVIRONMENT_TEMPERATURE_COLUMN_NAME, None)

    @classmethod
    def find_by_ess_unit(cls, ess_unit):
        if ess_unit is None:
            raise Exception("Must specify an ESS unit.")
        sql = "SELECT * FROM aess_bmu_data WHERE bmu_id IN (%s)" % ess_unit.get_bmu_ids()
        results = prepared_act_on_database(FETCH_ALL, sql, [])
        bmu_data = []
        if results:
            for result in results:
                bmu_data.append(cls(**result))

        return bmu_data
