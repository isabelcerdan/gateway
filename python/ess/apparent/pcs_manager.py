from ess.apparent.pcs_unit import PcsUnit
from lib.logging_setup import setup_logger


class PcsManager(object):
    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.ess_unit = kwargs.get('ess_unit', None)
        self.feed_name = kwargs.get('feed_name', "Aggregate")
        self.pcs_units = PcsUnit.find_all_by(ess_unit_id=self.ess_unit.id)

    def start(self):
        """
        Start all PCS units in ESS unit.
        :return:
        """
        for pcs_unit in self.pcs_units:
            pcs_unit.start()

    def stop(self):
        """
        Stop all PCS units in ESS unit.
        :return:
        """
        for pcs_unit in self.pcs_units:
            pcs_unit.stop()

    def derive_charge_per_unit(self, active_power, available_units, unavailable_units):
        """
        Order units by SOC and remove highest SOC one at a time until charge
        per unit is above minimum current needed for all remaining chargers.
        :param active_power: kW float (+discharge/-charge)
        :param available_units: List of PcsUnit objects available to fulfill active_power.
        :param unavailable_units: List of PcsUnit objects unavailable to fulfill active_power.
        :return:
        """
        active_power_per_unit = 0
        available_units.sort(key=lambda u: u.soc())
        while len(available_units) > 0:
            active_power_per_unit = active_power / len(available_units)
            if any([abs(active_power_per_unit) < u.min_charging_power() for u in available_units]):
                unavailable_units.append(available_units[-1])
                available_units = available_units[:-1]
            else:
                break
        return active_power_per_unit

    def reload(self):
        """
        Reload PcsUnit data from database.
        :return:
        """
        for pcs_unit in self.pcs_units:
            pcs_unit.reload()

    def set_active_power(self, active_power):
        """
        :param active_power:   (+discharge/-charge) float
        :return:
        """

        available_units, unavailable_units = PcsUnit.units_available_for(self.pcs_units, active_power)

        # evenly divide active power across available units
        active_power_per_unit = 0
        if len(available_units) == 0:
            self.logger.warning('Zero PCS Units available for active power command.')
        else:
            if active_power < 0:
                active_power_per_unit = self.derive_charge_per_unit(active_power, available_units, unavailable_units)
            else:
                active_power_per_unit = active_power / len(available_units)

            self.logger.debug('%d PCS units available for active power command...' % len(available_units))
            self.logger.debug('%f per available unit...' % active_power_per_unit)

        for pcs_unit in available_units:
            pcs_unit.set_active_power(active_power_per_unit)
        for pcs_unit in unavailable_units:
            pcs_unit.set_active_power(0)
