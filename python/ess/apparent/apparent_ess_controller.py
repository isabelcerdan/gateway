from lib.gateway_constants.BydCess400Constants import *

from lib.logging_setup import *
from ess.apparent.discharger import Discharger
from ess.artesyn_charger.artesyn_charger_relays import ArtesynChargerRelays
from ess.ess_driver import EssDriver


class ApparentEssDriver(EssDriver):
    def __init__(self, **kwargs):
        super(ApparentEssDriver, self).__init__(**kwargs)
        self.discharger = Discharger()
        self.logger.info('Discharger.feed_name = %s' % self.discharger.feed_name)
        self.charger = ArtesynChargerRelays.find_by()
        self.work_state = None
        self.active_power = 0
        self.reactive_power = 0
        self.set_work_state_stop()

    def set_work_state_stop(self):
        self.work_state = "Stopped"
        self.discharger.stop()
        self.charger.stop()
        self.save(unit_work_state="Stopped", active_power_cmd=0)

    def set_work_state_run(self):
        self.work_state = "Running"
        self.discharger.start()
        self.charger.start()
        self.save(unit_work_state="Running", active_power_cmd=0)

    def set_active_power(self, power=None):
        """
        :param power:   (+discharge/-charge) float
        :return:
        """
        if self.work_state == "Running":
            if self.ess_unit.is_capable_of(power):
                self.charger.set_active_power(power)
                self.logger.info('set_active_power: feed = %s' % self.ess_unit.feed_name)
                self.discharger.set_active_power(power)
                self.save(active_power_cmd=power)
            else:
                self.logger.debug("Requested active_power=%s cannot be fulfilled: capability='%s'" %
                                  (power, self.ess_unit.capability))
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_active_power command" %
                              self.work_state)
            return

    def set_reactive_power(self, power=None):
        if self.work_state == "Running":
            pass
            self.save(reactive_power_cmd=power)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_reactive_power command" %
                              self.work_state)
            return

    def set_voltage_control(self, voltage=None):
        if self.work_state == "Running":
            pass
            self.save(voltage_control_cmd=voltage)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_voltage_control command" %
                              self.work_state)
            return

    def set_frequency_control(self, frequency=None):
        if self.work_state == "Running":
            pass
            self.save(frequency_control_cmd=frequency)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_frequency_control command" %
                              self.work_state)
            return

    def save(self, **kwargs):
        params = ", ".join(["%s=%s" % (k, "%s") for k, _ in kwargs.iteritems()])
        values = [v for _, v in kwargs.iteritems()]
        sql = "UPDATE %s SET %s WHERE ess_id = %s" % (DB_ESS_MASTER_INFO_TABLE, params, self.ess_unit.id)
        prepared_act_on_database(EXECUTE, sql, values)
