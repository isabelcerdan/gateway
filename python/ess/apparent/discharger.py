"""
@module: discharger.py

@description: Discharger is the 'publisher' for commands to the inverter groups.

@copyright: Apparent Energy Inc.  2018
"""
import zmq
from zmq.backend.cython.utils import ZMQError
import time
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XSUB_URL, PUB_CONNECT_WAIT_TIME
from lib.logging_setup import setup_logger
import json
from lib.exceptions import ConnectError


class Discharger(object):
    POLLING_FREQ_SEC = 1
    REQUEST_RETRIES = 3
    REQUEST_TIMEOUT = 1000
    PUBLISHER_HWM = 100
    MAX_ZMQ_ERRORS = 5
    SETTLE_WAIT_TIME = 1.0

    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.pcs_unit_id = kwargs.get('pcs_unit_id', None)
        self.ess_unit = kwargs.get('ess_unit', None)
        if not self.pcs_unit_id:
            # System needs to be configured ...
            self.logger.info('pcs_unit_id None - set to 0')
            self.pcs_unit_id = 0
        self.topic = 'pcs_unit%d#' % int(self.pcs_unit_id)
        self.feed_name = 'aggregate'
        self.logger.info('Discharger: feed_name = %s' % self.feed_name)
        self.ctx = None
        self.pub_socket = None

    def start(self):
        self.set_active_power(0.0)

    def stop(self):
        self.set_active_power(0.0)

    def set_active_power(self, active_power):
        active_power = max(active_power, 0)
        command = ["active_power", str(active_power)]
        try:
            self.send(command)
        except:
            raise

    def send(self, cmd):
        """
        :param cmd: a list of strings starting with command followed by parameters.
        :return:
        """
        if self.ctx is None:
            try:
                self.ctx = zmq.Context(1)            
            except ZMQError as error:
                self.logger.exception(error)
                raise Exception(error)
    
            """
            Socket to send messages to the Gateway Broker.
            """
            connect_err_cnt = 0
            while True:
                try:
                    """
                    @note: When setting up the publisher socket, set the LINGER option
                           to zero, to avoid having the process hanging around
                           trying to deliver messages after it's socket has been closed.
                           
                           Set the high water mark for the publisher socket to 100
                           messages, to avoid gobbling up memory if one or more of the
                           subscriber processes fails and is not restarted.
                    """
                    self.pub_socket = self.ctx.socket(zmq.PUB)
                    self.pub_socket.hwm = Discharger.PUBLISHER_HWM
                    self.pub_socket.setsockopt(zmq.LINGER, 0)
                    url = MSG_BROKER_XSUB_URL
                    self.logger.debug('Connecting to %s' % url)
                    self.pub_socket.connect(url)
                    time.sleep(PUB_CONNECT_WAIT_TIME)
                except Exception as error:
                    self.logger.exception(error)
                    connect_err_cnt += 1
                    if connect_err_cnt <= Discharger.MAX_ZMQ_ERRORS:
                        error_string = 'Unable to create ZMQ PUB socket for BMU data - %s' \
                                        % repr(error)
                        self.logger.error(error_string)
                        self.pub_socket.close()
                        self.ctx.term()
                        raise ConnectError(error_string)
                    time.sleep(Discharger.SETTLE_WAIT_TIME)
                else:
                    break
                
        cmd_str = "|".join([cmd[0], " ".join(cmd[1:])])
        self.logger.debug('cmd string: %s' % cmd_str)
        try:
            self.pub_socket.send_multipart([self.topic, json.dumps(cmd_str)])
        except ZMQError as error:
            self.logger.exception(error)
            raise Exception(error)


if __name__ == '__main__':
    discharger = Discharger(feed_name='Aggregate', pcs_unit_id=1)
    discharger.start()
    discharger.set_active_power(1.5)
    discharger.set_active_power(-1.5)
    discharger.stop()
