'''
@module: pcs_unit.py

@description:

@copyright: Apparent Energy, Inc.

@created: 

@author:
'''
from lib.model import Model, RecordNotFound
from ess.apparent.bmu_data import BmuData
from ess.artesyn_charger.artesyn_charger_module import ArtesynChargerModule
from ess.ess_unit import EssUnit
from ess.apparent.discharger import Discharger
from alarm.alarm import Alarm
from alarm.alarm_constants import ALARM_ID_ESS_LIMITS_CELL_TEMPERATURE_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_MOSFET_TEMPERATURE_OVER_LIMIT, \
                                  ALARM_ID_ESS_LIMITS_ENVIRONMENT_TEMPERATURE_OVER_LIMIT
from lib.gateway_constants.EveBmuConstants import EVE_BMU_UNRESTRICTED_STATE, \
                                                  EVE_BMU_MAINTENANCE_STATE, \
                                                  EVE_BMU_FAULT_STATE, \
                                                  EVE_BMU_DISCHARGED_RTC_STATE, \
                                                  EVE_BMU_CHARGED_RTD_STATE
from lib.logging_setup import setup_logger
from lib.gateway_constants.DBConstants import PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME, \
                                              PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, \
                                              PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, \
                                              PCS_UNITS_TABLE_ID_COLUMN_NAME, \
                                              AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME, \
                                              DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME, \
                                              AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME, \
                                              DB_AESS_BMU_STATUS_TABLE_NAME, \
                                              AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME, \
                                              DB_PCS_UNITS_TABLE_NAME, \
                                              AESS_BATTERY_MODULE_UNIT_SOC_CHARGE_LIMIT_COLUMN_NAME, \
                                              AESS_BATTERY_MODULE_UNIT_SOC_DISCHARGE_LIMIT_COLUMN_NAME

from lib.gateway_constants.ArtesynConstants import ARTESYN_80V_MODULE_MODEL_NUM, \
                                                   ARTESYN_80V_MIN_CHARGING_CURRENT, \
                                                   ARTESYN_48V_MIN_CHARGING_CURRENT, \
                                                   ARTESYN_MODULE_NOMINAL_VOLTAGE


class BmuStatus(Model):
    sql_table = DB_AESS_BMU_STATUS_TABLE_NAME

    def __init__(self, **kwargs):
        self.battery_cell_overvoltage_warning = kwargs.get('battery_cell_overvoltage_warning', None)
        self.battery_cell_low_voltage_warning = kwargs.get('battery_cell_low_voltage_warning', None)
        self.battery_pack_overvoltage_warning = kwargs.get('battery_pack_overvoltage_warning', None)
        self.battery_pack_low_voltage_warning = kwargs.get('battery_pack_low_voltage_warning', None)
        self.charging_over_current_warning = kwargs.get('charging_over_current_warning', None)
        self.discharging_over_current_warning = kwargs.get('discharging_over_current_warning', None)
        self.charging_high_temperature_warning = kwargs.get('charging_high_temperature_warning', None)
        self.discharging_high_temperature_warning = kwargs.get('discharging_high_temperature_warning', None)
        self.charging_low_temperature_warning = kwargs.get('charging_low_temperature_warning', None)
        self.discharging_low_temperature_warning = kwargs.get('discharging_low_temperature_warning', None)
        self.environment_high_temperature_warning = kwargs.get('environment_high_temperature_warning', None)
        self.environment_low_temperature_warning = kwargs.get('environment_low_temperature_warning', None)
        self.mosfet_high_temperature_warning = kwargs.get('mosfet_high_temperature_warning', None)
        self.soc_low_warning = kwargs.get('soc_low_warning', None)
        self.battery_cell_over_voltage_protection = kwargs.get('battery_cell_over_voltage_protection', None)
        self.battery_cell_low_voltage_protection = kwargs.get('battery_cell_low_voltage_protection', None)
        self.battery_pack_over_voltage_protection = kwargs.get('battery_pack_over_voltage_protection', None)
        self.battery_pack_low_voltage_protection = kwargs.get('battery_pack_low_voltage_protection', None)
        self.charging_over_current_protection = kwargs.get('charging_over_current_protection', None)
        self.discharging_over_current_protection = kwargs.get('discharging_over_current_protection', None)
        self.short_circuit_protection = kwargs.get('short_circuit_protection', None)
        self.charger_overvoltage_protection = kwargs.get('charger_overvoltage_protection', None)
        self.charging_high_temperature_protection = kwargs.get('charging_high_temperature_protection', None)
        self.discharging_high_temperature_protection = kwargs.get('discharging_high_temperature_protection', None)
        self.charging_low_temperature_protection = kwargs.get('charging_low_temperature_protection', None)
        self.discharging_low_temperature_protection = kwargs.get('discharging_low_temperature_protection', None)
        self.mosfet_high_temperature_protection = kwargs.get('mosfet_high_temperature_protection', None)
        self.environment_high_temperature_protection = kwargs.get('environment_high_temperature_protection', None)
        self.environment_low_temperature_protection = kwargs.get('environment_low_temperature_protection', None)
        self.charging_mosfet_fault = kwargs.get('charging_mosfet_fault', None)
        self.discharging_mosfet_fault = kwargs.get('discharging_mosfet_fault', None)
        self.temperature_sensor_fault = kwargs.get('temperature_sensor_fault', None)
        self.battery_cell_fault = kwargs.get('battery_cell_fault', None)
        self.front_end_sampling_communication_fault = kwargs.get('front_end_sampling_communication_fault', None)
        self.state_of_charge = kwargs.get('state_of_charge', None)
        self.state_of_discharge = kwargs.get('state_of_discharge', None)
        self.charging_mosfet_is_on = kwargs.get('charging_mosfet_is_on', None)
        self.discharging_mosfet_is_on = kwargs.get('discharging_mosfet_is_on', None)
        self.charging_limiter_is_on = kwargs.get('charging_limiter_is_on', None)
        self.charger_inversed = kwargs.get('charger_inversed', None)
        self.heater_is_on = kwargs.get('heater_is_on', None)
        self.balance_status = kwargs.get('balance_status', None)
        self.constraint = kwargs.get('constraint', None)


class BmUnit(Model):
    sql_table = DB_AESS_BATTERY_MODULE_UNIT_TABLE_NAME

    def __init__(self, **kwargs):
        self.bmu_id = kwargs.get(AESS_BATTERY_MODULE_UNIT_BMU_ID_COLUMN_NAME, None)
        self.modbus_number = kwargs.get(AESS_BATTERY_MODULE_UNIT_MODBUS_NUMBER_COLUMN_NAME, None)
        self.sds_id = kwargs.get(AESS_BATTERY_MODULE_UNIT_SDS_ID_COLUMN_NAME, None)
        self.soc_charge_limit = kwargs.get(AESS_BATTERY_MODULE_UNIT_SOC_CHARGE_LIMIT_COLUMN_NAME, None)
        self.soc_discharge_limit = kwargs.get(AESS_BATTERY_MODULE_UNIT_SOC_DISCHARGE_LIMIT_COLUMN_NAME, None)
        self.data = None
        self.status = None
        self.reload()
        self.logger = setup_logger(__name__)

    def reload(self):
        try:
            self.data = BmuData.find_or_create_by(bmu_id=self.bmu_id)
        except RecordNotFound:
            pass
        try:
            self.status = BmuStatus.find_or_create_by(bmu_id=self.bmu_id)
        except RecordNotFound:
            pass

    def available_for_charge(self):
        if self.data is None:
            return False

        return self.data.state not in [#EVE_BMU_UNINITIALIZED_STATE,
                                       EVE_BMU_MAINTENANCE_STATE,
                                       EVE_BMU_FAULT_STATE,
                                       EVE_BMU_CHARGED_RTD_STATE]

    def available_for_discharge(self):
        if self.data is None:
            return False
        
        return self.data.state not in [#EVE_BMU_UNINITIALIZED_STATE,
                                       EVE_BMU_MAINTENANCE_STATE,
                                       EVE_BMU_FAULT_STATE,
                                       EVE_BMU_DISCHARGED_RTC_STATE,
                                       None]
    
    def set_soc_charge_limit(self, soc_charge_limit):
        if self.soc_charge_limit is None:
            self.soc_charge_limit = soc_charge_limit

    def set_soc_discharge_limit(self, soc_discharge_limit):
        if self.soc_discharge_limit is None:
            self.soc_discharge_limit = soc_discharge_limit


class PcsUnit(Model):
    sql_table = DB_PCS_UNITS_TABLE_NAME

    def __init__(self, **kwargs):
        self.id = kwargs.get(PCS_UNITS_TABLE_ID_COLUMN_NAME, None)
        self.ess_unit_id = kwargs.get(PCS_UNITS_TABLE_ESS_UNIT_ID_COLUMN_NAME, None)

        # pass in ess_unit object which gets reloaded from the database
        # at the top-level so that we don't need to reload our own
        # private instance.
        self.ess_unit = kwargs.get('ess_unit', None)
        if self.ess_unit is None:
            self.ess_unit = EssUnit.find_by(id=self.ess_unit_id)
        self.bmu_id_A = kwargs.get(PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME, None)
        self.bmu_id_B = kwargs.get(PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME, None)
        self.bm_units = []
        if self.bmu_id_A is not None and int(self.bmu_id_A) != 0:
            bmu = BmUnit.find_by(bmu_id=self.bmu_id_A)
            bmu.set_soc_charge_limit(self.ess_unit.soc_charge_limit)
            bmu.set_soc_discharge_limit(self.ess_unit.soc_discharge_limit)
            self.bm_units.append(bmu)
        if self.bmu_id_B is not None and int(self.bmu_id_B) != 0:
            bmu = BmUnit.find_by(bmu_id=self.bmu_id_B)
            bmu.set_soc_charge_limit(self.ess_unit.soc_charge_limit)
            bmu.set_soc_discharge_limit(self.ess_unit.soc_discharge_limit)
            self.bm_units.append(bmu)
        self.logger = setup_logger(__name__)
        self.charger_module = ArtesynChargerModule.find_by(pcs_unit_id=self.id, extra={'ess_unit': self.ess_unit})
        self.logger.debug('PcsUnit: id = %s' % repr(self.id))
        self.discharger_module = Discharger(pcs_unit_id=self.id, ess_unit=self.ess_unit)

    def reload(self):
        [bm_unit.reload() for bm_unit in self.bm_units]

    def start(self):
        self.set_active_power(0)

    def stop(self):
        self.set_active_power(0)

    def set_active_power(self, active_power):
        """
        :param active_power: kW float value, +discharge/-charge
        :return:
        """
        self.charger_module.set_active_power(active_power)
        self.discharger_module.set_active_power(active_power)

    def capable_of(self, active_power):
        """
        :param active_power: kW float value, +discharge/-charge
        :return:
        """
        if active_power >= 0:
            return self.available_for_discharge()
        else:
            return self.available_for_charge()

    def available_for_charge(self):
        """
        :return: True if all BMUs in the group are available for charge.
        """
        return all([bm_unit.available_for_charge() for bm_unit in self.bm_units])

    def available_for_discharge(self):
        """
        :return: True if all BMUs in the group are available for discharge.
        """
        return all([bm_unit.available_for_discharge() for bm_unit in self.bm_units])

    def soc(self):
        return sum([bmu.data.soc for bmu in self.bm_units]) / len(self.bm_units)

    def min_charging_power(self):
        if self.charger_module.model_num == ARTESYN_80V_MODULE_MODEL_NUM:
            return (ARTESYN_80V_MIN_CHARGING_CURRENT * ARTESYN_MODULE_NOMINAL_VOLTAGE) / 1000.0
        else:
            return (ARTESYN_48V_MIN_CHARGING_CURRENT * ARTESYN_MODULE_NOMINAL_VOLTAGE) / 1000.0

    def check_bmu_limits(self):
        for bmu in self.bm_units:
            if bmu.data.state in [EVE_BMU_MAINTENANCE_STATE, EVE_BMU_FAULT_STATE]:
                continue

            for cell_temperature in bmu.data.cell_temperatures:
                if cell_temperature >= self.ess_unit.battery_max_cell_temperature_limit:
                    Alarm.occurrence(ALARM_ID_ESS_LIMITS_CELL_TEMPERATURE_OVER_LIMIT)
                    bmu.data.save(state=EVE_BMU_FAULT_STATE)
                    self.logger.error("Breached maximum cell temperature limit on BMU %s. Cell temperature = %sC" \
                                      % (bmu.data.bmu_id, cell_temperature))
                    return

            if bmu.data.mosfet_temperature >= self.ess_unit.battery_max_mosfet_temperature_limit:
                Alarm.occurrence(ALARM_ID_ESS_LIMITS_MOSFET_TEMPERATURE_OVER_LIMIT)
                bmu.data.save(state=EVE_BMU_FAULT_STATE)
                self.logger.error("Breached maximum MOSFET temperature limit on BMU %s.  MOSFET temperature = %sC" \
                                  % (bmu.data.bmu_id,
                                     self.ess_unit.max_mosfet_temperature))
                return

            if bmu.data.environment_temperature >= self.ess_unit.battery_max_environment_temperature_limit:
                Alarm.occurrence(ALARM_ID_ESS_LIMITS_ENVIRONMENT_TEMPERATURE_OVER_LIMIT)
                bmu.data.save(state=EVE_BMU_FAULT_STATE)
                self.logger.error("Breached maximum environment temperature limit on BMU %s.  " \
                                  "Environment temperature = %sC" % (bmu.data.bmu_id,
                                                                     self.ess_unit.max_environment_temperature))
                return


            if bmu.data.soc / 100.0 >= self.ess_unit.soc_charge_limit:
                bmu.data.save(state=EVE_BMU_CHARGED_RTD_STATE)
                self.logger.debug('Breached SOC maximum charge limit on BMU soc')
                return
            elif bmu.data.soc / 100.0 <= self.ess_unit.soc_discharge_limit:
                bmu.data.save(state=EVE_BMU_DISCHARGED_RTC_STATE)
                self.logger.debug('Breached SOC minimum charge limit on BMU soc')
                return

            for cell_voltage in bmu.data.cell_voltages:
                if cell_voltage <= self.ess_unit.cell_voltage_limits.min_hard_limit:
                    bmu.data.save(state=EVE_BMU_DISCHARGED_RTC_STATE)
                    self.logger.debug("Breached minimum cell voltage hard-limit on BMU %s.  Battery min cell voltage:" \
                                      "%s <= %s (hard-limit)" % (bmu.data.bmu_id,
                                                                 self.ess_unit.min_cell_voltage,
                                                                 self.ess_unit.cell_voltage_limits.min_hard_limit))
                    return
                elif cell_voltage >= self.ess_unit.cell_voltage_limits.max_hard_limit:
                    bmu.data.save(state=EVE_BMU_CHARGED_RTD_STATE)
                    self.logger.error("Breached maximum cell voltage hard-limit on BMU %s.  Battery max cell voltage: " \
                                      "%s >= %s (hard-limit)" % (bmu.data.bmu_id,
                                                                 self.ess_unit.max_cell_voltage,
                                                                 self.ess_unit.cell_voltage_limits.max_hard_limit))
                    return

            bmu.data.save(state=EVE_BMU_UNRESTRICTED_STATE)


    @classmethod
    def units_available_for(cls, pcs_units, active_power):
        """
        Determine PCS units available to fulfill active power request.
        :param pcs_units: list of all PCS units
        :param active_power: kW float (+discharge/-charge)
        :return: tuple containing list of available PCS units and
            list of unavailable PCS units
        """
        available_units, unavailable_units = [], []
        for pcs_unit in pcs_units:
            if pcs_unit.capable_of(active_power):
                available_units.append(pcs_unit)
            else:
                unavailable_units.append(pcs_unit)

        return available_units, unavailable_units

