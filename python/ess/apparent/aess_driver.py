from ess.apparent.pcs_manager import PcsManager
from ess.ess_driver import EssDriver


class AessDriver(EssDriver):
    def __init__(self, **kwargs):
        super(AessDriver, self).__init__(**kwargs)
        self.pcs_manager = PcsManager(ess_unit=self.ess_unit)
        self.work_state = None
        self.active_power = 0
        self.reactive_power = 0
        self.set_work_state_stop()

    def set_work_state_stop(self):
        self.work_state = "Stopped"
        self.pcs_manager.stop()
        self.save(unit_work_state="Stopped", active_power_cmd=0)

    def set_work_state_run(self):
        self.work_state = "Running"
        self.pcs_manager.start()
        self.save(unit_work_state="Running", active_power_cmd=0)

    def running(self):
        return self.work_state == "Running"

    def stopped(self):
        return self.work_state == "Stopped"

    def set_active_power(self, active_power=None):
        """
        :param active_power:   (+discharge/-charge) float
        :return:
        """
        active_power = float(active_power)
        self.logger.debug('set_active_power: %s' % active_power)
        self.pcs_manager.reload()
        if self.running():
            if self.ess_unit.is_capable_of(active_power):
                self.pcs_manager.set_active_power(active_power)
                self.save(active_power_cmd=active_power)
            else:
                self.set_work_state_stop()
                self.logger.debug("Requested active_power=%s cannot be fulfilled: capability='%s'" %
                                  (active_power, self.ess_unit.capability))
        elif self.stopped():
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_active_power command" %
                              self.work_state)

            # call PCS manager stop from within set_active_power so
            # that changes to auto_shutdown_behavior take affect.
            self.pcs_manager.stop()
            return

    def set_reactive_power(self, power=None):
        if self.running():
            pass
            self.save(reactive_power_cmd=power)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_reactive_power command" %
                              self.work_state)
            return

    def set_voltage_control(self, voltage=None):
        if self.work_state == "Running":
            pass
            self.save(voltage_control_cmd=voltage)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_voltage_control command" %
                              self.work_state)
            return

    def set_frequency_control(self, frequency=None):
        if self.work_state == "Running":
            pass
            self.save(frequency_control_cmd=frequency)
        else:
            self.logger.debug("Battery not running (unit_work_state=%s); ignoring set_frequency_control command" %
                              self.work_state)
            return

    def set_battery_mode(self, mode=None):
        pass
