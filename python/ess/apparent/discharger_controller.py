#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: discharger_controller.py

@description: Create a Discharger Controller daemon that accepts active power commands.

@created: October 8, 2018

@copyright: Apparent Energy Inc.  2018

@author: patrice
"""
import zmq
from zmq.backend.cython.utils import ZMQError
from gateway_utils import *
from lib.gateway_constants.ZmqConstants import *
from lib.helpers import minmax
from inverter.inverter import Inverter
from ess.ess_daemon import EssDaemon
from alarm.alarm_constants import ALARM_ID_AESS_DISCHARGER_CONTROLLER_RESTARTED
import threading
import json
from lib.exceptions import DatabaseError, SignalExit, ProcessingError
import struct


class DischargerControllerThread(threading.Thread):
    SUBSCRIBER_SOCKET_TIMEOUT = 1500 #ms

    def __init__(self, pcs_unit_id=None, zctx=None, **kwargs):
        self.ess_unit = kwargs.pop('ess_unit')
        threading.Thread.__init__(self, **kwargs)

        self.shutdown_flag = threading.Event()
        self.logger = setup_logger('DschrgCntrlrThr.pcs%s' % pcs_unit_id)
        #self.logger.setLevel(logging.DEBUG)
        self.pcs_unit_id = pcs_unit_id
        self.zctx = zctx
        self.command_dict = {'active_power': self.set_active_power}
        self.subscriber = None
        self.poller = None
        self.feed_name = 'aggregate'    # TODO: set feed_name based on inverter feed assignments?
        self.seq_num = SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE
        self.last_curtail_value = None

    def resending_zero(self, curtail_value):
        result = (self.last_curtail_value == 0 and curtail_value == 0)
        self.last_curtail_value = curtail_value
        return result

    def set_active_power(self, active_power=None):
        if active_power is None:
            raise Exception('Missing active_power')

        active_power = float(active_power)
        self.logger.debug('pcs_unit_id = %s, feed_name = %s, active_power = %s' %
                          (repr(self.pcs_unit_id), self.feed_name, active_power))

        """
        The max rate of discharge is calculated based on the number of battery inverters
        in the PCS UNIT. This is then used to calculate the power limit percentage message
        to send to the appropriate battery inverters.

        :param pcs_unit_id: identifies which PCS unit id to target with the active power.
        :param feed_name: string specifying feed name to apply active power to.
        :param active_power: float number representing active power in kW to discharge
                    from the battery for this feed. This number will be bound to 0 and max
                    rate of discharge:
                    + = discharge, 0 = stop discharging, - = stop discharging
        :return:
        """
        max_rate_of_discharge = Inverter.count(battery=1, pcs_unit_id=self.pcs_unit_id) * Inverter.MAX_KW_POWER
        active_power = minmax(0.0, active_power, float(max_rate_of_discharge))
        if max_rate_of_discharge == 0:
            curtail_value = 0
        else:
            curtail_value = int((active_power / float(max_rate_of_discharge)) * 100.0)

        # Resending a curtail_value=0 to inverters over a long period of time can
        # completely drain a battery. This is due to how curtail_value=0 is handled
        # in the inverter. To prevent this situation, only send curtail_value=0 once
        # then stop the inverter heartbeat.
        if self.resending_zero(curtail_value):
            self.logger.debug("Do NOT resend curtail_value=0.")
            return False

        # Build an "encapsulated" TLV packet by PCS_UNIT_ID. The packet for the intended inverters by
        # PCS UNIT ID contains a sub-group of TLVs. If the PCS UNIT ID in the packet matches that which
        # is configured on the inverter, the sub-group of TLVs are intercepted and acted upon; otherwise,
        # the sub-group is simply skipped over and onto the next TLV, if any.
        multicast_ip = Inverter.get_multicast_ip(INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY, self.feed_name)
        self.logger.debug('active_power=%s, max_rate_of_discharge=%s, curtail_value=%s, multicast=%s' %
                          (active_power, max_rate_of_discharge, curtail_value, multicast_ip))

        # Build the packed data for the sub-group TLV train; construction of the subgroup begins with an "add" function,
        # the others use "append". For ramp rate: always try getting there as fast as possible either up or down
        sub_group_packed_data = add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_REGULATION_U16_TLV_TYPE, curtail_value)
        sub_group_packed_data = append_user_tlv_32bit_float(sub_group_packed_data,
                                                            GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE,
                                                            100.0)
        sub_group_packed_data = append_user_tlv_32bit_float(sub_group_packed_data,
                                                            GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_RAM_ONLY_F32_TLV_TYPE,
                                                            100.0)
        # Add the SEQUENCE_NUMBER and increment for the next call.
        sub_group_packed_data = append_user_tlv_32bit_unsigned_long(sub_group_packed_data,
                                                                    GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE,
                                                                    int(self.seq_num))
        sub_group_length = len(sub_group_packed_data) + 2  # extra 2 bytes to encompass the PCS UNIT ID itself
        number_of_user_tlv = 4
        self.seq_num += 1

        # Build the TLV to contain the PCS UNIT ID that specifies the intended PCS UNIT.
        group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_length, int(self.pcs_unit_id))
        number_of_user_tlv += 1

        # Finally: create the complete TLV-encoded packed data payload:
        master_tlv = add_master_tlv(number_of_user_tlv)
        complete_packed_data = master_tlv + (group_id_packed_data + sub_group_packed_data)

        # Finally finally ... send.
        self.send(multicast_ip, complete_packed_data)

    def send(self, multicast_ip, tlv):
        self.logger.debug('Sending datagram to %s' % multicast_ip)
        try:
            _socket = socket.socket(type=socket.SOCK_DGRAM)
            _socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 3)
            _socket.sendto(tlv, (multicast_ip, SG424_PRIVATE_UDP_PORT))
            _socket.close()
        except Exception as ex:
            self.logger.exception("Multicast Error: ip: %s\n command: %s" %
                                  (repr(multicast_ip), repr(tlv)))

    def launch_command(self, command, parameter):
        str_err = None
        try:
            if command is not None:
                if parameter == 'None':
                    self.command_dict[command]()
                else:
                    args = parameter.split(' ')
                    self.command_dict[command](*args)
        except Exception as error:
            str_err = 'Unable to process command - %s' % error
            self.logger.exception(str_err)

        return str_err

    def run(self):
        self.logger.info('DschrgCntrlrThr [pcs%d] starting.' % self.pcs_unit_id)

        """
        Main loop - wait for commands on the reply socket.
        """
        command, parameter = "active_power", "0.0"
        try:
            self.subscriber = self.zctx.socket(zmq.SUB)
            self.subscriber.connect(MSG_BROKER_XPUB_URL)
            sub_topic = 'pcs_unit%d#' % self.pcs_unit_id
            self.subscriber.setsockopt(zmq.SUBSCRIBE, sub_topic)
            self.logger.debug('Subscribing to %s...' % sub_topic )        
        except ZMQError as error:
            self.logger.exception(error)
            threading.thread.exit()
            
        self.poller = zmq.Poller()
        self.poller.register(self.subscriber, zmq.POLLIN)

        while True:
            if self.shutdown_flag.is_set():
                self.logger.info('Received shutdown signal')
                break    

            zsocks = dict(self.poller.poll(DischargerControllerThread.SUBSCRIBER_SOCKET_TIMEOUT))
            if self.subscriber in zsocks and zsocks[self.subscriber] == zmq.POLLIN:
                try:
                    [topic, data] = self.subscriber.recv_multipart()
                except zmq.ZMQError as e:
                    self.logger.exception('recv_multipart failed for %s' % topic)
                    if e.errno == zmq.ETERM:
                        break           # Interrupted
                self.logger.debug('Received a message for %s - [%s]...' % (topic, repr(data)))
                try:
                    cmd_data = json.loads(data)
                    command, parameter = cmd_data.split("|")
                except Exception as ex:
                    self.logger.exception("Unable to decode JSON data")
            else:
                self.logger.debug("Socket timeout, repeating previous command: %s, %s" % (command, parameter))

            try:
                self.launch_command(command, parameter)
            except Exception as ex:
                self.logger.exception("Unable to launch command: %s, %s" % (command, parameter))

        self.subscriber.close()
        

class DischargerControllerFactoryDaemon(EssDaemon):
    SOCKET_TIMEOUT = 1000 #ms
    alarm_id = ALARM_ID_AESS_DISCHARGER_CONTROLLER_RESTARTED

    def __init__(self, **kwargs):
        super(DischargerControllerFactoryDaemon, self).__init__(**kwargs)
        self.seq_num = SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE
        self.thread_list = []
        self.pcs_list = []
        self.zctx = None

    def get_operational_parameters(self):
        self.logger.debug('Fetching operational parameters...')
        sql = "SELECT %s FROM %s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                      DB_PCS_UNITS_TABLE_NAME)
        try:
            results = prepared_act_on_database(FETCH_ALL, sql, [])
        except Exception:
            self.logger.exception('Unable to fetch Discharger Controller operational parameters.')
            raise DatabaseError('Failed to fetch Disharger Controller operational parameters.')
        
        for result in results:
            self.pcs_list.append(result[PCS_UNITS_TABLE_ID_COLUMN_NAME])

    def whack_threads(self):
        for thread in self.thread_list:
            self.logger.debug('Setting shutdown flag for thread %s' % thread.getName())
            thread._Thread_stop()
        for thread in self.thread_list:
            self.logger.debug('Joining thread %s' % thread.getName())
            thread.join()

    def run(self):
        self.update()

        try:
            self.get_operational_parameters()
        except:
            raise

        try:
            self.zctx = zmq.Context(1)
        except ZMQError as error:
            self.logger.exception(error)
            raise

        for pcs_id in self.pcs_list:
            thread_name = 'pcs%d' % int(pcs_id)
            thread = DischargerControllerThread(pcs_id, self.zctx, name=thread_name, ess_unit=self.ess_unit)
            try:
                self.logger.debug('Starting thread pcs%d' % pcs_id)
                thread.start()
            except Exception as error:
                self.logger.exception(error)
                raise ProcessingError(error)
            self.thread_list.append(thread)
        
        while True:
            try:
                for thread in self.thread_list:
                    #self.logger.debug('Top of thread-join loop...')
                    try:
                        self.update()
                    except:
                        raise
                    
                    thread_name = ''
                    try:
                        thread_name = thread.getName() 
                        thread.join(10.0)
                    except Exception as error:
                        raise ProcessingError(error)
                    if not thread.is_alive():
                        self.logger.error('DischargerController thread %s died' % thread_name)
                        """
                        @note: remove the dead thread from the list and create a new one.
                        """
                        self.thread_list.remove(thread)
                        pcs_id = int(thread_name[3:])
                        thread = DischargerControllerThread(pcs_id, name=thread_name, ess_unit=self.ess_unit)
                        try:
                            self.logger.debug('Restarting Discharger controller thread %d' % pcs_id)
                            thread.start()
                        except Exception as error:
                            self.logger.exception(error)
                            raise ProcessingError(error)
                        self.thread_list.append(thread)
                    else:
                        #self.logger.debug('Thread %s is alive...' % thread_name)
                        pass
            except SignalExit as error:
                self.logger.warning('Got a SignalExit', error)
                self.whack_threads()
                self.zctx.term()
                break
            
        self.logger.warning('DischargerControllerFactoryDaemon fell through main loop')
        return False


if __name__ == '__main__':
    discharger_controller = DischargerControllerFactoryDaemon()
    discharger_controller.run()


