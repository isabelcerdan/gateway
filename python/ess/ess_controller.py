#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

"""
@module: ESS Controller

@description: ESS controller class for sending active power commands to ESS Units.

@copyright: Apparent Inc. 2018

@reference:

@author: patrice

@created: April 20, 2018
"""
import zmq
import json

from ess.byd.BydCess40Controller import BydCess40Driver
from ess.byd.BydCess400Controller import BydCess400Driver
from ess.apparent.apparent_ess_controller import ApparentEssDriver
from ess.apparent.aess_driver import AessDriver
from alarm.alarm_constants import ALARM_ID_ESS_CONTROLLER_DAEMON_RESTARTED
from ess.ess_master_info import EssMasterInfo
from ess.ess_daemon import EssDaemon
from ess.ess_model import *
from zmq.backend.cython.utils import ZMQError
from lib.gateway_constants.ZmqConstants import MSG_BROKER_XPUB_URL
from lib.exceptions import ZmqError


MAX_DB_ERRORS = 5
CONTROLLER_UPDATE_INTERVAL = 5

ZERO_CROSSING_DELAY = 0.250  # 250ms


class EssControllerDaemon(EssDaemon):
    alarm_id = ALARM_ID_ESS_CONTROLLER_DAEMON_RESTARTED

    """
    Generic ESS controller that defines interface for driving concrete ESS controllers.
    """

    def __init__(self, **kwargs):
        super(EssControllerDaemon, self).__init__(**kwargs)
        # self.logger.setLevel(logging.DEBUG)
        self.db_errors = 0
        #self.socket_timeout = kwargs.get('socket_timeout', 5000)
        self.socket_timeout = 10000 #ms
        if self.ess_unit.model.id == ESS_MODEL_ID_BYD_CESS_400:
            self.driver = BydCess400Driver(ess_unit=self.ess_unit)
        elif self.ess_unit.model.id == ESS_MODEL_ID_APPARENT_BATTERY:
            self.driver = ApparentEssDriver(ess_unit=self.ess_unit)
        elif self.ess_unit.model.id == ESS_MODEL_ID_BYD_CESS_40:
            self.driver = BydCess40Driver(ess_unit=self.ess_unit)
        elif self.ess_unit.model.id == ESS_MODEL_ID_APPARENT_EVE_ESS:
            self.driver = AessDriver(ess_unit=self.ess_unit)
        else:
            raise Exception("Unsupported ESS model id %s" % self.ess_unit.model.id)

        self.command_dict = \
            {
                'stop': self.driver.set_work_state_stop,
                'run': self.driver.set_work_state_run,
                'fault': self.driver.set_work_state_fault,
                'active_power': self.driver.set_active_power,
                'reactive_power': self.driver.set_reactive_power,
                'voltage_control': self.driver.set_voltage_control,
                'frequency_control': self.driver.set_frequency_control,
                'set_battery_mode': self.driver.set_battery_mode
            }
        
    def launch_command(self, command, parameter):
        str_err = None
        try:
            if command is not None:
                if parameter == 'None':
                    self.command_dict[command]()
                else:
                    self.command_dict[command](parameter)
        except Exception as error:
            str_err = 'Unable to process command - %s' % error
            self.logger.exception(str_err)

        return str_err

    def run(self):
        self.update()

        try:
            zctx = zmq.Context()
            subscriber = zctx.socket(zmq.SUB)
            topic = "essid%d-controller" % int(self.ess_unit.id)
            subscriber.setsockopt(zmq.SUBSCRIBE, topic)
            self.logger.debug('Subscribing to topic = %s...' % topic)
            subscriber.connect(MSG_BROKER_XPUB_URL)
            poller = zmq.Poller()
            poller.register(subscriber, zmq.POLLIN)
        except ZMQError as error:
            self.logger.exception(error)
            raise ZmqError(error)

        """
        Main loop - wait for commands from the GUI
        """
        while True:
            self.update()
            self.ess_unit.reload()

            try:
                zsocks = dict(poller.poll(self.socket_timeout))
            except Exception as error:
                str_err = repr(error)
                self.logger.error(str_err)
                continue

            if subscriber in zsocks and zsocks[subscriber] == zmq.POLLIN:
                try:
                    [topic, data] = subscriber.recv_multipart()
                except ValueError as error:
                    self.logger.warning(error)
                    continue
                if topic is None or data is None:
                    self.logger.info('Received a message with no topic or data frame.')
                    continue
                request = json.loads(data)
                command, parameter = request.split("|")
                self.logger.debug('Received %s|%s...' % (command, parameter))
                self.launch_command(command, parameter)
            else:

                self.logger.debug('Socket timeout... re-launch active_power command to ensure BMU refresh.')
                master_info = EssMasterInfo.find_by(ess_id=self.ess_unit.id)
                self.launch_command('active_power', master_info.active_power_cmd)
                #self.launch_command('reactive_power', master_info.reactive_power_cmd)
                #self.launch_command('voltage_control', master_info.voltage_control_cmd)
                #self.launch_command('frequency_control', master_info.frequency_control_cmd)

        subscriber.close()
        zctx.term()


if __name__ == '__main__':
    e = EssControllerDaemon(id=1)
    e.run()
