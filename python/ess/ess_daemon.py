from daemon.daemon import Daemon
from ess.ess_unit import EssUnit


class EssDaemon(Daemon):

    def __init__(self, **kwargs):
        super(EssDaemon, self).__init__(**kwargs)
        self.ess_unit = EssUnit.find_by(id=self.id)
