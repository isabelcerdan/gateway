from datetime import datetime

from lib.helpers import get_float_seconds_since
from lib.db import *
from lib.logging_setup import setup_logger


class EnergyBucketTask(object):
    def __init__(self):
        self.bucket_initialized = False
        self.bucket_real_a = 0
        self.bucket_real_b = 0
        self.bucket_real_c = 0
        self.current_bucket_sample = 0
        self.current_sample_time = datetime.now()
        self.last_bucket = datetime.now()
        self.logger = setup_logger(__name__)

    def handle_bucket(self):
        if self.bucket_has_been_initialized():
            if get_float_seconds_since(self.last_bucket) >= BUCKET_FREQUENCY:
                self.last_bucket = datetime.now()
                self.update_bucket_readings()

    def get_bucket_sample(self):
        string_to_get_sample = "SELECT * FROM %s;" % DB_BUCKET_CONFIG_TABLE
        bucket_config = prepared_act_on_database(FETCH_ALL, string_to_get_sample, ())
        if not bucket_config:
            self.logger.error("Unable to access bucket config table.")
        else:
            self.current_bucket_sample = int(bucket_config[0]['Current_Sample'])

    def save_bucket_sample(self):
        string_to_save_sample = "UPDATE %s SET %s = %s;" % (DB_BUCKET_CONFIG_TABLE,
                                                            'Current_Sample', "%s")
        args_to_save_sample = (self.current_bucket_sample,)
        prepared_act_on_database(EXECUTE, string_to_save_sample, args_to_save_sample)

    def bucket_has_been_initialized(self):
        sql = "SELECT * FROM %s;" % DB_BUCKET_CONFIG_TABLE
        bucket_initialized = prepared_act_on_database(FETCH_ALL, sql, [])

        if not bucket_initialized:
            self.bucket_initialized = False
            return False

        if str(bucket_initialized[0]['reinitialize_bucket']) == '1':
            self.bucket_initialized = False  # Not yet initialized but needs to be
        elif str(bucket_initialized[0]['reinitialize_bucket']) == '0':
            self.bucket_initialized = True  # Not requested therefore already initialized
        else:
            self.logger.error("Bucket, initialization can't be determined. Ignoring bucket for 15 minutes")
        if self.bucket_initialized:
            return True
        else:
            self.reinit_energy_bucket()
        return False

    def get_bucket_datetime(self):
        string_to_read_bucket_datetime = "SELECT * FROM %s WHERE %s = %s;" % (DB_BUCKET_TABLE,
                                                                              'Sample_ID', "%s")
        args_to_read_bucket_datetime = (self.current_bucket_sample,)
        times = prepared_act_on_database(FETCH_ALL,
                                         string_to_read_bucket_datetime, args_to_read_bucket_datetime)
        if not times:
            return None

        return times[0]['Sample_Time']

    def is_bucket_today(self):
        try:
            return self.get_bucket_datetime().day == self.current_sample_time.day
        except Exception as ex:
            self.logger.exception("Couldn't read bucket datetime.")
            return True

    def set_next_bucket_sample(self):
        if not self.current_bucket_sample == 60:
            self.current_bucket_sample += 1
            self.save_bucket_sample()
        else:
            self.current_bucket_sample = 0

    def update_current_bucket_sample(self):
        string_to_update_bucket_data = "UPDATE %s SET %s = %s, %s = %s, %s = %s," \
                                       " %s = %s WHERE %s = %s;" % (DB_BUCKET_TABLE,
                                                                    'Accumulated_Real_Energy_Phase_A', "%s",
                                                                    'Accumulated_Real_Energy_Phase_B', "%s",
                                                                    'Accumulated_Real_Energy_Phase_C', "%s",
                                                                    'Sample_Time', "%s",
                                                                    'Sample_ID', "%s")
        args_to_update_bucket_data = (self.bucket_real_a, self.bucket_real_b, self.bucket_real_c,
                                      self.current_sample_time, self.current_bucket_sample)
        prepared_act_on_database(EXECUTE, string_to_update_bucket_data, args_to_update_bucket_data)

    def update_bucket_readings(self):
        self.get_current_bucket_sample()
        if not self.is_bucket_today():
            self.set_next_bucket_sample()
        self.update_current_bucket_sample()

    def reinit_energy_bucket(self):
        self.set_bucket_reinit_status('0')
        self.empty_bucket_table()
        self.current_bucket_sample = 0
        self.save_bucket_sample()
        self.get_current_bucket_sample()
        if not self.populate_initial_bucket_data():
            self.logger.warning("Could not populate bucket")
            self.set_bucket_reinit_status('1')

    def set_bucket_reinit_status(self, reinit_requested):
        string_to_empty_bucket_table = "UPDATE %s SET %s = %s;" % (DB_BUCKET_CONFIG_TABLE,
                                                                   "reinitialize_bucket", "%s")
        args_to_empty_bucket_table = (reinit_requested,)
        prepared_act_on_database(EXECUTE, string_to_empty_bucket_table, args_to_empty_bucket_table)

    def empty_bucket_table(self):
        string_to_empty_bucket_table = "DELETE FROM %s;" % DB_BUCKET_TABLE
        prepared_act_on_database(EXECUTE, string_to_empty_bucket_table, ())

    def get_current_bucket_sample(self):
        self.current_sample_time = datetime.today()

        pcc1_sql = "SELECT %s,%s,%s FROM %s WHERE %s=%s AND %s='%s'" % \
                    (METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP,
                     METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP,
                     METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP,
                     DB_METER_READINGS_TABLE,
                     METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                     METER_DATA_TYPE_LAST_VALUE,
                     METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                     METER_ROLE_PCC1)

        pcc1_data = prepared_act_on_database(FETCH_ALL, pcc1_sql, ())

        if not pcc1_data:
            return False

        pcc2_sql = "SELECT %s,%s,%s FROM %s WHERE %s=%s AND %s='%s'" % \
                    (METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP,
                    METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP,
                    METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP,
                    DB_METER_READINGS_TABLE,
                    METER_READINGS_TABLE_DATA_TYPE_COLUMN_NAME,
                    METER_DATA_TYPE_LAST_VALUE,
                    METER_READINGS_TABLE_METER_ROLE_COLUMN_NAME,
                    METER_ROLE_PCC2)

        pcc2_data = prepared_act_on_database(FETCH_ALL, pcc2_sql, ())

        if not pcc2_data:
            return False

        pcc1_A = 0.0
        pcc1_B = 0.0
        pcc1_C = 0.0

        pcc2_A = 0.0
        pcc2_B = 0.0
        pcc2_C = 0.0

        if (pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP] != None):
            pcc1_A = float(pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP])

        if (pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP] != None):
            pcc1_B = float(pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP])

        if (pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP] != None):
            pcc1_C = float(pcc1_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP])


        if (pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP] != None):
            pcc2_A = float(pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_A_EXPORT_FP])

        if (pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP] != None):
            pcc2_B = float(pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_B_EXPORT_FP])

        if (pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP] != None):
            pcc2_C = float(pcc2_data[0][METER_READINGS_TABLE_ACCUM_REAL_ENERGY_C_EXPORT_FP])


        self.bucket_real_a = pcc1_A + pcc2_A
        self.bucket_real_b = pcc1_B + pcc2_B
        self.bucket_real_c = pcc1_C + pcc2_C

    # def populate_initial_bucket_data_feed(self, feed=None):
    #     if feed == 'A':
    #         feed_string = 'Accumulated_Real_Energy_Phase_A'
    #         feed_val = self.bucket_real_a
    #     elif feed == 'B':
    #         feed_string = 'Accumulated_Real_Energy_Phase_B'
    #         feed_val = self.bucket_real_b
    #     elif feed == 'C':
    #         feed_string = 'Accumulated_Real_Energy_Phase_C'
    #         feed_val = self.bucket_real_c
    #     else:
    #         return False
    #     string_to_populate_bucket_data = "UPDATE %s SET %s = %s, %s = %s WHERE %s = %s;" % (DB_BUCKET_TABLE,
    #                                                                                         feed_string, "%s",
    #                                                                                         'Sample_Time', "%s",
    #                                                                                         'Sample_ID', "%s")
    #     this_sample = 0
    #     while this_sample <= 60:  # 61 samples 0->60
    #         args_to_populate_bucket_data = (feed_val,
    #                                         self.current_sample_time, this_sample)
    #         result = prepared_act_on_database(EXECUTE, string_to_populate_bucket_data,
    #                                       args_to_populate_bucket_data)
    #         if result is False:  # if not result: would not return the same value!
    #             return False
    #
    #         this_sample += 1
    #     return True

    def populate_initial_bucket_data(self):
        string_to_populate_bucket_data = "INSERT INTO %s (%s, %s, %s, %s, %s) " \
                                         "VALUES (%s, %s, %s, %s, %s);" % (DB_BUCKET_TABLE,
                                                                           'Accumulated_Real_Energy_Phase_A',
                                                                           'Accumulated_Real_Energy_Phase_B',
                                                                           'Accumulated_Real_Energy_Phase_C',
                                                                           'Sample_Time',
                                                                           'Sample_ID',
                                                                           "%s", "%s", "%s", "%s", "%s")
        this_sample = 0
        while this_sample <= 60:  # 61 samples 0->60
            try:
                args_to_populate_bucket_data = (self.bucket_real_a, self.bucket_real_b, self.bucket_real_c,
                                                self.current_sample_time, this_sample)
                result = prepared_act_on_database(EXECUTE, string_to_populate_bucket_data,
                                                  args_to_populate_bucket_data)
                if result is False:  # if not result: would not return the same value!
                    self.logger.warning("populate bucket sample: %s failed" % this_sample)
                    return False

                this_sample += 1
            except Exception:
                self.logger.exception("Initialize bucket data failed")
        return True



