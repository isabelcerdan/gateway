# encoding: utf-8
import alarm

from lib.db import *

class EnergyBucket(object):
    def __init__(self, **kwargs):
        self.feed_name = kwargs.get('feed_name', 'Aggregate')
        self.enabled = get_default(kwargs, 'bucket_enabled', '0') == '1'
        self.size_kwh = get_default(kwargs, 'bucket_size', 0)
        self.period_in_days = get_default(kwargs, 'bucket_period', 32)
        self.level_column = 'Accumulated_Real_Energy_Phase_%s' % self.feed_name

        self.fullness = 0.0
        self.coefficient = 0.0

        self.starting_level = None
        self.current_level = None
        self.sample_start = None
        self.current_sample = None

        self.get_current_sample()

    def get_current_sample(self):
        sql = "SELECT * FROM energy_bucket_config;"
        buckets = prepared_act_on_database(FETCH_ALL, sql, ())
        for bucket in buckets:
            self.current_sample = int(bucket['Current_Sample'])

    def get_start_of_rolling_bucket_sample(self):
        starting_sample = self.current_sample - self.period_in_days
        if starting_sample < 0:
            starting_sample += 61
        self.sample_start = starting_sample

    def read_sample(self, sample_id):
        sql = "SELECT * FROM energy_bucket WHERE %s = %s;" % ('Sample_ID', "%s")
        bucket_yield = prepared_act_on_database(FETCH_ALL, sql, (sample_id,))
        for bucket in bucket_yield:
            return bucket[self.level_column]

    def check_fullness(self):
        if self.enabled:
            self.get_current_sample()
            self.get_start_of_rolling_bucket_sample()
            self.current_level = self.read_sample(self.current_sample)
            self.starting_level = self.read_sample(self.sample_start)

            self.fullness = (self.current_level - self.starting_level) / self.size_kwh
            if self.fullness < 0.5:
                alarm.Alarm.request_clear(alarm.ALARM_ID_ENERGY_BUCKET_50_CAPACITY)
                self.coefficient = 0
            elif self.fullness < 0.9:
                alarm.Alarm.request_clear(alarm.ALARM_ID_ENERGY_BUCKET_90_CAPACITY)
                alarm.Alarm.occurrence(alarm.ALARM_ID_ENERGY_BUCKET_50_CAPACITY)
                self.coefficient = self.fullness

            if self.coefficient >= 0.9:
                alarm.Alarm.occurrence(alarm.ALARM_ID_ENERGY_BUCKET_90_CAPACITY)
        else:
            self.coefficient = 0.0

        return self.coefficient

    def save(self):
        sql = "UPDATE power_regulators SET bucket_fullness = %s, bucket_coefficient = %s" \
              " WHERE feed_name = %s AND has_solar = 1" % ("%s", "%s", "%s")
        data = (self.fullness, self.coefficient, self.feed_name)
        prepared_act_on_database(EXECUTE, sql, data)
