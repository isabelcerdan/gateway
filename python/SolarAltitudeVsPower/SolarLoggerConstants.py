'''
@created: May 25, 2018

@module: Solar Logger Constants

@copyright: Apparent Inc. 2018

@reference: 
            
@author: steve
'''


SOLAR_LOGGER_MAX_DB_ERRORS = 5
SOLAR_LOGGER_DEFAULT_POLLING_FREQ_SEC = 10
SOLAR_LOGGER_DEFAULT_LOG_CYCLE = 2592000

'''
Solar Logger Exceptions
'''
class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class CommandError(Error):
    """
    Exception raised when unable to complete a command on the CESS.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class ParameterError(Error):
    """
    Exception raised when an invalid parameter is encountered.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class ProcessingError(Error):
    """
    Exception raised when an internal error is encountered.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DatabaseError(Error):
    """
    Exception raised when database error maximum is exceeded.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class NotSupportedError(Error):
    """
    Exception raised when a function is not fully implemented.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)

