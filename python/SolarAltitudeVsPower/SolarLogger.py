#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
@module: SolarLogger

@description: Continuously poll the database for power production and calculate the
              solar altitude for it.  Data is logged to a circular table.

@copyright: Apparent Inc. 2018

@reference: 

@author: steve

@created: May 25, 2018
'''
import os
import time
import signal
from lib.logging_setup import *
from datetime import datetime
import MySQLdb as MyDB
import MySQLdb.cursors as my_cursor
from contextlib import closing
import setproctitle
import numpy as np
from lib.helpers import datetime_to_utc
from lib.gateway_constants.DBConstants import *
from SolarLoggerConstants import *
import solar as solar

logger = setup_logger('SolarLogger')


def instantiate_solarlogger(lock=None):
    setproctitle.setproctitle('apparent-solarloggerd')
    try:
        SolarLogger()
    except Exception as error:
        critical_string = 'Unhandled exception - %s' % repr(error) 
        logger.exception(critical_string)
    logger.info('Waiting for Gateway Monitor to whack us...')
    time.sleep(10.0)
    logger.critical('SolarLogger was not killed - performing exit!')
    exit(1)
    

def sig_handler(signum, frame):
    print "Caught signal:", signum
    exit(1)
    

class SolarLogger(object):
    def __init__(self, **kwargs):
        self.polling_freq_ms = SOLAR_LOGGER_DEFAULT_POLLING_FREQ_SEC
        self.latitude = 0.0
        self.longitude = 0.0
        self.log_cycle = int(SOLAR_LOGGER_DEFAULT_LOG_CYCLE)

        self.run()

    def prepared_act_on_database(self, action, command, args):
        try:
            with closing(MyDB.connect(user=DB_U_NAME, passwd=DB_PASSWORD,
                                      db=DB_NAME, cursorclass=my_cursor.DictCursor)) as connection:
                with connection as cursor:
                    cursor.execute(command, args)
                    if action == FETCH_ONE:
                        return cursor.fetchone()
                    elif action == FETCH_ALL:
                        return cursor.fetchall()
                    return True
        except Exception as error:
            critical_string = "act_on_database failed. \nAction: %s \nString: %s \nArgs: %s - %s" \
                              % (action, command, args, repr(error))
            logger.exception(critical_string)
            raise MyDB.Error

    def set_pid(self):
        pid = str(os.getpid())
        mysql_string = "update %s set %s = %s;" % (DB_SOLAR_LOGGER_CONTROL_TABLE,
                                                   SOLAR_LOGGER_CONTROL_TABLE_PID_COLUMN_NAME, "%s")
        args = [pid,]
        try:
            self.prepared_act_on_database(EXECUTE, mysql_string, args)
        except MyDB.Error as error:
            err_string = "Cannot connect to DB to set BydCess400Monitor PID - %s" % repr(error)
            logger.exception(err_string)
            raise DatabaseError(err_string)
        except Exception as error:
            err_string = "Failed to write PID- %s" % repr(error)
            logger.exception(err_string)
            raise DatabaseError(err_string)
        pid_string = 'SolarLogger PID = %s' % pid
        logger.info(pid_string)
        return True

    def set_updated(self):
        heartbeat_string = "update %s set %s = %s;" % (DB_SOLAR_LOGGER_CONTROL_TABLE, 
                                                       SOLAR_LOGGER_CONTROL_TABLE_UPDATED_COLUMN_NAME,
                                                       "%s")
        args = [int(datetime_to_utc(datetime.utcnow())),]
        try:
            self.prepared_act_on_database(EXECUTE, heartbeat_string, args)
        except MyDB.MySQLError as error:
            err_string = "Cannot connect to database to set Solar Logger running status - %s" % repr(error)
            raise DatabaseError(err_string)
        except Exception as error:
            err_string = "Cannot set Solar Logger running status - %s" % repr(error)
            logger.exception(err_string)                
            self.db_errors += 1
            if self.db_errors > SOLAR_LOGGER_MAX_DB_ERRORS:
                raise DatabaseError('Maximum database error count exceeded.')
        
        return True

    def get_runtime_parameters(self):
        query_string = "SELECT %s, %s, %s, %s FROM %s;" % \
                        (SOLAR_LOGGER_CONTROL_TABLE_POLLING_FREQ_SEC_COLUMN_NAME,
                         SOLAR_LOGGER_CONTROL_TABLE_LATITUDE_COLUMN_NAME,
                         SOLAR_LOGGER_CONTROL_TABLE_LONGITUDE_COLUMN_NAME,
                         SOLAR_LOGGER_CONTROL_TABLE_LOG_CYCLE_COLUMN_NAME,
                         DB_SOLAR_LOGGER_CONTROL_TABLE)
        args = []
        try:
            response = self.prepared_act_on_database(FETCH_ALL, query_string, args)
        except Exception:
            logger.exception('Unable to fetch run-time parameters.')
            raise DatabaseError('Failed to fetch run-time parameters.')

        param_dict = {}
        param_dict = response[0] 
        self.polling_freq_sec = int(param_dict[SOLAR_LOGGER_CONTROL_TABLE_POLLING_FREQ_SEC_COLUMN_NAME])
        self.latitude = float(param_dict[SOLAR_LOGGER_CONTROL_TABLE_LATITUDE_COLUMN_NAME])
        self.longitude = float(param_dict[SOLAR_LOGGER_CONTROL_TABLE_LONGITUDE_COLUMN_NAME])
        self.log_cycle = int(param_dict[SOLAR_LOGGER_CONTROL_TABLE_LOG_CYCLE_COLUMN_NAME])
        logger.info('Polling frequency = %d seconds' % self.polling_freq_sec)

    def get_power_reading(self):
        sql = "SELECT %s FROM %s;" % ("Export_PCC_1_Delta", DB_NXO_STATUS_TABLE)
        args = []
        try:
            result = self.prepared_act_on_database(FETCH_ONE, sql, args)
        except:
            raise
        return result['Export_PCC_1_Delta']
    
    def get_solar_altitude(self):
        return solar.GetAltitude(self.latitude, self.longitude, datetime.utcnow())
    
    def update_solar_logger_data(self, power=None, solar_altitude=None):
        sql = 'REPLACE INTO %s SET %s = (SELECT MOD(COALESCE(MAX(%s), 0), %d)' \
              ' + 1 FROM %s AS t), %s = %s, %s = %s;' % \
                (DB_SOLAR_LOGGER_DATA_TABLE, 
                 SOLAR_LOGGER_DATA_ROW_ID_COLUMN_NAME,
                 SOLAR_LOGGER_DATA_ENTRY_ID_COLUMN_NAME_COLUMN_NAME,
                 self.log_cycle,
                 DB_SOLAR_LOGGER_DATA_TABLE,
                 SOLAR_LOGGER_DATA_PCC_VALUE_COLUMN_NAME, power,
                 SOLAR_LOGGER_DATA_SOLAR_ALTITUDE_COLUMN_NAME, solar_altitude)
        args = []
        try:
            self.prepared_act_on_database(EXECUTE, sql, args)
        except Exception as error:
            err_string = 'Unable to update %s - %s' % (DB_SOLAR_LOGGER_DATA_TABLE, repr(error))
            logger.exception(err_string)
            raise DatabaseError('Maximum database errors exceeded.')

    def run(self):
        logger.info("Solar Logger starting...")

        try:
            self.get_runtime_parameters()
        except:
            raise
        
        while True:
            try:
                self.set_updated()
            except:
                raise
            
            try:
                power = self.get_power_reading()
            except:
                raise
            
            try:
                solar_altitude = self.get_solar_altitude()
            except:
                raise
            
            try:
                self.update_solar_logger_data(power, solar_altitude)
            except:
                raise
            
            time.sleep(self.polling_freq_sec)
            

if __name__ == '__main__':
    
    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    
    instantiate_solarlogger()

    

