#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
"""
  Copyright Apparent Inc. 2018

  Description: DHCP Listener for handling DHCP events sent from dnsmasq

  The dnsmasq is configured using dhcp-script option to call this script
  whenever a DHCP event occurs.

  From the dnsmasq man page:
  
          -6 --dhcp-script=<path>
             Whenever a new DHCP lease is created, or an old one destroyed, or a TFTP 
             file transfer completes, the executable specified by this option is run. 
             <path> must be an absolute pathname, no PATH search occurs. The arguments 
             to the process are "add", "old" or "del", the MAC address of the host 
             (or DUID for IPv6) , the IP address, and the hostname, if known. "add" 
             means a lease has been created, "del" means it has been destroyed, "old" 
             is a notification of an existing lease when dnsmasq starts or a change to 
             MAC address or hostname of an existing lease (also, lease length or expiry 
             and client-id, if leasefile-ro is set). If the MAC address is from a network 
             type other than ethernet, it will have the network type prepended, 
             eg "06-01:23:45:67:89:ab" for token ring. The process is run as root 
             (assuming that dnsmasq was originally run as root) even if dnsmasq is 
             configured to change UID to an unprivileged user.

  
"""

import sys
from lib.db import *
from lib.logging_setup import setup_logger
from dhcp.dhcp_client import DhcpClient
import time


class DhcpListener(object):
    def __init__(self, argv):
        self.logger = setup_logger('DHCP-EVENT')
        self.logger.info("dnsmasq event: %s" % ",".join(argv[1:]))

        if len(argv) < 4:
            msg = "Expected at least four arguments, only got: %s" % (",".join(argv[1:]))
            self.logger.critical(msg)
            return

        _op = 1
        _mac = 2
        _ip = 3

        self.opcode = str(argv[_op])
        self.dhcp_client = DhcpClient(argv[_mac], argv[_ip])

        if self.dhcp_client.is_supported_device():
            if self.opcode == "add" or self.opcode == "old":
                self.dhcp_client.save()

                #
                # populate inverter_discovery_temp for use in setting inverters to gateway control
                #
                sql = "INSERT INTO inverter_discovery_temp (mac_address, ip_address, last_update) VALUES(%s, %s, %s)" \
                      " ON DUPLICATE KEY UPDATE mac_address=%s, ip_address=%s, last_update=%s" % (
                      "%s", "%s", "%s", "%s", "%s", "%s")
                args = (self.dhcp_client.mac_address, self.dhcp_client.ip_address, int(time.time()),
                        self.dhcp_client.mac_address, self.dhcp_client.ip_address, int(time.time()))
                prepared_act_on_database(EXECUTE, sql, args)

            elif self.opcode == "del":
                self.dhcp_client.destroy()
            else:
                msg = "dnsmasq sent unknown operation: %s,%s,%s" % (
                self.opcode, self.dhcp_client.mac_address, self.dhcp_client.ip_address)
                self.logger.critical(msg)
                raise Exception(msg)


if __name__ == '__main__':
    DhcpListener(sys.argv)
