from collections import deque, namedtuple
from datetime import datetime, timedelta
import os

from lib.logging_setup import *
from lib.helpers import try_float, to_datetime, truncate, merge_dicts
from lib.db import get_default
from lib.model import Model
from inverter.inverter import Inverter
import time

PowerReading = namedtuple('PowerReading', ('power', 'timestamp'))

meter_role_to_field_name = {
    'pcc': 'PCC_1',
    'pcc2': 'PCC_2',
    'solar': 'Solar_1',
    'pcc_combo': 'PCC_Combo'
}


class MeterFeed(Model):
    sql_table = "meter_feeds"
    id_field = "meter_role"

    def __init__(self, **kwargs):
        self.power_regulator_id = kwargs['power_regulator_id']
        self.meter_role = kwargs['meter_role']
        self.feed_name = kwargs['feed_name']
        self.feed_size = kwargs.get('feed_size', 0.0)
        self.energy_bucket = kwargs['energy_bucket']
        self.logger = setup_logger("%s_%s" % (__name__, self.feed_name))
        self.limit = self.get_real_power('threshold', None, **kwargs)
        self.target_offset = self.get_real_power('target_offset', 0.0, **kwargs)
        self.enabled = self.limit is not None
        self.meter_name = meter_role_to_field_name[self.meter_role]
        self.name = "_".join([self.meter_name, self.feed_name])
        self.current_power = self.get_real_power(self.current_power_name(), 0.0, **kwargs)
        self.solar_power = self.get_real_power(self.solar_power_name(), 0.0, **kwargs)
        self.timestamp = to_datetime(get_default(kwargs, self.timestamp_name(), datetime.now()))

        self.target_power = None
        self.current_limit = None
        self.excess_power = None
        self.last_power = None

        power_history = PowerReading(power=None, timestamp=None)
        self.power_history = deque([power_history] * MeterFeed.POWER_HISTORY_SIZE)

        self.proportional_coeff = 0.7
        self.integrative_coeff = 0.2
        self.derivative_coeff = 0.3
        self.isViolating = False
        self.violateStarted = None



    def current_power_name(self):
        return "_".join(['Export', self.name])

    def solar_power_name(self):
        return "_".join(['Export_Solar_1', self.feed_name])

    def timestamp_name(self):
        """
        :return: timestamp field name for the current meter
        (e.g. timestamp_pcc_1, timestamp_pcc_2, timestamp_solar_1)
        """
        return "timestamp_pcc_1" if self.meter_name == 'PCC_Combo' else \
            "_".join(['timestamp', self.meter_name.lower()])

    def get_real_power(self, key, default=None, **data):
        """
        Power Regulator algorithm assumes a "production" perspective
        (+export/-import) so flip real power (which is in consumption
        perspective +import/-export) prior to using it for curtailment
        and ramp rate calculations.

        :param key: name of key in data dictionary
        :param default: default value to return if value at key is None
        :param data: data dictionary
        :return: real power value for the given key
        """
        real_power = get_default(data, key, default)
        if real_power is not None:
            real_power = -try_float(real_power)
        return real_power

    def load_reading(self, **data):
        self.current_power = self.get_real_power(self.current_power_name(), **data)
        self.solar_power = self.get_real_power(self.solar_power_name(), **data)
        self.timestamp = to_datetime(data[self.timestamp_name()])

        # save current power reading to power readings history
        power_reading = PowerReading(power=self.current_power, timestamp=self.timestamp)
        self.power_history.appendleft(power_reading)
        self.power_history.pop()
        self.last_power = self.power_history[1].power

    def earliest_violation(self):
        dt = to_datetime(self.power_history[0].timestamp)
        for reading in self.power_history:
            if reading.power > self.current_limit:
                dt = reading.timestamp
            else:
                break

        return dt

    MAX_VIOLATION_SECONDS = 25

    def excess_violation(self):
        violation_seconds = (datetime.now() - self.earliest_violation()).total_seconds()
        return violation_seconds > MeterFeed.MAX_VIOLATION_SECONDS

    def evaluate(self):
        self.reload_limit()

        # Potentially move limit down based on energy bucket fullness
        self.current_limit = self.limit - (self.feed_size * self.energy_bucket.coefficient)

        # calculate target power relative to limit using curtail target coefficient set in configurator
        self.target_power = self.current_limit + self.target_offset

        # calculate how far away our current power is from our target power
        # positive excess power means we're above target, so need to adjust down
        # negative excess power means we're below target, so need to adjust up
        self.excess_power = self.current_power - self.target_power

        msg = "current=%s limit=%s target=%s" % \
              ("{:.3f}".format(self.current_power),
               "{:.3f}".format(self.current_limit),
               "{:.3f}".format(self.target_power))
        if self.current_power > self.current_limit:
            msg += "     *** VIOLATING ***"
        self.logger.debug(msg)

    MINIMUM_LOAD = 0.04

    def below_min_load(self):
        """
        The gateway is unable to accurately control inverters below a certain real power level.
        Check to see if our current load is below (import=negative, export=positive) the minimum
        load of 4% of inverter feed size.
        :return:
        """
        min_load = self.feed_size * self.MINIMUM_LOAD
        current_load = -self.current_power + self.solar_power
        return current_load < min_load

    POWER_HISTORY_SIZE = 128

    def violating(self):
        if self.current_power > self.current_limit:
            if not self.isViolating:
                self.isViolating = True
                self.violateStarted = time.time()
        else:
            if self.isViolating:
                self.isViolating = False
                now = time.time()
                nowStr = time.strftime("%Y-%m-%d %H:%M:%S.")
                msec = int(round(now * 1000)) % 1000
                startLt = time.localtime(self.violateStarted)
                startStr = time.strftime("%H:%M:%S", startLt)

                try:
                    self.dumpFile = open("/var/log/apparent/meter_feed.log", 'a')
                    self.dumpFile.write(str(os.getpid()) + " " + nowStr +
                                        ("%03u %s" % (msec, self.feed_name)) +
                                        " Violation started " + startStr + " duration = " +
                                        str(now - self.violateStarted))
                    self.dumpFile.write("\r\n")
                    self.dumpFile.flush()
                    self.dumpFile.close()
                except Exception:
                    print("FAILED TO OPEN /var/log/apparent/meter_feed.log FILE")


        return self.isViolating

    def crossing_limit(self):
        crossing = self.last_power < self.current_limit < self.current_power
        return crossing
    
    def worsening_violation(self):
        worsening = self.current_limit < self.last_power < self.current_power
        return worsening

    def pid_adjustment(self):
        
        # derive error (distance from target) history with timestamps
        error_history = []
        for r in self.power_history:
            error_history.append(PowerReading(power=(r.power - self.target_power), timestamp=r.timestamp))

        # Proportional is simply the most recent error
        proportional_error = error_history[0].power

        # Integrative is sum of all errors
        integrative_error = sum([e.power for e in error_history]) / MeterFeed.POWER_HISTORY_SIZE

        # Derivative is difference in error over time
        derivative_error, n_samples = 0, 0
        for current, previous in zip(error_history, error_history[1:]):
            if current.timestamp != previous.timestamp:
                derivative_error += (current.power - previous.power) / (current.timestamp - previous.timestamp)
                n_samples += 1
        derivative_error /= n_samples

        self.logger.debug("p=%s i=%s d=%s" % (proportional_error, integrative_error, derivative_error))

        return  (proportional_error * self.proportional_coeff) + \
               -(integrative_error * self.integrative_coeff) + \
                (derivative_error * self.derivative_coeff)

    def reload_limit(self):
        sql = "SELECT threshold, target_offset FROM meter_feeds " \
              "WHERE power_regulator_id = %s AND meter_role = %s AND feed_name = %s" % ("%s", "%s", "%s")
        args = [self.power_regulator_id, self.meter_role, self.feed_name]
        record = prepared_act_on_database(FETCH_ONE, sql, args)
        if record:
            self.limit = self.get_real_power('threshold', 0, **record)
            self.target_offset = self.get_real_power('target_offset', 0, **record)
        else:
            return False

    def within(self, timestamp, **kwargs):
        if timestamp is None:
            return False
        else:
            return timestamp >= datetime.now() - timedelta(**kwargs)

    def get_min_power(self, seconds):
        return min([p.power for p in self.power_history if self.within(p.timestamp, seconds=seconds)])

    def get_max_power(self, seconds):
        return max([p.power for p in self.power_history if self.within(p.timestamp, seconds=seconds)])

    def get_avg_power(self, seconds):
        power = [p.power for p in self.power_history if self.within(p.timestamp, seconds=seconds)]
        return sum(power) / len(power)