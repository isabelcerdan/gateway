#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

from operator import attrgetter
import os
import time

from lib.logging_setup import *
from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database, get_default
from energy_bucket.energy_bucket import EnergyBucket
from meter_feed import MeterFeed

from battery_regulator import BatteryRegulator
from solar_regulator import SolarRegulator
from lib.model import Model
from inverter.inverter import Inverter

class PowerRegulator(Model):
    sql_table = "power_regulators"
    id_field = "id"

    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.name = kwargs.get('name')
        self.feed_name = kwargs.get('feed_name')
        self.logger = setup_logger("Feed %s" % self.feed_name)
        self.enabled = get_default(kwargs, 'enabled', 0) == 1
        self.has_solar = kwargs.get('has_solar', False)
        self.has_storage = kwargs.get('has_storage', False)
        self.solar_curtailment_window = kwargs.get('solar_curtailment_window', 30.0)
        self.step_size = kwargs.get('step_size', None)
        self.battery_strategy = kwargs.get('battery_strategy', None)
        self.energy_bucket = EnergyBucket(**kwargs)
        self.save(curtail_value=0, ramp_rate=0.0)

        extra = {'energy_bucket': self.energy_bucket}
        if self.has_solar:
            extra['feed_size'] = Inverter.solar_feed_size(self.feed_name)
        self.meter_feeds = MeterFeed.find_all_by(power_regulator_id=self.id, extra=extra)
        self.online = True
        self.protect_alert = False

        if self.has_solar:
            self.solar_regulator = SolarRegulator(feed_name=self.feed_name, power_regulator_id=self.id)
        else:
            self.solar_regulator = None

        if self.has_storage:
            self.battery_regulator = BatteryRegulator(
                feed_name=self.feed_name,
                power_regulator_id=self.id,
                battery_strategy=self.battery_strategy,
                solar_regulator=self.solar_regulator,
                solar_curtailment_window=self.solar_curtailment_window,
                step_size=self.step_size)
        else:
            self.battery_regulator = None

    def load_meter_readings(self):
        if not self.meters_online():
            return False

        nxo_status = PowerRegulator.get_nxo_status()
        if nxo_status['Export_PCC_1_Delta'] is None:
            self.logger.info("NXO data contains None")
            return False
        for meter_feed in self.meter_feeds:
            meter_feed.load_reading(**nxo_status)
        return True

    def meters_online(self):
        sql = "SELECT online FROM energy_meter em " \
          "JOIN meter_feeds mf ON em.meter_role = mf.meter_role " \
          "JOIN daemon_control dc ON dc.name = CONCAT('meterd-', em.meter_role)" \
          "WHERE dc.enabled = 1 AND mf.threshold IS NOT NULL"
        results = prepared_act_on_database(FETCH_ALL, sql, [])
        return all(result['online'] == 1 for result in results)

    # Check the protection alert table.  If the 'tripped' flag is set, return True.
    def protection_alert(self):
        # Check to see if a network protection alert has been set.  If so, return 'we are in violation'.
        sql = "SELECT * FROM %s" % DB_PROTECTION_ALERT_TABLE
        try:
            results = prepared_act_on_database(FETCH_ALL, sql, [])
        except Exception as error:
            self.logger.exception('Failed to fetch PROTECTION ALERT fields from the database.')
            return False
        else:
            if not results:
                if self.protect_alert:
                    self.logger.info('Network protection alert removed.')
                    self.protect_alert = False
            else:
                if not self.protect_alert:
                    self.logger.error('Network protection alert detected!')
                    self.protect_alert = True
            return self.protect_alert

    def regulate(self):
        # load meter readings
        if not self.load_meter_readings():
            if self.solar_regulator:
                self.solar_regulator.do_not_produce()
            return

        # check if energy bucket is full
        if self.energy_bucket.check_fullness() >= 0.9:
            self.logger.info("bucket is 90% full; do not produce")
            if self.solar_regulator:
                self.solar_regulator.do_not_produce()
            return

        # check to see if a network protection trigger has been detected.
        if self.protection_alert():
            self.logger.info("protection alert")
            if self.solar_regulator:
                self.solar_regulator.do_not_produce()
            return

        # find meter feed with largest proportional error relative to target
        if len(self.meter_feeds) > 0:
            [m.evaluate() for m in self.meter_feeds]
            meter_feed = max(self.meter_feeds, key=attrgetter('excess_power'))
            self.logger.debug('Meter feed %s with max excess_power %f' % (meter_feed.feed_name, meter_feed.excess_power))
        else:
            return

        # regulate solar to bring meter feed closer to target
        if self.solar_regulator:
            self.solar_regulator.regulate(meter_feed)
        else:
            self.logger.debug('solar regulator disabled.')

        # regulate battery after solar to bring meter feed closer to target
        if self.battery_regulator:
            self.battery_regulator.regulate(meter_feed)
        else:
            self.logger.debug('battery regulator disabled.')

        self.energy_bucket.save()

    @staticmethod
    def get_nxo_status():
        """
        The nxo_status table contains all relevant data for running the algorithm.
        Query the table for meter feed data: enabled, threshold and most recent readings.

        :return: dict with field names as keys and data as values.
        """
        sql = "SELECT * FROM nxo_status WHERE Profile = 'Default'"
        results = prepared_act_on_database(FETCH_ONE, sql, [])
        return results


if __name__ == '__main__':
    PowerRegulator.find_by(id=1)
