"""
@module: Battery Regulator

@description: Regulate Energy Storage Systems.

@copyright: Apparent Inc. 2019

@reference:

@author: patrice

@created: February 4, 2019
"""
import time
import collections

from ess.ess_unit import EssUnit
from lib.logging_setup import *
from lib.db import get_default


class BatteryRegulator(object):
    SETTLE_TIME = 6.0

    def __init__(self, **kwargs):
        self.logger = setup_logger(__name__)
        self.power_regulator_id = kwargs['power_regulator_id']
        self.ess_units = EssUnit.find_all_by(power_regulator_id=self.power_regulator_id)
        self.feed_name = kwargs.get('feed_name', 'Aggregate')
        self.solar_regulator = kwargs.get('solar_regulator', None)
        self.solar_curtailment_window = kwargs.get('solar_curtailment_window', 30.0)
        default_step_size = self.solar_regulator.feed_size * 0.01 if self.solar_regulator else 0.0
        self.step_size = abs(get_default(kwargs, 'step_size', default_step_size))
        self.battery_strategy = kwargs.get('battery_strategy', None)

        self.start_power = None
        self.requested_active_power = 0
        self.end_time = time.time()
        self.current_limit = None
        self.recovery_pending = False

    def auto_regulate(self, meter_feed, auto_units):
        """
        1. Derive new active power needed to regulate meter feed.
        2. Order ESS units by corresponding priority.
        3. Allocate power to ESS units based on priority. Evenly split power
           to ESS units that have same priority until no remaining power to allocate.

        :param meter_feed:
        :param auto_units:
        :return:
        """
        active_power_delta = self.calculate_active_power_delta(meter_feed)
        total_operational_active_power = sum([e.operational_active_power for e in auto_units])

        remaining_active_power = active_power_delta
        ordered_units = self.order_units(total_operational_active_power + active_power_delta, auto_units)
        for priority, ess_units in ordered_units.items():
            for i, ess_unit in enumerate(ess_units):
                evenly_split_power = remaining_active_power / len(ess_units[i:])
                remaining_active_power -= ess_unit.try_active_power_delta(self.battery_strategy, evenly_split_power)
                if remaining_active_power == 0:
                    return remaining_active_power
        return remaining_active_power

    def reload(self):
        sql = "SELECT solar_curtailment_window, step_size, battery_strategy FROM power_regulators WHERE id = %s" % "%s"
        result = prepared_act_on_database(FETCH_ONE, sql, [self.power_regulator_id])
        if result is None:
            raise Exception("Power regulator record not found for power_regulator_id=%d" % self.power_regulator_id)
        self.solar_curtailment_window = result.get('solar_curtailment_window', 30.0)
        self.step_size = abs(get_default(result, 'step_size', self.step_size))
        self.battery_strategy = result.get('battery_strategy', EssUnit.BATTERY_STRATEGY_VARIABLE_CHARGE)

    def regulate(self, meter_feed):
        """
        unit_work_state: "Stop", "Running", "Starting", "Fault"
        capability: "Unrestricted", "Charge only", "Discharge only"

        active_power: + is discharging, - is charging
        :param
        meter_feed.excess_power:
            + is above target, so need to adjust down
            - is below target, so need to adjust up
        """
        self.reload()

        for ess_unit in self.ess_units:
            ess_unit.reload()

        auto_units = [e for e in self.ess_units if e.is_auto_mode()]
        if len(auto_units) > 0:
            if self.set_new_target(meter_feed):
                self.auto_regulate(meter_feed, auto_units)

    @staticmethod
    def order_units(requested_active_power, auto_units):
        """
        Order units by charge/discharge priority based on requested active power.
        Furthermore, order units within a priority by charge/discharge rate limit
        (lowest to highest) to guarantee full capacity usage.
        :param requested_active_power:
        :param auto_units:
        :return:
        """
        ordered_units = collections.OrderedDict()
        if requested_active_power < 0:
            for e in auto_units:
                if e.is_capable_of(requested_active_power):
                    if e.charge_priority not in ordered_units:
                        ordered_units[e.charge_priority] = [e]
                    else:
                        ordered_units[e.charge_priority].append(e)
            for priority, ess_units in ordered_units.items():
                ess_units.sort(key=lambda e: abs(e.charge_rate_limit))
        else:
            for e in auto_units:
                if e.is_capable_of(requested_active_power):
                    if e.discharge_priority not in ordered_units:
                        ordered_units[e.discharge_priority] = [e]
                    else:
                        ordered_units[e.discharge_priority].append(e)
            for priority, ess_units in ordered_units.items():
                ess_units.sort(key=lambda e: abs(e.discharge_rate_limit))

        return ordered_units

    def recent_solar_curtailment(self):
        """
        :return True if solar production was curtailed within trailing user-defined window
        """
        now = time.time()
        return now < (self.solar_regulator.last_curtailment_time + self.solar_curtailment_window)

    def set_new_target(self, meter_feed):
        """
        Check if we should continue holding active power setting based on a time constant.
        :return:
        """
        now = time.time()
        self.logger.debug("stay-on-target: %s < %s" % (now, self.end_time))

        if now < self.end_time:
            return False
        else:
            self.end_time = now + self.SETTLE_TIME
            self.start_power = meter_feed.current_power
            return True

    def dead_band_lower_bound(self, meter_feed):
        """
        Subtract a multiple of target_offset from target to get the
        dead-band lower bound. Note, power readings always in production
        perspective (+export/-import), so a more negative power reading
        means more load, which from the battery perspective means we
        need to turn down charge (load) or turn up discharge.

        :param meter_feed:
        :return: Return float kW value of dead-band lower bound.
        """
        return meter_feed.target_power + meter_feed.target_offset

    def calculate_active_power_delta(self, meter_feed):
        """
        When solar power is being actively regulated to target, we don't want
        to use the battery to also regulate to target since the two control
        loops would "fight" each other for control and cause instability.
        instead, if solar is actively being curtailed, we should start charging
        the battery until the solar stops curtailing.

        :param meter_feed:
            + excess_power is above target, so need to adjust down
            - excess_power is below target, so need to adjust up
        :return:
        """
        if self.solar_regulator is not None and meter_feed.solar_power >= 0.0:
            if self.solar_regulator.solar_curtailed:
                # If solar curtailment was observed since our last control
                # loop iteration, bump up the charging by one step size.
                delta = -self.step_size
                self.solar_regulator.solar_curtailed = False
            elif meter_feed.get_max_power(self.SETTLE_TIME) < self.dead_band_lower_bound(meter_feed):
                # If the maximum power observed during the battery regulator
                # settle time was less than the dead-band lower bound, turn
                # down charge on the battery by one step size.
                delta = max(self.step_size, int(-meter_feed.excess_power / self.step_size))
            else:
                # hold steady (delta=0) if within dead-band
                delta = 0.0
        else:
            delta = -meter_feed.excess_power

        self.logger.debug("delta=%s" % delta)
        return delta
