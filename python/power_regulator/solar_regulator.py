import copy
import collections
import os

from inverter.inverter import Inverter
from lib.helpers import truncate, minmax
from lib.gateway import Gateway
import tlv_utils
from readings_logger.readings_logger import ReadingsLogger

from alarm.alarm_constants import ALARM_ID_EXCESS_EXPORT_VIOLATION

from gateway_utils import *
from meter.meter import Meter

FASTEST_RAMP_RATE_UP = 20  # %/s
SLOWEST_RAMP_RATE_UP = 0.1  # %/s
FASTEST_RAMP_RATE_DOWN = 100  # %/s
SLOWEST_RAMP_RATE_DOWN = 0.1  # %/s


class SolarCommand(object):
    def __init__(self, **kwargs):
        self.feed_name = kwargs.get('feed_name', '')
        self.curtail_target = kwargs.get('curtail_target', 99.0)
        self.ramp_rate_up = kwargs.get('ramp_rate_up', SLOWEST_RAMP_RATE_UP)
        self.ramp_rate_down = kwargs.get('ramp_rate_down', FASTEST_RAMP_RATE_DOWN)
        self.ramping_up = kwargs.get('ramping_up', True)
        self.start_power = kwargs.get('start_power', None)
        self.end_time = kwargs.get('end_time', None)
        self.violating = kwargs.get('violating', False)
        self.stop_heart_beat = kwargs.get('stop_heart_beat', False)


    def get_feed_name(self):
        return 'Delta' if self.feed_name in ('Aggregate', 'Delta', 'Agg') else self.feed_name

    def active_ramp_rate(self):
        if self.violating:
            return -100.0
        if self.ramping_up:
            return self.ramp_rate_up
        else:
            return -self.ramp_rate_down

    def to_tlv(self, sequence_number):
        packer = tlv_utils.TLVPacker()

        if self.violating:

            # If a load-rejection event was detected, send a CURTAILMENT TLV so that
            # inverters ramp down as fast as possible to avoid export violation.
            # We do not need to send a ramp rate down in this case since the inverter
            # automatically ramps down as fast as possible when it receives a
            # CURTAILMENT TLV.
            #
            # If an inverter is entering curtailment, the inverter will take a new
            # snapshot of the power being produced and curtailment set-points will be
            # a percentage of this reference power level.

            # 2 TLVs = CURTAIL + KEEP_ALIVE SEQUENCE NUMBER

            packer.add(GTW_TO_SG424_CURTAILMENT_U16_TLV_TYPE, round(self.curtail_target))

        else:

            # If this is not a load-rejection event, regulate inverters by sending a new
            # REGULATE TLV with a new set-point along with a ramp rate up if the new
            # curtail set-point is 100% or a ramp rate down if the new curtail set-point
            # is < 100%.
            #
            # If an inverter is entering curtailment, the inverter will take a new
            # snapshot of the power being produced and curtailment set-points will be
            # a percentage of this reference power level.

            # 3 TLVs = REGULATION + (NORMAL_RAMP_RATE_UP or NORMAL_RAMP_RATE_DOWN) + KEEP_ALIVE SEQUENCE NUMBER

            packer.add(GTW_TO_SG424_REGULATION_U16_TLV_TYPE, round(self.curtail_target))

            # curtail target of 100% means we are ramping up so send ramp rate up
            if self.ramping_up:
                packer.add(GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE, self.ramp_rate_up)
            else:  # otherwise we are curtailing so send ramp rate down
                packer.add(GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_RAM_ONLY_F32_TLV_TYPE,
                           self.ramp_rate_down)

        # Add the SEQUENCE NUMBER and increment for the next call.
        packer.add(GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE, int(sequence_number))
        # self.sequence_number += 1 <- incremented upon return

        '''

        UNIVERSAL AIF ... DEPRECATED AND REMOVED FROM THE SYSTEM.

        # If this is first-time setup, we should also send the universal_aif info
        if sequence_number == SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE:
            universal_aif = get_universal_aif_db_row()

            # Proceed only if the region name has been configured.
            if universal_aif:
                if UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME in universal_aif and \
                        universal_aif[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME] != REGION_NAME_UNDEFINED:

                    packer.add(GTW_TO_SG424_REGION_NAME_STRING_TLV_TYPE,
                               universal_aif[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME])
                    packer.add(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE,
                               universal_aif[UNIVERSAL_AIF_TABLE_POWER_FACTOR_COLUMN_NAME])
                    packer.add(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE,
                               universal_aif[UNIVERSAL_AIF_TABLE_POWER_FACTOR_CURRENT_LEADING_COLUMN_NAME])
                    packer.add(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE,
                               universal_aif[UNIVERSAL_AIF_TABLE_POWER_FACTOR_ENABLE_COLUMN_NAME])
        '''

        return packer.pack()


class SolarRegulator(object):
    def __init__(self, **kwargs):
        self.feed_name = kwargs.get('feed_name', None)
        self.power_regulator_id = kwargs.get('power_regulator_id', None)
        self.feed_size = Inverter.solar_feed_size(self.feed_name)
        self.new_solar_command = SolarCommand(feed_name=self.feed_name)
        self.old_solar_command = SolarCommand(feed_name=self.feed_name)
        self.multicast_ip = Inverter.get_multicast_ip(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, self.feed_name)
        self.udp_port = SG424_PRIVATE_UDP_PORT

        gateway = Gateway.find_by()
        self.ramp_rate_knee = gateway.ramp_rate_knee
        self.ramp_rate_slope = gateway.ramp_rate_slope
        self.violation_response_factor = gateway.violation_response_factor
        self.stop_heart_beat = False
        self.solar_meter_present = Meter.count(meter_role='solar') > 0
        self.sequence_number = SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE
        self.settle_time = gateway.settle_time
        self.logger = setup_logger("%s_%s" % (__name__, self.feed_name))
        self.last_curtailment_time = 0
        self.solar_curtailed = False

        self.readings_logger = ReadingsLogger(basename='curtail_log')


    def do_not_produce(self):
        self.new_solar_command.curtail_target = 0
        self.new_solar_command.violating = True
        self.new_solar_command.ramping_up = False
        self.new_solar_command.ramp_rate_down = FASTEST_RAMP_RATE_DOWN
        self.new_solar_command.ramp_rate_up = SLOWEST_RAMP_RATE_UP

    def regulate(self, meter_feed):
        self.new_solar_command.name = meter_feed.name
        isViolating = meter_feed.violating()
        # if we've been consistently violating for longer than
        # 25 seconds, curtail to zero to stop solar production.
        if meter_feed.excess_violation():
            Alarm.occurrence(ALARM_ID_EXCESS_EXPORT_VIOLATION)
            self.do_not_produce()
        else:
            Alarm.request_clear(ALARM_ID_EXCESS_EXPORT_VIOLATION)

            # if we are below a minimum required load, stop solar production
            if self.stay_on_target(meter_feed):
                self.new_solar_command = copy.copy(self.old_solar_command)
            else:
                self.set_new_target(meter_feed, isViolating)

        self.save()
        self.old_solar_command = copy.copy(self.new_solar_command)

        # keep track of when we are in active curtailment
        if self.active_curtailment():
            self.last_curtailment_time = time.time()
            self.solar_curtailed = True

        self.send(self.new_solar_command.to_tlv(self.sequence_number))
        self.sequence_number += 1

    def active_curtailment(self):
        """
        We are actively curtailing/regulating when ramping down and curtail target is < 100%
        :return:
        """
        return (not self.new_solar_command.ramping_up) and (self.new_solar_command.curtail_target < 100)

    SAMPLES_IN_15_DAYS = 4 * 60 * 60 * 24 * 15

    def save(self):
        sql = "UPDATE power_regulators SET curtail_value = %s, ramp_rate = %s WHERE id = %s" % ("%s", "%s", "%s")
        data = [self.new_solar_command.curtail_target, self.new_solar_command.active_ramp_rate(), self.power_regulator_id]
        prepared_act_on_database(EXECUTE, sql, data)

        curtail_value_field_name = "Curtail_Value_%s" % self.new_solar_command.get_feed_name()
        readings_list = [("timestamp", time.time()), (curtail_value_field_name, self.new_solar_command.curtail_target)]
        row = collections.OrderedDict(readings_list)
        self.readings_logger.write_reading(row)

    def stay_on_target(self, meter_feed):
        """
        Allow time for the new power to reach target power after a
        regulate/curtailment command (up or down) has been initiated.

        :param meter_feed: MeterFeed object for new NXO meter feed.
        :return: True if stay on target, False if set new target.
        """

        # if end_time hasn't been set, we need to set an initial target
        if self.new_solar_command.end_time is None:
            return False

        # if we are crossing the limit or we observe a worsening limit violation, immediately set new target
        if meter_feed.crossing_limit() or meter_feed.worsening_violation():
            return False

        # check if we are within our time window
        if time.time() < self.old_solar_command.end_time:

            # if we are ramping up
            if self.old_solar_command.ramping_up:
                # continue to ramp up (i.e. stay on target) if we are above
                # our start power and still below our target power
                if self.old_solar_command.start_power <= meter_feed.current_power < meter_feed.target_power:
                    return True
            else:
                # continue to ramp down towards target power if we are above the target power
                if meter_feed.target_power < meter_feed.current_power:
                    return True

        return False

    def solar_meter_online(self):
        if not self.solar_meter_present:
            return False

        sql = "select online from energy_meter where meter_role like %s" % "%s"
        result = prepared_act_on_database(FETCH_ONE, sql, ['%solar%'])
        return result['online'] == 1

    def set_new_target(self, meter_feed, isViolating):
        """
        Set a curtailment set-point based on new distance from target power
        (i.e. proportional error). Derive new ramp rate from error using linear
        slope intercept equation with tuned ramp rate knee and ramp rate slope
        parameters.
        """

        # for load-rejection events, derive error based on solar power
        if isViolating:
            if self.solar_meter_online():
                # derive error as % of solar power but don't divide by zero or negative solar power
                error = meter_feed.excess_power / max(0.001, meter_feed.solar_power)
            else:
                # if we don't have the solar meter, derive the error based on feed size
                error = meter_feed.excess_power / max(0.001, self.feed_size)

            # use violation response factor to tune how aggressively we respond to a violation
            error *= self.violation_response_factor
            self.new_solar_command.violating = True
        else:
            error = meter_feed.excess_power / max(self.feed_size, 1.0)
            self.new_solar_command.violating = False

        # positive error means we are above our target so need to regulate/curtail down
        if error >= 0:
            # apply error correction to old curtail target
            new_curtail_target = self.old_solar_command.curtail_target * (1.0 - error)

            # if curtail_target in [99.5, 100), do not round to 100 since this
            # would make inverters continue ramping past the target power.
            if int(new_curtail_target) == 99:
                self.new_solar_command.curtail_target = 99.0
            else:
                self.new_solar_command.curtail_target = minmax(0.0, new_curtail_target, 100.0)

            # ramp rate down uses a more aggressive response by a factor of 4 when above the target
            ramp_rate = truncate((abs(error * 100.0) - self.ramp_rate_knee) * 4 / self.ramp_rate_slope)
        else:
            # ramp up at 100% to get to our target faster, throttle ramping using ramp rate
            self.new_solar_command.curtail_target = 100.0
            ramp_rate = truncate((abs(error * 100.0) - self.ramp_rate_knee) / self.ramp_rate_slope)

        # check if curtail target is ramping up or down based on old curtail target
        self.new_solar_command.ramping_up = self.new_solar_command.curtail_target >= self.old_solar_command.curtail_target

        self.new_solar_command.ramp_rate_up = minmax(SLOWEST_RAMP_RATE_UP, ramp_rate, FASTEST_RAMP_RATE_UP)
        self.new_solar_command.ramp_rate_down = minmax(SLOWEST_RAMP_RATE_DOWN, ramp_rate, FASTEST_RAMP_RATE_DOWN)

        self.new_solar_command.start_power = meter_feed.current_power
        self.new_solar_command.end_time = int(time.time()) + self.settle_time

        self.logger.debug('NEW-TARGET: old_curtail=%s new_curtail=%s ramp_rate=%s' %
                          (self.old_solar_command.curtail_target, self.new_solar_command.curtail_target, self.new_solar_command.active_ramp_rate()))

    # def get_future_power(self, meter_feed):
    #     """
    #     Predict future power based on real power delta between new
    #     reading and history of readings.
    #
    #     :return: Projected real power POWER_READINGS_SIZE seconds from now.
    #     """
    #     power_increase = 0.0
    #     for reading in meter_feed.power_readings:
    #         power_delta = meter_feed.current_power - reading.power
    #         time_delta = meter_feed.timestamp - reading.timestamp
    #
    #         if time_delta.total_seconds() > 0:
    #             # calculate measured ramp rate based on recent power readings
    #             measured_ramp_rate = 100.0 * (power_delta / (meter_feed.feed.size * time_delta.total_seconds()))
    #
    #             # only assess future power if we are ramping up faster than the control ramp rate
    #             # Add 15% margin of error and 3% offset to prevent future power from being too new
    #             # and causing instability in the algorithm.
    #             if measured_ramp_rate > (self.old_solar_command.active_ramp_rate() * 1.15) + 3.0 > 0:
    #                 self.logger.debug("measured_ramp_rate=%s active_ramp_rate=%s" % (
    #                     measured_ramp_rate, self.old_solar_command.active_ramp_rate()))
    #                 power_increase = max(power_increase, power_delta * (2.0 / time_delta.total_seconds()))
    #
    #     return meter_feed.current_power + power_increase

    def send(self, tlv):
        try:
            _socket = socket.socket(type=socket.SOCK_DGRAM)
            _socket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 3)
            _socket.sendto(tlv, (self.multicast_ip, self.udp_port))
            _socket.close()
        except Exception as ex:
            self.logger.exception("Multicast Error: ip: %s\n command: %s" %
                                  (repr(self.multicast_ip), repr(tlv)))
