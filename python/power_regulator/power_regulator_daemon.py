#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

from lib.gateway import Gateway
from power_regulator import PowerRegulator
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_POWER_REGULATOR_DAEMON_RESTARTED

class PowerRegulatorDaemon(Daemon):
    source_table = "power_regulators"
    source_id_field = "id"

    polling_freq_sec = 0.500    # 500 ms
    alarm_id = ALARM_ID_POWER_REGULATOR_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(PowerRegulatorDaemon, self).__init__(**kwargs)
        self.power_regulator = PowerRegulator.find_by(id=self.id)
        self.gateway = Gateway.find_by()

    def work(self):
        self.gateway.reload()
        if self.gateway.in_power_control_mode():
            self.power_regulator.regulate()
