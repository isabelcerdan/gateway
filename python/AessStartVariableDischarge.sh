#!/bin/sh
# Place the Apparent in variable discharge mode.
#
# copyright Apparent Inc. 2018
#

# Set the Apparent battery mode to 'auto'.
/usr/share/apparent/python/DbUpdate.py --table ess_units \
                                       --column mode \
                                       --column-value auto \
                                       --where-column-1 id \
                                       --where-value-1 1

# Set the Apparent battery strategy to 'variable_discharge'.
/usr/share/apparent/python/DbUpdate.py --table power_regulators \
                                       --column battery_strategy \
                                       --column-value variable_discharge \
                                       --where-column-1 id \
                                       --where-value-1 1

/usr/share/apparent/python/DbCompare.py --table ess_units \
                                        --column mode \
                                        --where-column id \
                                        --where-value 1 \
                                        --compare-value auto
if [ $? -eq 0 ]
then
  echo "Successful database comparison"
else
  echo "Comparison failed - check log file" >&2
fi

/usr/share/apparent/python/DbCompare.py --table power_regulators \
                                        --column battery_strategy \
                                        --where-column id \
                                        --where-value 1 \
                                        --compare-value variable_discharge
if [ $? -eq 0 ]
then
  echo "Successful database comparison"
else
  echo "Comparison failed - check log file" >&2
fi

