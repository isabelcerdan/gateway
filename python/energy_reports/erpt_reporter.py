#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import os
import time
import random
import socket
from datetime import datetime
import threading
import traceback
import json

from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.DBConstants import *
from lib.db import prepared_act_on_database, FETCH_ONE, FETCH_ALL, EXECUTE
import gateway_utils as gu
import requests
import logging.handlers

# disable "Insecure Request Warnings" since we knowingly query remote meter with no SSL certificate
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

from lib.gateway_constants.GatewayConstants import *

oneDayInSeconds = 86400
onehourInSeconds = 3600

defaultManageOldDataRate = onehourInSeconds
defaultDeleteExportedCutoff = (onehourInSeconds * 2)
defaultDeleteOldDataCutoff = (oneDayInSeconds * 7)
defaultCheckForConfigChange = 10
defaultReportAttemptLimit = 2
defaultLogConfigRate = 3600


# This class send data to energy review server and manages old data in the database.
class ErptReportingThread(threading.Thread):
    def __init__(self, daemon_logger, data_logger):
        super(ErptReportingThread, self).__init__()
        self.logger = daemon_logger
        self.data_logger = data_logger
        self.stoprequest = threading.Event()

        # Reporting configurable items
        self.server = "www.xetinc.com"
        self.port = 8180
        self.rowCountToExport = 50
        self.energyReportingRate = 900
        self.alarmReportingRate = 120
        self.reportTimeout = 5.0

        self.gatewayId = self.getGatewayId()
        self.api = "AcquiSuite/servlet/upload"
        self.mode = "UPLOADARRAY"
        self.reportVersion = 1.0
        self.session = requests.Session()


        # Reporting time configuration items
        self.b2bTime = 10
        self.reportAttemptLimit = defaultReportAttemptLimit
        self.manageOldDataRate = defaultManageOldDataRate
        self.deleteExportedCutoff = defaultDeleteExportedCutoff
        self.deleteAllCutoff = defaultDeleteOldDataCutoff
        self.checkForConfigChange = defaultCheckForConfigChange

        #status items
        self.energyReportingOffset = self.energyReportingRate / 2
        self.energyStagger = random.randint(1, (self.energyReportingRate / 4)) + self.energyReportingOffset
        self.energyStaggerCount = -1
        self.alarmStaggerCount = -1
        self.configStaggerCount = -1
        self.logStaggerCount = defaultLogConfigRate + 1

        self.data_logger("Starting ErptReportingThread")
        self.checkForConfigUpdate()
        self.logger.info("ErptReportingThread Initialized, rate=%u, stagger=%u" %
                         (self.energyReportingRate, self.energyStagger))

        # Set alarm stagger count to be 60 less then the alarm reporting rate
        # so that the first alarm report can go out 60 seconds after process startup
        if (self.alarmReportingRate > 60):
            self.alarmStaggerCount = self.alarmReportingRate - 60

        # Set energy stagger count to 60 less then the energy stagger reporting rate
        # Since the energy reports stagger time does not start till after the 15 minute
        # wall clock boundary, just set the stagger count so that the first report will be
        # attempted after 60 seconds. After the first report the sequnce will sync to
        # wall clock
        self.energyStaggerCount = self.energyStagger - 60



    def run(self):
        self.logger.info("Starting ErptReportingThread")
        queue_err_cnt = 0

        while not self.stoprequest.isSet():
            try:
                time.sleep(1)

                # dump current config to log one an hour
                self.logStaggerCount += 1
                if (self.logStaggerCount > defaultLogConfigRate):
                    self.logStaggerCount = 0
                    self.logConfig("periodic dump", logging.NOTSET)

                #check for a config change for the datbase
                self.configStaggerCount += 1
                if (self.configStaggerCount > self.checkForConfigChange):
                    self.configStaggerCount = 0
                    self.checkForConfigUpdate()

                # check to send alarm report
                self.alarmStaggerCount += 1
                if self.alarmStaggerCount >= self.alarmReportingRate:
                    self.alarmStaggerCount = 0
                    sentOk, sentCount, exportCount = self.buildAlarmReport()

                    self.data_logger("Alarm Report: status=%s, attempted to send=%u, sent=%u" %
                                     (sentOk,
                                      sentCount,
                                      exportCount), logging.INFO)

                    totalRows, rowsExported, rowsToExport, rowsFailedToExport = \
                         self.getEnergyReportCounts(DB_ALARM_REPORTS_TABLE)

                    self.data_logger("%s table, total=%s, exported=%s, pending export=%s, failed to export=%s" %
                                     (DB_ALARM_REPORTS_TABLE,
                                      totalRows,
                                      rowsExported,
                                      rowsToExport,
                                      rowsFailedToExport), logging.INFO)

                    # if able to send last report message and there is more to send,
                    # then send another report in a few seconds
                    if sentOk and (rowsToExport > 0):
                        self.data_logger("Sending Alarm pending export in %u seconds" % self.b2bTime, logging.INFO)
                        self.alarmStaggerCount = self.alarmReportingRate - self.b2bTime
                        nextReport = int(time.time()) + self.b2bTime
                    else:
                        nextReport = int(time.time()) + self.alarmReportingRate

                    timeStr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(nextReport))
                    self.data_logger("**** Next Alarm report at approx %s" % timeStr, logging.INFO)

                # check to send an energy report
                if (self.energyStaggerCount < 0):
                    if (int(time.time()) % self.energyReportingRate) == 0:
                        self.energyStaggerCount = 0
                else:
                    self.energyStaggerCount += 1
                    if (self.energyStaggerCount >= self.energyStagger):
                        self.energyStaggerCount = -1
                        sentOk, sentCount, exportCount = self.buildEnergyReport()

                        self.data_logger("Energy Report: status=%s, attempted to send=%u, sent=%u" %
                                         (sentOk,
                                          sentCount,
                                          exportCount), logging.INFO)

                        totalRows, rowsExported, rowsToExport, rowsFailedToExport = \
                             self.getEnergyReportCounts(DB_ENERGY_REPORTS_TABLE)

                        self.data_logger("%s table, total=%s, exported=%s, pending export=%s, failed to export=%s" %
                                         (DB_ENERGY_REPORT_CONFIG_TABLE,
                                          totalRows,
                                          rowsExported,
                                          rowsToExport,
                                          rowsFailedToExport), logging.INFO)

                        # if able to send last report message and there is more to send,
                        # then send another report in a few seconds
                        if sentOk and (rowsToExport > 0):
                            self.data_logger("Sending Energy pending export in %u seconds" % self.b2bTime, logging.INFO)
                            self.energyStaggerCount = self.energyStagger - self.b2bTime
                            nextReport = int(time.time()) + self.b2bTime
                        else:
                            nextReport = int(time.time()) + self.energyReportingRate

                        timeStr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(nextReport))
                        self.data_logger("**** Next Energy report at approx %s" % timeStr, logging.INFO)

                # call method to remove old data from the database
                if ((int(time.time()) % self.manageOldDataRate) == 0):
                    self.manageOldData()

            except Exception as e:
                self.logger.error('Some sort of Execption. ')
                self.logger.error(traceback.format_exc())
                break

        self.logger.info("ErptReportingThread - ending")

    # Die
    def join(self, timeout=None):
        self.stoprequest.set()
        super(ErptReportingThread, self).join(timeout)

    #
    # Check for a configuration update
    #
    def logConfig(self, msg, logLevel):

        self.data_logger("Erpt Process Configuration - %s" % msg, logLevel)
        self.data_logger("  Report server: %s" % self.server, logLevel)
        self.data_logger("  Report port: %u" % self.port, logLevel)
        self.data_logger("  Energy Reporting Rate: %u seconds, stagger=%u" %
                         (self.energyReportingRate, self.energyStagger), logLevel)
        self.data_logger("  Alarm Reporting Rate: %u seconds" % self.alarmReportingRate, logLevel)
        self.data_logger("  Rows per Batch report: %u" % self.rowCountToExport, logLevel)
        self.data_logger("  Report timeout: %f" % self.reportTimeout, logLevel)
        self.data_logger("  Back To Back reporting delay: %u seconds" % self.b2bTime, logLevel)
        self.data_logger("  Report attempt limit: %u reports" % self.reportAttemptLimit, logLevel)
        self.data_logger("  Manage Old Data rate: %u seconds" % self.manageOldDataRate, logLevel)
        self.data_logger("  Delete Exported Data cutoff: %u seconds in the past" % self.deleteExportedCutoff, logLevel)
        self.data_logger("  Delete All Data cutoff: %u seconds in the past" % self.deleteAllCutoff, logLevel)
        self.data_logger("  Check for configuration changes rate: %u seconds" % self.checkForConfigChange, logLevel)

    #
    # Check for a configuration update
    #
    def checkForConfigUpdate(self):
        configResult = None
        updated = False

        try:
            sql = "SELECT * FROM %s" % DB_ENERGY_REPORT_CONFIG_TABLE
            configResult = prepared_act_on_database(FETCH_ONE, sql, [])

        except Exception as ex:
            self.data_logger("Unable to get %s table Database" % DB_ENERGY_REPORT_CONFIG_TABLE, logging.ERROR)
            self.data_logger(str(ex), logging.ERROR)
            self.data_logger(traceback.format_exc(), logging.ERROR)

        if configResult:
            #server
            if configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_URL_COLUMN_NAME] != self.server:
                self.server = configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_URL_COLUMN_NAME]
                self.data_logger("Updating Report server to: %s" % self.server, logging.INFO)
                updated = True

            #port
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_PORT_COLUMN_NAME]) != self.port:
                self.port = int(configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_PORT_COLUMN_NAME])
                self.data_logger("Updating Report port to: %u" % self.port, logging.INFO)
                updated = True

            # energy report rate
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_RATE_COLUMN_NAME]) != self.energyReportingRate:
                self.energyReportingRate = int(configResult[ENERGY_REPORT_CONFIG_TABLE_INVERTER_ER_RATE_COLUMN_NAME])
                self.energyReportingOffset = self.energyReportingRate / 2
                self.energyStagger = random.randint(1, (self.energyReportingRate / 4)) + self.energyReportingOffset
                self.data_logger("Updating Energy Reporting Rate to: %u, stagger is: %u" %
                                 (self.energyReportingRate, self.energyStagger), logging.INFO)
                updated = True

            #alarm report rate
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_ALARM_RPT_RATE_COLUMN_NAME]) != self.alarmReportingRate:
                self.alarmReportingRate = int(configResult[ENERGY_REPORT_CONFIG_TABLE_ALARM_RPT_RATE_COLUMN_NAME])
                self.data_logger("Updating Alarm Reporting Rate to: %u" % self.alarmReportingRate, logging.INFO)
                updated = True

            #report size
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_REPORTS_PER_BATCH_COLUMN_NAME]) != self.rowCountToExport:
                self.rowCountToExport = int(configResult[ENERGY_REPORT_CONFIG_TABLE_REPORTS_PER_BATCH_COLUMN_NAME])
                self.data_logger("Updating Rows per Batch report to: %u" % self.rowCountToExport, logging.INFO)
                updated = True

            # back-to-back time
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_B2B_RPT_COLUMN_NAME]) != self.b2bTime:
                self.b2bTime = int(configResult[ENERGY_REPORT_CONFIG_TABLE_B2B_RPT_COLUMN_NAME])
                self.data_logger("Updating Back To Back reporting delay to: %u" % self.b2bTime, logging.INFO)
                updated = True

            #  max count for attempting to send a report
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_RPT_ATTEMPT_LIMIT_COLUMN_NAME]) != self.reportAttemptLimit:
                self.reportAttemptLimit = int(configResult[ENERGY_REPORT_CONFIG_TABLE_RPT_ATTEMPT_LIMIT_COLUMN_NAME])
                self.data_logger("Updating Report attempt limit to: %u" % self.reportAttemptLimit, logging.INFO)
                updated = True

            # rate at which old data is checked
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_MANAGE_DATA_RATE_COLUMN_NAME]) != self.manageOldDataRate:
                self.manageOldDataRate = int(configResult[ENERGY_REPORT_CONFIG_TABLE_MANAGE_DATA_RATE_COLUMN_NAME])
                self.data_logger(" Manage Old Data rate to: %u" % self.manageOldDataRate, logging.INFO)
                updated = True
            # cutoff seconds in past for deleting exported data
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_DELETE_EXPORT_CUTOFF_COLUMN_NAME]) != self.deleteExportedCutoff:
                self.deleteExportedCutoff = int(configResult[ENERGY_REPORT_CONFIG_TABLE_DELETE_EXPORT_CUTOFF_COLUMN_NAME])
                self.data_logger("Updating Delete Exported Data cutoff to: %u" % self.deleteExportedCutoff, logging.INFO)
                updated = True

            # cutoff seconds in the past for deleting all data
            if int(configResult[ENERGY_REPORT_CONFIG_TABLE_DELETE_ALL_CUTOFF_COLUMN_NAME]) != self.deleteAllCutoff:
                self.deleteAllCutoff = int(configResult[ENERGY_REPORT_CONFIG_TABLE_DELETE_ALL_CUTOFF_COLUMN_NAME])
                self.data_logger("Updating Delete All Data cutoff to: %u" % self.deleteAllCutoff, logging.INFO)
                updated = True

            # cutoff seconds in the past for deleting all data
            if float(configResult[ENERGY_REPORT_CONFIG_TABLE_REPORT_TIMEOUT_COLUMN_NAME]) != self.reportTimeout:
                self.reportTimeout = float(configResult[ENERGY_REPORT_CONFIG_TABLE_REPORT_TIMEOUT_COLUMN_NAME])
                self.data_logger("Updating report timeout to: %f" % self.reportTimeout, logging.INFO)
                updated = True

        else:
            self.data_logger("Unable to read config data from the database", logging.ERROR)

        # log all the configuration
        if updated == True:
            self.logConfig("updated config", logging.DEBUG)


    #
    # Get the Gateway ID to send to energy review
    #
    def getGatewayId(self):
        from uuid import getnode as get_mac
        mac = '-'.join('%02X' % ((get_mac() >> 8 * i) & 0xff) for i in reversed(xrange(6)))
        results = None

        try:
            sql = "SELECT %s FROM %s" % \
                  (ER_CONFIG_GATEWAY_BASE_SN_COLUMN,
                   DB_ENERGY_REPORT_CONFIG_TABLE)

            results = prepared_act_on_database(FETCH_ONE, sql, [])
        except Exception as ex:
            self.data_logger("Unable to get Gateway ID from the Database", logging.ERROR)
            self.data_logger(str(ex), logging.ERROR)
            self.data_logger(traceback.format_exc(), logging.ERROR)

        if results and \
           results[ER_CONFIG_GATEWAY_BASE_SN_COLUMN] is not None and \
           len(results[ER_CONFIG_GATEWAY_BASE_SN_COLUMN]) > 0:

            gateway_id = '-'.join([results[ER_CONFIG_GATEWAY_BASE_SN_COLUMN], mac[-8:]])
        else:
            gateway_id = mac

        return gateway_id

    #
    # removes old data from the database
    #
    def manageOldData(self):
        #
        # Delete exported data older then the cutoff
        #
        exportedDeleteCutoff = int(time.time()) - self.deleteExportedCutoff

        sql_string = "DELETE FROM %s WHERE %s>0 AND %s<%s;" % \
                     (DB_ENERGY_REPORTS_TABLE,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      "%s")

        sql_args = [exportedDeleteCutoff, ]
        result = prepared_act_on_database(EXECUTE_ROW_COUNT, sql_string, sql_args)
        if result > 0:
            self.data_logger("Deleted " + str(result) +
                             " exported energy reports before " +
                             str(exportedDeleteCutoff), logging.INFO)


        sql_string = "DELETE FROM %s WHERE %s>0 AND %s<%s;" % \
                     (DB_ALARM_REPORTS_TABLE,
                     ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      "%s")

        sql_args = [exportedDeleteCutoff, ]
        result = prepared_act_on_database(EXECUTE_ROW_COUNT, sql_string, sql_args)
        if result > 0:
          self.data_logger("Deleted " + str(result) +
                           " exported alarm reports before " +
                           str(exportedDeleteCutoff), logging.INFO)


        #
        # flush all data older then the Flush Cutoff
        #
        flushDataCutoff = int(time.time()) - self.deleteAllCutoff

        sql_string = "DELETE FROM %s WHERE %s<%s;" % \
                     (DB_ENERGY_REPORTS_TABLE,
                      ENERGY_REPORT_UTC_START_TIME_COLUMN,
                      "%s")

        sql_args = [flushDataCutoff, ]
        result = prepared_act_on_database(EXECUTE_ROW_COUNT, sql_string, sql_args)
        if result > 0:
            self.data_logger("Deleted " + str(result) +
                             " energy reports before " +
                             str(flushDataCutoff), logging.INFO)

        sql_string = "DELETE FROM %s WHERE %s<%s;" % \
                     (DB_ALARM_REPORTS_TABLE,
                      ALARM_REPORT_UTC_START_TIME_COLUMN,
                      "%s")

        sql_args = [flushDataCutoff, ]
        result = prepared_act_on_database(EXECUTE_ROW_COUNT, sql_string, sql_args)

        if result > 0:
            self.data_logger("Deleted " + str(result) +
                             " alarm reports before " +
                             str(flushDataCutoff), logging.INFO)

    #
    # get counts of data items from the data base alarm and energy report tables
    #
    def getEnergyReportCounts(self, dbTable):
        
        #
        # Get total rows in the database
        #
        sql_string = "SELECT COUNT(*) FROM %s " %  (dbTable)
                      
        totalRows = prepared_act_on_database(FETCH_ROW_COUNT, sql_string, ())

        #
        # Get count of rows exported
        #
        sql_string = "SELECT COUNT(*) FROM %s WHERE %s !=0" % \
                     (dbTable,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN)

        rowsExported = prepared_act_on_database(FETCH_ROW_COUNT, sql_string, ())

        #
        # Get count of rows to be exported
        #
        sql_string = "SELECT COUNT(*) FROM %s WHERE %s=0 AND %s<%s" % \
              (dbTable,
               ENERGY_REPORT_EXPORT_TIME_COLUMN,
               ENERGY_REPORT_SENT_COLUMN,
               self.reportAttemptLimit)
        
        rowsToExport = prepared_act_on_database(FETCH_ROW_COUNT, sql_string, ())

        #
        # Get count of rows failed to be  exported
        #
        sql_string = "SELECT COUNT(*) FROM %s WHERE %s=0 AND %s>=%s" % \
                     (dbTable,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      ENERGY_REPORT_SENT_COLUMN,
                      self.reportAttemptLimit)

        rowsFailedToExport = prepared_act_on_database(FETCH_ROW_COUNT, sql_string, ())

        return totalRows, rowsExported, rowsToExport, rowsFailedToExport


    #
    # Build Energy report to send to Energy Review
    #
    def buildEnergyReport(self):

        sql_string = "SELECT * FROM %s " \
                     "WHERE %s<%s AND %s=0 " \
                     "ORDER BY %s ASC LIMIT %s;" % \
                     (DB_ENERGY_REPORTS_TABLE,
                      ENERGY_REPORT_SENT_COLUMN,
                      self.reportAttemptLimit,
                      ENERGY_REPORT_EXPORT_TIME_COLUMN,
                      ENERGY_REPORT_UTC_START_TIME_COLUMN,
                      "%s")
        sql_args = [self.rowCountToExport, ]
        response = prepared_act_on_database(FETCH_ALL, sql_string, sql_args)

        if (len(response)) > 0:
            row_id_list = []
            report_list = []
            for idx in range(0, len(response)):
                report = ""
                row_id_list.append(int(response[idx][ENERGY_REPORT_ROW_ID_COLUMN]))

                sn_data = gu.get_db_inverters_row_data_by_sn(response[idx][ENERGY_REPORT_SERIAL_NUMBER_COLUMN])

                report += str(response[idx][ENERGY_REPORT_SERIAL_NUMBER_COLUMN] )+ ","
                report += str(response[idx][ENERGY_REPORT_DATA_SCHEMA_COLUMN]) + ","
                report += str(sn_data[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME]) + ","
                report += str(sn_data[INVERTERS_TABLE_VERSION_COLUMN_NAME]) + ","
                report += str(response[idx][ENERGY_REPORT_UTC_START_TIME_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_DATA_INTERVAL_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_FLAGS_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_DCV_SOLAR_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_DCI_SOLAR_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_ACV_OUT_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_ACI_OUT_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_PHASE_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_WATT_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_VAR_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_TEMP_COLUMN])

                report_list.append(report)

            retVal, exportCount = self.sendReport(DB_ENERGY_REPORTS_TABLE, report_list, row_id_list)
            return (retVal, len(report_list), exportCount)

        return True, 0, 0


    #
    # Build Alarm report
    #
    def buildAlarmReport(self):

        sql_string = "SELECT * FROM %s " \
                     "WHERE %s<%s AND %s=0 " \
                     "ORDER BY %s ASC LIMIT %s;" % \
                     (DB_ALARM_REPORTS_TABLE,
                      ALARM_REPORT_SENT_COLUMN,
                      self.reportAttemptLimit,
                      ALARM_REPORT_EXPORT_TIME_COLUMN,
                      ALARM_REPORT_UTC_START_TIME_COLUMN,
                      "%s")

        sql_args = [self.rowCountToExport, ]
        response = prepared_act_on_database(FETCH_ALL, sql_string, sql_args)

        if (len(response)) > 0:
            row_id_list = []
            report_list = []
            for idx in range(0, len(response)):
                report = ""
                row_id_list.append(int(response[idx][ALARM_REPORT_ROW_ID_COLUMN]))
                try:
                    sn_data = gu.get_db_inverters_row_data_by_sn(response[idx][ENERGY_REPORT_SERIAL_NUMBER_COLUMN])
                except Exception as ex:
                    self.data_logger("Inverter serial number %s not in database" %
                                     str(response[idx][ENERGY_REPORT_SERIAL_NUMBER_COLUMN]), logging.WARN)
                    self.data_logger(str(ex), logging.WARN)
                    self.data_logger(traceback.format_exc(), logging.WARN)
                    continue

                report += str(response[idx][ENERGY_REPORT_SERIAL_NUMBER_COLUMN]) + ","
                report += str(response[idx][ENERGY_REPORT_DATA_SCHEMA_COLUMN]) + ","
                report += str(sn_data[INVERTERS_TABLE_PRODUCT_PART_NUMBER_COLUMN_NAME]) + ","
                report += str(sn_data[INVERTERS_TABLE_VERSION_COLUMN_NAME]) + ","
                report += str(response[idx][ENERGY_REPORT_UTC_START_TIME_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_0_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_1_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_2_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_3_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_4_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_5_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_6_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_NEW_STATE_7_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_0_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_1_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_2_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_3_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_4_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_5_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_6_COLUMN]) + ","
                report += str(response[idx][ALARM_REPORT_PREV_STATE_7_COLUMN])

                report_list.append(report)

            retVal, exportCount = self.sendReport(DB_ALARM_REPORTS_TABLE, report_list, row_id_list)
            return (retVal, len(report_list), exportCount)

        return True, 0, 0

    #
    # send  report
    #
    def sendReport(self, dbTable, report_list, row_id_list):
        retVal = False
        exportCount = 0
        data = []


        headers = {'content-type': 'application/json'}
        msgParams = {"VERSION": self.reportVersion,
                     "MODE": self.mode,
                     "SN": self.gatewayId,
                     "COUNT": len(report_list)}

        self.url = "http://%s:%s" % (self.server, self.port)
        self.path = os.path.join(self.url, self.api)

        self.data_logger("Send %s to: %s" % (dbTable, self.path), logging.INFO)
        for idx in range(0, len(report_list)):
            d = {str(row_id_list[idx]): report_list[idx]}
            self.data_logger(str(d), logging.DEBUG)
            data.append(d)

        # send data
        with requests.session() as s:
            s.keep_alive = False
            try:
                response = s.post(self.path,
                                  params=msgParams,
                                  data=json.dumps(data),
                                  headers=headers,
                                  verify=False,
                                  timeout=self.reportTimeout)

            except requests.ConnectionError as ex:
                self.data_logger("POST request to %s failed" % self.path, logging.WARN)
                self.data_logger(str(ex), logging.WARN)
                return retVal, exportCount

            except Exception as ex:
                self.data_logger("POST request to %s failed" % self.path, logging.ERROR)
                self.data_logger(str(ex), logging.ERROR)
                self.data_logger(traceback.format_exc(), logging.ERROR)
                return retVal, exportCount

            try:
                # update sent count
                row_id_list_str = ",".join(map(str, row_id_list))

                sql_string = "UPDATE %s SET %s= %s + 1 " \
                             "WHERE %s IN (%s);" % \
                             (dbTable,
                              REPORT_SENT_COLUMN,
                              REPORT_SENT_COLUMN,
                              REPORT_ROW_ID_COLUMN,
                              row_id_list_str)

                result = prepared_act_on_database(EXECUTE, sql_string, None)

                retVal, exportCount = self.handleResponse(dbTable,
                                             response,
                                             row_id_list)

            except Exception as ex:
                self.data_logger("Processing response to %s failed" % self.path, logging.WARN)
                self.data_logger(str(ex), logging.WARN)
                self.data_logger(traceback.format_exc(), logging.WARN)
                return retVal, exportCount

        return True, exportCount

    #
    # handle the response
    #
    def handleResponse(self, dbTable, response, row_id_list):

        code = response.status_code
        header = response.headers

        if code != 200:
            self.data_logger("request url = " + str(response.request.url), logging.CRITICAL)
            self.data_logger("request header = " + str(response.request.headers), logging.CRITICAL)
            self.data_logger("response header = " + str(response.headers), logging.CRITICAL)
            self.data_logger("response  = " + str(response) + " " + str(response.headers), logging.CRITICAL)
            self.data_logger("response content= " + str(response.content), logging.CRITICAL)
            return False, 0

        if "SUCCESS" in response.content:
            self.data_logger("Response: Success from Energy Review", logging.INFO)
        else:
            if (header['content-type'] == 'application/json'):
                jsonData = json.loads(response.content)

                if 'version' in jsonData:
                    respVersion = jsonData['version']
                    self.data_logger("Response Version = %s" % respVersion, logging.INFO)

                if 'badRows' in jsonData:
                    badRows = jsonData['badRows']

                    for idx in range(0, len(badRows)):
                        if badRows[idx] in row_id_list:
                            row_id_list.remove(badRows[idx])
                            self.data_logger("badRow = %u" % badRows[idx], logging.INFO)
                        else:
                            self.data_logger("badRow %u is not in data list" % badRows[idx], logging.INFO)
                else:
                    self.data_logger("No BadRows in data", logging.INFO)



        #build list of rows to set exported time
        if (len(row_id_list) > 0):
            row_id_list_str = ",".join(map(str, row_id_list))

            sql_string = "UPDATE %s SET %s=%s " \
                         "WHERE %s IN (%s);" % \
                         (dbTable,
                          REPORT_EXPORT_TIME_COLUMN,
                          "%s",
                          REPORT_ROW_ID_COLUMN,
                          row_id_list_str)

            result = prepared_act_on_database(EXECUTE, sql_string, [int(time.time()),])
        else:
            self.data_logger("Response: All sent data is on badRow list", logging.INFO)

        return True, len(row_id_list)
