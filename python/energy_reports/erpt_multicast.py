#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8
import os
import time
import socket
from datetime import datetime
import threading
import Queue
from Queue import Empty

from lib.gateway_constants.GatewayConstants import *
from lib.gateway_constants.InverterConstants import *
import gateway_utils as gu
from energy_reports.erpt_data_handler import *


# Globals
queue = Queue.Queue(maxsize=0)


ERPT_RECEIVE_SOCKET_TIMEOUT = 10.0
ERPT_QUEUE_WAIT_SECONDS = 10.0

# Class to receive the response to the multicast messages.  The responses are pushed on to the queue for handling
# by the process thread class
class ErptReceiveThread(threading.Thread):
    def __init__(self, daemon_logger, data_logger):
        super(ErptReceiveThread, self).__init__()
        self.recv_socket = None
        self.lan_address = None
        self.logger = daemon_logger
        self.data_logger = data_logger
        self.stoprequest = threading.Event()
        self.logger.info("ErptReceiveThread Initialized")


    def setup_recv_socket(self):
        # Initialize the socket. SOCK_DGRAM specifies a UDP datagram.
        self.lan_address = gu.get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
        if self.lan_address is None:
            self.logger.error('Unable to get LAN IP address to bind to.')
            return False

        try:
            self.recv_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            self.recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.recv_socket.settimeout(ERPT_RECEIVE_SOCKET_TIMEOUT)

        except Exception as error:
            err_string = "init socket failed - %s" % error
            self.logger.error(err_string)
            return False

        # Bind socket to local host and port
        try:
            self.logger.info("Binding to %s, %d" % (self.lan_address, ERPT_DATA_PORT))
            self.recv_socket.bind((self.lan_address, ERPT_DATA_PORT))

        except socket.error, msg:
            err_string = "Bind failed. Error Code : " + str(msg[0]) + " Message " + msg[1]
            self.logger.error(err_string)
            self.close_recv_socket()
            return False

        return True


    def close_recv_socket(self):
        self.logger.info('Closing Socket')
        retVal = False

        if self.recv_socket is None:
            pass
        else:
            try:
                self.recv_socket.close()
            except Exception as error:
                error_string = 'Error closing receive socket - %s' % repr(error)
                self.logger.error(error_string)
                retVal = True

        return retVal

    def run(self):
        self.logger.info("Starting ErptReceiveThread")
        retVal = True

        if not self.setup_recv_socket():
            self.logger.error("Unable to set up receive socket for HOS audit thread.")
            retVal = False
        else:
            while not self.stoprequest.isSet():
                try:
                    my_data, my_addr = self.recv_socket.recvfrom(1024)
                    data_packet = [my_data, my_addr, time.time()]

                except socket.timeout:
                    # Check for reason to exit
                    continue

                except Exception as error:
                    self.logger.error('Socket receive error - %s' % error)
                    retVal = False
                    break

                try:
                    queue.put(data_packet)

                except Exception as err:
                    self.logger.error('Error putting message data on queue - %s' % err)
                    retVal = False
                    break


        self.close_recv_socket()
        return retVal

    def join(self, timeout=None):
        self.stoprequest.set()
        super(ErptReceiveThread, self).join(timeout)



# This class processes the response message put on the queue by the receive thread. The PsaProcessData class does the
# actual work of unpacking the response and building the display string
class ErptProcessThread(threading.Thread):
    def __init__(self, daemon_logger, data_logger):
        super(ErptProcessThread, self).__init__()
        self.logger = daemon_logger
        self.data_logger = data_logger
        self.stoprequest = threading.Event()

        self.logger.info("ErptProcessThread Initialized")

    def handleResponse(self, ipAddr, tlvRsp, msgRxTime):
        pd = ErptProcessData(ipAddr, tlvRsp, self.logger, self.data_logger)
        pd.processData(msgRxTime)
        return True

    def run(self):
        self.logger.info("Starting ErptProcessThread")
        queue_err_cnt = 0

        while not self.stoprequest.isSet():
            try:
                port_packet = queue.get(timeout=ERPT_QUEUE_WAIT_SECONDS)

            except Empty:
                # Check for reason to exit
                continue

            except Exception as error:
                error_string = 'queue get failed - %s' % repr(error)
                self.logger.error(error_string)
                queue_err_cnt += 1
                if queue_err_cnt > 5:
                    self.logger.error('Unable to retrieve events from queue.')
                    # Exiting
                    break

                continue

            try:
                # arguments are ip address, response data, and received time object
                self.handleResponse(port_packet[1][0], port_packet[0], port_packet[2])
                queue_err_cnt = 0

            except Exception as err:
                error_string = 'Unable to process responses: %s' % repr(err)
                self.logger.error(error_string)


    def join(self, timeout=None):
        self.stoprequest.set()
        super(ErptProcessThread, self).join(timeout)


