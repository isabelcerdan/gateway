#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_ERPT_DAEMON_RESTARTED

import os
import time
import math
import socket
import struct
import calendar
from datetime import datetime

import time

import gateway_utils as gu
from lib.db import prepared_act_on_database
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.logging_setup import setup_logger
import traceback

import logging.handlers
from energy_reports.erpt_multicast import *
from energy_reports.erpt_reporter import *



class ErptDaemon(Daemon):

    alarm_id = ALARM_ID_ERPT_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(ErptDaemon, self).__init__(**kwargs)
        self.pid = 0                        # process ID, read from system at process start
        self.detail_log = 0                 # detailed INFO log info flag, read from the Db
        self.last_update_time = 0

        self.dumpPath = kwargs.get('dumpPath', "/var/log/apparent/erpt.log")
        self.dumpFile = None
        self.log_level = logging.INFO
        self.checkDebugLevel = 14

    def logStringToLevel (self, levelString):
        level = 20

        if (levelString == 'NOTSET'):
            level = logging.NOTSET
        elif (levelString == 'DEBUG'):
            level = logging.DEBUG
        elif (levelString == 'INFO'):
            level = logging.INFO
        elif (levelString == 'WARNING'):
            level = logging.WARN
        elif (levelString == 'WARN'):
            level = logging.WARN
        elif (levelString == 'ERROR'):
            level = logging.ERROR
        elif (levelString == 'FATAL'):
            level = logging.FATAL
        elif (levelString == 'CRITICAL'):
            level = logging.FATAL

        return level

    def logLevelToString(self, level):
        levelString = 'INFO'

        if (level == logging.NOTSET):
            levelString = 'NOTSET'
        elif (level == logging.DEBUG):
            levelString = 'DEBUG'
        elif (level == logging.INFO):
            levelString = 'INFO'
        elif (level == logging.WARN):
            levelString = 'WARN'
        elif (level == logging.WARNING):
            levelString = 'WARN'
        elif (level == logging.ERROR):
            levelString = 'ERROR'
        elif (level == logging.FATAL):
            levelString = 'FATAL'
        elif (level == logging.CRITICAL):
            levelString = 'FATAL'

        return levelString
    #
    # Logging function
    #
    def erpt_log(self, logStr, level=0):
        if (level < self.log_level and level != 0):
            return

        try:
            self.dumpFile = open(self.dumpPath, 'a')
        except Exception:
            self.logger.error("FAILED TO OPEN file = " + str(self.dumpPath))
            return

        now = time.time()
        nowStr = time.strftime("%Y-%m-%d %H:%M:%S.")
        msec = int(round(now * 1000)) % 1000
        self.dumpFile.write(nowStr +
                            ("%03u [%s] " % (msec, self.logLevelToString(level))) +
                            logStr)

        self.dumpFile.write("\r\n")
        self.dumpFile.flush()

        self.dumpFile.close()

    #
    # Update time for daemon control
    #
    def get_last_time(self):
        return self.last_update_time

    def set_last_time(self, now):
        self.last_update_time = now

    def updateRunning(self):
        now = int(time.time())

        if (self.get_last_time() + SET_UPDATE_BIT_FREQUENCY) <= now:
            self.update()
            self.set_last_time(now)

            self.checkDebugLevel += 1
            if (self.checkDebugLevel >= 12):
                self.checkDebugLevel = 0

                dbLogLevel = logging.INFO
                sql = "SELECT log_level FROM gateway"
                result = prepared_act_on_database(FETCH_ONE, sql, [])
                if result:
                    dbLogLevel = result['log_level']

                self.log_level = self.logStringToLevel(dbLogLevel)

    #
    # Startup processing threads
    #
    def threadStartup(self):
        self.rxThread = ErptReceiveThread(self.logger, self.erpt_log)
        self.processThread = ErptProcessThread(self.logger, self.erpt_log)
        self.reportThread = ErptReportingThread(self.logger, self.erpt_log)

        try:
            self.logger.debug("Initializing processing thread...")
            self.processThread.daemon = True
            self.processThread.start()
        except:
            self.logger.error("Unable to start processing thread.")
            self.logger.error(traceback.format_exc())
            return False

        try:
            self.logger.debug("Initializing receive thread...")
            self.rxThread.daemon = True
            self.rxThread.start()
        except:
            self.logger.error("Unable to start receive thread.")
            self.logger.error(traceback.format_exc())
            # kill the process thread on the way out
            self.processThread.join()
            return False

        try:
            self.logger.debug("Initializing reporter thread...")
            self.reportThread.daemon = True
            self.reportThread.start()
        except:
            self.logger.error("Unable to start reporter thread.")
            self.logger.error(traceback.format_exc())
            # kill the process thread on the way out
            self.rxThread.join()
            self.processThread.join()
            return False

        return True

    #
    # Kill (cleanly) the child threads
    def exitClean(self):
        if self.reportThread is not None:
            self.logger.info("Erpt - killing reporter Thread")
            self.reportThread.join()
            self.reportThread = None

        if self.rxThread is not None:
            self.logger.info("Erpt - killing RX Thread")
            self.rxThread.join()
            self.rxThread = None

        if self.processThread is not None:
            self.logger.info("Erpt - killing Process Thread")
            self.processThread.join()
            self.processThread = None

    #
    #
    # Main loop
    #
    def run(self):

        self.updateRunning()
        self.logger.info("Erpt Starting")

        ok = self.threadStartup()
        if not ok:
            self.exitClean()
            exit(-1)

        # Run forever until eternity is reached.
        while True:
            time.sleep(1)
            self.updateRunning()

            if (not self.rxThread.is_alive()) or \
                (not self.processThread.is_alive()) or \
                (not self.reportThread.is_alive()):

                self.logger.info("Erpt - a thread ended, killing other threads")
                self.exitClean()
                self.logger.info("Erpt - exiting")
                self.erpt_log("Erpt - exiting")

                time.sleep(1)

        # end while

    # end run function


if __name__ == '__main__':
    ErptDaemon()
