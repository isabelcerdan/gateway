#!/usr/share/apparent/.py2-virtualenv/bin/python
# encoding: utf-8

import socket
import traceback

import gateway_utils as gu
import tlv_utils
from inverterMessages.tlvMsgPeek import *
from inverterMessages.tlvObjectList import *
from inverterMessages.tlvMasterSG424 import *
from inverterMessages.tlvEnergyMsg import *
from inverterMessages.tlvAlarmMsg import *
from datetime import datetime



# process the alarm and energy data messages from the inverter and send acknowledge message
# back to the inverter
class ErptProcessData(object):
    def __init__(self, ipAddr, rspData, daemon_logger, data_logger):
        self.rspData = rspData
        self.ipAddr = ipAddr
        self.energyObj = None
        self.alarmObj = None

        self.logger = daemon_logger
        self.data_logger = data_logger

    #
    # process the TLV message from the inverter
    #
    def processData(self, rxTime):
        try:
            tlvList = msgPeek(False, self.rspData)
            msgs = tlvList.getTlvDict()

            for key in msgs:
                try:
                    obj = TlvObjects(key, self.ipAddr)
                    tlvObj = obj.getObj()
                    tlvObj.unpackDataStruct(msgs[key])

                    if tlvObj.getTlvId() == SG424_TO_GTW_MASTER_U16_TLV_TYPE:
                        pass

                    elif tlvObj.getTlvId() == SG424_TO_GTW_ENERGY_REPORT_STRUCTURED_TLV_TYPE:
                        self.energyObj = tlvObj
                        self.data_logger(str(self.ipAddr) + self.energyObj.logStr(), logging.DEBUG)

                        ok = self.persistData(tlvObj, rxTime)
                        ackTlv = self.energyObj.buildAcknowledgeData(rxTime, ok)
                        self.send_acknowledge(ackTlv, "Energy", tlvObj.getDataValByName("utcTimestampStart"))

                    elif tlvObj.getTlvId() == SG424_TO_GTW_ALARM_REPORT_STRUCTURED_TLV_TYPE:
                        self.alarmObj = tlvObj
                        leadStr = str(self.ipAddr) + self.alarmObj.logStr()
                        for k, v in self.alarmObj.changeList.items():
                            self.data_logger(leadStr + " " + v['alarmName'] + ": " + v['status'], logging.DEBUG)

                        ok = self.persistData(tlvObj, rxTime)
                        ackTlv = self.alarmObj.buildAcknowledgeData(rxTime, ok)
                        self.send_acknowledge(ackTlv, "Alarm", tlvObj.getDataValByName("utcTimestampStart"))

                    else:
                        self.data_logger("Unknown response TLV, id=%u" % tlvObj.tlvId(), logging.WARN)

                except TlvExceptionWrongSize as e:
                    if len(msgs[key]) > 0:
                        print(TlvDataObject.dumpHexBuf(msgs[key]))
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except TlvExceptionNoVersion as e:
                    if len(msgs[key]) > 0:
                        print(TlvDataObject.dumpHexBuf(msgs[key]))
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except TlvExceptionUnsupportedVersion as e:
                    if len(msgs[key]) > 0:
                        print(TlvDataObject.dumpHexBuf(msgs[key]))
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except TlvExceptionNoMaster as e:
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except TlvExceptionCannotParseData as e:
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except TlvExceptionCannotLoadData as e:
                    self.logger.error(e.msg)
                    self.logger.error(traceback.format_exc())

                except Exception as e:
                    self.logger.error(("ErptProcessData, Can't parse TLV id=%u,  " % key) + str(e))
                    self.logger.error(traceback.format_exc())

        except Exception as e:
            errorStr =  "Unable to parse response: " + str(e)
            self.logger.error(errorStr)
            self.logger.error(traceback.format_exc())

    #
    # Build a long response (vertical) for debugging
    #
    def persistData(self, tlvObj, rxTime):
        persistOk = True

        try:
            tlvObj.persistData(rxTime)

        except MySQLdb.IntegrityError as ex:
            self.data_logger("IntegrityError - Duplicate Data", logging.WARN)
            self.data_logger(str(ex), logging.WARN)
            self.data_logger(traceback.format_exc(), logging.WARN)

        except Exception as ex:
            self.data_logger(str(ex), logging.ERROR)
            self.data_logger(traceback.format_exc(), logging.ERROR)
            persistOk = False

        return persistOk

    #
    # Build a long response (vertical) for debugging
    #
    def longDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.energyObj is not None:
                rspString += self.energyObj.dumpDecodedBuf()

            if self.alarmObj is not None:
                rspString += self.alarmObj.dumpDecodedBuf()

        except Exception as e:
            errorStr =  "Unable to build long response buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)
            self.logger.error(traceback.format_exc())


        return rspString

    #
    # BUild short string for debugging and logging
    #
    def shortDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.energyObj is not None:
                rspString += " | " + self.energyObj.dumpShortDecodedBuf()

            if self.alarmObj is not None:
                rspString += "|" + self.alarmObj.dumpShortDecodedBuf()

        except Exception as e:
            errorStr =  "Unable to build short response buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)
            self.logger.error(traceback.format_exc())

        return rspString

    #
    # build header string for debug output
    #
    def getShortHeaders(self):
        labelString = ""
        itemsString = ""

        try:
            #Build response string in following order
            if self.energyObj is not None:
                str1, str2 = self.energyObj.dumpHeaderStrings()
                labelString += " | " + str1
                itemsString += " | " + str2

            if self.alarmObj is not None:
                str1, str2 = self.alarmObj.dumpHeaderStrings()
                labelString += " | " + str1
                itemsString += " | " + str2

        except Exception as e:
            errorStr =  "Unable to build header string buffer: " + str(e)
            labelString += errorStr
            itemsString += errorStr
            self.logger.error(errorStr)
            self.logger.error(traceback.format_exc())

        return labelString, itemsString

    #
    # dump data in hex format - for debugging
    #
    def hexDump(self):
        rspString = ""

        try:
            #Build response string in following order
            if self.energyObj is not None:
                rspString += self.energyObj.dumpHexBuf(self.rspData)

            if self.alarmObj is not None:
                rspString += self.alarmObj.dumpHexBuf(self.rspData)

        except Exception as e:
            errorStr =  "Unable to build hex buffer: " + str(e)
            rspString += errorStr
            self.logger.error(errorStr)
            self.logger.error(traceback.format_exc())

        return rspString

    #
    # Send acknowledge message back to the inverter
    #
    def send_acknowledge(self, ackTlv, msgType, utc):

        retVal = False
        number_of_user_tlv = 1
        packed_data = gu.add_master_tlv(number_of_user_tlv)
        packed_data += ackTlv

        # OPEN THE SOCKET:
        try:
            fancy_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        except Exception:  # as error:
            self.logger.error("socket.socket failed ...")

        else:
            # SEND THE PACKET:
            try:
                fancy_socks.sendto(packed_data, (self.ipAddr, SG424_PRIVATE_UDP_PORT))
                self.data_logger("Ack " + msgType +
                                 ", IP=" + self.ipAddr +
                                 ", utc=" + str(utc), logging.DEBUG)


            except Exception:  # as error:
                self.logger.error("sendto failed ...")

            # Close the socket:
            try:
                fancy_socks.close()

            except Exception:  # as error:
                self.logger.error("close socket failed ...")

            else:
                retVal = True

        return retVal

