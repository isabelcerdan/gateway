#!/usr/share/apparent/.py2-virtualenv/bin/python
'''
Notify Daemon

Created on Mar 10, 2017

@copyright: Apparent Inc. 2017

@author: steve
'''

import signal
import time
from datetime import datetime, timedelta

from lib.gateway_constants.DBConstants import *
from threading import Thread
from smtplib import SMTPException
import smtplib
import email.utils
from email.mime.text import MIMEText
from lib.db import prepared_act_on_database, FETCH_ONE, EXECUTE
from lib.logging_setup import *
from daemon.daemon import Daemon
from alarm.alarm_constants import ALARM_ID_NOTIFY_DAEMON_RESTARTED


NOTIFY_DAEMON_MAX_DB_ERRORS = 10
NOTIFY_DAEMON_MAX_SMTP_ERRORS = 10
UPDATE_INTERVAL = 5


class NotifyDaemon(Daemon):
    alarm_id = ALARM_ID_NOTIFY_DAEMON_RESTARTED

    def __init__(self, **kwargs):
        super(NotifyDaemon, self).__init__(**kwargs)
        self.db_error_cnt = 0
        self.next_heartbeat = datetime.now()
        self.smtp_error_cnt = 0

    def work(self):
        mysql_string = 'select * from %s;' % DB_PROTECTION_ALERT_TABLE
        args = []
        try:
            alert_results = prepared_act_on_database(FETCH_ALL, mysql_string, args)
        except Exception as error:
            error_string = 'Failed to fetch PROTECTION ALERT fields from the database - %s' % repr(error)
            self.logger.error(error_string)
            self.db_error_cnt += 1
            if self.db_error_cnt > NOTIFY_DAEMON_MAX_DB_ERRORS:
                self.logger.error('Maximum database error count exceeded.')
                return False  # pull the plug...
            return True
        else:
            if not alert_results:
                # No alerts - we're good.
                return True

            for alert_result in alert_results:
                #mac_addr = alert_result[PROTECTION_ALERT_MAC_ADDR]
                #ip_addr = alert_result[PROTECTION_ALERT_IP_ADDR]
                device_name = alert_result[PROTECTION_ALERT_DEVICE_NAME]
                alert_time = alert_result[PROTECTION_ALERT_ALERT_TIME]
                input_name = alert_result[PROTECTION_ALERT_INPUT_NAME]

                # Grab the intended email fields from the database.
                mysql_string = 'select * from %s;' % DB_NETPROTECT_EMAIL_TABLE
                args = []
                try:
                    email_results = prepared_act_on_database(FETCH_ALL, mysql_string, args)
                except Exception as err:
                    err_string = 'Unable to fetch email info from database for device %s - %s' % \
                                 (alert_result[PROTECTION_ALERT_DEVICE_NAME], repr(error))
                    self.logger.error(err_string)
                    self.db_error_cnt += 1
                    if self.db_error_cnt > NOTIFY_DAEMON_MAX_DB_ERRORS:
                        self.logger.error('Maximum allowed database errors exceeded.')
                        return False
                    continue

                if not email_results:
                    self.logger.error('Netprotect Email table contains no entries.  Please provision a notification recipient.')
                    return False

                for email_result in email_results:
                    # Clear to send: sent_count == 0 or now >= next_send_time.
                    send_count = int(email_result[NETPROTECT_EMAIL_SEND_COUNT])
                    num_sent = int(email_result[NETPROTECT_EMAIL_NUM_RESENDS])
                    if send_count >= num_sent:
                        # No more spamming.
                        continue

                    # Build the message body.
                    if send_count == 0 or datetime.now() >= email_result[NETPROTECT_EMAIL_NEXT_SEND_TIME]:
                        if email_result[NETPROTECT_EMAIL_INPUT_NAME] != input_name:
                            continue

                        # Good to go - send the email.
                        msg_body = email_result[NETPROTECT_EMAIL_MSG_BODY] + '  ' + \
                                   input_name + ' at ' + device_name + ' on ' + str(alert_time) + '.'
                        address = email_result[NETPROTECT_EMAIL_EMAIL_ADDR]
                        name = email_result[NETPROTECT_EMAIL_RECIPIENT_NAME]
                        msg = MIMEText(msg_body)
                        msg['To'] = email.utils.formataddr((name, address))
                        msg['From'] = email.utils.formataddr((email_result[NETPROTECT_EMAIL_FROM_NAME],
                                                              email_result[NETPROTECT_EMAIL_FROM_ADDRESS]))
                        msg['Subject'] = 'Trip Event Notification'
                        info_string = 'Sending email to %s for event on %s' % (name, input_name)
                        self.logger.info(info_string)
                        server = smtplib.SMTP(GATEWAY_GMAIL_SMTP_SERVER, GATEWAY_GMAIL_SMTP_SERVER_PORT)
                        server.set_debuglevel(False) # show communication with the server
                        server.ehlo()
                        server.starttls()
                        try:
                            server.login(GATEWAY_GMAIL_ADDRESS, GATEWAY_GMAIL_PASSWORD)
                        except Exception as err:
                            error_string = 'Unable to login to SMTP server - %s' %  repr(err)
                            self.logger.error(error_string)
                            server.quit()
                            self.smtp_error_cnt += 1
                            if self.smtp_error_cnt > NOTIFY_DAEMON_MAX_SMTP_ERRORS:
                                self.logger.error('Maximum SMTP errors exceeded.')
                                return False
                            continue
                        try:
                            server.sendmail(GATEWAY_GMAIL_ADDRESS, email.utils.formataddr((name, address)), msg.as_string())
                        except SMTPException:
                            err_string = 'Unable to send email notification to %s' % name
                            self.logger.error(err_string)
                            self.smtp_error_cnt += 1
                            if self.smtp_error_cnt > NOTIFY_DAEMON_MAX_SMTP_ERRORS:
                                self.logger.error('Maximum SMTP errors exceeded.')
                                return False
                        finally:
                            server.quit()

                        # Update the count and next send time fields in the database.
                        send_count += 1
                        next_send_seconds = int(email_result[NETPROTECT_EMAIL_RESEND_INTERVAL])
                        update_string = 'update %s set %s = %s, %s = %s where (%s = %s and %s = %s)' % \
                                        (DB_NETPROTECT_EMAIL_TABLE,
                                         NETPROTECT_EMAIL_SEND_COUNT, '%s',
                                         NETPROTECT_EMAIL_NEXT_SEND_TIME, '%s',
                                         NETPROTECT_EMAIL_RECIPIENT_NAME, '%s',
                                         NETPROTECT_EMAIL_INPUT_NAME, '%s')
                        next_send_time = datetime.now() + timedelta(seconds=next_send_seconds)
                        args = [send_count, next_send_time, name, input_name]
                        try:
                            prepared_act_on_database(EXECUTE, update_string, args)
                        except Exception as error:
                            error_string = 'Unable to update %s after sending email - %s' % (DB_NETPROTECT_EMAIL_TABLE, error)
                            self.logger.error(error_string)
                            self.db_error_cnt += 1
                            if self.db_error_cnt > NOTIFY_DAEMON_MAX_DB_ERRORS:
                                self.logger.error('Maximum database error count exceeded.')
                                return False  # pull the plug...
                    else:
                        pass
            return True
