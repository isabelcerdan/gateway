#!/usr/share/apparent/.py2-virtualenv/bin/python
import sys
import socket
import struct
import time
from time import mktime, strptime
import datetime
import os
from lib.gateway_constants.DBConstants import *
from lib.gateway_constants.InverterConstants import *
from lib.gateway_constants.GatewayConstants import *
from lib.gateway_constants.ArtesynConstants import *
from lib.logging_setup import *
import gateway_utils as gu
import tlv_utils
from power_status.psa_multicast import *
from power_status.psa_unicast import *
from power_status.psa_util import *
from inverterMessages.gptsInverterQuery import *
from inverterMessages.gptsCounterQuery import *

from multiprocessing import active_children
from daemon.daemon_monitor import DaemonMonitor
from daemon.daemon import Daemon
from Artesyn.ArtesynControllerDaemon import ArtesynControllerDaemon
from ess.ess_unit import *


from lib.db import prepared_act_on_database, FETCH_ONE, FETCH_ALL, EXECUTE

from collections import namedtuple

from inverterMessages.tlvPsaPower import *
from inverterMessages.tlvPsaStatusCurrent import *
from inverterMessages.tlvPsaStatusLatched import *


DEVIL_STARTING_PERCENTAGE = 666

QUERY_LIST_FROM_DATABASE = 1
QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST = 2
QUERY_LIST_CAME_FROM_OUTTER_SPACE = 3

DELETE_DEFERRED_LIST_ACTION = 1
APPEND_DEFERRED_LIST_ACTION = 2
UNKNON_DEFERRED_LIST_ACTION = 3

INVERTER_CONTROL_BIT_GATEWAY = "GATEWAY"
INVERTER_CONTROL_BIT_LOW_POWER = "LOW_POWER"
INVERTER_CONTROL_BIT_SLEEP = "SLEEP"
INVERTER_CONTROL_BIT_ALL = "ALL"

XET_INFO_REQUEST_STRING_NODESTATUS = "NODESTATUS"
XET_INFO_REQUEST_STRING_XETINFO = "XETINFO"
XET_INFO_REQUEST_STRING_NAMESERVICE = "NAMESERVICE"
XET_INFO_REQUEST_STRING_ALL = ""

# TODO: eventually, this sort of data (in this case, parameters for various tools) will be managed in a dictionary.

# This set of command parameters used in the SET AIF IMMED command (PF/PF_C_LEAD/PF_ENABLE managed via UNIVERSAL AIF):
NUMBER_OF_MANAGED_AIF_IMMED_PARAMETERS = 8
CMD_PARAM_AIF_IMMED_CONNECT = "CONNECT"
CMD_PARAM_AIF_IMMED_PWR_LIMIT_PCT = "PWR_LIMIT_PCT"
CMD_PARAM_AIF_IMMED_PWR_LIMIT_ENABLE = "PWR_LIMIT_ENABLE"
CMD_PARAM_AIF_IMMED_POWER_FACTOR = "POWER_FACTOR"
CMD_PARAM_AIF_IMMED_POWER_FACTOR_C_LEAD = "POWER_FACTOR_C_LEAD"
CMD_PARAM_AIF_IMMED_POWER_FACTOR_ENABLE = "POWER_FACTOR_ENABLE"
CMD_PARAM_AIF_IMMED_VAR_LIMIT_PCT = "VAR_LIMIT_PCT"
CMD_PARAM_AIF_IMMED_VAR_LIMIT_ENABLE = "VAR_LIMIT_ENABLE"

# This set of command parameters used in the SET AIF SETTINGS command:
NUMBER_OF_MANAGED_AIF_SETTINGS_PARAMETERS = 15
CMD_PARAM_AIF_SETTINGS_AC_V_OFFSET = "AC_V_OFFSET"
CMD_PARAM_AIF_SETTINGS_MAX_OUT_PWR = "MAX_OUT_PWR"
CMD_PARAM_AIF_SETTINGS_RAMP_RATE_UP = "RAMP_RATE_UP"
CMD_PARAM_AIF_SETTINGS_RAMP_RATE_DOWN = "RAMP_RATE_DOWN"
CMD_PARAM_AIF_SETTINGS_RECONN_RR_PCT = "RECONN_RR_PCT"
CMD_PARAM_AIF_SETTINGS_RTS_DEL_MSEC = "RTS_DEL_MSEC"
CMD_PARAM_AIF_SETTINGS_MAX_OUT_VA = "MAX_OUT_VA"
CMD_PARAM_AIF_SETTINGS_MAX_OUT_VAR_Q1Q4 = "MAX_OUT_VAR_Q1Q4"
CMD_PARAM_AIF_SETTINGS_MIN_PF_Q1 = "MIN_PF_Q1"
CMD_PARAM_AIF_SETTINGS_MIN_PF_Q4 = "MIN_PF_Q4"
CMD_PARAM_AIF_SETTINGS_OUT_VAR_RR_PCT = "OUT_VAR_RR_PCT"
CMD_PARAM_AIF_SETTINGS_V_WAVER = "V_WAVER"
CMD_PARAM_AIF_SETTINGS_V_WAVER_ENA = "V_WAVER_ENA"
CMD_PARAM_AIF_SETTINGS_W_WAVER = "W_WAVER"
CMD_PARAM_AIF_SETTINGS_REAL_REACT_PRIO = "REAL_REACT_PRIO"

PCS_UNIT_ID_GOOD_TLV_TEST_1_TITLE = "ONE ENCAPSULATED PCS UNIT ID GROUP"
PCS_UNIT_ID_GOOD_TLV_TEST_2_TITLE = "TWO ENCAPSULATED PCS UNIT ID GROUPS"

PCS_UNIT_ID_BAD_TLV_TEST_1_TITLE = "PCS UNIT ID START/END DIFFERENT"
PCS_UNIT_ID_BAD_TLV_TEST_2_TITLE = "COMMAND_SET_END MISSING"
PCS_UNIT_ID_BAD_TLV_TEST_3_TITLE = "COMMAND_SET_START MISSING"
PCS_UNIT_ID_BAD_TLV_TEST_4_TITLE = "packet has 2 COMMAND_SET_START and only 1 COMMAND_SET_END TLV"
PCS_UNIT_ID_BAD_TLV_TEST_5_TITLE = "Encapsulated START/END with bogus LONG length"

# This set of command parameters used in the SET EERPT command:
NUMBER_OF_MANAGED_EERPT_PARAMETERS = 6
CMD_PARAM_SET_EERPT_SERVER_ENABLE = "ENA"
CMD_PARAM_SET_EERPT_SERVER_RATE = "RATE"
CMD_PARAM_SET_EERPT_SERVER_URL = "URL"
CMD_PARAM_SET_EERPT_SERVER_PORT = "PORT"
CMD_PARAM_SET_EERPT_SERVER_USER_NAME = "USER"
CMD_PARAM_SET_EERPT_SERVER_PASSWORD = "PASS"

#
# General purpose TLV-sending service module.
#
# Used primarilly by the GUI to effect changes in the inverters:
#
# -> when the GUI user effects an inverter data change, this module
#    invoked with suitable parameters which results in the construction
#    of a TLV-encode packet sent to the inverter in question.

#logger = setup_logger('gpts')


# -----------------------------------------------------------------------------
#
# The "list" of GPTS user input command facilities. These indices are used to
# access a list of named tuples, each tuple containing:
#
# - a handler: the name of the function that implements a specific the command, and
# - a helper: the name of the function that prints help for the corresponding command
INDEXOF_HELP_COMMAND = 0
INDEXOF_H_COMMAND = 1

# These 4 are likely to be removed in a future near you:
INDEXOF_SEND_KEEP_ALIVE_TEST_COMMAND = 2
INDEXOF_FULL_UPGRADE_LOOP_TEST_COMMAND = 3

# The rest are useful. And used.

INDEXOF_PLAIN_RESET_BY_INV_TYPE_COMMAND = 4
INDEXOF_UPGRADE_RESET_BY_INV_TYPE_COMMAND = 5
INDEXOF_PLAIN_RESET_BY_IP_COMMAND = 6
INDEXOF_UPGRADE_RESET_BY_IP_COMMAND = 7
INDEXOF_PLAIN_RESET_BY_MAC_COMMAND = 8
INDEXOF_UPGRADE_RESET_BY_MAC_COMMAND = 9
INDEXOF_PLAIN_RESET_BY_SN_COMMAND = 10
INDEXOF_UPGRADE_RESET_BY_SN_COMMAND = 11
INDEXOF_UPGRADE_RESET_BY_HTTP_COMMAND = 12
INDEXOF_SEND_CLI_COMMAND = 13
INDEXOF_XET_INFO_BY_NBNS_COMMAND= 14
INDEXOF_SET_KEEP_ALIVE_COMMAND = 15
INDEXOF_QUERY_COMMAND = 16
INDEXOF_INVERTERS_COMM_COMMAND = 17
INDEXOF_DC_OUTPUT_STRING_COMMAND = 18
INDEXOF_DC_GWA_OUTPUT_STRING_COMMAND = 19
INDEXOF_SET_CLEAR_INV_CTRL_COMMAND = 20
INDEXOF_MOM_COMMAND = 21
INDEXOF_GET_AIF_SETTINGS_COMMAND = 22
INDEXOF_GET_AIF_IMMED_COMMAND = 23
INDEXOF_SET_AIF_SETTINGS_COMMAND= 24
INDEXOF_SET_AIF_IMMED_COMMAND = 25
INDEXOF_DYNAMIC_PF_COMMAND = 26
INDEXOF_ITCA_COMMAND = 27
INDEXOF_GIDC_COMMAND = 28
INDEXOF_DICA_COMMAND = 29
INDEXOF_QIP_COMMAND = 30
INDEXOF_IIDC_COMMAND = 31
INDEXOF_PDIU_COMMAND = 32
INDEXOF_LDIU_COMMAND = 33
INDEXOF_START_FEED_DETECT_COMMAND = 34
INDEXOF_UNIVERSAL_AIF_COMMAND = 35
INDEXOF_GATEWAY_OPERATION_MODE_COMMAND = 36
INDEXOF_PCC_FEED_MODE_COMMAND = 37
INDEXOF_ASSIGN_PCS_UNIT_ID_COMMAND = 38
INDEXOF_PCS_UNIT_SET_POWER_COMMAND = 39
INDEXOF_GET_CATCH_UP_REPORTS_COMMAND = 40
INDEXOF_FORCED_INVERTER_UPGRADE_COMMAND = 41
INDEXOF_GET_EERPT_INFO_COMMAND = 42
INDEXOF_SET_EERPT_INFO_COMMAND = 43
INDEXOF_PCS_UNIT_ID_GOOD_TLV_TEST_COMMAND = 44
INDEXOF_PCS_UNIT_ID_BAD_TLV_TEST_COMMAND = 45
INDEXOF_PSA_COMMAND = 46
INDEXOF_START_PUI_ASSOCIATION_COMMAND = 47
INDEXOF_SEND_CURTAIL_COMMAND = 48
INDEXOF_START_CHARGER_ASSOCIATION_COMMAND = 49
INDEXOF_IQM_COMMAND = 50
INDEXOF_COUNTERS_COMMAND = 51
INDEXOF_SHOW_BMU_SOC_PAIRS = 52
INDEXOF_GROUP_SIMPLE_TLV_TEST_COMMAND = 53
INDEXOF_GROUP_COMPOUND_TLV_TEST_COMMAND = 54
INDEXOF_GROUP_INVALID_TLV_TEST_COMMAND = 55
INDEXOF_QUERY_BY_GROUP_COMMAND = 56
INDEXOF_PLAIN_RESET_BY_GROUP_COMMAND = 57
INDEXOF_UPGRADE_RESET_BY_GROUP_COMMAND = 58
INDEXOF_ARTESYN_CLI_SESSION_COMMAND = 59

INDEXOF_LAST_AND_INVALID_COMMAND = 60 # <- always last.

# -----------------------------------------------
GPTS_COMMAND_HELP = "HELP"
GPTS_COMMAND_H = "H"

# These 4 are likely to be removed in a future near you:
GPTS_COMMAND_SEND_KEEP_ALIVE_TEST = "SKA_TEST"
GPTS_COMMAND_FULL_UPGRADE_LOOP_TEST = "FUT"

# The rest are useful.

GPTS_COMMAND_PLAIN_RESET_BY_INV_TYPE = "RESET_BY_INV_TYPE"
GPTS_COMMAND_UPGRADE_RESET_BY_INV_TYPE = "URESET_BY_INV_TYPE"
GPTS_COMMAND_PLAIN_RESET_BY_IP = "RESET_BY_IP"
GPTS_COMMAND_UPGRADE_RESET_BY_IP = "URESET_BY_IP"
GPTS_COMMAND_PLAIN_RESET_BY_MAC = "RESET_BY_MAC"
GPTS_COMMAND_UPGRADE_RESET_BY_MAC = "URESET_BY_MAC"
GPTS_COMMAND_PLAIN_RESET_BY_SN = "RESET_BY_SN"
GPTS_COMMAND_UPGRADE_RESET_BY_SN = "URESET_BY_SN"
GPTS_COMMAND_UPGRADE_RESET_BY_HTTP = "URESET_BY_HTTP"
GPTS_COMMAND_SEND_CLI = "CLI"
GPTS_COMMAND_XET_INFO_BY_NBNS = "NBNS"
GPTS_COMMAND_SET_KEEP_ALIVE = "SET_KA"
GPTS_COMMAND_QUERY = "QUERY"
GPTS_COMMAND_INVERTERS_COMM = "COMM"
GPTS_COMMAND_DC = "DC"
GPTS_COMMAND_DC_GWA = "DC_GWA"
GPTS_COMMAND_SET_CLEAR_INV_CTRL = "INV_CTRL"
GPTS_COMMAND_MOM = "MOM"
GPTS_COMMAND_GET_AIF_SETTINGS = "GET_AIF_SETTINGS"
GPTS_COMMAND_GET_AIF_IMMED = "GET_AIF_IMMED"
GPTS_COMMAND_SET_AIF_SETTINGS = "SET_AIF_SETTINGS"
GPTS_COMMAND_SET_AIF_IMMED = "SET_AIF_IMMED"
GPTS_COMMAND_DYNAMIC_PF = "DYNAMIC_PF"
GPTS_COMMAND_ITCA = "ITCA"
GPTS_COMMAND_GIDC = "GIDC"
GPTS_COMMAND_DICA = "DICA"
GPTS_COMMAND_QIP = "QIP"
GPTS_COMMAND_IIDC = "IIDC"
GPTS_COMMAND_PDIU = "PDIU"
GPTS_COMMAND_LDIU = "LDIU"
GPTS_COMMAND_START_FEED_DETECT = "SFD"
GPTS_COMMAND_UNIVERSAL_AIF = "UNIVERSAL_AIF"
GPTS_COMMAND_GATEWAY_OPERATION_MODE = "GOM"
GPTS_COMMAND_PCC_FEED = "FEED"
GPTS_COMMAND_ASSIGN_PCS_UNIT_ID = "PCS_UNIT_ID"
GPTS_COMMAND_PCS_UNIT_SET_POWER = "PCS_UNIT_SET_POWER"
GPTS_COMMAND_GET_CATCH_UP_REPORTS = "GET_CU_REPORTS"
GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST = "PUI_GOOD"
GPTS_COMMAND_PCS_UNIT_ID_BAD_TLV_TEST = "PUI_BAD"
GPTS_COMMAND_FORCED_INVERTER_UPGRADE = "FIU"
GPTS_COMMAND_GET_EERPT_INFO = "GET_EERPT"
GPTS_COMMAND_SET_EERPT_INFO = "SET_EERPT"
GPTS_COMMAND_PSA = "PSA"
GPTS_COMMAND_IQM = "IQM"
GPTS_COMMAND_COUNTERS = "COUNTERS"
GPTS_COMMAND_START_PUI_ASSOC = "INVERTER_ASSOC"
GPTS_COMMAND_SEND_CURTAIL = "SEND_CURTAIL"
GPTS_COMMAND_START_CHARGER_PCS_ASSOC = "CHARGER_ASSOC"
GPTS_COMMAND_SHOW_BMU_SOC_PAIRS = "SHOW_BMU_SOC_PAIRS"
GPTS_COMMAND_GROUP_SIMPLE_TLV_TEST = "GROUP_SIMPLE"
GPTS_COMMAND_GROUP_COMPOUND_TLV_TEST = "GROUP_COMPOUND"
GPTS_COMMAND_GROUP_INVALID_TLV_TEST = "GROUP_INVALID"
GPTS_COMMAND_QUERY_BY_GROUP = "QUERY_BY_GROUP"
GPTS_COMMAND_PLAIN_RESET_BY_GROUP = "PLAIN_RESET_BY_GROUP"
GPTS_COMMAND_UPGRADE_RESET_BY_GROUP = "UPGRADE_RESET_BY_GROUP"
GPTS_COMMAND_ARTESYN_CLI_SESSION = "ARTESYN_CLI"

# -----------------------------------------------
def lookup_help(self):
    self.print_full_help_menu()

# These 4 are likely to be removed in a future near you:
def lookup_send_keep_alive_test(self):
    self.send_keep_alive_test()
def lookup_full_upgrade_test(self):
    self.full_upgrade_test()

# The rest are useful.

def lookup_plain_reset_by_inv_type(self):
    self.send_tlv_reset_by_inv_type(GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE)
def lookup_upgrade_reset_by_inv_type(self):
    self.send_tlv_reset_by_inv_type(GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE)
def lookup_plain_reset_by_ip(self):
    self.send_tlv_reset_by_ip(GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE)
def lookup_upgrade_reset_by_ip(self):
    self.send_tlv_reset_by_ip(GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE)
def lookup_plain_reset_by_mac(self):
    self.send_tlv_reset_by_mac_or_sn(GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE)
def lookup_upgrade_reset_by_mac(self):
    self.send_tlv_reset_by_mac_or_sn(GTW_TO_SG424_UPGRADE_RESET_BY_MAC_STRING_TLV_TYPE)
def lookup_plain_reset_by_sn(self):
    self.send_tlv_reset_by_mac_or_sn(GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE)
def lookup_upgrade_reset_by_sn(self):
    self.send_tlv_reset_by_mac_or_sn(GTW_TO_SG424_UPGRADE_RESET_BY_SN_STRING_TLV_TYPE)
def lookup_upgrade_reset_by_http(self):
    self.upgrade_reset_by_http()
def lookup_send_cli(self):
    self.validate_and_send_cli_command()
def lookup_xet_info_by_nbns(self):
    self.validate_and_get_nbns_info()
def lookup_set_keep_alive_timeout(self):
    self.set_keep_alive_timeout()
def lookup_query(self):
    self.query_inverters()
def lookup_get_inverters_comm(self):
    self.get_inverters_comm()
def lookup_dc(self):
    self.get_dc_output()
def lookup_dc_gwa(self):
    self.get_dc_gwa_output()
def lookup_set_clear_inv_ctrl(self):
    self.inverter_control_bit_update()
def lookup_mom(self):
    self.get_mom_data()
def lookup_get_aif_settings(self):
    self.get_aif_settings_data()
def lookup_get_aif_immed(self):
    self.get_aif_immed_data()
def lookup_set_aif_settings(self):
    self.set_aif_settings()
def lookup_set_aif_immed(self):
    self.set_aif_immed()
def lookup_dynamic_pf(self):
    self.send_dynamic_power_factor_command()
def lookup_itca(self):
    self.inverters_table_consistency_audit()
def lookup_gidc(self):
    self.get_inverters_debug_counters()
def lookup_dica(self):
    self.db_to_inverters_consistency_audit()
def lookup_qip(self):
    self.quick_inverters_ping()
def lookup_iidc(self):
    self.init_inverter_debug_counters()
def lookup_pdiu(self):
    self.prepare_deferred_inverter_upgrade()
def lookup_ldiu(self):
    self.launch_deferred_inverter_upgrade()
def lookup_start_feed_detect(self):
    self.start_feed_detect()
def lookup_universal_aif(self):
    self.set_universal_aif()
def lookup_gateway_operation_mode(self):
    self.gateway_operation_mode()
def lookup_pcc_feed(self):
    self.send_pcc_feed_setting()
def lookup_assign_pcs_unit_id(self):
    self.assign_pcs_unit_id()
def lookup_pcs_unit_set_power(self):
    self.pcs_unit_set_power()
def lookup_get_catch_up_reports(self):
    self.get_catch_up_reports()
def lookup_pcs_unit_id_good_tlv_test(self):
    self.pcs_unit_id_good_tlv_test()
def lookup_pcs_unit_id_bad_tlv_test(self):
    self.pcs_unit_id_bad_tlv_test()
def lookup_forced_inverter_upgrade(self):
    self.forced_inverter_upgrade()
def lookup_get_eerpt_info(self):
    self.get_eerpt_info()
def lookup_set_eerpt_info(self):
    self.set_eerpt_info()
def lookup_psa(self):
    self.inverters_power_status_audit()
def lookup_iqm(self):
    self.send_query_iqm()
def lookup_packed_counters(self):
    self.send_query_packed_counters()
def lookup_start_pui_association(self):
    self.start_pui_association()
def lookup_send_curtail(self):
    self.send_curtail()
def lookup_start_charger_association(self):
    self.start_charger_association()
def lookup_show_bmu_soc_pairs(self):
    self.show_bmu_soc_pairs()
def lookup_group_simple_tlv_command_test(self):
    self.group_simple_tlv_command_test()
def lookup_group_compound_tlv_command_test(self):
    self.group_compound_tlv_command_test()
def lookup_group_invalid_tlv_command_test(self):
    self.group_invalid_tlv_command_test()
def lookup_query_by_group(self):
    self.query_by_group()
def lookup_plain_reset_by_group(self):
    self.plain_reset_by_group()
def lookup_upgrade_reset_by_group(self):
    self.upgrade_reset_by_group()
def lookup_artesyn_cli_session(self):
    self.artesyn_cli_session()
# -----------------------------------------------
def lookup_help_help(self):
    self.print_full_help_menu()

# These 4 are likely to be removed in a future near you:
def lookup_help_send_keep_alive_test(self):
    self.print_send_keep_alive_test_help()
def lookup_help_full_upgrade_test(self):
    self.print_full_upgrade_loop_test_help()

# The rest are useful. These provide help on an individual GPTS command basis.

def lookup_help_plain_reset_by_inv_type(self):
    self.print_plain_reset_by_inv_type_help()
def lookup_help_upgrade_reset_by_inv_type(self):
    self.print_upgrade_reset_by_inv_type_help()
def lookup_help_plain_reset_by_ip(self):
    self.print_plain_reset_by_ip_help()
def lookup_help_upgrade_reset_by_ip(self):
    self.print_upgrade_reset_by_ip_help()
def lookup_help_plain_reset_by_mac(self):
    self.print_plain_reset_by_mac_help()
def lookup_help_upgrade_reset_by_mac(self):
    self.print_upgrade_reset_by_mac_help()
def lookup_help_plain_reset_by_sn(self):
    self.print_plain_reset_by_sn_help()
def lookup_help_upgrade_reset_by_sn(self):
    self.print_upgrade_reset_by_sn_help()
def lookup_help_upgrade_reset_by_http(self):
    self.print_upgrade_reset_by_http_help()
def lookup_help_send_cli(self):
    self.print_send_cli_help()
def lookup_help_xet_info_by_nbns(self):
    self.print_xet_info_by_nbns_help()
def lookup_help_set_keep_alive(self):
    self.print_set_keep_alive_help()
def lookup_help_query(self):
    self.print_query_help()
def lookup_help_inverters_comm(self):
    self.print_inverters_comm_help()
def lookup_help_dc(self):
    self.print_dc_output_string_help()
def lookup_help_dc_gwa(self):
    self.print_dc_gwa_output_string_help()
def lookup_help_set_clear_inv_ctrl(self):
    self.print_set_clear_inv_ctrl_help()
def lookup_help_mom(self):
    self.print_mom_help()
def lookup_help_get_aif_settings(self):
    self.print_get_aif_settings_help()
def lookup_help_get_aif_immed(self):
    self.print_get_aif_immed_help()
def lookup_help_set_aif_settings(self):
    self.print_set_aif_settings_help()
def lookup_help_set_aif_immed(self):
    self.print_set_aif_immed_help()
def lookup_help_dynamic_pf(self):
    self.print_dymanic_pf_help()
def lookup_help_itca(self):
    self.print_itca_help()
def lookup_help_gidc(self):
    self.print_gidc_help()
def lookup_help_dica(self):
    self.print_dica_help()
def lookup_help_qip(self):
    self.print_qip_help()
def lookup_help_iidc(self):
    self.print_iidc_help()
def lookup_help_pdiu(self):
    self.print_pdiu_help()
def lookup_help_ldiu(self):
    self.print_ldiu_help()
def lookup_help_start_feed_detect(self):
    self.print_start_feed_detect_help()
def lookup_help_universal_aif(self):
    self.print_universal_aif_help()
def lookup_help_gateway_operation_mode(self):
    self.print_gateway_operation_mode_help()
def lookup_help_pcc_feed(self):
    self.print_pcc_feed_help()
def lookup_help_assign_pcs_unit_id(self):
    self.print_assign_pcs_unit_id_help()
def lookup_help_pcs_unit_set_power(self):
    self.print_pcs_unit_set_power_help()
def lookup_help_get_catch_up_reports(self):
    self.print_get_catch_up_reports_help()
def lookup_help_pcs_unit_id_good_tlv_test(self):
    self.print_pcs_unit_id_good_tlv_test_help()
def lookup_help_pcs_unit_id_bad_tlv_test(self):
    self.print_pcs_unit_id_bad_tlv_test_help()
def lookup_help_forced_inverter_upgrade(self):
    self.print_forced_inverter_upgrade_help()
def lookup_help_get_eerpt_info(self):
    self.print_get_eerpt_info_help()
def lookup_help_set_eerpt_info(self):
    self.print_set_eerpt_info_help()
def lookup_help_psa(self):
    self.print_psa_help()
def lookup_help_iqm(self):
    self.print_iqm_help()
def lookup_help_packed_counters(self):
    self.print_packed_counters_help()
def lookup_help_start_pui_association(self):
    self.print_start_pui_association_help()
def lookup_help_send_curtail(self):
    self.print_send_curtail_help()
def lookup_help_start_charger_association(self):
    self.print_start_charger_association_help()
def lookup_help_show_bmu_soc_pairs(self):
    self.print_show_bmu_soc_pairs_help()
def lookup_help_group_simple_tlv_command_test(self):
    self.print_group_simple_tlv_command_test_help()
def lookup_help_group_compound_tlv_command_test(self):
    self.print_group_compound_tlv_command_test_help()
def lookup_help_group_invalid_tlv_command_test(self):
    self.print_group_invalid_tlv_command_test_help()
def lookup_help_query_by_group(self):
    self.print_query_by_group_help()
def lookup_help_plain_reset_by_group(self):
    self.print_plain_reset_by_group_help()
def lookup_help_upgrade_reset_by_group(self):
    self.print_upgrade_reset_by_group_help()
def lookup_help_artesyn_cli_session(self):
    self.print_artesyn_cli_session_help()

# -----------------------------------------------

GPTSCommandInfo = namedtuple("GPTSInfo", "index handler helper")

gpts_command_lookup_table = {
    GPTS_COMMAND_HELP:                     GPTSCommandInfo(INDEXOF_HELP_COMMAND,                     lookup_help,                     lookup_help_help),
    GPTS_COMMAND_H:                        GPTSCommandInfo(INDEXOF_H_COMMAND,                        lookup_help,                     lookup_help_help),

    GPTS_COMMAND_SEND_KEEP_ALIVE_TEST:     GPTSCommandInfo(INDEXOF_SEND_KEEP_ALIVE_TEST_COMMAND,     lookup_send_keep_alive_test,     lookup_help_send_keep_alive_test),
    GPTS_COMMAND_FULL_UPGRADE_LOOP_TEST:   GPTSCommandInfo(INDEXOF_FULL_UPGRADE_LOOP_TEST_COMMAND,   lookup_full_upgrade_test,        lookup_help_full_upgrade_test),

    GPTS_COMMAND_PLAIN_RESET_BY_INV_TYPE:  GPTSCommandInfo(INDEXOF_PLAIN_RESET_BY_INV_TYPE_COMMAND,  lookup_plain_reset_by_inv_type,  lookup_help_plain_reset_by_inv_type),
    GPTS_COMMAND_UPGRADE_RESET_BY_INV_TYPE:GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_INV_TYPE_COMMAND,lookup_upgrade_reset_by_inv_type,lookup_help_upgrade_reset_by_inv_type),
    GPTS_COMMAND_PLAIN_RESET_BY_IP:        GPTSCommandInfo(INDEXOF_PLAIN_RESET_BY_IP_COMMAND,        lookup_plain_reset_by_ip,        lookup_help_plain_reset_by_ip),
    GPTS_COMMAND_UPGRADE_RESET_BY_IP:      GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_IP_COMMAND,      lookup_upgrade_reset_by_ip,      lookup_help_upgrade_reset_by_ip),
    GPTS_COMMAND_PLAIN_RESET_BY_MAC:       GPTSCommandInfo(INDEXOF_PLAIN_RESET_BY_MAC_COMMAND,       lookup_plain_reset_by_mac,       lookup_help_plain_reset_by_mac),
    GPTS_COMMAND_UPGRADE_RESET_BY_MAC:     GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_MAC_COMMAND,     lookup_upgrade_reset_by_mac,     lookup_help_upgrade_reset_by_mac),
    GPTS_COMMAND_PLAIN_RESET_BY_SN:        GPTSCommandInfo(INDEXOF_PLAIN_RESET_BY_SN_COMMAND,        lookup_plain_reset_by_sn,        lookup_help_plain_reset_by_sn),
    GPTS_COMMAND_UPGRADE_RESET_BY_SN:      GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_SN_COMMAND,      lookup_upgrade_reset_by_sn,      lookup_help_upgrade_reset_by_sn),
    GPTS_COMMAND_UPGRADE_RESET_BY_HTTP:    GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_HTTP_COMMAND,    lookup_upgrade_reset_by_http,    lookup_help_upgrade_reset_by_http),
    GPTS_COMMAND_SEND_CLI:                 GPTSCommandInfo(INDEXOF_SEND_CLI_COMMAND,                 lookup_send_cli,                 lookup_help_send_cli),
    GPTS_COMMAND_XET_INFO_BY_NBNS:         GPTSCommandInfo(INDEXOF_XET_INFO_BY_NBNS_COMMAND,         lookup_xet_info_by_nbns,         lookup_help_xet_info_by_nbns),
    GPTS_COMMAND_SET_KEEP_ALIVE:           GPTSCommandInfo(INDEXOF_SET_KEEP_ALIVE_COMMAND,           lookup_set_keep_alive_timeout,   lookup_help_set_keep_alive),
    GPTS_COMMAND_QUERY:                    GPTSCommandInfo(INDEXOF_QUERY_COMMAND,                    lookup_query,                    lookup_help_query),
    GPTS_COMMAND_INVERTERS_COMM:           GPTSCommandInfo(INDEXOF_INVERTERS_COMM_COMMAND,           lookup_get_inverters_comm,       lookup_help_inverters_comm),
    GPTS_COMMAND_DC:                       GPTSCommandInfo(INDEXOF_DC_OUTPUT_STRING_COMMAND,         lookup_dc,                       lookup_help_dc),
    GPTS_COMMAND_DC_GWA:                   GPTSCommandInfo(INDEXOF_DC_GWA_OUTPUT_STRING_COMMAND,     lookup_dc_gwa,                   lookup_help_dc_gwa),
    GPTS_COMMAND_SET_CLEAR_INV_CTRL:       GPTSCommandInfo(INDEXOF_SET_CLEAR_INV_CTRL_COMMAND,       lookup_set_clear_inv_ctrl,       lookup_help_set_clear_inv_ctrl),
    GPTS_COMMAND_MOM:                      GPTSCommandInfo(INDEXOF_MOM_COMMAND,                      lookup_mom,                      lookup_help_mom),
    GPTS_COMMAND_GET_AIF_SETTINGS:         GPTSCommandInfo(INDEXOF_GET_AIF_SETTINGS_COMMAND,         lookup_get_aif_settings,         lookup_help_get_aif_settings),
    GPTS_COMMAND_GET_AIF_IMMED:            GPTSCommandInfo(INDEXOF_GET_AIF_IMMED_COMMAND,            lookup_get_aif_immed,            lookup_help_get_aif_immed),
    GPTS_COMMAND_SET_AIF_SETTINGS:         GPTSCommandInfo(INDEXOF_SET_AIF_SETTINGS_COMMAND,         lookup_set_aif_settings,         lookup_help_set_aif_settings),
    GPTS_COMMAND_SET_AIF_IMMED:            GPTSCommandInfo(INDEXOF_SET_AIF_IMMED_COMMAND,            lookup_set_aif_immed,            lookup_help_set_aif_immed),
    GPTS_COMMAND_DYNAMIC_PF:               GPTSCommandInfo(INDEXOF_DYNAMIC_PF_COMMAND,               lookup_dynamic_pf,               lookup_help_dynamic_pf),
    GPTS_COMMAND_ITCA:                     GPTSCommandInfo(INDEXOF_ITCA_COMMAND,                     lookup_itca,                     lookup_help_itca),
    GPTS_COMMAND_GIDC:                     GPTSCommandInfo(INDEXOF_GIDC_COMMAND,                     lookup_gidc,                     lookup_help_gidc),
    GPTS_COMMAND_DICA:                     GPTSCommandInfo(INDEXOF_DICA_COMMAND,                     lookup_dica,                     lookup_help_dica),
    GPTS_COMMAND_QIP:                      GPTSCommandInfo(INDEXOF_QIP_COMMAND,                      lookup_qip,                      lookup_help_qip),
    GPTS_COMMAND_IIDC:                     GPTSCommandInfo(INDEXOF_IIDC_COMMAND,                     lookup_iidc,                     lookup_help_iidc),
    GPTS_COMMAND_PDIU:                     GPTSCommandInfo(INDEXOF_PDIU_COMMAND,                     lookup_pdiu,                     lookup_help_pdiu),
    GPTS_COMMAND_LDIU:                     GPTSCommandInfo(INDEXOF_LDIU_COMMAND,                     lookup_ldiu,                     lookup_help_ldiu),
    GPTS_COMMAND_START_FEED_DETECT:        GPTSCommandInfo(INDEXOF_START_FEED_DETECT_COMMAND,        lookup_start_feed_detect,        lookup_help_start_feed_detect),
    GPTS_COMMAND_UNIVERSAL_AIF:            GPTSCommandInfo(INDEXOF_UNIVERSAL_AIF_COMMAND,            lookup_universal_aif,            lookup_help_universal_aif),
    GPTS_COMMAND_GATEWAY_OPERATION_MODE:   GPTSCommandInfo(INDEXOF_GATEWAY_OPERATION_MODE_COMMAND,   lookup_gateway_operation_mode,   lookup_help_gateway_operation_mode),
    GPTS_COMMAND_PCC_FEED:                 GPTSCommandInfo(INDEXOF_PCC_FEED_MODE_COMMAND,            lookup_pcc_feed,                 lookup_help_pcc_feed),
    GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST:GPTSCommandInfo(INDEXOF_PCS_UNIT_ID_GOOD_TLV_TEST_COMMAND,lookup_pcs_unit_id_good_tlv_test,lookup_help_pcs_unit_id_good_tlv_test),
    GPTS_COMMAND_PCS_UNIT_ID_BAD_TLV_TEST: GPTSCommandInfo(INDEXOF_PCS_UNIT_ID_BAD_TLV_TEST_COMMAND, lookup_pcs_unit_id_bad_tlv_test, lookup_help_pcs_unit_id_bad_tlv_test),
    GPTS_COMMAND_ASSIGN_PCS_UNIT_ID:       GPTSCommandInfo(INDEXOF_ASSIGN_PCS_UNIT_ID_COMMAND,       lookup_assign_pcs_unit_id,       lookup_help_assign_pcs_unit_id),
    GPTS_COMMAND_PCS_UNIT_SET_POWER:       GPTSCommandInfo(INDEXOF_PCS_UNIT_SET_POWER_COMMAND,       lookup_pcs_unit_set_power,       lookup_help_pcs_unit_set_power),
    GPTS_COMMAND_GET_CATCH_UP_REPORTS:     GPTSCommandInfo(INDEXOF_GET_CATCH_UP_REPORTS_COMMAND,     lookup_get_catch_up_reports,     lookup_help_get_catch_up_reports),
    GPTS_COMMAND_FORCED_INVERTER_UPGRADE:  GPTSCommandInfo(INDEXOF_FORCED_INVERTER_UPGRADE_COMMAND,  lookup_forced_inverter_upgrade,  lookup_help_forced_inverter_upgrade),
    GPTS_COMMAND_GET_EERPT_INFO:           GPTSCommandInfo(INDEXOF_GET_EERPT_INFO_COMMAND,           lookup_get_eerpt_info,           lookup_help_get_eerpt_info),
    GPTS_COMMAND_SET_EERPT_INFO:           GPTSCommandInfo(INDEXOF_SET_EERPT_INFO_COMMAND,           lookup_set_eerpt_info,           lookup_help_set_eerpt_info),
    GPTS_COMMAND_PSA:                      GPTSCommandInfo(INDEXOF_PSA_COMMAND,                      lookup_psa,                      lookup_help_psa),
    GPTS_COMMAND_START_PUI_ASSOC:          GPTSCommandInfo(INDEXOF_START_PUI_ASSOCIATION_COMMAND,    lookup_start_pui_association,    lookup_help_start_pui_association),
    GPTS_COMMAND_SEND_CURTAIL:             GPTSCommandInfo(INDEXOF_SEND_CURTAIL_COMMAND,             lookup_send_curtail,             lookup_help_send_curtail),
    GPTS_COMMAND_START_CHARGER_PCS_ASSOC:  GPTSCommandInfo(INDEXOF_START_CHARGER_ASSOCIATION_COMMAND,lookup_start_charger_association,lookup_help_start_charger_association),
    GPTS_COMMAND_IQM:                      GPTSCommandInfo(INDEXOF_IQM_COMMAND,                      lookup_iqm,                      lookup_help_iqm),
    GPTS_COMMAND_COUNTERS:                 GPTSCommandInfo(INDEXOF_COUNTERS_COMMAND,                 lookup_packed_counters,          lookup_help_packed_counters),
    GPTS_COMMAND_SHOW_BMU_SOC_PAIRS:       GPTSCommandInfo(INDEXOF_SHOW_BMU_SOC_PAIRS,               lookup_show_bmu_soc_pairs,       lookup_help_show_bmu_soc_pairs),

    GPTS_COMMAND_GROUP_SIMPLE_TLV_TEST:    GPTSCommandInfo(INDEXOF_GROUP_SIMPLE_TLV_TEST_COMMAND,    lookup_group_simple_tlv_command_test,   lookup_help_group_simple_tlv_command_test),
    GPTS_COMMAND_GROUP_COMPOUND_TLV_TEST:  GPTSCommandInfo(INDEXOF_GROUP_COMPOUND_TLV_TEST_COMMAND,  lookup_group_compound_tlv_command_test, lookup_help_group_compound_tlv_command_test),
    GPTS_COMMAND_GROUP_INVALID_TLV_TEST:   GPTSCommandInfo(INDEXOF_GROUP_INVALID_TLV_TEST_COMMAND,   lookup_group_invalid_tlv_command_test,  lookup_help_group_invalid_tlv_command_test),

    GPTS_COMMAND_QUERY_BY_GROUP:           GPTSCommandInfo(INDEXOF_QUERY_BY_GROUP_COMMAND,           lookup_query_by_group,           lookup_help_query_by_group),
    GPTS_COMMAND_PLAIN_RESET_BY_GROUP:     GPTSCommandInfo(INDEXOF_PLAIN_RESET_BY_GROUP_COMMAND,     lookup_plain_reset_by_group,     lookup_help_plain_reset_by_group),
    GPTS_COMMAND_UPGRADE_RESET_BY_GROUP:   GPTSCommandInfo(INDEXOF_UPGRADE_RESET_BY_GROUP_COMMAND,   lookup_upgrade_reset_by_group,   lookup_help_upgrade_reset_by_group),
    GPTS_COMMAND_ARTESYN_CLI_SESSION:      GPTSCommandInfo(INDEXOF_ARTESYN_CLI_SESSION_COMMAND,      lookup_artesyn_cli_session,      lookup_help_artesyn_cli_session),
}

# Done with GPTS command-related definitions.
# -----------------------------------------------------------------------------


def execute_general_purpose_tlv_send_script():
    GeneralPurposeTlvSendScript()

class GeneralPurposeTlvSendScript:
    def __init__(self):

        # Sending a TLV-encoded command to an inverter.
        # A variable number of arguments follow.
        self.inverter_ip_address = ""
        self.inverter_string_position = 0
        self.inverter_string_id = DEFAULT_STRING_ID

        self.inverter_feed_name = None
        self.pcs_unit_id = 0
        self.group_id = 0

        self.reset_tlv = 0
        self.num_of_user_args = len(sys.argv) - 1

        self.this_boot_revision = 0
        self.this_boot_file = ""
        self.next_whole_percentage = 0

        self.number_of_inverters_to_scan = 0
        self.number_of_inverters_scanned_so_far = 0
        self.last_percentage_progress_displayed = DEVIL_STARTING_PERCENTAGE

        # Some tools have a progress indicatior:
        self.number_of_inverters_audited_so_far = 0
        self.percentage_audit_progress = 0
        self.last_percentage_audit_progress_written = 0
        self.next_expected_percentage_for_display = 10

        # For charger/BMU association:
        self.chassis_command = None
        self.chassis_id = 0
        self.charger_command = None
        self.charger_id = 0
        self.charger_parameter_string = None
        self.charger_command_power_up = None
        self.charger_command_set_vref_chassis = None
        self.charger_command_set_output_current_level = None
        self.charger_command_power_down = None
        self.bmu_current_readings_comma_separated = None

        # With data initialized, get to work:
        self.setup_logger()

        # ---------------------------------------
        self.run_general_purpose_tlv_send_script()  # <- CALL THE SCRIPT'S MAIN PROCESS!!!
        # ---------------------------------------

    def setup_logger(self):
        self.my_logger = setup_logger("GPTS")

    def log_info(self, message, error=False):
        self.my_logger.info(message, exc_info=(error != False))

    def set_keep_alive_timeout(self):
        if self.num_of_user_args == 3:
            keep_alive_timout = int(sys.argv[2])
            comma_separated_arg = str(sys.argv[3])
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_arg)
            if ip_list:
                for each_ip in ip_list:
                    if gu.is_send_keep_alive_timeout(each_ip, keep_alive_timout):
                        print("IP %s -> SENT KEEP ALIVE TIMEOUT OF %smsec -> OK" % (each_ip, keep_alive_timout))
                    else:
                        print("IP %s -> SENT KEEP ALIVE TIMEOUT OF %smsec -> *FAILED*" % (each_ip, keep_alive_timout))
            else:
                print("IP_LIST FROM ARGUMENT WAS EMPTY")
        else:
            print("INVALID NUMBER OF USER ARGUMENTS - %s IS INCORRRECT" % (self.num_of_user_args))
            self.print_set_keep_alive_help()

    #
    # Parse the input command line looking for 'delete' and 'delay' command line parameters
    #
    # the return value deleteFile will be True if 'delete' is found
    # the return value delayTime will be set to input delay value x where input is 'delay=x'
    #   if error detected in delay parameter, then delayTime will be set to -1.0
    #
    def parse_cmd_line_delete_delay(self, argv):
        deleteFile = False
        delayTime = 0.0
        newArgs = []

        # peel out 'delete' and 'delay' command line args if they exist and
        # create a newArgs list
        for arg in argv:
            if arg.upper() == "DELETE":
                deleteFile = True
            elif "DELAY" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("Bad delay time value, missing")
                    delayTime = -1.0

                try:
                    delayTime = float(sp[1])
                except:
                    print ("Bad delay time value, can not covert to float")
                    delayTime = -1.0
                    pass
            else:
                newArgs.append(arg)

        return deleteFile, delayTime, newArgs

    def psa_parse_cmd_line_delete_delay(self, argv):
        deleteFile = False
        decodeOutput = False
        mcastHos = False
        mcastAll = False
        delayTime = 0.0
        newArgs = []

        # peel out 'delete' and 'delay' command line args if they exist and
        # create a newArgs list
        for arg in argv:
            if arg.upper() == "DECODE":
                decodeOutput = True
            elif arg.upper() == "DELETE":
                deleteFile = True
            elif arg.upper() == "MCASTALL":
                mcastAll = True
            elif arg.upper() == "MCASTHOS":
                mcastHos = True
            elif "DELAY" in arg.upper():
                sp = arg.split("=")
                if len(sp) != 2:
                    print ("Bad delay time value, missing")
                    delayTime = -1.0

                try:
                    delayTime = float(sp[1])
                except:
                    print ("Bad delay time value, can not covert to float")
                    delayTime = -1.0
                    pass
            else:
                newArgs.append(arg)

        return decodeOutput, mcastAll, mcastHos, deleteFile, delayTime, newArgs

    def get_dica_inverter_data_from_ip(self, this_ip):
        select_string = "SELECT %s,%s,%s,%s,%s,%s,%s,%s FROM %s WHERE %s=%s" % (INVERTERS_TABLE_INVERTERS_ID_COLUMN_NAME,
                                                                                INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                                INVERTERS_TABLE_VERSION_COLUMN_NAME,
                                                                                INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                                                INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                                                INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                                                                INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME,
                                                                                INVERTERS_TABLE_GROUP_ID_COLUMN_NAME,
                                                                                DB_INVERTER_NXO_TABLE,
                                                                                INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, '%s')
        select_args = (this_ip, )
        inverter_row_data = prepared_act_on_database(FETCH_ONE, select_string, select_args)
        return inverter_row_data

    def is_valid_and_fixed_pcs_unit_ip(self, inverter_db_row_data):
        pcs_unit_id_fixed = False
        # check the pcs unit ID: it cannot be NULL
        pcs_unit_id = inverter_db_row_data[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME]
        if pcs_unit_id is None:
            inverter_db_row_data[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME] = "0"
            some_ip = inverter_db_row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
            gu.update_db_pcs_unit_id_by_ip(some_ip, 0)
            pcs_unit_id_fixed = True
        return pcs_unit_id_fixed

    def upgrade_reset_by_http(self):
        # Note: this also works, as a terminal command ...
        #       terminal_line_command = "sudo wget http://" + inverter_ip_address + ":80/upgrade.htm?upgradereset"
        #       print("LEGACY INVERTER RESET VIA OS COMMAND: " + terminal_line_command)
        #       return_value = os.system(terminal_line_command)
        if len(sys.argv) != 3:
            print("INVALID NUMBER OF ARGUMENTS ...")
            self.print_upgrade_reset_by_http_help()
            return

        comma_separated_thingie = sys.argv[2]
        comma_separated_thingie = comma_separated_thingie.upper()
        ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_thingie)
        if ip_list:
            for each_ip in ip_list:
                if gu.is_ip_looka_lika_multicast(each_ip):
                    print("IP %s NOT ALLOWED WITH HTTP UPGRADE RESET ..." % (each_ip))
                else:
                    # Unicast IP only. Do the upgrade reset of this inverter.
                    gu.send_http_upgrade_reset(each_ip)

    def send_tlv_reset_by_mac_or_sn(self, reset_type):
        # This is a best-effort utility, inverter reset being sent via multicast. Only basic validation of
        # the send effort is provided to the caller: that it was received by all inverters is at best a
        # best-effort. Construct the TLV data that will be inserted into the UDP payload consisting of a
        # list of MAC or SN, and send.
        if (reset_type == GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE) \
                or (reset_type == GTW_TO_SG424_UPGRADE_RESET_BY_MAC_STRING_TLV_TYPE) \
                or (reset_type == GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE) \
                or (reset_type == GTW_TO_SG424_UPGRADE_RESET_BY_SN_STRING_TLV_TYPE):
            if len(sys.argv) == 3:
                argument_list = []
                comma_separated_ip_list = sys.argv[2]
                argument_list = comma_separated_ip_list.split(',')
                number_of_items = len(argument_list)
                packed_data = gu.add_master_tlv(number_of_items)
                number_of_tlvs_appended = 0
                if (reset_type == GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE) \
                or (reset_type == GTW_TO_SG424_UPGRADE_RESET_BY_MAC_STRING_TLV_TYPE):
                    print("CHECKING %s MAC ADDRESSES ..." % (number_of_items))
                    for this_mac in argument_list:
                        if gu.is_mac_looka_lika_mac(this_mac):
                            print("ADDING MAC %s TO TLV LIST" % (this_mac))
                            packed_data = gu.append_user_tlv_mac_as_hex(packed_data, reset_type, this_mac)
                            number_of_tlvs_appended += 1
                        else:
                            print("MAC %s NO LOOKING GOOD, NOT ADDED ... " % (this_mac))
                else:
                    print("CHECKING %s SERIAL NUMBERS ..." % (number_of_items))
                    for this_sn in argument_list:
                        # For now, validation is simply ensuring 12 characters were entered, all digits.
                        if len(this_sn) == 12 and this_sn.isdigit():
                            print("ADDING SN %s TO TLV LIST" % (this_sn))
                            packed_data = gu.append_user_tlv_string(packed_data, reset_type, this_sn)
                            number_of_tlvs_appended += 1
                        else:
                            print("SN %s NO LOOKING GOOD, NOT ADDED ... " % (this_sn))

                if number_of_tlvs_appended > 0:
                    multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
                    if gu.is_send_tlv_packed_data(multicast_ip, packed_data):
                        print("RESET WITH %s TLVs SENT VIA MULTICAST" % (number_of_tlvs_appended))
                    else:
                        print("FAILED TO SEND MULTICAST ...")
                else:
                    print("NOTHING ADDED TO PACKED DATA, NOTHING SENT ...")
            else:
                print("EXPECTING 1 USER ARG: COMMA-SEPARATED MAC/SN LIST")

        else:
            # Coding error?
            print("TLV RESET BY MAC/IP TYPE INVALID, EXPECTED 1 of %s/%s/%s/%s BUT GOT %s" % (GTW_TO_SG424_SIMPLE_RESET_BY_MAC_STRING_TLV_TYPE,
                                                                                              GTW_TO_SG424_UPGRADE_RESET_BY_MAC_STRING_TLV_TYPE,
                                                                                              GTW_TO_SG424_SIMPLE_RESET_BY_SN_STRING_TLV_TYPE,
                                                                                              GTW_TO_SG424_UPGRADE_RESET_BY_SN_STRING_TLV_TYPE,
                                                                                              reset_type))

    def send_tlv_reset_by_inv_type(self, reset_type):

        reset_string_type = ""
        multicast_ip = ""
        inverter_type = ""

        if reset_type == GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE:
            reset_string_type = "PLAIN"
        elif reset_type == GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE:
            reset_string_type = "UPGRADE"
        else:
            # Coding error? Why yes, it is!
            print("ICK - I SNEEZED WHILE EATING SPAGUETTI!!!!")
            return

        if len(sys.argv) != 3:
            print("BAD NUMBER OF ARGS ...")
            if reset_type == GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE:
                self.print_plain_reset_by_inv_type_help()
            else:
                self.print_upgrade_reset_by_inv_type_help()
            return

            inverter_type = sys.argv[2]
            inverter_type = inverter_type.upper()
            if inverter_type == "ALL":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL)
            elif inverter_type == "SOLAR":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY)
            elif inverter_type == "BATTERY":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY)
            else:
                print("BAD ARG ...")
                if reset_type == GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE:
                    self.print_plain_reset_by_inv_type_help()
                else:
                    self.print_upgrade_reset_by_inv_type_help()
                return

        # Construct the TLV: 1 reset TLV with dummy_data 5678
        packed_data = gu.add_master_tlv(1)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, reset_type, 5678)

        this_string = "MULTICASTED %s (INVERTER TYPE %s) %s RESET ... " % (multicast_ip, inverter_type, reset_string_type)
        if gu.is_send_tlv_packed_data(multicast_ip, packed_data):
            print(this_string + "OK")
        else:
            print(this_string + "*FAILED*")

        return

    def send_tlv_reset_by_ip(self, reset_type):
        if reset_type == GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE:
            reset_string_type = "PLAIN"
        elif reset_type == GTW_TO_SG424_UPGRADE_RESET_COMMAND_U16_TLV_TYPE:
            reset_string_type = "UPGRADE"
        else:
            # Coding error? Why yes, it is!
            print("ICK - I SNEEZED WHILE EATING SPAGUETTI!!!!")
            return

        # Construct the TLV: 1 reset TLV with dummy_data 5678
        packed_data = gu.add_master_tlv(1)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, reset_type, 5678)

        if len(sys.argv) == 3:
            # Extract the argument and determine what the user wants.
            comma_separated_list = sys.argv[2]
            comma_separated_list = comma_separated_list.upper()
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_list)
            if ip_list:
                for ip_address in ip_list:
                    if gu.is_send_tlv_packed_data(ip_address, packed_data):
                        print("%s RESET SENT TO %s" % (reset_string_type, ip_address))
                    else:
                        print("FAILED TO SEND TO %s" % (ip_address))
            else:
                print("IP LIST WAS ... EMPTY!!!")
        else:
            print("INVALID NUMBER OF ARGUMENT")
            if reset_type == GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE:
                self.print_plain_reset_by_ip_help()
            else:
                self.print_upgrade_reset_by_ip_help()

    def validate_and_send_cli_command(self):
        if self.num_of_user_args == 3:
            cli_command = str(sys.argv[2])
            comma_separated_thingies = sys.argv[3]
            comma_separated_thingies = comma_separated_thingies.upper()
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_thingies)
            if ip_list:
                # Send the command to each inverter based on IP address.
                print("SEND THE CLI TO THE FOLLOWING:")
                for each_ip in ip_list:
                    send_result = gu.send_cli_command(each_ip, cli_command)
                    if send_result:
                        print("%s -> SENT TLV-encoded CLI: %s" % (each_ip, cli_command))
                    else:
                        print("%s -> SENT TLV-encoded CLI ... *FAILED*" % (each_ip))
            else:
                print("IP LIST WAS EMPTY ...")

        else:
            print("INVALID NUMBER OF ARGUMENTS ENTERED ...")
            self.print_send_cli_help()

    def validate_and_get_nbns_info(self):
        if self.num_of_user_args != 3:
            print("INVALID NUMBER OF USER ARGUMENTS ...")
            self.print_xet_info_by_nbns_help()
            return

        # First user arg is the NBNS request.
        user_nbns_question = str(sys.argv[2])
        user_nbns_question = user_nbns_question.upper()
        nbns_question = []
        if user_nbns_question == "NODESTATUS":
            nbns_question.append(NBNS_NODESTATUS_QUESTION)
        elif user_nbns_question == "XETINFO":
            nbns_question.append(NBNS_XETINFO_QUESTION)
        elif user_nbns_question == "NAMESERVICE":
            nbns_question.append(NBNS_NAMESERVICE_QUESTION)
        elif user_nbns_question == "ALL":
            nbns_question.append(NBNS_XETINFO_QUESTION)
            nbns_question.append(NBNS_NODESTATUS_QUESTION)
            nbns_question.append(NBNS_NAMESERVICE_QUESTION)
        else:
            print("INVALID NBNS PARAMETERS")
            self.print_xet_info_by_nbns_help()
            return

        # Extract the comma-separated list of stuff.
        comma_separated_thingies = sys.argv[3]
        comma_separated_thingies = comma_separated_thingies.upper()
        ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_thingies)

        if ip_list:
            for each_ip in ip_list:
                if gu.is_ip_looka_lika_unicast(each_ip):
                    # Unicast IP. Get the NBNS info.

                    # Get and display the requested NBNS question.
                    for this_question in nbns_question:
                        nbns_response_string = gu.send_and_get_nbns_question(each_ip, this_question)
                        if len(nbns_response_string) == 0:
                            print("IP %s GOT NULL NBNS RESPONSE STRING" % (each_ip))
                        else:
                            # Got a valid response. Some NBNS questions need a wee bit of tweeking.
                            if this_question == NBNS_NODESTATUS_QUESTION:
                                # Tweek this response.
                                # TODO: EXPLAIN AND REPLACE THE MAGIC NUMBERS 1 & 16 IN THE FOLLOWING:
                                node_status = ""
                                for char_char in nbns_response_string[1:16]:
                                    node_status += chr(int(ord(char_char)))
                                print("%s NODE STATUS/%s: %s" % (each_ip, this_question, node_status))
                            elif this_question == NBNS_XETINFO_QUESTION:
                                # Print the response as it arrived.
                                print("%s XETINFO/%s: %s" % (each_ip, this_question, nbns_response_string))
                            else:
                                # response is an IP address. Format it for print/display.
                                # TODO: EXPLAIN AND REPLACE THE MAGIC NUMBERS 2 & 6 IN THE FOLLOWING:
                                this_here_ip = ""
                                for char_char in nbns_response_string[2:6]:
                                    this_hex = hex(int(ord(char_char)))
                                    this_decimal_number = int(this_hex, 16)
                                    this_here_ip += str(this_decimal_number)
                                    this_here_ip += '.'
                                print("%s NAMESERVICE/%s: %s" % (each_ip, this_question, this_here_ip))
                else:
                    print("Please, no NBNS monkey business with IP %s" % (each_ip))
        else:
            print("IP LIST WAS EMPTY ...")

    def full_upgrade_test(self):
        # This utility will upgrade all inverters from boot loader version R<x1> -> R<x2> over and over
        # as many times as specified by the user.
        #
        # In order for this utility to run correctly, the following requirements must be met:
        #
        # 1. Standard application upgrade file must be present, as per the "software_directory" and
        #    "file_name" in the inverter_upgrade table.
        # 2. The bootloader files must be present in the folder as per the "bootloader_directory" in
        #    the inverters table. There are 4 files that must be present in the directory, for example,
        #    to perform full upgrade loop testing with boot replacement from R40 -> R41, these 4 files
        #    must be present:
        #        - SG424_LEGACY_BootComboR40.hex
        #        - SG424_LEGACY_BootComboR41.hex
        #        - SG424_BootComboR40.hex           <- same as "LEGACY" but with rev6 signature
        #        - SG424_BootComboR41.hex           <- same as "LEGACY" but with rev6 signature

        first_boot_rev = int(sys.argv[2])
        second_boot_rev = int(sys.argv[3])
        number_of_full_upgrades = int(sys.argv[4])
        comma_separated_ip = sys.argv[5]
        self.ip_list = comma_separated_ip.split(",")
        first_legacy_boot_version_file_name = "SG424_LEGACY_BootComboR" + str(first_boot_rev) + ".hex"
        second_legacy_boot_version_file_name = "SG424_LEGACY_BootComboR" + str(second_boot_rev) + ".hex"

        fut_pid = str(os.getpid())

        this_string = ("FUT: full upgrade with boot loop testing, from boot R%s to R%s over %s loops (PID=%s). MOTHER ..." % (first_boot_rev, second_boot_rev, number_of_full_upgrades, fut_pid))

        print(this_string)
        self.log_info(this_string)

        print("THIS FACILITY NEEDS TO BE UPDATED, IT HAS BEEN DISABLED FOR NOW")
        return

        for index, each_ip in enumerate(self.ip_list):
            if gu.is_this_ip_in_database(each_ip):
                print("HIT ON IP %s" % each_ip)
            else:
                print("IP %s NOT IN DATABASE - REMOVED FORM LIST" % each_ip)
                del self.ip_list[index]

        if len(self.ip_list) == 0:
            print("NO IP IN LIST - NOTHING TO DO")
            return

        for iggy in range (0, number_of_full_upgrades):

            this_string = "FUT: UPGRADE FROM BOOT R" + str(first_boot_rev) + " -> R" + str(second_boot_rev) \
                          + " *ROUND " + str(iggy+1) + " OF " + str(number_of_full_upgrades) + "*"
            print(this_string)
            self.log_info(this_string)
            time.sleep(5)
            self.this_boot_revision = first_boot_rev
            self.this_boot_file = first_legacy_boot_version_file_name
            self.upgrade_inverters_with_boot()

            time.sleep(5)
            self.this_boot_revision = second_boot_rev
            self.this_boot_file = second_legacy_boot_version_file_name
            self.upgrade_inverters_with_boot()

            this_string = "FUT: upgrade round " + str(iggy+1) + " complete"
            print(this_string)
            self.log_info(this_string)

        print("FUT: DONE")
        self.log_info("FUT: DONE")

        return

    def upgrade_inverters_with_boot(self):

        # NOTE: this FUT facility sets and prepares ALL inverters in the system for boot/application upgrade.
        #       In the future, it might be preferable to launch it with an IP address sub-list of inverters
        #       to upgrade.
        #
        # Prepare to upgrade all inverters by setting upgrade_status to upgrade_waiting.
        for each_ip in self.ip_list:
            update_string = "UPDATE %s SET %s = %s,%s = 0 WHERE %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                             INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                             INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME,
                                                                             INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
            update_args = (INVERTERS_UPGRADE_STATUS_UPGRADE_WAITING, each_ip)
            prepared_act_on_database(EXECUTE, update_string, update_args)

        # Launch the upgrade process by setting upgrade_command to start_upgrade in the inverter_upgrade table.
        # The monitor will then launch the upgrade_all_inverters.py daemon/script.
        #
        # Enter the following SQL command (the appropriate boot rev and boot combo file is specified by what
        # is in the inverter_upgrade table):
        #
        # update inverter_upgrade set boot_rev = 40,bootloader_file_name = 'SG424_LEGACY_BootComboR40.hex', upgrade_command = 'start_upgrade';
        this_string = "GROUP UPGRADE ALL INVERTEBRATES WITH R%s (bootloader file %s)" % (self.this_boot_revision, self.this_boot_file)
        print(this_string)
        self.log_info(this_string)
        update_string = "UPDATE %s SET %s = %s, %s = %s, %s = %s;" % (DB_INVERTER_UPGRADE_TABLE,
                                                                      INVERTER_UPGRADE_TABLE_BOOT_REV_COLUMN_NAME, "%s",
                                                                      INVERTER_UPGRADE_TABLE_BOOTLOADER_FILE_NAME_COLUMN_NAME, "%s",
                                                                      INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME, "%s")
        update_args = (self.this_boot_revision, self.this_boot_file, INVERTER_UPGRADE_COMMAND_START_UPGRADE)
        prepared_act_on_database(EXECUTE, update_string, update_args)

        # The upgrade will now start automatically when the monitor detects the change in the upgrade_command field.
        # Large scale upgrades can take time, don't bother notifying too frequently, say once every 2 minutes.
        CHECK_EVERY_SO_OFTEN = 30
        PRINT_STILL_ONGOING = 120
        eternity_is_very_patient = True
        check_ongoing_seconds = 0
        while eternity_is_very_patient:
            time.sleep(CHECK_EVERY_SO_OFTEN)
            select_string = "select %s from %s;" % (INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME,
                                                    DB_INVERTER_UPGRADE_TABLE)
            upgrade_command = prepared_act_on_database(FETCH_ONE, select_string, ())

            upgrade_result = upgrade_command[INVERTER_UPGRADE_TABLE_UPGRADE_COMMAND_COLUMN_NAME]
            if upgrade_result == INVERTER_UPGRADE_COMMAND_NO_COMMAND:
                eternity_is_very_patient = False
                this_string = "GROUP UPGRADE COMPLETE"
                print(this_string)
                self.log_info(this_string)

            else:
                # Upgrade is still on-going. Has the user reached the limits of patience?
                check_ongoing_seconds += CHECK_EVERY_SO_OFTEN
                if check_ongoing_seconds >= PRINT_STILL_ONGOING:
                    check_ongoing_seconds = 0
                    this_string = "UPGRADE STILL ON-GOING ..."
                    print(this_string)
                    # self.log_info(this_string) ... NO, just log to terminal where script was launched from.
                else:
                    # Not done. Have a wee. And a wee bit of patience.
                    pass

    def send_keep_alive_test(self):
        # Extract 2 arguments: timeout(msec) and the IP address.
        keep_alive_timeout_msec = int(sys.argv[2])
        ip_address = str(sys.argv[3])
        timeout_secs = keep_alive_timeout_msec / 1000

        forever = True
        print("SEND TEST keepAlive TO " + ip_address + " with timeout(secs) = " + str(timeout_secs) + " *FOR EVER AND EVER*")
        number_of_user_tlv = 3
        magical_sequence_number = SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE
        # To include fallback and NTP URL: get them based on the inverter's group_id.
        group_id = gu.db_get_inverter_group_id(ip_address)
        this_group_row_data = gu.db_get_group_id_row_data(group_id)
        if this_group_row_data:
            fallback_setting_percent = this_group_row_data[GROUPS_TABLE_FALLBACK_COLUMN_NAME]
            ntp_server_url = this_group_row_data[GROUPS_TABLE_NTP_URL_SG424_COLUMN_NAME]
        else:
            fallback_setting_percent = 0
            ntp_server_url = "no_server.com"

        while forever:
            time.sleep(timeout_secs)
            print("SEQ_NUM: " + str(magical_sequence_number))
            packed_data = gu.add_master_tlv(number_of_user_tlv)
            packed_data = gu.append_user_tlv_32bit_unsigned_long(packed_data, GTW_TO_SG424_TEST_KEEP_ALIVE_U32_TLV_TYPE, magical_sequence_number)
            packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data,
                                                                  SG424_TO_GTW_INVERTER_FALLBACK_POWER_SETTING_PERCENT_U16_TLV_TYPE,
                                                                  fallback_setting_percent)
            packed_data = gu.append_user_tlv_string(packed_data, SG424_TO_GTW_NTP_SERVER_STRING_TLV_TYPE, ntp_server_url)
            if not gu.is_send_tlv_packed_data(ip_address, packed_data):
                print("Failed to send ...")
            magical_sequence_number += 1

    def display_get_comm_progress(self):
        # Display progress in increments of 10%. Don't bother with 0%, and don't repeat.
        # Sometimes the percentage doesn't come out to whole increments of 10%, this is
        # accounted for. Only display progress if there are a lot of inverters, in this
        # case, if there are more than 100 inverters.
        if self.number_of_inverters_to_scan > 99:
            if self.last_percentage_progress_displayed == DEVIL_STARTING_PERCENTAGE:
                self.next_whole_percentage = 10

            percentage_progress = (self.number_of_inverters_scanned_so_far * 100) / self.number_of_inverters_to_scan
            this_modulus = percentage_progress % 10
            if (this_modulus == 0 and percentage_progress != 0) or (percentage_progress > self.next_whole_percentage):
                # A multiple of 10 percent. Or the current percent is more than the next expected percentgage.
                if percentage_progress != self.next_whole_percentage:
                    # Just went past an increment of 10%, so set it to the whole increment.
                    percentage_progress = self.next_whole_percentage
                if self.last_percentage_progress_displayed != percentage_progress:
                    self.last_percentage_progress_displayed = percentage_progress
                    self.next_whole_percentage += 10
                    print("SCAN: " + str(percentage_progress) + "% COMPLETE ...")

    def get_inverters_comm(self):

        print ("Deprecated - replaced with gpts psa")
        return

    def query_inverters(self):
        # One form of the command:
        #
        #   [0]    [1]         [2]
        # gpts.py query <comma-separated thingies>              <- 3 arguments: query this list of things
        #
        if len(sys.argv) != 3:
            print("INVALID NUMBER OF ARGUMENTS ...")
            self.print_query_help()
            return

        one_arg_only = sys.argv[2]
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            print("QUERY INVERTERS BY IP ON BASIS OF INVERTER TYPE %s" % (one_arg_only))
            check_user_thingies = False
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
            self.query_by_ip_list(ip_list)

        else:

            user_argument_list = sys.argv[2].split(',')
            if gu.is_ip_looka_lika_unicast(user_argument_list[0]):
                # Treat the list as IP addresses. Create a comma-separated list of IP addresses from this argument.
                print("QUERY INVERTERS BY IP LIST")
                self.query_by_ip_list(user_argument_list)

            elif gu.is_string_id_looka_lika_string_id(user_argument_list[0]):
                # Treat the comma-separated list as string IDs.
                print("QUERY INVERTERS BY STRING ID LIST OF LENGTH %s" % (len(user_argument_list)))
                for this_string_id in user_argument_list:
                    select_string = "SELECT %s FROM %s WHERE %s=%s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                                        DB_INVERTER_NXO_TABLE,
                                                                        INVERTERS_TABLE_STRING_ID_COLUMN_NAME, "%s")
                    select_args = (this_string_id,)
                    string_id_db_entry = prepared_act_on_database(FETCH_ALL, select_string, (select_args))
                    if string_id_db_entry:
                        this_ip_list = []
                        for each_inverter in string_id_db_entry:
                            this_ip_list.append(each_inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
                        if len(this_ip_list) != 0:
                            print("QUERY %s INVERTERS BASED ON STRING ID %s"% (len(this_ip_list), this_string_id))
                            self.query_by_ip_list(this_ip_list)
                            del this_ip_list[:]
                        else:
                            # ODD ... if there's a STRING ID in the database, why is the IP list length 0?
                            print("NO IP IN DATABASE BASED ON STRING ID %s" % (this_string_id))
                    else:
                        print("NO ENTRY IN DATABASE BASED ON STRING ID %s" % (this_string_id))

            elif gu.is_mac_looka_lika_mac(user_argument_list[0]):
                # Treat the list as MAC addresses.
                print("QUERY INVERTERS BY MAC LIST")
                self.query_by_mac_or_sn_list(GTW_TO_SG424_QUERY_BY_MAC_6HEX_BYTES_TLV_TYPE, user_argument_list)
            elif (len(user_argument_list[0]) == 12) and (user_argument_list[0].isdigit()):
                # Treat the list as serial numbers.
                print("QUERY INVERTERS BY SERIAL NUMBER LIST")
                self.query_by_mac_or_sn_list(GTW_TO_SG424_QUERY_BY_SERIAL_NUMBER_STRING_TLV_TYPE, user_argument_list)

            elif user_argument_list[0].isdigit():
                # All arguments to be interpreted/treated as group ID.
                print("QUERY INVERTERS BY GROUP ID LIST")
                ip_list = []
                for each_group_id in user_argument_list:
                    if each_group_id.isdigit():
                        itty_bitty_ip_list = gu.get_ip_list_from_group_id(each_group_id)
                        if len(itty_bitty_ip_list) != 0:
                            this_string = "GROUP ID %s -> IP =" % (each_group_id)
                            for each_ip in itty_bitty_ip_list:
                                ip_list.append(each_ip)
                                this_string += " %s," % (each_ip)
                            this_string = this_string[:-1]# Drop the last comma
                            print(this_string)
                        else:
                            print("GROUP ID %s HAS NO INVERTERS" % (each_group_id))
                    else:
                        print("GROUP ID %s MUST BE A DIGIT" % (each_group_id))
                # ... for
                if len(ip_list) != 0:
                    self.query_by_ip_list(ip_list)
                else:
                    print("THERE WERE NO IPs FOR THE SPECIFIED GROUP ID LIST ...")

            else:
                print("First arg = %s not looka lika anything I lika, reconsider your request ..." % (user_argument_list[0]))
                self.print_query_help()

    def query_by_ip_list(self, ip_list):
        # Some validation of each IP is performed: does it look like a reasonable unicast IP?
        # Check that if valid, if the IP is in the database. If not, complain some, but query anyway.
        print("QUERY INVERTERS BY IP LIST OF LENGTH %s" % (len(ip_list)))
        for this_ip in ip_list:
            print("----------------------------------------------------------------")
            if gu.is_ip_looka_lika_unicast(this_ip):
                if not gu.is_this_ip_in_database(this_ip):
                    # Most likely, a user-supplied IP argument list. Could be an IP fat-fingered by the user,
                    # or is trying to query an inverter that somehow, did not make it into the database.
                    print("IP %s NOT IN DATABASE - WILL QUERY NEVERTHELESS" % (this_ip))
                SG424_data = gu.send_and_get_query_data(this_ip)
                if SG424_data:
                    # Packet received.
                    payload_length = len(SG424_data)
                    (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                    gu.print_inverter_tlv_list_length_title(tlv_id_list, tlv_data_list, this_ip, payload_length)
                    gu.print_inverter_tlv_id_and_data_list(tlv_id_list, tlv_data_list)
                else:
                    print("%s -> NO_RESPONSE" % (this_ip))
            else:
                print("IP %s doesn't looka lika unicast address ..." % (this_ip))
        # for this_ip ...

    def query_by_mac_or_sn_list(self, request_by_tlv, mac_or_sn_list):
        ok_to_query = False
        query_list = []
        query_type_string = ""

        if len(mac_or_sn_list) > 0:
            if request_by_tlv == GTW_TO_SG424_QUERY_BY_MAC_6HEX_BYTES_TLV_TYPE:
                # Get the MACs list:
                query_type_string = "QUERY-BY-MAC"
                for this_mac in mac_or_sn_list:
                    if gu.is_mac_looka_lika_mac(this_mac):
                        query_list.append(this_mac)
                        ok_to_query = True
                        if not gu.is_this_mac_address_in_database(this_mac):
                            # Most likely, a user-supplied IP argument list. Could be a MAC fat-fingered by the user,
                            # or is trying to query an inverter that somehow, did not make it into the database.
                            print("MAC %s NOT IN DATABASE - WILL QUERY NEVERTHELESS" % (this_mac))
                    else:
                        print("Sorry, but this mac %s is whack" % (this_mac))

            elif request_by_tlv == GTW_TO_SG424_QUERY_BY_SERIAL_NUMBER_STRING_TLV_TYPE:
                # Get the serial numbers list:
                query_type_string = "QUERY-BY-SN"
                for this_sn in mac_or_sn_list:
                    if len(this_sn) == 12 and this_sn.isdigit():
                        query_list.append(this_sn)
                        ok_to_query = True
                        if not gu.is_this_serial_number_in_database(this_sn):
                            # Most likely, a user-supplied IP argument list. Could be a serial number fat-fingered by
                            # he user, or is trying to query an inverter that somehow, did not make it into the database.
                            print("SN %s NOT IN DATABASE - WILL QUERY NEVERTHELESS" % (this_sn))
                    else:
                        print("Sorry, but your serial number %s has a funky odor" % (this_sn))

            else:
                # Not expected, given that this function is called under controlled condition.
                # Supposedly enough time has passed for the impossible to happen.
                print("How did this function get called with TLV " + str(request_by_tlv) + "???")
                ok_to_query = False

        else:
            print("MAMA BAD ... presumed coding error!!!")
            ok_to_query = False

        if ok_to_query:
            # This condition couldn't be any other way ...
            print("WILL QUERY %s INVERTERS VIA MULTICASTED %s" % (len(query_list), query_type_string))
            for this_query in query_list:
                print("----------------------------------------------------------------")
                print("QUERYING %s VIA MULTICAST (TLV=%s), WAITING FOR RESPONSES ..." % (this_query, request_by_tlv))
                (replying_ip_address, SG424_data) = gu.get_inverter_query_data_by_mac_or_sn(request_by_tlv, this_query)
                if SG424_data:
                    # Packet received.
                    payload_length = len(SG424_data)
                    (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                    gu.print_inverter_tlv_list_length_title(tlv_id_list, tlv_data_list, replying_ip_address, payload_length)
                    gu.print_inverter_tlv_id_and_data_list(tlv_id_list, tlv_data_list)

                else:
                    print(" -> NO_RESPONSE FROM ANY INVERTER")


    # -----------------------------------------------------------------------------------------------------------------
    #
    # A small set of commands to "MULTICAST BY GROUP ID".
    #
    # These commands differ from other similar commands that create a list of IP addresses from a
    # given group ID (contained in a comma-separated list of thingies). The following set of commands
    # multicast a single packet with the desired sub-TLV encapsulated in group ID TLV.
    #
    # The following commands are available:
    #
    # - query: multicast a "plain query" encapsulated in a group ID TLV and wait for responses
    # - plain reset: multicast a "plain reset TLV" encapsulated in a group ID TLV
    # - upgrade query: same as plain except upgrade reset.
    #

    def query_by_group(self):
        # Only 1 parameter is expected: a comma-separated list of group ID.
        #
        # Command is of the form:
        #
        #   [0]    [1]         [2]
        # gpts.py query_by_group <comma-separated group IDs>      <- 3 arguments: query inverters by (list of) group IDs
        #
        if len(sys.argv) != 3:
            print("INVALID NUMBER OF ARGUMENTS ...")
            self.plain_reset_by_group()
            return

        one_arg_only = sys.argv[2]
        group_id_list = one_arg_only.split(',')
        print("WILL QUERY AS MANY INVERTERS THAT ARE ASSIGNED TO THE FOLLOWING %s GROUPS:" % len(group_id_list))
        number_of_user_tlv = 0
        total_tlv_payload = ""
        for each_group_id_as_string in group_id_list:
            if each_group_id_as_string.isdigit():
                group_id = int(each_group_id_as_string)
                # Is this group ID in the groups table?
                database_row_data = gu.db_get_group_id_row_data(group_id)
                if database_row_data:
                    # Create a packed data buffer that will be included in the multicasted packet.
                    print("BUILD ENCAPSULATED GROUP TLV FOR ID %s" % (group_id))
                    sub_group_packed_data = gu.add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE, 1234)
                    sub_group_length = len(sub_group_packed_data) + 2  # extra 2 bytes to encompass the group ID itself
                    group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_length, group_id)
                    this_group_packed_data = (group_id_packed_data + sub_group_packed_data)
                    total_tlv_payload += this_group_packed_data
                    number_of_user_tlv += 2
                else:
                    print("THIS GROUP ID %s WAS  NOT IN THE DATABASE" % (group_id))
            else:
                print("THIS GROUP ID %s SHOULD HAVE BEEN DIGIT - IT DIDN'T EVEN PASS FOR A PINKY" % (group_id))

        if number_of_user_tlv > 0:
            master_packed_data = gu.add_master_tlv(number_of_user_tlv)
            complete_packed_data = master_packed_data + total_tlv_payload
            print("HEX TLV PACKED DATA TO SEND:")                              # <- optional
            gu.print_this_hex_data(complete_packed_data)                       # <- optional

            # Send, wait/print RESPONSES
            gu.send_and_query_by_encapsulated_group_id(complete_packed_data)

        else:
            print("THERE WAS NOTHING TO SEND!!!")

        return

    def plain_reset_by_group(self):
        # Only 1 parameter is expected: a comma-separated list of group ID.
        #
        # Command is of the form:
        #
        #   [0]    [1]         [2]
        # gpts.py plain_reset_by_group <comma-separated group IDs>      <- 3 arguments: query inverters by (list of) group IDs
        #
        if len(sys.argv) != 3:
            print("INVALID NUMBER OF ARGUMENTS ...")
            self.plain_reset_by_group()
            return

        one_arg_only = sys.argv[2]
        group_id_list = one_arg_only.split(',')
        print("WILL MULTICAST PLAIN RESET TO AS MANY INVERTERS THAT ARE ASSIGNED TO THE FOLLOWING %s GROUPS:" % len(group_id_list))
        number_of_user_tlv = 0
        total_tlv_payload = ""
        for each_group_id_as_string in group_id_list:
            if each_group_id_as_string.isdigit():
                group_id = int(each_group_id_as_string)
                # Is this group ID in the groups table?
                database_row_data = gu.db_get_group_id_row_data(group_id)
                if database_row_data:
                    # Create a packed data buffer that will be included in the multicasted packet.
                    print("BUILD ENCAPSULATED GROUP TLV FOR ID %s" % (group_id))
                    sub_group_packed_data = gu.add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE, 1234)
                    sub_group_length = len(sub_group_packed_data) + 2  # extra 2 bytes to encompass the group ID itself
                    group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_GROUP_ID_U16_TLV_TYPE, sub_group_length, group_id)
                    this_group_packed_data = (group_id_packed_data + sub_group_packed_data)
                    total_tlv_payload += this_group_packed_data
                    number_of_user_tlv += 2
                else:
                    print("THIS GROUP ID %s WAS  NOT IN THE DATABASE" % (group_id))
            else:
                print("THIS GROUP ID %s SHOULD HAVE BEEN DIGIT - IT DIDN'T EVEN PASS FOR A PINKY" % (group_id))

        if number_of_user_tlv > 0:
            master_packed_data = gu.add_master_tlv(number_of_user_tlv)
            complete_packed_data = master_packed_data + total_tlv_payload
            print("HEX TLV PACKED DATA TO SEND:")                              # <- optional
            gu.print_this_hex_data(complete_packed_data)                       # <- optional

            # Send, don't wait.
            multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            send_result = gu.is_send_tlv_packed_data(multicast_ip, complete_packed_data)
            if send_result:
                print("MCAST (%s) PLAIN RESET BY GROUP ID SUCCESS" % (multicast_ip))
            else:
                print("MCAST (%s) PLAIN RESET BY GROUP ID ... *FAILED*" % (multicast_ip))

        else:
            print("THERE WAS NOTHING TO SEND!!!")

        return

    def upgrade_reset_by_group(self):
        print("NOT READY FOR upgrade_reset_by_group")
        return

    # help facilities to support the above by-group commands:
    def print_query_by_group_help(self):
        print("NOT READY FOR print_query_by_group_help")
        return

    def print_plain_reset_by_group_help(self):
        print("NOT READY FOR print_plain_reset_by_group_help")
        return

    def print_upgrade_reset_by_group_help(self):
        print("NOT READY FOR print_upgrade_reset_by_group_help")
        return

    #
    #
    #
    # -----------------------------------------------------------------------------------------------------------------


    def get_mom_data(self):

        # Requires 4 arguments: gpts.py mom frequency IP
        #                         [0]   [1]    [2]    [3]
        if len(sys.argv) == 4:
            # 4 arguments (gpts.py=[0], mom=[1], and the frequency/IP args are in [2] and [3].
            # Get the frequency.
            try:
                frequency = float(sys.argv[2])
            except:
                print("frequency must be: 0, or a float or integer")
                return

            # Get and validate the IP address.
            ip_address = str(sys.argv[3])
            if gu.is_ip_looka_lika_multicast(ip_address):
                # Don't send this to the multicast!
                print("You must be insane to think I would multicast this request")
            elif not gu.is_ip_looka_lika_unicast(ip_address):
                print("No can do ... IP %s don't look like no unicast IP" % (ip_address))
            else:
                # Input is good. Proceed with getting something from mom.
                if frequency > 0:
                    print("CALL MOM EVERY %s SECONDS - ONLY CTRL-C WILL STOP THIS PROCESS ..." % (frequency))

                if not gu.is_this_ip_in_database(ip_address):
                    # This IP is not in the database. User must be smoking
                    print("IP %s NOT IN DATABASE - PROCEEDING NEVERTHELESS" % (ip_address))

                # OPEN THE SOCKET:
                try:
                    fancy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                except Exception: # as error:
                    print("socket.socket failed ...")
                else:
                    # Print a header to identify the columns that will be printed out.
                    gu.print_mom_header()

                    keep_sending = True
                    while keep_sending:

                        SG424_data = gu.send_and_get_mom_data(ip_address)
                        if SG424_data:
                            # Packet received. Extract the list of TLV and data, and parse/print MOM data.
                            # print_this_hex_data(SG424_data) # <- you dare look at the raw data arriving on the wire?
                            (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                            gu.parse_and_print_mom_data(ip_address, tlv_id_list, tlv_data_list)
                        else:
                            # No response. Or the inverter does not respond to requests for MOM.
                            print(ip_address + " -> NO_MOM_RESPONSE")

                        if frequency == 0:
                            # Once only, so ...
                            keep_sending = False
                        else:
                            # Timed ...
                            time.sleep(frequency)
                            keep_sending = True

        else:
            print("ARGUMENT COUNT IS ALL WRONG - GO ASK MOM FOR HELP")

    def get_dc_output_by_tlv(self, dc_or_dc_gwa_tlv):

        # Requires ...
        if len(sys.argv) == 4:
            # 4 arguments (gpts.py=[0], dc=[1], and the frequency/IP args are in [2] and [3].
            # Get the frequency.
            try:
                frequency = float(sys.argv[2])
            except:
                print("frequency must be: 0, or a float or integer")
                if dc_or_dc_gwa_tlv == GTW_TO_SG424_GET_DC_SM_OUTPUT_U16_TLV_TYPE:
                    self.print_dc_output_string_help()
                else:
                    self.print_dc_gwa_output_string_help()
                return

            # Get and validate the comma-separated list of thingies ...
            comma_separated_string = sys.argv[3]
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)

            new_loop_string = None
            if len(ip_list) > 1:
                if dc_or_dc_gwa_tlv == GTW_TO_SG424_GET_DC_SM_OUTPUT_U16_TLV_TYPE:
                    new_loop_string = "LOOP THROUGH IP LIST TO GET DC OUTPUT ..."
                else:
                    new_loop_string = "LOOP THROUGH IP LIST TO GET DC_GWA OUTPUT ..."

            number_of_user_tlv = 1
            packed_data = gu.add_master_tlv(number_of_user_tlv)
            packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, dc_or_dc_gwa_tlv, 1234)
            if frequency > 0:
                print("ONLY CTRL-C WILL STOP THIS PROCESS ...")

            keep_sending = True
            while keep_sending:
                if new_loop_string:
                    print(new_loop_string)
                for ip_address in ip_list:
                    # Multicast check not required: would have been removed.
                    # Wacked IP check not required: would have been removed.
                    # Duplicate IP check not required: would not have been added.
                    if not gu.is_this_ip_in_database(ip_address):
                        print("IP %s NOT IN DATABASE - WILL QUERY NEVERTHELES" % (ip_address))

                    # OPEN THE SOCKET:
                    try:
                        fancy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    except Exception: # as error:
                        print("GET DC: socket.socket failed ...")
                    else:

                        # SEND THE PACKET:
                        try:
                            fancy_sock.sendto(packed_data, (ip_address, SG424_PRIVATE_UDP_PORT))
                        except Exception: # as error:
                            # Facility/system resource issue.
                            print("GET DC: sendto failed ...")
                            keep_sending = False
                        else:

                            # Now wait for a response:
                            try:
                                fancy_sock.settimeout(3.0)
                                private_port_packet = fancy_sock.recvfrom(1024)
                            except Exception:
                                # Timed out waiting to receive a packet. Simply means the inverter
                                # at that IP is not responding. Keep trying.
                                print(ip_address + " -> NO_RESPONSE")

                            else:

                                # Packet received, data is in private_port_packet field.
                                (some_ip, some_port, SG424_data) = gu.get_udp_components(private_port_packet)
                                (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                                # Only 1 TLV is expected in this response, but the possibility exists that there may
                                # be more than currently expected by the Gateway. Just ignore them.
                                if (len(tlv_id_list) == len(tlv_data_list)) and len(tlv_id_list) != 0:
                                    for tlv_id, tlv_data in zip(tlv_id_list, tlv_data_list):
                                        if tlv_id == SG424_TO_GTW_DC_SM_OUTPUT_STRING_TLV_TYPE \
                                        or tlv_id == SG424_TO_GTW_DC_GWA_SM_OUTPUT_STRING_TLV_TYPE:
                                            dc_sm_output_string = tlv_data
                                            print(dc_sm_output_string)
                                        else:
                                            # Ignore this TLV.
                                            pass
                                else:
                                    # Got a response, but no data included? A corrupted response from the inverter?
                                    print("%s -> rcv'd zero length TLV response to FD query" % (ip_address))

                        # Close the socket:
                        try:
                            fancy_sock.close()
                        except Exception: # as error:
                            print("close socket failed ...")
                        else:
                            pass
                # for ...

                if frequency == 0:
                    # Once only, so ...
                    keep_sending = False
                else:
                    # Timed ...
                    time.sleep(frequency)
                    keep_sending = True
            # while ...

        else:
            print("you bonehead ... get some help!")
            if dc_or_dc_gwa_tlv == GTW_TO_SG424_GET_DC_SM_OUTPUT_U16_TLV_TYPE:
                self.print_dc_output_string_help()
            else:
                self.print_dc_gwa_output_string_help()

    def get_dc_output(self):
        self.get_dc_output_by_tlv(GTW_TO_SG424_GET_DC_SM_OUTPUT_U16_TLV_TYPE)

    def get_dc_gwa_output(self):
        self.get_dc_output_by_tlv(GTW_TO_SG424_GET_DC_GWA_SM_OUTPUT_U16_TLV_TYPE)

    def inverters_table_consistency_audit(self):
        # Synopsis: checking oddities on all rows in the inverters table,
        #           auditing a few of the columns for consistency and sanity.

        # -----------------------------------------------------------------------------------------
        #
        # PREAMBLE:
        #

        deleteFile, delayTime, newArgs = self.parse_cmd_line_delete_delay(sys.argv)

        if deleteFile == True:
            terminal_line_command = "sudo rm " + ITCA_RESULTS_PATH_AND_FILE_NAME
            donut_care_return = os.system(terminal_line_command)
            print("ITCA: file " + ITCA_RESULTS_PATH_AND_FILE_NAME + " deleted before start")

        audit_start_utc = int(time.time())
        current_dt = time.strftime("%Y-%m-%d %H:%M:%S")

        # First things first: open the text file into which the results of the audit will be written.
        try:
            results_file_handle = open(ITCA_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            print("FAILED TO OPEN " + ITCA_RESULTS_PATH_AND_FILE_NAME + " FILE")
            return

        this_string = "ITCA: INVERTERS TABLE CONSISTENCY AUDIT LAUNCHED AT " + str(current_dt)
        # For the terminal:
        print(this_string)

        # For the results file:
        this_string = "---------------------------------------------------------------\r\n" + this_string
        results_file_handle.write(this_string + LINE_TERMINATION)
        #
        # END OF PREAMBLE
        #
        # -----------------------------------------------------------------------------------------

        # ---------------------------------------------------------------------------
        #
        # *** CORE OF THIS AUDIT'S PROCESSING ...
        #
        fetch_results = []
        inverters_table_row_count = 0
        select_string = "SELECT COUNT(*) FROM %s;" % DB_INVERTER_NXO_TABLE
        fetch_results = prepared_act_on_database(FETCH_ONE, select_string, ())
        inverters_table_row_count = int(fetch_results['COUNT(*)'])
        this_string = "INVERTERS ROW COUNT: %s" % (inverters_table_row_count)
        results_file_handle.write(this_string + LINE_TERMINATION)
        # Check in turn:
        # - "IP_Address"       <- any "NULL" entries?
        # - "serialNumber      <- any "NULL" serial number entries?
        # - "mac_address"      <- any "NULL" mac entries?
        # - "IP_Address"       <- any repeated IP addresses?
        # - "serialNumber"     <- any repeated serial numbers?
        # - "mac_address"      <- any repeated MAC addresses?
        # - "version"          <- all rows have the same version?
        # - "stringPosition"   <- all rows have a string position from 0 to 8?
        # - "version"          <- how many inverters are running different versions of software?
        # - "battery"          <- battery-related data
        #
        # NOTE: - "duplicate IP addresses" in the database is a known and observed problem. The distinct_ip_address
        #         audit can result in several actions. If IP is duplicate but MAC and SN are distinct, then IP is set
        #         to 0.0.0.0 for all duplicated entries, and a TLV command is sent via multicast to reset an inverter
        #         by MAC and SN, so 1 or more inverter can reset themselves. On the other hand, if IP/MAC/SN rows are
        #         the same, then the rows are considered duplicates; the row with the highest ID is retained, and the
        #         other rows are deleted.
        #
        gu.inverters_db_consistency_check_null_ip_address(results_file_handle)                # <- display/info only
        gu.inverters_db_consistency_check_null_sn(results_file_handle)                        # <- display/info only
        gu.inverters_db_consistency_check_null_mac_address(results_file_handle)               # <- display/info only
        gu.inverters_db_consistency_check_distinct_ip_address(results_file_handle)            # <- database can be modified
        gu.inverters_db_consistency_check_distinct_sn(results_file_handle)                    # <- display/info only *** FOR NOW***
        gu.inverters_db_consistency_check_distinct_mac_address(results_file_handle)           # <- display/info only *** FOR NOW***
        gu.inverters_db_consistency_check_version(results_file_handle)                        # <- display/info only
        gu.inverters_db_consistency_check_string_id_and_position(results_file_handle)         # <- database can be modified
        gu.inverters_db_consistency_check_feed_name(results_file_handle)                      # <- database can be modified
        gu.battery_tables_db_consistency_check(results_file_handle)                           # <- database can be modified
        #
        #
        # END OF THIS AUDIT
        #
        # -----------------------------------------------------------------------------------------


        # -----------------------------------------------------------------------------------------
        #
        # POSTAMBLE(?):
        #
        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        this_string = "ITCA: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        results_file_handle.write(this_string + LINE_TERMINATION)
        this_string += ", RESULTS WRITTEN TO " + ITCA_RESULTS_PATH_AND_FILE_NAME
        print(this_string)

        # Close the results file.
        results_file_handle.close()
        #
        # -----------------------------------------------------------------------------------------

    def db_to_inverters_consistency_audit(self):
        # I am ... DICA.
        #
        # This facility will query 1 or more inverters, and write free-format text to a log file.
        #
        # This facility called with 2 or 3 arguments:
        #
        #                [0]   [1]        [2]
        # sudo python gpts.py dica  <ALL|SOLAR|BATTERY|comma-separated list of thingies>  <- arg length = 3
        #
        # If only 2 arguments are used, then ALL devices will be queried

        # Synopsis: go through some list of IP addresses, and for each:
        #
        #           - read the inverters database table,
        #             for each entry:
        #                 - if it is in a "non-gateway" group: DO NO QUERY
        #                 - send a TLV-encoded query to the indicated IP
        #                 - wait for a response
        #                 - check the TLV contained in the packet
        #                 - for each TLV, compare to that component in the database
        #                 - if OK, then "no news is good news"
        #                 - else: - update the inverter as necessary,
        #                         - and/or update the database as necessary
        #                         - write to the results file

        # -----------------------------------------------------------------------------------------
        #
        # PREAMBLE:
        #

        ip_list = []
        audit_start_utc = int(time.time())
        current_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        progress_display_title = "DB-to-inverters audit: "
        universal_aif_row = gu.get_universal_aif_db_row()
        if UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME not in universal_aif_row \
        or universal_aif_row[UNIVERSAL_AIF_TABLE_REGION_NAME_COLUMN_NAME] == REGION_NAME_UNDEFINED:
            universal_aif_row = {}

        number_of_user_args = len(sys.argv) -2

        if number_of_user_args == 0:
            comma_separated_thingies = "ALL"
        else:
            comma_separated_thingies = sys.argv[2]

        # There is no multicasting. If the user specified "ALL", "SOLAR" "BATTERY" as the only
        # "comma-separated list of thingies", then extract all relevant IP addresses from the
        # database into a list. Otherwise, validate the user argument as a comma-separated list
        # of things".
        check_user_thingies = True
        user_argument_list = comma_separated_thingies.split(',')

        if len(user_argument_list) == 1:
            one_arg_only = user_argument_list[0]
            one_arg_only = one_arg_only.upper()
            if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
                check_user_thingies = False
                ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)

        if check_user_thingies:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_thingies)
            if not ip_list:
                this_string = "DICA: IP LIST FROM ARGUMENT WAS EMPTY - NOTHING TO DO"
                print(this_string)
                return
            else:
                # Go through the list and remove any multicast-looking IPs from the list.
                for index, this_ip in enumerate(ip_list):
                    if gu.is_ip_looka_lika_multicast(this_ip):
                        # drop this IP from the list.
                        del ip_list[index]

        # First things first: open the text file into which the results of the audit will be written.
        try:
            results_file_handle = open(DICA_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            print("FAILED TO OPEN %s FILE" % (DICA_RESULTS_PATH_AND_FILE_NAME))
            return

        # print to the screen and write to the results file.
        this_string = "DICA: DB/INVERTERS CONSISTENCY AUDIT LAUNCHED AT " + str(current_dt)
        print(this_string)
        this_string = "---------------------------------------------------------------\r\n" + this_string
        results_file_handle.write(this_string + LINE_TERMINATION)
        #
        # END OF PREAMBLE
        #
        # -----------------------------------------------------------------------------------------

        # ---------------------------------------------------------------------------
        #
        # *** CORE OF THIS AUDIT'S PROCESSING ...
        #
        if len(ip_list) == 0:
            this_string = "GPTS DICA: NO INVERTERS FROM THE DATABASE"
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        # -----------------------------------------------------------------------------------------
        # Setup based on parameters done. Query inverters based on constructed inverters_data_list.
        #
        this_string = "DICA: DB-TO-INVERTER CONSISTENCY ON %s ENTRIES FROM " % (len(ip_list))
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)

        # DICA runs even if in observation mode, provide the indication that this happened.
        if gu.is_gateway_in_observation_mode():
            this_string = "DICA AUDIT EXECUTED WHILE GATEWAY IN OBSERVATION MODE"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)

        excel_title_string = "- EXCEL,INDEX,IP,STR_ID,STR_POS,MODE,VERSION,BOOT,PROD_PN,SW_PROF,SN,MAC,RESETS," \
                             + "UPTIME,FEED,PCC,CONNECT,PF,PF_C_LEAD,PF_ENA,ET,INV_CTRL,KA_TMO,ERC,WATTS,VOLTS,AMPS," \
                             + "DC_GWA_SM_OUTPUT,BATTERY,FALLBACK,NTP_URL,PCS_UNIT_ID,GROUP_ID,ER_SERVER_HTTP,ER_GTW_TLV"
        results_file_handle.write(excel_title_string + LINE_TERMINATION)
        self.excel_index = 0

        # Setup data for progress indicator.
        self.number_of_inverters_to_audit = len(ip_list)
        self.init_data_for_progress_indicator()

        for each_ip in ip_list:
            this_inverter_db_row_data = self.get_dica_inverter_data_from_ip(each_ip)
            if self.is_valid_and_fixed_pcs_unit_ip(this_inverter_db_row_data):
                this_string = "PCS UNIT ID ID AGAINST IP %s FIXED/SET TO 0" % (each_ip)
                results_file_handle.write(this_string + LINE_TERMINATION)

            if gu.is_gateway_control_set_against_this_ip(each_ip):
                # This inverter is under gateway control. It is to be audited.
                self.number_of_inverters_audited_so_far += 1
                # For the inverter at this IP, send a general purpose TLV query,
                # and wait for the response.
                self.query_response_received = False
                self.inverter_ip_address = this_inverter_db_row_data[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME]
                self.db_inverter_app_version = this_inverter_db_row_data[INVERTERS_TABLE_VERSION_COLUMN_NAME]
                if not self.db_inverter_app_version:
                    self.db_inverter_app_version = "NULL"
                self.inverter_string_id = this_inverter_db_row_data[INVERTERS_TABLE_STRING_ID_COLUMN_NAME]
                self.inverter_string_position = this_inverter_db_row_data[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME]
                self.inverter_feed_name = this_inverter_db_row_data[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME]
                self.pcs_unit_id = this_inverter_db_row_data[INVERTERS_TABLE_PCS_UNIT_ID_COLUMN_NAME]
                self.group_id = this_inverter_db_row_data[INVERTERS_TABLE_GROUP_ID_COLUMN_NAME]

                if not self.inverter_feed_name:
                    self.inverter_feed_name = ""

                if self.inverter_ip_address:
                    self.excel_index += 1

                    # Perform only 1 retry if no response. The following is a "dumb-dumb loop".
                    # If more than 1 retry is desirable, use a while loop with counter.
                    this_string = "VALIDATING DATA FOR " + self.inverter_ip_address + ":"
                    results_file_handle.write(this_string + LINE_TERMINATION)

                    # ------------------------------------------------------------------------------------------------------
                    def complete_audit(packet_payload, universal_aif_row):
                        """
                        This is what we are really here to do.
                        We have a UDP packet response from the inverter after we sent it a query,
                        so now we have to extract the information we want out of it, and
                        compare it against the database like we said we would.
                        :param udp_packet: A TLV-formatted response from the inverter following a QUERY request sent to it
                        :return: Nothing
                        """
                        (tlv_id_list, tlv_values_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(packet_payload)
                        this_database_row_list = gu.get_db_inverters_row_data(self.inverter_ip_address)

                        tlv_utils.TLVTranslator.verify_query_data_against_db(tlv_id_list, tlv_values_list,
                                                                   this_database_row_list, universal_aif_row,
                                                                   results_file_handle,
                                                                   self.number_of_inverters_audited_so_far, self.inverter_ip_address,
                                                                   self.inverter_string_id, self.inverter_string_position)
                    # end def
                    SG424_data = gu.send_and_get_query_data(self.inverter_ip_address)
                    if SG424_data:
                        # Response received from the inverter. Compare the data against that in the database.
                        complete_audit(SG424_data, universal_aif_row)
                    else:
                        # Try just once more. Sleep not required, it's dne by the send/get routine.
                        SG424_data = gu.send_and_get_query_data(self.inverter_ip_address)
                        if SG424_data:
                            # Response received from the inverter on this 2nd attempt. Use this response and continue.
                            complete_audit(SG424_data, universal_aif_row)
                        else:
                            # 2 tries, no response. Write NO RESPONSE against this inverter. Set the database COMM field to NO_COMM.
                            results_file_handle.write( "No response from " + str(self.inverter_ip_address) + LINE_TERMINATION)
                            gu.update_inverters_comm_column_by_ip(INVERTERS_TABLE_COMM_STRING_NO_COMM, self.inverter_ip_address)
                    # ------------------------------------------------------------------------------------------------------

            else:
                this_string = "IP %s BYPASSED - IT IS NOT UNDER GATEWAY CONTROL" % (self.inverter_ip_address)
                results_file_handle.write(this_string + LINE_TERMINATION)

            # Update progress indicator.
            self.update_progress_indicator(progress_display_title)

        # ... for

        #
        #
        # END OF THIS AUDIT
        #
        # -----------------------------------------------------------------------------------------

        # -----------------------------------------------------------------------------------------
        #
        # POSTAMBLE(?):
        #
        #
        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        this_string = "DICA: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        results_file_handle.write(this_string + LINE_TERMINATION)
        this_string += ", RESULTS WRITTEN TO %s" % (DICA_RESULTS_PATH_AND_FILE_NAME)
        print(this_string)

        # Close the results file.
        results_file_handle.close()
    #
    #
    # -----------------------------------------------------------------------------------------


    def inverters_power_status_audit(self):

    # Synopsis: go through the entire inverters table, and for each entry:
    #
    #           - read the inverters database table,
    #             for each entry:
    #                 - send a TLV-encoded query to the indicated IP
    #                 - wait for a response
    #                 - check the TLV contained in the packet
    #                 - for each TLV, compare to that component in the database
    #                 - if OK, then "no news is good news"
    #                 - else: write to the results file

    # -----------------------------------------------------------------------------------------
    #
    # PREAMBLE:
    #
    # This facility will query 1 or more inverters, and write free-format text to a log file.
    # This file can be pre-delete by specifying the "delete" argument when calling function
    #
    # This facility called with 0 to 3 arguments:
    #
    # 0 args - don't delete file, IPs from Database and no inter query message delay
    #

        query_list_origin = QUERY_LIST_FROM_DATABASE
        audit_start_utc = int(time.time())
        psa_util = PsaUtil(self.my_logger)
        cur_run_mode = psa_util.psa_get_run_mode()

        decode, mcastAll, mcastHos, deleteFile, delayTime, newArgs = self.psa_parse_cmd_line_delete_delay(sys.argv)

        if len(newArgs) == 3:
            # The next option is a comma-separated list of IP addresses.
            ip_list = newArgs[2]
            query_list_origin = QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST

        elif len(newArgs) > 3:
            print("Too many user arguments ...")
            self.print_psa_help()
            return

        if delayTime < 0.0:
            print ("Bad delay time value")
            self.print_psa_help()
            return

        if deleteFile == True:
            terminal_line_command = "sudo rm " + PSA_GPTS_RESULTS_PATH_AND_FILE_NAME
            donut_care_return = os.system(terminal_line_command)
            print("GPTS PSA: file %s deleted before start" % (PSA_GPTS_RESULTS_PATH_AND_FILE_NAME))

        # First things first: open the text file into which the results of the audit will be written.
        try:
            results_file_handle = open(PSA_GPTS_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception:  # as err_string:
            print("GPTS PSA: FAILED TO OPEN %s FILE" % (PSA_GPTS_RESULTS_PATH_AND_FILE_NAME))
            return

        #
        # END OF PREAMBLE
        #
        # -----------------------------------------------------------------------------------------

        if (query_list_origin == QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST):
            # IP list is supplied by the user.
            separated_ip_list = ip_list.split(",")
            if len(separated_ip_list) == 0:
                # Really?
                print("HOW WAS I GIVEN A ZERO-LENGTH IP LIST?")
                return

            # Check each IP in the list: validity, non-gateway group membership.
            for this_ip in separated_ip_list:
                # First check that each IP in the list is seemingly valid.
                if not gu.is_ip_looka_lika_unicast(this_ip):
                    # Does not look like an IP address. Drop or quit? Let's just quit.
                    print("IP %s DOES NOT LOOKA LIKA IP, SO SKIPPING" % (this_ip))
                    separated_ip_list.remove(this_ip)
                else:
                    # Seemingly valid-looking IP. Is it a member of a non-gateway group?
                    if not gu.is_gateway_control_set_against_this_ip(this_ip):
                        # Oooops - GTW_CTRL NOT set against this inverter, it is non-gateway. No PSA audit, please!
                        print("IP %s IS IN NON-GATEWAY GROUP, SO SKIPPING" % (this_ip))
                        separated_ip_list.remove(this_ip)

            count, inverter_id_list = psa_util.create_inverters_id_list_from_list(separated_ip_list)
        else:
            count, inverter_id_list = psa_util.create_inverters_id_list()

        if count == 0:
            print("No matchng inverters found in database")
            return
        # -----------------------------------------------------------------------------------------
        # Setup based on parameters done. Query inverters based on constructed inverters_data_list.
        #

        if (mcastAll or mcastHos):
            if mcastAll:
                ts = "GPTS PSA: POWER STATUS AUDIT Multicast All "
            else:
                ts = "GPTS PSA: POWER STATUS AUDIT Multicast HOS "

            print(ts)
            results_file_handle.write(ts + LINE_TERMINATION)
        else:
            ts = "GPTS PSA: POWER STATUS AUDIT ON " + str(len(inverter_id_list)) + " ENTRIES FROM "
            if query_list_origin == QUERY_LIST_FROM_DATABASE:
                ts += "THE DATABASE"
            elif query_list_origin == QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST:
                ts += "COMMA_SEPARATED LIST"
            else:
                print(ts)
                print("HEY, I MESSED UP, I DUNNO WHERE IP IS COMING FROM!!!")
                return

            print(ts)
            results_file_handle.write(ts + LINE_TERMINATION)

            # Setup data for progress indicator.
            self.number_of_inverters_to_audit = len(inverter_id_list)
            self.init_data_for_progress_indicator()

        powerLabelStr, powerItemStr = TlvPsaPower.dumpHeaderStrings()
        latchedLabelStr, latchedItemStr = TlvPsaStatusLatched.dumpHeaderStrings()
        currentLabelStr, currentItemStr = TlvPsaStatusCurrent.dumpHeaderStrings()

        hdrTimeString = time.strftime("%Y-%m-%d %H:%M:%S") + " "
        hdrString = hdrTimeString + \
                "{:<33}".format(" ") + \
                powerLabelStr + \
                latchedLabelStr + \
                currentLabelStr

        results_file_handle.write(hdrString + LINE_TERMINATION)

        hdrString = hdrTimeString + \
                "{:<16s}".format("IP Address") + \
                "{:<13s}".format("serial num") + \
                "{:<4s}".format("mode") + \
                powerItemStr + \
                latchedItemStr + \
                currentItemStr

        results_file_handle.write(hdrString + LINE_TERMINATION)

        if (mcastAll or mcastHos):
            if ((cur_run_mode == PSA_RUN_MODE_MCAST_ALL) or
                (cur_run_mode == PSA_RUN_MODE_MCAST_HOS)):
                print("HEY, PSA DAEMON IN MCAST MODE - EXITING")
                return

            if mcastAll:
                runMode = PSA_RUN_MODE_MCAST_ALL
            else:
                runMode = PSA_RUN_MODE_MCAST_HOS

            mcastMsg = PsaMulticastMessage(self.my_logger,
                                       results_file_handle,
                                       runMode,
                                       GTW_TO_SG424_PSA_POWER_ALL_STATUS_U16_TLV_TYPE,
                                       psa_util)
            mcastMsg.send()

        else:
            ucastMsg = PsaUnicastMessage(self.my_logger,
                                         results_file_handle,
                                         inverter_id_list,
                                         delayTime,
                                         GTW_TO_SG424_PSA_POWER_ALL_STATUS_U16_TLV_TYPE,
                                         psa_util,
                                         (decode and (len(inverter_id_list) == 1)))

            if decode and (len(inverter_id_list) == 1):
                ucastMsg.startSendOne(inverter_id_list[0])
            else:
                ucastMsg.startSendAll()

        #
        #
        # END OF THIS AUDIT
        #
        # -----------------------------------------------------------------------------------------

        # -----------------------------------------------------------------------------------------
        #
        # POSTAMBLE(?):
        #
        #
        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        ts = "PSA: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        results_file_handle.write(ts + LINE_TERMINATION)
        ts += ", RESULTS WRITTEN TO %s" % (PSA_GPTS_RESULTS_PATH_AND_FILE_NAME)
        print(ts)

        # Close the results file.
        results_file_handle.close()
    #
    #

        return


    def send_query_iqm(self):

        query = GptsTlvQuery(sys.argv)
        query.send()
        return

    def send_query_packed_counters(self):

        query = GptsCounterQuery(sys.argv)
        query.get_inverter_counters()
        return






    def quick_inverters_ping(self):

        # Send a multi-casted query inverter, and wait for as many responses as possible.
        # Collect the IP addresses of all those that responded, and write a list of
        # comma-separated IP addresses to standard output. Only 1 user argument.
        number_of_user_tlv = 1
        dummy_data = 1234
        packed_data = gu.add_master_tlv(number_of_user_tlv)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_QUERY_INVERTER_U16_TLV_TYPE, dummy_data)

        if len(sys.argv) == 3:
            user_arg = sys.argv[2]
            user_arg = user_arg.upper()
            if user_arg == "ALL":
                inverter_type = INVERTER_MCAST_SG424_TYPE_ALL
            elif user_arg == "SOLAR":
                inverter_type = INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY
            elif user_arg == "BATTERY":
                inverter_type = INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY
            else:
                print("INVALID INVERTER TYPE SPECIFIED")
                self.print_qip_help()
                return
        else:
            print("INVALID NUMBER OF ARGUMENTS")
            self.print_qip_help()
            return

        multicast_ip = gu.construct_multicast_ip_gen2(inverter_type, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
        print("SENT MULTICAST %s QIP FOR INVERTER TYPE %s" % (multicast_ip, inverter_type))

        if gu.is_send_multicast_packet_and_close_socket(packed_data, multicast_ip, SG424_PRIVATE_UDP_PORT):
            # Multicast packet sent, now setup for listening.
            #
            # NOTE: multicasted TLV command already on its way towards the field of inverters, but this method is
            #       not yet setup for listening! This is being setup right now. Testing shows that the following
            #       setup, binding and listening will happen long before a single inverter responds, so this is
            #       "safe". Stricktly speaking though, this is cause for discomfort. This has already been noted
            #       with the "get_inverter_query_data_by_mac_or_sn" facility.
            gateway_lan_address = gu.get_gateway_ip_address(GATEWAY_LAN_INTERFACE_NAME)
            try:
                clean_socks = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
                clean_socks.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                clean_socks.settimeout(7.0)
            except Exception as error:
                print("socket.socket/setsockopt failed (listening) ...")
            else:
                # Bind socket to local host and "well-known" port
                try:
                    clean_socks.bind((gateway_lan_address, GATEWAY_QUERY_RESPONSE_PORT))
                except Exception:  # as error:
                    print("bind failed ...")
                else:

                    # Wait for a response. Print a marker consisting of ||| at the beginning and end so that the
                    # GUI tool can grab the IP (if any). Don't print while waiting until the process is done.
                    still_wiggling = 2
                    number_of_responses_received = 0
                    print("WAITING ON PORT %s FOR RESPONSES FROM THE FOLLOWING ..." % (GATEWAY_QUERY_RESPONSE_PORT))
                    print("|||")
                    while still_wiggling > 0:
                        try:
                            private_port_packet = clean_socks.recvfrom(4096)
                        except socket.timeout:
                            # Timed out waiting to receive a packet. This is normal, so wiggle a little less.
                            still_wiggling -= 1
                            if still_wiggling > 0:
                                pass # print("No response, wait a bit longer ...")
                            else:
                                pass # print("Still no other responses, no more waiting ...")
                            continue
                        except Exception as error:
                            print("Socket receive error - %s" % error)
                            break
                        else:

                            # Packet received. Just print the IP address.
                            (ip_address, ip_address_port, SG424_data) = gu.get_udp_components(private_port_packet)
                            (query_tlv_list, query_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                            number_of_responses_received += 1
                            print("%s" % ip_address)

                    # while ...

                    # Close the socket.
                    clean_socks.close()

                    print("|||")
                    print("DONE - RECEIVED %s INVERTER RESPONSES" % (number_of_responses_received))

        else:
            print("FAILED TO END/CLOSE A MULTICAST PACKET")


    def init_inverter_debug_counters(self):

        # Does the user want to target inverters based on comma-separated IP list, or ALL inverters?
        inverters_ip_list = []
        argument_list = []
        if len(sys.argv) == 3:
            # By IP.
            comma_separated_list = sys.argv[2]
            argument_list = comma_separated_list.split(',')
            print("INIT DEBUG COUNTERS BASED ON COMMA-SEPARATED IP LIST OF LENGTH %s" % (len(argument_list)))
            for this_ip in argument_list:
                if gu.is_ip_looka_lika_unicast(this_ip):
                    inverters_ip_list.append(this_ip)
                else:
                    print("IP %s smells funny ... not included" % (this_ip))
        else:
            multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            print("INIT DEBUG COUNTERS BASED ON MULTICAST TO ALL")
            inverters_ip_list.append(multicast_ip)

        # Prepare the TLV command with dummy number of 1234
        packed_data = gu.add_master_tlv(1)
        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_INIT_DEBUG_COUNTERS_U16_TLV_TYPE, 1234)

        # Send to all IPs in the list.
        for this_ip in inverters_ip_list:
            send_result = gu.is_send_tlv_packed_data(this_ip, packed_data)
            if send_result:
                print("SENT INIT DEBUG TO %s" % (this_ip))
            else:
                print("*FAILED* TO SEND INIT DEBUG TO %s" % (this_ip))

    def init_data_for_progress_indicator(self):
        # For progress indication, start with 0 percent.
        #self.number_of_inverters_to_audit <- setup by caller of this function.
        self.number_of_inverters_audited_so_far = 0
        self.percentage_audit_progress = 0
        self.last_percentage_audit_progress_written = 0
        self.next_expected_percentage_for_display = 10

    def update_progress_indicator(self, progress_display_title):
        self.percentage_audit_progress = (self.number_of_inverters_audited_so_far * 100) / self.number_of_inverters_to_audit

        # Print a notice for every 10% of progress.
        if self.last_percentage_audit_progress_written != self.percentage_audit_progress:
            # Not the same. Is the percentage just written a multiple of 10?
            this_modulus = self.percentage_audit_progress % 10
            print_this_progress = False
            if this_modulus == 0:
                # A multiple of 10 percent.
                print_this_progress = True
            elif self.percentage_audit_progress > self.next_expected_percentage_for_display:
                # Not a multiple of 10%, but it did go past the next expected multiple of 10%.
                # Re-calculate to get and display an exact multiple of 10%.
                this_number = self.percentage_audit_progress / 10
                self.percentage_audit_progress = this_number * 10
                print_this_progress = True
            if print_this_progress:
                print(progress_display_title + str(self.percentage_audit_progress) + "% of completion")
                self.last_percentage_audit_progress_written = self.percentage_audit_progress
                self.next_expected_percentage_for_display += 10

    def get_inverters_debug_counters(self):

        # Synopsis: get the RAM-based debug counters from 1 or more SG424 inverters.
        #
        # Command is entered in the following forms:
        #
        # gpts.py gidc                         ... 2 parameters: no delete, get from all IP from the database
        # gpts.py gidc delete                  ... 3 parameters: delete results file, get from all IP from the database
        # gpts.py gidc <some IP list>          ... 3 parameters: no delete, get from comma-separatd IP list
        # gpts.py gidc delete <some IP list>   ... 4 parameters: delete, get from comma-separatd IP list
        # gpts.py gidc delete <some IP list> delay=1.5  ... 5 parameters: delete, get from comma-separatd IP list,
        #                                                                 delay 1.5 sec between queries

        audit_start_utc = int(time.time())
        current_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        progress_display_title = "Get Counters progress: "
        comma_separated_ip_list = ""
        query_list_origin = QUERY_LIST_FROM_DATABASE

        deleteFile, delayTime, newArgs = self.parse_cmd_line_delete_delay(sys.argv)

        if len(newArgs) == 3:
            # The next option is a comma-separated list of IP addresses.
            comma_separated_ip_list = newArgs[2]
            query_list_origin = QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST

        elif len(newArgs) > 3:
            print("Too many user arguments ...")
            self.print_gidc_help()
            return

        if delayTime < 0.0:
            print ("Bad delay time value")
            self.print_gidc_help()
            return

        if deleteFile == True:
            terminal_line_command = "sudo rm " + GIDC_RESULTS_PATH_AND_FILE_NAME
            donut_care_return = os.system(terminal_line_command)
            print("GIDC: file %s deleted before start" % (GIDC_RESULTS_PATH_AND_FILE_NAME))

        # First things first: open the text file into which the results of the audit will be written.
        try:
            results_file_handle = open(GIDC_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            print("FAILED TO OPEN " + GIDC_RESULTS_PATH_AND_FILE_NAME + " FILE")
            return

        this_string = "GIDC: GET INVERTERS DEBUG COUNTERS LAUNCHED AT %s" % (current_dt)
        # For the terminal:
        print(this_string)

        # For the results file:
        this_string = "---------------------------------------------------------------\r\n" + this_string
        results_file_handle.write(this_string + LINE_TERMINATION)

        # ---------------------------------------------------------------------------
        #
        # *** CORE OF THIS AUDIT'S PROCESSING ...
        #
        # Extract all rows from the inverters table to be audited.
        inverter_ip_list = []
        inverter_version_list = []
        inverter_string_id_ist = []
        inverter_string_position_list = []
        inverter_feed_name_list = []

        if query_list_origin == QUERY_LIST_FROM_DATABASE:
            select_string = "SELECT %s,%s,%s,%s,%s FROM %s order by %s,%s;" %\
                                                   (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME,
                                                    INVERTERS_TABLE_VERSION_COLUMN_NAME,
                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                    INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                                    INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                                    DB_INVERTER_NXO_TABLE,
                                                    INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                                    INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME)
            inverters_db = prepared_act_on_database(FETCH_ALL, select_string, ())
            for each_inverter in inverters_db:
                inverter_ip_list.append(each_inverter[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])
                if each_inverter[INVERTERS_TABLE_VERSION_COLUMN_NAME]:
                    inverter_version_list.append(each_inverter[INVERTERS_TABLE_VERSION_COLUMN_NAME])
                else:
                    inverter_version_list.append("NULL")
                inverter_string_id_ist.append(each_inverter[INVERTERS_TABLE_STRING_ID_COLUMN_NAME])
                inverter_string_position_list.append(each_inverter[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME])
                inverter_feed_name_list.append(each_inverter[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME])

        else:
            # IP list is supplied by the user.
            query_list_origin = QUERY_LIST_FROM_COMMA_SEPARATED_IP_LIST
            ip_argument_list = comma_separated_ip_list.split(',')
            for this_one_ip in ip_argument_list:
                if gu.is_ip_looka_lika_unicast(this_one_ip):
                    if not gu.is_this_ip_in_database(this_one_ip):
                        print("IP %s NOT IN DATABASE - WILL QUERY NEVERTHELESS" % (this_one_ip))
                        inverter_ip_list.append(this_one_ip)
                        inverter_version_list.append("NULL")
                        inverter_string_id_ist.append("xx:xx:xx")
                        inverter_string_position_list.append(99)
                        inverter_feed_name_list.append("ZZZ")
                    else:
                        select_string = "SELECT %s,%s,%s,%s FROM %s WHERE %s=%s;" % \
                                        (INVERTERS_TABLE_VERSION_COLUMN_NAME,
                                         INVERTERS_TABLE_STRING_ID_COLUMN_NAME,
                                         INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME,
                                         INVERTERS_TABLE_FEED_NAME_COLUMN_NAME,
                                         DB_INVERTER_NXO_TABLE,
                                         INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
                        select_arg = (this_one_ip,)
                        one_inverters_entry = prepared_act_on_database(FETCH_ONE, select_string, (select_arg))
                        if one_inverters_entry:
                            print("IP %s TO BE QUERIED FOR COUNTERS" % (this_one_ip))
                            inverter_ip_list.append(this_one_ip)
                            if one_inverters_entry[INVERTERS_TABLE_VERSION_COLUMN_NAME]:
                                inverter_version_list.append(one_inverters_entry[INVERTERS_TABLE_VERSION_COLUMN_NAME])
                            else:
                                inverter_version_list.append("NULL")
                            inverter_string_id_ist.append(one_inverters_entry[INVERTERS_TABLE_STRING_ID_COLUMN_NAME])
                            inverter_string_position_list.append(one_inverters_entry[INVERTERS_TABLE_STRING_POSITION_COLUMN_NAME])
                            inverter_feed_name_list.append(one_inverters_entry[INVERTERS_TABLE_FEED_NAME_COLUMN_NAME])
                        else:
                            # ???
                            print("")

        # --------------------------------------------------------------------------------------------------------------
        self.number_of_inverters_to_audit = len(inverter_ip_list)
        this_string = "GIDC: GET DEBUG COUNTERS FROM %s INVERTERS" % (self.number_of_inverters_to_audit)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)
        self.excel_index = 0

        # Setup data for progress indicator.
        self.init_data_for_progress_indicator()

        for iggy in range (0, self.number_of_inverters_to_audit):
            self.number_of_inverters_audited_so_far += 1
            self.excel_index += 1
            self.get_debug_counters_response_received = False

            self.inverter_ip_address = inverter_ip_list[iggy]
            self.db_inverter_app_version = inverter_version_list[iggy]
            self.inverter_string_id = inverter_string_id_ist[iggy]
            self.inverter_string_position = inverter_string_position_list[iggy]
            self.inverter_feed_name = inverter_feed_name_list[iggy]
            comma_separated_debug_counters_string = gu.get_inverter_debug_counters_data(self.inverter_ip_address)

            # Each IP has this line of data:
            this_string = "- EXCEL,%s,%s,%s,%s,%s,%s" % (self.excel_index,
                                                         self.inverter_ip_address,
                                                         self.inverter_feed_name,
                                                         self.inverter_string_id,
                                                         self.inverter_string_position,
                                                         self.db_inverter_app_version)

            if comma_separated_debug_counters_string:
                this_string += comma_separated_debug_counters_string
            else:
                # No response received from this inverter.
                this_string += ",NO_RESPONSE"
            results_file_handle.write(this_string + LINE_TERMINATION)

            if delayTime > 0:
                time.sleep(delayTime)

            # Update progress indicator.
            self.update_progress_indicator(progress_display_title)
        # ... for

        #
        #
        # END OF THIS AUDIT
        #
        # ---------------------------------------------------------------------------

        audit_end_utc = int(time.time())
        elapsed_time = audit_end_utc - audit_start_utc
        completion_dt = time.strftime("%Y-%m-%d %H:%M:%S")
        this_string = "GIDC: COMPLETED: " + completion_dt + " (elapsed UTC = " + str(elapsed_time) + ")"

        results_file_handle.write(this_string + LINE_TERMINATION)
        this_string += ", RESULTS WRITTEN TO " + GIDC_RESULTS_PATH_AND_FILE_NAME
        print(this_string)

        # Close the results file.
        results_file_handle.close()

    def send_dynamic_power_factor_command(self):

        # Arguments consist of 1, 2 or 3 sets of feed/pf/pf_c_lead values (no IP address, since it is sent to the "ALL"
        # inverters multicast group). When specifying 1, 2 or 3 feeds, the total number of arguments must be exactly 4,
        # 8 or 10:
        #
        # 1 feed:      sudo python gpts.py dynamic_pf A 0.777 0                        <-  3 variables + 1 =  4 args
        # 2 feeds:     sudo python gpts.py dynamic_pf A 0.777 0 B 0.888 1              <-  6 variables + 1 =  7 args
        # 3 feeds:     sudo python gpts.py dynamic_pf A 0.777 1 B 0.888 0 C 0.999 1    <-  9 variables + 1 = 10 args
        # "ALL" feeds: sudo python gpts.py dynamic_pf ALL 0.777 1                      <-  3 variables + 1 =  4 args
        if self.num_of_user_args == 4 or self.num_of_user_args == 7 or self.num_of_user_args == 10:

            number_of_feeds = (self.num_of_user_args - 1) / 3
            arg_index = 1
            user_packed_data = ""
            number_of_user_tlv = 0

            this_string = "PREP/SEND DYNAMIC PF PACKET FOR " +  str(number_of_feeds) + " FEEDS:"

            for iggy in range (0, number_of_feeds):

                # 1st argument in group is the feed:
                arg_index += 1
                this_feed = sys.argv[arg_index]
                this_feed = this_feed.upper()

                # 2nd argument in group is the power factor (get its float value for validation):
                arg_index += 1
                power_factor_as_string = sys.argv[arg_index]
                pf_as_float = float(power_factor_as_string)

                # 3rd argument in the group is the "power factor current leading 0/1":
                arg_index += 1
                power_factor_current_leading = int(sys.argv[arg_index])

                # Verification of data:
                if this_feed == FEED_ALL_LITERAL or this_feed == FEED_A_LITERAL or this_feed == FEED_B_LITERAL or this_feed == FEED_C_LITERAL:
                    if this_feed == FEED_ALL_LITERAL:
                        if self.num_of_user_args == 4:
                            # When "ALL" is specified, should only have 4 arguments.
                            power_factor_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_ALL_F32_TLV_TYPE
                            power_factor_current_leading_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_ALL_LEADING_U16_TLV_TYPE
                        else:
                            print("ALL Feeds specified, but argument count incorrect")
                            return
                    elif this_feed == FEED_A_LITERAL:
                        power_factor_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_F32_TLV_TYPE
                        power_factor_current_leading_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_A_LEADING_U16_TLV_TYPE
                    elif this_feed == FEED_B_LITERAL:
                        power_factor_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_F32_TLV_TYPE
                        power_factor_current_leading_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_B_LEADING_U16_TLV_TYPE
                    else:
                        # Only FEED_C_LITERAL remains, so ...
                        power_factor_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_F32_TLV_TYPE
                        power_factor_current_leading_tlv = GTW_TO_SG424_DYNAMIC_POWER_FACTOR_FEED_C_LEADING_U16_TLV_TYPE

                    if pf_as_float <= 1.0 and pf_as_float >= 0.707:
                        # Power factor "current leading" must be 0 or 1:
                        if power_factor_current_leading == 0 or power_factor_current_leading == 1:
                            # Good to go. Add the data to the TLV.
                            this_string += " " + this_feed + "/" + power_factor_as_string + "/" + str(power_factor_current_leading)
                            number_of_user_tlv += 2
                            user_packed_data = gu.append_dynamic_power_factor(user_packed_data, power_factor_tlv, pf_as_float,
                                                                              power_factor_current_leading_tlv, power_factor_current_leading)
                        else:
                            print("Good things might happen by entering a PF current leading value of 0 or 1")
                            return
                    else:
                        print("I'd be pleased if you entered Pf as a float between 1.0 and 0.707")
                        return
                else:
                    print("BACK TO KINDERGARDEN FOR YOU, TO LEARN THAT FEED MUST BE A, B, C or ALL")
                    return

            # Got here? Then so far so good. Construct the overall power factor TLV packet and send it.
            multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            this_string += " -> " + multicast_ip
            print(this_string)
            master_packed_data = gu.add_master_tlv(number_of_user_tlv)
            total_packed_data = master_packed_data + user_packed_data
            # print_this_hex_data(total_packed_data) # <- to display raw TLV HEX-encoded data for debug/analysis.
            if not gu.is_send_tlv_packed_data(multicast_ip, total_packed_data):
                print("BUT FAILED TO SEND ...")

        else:
            print("THERE MUST BE 3, 6 or 9 VARIABLES (i.e., 1, 2 or 3 sets of FEED PF CURR_LEAD)")

    def inverter_control_bit_update(self):
        # gpts.py inv_ctrl <gateway|low_power|sleep|all> <SET|CLEAR> <comma-separated thingies>
        #   [0]      [1]              [2]                    [3]             [4]
        if len(sys.argv) != 5:
            print("ARG, MATEY ... INVALID NUMBER OF ARGS!")
            self.print_set_clear_inv_ctrl_help()
            return

        # Retrieve the arguments:
        inverter_control_bit = sys.argv[2]
        inverter_control_bit = inverter_control_bit.upper()

        set_or_clear_as_string = sys.argv[3]
        set_or_clear_as_string = set_or_clear_as_string.upper()

        comma_separated_string = sys.argv[4]

        # Validate arguments. First validate the inverter control bit of interest.
        if inverter_control_bit == INVERTER_CONTROL_BIT_GATEWAY:
            tlv_type = GTW_TO_SG424_UPDATE_INV_CTRL_GATEWAY_CONTROL_U16_TLV_TYPE
        elif inverter_control_bit == INVERTER_CONTROL_BIT_LOW_POWER:
            tlv_type = GTW_TO_SG424_UPDATE_INV_CTRL_LOW_POWER_U16_TLV_TYPE
        elif inverter_control_bit == INVERTER_CONTROL_BIT_SLEEP:
            tlv_type = GTW_TO_SG424_UPDATE_INV_CTRL_SLEEP_U16_TLV_TYPE
        elif inverter_control_bit == INVERTER_CONTROL_BIT_ALL:
            # NOTE: only the CLEAR action is available for ALL, checked below.
            tlv_type = GTW_TO_SG424_CLEAR_ALL_INV_CTRL_BITS_U16_TLV_TYPE
        else:
            print("BAD BAD 1ST PARAM ...")
            self.print_set_clear_inv_ctrl_help()
            return

        # Validate the action to set or clear:
        if set_or_clear_as_string == "SET":
            set_clear_value = 1
        elif set_or_clear_as_string == "CLEAR":
            set_clear_value = 0
        else:
            print("BAD BAD 2ND PARAM ...")
            self.print_set_clear_inv_ctrl_help()
            return

        # So far so good. Before proceeding with the update, ensure that if the user specified "ALL" bits,
        # that the action was a CLEAR only: rule is you cannot SET ALL INV_CTRL BITS.
        if (set_clear_value == 1) and (tlv_type == GTW_TO_SG424_CLEAR_ALL_INV_CTRL_BITS_U16_TLV_TYPE):
            # Attempt to set all bits ...
            print("IT IS A NO-NO TO SET ALL INV_CTRL BITS, ALL CAN ONLY BE SPECIFIED TO CLEAR")
            self.print_set_clear_inv_ctrl_help()
            return

        # Lastly, validate the "comma-separated list of thingies".
        ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)

        # Now send.
        if ip_list:
            for each_ip in ip_list:
                if gu.is_sent_inv_ctrl_update_bit_by_ip(each_ip, tlv_type, set_clear_value):
                    print("%s -> SENT TLV/value %s/%s" % (each_ip, tlv_type, set_clear_value))
                else:
                    print("%s -> *SENDTO FAILED*" % (each_ip))
        else:
            print("HEY - comma-separated list of thingies did not contain anything!")

    '''
    NOT IMPLEMENTED UNTIL A REASONABLE LIST OF REQUIREMENTS HAS BEEN DEFINED.
    def irradiance_inverter_setup(self):
        if self.num_of_user_args == 3:
            # Retrieve the user's 2 arguments. The first one must be either "ADD" or "DELETE",
            # the 2nd must be a reasonably good looking unicast IP address.
            inverter_action = sys.argv[2]
            inverter_action = inverter_action.upper()
            ip_address = sys.argv[3]
            inv_ctrl_irra_action_bit = 0

            # Validate the 1st argument:
            if inverter_action == "ADD":
                inv_ctrl_irra_action_bit = 1
            elif inverter_action == "DELETE":
                inv_ctrl_irra_action_bit = 0
            else:
                print("INVALID 1ST ARG, EXPECTING ADD OR DELETE ...")
                return

            # Validate the 2nd argument:
            if not is_ip_looka_lika_unicast(ip_address):
                print("NOT A PARTICULARLY GOOD LOOKING IP ...")
                return

            # Arguments are good. Proceed with setup.
            number_of_user_tlv = 0
            irra_inverter_ip_list = get_irra_inverter_ip_list()
            max_irra_inverters_allowed = get_max_irra_inverters_from_irra_control()

            if inverter_action == "ADD":
                if ip_address in irra_inverter_ip_list:
                    print("IP %s IN irradiance_inverters TABLE, NOTHING TO ADD" % ip_address)
                else:
                    # Check if the irradiance inverters table is less than max allowed.
                    if len(irra_inverter_ip_list) < max_irra_inverters_allowed:
                        number_of_user_tlv = 1
                        packed_data = gu.add_master_tlv(number_of_user_tlv)
                        packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data,
                                                                              GTW_TO_SG424_INV_CTRL_UPDATE_IRRADIANCE_BIT_U16_TLV_TYPE,
                                                                              inv_ctrl_irra_action_bit)
                    else:
                        print("IRRADIANCE_INVERTERS TABLE AT MAX CAPACITY")
            else:
                # Check if the IP to delete is in the IP irradiance table.
                if ip_address in irra_inverter_ip_list:
                    number_of_user_tlv = 1
                    packed_data = gu.add_master_tlv(number_of_user_tlv)
                    packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data,
                                                                          GTW_TO_SG424_INV_CTRL_UPDATE_IRRADIANCE_BIT_U16_TLV_TYPE,
                                                                          inv_ctrl_irra_action_bit)
                else:
                    print("IP %s NOT IN IRRA TABLE, NOTHING TO DELETE" % (ip_address))

            # Now to do actual work:
            if number_of_user_tlv > 0:
                if is_update_and_query_irradiance(ip_address, packed_data, inv_ctrl_irra_action_bit):
                    # The action  and query OK, now update the database.
                    execute_on_irradiance_inverters_table(inverter_action, ip_address)
                else:
                    print("IP %s: UPDATE/QUERY FAILED, NO DATABASE UPDATE" % (ip_address))

        else:
            print("ARG, MATEY, INVALID NUMBER OF ARGS ...")
    '''

    def get_aif_immed_data(self):
        # Only 1 user argument is expected: a comma-separated list of thingies.
        # Therefore only 1 user argument is expected.
        #
        #                    [0]       [1]                      [2]
        # Command format: gpts.py get_aif_settings <ALL | SOLAR | BATTERY | comma-separated thingies>
        ip_list = []
        number_of_user_args = len(sys.argv) - 2

        if number_of_user_args != 1:
            print("GET_AIF_IMMED: 1 ARG ONLY PLEASE")

        comma_separated_string = sys.argv[2]
        ip_list = []
        comma_separated_string = comma_separated_string.upper()
        if comma_separated_string == "ALL":
            ip_list = gu.get_database_total_inverters_ip_list()
        elif comma_separated_string == "SOLAR":
            ip_list = gu.get_database_solar_inverters_ip_list()
        elif comma_separated_string == "BATTERY":
            ip_list = gu.get_database_battery_inverters_ip_list()
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)

        for each_ip in ip_list:
            if gu.is_ip_looka_lika_unicast(each_ip):
                (aif_immed_tlv_id_list, aif_immed_values_list) = gu.get_aif_immed_data_by_ip(each_ip)
                if (len(aif_immed_tlv_id_list) == len(aif_immed_values_list)) and len(aif_immed_tlv_id_list) > 0:
                    # Good to print.
                    print("%s ... AIF IMMED DATA (%s components):" % (each_ip, len(aif_immed_tlv_id_list)))
                    gu.print_inverter_tlv_id_and_data_list(aif_immed_tlv_id_list, aif_immed_values_list)
                else:
                    # Either unequal lengths, or no response.
                    if len(aif_immed_tlv_id_list) != len(aif_immed_values_list):
                        print("UNEXPECTED UNEQUAL ID/VALUES LIST LENGTHS = %s/%s" % (len(aif_immed_tlv_id_list), len(aif_immed_values_list)))
                    else:
                        # Zero length means no response.
                        print("%s -> NO_RESPONSE" % (each_ip))
            else:
                print("IP %s ... not looking like a unicast of any sorts" % (each_ip))

    def get_aif_settings_data(self):
        # Only 1 user argument is expected: a comma-separated list of thingies.
        # Therefore only 1 user argument is expected.
        #
        #                    [0]       [1]                      [2]
        # Command format: gpts.py get_aif_settings <ALL | SOLAR | BATTERY | comma-separated thingies>
        ip_list = []
        number_of_user_args = len(sys.argv) - 2

        if number_of_user_args != 1:
            print("GET_AIF_IMMED: 1 ARG ONLY PLEASE")

        comma_separated_string = sys.argv[2]
        comma_separated_string = comma_separated_string.upper()
        one_arg_only = sys.argv[2]
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)
            # Go through the list and remove any multicast-looking IPs from the list.
            for index, this_ip in enumerate(ip_list):
                if gu.is_ip_looka_lika_multicast(this_ip):
                    # drop this IP from the list.
                    del ip_list[index]

        if len(ip_list) == 0:
            print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
            return

        for each_ip in ip_list:
            if gu.is_ip_looka_lika_unicast(each_ip):
                (aif_settings_tlv_id_list, aif_settings_data_list) = gu.get_aif_settings_data_by_ip(each_ip)
                if (len(aif_settings_tlv_id_list) == len(aif_settings_data_list)) and len(aif_settings_tlv_id_list) > 0:
                    # Good to print.
                    print("%s ... AIF SETTINGS DATA (%s components):" % (each_ip, len(aif_settings_tlv_id_list)))
                    gu.print_inverter_tlv_id_and_data_list(aif_settings_tlv_id_list, aif_settings_data_list)
                else:
                    # Either unequal lengths, or no response.
                    if len(aif_settings_tlv_id_list) != len(aif_settings_data_list):
                        print("UNEXPECTED UNEQUAL ID/VALUES LIST LENGTHS = %s/%s" % (len(aif_settings_tlv_id_list), len(aif_settings_data_list)))
                    else:
                        # Zero length means no response.
                        print("%s -> NO_RESPONSE" % (each_ip))

    def set_aif_immed(self):
        # Parameters come in pairs, with the last MANDATORY parameter being a singular "comma-separated list of thingies".
        # An odd number of parameters is therefore expected.
        ip_list = []
        tlv_id_list = []
        tlv_values_list = []
        aif_immed_update_column_name_list = []
        aif_immed_update_column_value_list = []

        total_number_of_arguments = len(sys.argv)
        number_of_user_arguments = total_number_of_arguments - 2

        # To update at a minimum 1 parameter, must have at least 4 arguments.
        # If CONNECT is specified, update the database (it's the only AIF immed
        # stored in the database).

        remainder = number_of_user_arguments % 2
        number_of_pairs = number_of_user_arguments / 2

        if remainder == 0:
            # Even number of arguments. Not good.
            print("EVEN NUMBER OF ARGUMENTS")
            self.print_set_aif_immed_help()
            return

        #  magic number 3 ... at least 1 AIF IMMED pair + the last comma-separated thingy parameter.
        if (number_of_user_arguments < 3) or (number_of_pairs > NUMBER_OF_MANAGED_AIF_IMMED_PARAMETERS):
            print("NUMBER OF ARGUMENTS IS NOT CORRECT ...")
            self.print_set_aif_immed_help()
            return

        print("WITH %s USER ARGUMENTS, PARSE FOR %s PAIRS" % (number_of_user_arguments, number_of_pairs))
        pop = 2
        for iggy in range (0, number_of_pairs):
            this_argument = sys.argv[pop]
            this_argument = this_argument.upper()

            # Validate the argument.
            if this_argument == CMD_PARAM_AIF_IMMED_CONNECT:
                this_arg_as_string = sys.argv[pop + 1]
                if this_arg_as_string.isdigit():
                    this_value = int(sys.argv[pop + 1])
                    if this_value == 0 or this_value == 1:
                        print("ADD %s/%s TO LIST" % (this_argument, this_value))
                        tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_IN_EEPROM_U16_TLV_TYPE)
                        tlv_values_list.append(this_value)
                        # Database to be updated:
                        aif_immed_update_column_name_list.append(INVERTERS_TABLE_CONNECT_COLUMN_NAME)
                        aif_immed_update_column_value_list.append(this_value)
                    else:
                        print("%s WITH INVALID VALUE, ENTER 0 or 1" % this_argument)
                        break
                else:
                    print("ENTER A DIGIT FOR THE %s ARGUMENT" % CMD_PARAM_AIF_IMMED_CONNECT)
                    break

            elif this_argument == CMD_PARAM_AIF_IMMED_PWR_LIMIT_PCT:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_IMMED_CTRLS_OUTPUT_POWER_LIMIT_PCT_MIN \
                  and this_value <= AIF_IMMED_CTRLS_OUTPUT_POWER_LIMIT_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID RANGE, ENTER: %s->%s" % (this_argument,
                                                               AIF_IMMED_CTRLS_OUTPUT_POWER_LIMIT_PCT_MIN,
                                                               AIF_IMMED_CTRLS_OUTPUT_POWER_LIMIT_PCT_MAX))
                    break
            elif this_argument == CMD_PARAM_AIF_IMMED_PWR_LIMIT_ENABLE:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s WITH INVALID VALUE, ENTER 0 or 1" % this_argument)
                    break
            elif this_argument == CMD_PARAM_AIF_IMMED_POWER_FACTOR:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_IMMED_CTRLS_OUTPUT_POWER_FACTOR_MIN \
                  and this_value <= AIF_IMMED_CTRLS_OUTPUT_POWER_FACTOR_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID RANGE, ENTER: %s->%s" % (this_argument,
                                                               AIF_IMMED_CTRLS_OUTPUT_POWER_FACTOR_MIN,
                                                               AIF_IMMED_CTRLS_OUTPUT_POWER_FACTOR_MAX))
                    break
            elif this_argument == CMD_PARAM_AIF_IMMED_POWER_FACTOR_C_LEAD:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s WITH INVALID VALUE, ENTER 0 or 1" % this_argument)
                    break

            elif this_argument == CMD_PARAM_AIF_IMMED_POWER_FACTOR_ENABLE:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s WITH INVALID VALUE, ENTER 0 or 1" % this_argument)
                    break
            elif this_argument == CMD_PARAM_AIF_IMMED_VAR_LIMIT_PCT:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_IMMED_CTRLS_OUTPUT_VAR_LIMIT_PCT_MIN \
                  and this_value <= AIF_IMMED_CTRLS_OUTPUT_VAR_LIMIT_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID RANGE, ENTER: %s->%s" % (this_argument,
                                                               AIF_IMMED_CTRLS_OUTPUT_VAR_LIMIT_PCT_MIN,
                                                               AIF_IMMED_CTRLS_OUTPUT_VAR_LIMIT_PCT_MAX))
                    break
            elif this_argument == CMD_PARAM_AIF_IMMED_VAR_LIMIT_ENABLE:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s WITH INVALID VALUE, ENTER 0 or 1" % this_argument)
                    break

            else:
                print("BAD ARGUMENT=*%s* - I DON'T UNDERSTAND WHAT YOU WANT ..." % (this_argument))
                self.print_set_aif_immed_help()
                return

            # Get ready for the next argument pair.
            pop += 2

        # ... for

        # Now command inverters with the update. Update the database as necessary.

        comma_separated_string = sys.argv[total_number_of_arguments - 1]
        comma_separated_string = comma_separated_string.upper()
        one_arg_only = comma_separated_string
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)
            # Go through the list and remove any multicast-looking IPs from the list.
            for index, this_ip in enumerate(ip_list):
                if gu.is_ip_looka_lika_multicast(this_ip):
                    # drop this IP from the list.
                    del ip_list[index]

        if ip_list:
            for ip_address in ip_list:
                if gu.is_send_aif_immed_configuration_command(ip_address, tlv_id_list, tlv_values_list): # tlv_packer
                    this_string = "IP -> %s ... DATA PACKED AND SENT OK " % (ip_address)
                    # Update the database if necessary.
                    if (len(aif_immed_update_column_name_list) == len(aif_immed_update_column_value_list)) \
                            and len(aif_immed_update_column_name_list) != 0:
                        # Update the database.
                        gu.update_inverters_aif_immed_column_by_ip(ip_address,
                                                                   aif_immed_update_column_name_list,
                                                                   aif_immed_update_column_value_list)
                        this_string += "(database updated based on IP %s)" % (ip_address)
                    else:
                        this_string += "(database update NOT required)"
                    print(this_string)

                else:
                    this_string = "IP -> %s ... DATA PACKED AND SENT ... ***FAILED***" % (ip_address)
        else:
            print("IP LIST WAS ... EMPTY!!!")

        return

    def set_aif_settings(self):

        # Parameters come in pairs, with the last MANDATORY parameter being a singular "comma-separated list of thingies".
        # An odd number of parameters is therefore expected. There are no database updates w.r.t. AIF SETTINGS. Yet!
        ip_list = []
        tlv_id_list = []
        tlv_values_list = []

        total_number_of_arguments = len(sys.argv)
        number_of_user_arguments = total_number_of_arguments - 2

        # To update at a minimum 1 parameter, must have at least 4 arguments.

        remainder = number_of_user_arguments % 2
        number_of_pairs = number_of_user_arguments / 2

        if remainder == 0:
            # Even number of arguments. Not good.
            print("EVEN NUMBER OF ARGUMENTS")
            self.print_set_aif_settings_help()
            return

        # magic number 3 ... at least 1 AIF SETTING pair + the last comma-separated thingy parameter.
        if (number_of_user_arguments < 3) or (number_of_pairs > NUMBER_OF_MANAGED_AIF_SETTINGS_PARAMETERS):
            print("NUMBER OF ARGUMENTS IS NOT CORRECT ...")
            self.print_set_aif_settings_help()
            return

        pop = 2
        for iggy in range (0, number_of_pairs):
            this_argument = sys.argv[pop]
            this_argument = this_argument.upper()

            if this_argument == CMD_PARAM_AIF_SETTINGS_AC_V_OFFSET:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_AC_VOLTAGE_OFFSET_V_MIN and this_value <= AIF_SETTINGS_AC_VOLTAGE_OFFSET_V_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_AC_VOLTAGE_OFFSET_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_AC_VOLTAGE_OFFSET_V_MIN,
                                                                  AIF_SETTINGS_AC_VOLTAGE_OFFSET_V_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_MAX_OUT_PWR:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_MAX_OUTPUT_POWER_MIN and this_value <= SG424_MAX_POUT_HW_REV9_OR_LATER:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_AC_OUTPUT_POWER_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_MAX_OUTPUT_POWER_MIN,
                                                                  SG424_MAX_POUT_HW_REV9_OR_LATER))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_RAMP_RATE_UP:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_MIN and this_value <= AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_MIN,
                                                                  AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_RAMP_RATE_DOWN:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_MIN and this_value <= AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_MIN,
                                                                  AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_RECONN_RR_PCT:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_MIN and this_value <= AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_MIN,
                                                                  AIF_SETTINGS_RECONNECT_RAMP_RATE_PCT_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_RTS_DEL_MSEC:
                this_value = int(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_RETURN_TO_SERVICE_MSEC_MIN and this_value <= AIF_SETTINGS_RETURN_TO_SERVICE_MSEC_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_IN_EEPROM_U32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_RETURN_TO_SERVICE_MSEC_MIN,
                                                                  AIF_SETTINGS_RETURN_TO_SERVICE_MSEC_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_MAX_OUT_VA:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_MAX_OUTPUT_VA_MIN and this_value <= AIF_SETTINGS_MAX_OUTPUT_VA_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_MAX_OUTPUT_VA_MIN,
                                                                  AIF_SETTINGS_MAX_OUTPUT_VA_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_MAX_OUT_VAR_Q1Q4:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_MAX_OUTPUT_VAR_Q1Q4_MIN and this_value <= AIF_SETTINGS_MAX_OUTPUT_VAR_Q1Q4_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_MAX_OUTPUT_VAR_Q1Q4_MIN,
                                                                  AIF_SETTINGS_MAX_OUTPUT_VAR_Q1Q4_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_MIN_PF_Q1:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_MIN_POWER_FACTOR_Q1_MIN and this_value <= AIF_SETTINGS_MIN_POWER_FACTOR_Q1_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q1_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_MIN_POWER_FACTOR_Q1_MIN,
                                                                  AIF_SETTINGS_MIN_POWER_FACTOR_Q1_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_MIN_PF_Q4:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_MIN_POWER_FACTOR_Q4_MIN and this_value <= AIF_SETTINGS_MIN_POWER_FACTOR_Q4_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_MINIMUM_POWER_FACTOR_Q4_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_MIN_POWER_FACTOR_Q4_MIN,
                                                                  AIF_SETTINGS_MIN_POWER_FACTOR_Q4_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_OUT_VAR_RR_PCT:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_MIN and this_value <= AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_MIN,
                                                                  AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_V_WAVER:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_VOLTAGE_WAVERING_V_MIN and this_value <= AIF_SETTINGS_VOLTAGE_WAVERING_V_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_VOLTS_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_VOLTAGE_WAVERING_V_MIN,
                                                                  AIF_SETTINGS_VOLTAGE_WAVERING_V_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_V_WAVER_ENA:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER 0 or 1" % (this_argument, this_value))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_W_WAVER:
                this_value = float(sys.argv[pop + 1])
                if this_value >= AIF_SETTINGS_POWER_WAVERING_WATTS_MIN and this_value <= AIF_SETTINGS_POWER_WAVERING_WATTS_MAX:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_POWER_WAVERING_WATTS_IN_EEPROM_F32_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("%s INVALID VALUE %s ENTER %s .. %s" % (this_argument, this_value,
                                                                  AIF_SETTINGS_POWER_WAVERING_WATTS_MIN,
                                                                  AIF_SETTINGS_POWER_WAVERING_WATTS_MAX))
            elif this_argument == CMD_PARAM_AIF_SETTINGS_REAL_REACT_PRIO:
                this_value = int(sys.argv[pop + 1])
                if this_value == 0 or this_value == 1:
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_AIF_SETTINGS_REAL_REACTIVE_PRIORITY_IN_EEPROM_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)
                else:
                    print("ARG %s - INVALID VALUE %s ENTERED, TRY WITH A, B, C, ALL" % (this_argument, this_value))

            else:
                print("BAD ARGUMENT. I DON'T UNDERSTAND WHAT YOU WANT ...")
                self.print_set_aif_settings_help()
                return

            # Get ready for the next argument pair.
            pop += 2

        # ... for

        # Now command inverters with the update.
        comma_separated_string = sys.argv[total_number_of_arguments - 1]
        comma_separated_string = comma_separated_string.upper()
        one_arg_only = comma_separated_string
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)
            # Go through the list and remove any multicast-looking IPs from the list.
            for index, this_ip in enumerate(ip_list):
                if gu.is_ip_looka_lika_multicast(this_ip):
                    # drop this IP from the list.
                    del ip_list[index]

        if ip_list:
            for ip_address in ip_list:
                if gu.is_send_aif_settings_configuration_command(ip_address, tlv_id_list, tlv_values_list):
                    this_string = "IP -> %s ... DATA PACKED AND SENT OK " % (ip_address)
                else:
                    this_string = "IP -> %s ... DATA PACKED AND SENT ... ***FAILED***" % (ip_address)
                print(this_string)
        else:
            print("IP LIST WAS ... EMPTY!!!")
        return

    def get_catch_up_reports(self):
        # Only 1 user argument is expected: a comma-separated list of thingies.
        # Therefore only 1 user argument is expected.
        #
        #                    [0]       [1]                      [2]
        # Command format: gpts.py get_cu_reports <ALL|SOLAR|BATTERY|comma-separated thingies>
        ip_list = []
        number_of_user_args = len(sys.argv) - 2

        if number_of_user_args != 1:
            print("INVALID NUMBER OF ARGUMENTS")
            self.print_get_catch_up_reports_help()
            return

        ip_list = []
        comma_separated_string = sys.argv[2]
        comma_separated_string = comma_separated_string.upper()
        one_arg_only = sys.argv[2]
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)
            # Go through the list and remove any multicast-looking IPs from the list.
            for index, this_ip in enumerate(ip_list):
                if gu.is_ip_looka_lika_multicast(this_ip):
                    # drop this IP from the list.
                    del ip_list[index]

        if len(ip_list) == 0:
            print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
            return

        for each_ip in ip_list:
            (catch_up_reports_tlv_id_list, catch_up_reports_values_list) = gu.get_catch_up_reports_data_by_ip(each_ip)
            if (len(catch_up_reports_tlv_id_list) == len(catch_up_reports_values_list)) and len(catch_up_reports_tlv_id_list) > 0:
                # Good to print.
                print("%s ... CATCH UP REPORTS DATA (%s components):" % (each_ip, len(catch_up_reports_tlv_id_list)))
                gu.print_inverter_tlv_id_and_data_list(catch_up_reports_tlv_id_list, catch_up_reports_values_list)
            else:
                # Either unequal lengths, or no response.
                if len(catch_up_reports_tlv_id_list) != len(catch_up_reports_values_list):
                    print("UNEXPECTED UNEQUAL ID/VALUES LIST LENGTHS = %s/%s" % (len(catch_up_reports_tlv_id_list), len(catch_up_reports_values_list)))
                else:
                    # Zero length means no response.
                    print("%s -> NO_RESPONSE" % (each_ip))
        return

    def pcs_unit_id_add_user_tlv(self, packed_data, rank):
        if rank == 0:
            # rank 0 is ALWAYS an ADD.
            packed_data = gu.add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_REGULATION_U16_TLV_TYPE, 100)
        elif rank == 1:
            packed_data = gu.append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE, 0.5)
        elif rank == 2:
            packed_data = gu.append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_RAM_ONLY_F32_TLV_TYPE, 0.5)
        elif rank == 3:
            packed_data = gu.append_user_tlv_32bit_unsigned_long(packed_data, GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE, 45678)
        elif rank == 4:
            packed_data = gu.append_user_tlv_32bit_unsigned_long(packed_data, GTW_TO_SG424_AIF_SETTINGS_RETURN_TO_SERVICE_DELAY_MSEC_IN_EEPROM_U32_TLV_TYPE, 366000)
        elif rank == 5:
            packed_data = gu.append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VA_IN_EEPROM_F32_TLV_TYPE, 321.6)
        elif rank == 6:
            packed_data = gu.append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_SETTINGS_MAXIMUM_OUTPUT_VAR_Q1Q4_IN_EEPROM_F32_TLV_TYPE, 222.2)
        elif rank == 7:
            packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_AIF_SETTINGS_VOLTAGE_WAVERING_ENABLE_IN_EEPROM_U16_TLV_TYPE, 1)
        elif rank == 8:
            packed_data = gu.append_user_tlv_string(packed_data, GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "yabadaba-doo.ORG")
        elif rank == 9:
            packed_data = gu.append_user_tlv_32bit_float(packed_data, GTW_TO_SG424_AIF_SETTINGS_OUTPUT_VAR_RAMP_RATE_PCT_IN_EEPROM_F32_TLV_TYPE, 33.3)
        return packed_data

    def build_pcs_unit_id_encapsulated_packed_data(self):
        complete_packed_data = ""
        if self.user_desire == 1:
            # This "complete" TLV packet contains a single encapsulated START/END set.
            # Build the sub-group data that will be encompassed within the START/END TLVs.
            sub_group_packed_data = ""
            for iggy in range (0, self.number_user_tlv_batch_1):
                sub_group_packed_data = self.pcs_unit_id_add_user_tlv(sub_group_packed_data, iggy)
            sub_group_length = len(sub_group_packed_data) + 2  # extra 2 bytes to encompass the PCS UNIT ID itself
            number_of_user_tlv = self.number_user_tlv_batch_1

            # Build the TLV to contain the encapsulated set of TLVs:
            group_id_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_length, int(self.pcs_unit_id))
            number_of_user_tlv += 1
            # Finally: create the complete TLV-encoded packet:
            master_tlv = gu.add_master_tlv(number_of_user_tlv)
            complete_packed_data = master_tlv + (group_id_packed_data + sub_group_packed_data)

        elif self.user_desire == 2:
            # This "complete" TLV packet contains 2 encapsulated START/END set. The payload portion will be
            # constructed as follows:
            # - a PCS START/data/END TLV with first value pcs unit id
            # - a simple non-encapsulated TLV
            # - a PCS START/data/END TLV with second value pcs unit id
            # - another simple non-encapsulated TLV

            # Build the FIRST sub-group data that will be encompassed within the START/END TLVs.
            sub_group_1_packed_data = ""
            for iggy in range (0, self.number_user_tlv_batch_1):
                sub_group_1_packed_data = self.pcs_unit_id_add_user_tlv(sub_group_1_packed_data, iggy)
            sub_group_1_length = len(sub_group_1_packed_data) + 2  # extra 2 bytes to encompass the PCS UNIT ID itself
            number_of_user_tlv = self.number_user_tlv_batch_1
            # Build the TLV to contain the encapsulated set of TLVs:
            group_id_1_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_1_length, int(self.pcs_unit_id_batch__1))
            number_of_user_tlv += 1

            # Add a non-encapsulated TLV:
            non_encapsulated_packed_data_1 = gu.add_user_tlv_string(GTW_TO_SG424_CLI_COMMAND_STRING_TLV_TYPE, "yo-mama")
            number_of_user_tlv += 1

            # Build the SECOND sub-group data that will be encompassed within the START/END TLVs.
            sub_group_2_packed_data = ""
            for iggy in range (0, self.number_user_tlv_batch_2):
                sub_group_2_packed_data = self.pcs_unit_id_add_user_tlv(sub_group_2_packed_data, iggy)
            sub_group_2_length = len(sub_group_2_packed_data) + 2  # extra 2 bytes to encompass the PCS UNIT ID itself
            number_of_user_tlv += self.number_user_tlv_batch_2
            # Build the TLV to contain the encapsulated set of TLVs:
            group_id_2_packed_data = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_2_length, int(self.pcs_unit_id_batch_2))
            number_of_user_tlv += 1

            # Because the above test data may mangle the NTP URL, include a valid TLV OUTSIDE of the encapsulated set.
            non_encapsulated_packed_data_2 = gu.add_user_tlv_string(GTW_TO_SG424_NTP_SERVER_STRING_TLV_TYPE, "time.google.com")
            number_of_user_tlv += 1

            # Finally: create the complete TLV-encoded packet:
            master_tlv = gu.add_master_tlv(number_of_user_tlv)
            complete_packed_data = master_tlv \
                                   + (group_id_1_packed_data + sub_group_1_packed_data) \
                                   + non_encapsulated_packed_data_1 \
                                   + (group_id_2_packed_data + sub_group_2_packed_data) \
                                   + non_encapsulated_packed_data_2

        else:
            pass

        return complete_packed_data

    def pcs_unit_id_good_tlv_test(self):
        '''
        gpts.py %s <1> <PCS UNIT ID> <# OF TLVs> <IP>" % (GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST))
        gpts.py %s <2> <PCS UNIT ID #1> <# OF TLVs> <PCS UNIT ID #2> <# OF TLVs> <IP>" % (GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST))
        '''
        number_of_user_arguments = len(sys.argv) - 2
        if number_of_user_arguments >= 3:
            multicast_ip = ""
            ip_list = []
            self.number_user_tlv_batch_1 = 0
            self.number_user_tlv_batch_2 = 0
            self.pcs_unit_id_batch_1 = 0
            self.pcs_unit_id_batch_2 = 0
            # At the very minimum, gotta have 3.
            self.user_desire = int(sys.argv[2])
            if self.user_desire == 1:
                # Must have 3 or 4 user arguments.
                self.pcs_unit_id_batch_1 = int(sys.argv[3])
                self.number_user_tlv_batch_1 = int(sys.argv[4])
                if number_of_user_arguments == 3:
                    # No IP. Multicast.
                    multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
                elif number_of_user_arguments == 4:
                    # Extract IP. Comma-separated OK.
                    ip_list = gu.get_ip_list_from_comma_separated_list(sys.argv[5])
                else:
                    print("Wrong number of arguments[1], you gave me %s." % (number_of_user_arguments))
                    self.print_pcs_unit_id_good_tlv_test_help()
                    return

                complete_packed_data = self.build_pcs_unit_id_encapsulated_packed_data()

            elif self.user_desire == 2:
                self.pcs_unit_id_batch_1 = int(sys.argv[3])
                self.number_user_tlv_batch_1 = int(sys.argv[4])
                self.pcs_unit_id_batch_2 = int(sys.argv[5])
                self.number_user_tlv_batch_2 = int(sys.argv[6])

                # Must have 5 or 6 user arguments.
                if number_of_user_arguments == 5:
                    # No IP. Multicast.
                    multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
                elif number_of_user_arguments == 6:
                    # Extract IP. Comma-separated OK.
                    ip_list = gu.get_ip_list_from_comma_separated_list(sys.argv[7])
                else:
                    print("Wrong number of arguments[2], you gave me %s." % (number_of_user_arguments))
                    self.print_pcs_unit_id_good_tlv_test_help()
                    return

                complete_packed_data = self.build_pcs_unit_id_encapsulated_packed_data()

            else:
                print("I know nothing about this test number %s" % (self.user_desire))
                self.print_pcs_unit_id_good_tlv_test_help()
                return

            # Now send.
            if multicast_ip:
                this_string = "PUI ENCAPSULATED TEST PACKET MCAST TO %s ..." % (multicast_ip)
                if gu.is_send_multicast_packet_and_close_socket(complete_packed_data, multicast_ip, SG424_PRIVATE_UDP_PORT):
                    this_string += "OK"
                else:
                    this_string += "FAILED"
                print(this_string)
            else:
                for this_ip in ip_list:
                    this_string = "PUI ENCAPSULATED TEST PACKET UNICAST TO %s ..." % (this_ip)
                    if gu.is_send_tlv_packed_data(this_ip, complete_packed_data):
                        this_string += "OK"
                    else:
                        this_string += "FAILED"
                    print(this_string)

        else:
            print("You gave me %s arguments." % (number_of_user_arguments))
            self.print_pcs_unit_id_good_tlv_test_help()
            return

    def pcs_unit_id_bad_tlv_test(self):

        # ---------------------------------------------------------------------------------------------------------------------------------
        # Some supporting mini functions to construct and send badly-formatted TLVs.
        def send_to_this_multicast(some_text, some_packed_data, some_multicast_ip):
            this_string = "TEST \"%s\" - PUI ENCAPSULATED BAD TLV TEST PACKET MCAST TO %s ..." % (some_text, some_multicast_ip)
            if gu.is_send_multicast_packet_and_close_socket(some_packed_data, some_multicast_ip, SG424_PRIVATE_UDP_PORT):
                this_string += "OK"
            else:
                this_string += "FAILED"
            print(this_string)

        def send_to_this_ip_list(some_text, some_packed_data, some_ip_list):
            print("TEST \"%s\" - PUI ENCAPSULATED BAD TLV TEST PACKET UNICAST TO ... " % (some_text))
            for this_ip in some_ip_list:
                if gu.is_send_tlv_packed_data(this_ip, some_packed_data):
                    print("IP %s OK" % (this_ip))
                else:
                    print("IP %s N O OK" % (this_ip))

        # This is a bare-bones, quick-and-dirty set of functions. Something better could be devised,
        # but not in the hour it took to develop these.

        def encapsulated_bad_test_1(pcs_unit_id, ip_list, multicast_ip):
            # 1 ENCAPSULATED PCS START/END TLV BUT WITH DIFFERENT START/END VALUES.
            # NO LONGER VALID SINCE THE END TLV HAS BEEN REMOVED
            print("NO BAD TEST 1 DEFINED")

        def encapsulated_bad_test_2(pcs_unit_id, ip_list, multicast_ip):
            # COMMAND_SET_END MISSING.
            # NO LONGER VALID SINCE THE END TLV HAS BEEN REMOVED
            print("NO BAD TEST 2 DEFINED")

        def encapsulated_bad_test_3(pcs_unit_id, ip_list, multicast_ip):
            # COMMAND_SET_START MISSING.
            # NO LONGER VALID SINCE THE END TLV HAS BEEN REMOVED
            print("NO BAD TEST 3 DEFINED")

        def encapsulated_bad_test_4(pcs_unit_id, ip_list, multicast_ip):
            # packet has 2 COMMAND_SET_START WITH SECOND SET MISSING THE END TLV.
            # NO LONGER VALID SINCE THE END TLV HAS BEEN REMOVED
            print("NO BAD TEST 4 DEFINED")

        def encapsulated_bad_test_5(pcs_unit_id, ip_list, multicast_ip):
            # Bogus length packet.
            some_string_data = "trick_the_inverter_into_doing_something_stupid"
            bogus_length = 123
            sub_group_packed_data = gu.add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_REGULATION_U16_TLV_TYPE, 100)
            sub_group_packed_data = gu.append_user_tlv_32bit_float(sub_group_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE, 0.5)
            sub_group_packed_data = gu.append_user_tlv_32bit_float(sub_group_packed_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_DOWN_PCT_RAM_ONLY_F32_TLV_TYPE, 0.5)
            sub_group_packed_data = gu.append_user_tlv_string_with_invalid_length(sub_group_packed_data, GTW_TO_SG424_PLAIN_RESET_COMMAND_U16_TLV_TYPE, bogus_length, some_string_data)
            sub_group_packed_data = gu.append_user_tlv_32bit_unsigned_long(sub_group_packed_data, GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE, 45678)
            sub_group_length = len(sub_group_packed_data) + 2  # extra 2 bytes to encompass the PCS UNIT ID itself
            number_of_user_tlv = 5
            packed_data_start = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_length, int(pcs_unit_id))
            number_of_user_tlv += 1
            master_tlv = gu.add_master_tlv(number_of_user_tlv)
            complete_packed_data = master_tlv + (packed_data_start + sub_group_packed_data)
            if multicast_ip:
                send_to_this_multicast(PCS_UNIT_ID_BAD_TLV_TEST_5_TITLE, complete_packed_data, multicast_ip)
            else:
                send_to_this_ip_list(PCS_UNIT_ID_BAD_TLV_TEST_5_TITLE, complete_packed_data, ip_list)
        #
        # ---------------------------------------------------------------------------------------------------------------------------------

        number_of_user_arguments = len(sys.argv) - 2
        if number_of_user_arguments >= 2:
            user_desire = int(sys.argv[2])
            pcs_unit_id = int(sys.argv[3])
            ip_list = []
            multicast_ip = ""

            if number_of_user_arguments == 3:
                # Extract IP list.
                ip_list = gu.get_ip_list_from_comma_separated_list(sys.argv[4])
            else:
                multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)

            if user_desire == 1:
                print("BAD TEST 1 REMOVED AS END TLV REMOVED")
            elif user_desire == 2:
                print("BAD TEST 2 REMOVED AS END TLV REMOVED")
            elif user_desire == 3:
                print("BAD TEST 3 REMOVED AS END TLV REMOVED")
            elif user_desire == 4:
                print("BAD TEST 4 REMOVED AS END TLV REMOVED")
            elif user_desire == 5:
                encapsulated_bad_test_5(pcs_unit_id, ip_list, multicast_ip)
            else:
                print("Wrong 1st paramater value.")
                self.print_pcs_unit_id_bad_tlv_test_help()
                return

        else:
            print("Wrong number of arguments.")
            self.print_pcs_unit_id_bad_tlv_test_help()
            return

    def forced_inverter_upgrade(self):
        # Forced upgrade of all communicating inverters (SG424_APP, BOOT_LOADER, BOOT_UPDATER")
        # based on forced upgrade parameters in inverter_upgrade control:
        #
        # for all inverters in the database: if comm == SG424_APP or BOOT_LOADER or BOOT_UPDATER
        #                                    AND version is NOT == force upgrade version
        #                                    THEN: setup this inverter for upgrade
        #
        # First: open the file handler into which resutls of the FIU will be written.
        try:
            results_file_handle = open(FIU_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            # This has never been observed during development.
            # And a lot of fat good this print does if launched from the scheduler.
            print("FAILED TO OPEN %s FILE" % (FIU_RESULTS_PATH_AND_FILE_NAME))
            return

        this_time = time.strftime("%Y-%m-%d %H:%M:%S")
        current_utc = int(time.time())
        this_string = "-----------------------------------------------------------\n"
        this_string += "FIU: FORCED INVERTER UPGRADE LAUNCHED (AT %s UTC = %s)" % (this_time, current_utc)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)

        # Is the forced inverter upgrade feature enabled?
        if not gu.is_forced_inverter_upgrade_enabled():
            this_string = "FIU CANNOT RUN ... FORCED INVERTER UPGRADE DISABLED"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        # Make sure there is no active upgrade presently on-going. This could mess things up.
        if gu.is_inverter_upgrade_in_progress():
            this_string = "FIU CANNOT RUN ... UPGRADE PROCESS ALREADY ON-GOING"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        # Next get the force upgrade parameters from the inverter upgrade table.
        select_string = "SELECT %s,%s,%s,%s FROM %s;" % (INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_LIMIT_COLUMN_NAME,
                                                         INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_VERSION_COLUMN_NAME,
                                                         INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_DIRECTORY_COLUMN_NAME,
                                                         INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_FILE_NAME_COLUMN_NAME,
                                                         DB_INVERTER_UPGRADE_TABLE)
        select_result = prepared_act_on_database(FETCH_ONE, select_string, ())
        forced_inverter_limit = select_result[INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_LIMIT_COLUMN_NAME]
        forced_inverter_version = select_result[INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_VERSION_COLUMN_NAME]
        forced_upgrade_directory = select_result[INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_DIRECTORY_COLUMN_NAME]
        forced_upgrade_file_name = select_result[INVERTER_UPGRADE_TABLE_FORCED_UPGRADE_FILE_NAME_COLUMN_NAME]
        if (forced_inverter_version) and (forced_upgrade_file_name) and (forced_upgrade_directory):
            this_string = "FIU BASED ON VERSION=%s SG424 UPGRADE PATH/FILE = %s/%s" % (forced_inverter_version,
                                                                                       forced_upgrade_directory,
                                                                                       forced_upgrade_file_name)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            # Verify existence of the upgrade file path and file name.
            full_upgrade_file_path_name = "%s/%s" % (forced_upgrade_directory, forced_upgrade_file_name)
            if os.path.isfile(full_upgrade_file_path_name):
                # Get the IP list of inverters to upgrade. Only those inverters that are communicating are
                # extracted, based on the "comm" column in the inverters table.
                forced_upgrade_ip_list = gu.get_comm_ip_list_from_not_version(forced_inverter_version, forced_inverter_limit)
                if not forced_upgrade_ip_list:
                    this_string = "FIU: IP LIST EMPTY - NO INVERTERS TO UPGRADE"
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)
                    results_file_handle.close()
                    return

                # Now pare down the list further




                # NOW READY TO DO ACTUAL WORK!
                # Set the upgrade_status in the inverters table to no_command and upgrade status to 0/zero.
                this_string = "FIU: INVERTERS TABLE UPGRADE_STATUS/PROGRESS SET TO NO_ACTION/0"
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                update_string = "UPDATE %s SET %s = %s,%s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                    INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                    INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s")
                update_args = (INVERTERS_UPGRADE_STATUS_NO_ACTION, 0)
                prepared_act_on_database(EXECUTE, update_string, update_args)

                # The list of IPs to be upgraded or not can be very large. Only state the count for each.
                this_string = "FIU: PREPARING TO SET UPGRADE STATUS FOR SELECTED INVERTERS:\r\n"
                number_of_forced_upgraded_inverters = 0
                number_of_unmosted__inverters = 0
                for this_ip in forced_upgrade_ip_list:
                    if gu.is_fiu_set_against_this_ip(this_ip):
                        number_of_forced_upgraded_inverters += 1
                        # NO: CAN BE TOO MANY: this_inverter_version = gu.get_inverters_version_column_by_ip(this_ip)
                        # NO: CAN BE TOO MANY: this_string += "%s ... from %s\n" % (this_ip, this_inverter_version)
                        update_string = "UPDATE %s SET %s = %s WHERE %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                                  INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                                  INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
                        update_args = (INVERTERS_UPGRADE_STATUS_UPGRADE_WAITING, this_ip)
                        prepared_act_on_database(EXECUTE, update_string, update_args)
                    else:
                        # Inverter is part of a group where forced upgrade is cleared. Inverter is left unmolested.
                        number_of_unmosted__inverters += 1

                this_string += "NUMBER OF INVERTERS TO BE UPGRADED = %s\r\n" % (number_of_forced_upgraded_inverters)
                this_string += "NUMBER OF INVERTERS LEFT ALONE:      %s" % (number_of_unmosted__inverters)
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)

                if number_of_forced_upgraded_inverters > 0:

                    # Copy the upgrade file name to the appropriate location where the upgrade script can find it,
                    # and set the upgrade file name in the inverter_upgrade table.
                    #
                    # The upgrade file's existence has been verified. The user may have specified a full path name (typically
                    # from the $HOME/Downloads directory), so get the file name alone without any directory path information.
                    file_name_components = full_upgrade_file_path_name.split('/')
                    if len(file_name_components) == 1:
                        # There was no path name information. The file name is the file name.
                        upgrade_file_name_only = file_name_components[0]
                    else:
                        # Path name information specified. The actual file name is the last component.
                        upgrade_file_name_only = file_name_components[len(file_name_components) - 1]
                    select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_SOFTWARE_DIRECTORY_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
                    upgrade_row = prepared_act_on_database(FETCH_ONE, select_string, ())
                    upgrade_directory = upgrade_row[INVERTER_UPGRADE_TABLE_SOFTWARE_DIRECTORY_COLUMN_NAME]
                    this_string = "FIU: INVERTER_UPGRADE TABLE FILE NAME UPDATED WITH %s" % (upgrade_file_name_only)
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)

                    update_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_UPGRADE_TABLE, INVERTER_UPGRADE_TABLE_FILE_NAME_COLUMN_NAME, "%s")
                    update_args = (upgrade_file_name_only, )
                    prepared_act_on_database(EXECUTE, update_string, update_args)

                    # Now copy that user file to that directory.
                    hex_file_copy_command = "sudo cp %s %s/." % (full_upgrade_file_path_name, upgrade_directory)
                    this_string = "FIU: EXECUTING COPY COMMAND: %s" % (hex_file_copy_command)
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)
                    donut_care_return = os.system(hex_file_copy_command)

                    # FINALLY ... set the command field in the upgrade table. This is what the gateway_monitor checks.
                    # When it detects "start_upgrade" in the command column, it will launch the inverter upgrade script.query_by
                    this_string = "FIU: SETTING INVERTER_UPGRADE TABLE COMMAND TO START. FIU DONE - UPGRADE SHOULD START"
                    gu.set_inverter_upgrade_command(INVERTER_UPGRADE_COMMAND_START_UPGRADE)
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)

                else:
                    this_string = "FIU: THERE WERE 0 INVERTERS TO BE FORCED UPGRADED - NO ONE IS COMING TO MY PARTY"
                    print(this_string)
                    results_file_handle.write(this_string + LINE_TERMINATION)

            else:
                # Upgrade file not found or does not exist. Cannot upgrade.
                this_string = "FIU: UPGRADE PATH/DIR %s NOT FOUND - NO FORCED UPGRADE" % (full_upgrade_file_path_name)
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)

        else:
            this_string = "FIU: SOME UPGRADE PARAMS (->%s<- ->%s<- ->%s<-) WERE NULL" % (forced_inverter_version,
                                                                                         forced_upgrade_file_name,
                                                                                         forced_upgrade_directory)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)

        results_file_handle.close()
        return

    def prepare_deferred_inverter_upgrade(self):
        #
        # 3 arguments: - upgrade session title
        #              - SG424 hex name/path
        #              - IP/STRING ID comma-separated items
        #
        #
        # 1. Check if the upgrade session title exists in the deferred upgrade table. If it does, it will be deleted.
        # 2. Write the following 3 components to the deferred upgrade table:
        #    2.1  Upgrade session title
        #    2.2. Write the presumed SG424 hex name/path as the 1st line of the session file. *NOT VALIDATED*
        #    2.3. Write the presumed comma-separated IP/STRING ID list as the 2nd line of the session file. *NOT VALIDATED*

        try:
            results_file_handle = open(DIU_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            # This has never been observed during development.
            print("FAILED TO OPEN %s FILE" % (DIU_RESULTS_PATH_AND_FILE_NAME))
            return

        this_time = time.strftime("%Y-%m-%d %H:%M:%S")
        current_utc = int(time.time())

        number_of_user_args = len(sys.argv) - 2
        if (number_of_user_args == 3) or (number_of_user_args == 2):
            session_title = sys.argv[2]
            inverter_upgrade_file = sys.argv[3]
            if (number_of_user_args == 2):
                # list of comma-separated items omitted - user wants to upgrade ALL inverters, so ...
                comma_separated_items = PDIU_UPGRADE_ALL_INVERTERS
            else:
                comma_separated_items = sys.argv[4]
                comma_separated_items = comma_separated_items.strip("\n")
                comma_separated_items = comma_separated_items.strip("\r")

            this_string = "-----------------------------------------------------------\n"
            this_string += "PDIU: PREPARE DEFERRED INVERTER UPGRADE FILE (AT %s UTC = %s)" % (this_time, current_utc)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)

            if gu.is_session_title_in_dui_table(session_title):
                this_string = "DELETING/REPLACING SESSION TITLE %s IN DIU TABLE" % (session_title)
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                gu.delete_session_title_row_from_dui_table(session_title)

            # Write the sesssion tile, SG424 upgrade file spec and comma-separated string to the DIU table.
            this_string = "DIU TABLE UPDATED WITH:\n"
            this_string += "- SESSION TITLE = %s\n" % (session_title)
            this_string += "- SG424 upgrade file = %s\n" % (inverter_upgrade_file)
            this_string += "- comma-separated items = %s" % (comma_separated_items)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)

            # -------------------------------------------------------------------------------------
            gu.insert_into_dui_table(session_title, inverter_upgrade_file, comma_separated_items)
            # -------------------------------------------------------------------------------------

            # Close the file. Prepare phase is complete.
            this_string = "PREPARE DEFERRED INVERTER UPGRADE PHASE COMPLETE"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()

        else:
            this_string = "-----------------------------------------------------------\n"
            this_string += "PDIU: NUMBER OF ARGS = %s - EXPECTING 2 or 3\n" % (number_of_user_args)
            this_string += "PDIU: UNCEREMONIOUSLY TERMINATED (AT %s UTC = %s)" % (this_time, current_utc)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            self.print_pdiu_help()

        return

    def launch_deferred_inverter_upgrade(self):

        # Because this launcher is generally expected to be driven by the scheduler or command line,
        # the "results" are writtten to both a file as well as printed out.

        try:
            results_file_handle = open(DIU_RESULTS_PATH_AND_FILE_NAME, 'a')
        except Exception: # as err_string:
            # This has never been observed during development.
            # And a lot of fat good this print does if launched from the scheduler.
            print("FAILED TO OPEN %s FILE" % (DIU_RESULTS_PATH_AND_FILE_NAME))
            return

        this_time = time.strftime("%Y-%m-%d %H:%M:%S")
        current_utc = int(time.time())
        this_string = "-----------------------------------------------------------\n"
        this_string += "LDIU: LAUNCHED AT %s (UTC = %s)" % (this_time, current_utc)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)

        # First things first: make sure there is no active upgrade presently on-going. This could mess things up.
        if gu.is_inverter_upgrade_in_progress():
            this_string = "DEFERRED UPGRADE CANNOT RUN ... UPGRADE PROCESS ALREADY ON-GOING"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        # Next: get the session title as an argument. There should be only 1 user argument.
        number_of_user_args = len(sys.argv) - 2
        if number_of_user_args != 1:
            this_string = "LDIU: NUMBER OF ARGS = %s - EXPECTING ONLY 1\n" % (number_of_user_args)
            this_string += "LDIU: UNCEREMONIOUSLY TERMINATED (AT %s UTC = %s)" % (this_time, current_utc)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            self.print_ldiu_help()
            return

        # Get the SG424 upgrade file spec and the list of items to upgrade based on the session title.
        session_title_as_arg = sys.argv[2]
        session_title = ""
        upgrade_file_name = ""
        comma_separated_arg = ""
        (session_title, upgrade_file_name, comma_separated_arg) = gu.get_diu_data_by_session_title(session_title_as_arg)
        if (not session_title):
            this_string = "NO SESSION TITLE %s IN DEFERRED UPGRADE TABLE - LDIU PROCESS ABANDONED" % (session_title)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return
        elif (not upgrade_file_name) or (not comma_separated_arg):
            this_string = "SESSION TITLE %s: UGRADE FILE ->%s<- AND/OR LIST OF ITEMS ->%s<- = NULL - LDIU PROCESS ABANDONED"\
                                                                                                 % (session_title,
                                                                                                    upgrade_file_name,
                                                                                                    comma_separated_arg)
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        upgrade_file_name_components = upgrade_file_name.split('.')
        if len(upgrade_file_name_components) == 2:
            # Examine the last component of the file name:
            file_type = upgrade_file_name_components[1]
            if file_type != "hex":
                this_string = "UPGRADE FILE TYPE = %s AND MUST BE .hex" % (file_type)
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                self.print_ldiu_help()
                results_file_handle.close()
                return

            # Check if the file exists.
            if not os.path.isfile(upgrade_file_name):
                # The file does not exists.
                this_string = "DID NOT FIND FILE %s - LDIU PROCESS ABANDONED" % (upgrade_file_name)
                print(this_string)
                results_file_handle.write(this_string + LINE_TERMINATION)
                results_file_handle.close()
                self.print_ldiu_help()
                return

        else:
            this_string = "UPGRADE FILE SPECIFICATION MUST BE SOMETHING LIKE blah_blah/yea_yea/something.hex"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            self.print_ldiu_help()
            return

        # Now check the comma-separated list. Comma-separated serial number, group ID,
        # IP or string ID is allowed, as well as "ALL". But not a mix and match, maybe later.
        # Extract and do basic sanity.
        ip_list = []
        argument_list = comma_separated_arg.split(',')
        first_argument = argument_list[0]

        if len(argument_list) == 1 and first_argument == PDIU_UPGRADE_ALL_INVERTERS:
            # User want to upgrade the whole shabang. So get all IP from the database.
            this_string = "LDIU: SETUP TO UPGRADING ALL INVERTERS IN DATABASE"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            select_string = "SELECT %s FROM %s;" % (INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, DB_INVERTER_NXO_TABLE)
            total_ip_list = prepared_act_on_database(FETCH_ALL, select_string, ())
            for each_ip in total_ip_list:
                ip_list.append(each_ip[INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME])

        else:
            # Get the IP list based on comma-separated set of ... IP, SN, GROUP ID, STRING ID, MAC.
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_arg)

        if len(ip_list) == 0:
            this_string = "NO IPs FOR ANY INVERTERS TO UPGRADE - NO UPGRADE LAUNCHED"
            print(this_string)
            results_file_handle.write(this_string + LINE_TERMINATION)
            results_file_handle.close()
            return

        # -----------------------------------------------------------------------------------------
        # THIS SECTION OF CODE IS "REPEATED" BY THE FIU (FORCED INVERTER UPGRADE)
        # # (EXCEPT FOR THE BITS HERE AND THERE THAT WRITE TO THE RESULTS FILE):
        # IT NEEDS TO BE CALLED FROM A COMMON FUNCTION, OR AT LEADST A COMMON SET OF FUNCTIONS:

        # NOW READY TO DO ACTUAL WORK!
        # Set the upgrade_status in the inverters table to no_command and upgrade status to 0/zero.
        this_string = "INVERTERS TABLE: UPGRADE_STATUS/PROGRESS SET TO NO_ACTION/0"
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)
        update_string = "UPDATE %s SET %s = %s,%s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                            INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                            INVERTERS_TABLE_UPGRADE_PROGRESS_COLUMN_NAME, "%s")
        update_args = (INVERTERS_UPGRADE_STATUS_NO_ACTION, 0)
        prepared_act_on_database(EXECUTE, update_string, update_args)

        this_string = "SETTING UPGRADE STATUS IN INVERTERS TABLE"
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)
        for this_ip in ip_list:
            update_string = "UPDATE %s SET %s = %s WHERE %s = %s;" % (DB_INVERTER_NXO_TABLE,
                                                                      INVERTERS_TABLE_UPGRADE_STATUS_COLUMN_NAME, "%s",
                                                                      INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
            update_args = (INVERTERS_UPGRADE_STATUS_UPGRADE_WAITING, this_ip)
            prepared_act_on_database(EXECUTE, update_string, update_args)

        # Copy the upgrade file name to the appropriate location where the upgrade script can find it,
        # and set the upgrade file name in the inverter_upgrade table.
        #
        # The upgrade file's existence has been verified. The user may have specified a full path name (typically
        # from the $HOME/Downloads directory), so get the file name alone without any directory path information.
        file_name_components = upgrade_file_name.split('/')
        if len(file_name_components) == 1:
            # There was no path name information. The file name is the file name.
            upgrade_file_name_only = file_name_components[0]
        else:
            # Path name information specified. The actual file name is the last component.
            upgrade_file_name_only = file_name_components[len(file_name_components) - 1]
        select_string = "SELECT %s FROM %s;" % (INVERTER_UPGRADE_TABLE_SOFTWARE_DIRECTORY_COLUMN_NAME, DB_INVERTER_UPGRADE_TABLE)
        upgrade_row = prepared_act_on_database(FETCH_ONE, select_string, ())
        upgrade_directory = upgrade_row[INVERTER_UPGRADE_TABLE_SOFTWARE_DIRECTORY_COLUMN_NAME]
        this_string = "INVERTER_UPGRADE TABLE: FILE NAME UPDATED WITH %s" % (upgrade_file_name_only)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)

        update_string = "UPDATE %s SET %s = %s;" % (DB_INVERTER_UPGRADE_TABLE, INVERTER_UPGRADE_TABLE_FILE_NAME_COLUMN_NAME, "%s")
        update_args = (upgrade_file_name_only, )
        prepared_act_on_database(EXECUTE, update_string, update_args)

        # Now copy that user file to that directory.
        hex_file_copy_command = "sudo cp %s %s/." % (upgrade_file_name, upgrade_directory)
        this_string = "EXECUTING COPY COMMAND: %s" % (hex_file_copy_command)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)
        donut_care_return = os.system(hex_file_copy_command)

        # FINALLY ... set the command field in the upgrade table. This is what the gateway_monitor checks.
        # When it detects "start_upgrade" in the command column, it will launch the inverter upgrade script.
        this_string = "SETTING INVERTER_UPGRADE TABLE COMMAND TO START. LDIU GOING HOME - UPGRADE SHOULD START"
        gu.set_inverter_upgrade_command(INVERTER_UPGRADE_COMMAND_START_UPGRADE)
        print(this_string)
        results_file_handle.write(this_string + LINE_TERMINATION)


        # END OF "REPEATED CODE SECTION".
        #
        # -----------------------------------------------------------------------------------------

        results_file_handle.close()

        return

    def get_eerpt_info(self):
        #                    [0]      [1]                  [2]
        # Command format: gpts.py get_eerpt <ALL | SOLAR | BATTERY | comma-separated thingies>
        ip_list = []
        number_of_user_args = len(sys.argv) - 2

        if number_of_user_args != 1:
            print("GET EERPT INFO: 1 ARG ONLY PLEASE")
            self.print_get_eerpt_info_help()
            return

        comma_separated_string = sys.argv[2]
        comma_separated_string = comma_separated_string.upper()
        one_arg_only = sys.argv[2]
        one_arg_only = one_arg_only.upper()
        if (one_arg_only == "ALL") or (one_arg_only == "SOLAR") or (one_arg_only == "BATTERY"):
            ip_list = gu.gimme_ip_list_from_inverter_type(one_arg_only)
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)
            # Go through the list and remove any multicast-looking IPs from the list.
            for index, this_ip in enumerate(ip_list):
                if gu.is_ip_looka_lika_multicast(this_ip):
                    # drop this IP from the list.
                    del ip_list[index]

        if len(ip_list) == 0:
            print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
            return

        for each_ip in ip_list:
            SG424_data = gu.send_and_get_eerpt_data(each_ip)
            if SG424_data:
                # Packet received.
                payload_length = len(SG424_data)
                (tlv_id_list, tlv_data_list) = gu.extract_list_of_tlv_id_and_data_from_packed_data(SG424_data)
                gu.print_inverter_tlv_list_length_title(tlv_id_list, tlv_data_list, each_ip, payload_length)
                gu.print_inverter_tlv_id_and_data_list(tlv_id_list, tlv_data_list)
            else:
                print("%s -> NO_RESPONSE" % (each_ip))

        return

    def set_eerpt_info(self):

        # Parameters come in pairs, the last one is a comma-separated list of sorts, mandatory.
        # If there's an even-number of parameters, then the last parameter was not included, not good.
        #
        # Command format:
        # gpts.py gpts.py set_eerpt <1..%s params with values> <ALL | SOLAR | BATTERY | comma-separated list of THINGIES>
        #
        # If ALL or SOLAR or BATTERY is specified, it is MULTICASTED.

        tlv_id_list = []
        tlv_values_list = []

        number_of_parameters = len(sys.argv)
        number_of_user_arguments = number_of_parameters - 2
        multicast_was_specified = False

        # To update at a minimum 1 parameter, must have at least 3 arguments.

        remainder = number_of_user_arguments % 2
        number_of_pairs = number_of_user_arguments / 2
        if (number_of_user_arguments >= 2) or (number_of_pairs <= NUMBER_OF_MANAGED_EERPT_PARAMETERS):
            print("WITH %s USER ARGUMENTS, PARSE FOR %s PAIRS" % (number_of_user_arguments, number_of_pairs))
            # Parse the user input.
            pop = 2
            for iggy in range (0, number_of_pairs):
                this_argument = sys.argv[pop]
                this_argument = this_argument.upper()

                if this_argument == CMD_PARAM_SET_EERPT_SERVER_ENABLE:
                    this_value = int(sys.argv[pop + 1])
                    if (this_value >= 0) and (this_value <= 3):
                        print("ADD %s/%s TO LIST" % (this_argument, this_value))
                        tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_ENABLE_U16_TLV_TYPE)
                        tlv_values_list.append(this_value)
                    else:
                        print("%s INVALID VALUE %s ENTER 0, 1, 2, or 3" % (this_argument))

                elif this_argument == CMD_PARAM_SET_EERPT_SERVER_RATE:
                    this_value = int(sys.argv[pop + 1])
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_RATE_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)

                elif this_argument == CMD_PARAM_SET_EERPT_SERVER_URL:
                    this_value = sys.argv[pop + 1]
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_URL_STRING_TLV_TYPE)
                    tlv_values_list.append(this_value)

                elif this_argument == CMD_PARAM_SET_EERPT_SERVER_PORT:
                    this_value = int(sys.argv[pop + 1])
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_PORT_U16_TLV_TYPE)
                    tlv_values_list.append(this_value)

                elif this_argument == CMD_PARAM_SET_EERPT_SERVER_USER_NAME:
                    this_value = sys.argv[pop + 1]
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_USER_NAME_STRING_TLV_TYPE)
                    tlv_values_list.append(this_value)

                elif this_argument == CMD_PARAM_SET_EERPT_SERVER_PASSWORD:
                    this_value = sys.argv[pop + 1]
                    print("ADD %s/%s TO LIST" % (this_argument, this_value))
                    tlv_id_list.append(GTW_TO_SG424_EERPT_SERVER_PASSWORD_STRING_TLV_TYPE)
                    tlv_values_list.append(this_value)

                else:
                    print("BAD ARGUMENT ->%s<- ... I DON'T UNDERSTAND WHAT YOU WANT ..." % (this_argument))
                    self.print_set_eerpt_info_help()
                    return

                # Get ready for the next argument pair.
                pop += 2

            # ... for

        else:
            print("NUMBER OF ARGUMENTS IS NOT CORRECT ...")
            self.print_set_eerpt_info_help()
            return

        # Finally, extract and check the IP. It can be unicast or multicast.
        ip_list = []
        if remainder == 1:
            # Odd number of arguments, the last arg is a comma-separated list of sorts.

            comma_separated_string = sys.argv[number_of_parameters - 1]
            comma_separated_string = comma_separated_string.upper()
            if comma_separated_string == "ALL":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL)
                multicast_was_specified = True
            elif comma_separated_string == "SOLAR":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_SOLAR_ONLY)
                multicast_was_specified = True
            elif comma_separated_string == "BATTERY":
                multicast_ip = gu.construct_generic_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY)
                multicast_was_specified = True
            else:
                ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_string)


        # Now send.
        if multicast_was_specified:
            this_string = "SEND USING MULTICAST IP %s ... DATA PACK AND SEND " % (multicast_ip)
            if gu.is_send_eerpt_configuration_command(multicast_ip, tlv_id_list, tlv_values_list):
                this_string += "OK"
            else:
                this_string += "N O K"
            print(this_string)
        else:
            for ip_address in ip_list:
                this_string = "IP -> %s ... DATA PACK AND SEND = " % (ip_address)
                # -----------------------------------------------------------------------------
                # Ready to send the config data. Create the packed data, and send.
                if gu.is_send_eerpt_configuration_command(ip_address, tlv_id_list, tlv_values_list):
                    this_string +=  "OK"
                else:
                    this_string += "N O K"
                print(this_string)
                # -----------------------------------------------------------------------------
        return

    def set_universal_aif(self):

        # UNIVERSAL-AIF modified since its introduction ... SG424 inverters are no longer updated
        # with any notion of region name, nor is the software profile to be updated.

        # Strip off the args that got us here, only pay attention to the arguments passed TO this method call
        universal_aif_args = sys.argv[2:]
        num_args = len(universal_aif_args)
        if num_args != 1:
            print("You did not have the right number of arguments")
            self.print_universal_aif_help()

        else:
            valid_region_names = REGION_NAME_DICT
            valid_region_names.remove(REGION_NAME_UNDEFINED)

            region_name = universal_aif_args[0].upper()

            if region_name not in valid_region_names:
                print("You must use one of the predefined region names")
                self.print_universal_aif_help()

            else:
                # (power_factor, current_leading, pf_enabled)
                pf_info_by_region_name = {
                    REGION_NAME_CA120      : ( 1.00 , 0 , 0 ),
                    REGION_NAME_HI120      : ( 0.95 , 0 , 1 ),
                    REGION_NAME_IEEE       : ( 1.00 , 0 , 0 ),
                    REGION_NAME_NA120      : ( 1.00 , 0 , 0 ),
                    REGION_NAME_MFG120     : ( 1.00 , 0 , 0 )
                }

                (pf, c_lead, enable) = pf_info_by_region_name[region_name]

                # If you made it here, it means the user entered good values
                gu.update_universal_aif_table(region_name, pf, c_lead, enable)
                print("UPDATED REGION NAME (region=%s pf=%.3f c_lead=%i enable=%i)" % (region_name, pf, c_lead, enable))

        '''

        """
        This method does 3 things:
         - Parse the arguments and decide what the user is trying to do (update specific feeds or update universally)
         - Update the database with the relevant information
         - Send a multicast update to all of the inverters
        :author: Sydney Park
         """

        def validate_feed_name(name):
            """
            Possible feed names:
            ALL A B C AB BA AC CA BC CB ABC ACB BAC BCA CAB CBA
            Really doesn't matter as long as all of the letters are valid.
            User can even specify a feed twice (ACBC) if they want to waste time and processing.
            :return: True if valid, false if invalid
            """
            if name == FEED_ALL_LITERAL:
                return True
            for abc in name:
                if abc != FEED_A_LITERAL and abc != FEED_B_LITERAL and abc != FEED_C_LITERAL:
                    return False
                # else keep checking
            # if we made it through the for loop, then they were all valid characters
            return True
        def validate_pf(power_factor):
            return power_factor >= MAX_SG424_POWER_FACTOR and power_factor <= 1.0
        def validate_boolean(boolean_as_int):
            return boolean_as_int == 0 or boolean_as_int == 1
        def send_multicast_sw_profile_update(region_name, pf, c_lead, enable):
            packet = gu.add_master_tlv(4)
            packet = gu.append_user_tlv_string(packet, GTW_TO_SG424_REGION_NAME_STRING_TLV_TYPE, region_name)
            packet = gu.append_user_tlv_32bit_float(packet, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_IN_EEPROM_F32_TLV_TYPE, pf)
            packet = gu.append_user_tlv_16bit_unsigned_short(packet, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM_U16_TLV_TYPE, c_lead)
            packet = gu.append_user_tlv_16bit_unsigned_short(packet, GTW_TO_SG424_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM_U16_TLV_TYPE, enable)
            multicast_ip = construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_ALL, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
            if is_send_multicast_packet_and_close_socket(packet, multicast_ip, SG424_PRIVATE_UDP_PORT):
                print("MCAST TO %s SUCCESSFUL" % (multicast_ip))
                # pass
            else:
                print "ERROR: Multicast update to inverters failed."

        # Strip off the args that got us here, only pay attention to the arguments passed TO this method call
        universal_aif_args = sys.argv[2:]
        num_args = len(universal_aif_args)
        if num_args != 1:
            print("You did not have the right number of arguments")
            self.print_universal_aif_help()

        else:
            valid_region_names = REGION_NAME_DICT
            valid_region_names.remove(REGION_NAME_UNDEFINED)

            region_name = universal_aif_args[0].upper()

            if region_name not in valid_region_names:
                print("You must use one of the predefined region names")
                self.print_universal_aif_help()

            else:
                # (power_factor, current_leading, pf_enabled)
                pf_info_by_region_name = {
                    REGION_NAME_CA120      : ( 1.00 , 0 , 0 ),
                    REGION_NAME_HI120      : ( 0.95 , 0 , 1 ),
                    REGION_NAME_IEEE       : ( 1.00 , 0 , 0 ),
                    REGION_NAME_NA120      : ( 1.00 , 0 , 0 ),
                    REGION_NAME_MFG120     : ( 1.00 , 0 , 0 )
                }

                (pf, c_lead, enable) = pf_info_by_region_name[region_name]

                # If you made it here, it means the user entered good values
                update_universal_aif_table(region_name, pf, c_lead, enable)
                send_multicast_sw_profile_update(region_name, pf, c_lead, enable)
                print("UPDATED REGION NAME (region=%s pf=%.3f c_lead=%i enable=%i)" % (region_name, pf, c_lead, enable))

        '''


    def gateway_operation_mode(self):
        # User request to change the Gateway's operation mode. 2 choices:
        #
        # - power control mode: standard NXO control, as determined by settings/thresholds
        # - observation mode: NXO control disabled, meter readings posted for (you guessed it): observation.
        #
        # Command format: gpts.py gom <power_control_mode | observation_mode>
        if len(sys.argv) != 3:
            print("GOM EXPECTS ONLY 1 USER ARGUMENT TO CHEW ON ...")
            self.print_gateway_operation_mode_help()
        else:
            current_operation_mode = gu.get_gateway_operation_mode()
            operation_mode_argument = sys.argv[2]
            if operation_mode_argument == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE:
                # Enter power control mode ...
                if current_operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE:
                    # Already in power control mode, let the user know. But do it again nevertheless.
                    print("GATEWAY ALREADY UNDER POWER CONTROL MODE - NO CHANGE REQUIRED")
                # System will now enter power control mode. Update the database, and multicast a heartbeat
                # command to all inverters. This will ensure they do not produce energy until the solar
                # regulator is ready for NXO control.
                print("SET GATEWAY OPERATION MODE -> POWER CONTROL")
                gu.set_gateway_operation_mode(operation_mode_argument)
                gu.send_heartbeat_command()

            elif operation_mode_argument == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE:
                # Enter observation mode ...
                if current_operation_mode == GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE:
                    # Already in observation mode,  let the user know. But do it again nevertheless.
                    print("GATEWAY ALREADY UNDER OBSERVATION MODE - NO CHANGE REQUIRED")
                gu.set_gateway_operation_mode(operation_mode_argument)
                gu.send_inverters_clear_gateway_ctrl_command()
            else:
                print("GOM SPITS OUT WHAT WAS ENTERED, INSTEAD, TRY ...")
                self.print_gateway_operation_mode_help()

    def start_pui_association(self):
        # number of user arguments = length of sys.argv - 2
        number_user_arguments = len(sys.argv) - 2
        if number_user_arguments != 1:
            print("INVALID NUMBER OF ARGUMENTS FOR PUI ASSOCIATION")
            self.print_start_pui_association_help()
        else:
            # Before making any changes or updates to any table to get this party moving,
            # check that PUI association control is in the quiet state.
            ok_to_launch_pui = False
            if gu.is_pui_assoc_process_command_no_command():
                ok_to_launch_pui = True
            else:
                # The command is not " no command", the PUI process is not idle. On the other hand,
                # if is has not updated for some time, say, 15 minutes, consider it dead/crashed.
                pui_timestamp = gu.get_pui_assoc_timestamp()
                current_time = time.strftime("%Y-%m-%d %H:%M:%S")
                day1_format = datetime(*time.strptime(current_time, "%Y-%m-%d %H:%M:%S")[:6])
                day2_format = ('%s' % pui_timestamp)
                day2_format = datetime(*time.strptime(day2_format, "%Y-%m-%d %H:%M:%S")[:6])
                elapsed_seconds = abs(day2_format - day1_format).seconds
                current_command = gu.get_pui_assoc_command()
                if elapsed_seconds > MAX_ELAPSED_SECONDS_SINCE_LAST_TIMESTAMP:
                    print("PUI ASSOC COMMAND = %s: NOT UPDATED FOR %s SECONDS, TO BE FORCED LAUNCHED" % (current_command, elapsed_seconds))
                    ok_to_launch_pui = True
                else:
                    print("PUI ASSOCIATION IS NOT %s - PROCESS NOT LAUNCHED" % (PUI_ASSOC_NO_COMMAND_COMMAND))

            if ok_to_launch_pui:
                # The PUI controller is dead quiet. Proceed with the remaining preparations.
                # Get the ESS unit and the comma-separated argument. Validate the ESS unit.
                comma_separated_arg = sys.argv[2]
                comma_separated_arg = comma_separated_arg.upper()
                ip_list = []
                if comma_separated_arg == "SOLAR":
                    print("WHY ARE YOU ASKING FOR PUI ASSOC OF SOLAR INVERTERS?")
                elif (comma_separated_arg == "ALL") or (comma_separated_arg == "BATTERY"):
                    print("SELECTING ALL BATTERY INVERTERS")
                    ip_list = gu.get_database_battery_inverters_ip_list()
                else:
                    ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_arg)

                if len(ip_list) == 0:
                    print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
                else:
                    # With the user's list of inverters IP, extract only battery inverters.
                    # Start by setting all battery inverters PUI association status to idle.
                    update_string = "UPDATE %s SET %s=%s WHERE %s=1;" % (DB_INVERTER_NXO_TABLE,
                                                                         INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                                         INVERTERS_TABLE_BATTERY_COLUMN_NAME)
                    update_args = (INVERTERS_TABLE_PUI_ASSOC_STATUS_NO_ACTION)
                    prepared_act_on_database(EXECUTE, update_string, update_args)
                    association_count = 0
                    for each_ip in ip_list:
                        if gu.is_this_ip_battery_inverter(each_ip):
                            # Set its association status to waiting.
                            association_count += 1
                            update_string = "UPDATE %s SET %s=%s WHERE %s=%s;" % (DB_INVERTER_NXO_TABLE,
                                                                                  INVERTERS_TABLE_PUI_ASSOC_STATUS_COLUMN_NAME, "%s",
                                                                                  INVERTERS_TABLE_IP_ADDRESS_COLUMN_NAME, "%s")
                            update_args = (INVERTERS_TABLE_PUI_ASSOC_STATUS_WAITING, each_ip)
                            prepared_act_on_database(EXECUTE, update_string, update_args)
                            print("IP %s -> PUI ASSOC STATUS SET TO ... waiting" % (each_ip))
                        else:
                            print("IP %s IS NOT A BATTERY INVERTER - NOT INCLUDED IN PROCESS" % (each_ip))
                    if association_count > 0:
                        # Set the PUI controller command to "start".
                        gu.update_pui_assoc_command(PUI_ASSOC_START_COMMAND)
                        print("EXPECT PUI ASSOCIATION TO START")
                    else:
                        print("NO BATTERY INVERTERS FROM COMMA SEPARATED THINGIES - NO PUI ASSOCIATION")
        return

    def start_charger_association(self):
        # There is 1 argument: "ALL" chargers, or a comma-separated list of charger ID.
        #
        # Before making any changes or updates to any table to get this party going,
        # check that the "charger association process" is in the quiet state.
        number_user_arguments = len(sys.argv) - 2
        if number_user_arguments != 1:
            print("INVALID NUMBER OF ARGUMENTS FOR CHARGER ASSOCIATION")
            self.print_start_charger_association_help()
            return

        ok_to_launch_charger_assoc = False

        if not gu.is_charger_assoc_process_command_no_command():
            # The command is not " no command", the charger process is not idle. On the other hand,
            # if is has not updated for some time, say, 15 minutes, consider it dead/crashed.
            charger_timestamp = gu.get_charger_assoc_timestamp()
            current_time = time.strftime("%Y-%m-%d %H:%M:%S")
            day1_format = datetime(*time.strptime(current_time, "%Y-%m-%d %H:%M:%S")[:6])
            day2_format = ('%s' % charger_timestamp)
            day2_format = datetime(*time.strptime(day2_format, "%Y-%m-%d %H:%M:%S")[:6])
            elapsed_seconds = abs(day2_format - day1_format).seconds
            current_command = gu.get_charger_assoc_command()
            if elapsed_seconds > MAX_ELAPSED_SECONDS_SINCE_LAST_TIMESTAMP:
                print("CHARGER ASSOC COMMAND = %s: NOT UPDATED FOR %s SECONDS, TO BE FORCED LAUNCHED" % (current_command, elapsed_seconds))
            else:
                print("CHARGER ASSOCIATION IS NOT %s - PROCESS NOT LAUNCHED" % (CHARGER_ASSOC_NO_COMMAND_COMMAND))
                return

        # The charger association process is quiet, nothing running. What does the user want?
        # First init the "associate" and "status" columns of the charger module table.
        gu.init_all_assoc_associate_and_status_inactive()
        what_to_associate = sys.argv[2]
        what_to_associate = what_to_associate.upper()
        if what_to_associate == "FULL":
            # Associate everything that is discoverable.
            print("SETUP FOR *FULL* CHARGER ASSOCIATION")
            gu.update_charger_assoc_association_type(CHARGER_ASSOC_FULL_ASSOCIATE_TYPE)
            ok_to_launch_charger_assoc = True
        else:
            # Associate selected chargers based on id.
            this_string = "SETUP FOR *SELECTED* CHARGER ASSOCIATION FOR CHARGER ID "
            gu.update_charger_assoc_association_type(CHARGER_ASSOC_SELECT_ASSOCIATE_TYPE)
            associate_count = 0
            charger_id_list = what_to_associate.split(',')
            string_of_charger_id = ""
            for charger_id in charger_id_list:
                if charger_id.isdigit():
                    (module_number, chassis_id) = gu.get_module_number_and_chassis_id_from_charger_id(charger_id)
                    if (module_number != 0) and (chassis_id != 0):
                        associate_count += 1
                        gu.set_charger_to_associate_with_status_waiting(charger_id)
                        string_of_charger_id += "%s," % (charger_id)
                    else:
                        print("CHARGER ID %s FROM COMMA LIST NOT IN %s TABLE - NOT ADDED" % (charger_id, DB_AESS_CHARGER_MODULE_TABLE_NAME))
                else:
                    print("CHARGER ID %s FROM COMMA LIST NOT A DIGIT - NOT ADDED" % (charger_id))
            if associate_count > 0:
                string_of_charger_id = string_of_charger_id[:-1] # Remove the very last comma.
                launch_string = this_string + string_of_charger_id
                print(launch_string)
                ok_to_launch_charger_assoc = True
            else:
                print("COMMA-SEPARATED LIST OF CHARGER ID YIELDED ZERO CHARGERS TO ASSOCIATE")

        if ok_to_launch_charger_assoc:
            # Update the CHARGER controller command to "start".
            gu.update_charger_assoc_command(CHARGER_ASSOC_START_COMMAND)
            print("EXPECT CHARGER ASSOCIATION TO START")

        return

    def show_bmu_soc_pairs(self):
        print("SHOWING BMU SOC PAIRS FOR LOW-DEF BATTERY SYSTEM:")
        print("ASUMPTION MADE: GPTS ITCA WAS RUN TO AUDIT %s TABLE" % (DB_PCS_UNITS_TABLE_NAME))

        select_string = "SELECT %s,%s,%s FROM %s;" % (PCS_UNITS_TABLE_ID_COLUMN_NAME,
                                                      PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME,
                                                      PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME,
                                                      DB_PCS_UNITS_TABLE_NAME)
        bmu_list = prepared_act_on_database(FETCH_ALL, select_string, ())
        for each_bmu_pair in bmu_list:
            id = each_bmu_pair[PCS_UNITS_TABLE_ID_COLUMN_NAME]
            bmu_id_a = each_bmu_pair[PCS_UNITS_TABLE_BMU_ID_A_COLUMN_NAME]
            bmu_id_b = each_bmu_pair[PCS_UNITS_TABLE_BMU_ID_B_COLUMN_NAME]
            # check that both are digits.
            if (bmu_id_b == 0):
                print("PCS UNITS ID %s: not paired for BMU ID %s" % (id, bmu_id_a))
            else:
                # Paired/low-def system. Get the SOC for each.
                (bmu_id_a_soc, bmu_id_b_soc) = gu.get_bmu_soc_pair_from_bmu_id_pair(bmu_id_a, bmu_id_b)
                percentage_difference = abs(bmu_id_a_soc - bmu_id_b_soc)
                print("PCS UNIT ID %s BMU PAIRED: BMU A/B ID=%s/%s SOC=%s/%s DIFF=%s" \
                                          % (id, bmu_id_a, bmu_id_b, bmu_id_a_soc, bmu_id_b_soc, percentage_difference))
        return

    def group_simple_tlv_command_test(self):

        if len(sys.argv) != 5:
            print("SEND GROUP SIMPLE TLV EXPECTS 3 USER ARGUMENTS")
            self.print_group_simple_tlv_command_test_help()
            return

        group_id_string = sys.argv[2]
        group_id_test_number_string = sys.argv[3]
        destination_string = sys.argv[4]
        destination_string = destination_string.upper()

        if group_id_string.isdigit():
            group_id = int(group_id_string)
            if not group_id  > 0:
                print("FIRST PARAM GROUP ID MUST BE > 0")
                self.print_group_simple_tlv_command_test_help()
                return
        else:
            print("FIRST PARAM GROUP ID MUST BE DIGIT")
            self.print_group_simple_tlv_command_test_help()
            return

        # Only 3 test items are defined for simple group testing.
        complete_packed_data = None
        if group_id_test_number_string.isdigit():
            group_id_test_number = int(group_id_test_number_string)
            if group_id_test_number == 1:
                complete_packed_data = gu.build_simple_group_id_packed_tlv_test_1(group_id)
            elif group_id_test_number == 2:
                complete_packed_data = gu.build_simple_group_id_packed_tlv_test_2(group_id)
            elif group_id_test_number == 3:
                complete_packed_data = gu.build_simple_group_id_packed_tlv_test_3(group_id)
            else:
                print("I ONLY KNOW OF GROUP TLV TEST 1, 2 and 3")
                self.print_group_simple_tlv_command_test_help()
                return
        else:
            print("FIRST PARAM GROUP ID TEST NUMBER MUST BE A DIGIT")
            self.print_group_simple_tlv_command_test_help()
            return

        # Check the destination.
        if destination_string == "SOLAR":
            ip_address = SG424_MULTICAST_GENERIC_SOLAR
        elif destination_string == "BATTERY":
            ip_address = SG424_MULTICAST_GENERIC_BATTERY
        elif destination_string == "ALL":
            ip_address = SG424_MULTICAST_GENERIC_ALL
        elif destination_string == "NON_GTW":
            ip_address = SG424_MULTICAST_NON_GATEWAY
        else:
            print("I ONLY KNOW OF SOLAR, BATTERY, ALL, NON_GTW")
            self.print_group_simple_tlv_command_test_help()
            return

        print("HEX TLV PACKED DATA TO SEND:")             # <- optional
        gu.print_this_hex_data(complete_packed_data)      # <- optional

        if gu.is_send_tlv_packed_data(ip_address, complete_packed_data):
            print("MCAST SEND TO %s SUCCESS" % (ip_address))
        else:
            print("MCAST SEND TO %s ... ***FAILED***" % (ip_address))

    def group_compound_tlv_command_test(self):

        if len(sys.argv) != 6:
            print("SEND GROUP COMPOUND TLV EXPECTS 4 USER ARGUMENTS")
            self.print_group_compound_tlv_command_test_help()
            return

        group_id_1_string = sys.argv[2]
        group_id_2_string = sys.argv[3]
        group_id_test_number_string = sys.argv[4]
        destination_string = sys.argv[5]
        destination_string = destination_string.upper()

        if group_id_1_string.isdigit() and group_id_2_string.isdigit():
            group_id_1 = int(group_id_1_string)
            group_id_2 = int(group_id_2_string)
            if (not group_id_1  > 0) or (not group_id_2 > 0):
                print("FIRST/SECOND PARAM GROUP ID MUST BE > 0")
                self.print_group_compound_tlv_command_test_help()
                return
        else:
            print("FIRST/SECOND PARAMs GROUP ID MUST BE DIGIT")
            self.print_group_compound_tlv_command_test_help()
            return

        # Only 4 test items are defined for compound group testing.
        complete_packed_data = None
        if group_id_test_number_string.isdigit():
            group_id_test_number = int(group_id_test_number_string)
            if group_id_test_number == 1:
                complete_packed_data = gu.build_compound_group_id_packed_tlv_test_4(group_id_1, group_id_2)
            elif group_id_test_number == 2:
                complete_packed_data = gu.build_compound_group_id_packed_tlv_test_5(group_id_1, group_id_2)
            elif group_id_test_number == 3:
                complete_packed_data = gu.build_compound_group_id_packed_tlv_test_6(group_id_1, group_id_2)
            elif group_id_test_number == 4:
                complete_packed_data = gu.build_compound_group_id_packed_tlv_test_7(group_id_1, group_id_2)

            elif group_id_test_number == 5:
                #complete_packed_data = gu.build_compound_group_id_packed_tlv_test_8(group_id_1, group_id_2)
                print("TEST ID 5 ... NOT DEFINED/CODED")
                return
            elif group_id_test_number == 6:
                #complete_packed_data = gu.build_compound_group_id_packed_tlv_test_9(group_id_1, group_id_2)
                print("TEST ID 6 ... NOT DEFINED/CODED")
                return
            elif group_id_test_number == 7:
                #complete_packed_data = gu.build_compound_group_id_packed_tlv_test_10(group_id_1, group_id_2)
                print("TEST ID 7 ... NOT DEFINED/CODED")
                return
            elif group_id_test_number == 8:
                #complete_packed_data = gu.build_compound_group_id_packed_tlv_test_11(group_id_1, group_id_2)
                print("TEST ID 8 ... NOT DEFINED/CODED")
                return
            elif group_id_test_number == 9:
                #complete_packed_data = gu.build_compound_group_id_packed_tlv_test_12(group_id_1, group_id_2)
                print("TEST ID 9 ... NOT DEFINED/CODED")
                return

            else:
                print("I ONLY KNOW OF GROUP TLV TEST 1 .. 9")
                self.print_group_compound_tlv_command_test_help()
                return
        else:
            print("FIRST PARAM GROUP ID TEST NUMBER MUST BE A DIGIT")
            self.print_group_compound_tlv_command_test_help()
            return

        # Check the destination.
        if destination_string == "SOLAR":
            ip_address = SG424_MULTICAST_GENERIC_SOLAR
        elif destination_string == "BATTERY":
            ip_address = SG424_MULTICAST_GENERIC_BATTERY
        elif destination_string == "ALL":
            ip_address = SG424_MULTICAST_GENERIC_ALL
        elif destination_string == "NON_GTW":
            ip_address = SG424_MULTICAST_NON_GATEWAY
        else:
            print("I ONLY KNOW OF SOLAR, BATTERY, ALL, NON_GTW")
            self.print_group_simple_tlv_command_test_help()
            return

        print("HEX TLV PACKED DATA TO SEND:")             # <- optional
        gu.print_this_hex_data(complete_packed_data)      # <- optional

        if gu.is_send_tlv_packed_data(ip_address, complete_packed_data):
            print("MCAST SEND TO %s SUCCESS" % (ip_address))
        else:
            print("MCAST SEND TO %s ... ***FAILED***" % (ip_address))

    def group_invalid_tlv_command_test(self):
        print("SEND GROUP INVALID TLV ... NOT IMPLEMENTED YET")
        return

    def send_curtail(self):
        if len(sys.argv) != 4:
            print("SEND CURTAIL EXPECTS 2 USER ARGUMENTS")
            self.print_send_curtail_help()
        else:

            curtail_value_string = sys.argv[2]
            comma_separated_arg = sys.argv[3]
            if curtail_value_string.isdigit():
                curtail_value = int(curtail_value_string)
                if (curtail_value < 0) or (curtail_value > 100):
                    print("CURTAIL VALUE MUST BE 0..100")
                else:
                    ip_list = []
                    if comma_separated_arg[0] == "SOLAR":
                        # Get solar-only multicast IP
                        ip_list.append(SG424_MULTICAST_GENERIC_SOLAR)
                    elif comma_separated_arg[0] == "BATTERY":
                        # Get battery-only multicast IP
                        ip_list.append(SG424_MULTICAST_GENERIC_BATTERY)
                    elif comma_separated_arg[0] == "ALL":
                        # Get all-inverters multicast IP
                        ip_list.append(SG424_MULTICAST_GENERIC_ALL)
                    else:
                        ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_arg)

                    if len(ip_list) == 0:
                        print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
                    else:
                        # With the user's list of inverters IP, send a simple curtailment command.
                        for this_ip in ip_list:
                            if gu.is_send_simple_curtail_packet(curtail_value, this_ip):
                                print("CURTAIL SENDTO %s ... OK" % (this_ip))
                            else:
                                print("CURTAIL SENDTO %s ... FAILED" % (this_ip))

            else:
                print("CURTAIL VALUE MUST BE 0..100")

    def start_feed_detect(self):
        update_string = "update %s set %s = %s;" % (FEED_DISCOVERY_TABLE,
                                                    FEED_DISCOVERY_TBL_DISCOVERY_COMMAND_COLUMN_NAME, "%s")
        update_args = (FEED_DISCOVERY_COMMAND_START_DISCOVERY)
        prepared_act_on_database(EXECUTE, update_string, update_args)

    def send_pcc_feed_setting(self):
        #                    [0]   [1]     [2]             [3]
        # Command format: gpts.py feed <A|B|C|NULL> <comma-separated thingies>

        # Only 2 user arguments following feed.
        number_user_arguments = len(sys.argv) - 2

        if number_user_arguments != 2:
            print("INVAID NUMBER OF ARGS FOLLOWING FEED")
            self.print_pcc_feed_help()
            return

        feed_specification = sys.argv[2]
        feed_specification = feed_specification.upper()
        comma_separated_thingies = sys.argv[3]

        db_feed_string = None
        inverter_feed_value = int(FEED_ALL_MULTICAST)     # value: 0
        inverter_pcc_value = int(PCC_ALL_MULTICAST)       # value: 0
        if feed_specification == "A":
            inverter_feed_value = int(FEED_A_MULTICAST)   # value: 1
            inverter_pcc_value = int(PCC_1_MULTICAST)     # value: 1
            db_feed_string = "A"
        elif feed_specification == "B":
            inverter_feed_value = int(FEED_B_MULTICAST)   # value: 2
            inverter_pcc_value = int(PCC_1_MULTICAST)     # value: 1
            db_feed_string = "B"
        elif feed_specification == "C":
            inverter_feed_value = int(FEED_C_MULTICAST)   # value: 4
            inverter_pcc_value = int(PCC_1_MULTICAST)     # value: 1
            db_feed_string = "C"
        elif feed_specification == "NULL":
            # Already set above, but do it again.
            db_feed_string = None
            inverter_feed_value = int(FEED_ALL_MULTICAST) # value: 0
            inverter_pcc_value = int(PCC_ALL_MULTICAST)   # value: 0
            pass
        else:
            # Only A/B/C/NULL acepted.
            print("FEED VALUE INVALID - TRY AGAIN")
            self.print_pcc_feed_help()
            return

        # Get the IP list.
        ip_list = []
        comma_separated_thingies = comma_separated_thingies.upper()
        if comma_separated_thingies == "ALL":
            ip_list = gu.get_database_total_inverters_ip_list()
        elif comma_separated_thingies == "SOLAR":
            ip_list = gu.get_database_solar_inverters_ip_list()
        elif comma_separated_thingies == "BATTERY":
            ip_list = gu.get_database_battery_inverters_ip_list()
        else:
            ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_thingies)

        # Now send.
        if ip_list:
            for each_ip in ip_list:
                if gu.is_send_feed_pcc_command(each_ip, inverter_pcc_value, inverter_feed_value):
                    print("%s -> SENT PCC/FEED %s/%s" % (each_ip, inverter_pcc_value, inverter_feed_value))
                else:
                    print("%s -> PCC/FEED %s/%s ... *SENDTO FAILED*" % (each_ip, inverter_pcc_value, inverter_feed_value))
                # Update the database. If the send failed, let the audit catch it.
                gu.update_inverters_feed_name_column_by_ip(db_feed_string, each_ip)
        else:
            print("HEY - comma-separated list of thingies did not contain anything!")

    def assign_pcs_unit_id(self):
        # Only 2 user arguments, so ...
        if len(sys.argv) == 4:
            # 1st argument: the PCS UNIT ID.
            pcs_unit_id_as_string = sys.argv[2]
            comma_separated_arg = sys.argv[3]

            if pcs_unit_id_as_string.isdigit():
                pcs_unit_id = int(pcs_unit_id_as_string)
                packed_data = gu.add_master_tlv(1)
                packed_data = gu.append_user_tlv_16bit_unsigned_short(packed_data, GTW_TO_SG424_PCS_UNIT_ID_CONFIG_U16_TLV_TYPE, pcs_unit_id)

                ip_list = []
                if comma_separated_arg[0] == "SOLAR":
                    # PCS UNIT ID 0 only is valid. Get solar-only multicast IP.
                    if pcs_unit_id == 0:
                        ip_list.append(SG424_MULTICAST_GENERIC_SOLAR)
                    else:
                        print("WILL NOT SEND NON-ZERO PCS UNIT ID TO ALL/SOLAR INVERTERS")
                elif comma_separated_arg[0] == "BATTERY":
                    # Get battery-only multicast IP
                    ip_list.append(SG424_MULTICAST_GENERIC_BATTERY)
                elif comma_separated_arg[0] == "ALL":
                    if pcs_unit_id == 0:
                        ip_list.append(SG424_MULTICAST_GENERIC_ALL)
                    else:
                        # TODO: maybe the system is comprised of BATTERY inverters only? CHECK PLEASE!!!
                        print("WILL NOT SEND NON-ZERO PCS UNIT ID TO ALL INVERTERS")
                else:
                    ip_list = gu.gimme_ip_list_from_comma_separated_thingies(comma_separated_arg)

                if len(ip_list) == 0:
                    print("NO IPs FOR ANY INVERTERS BASED ON ARGUMENT")
                else:
                    # With the user's list of inverters IP, update the database, and update the inverter.
                    for this_ip in ip_list:
                        gu.update_db_pcs_unit_id_by_ip(this_ip, pcs_unit_id)
                        this_string = "IP %s -> DB UPDATED WITH PCS UNIT ID %s ... INVERTER SEND " % (this_ip, pcs_unit_id)
                        if gu.is_send_tlv_packed_data(this_ip, packed_data):
                            # Now update the database.
                            this_string += "OK"
                        else:
                            this_string += "* SEND FAILED *"
                        print(this_string)
            else:
                print("1st argument PCS UNIT ID must be an integer ...")
                self.print_assign_pcs_unit_id_help()
        else:
            print("only 2 user arguments expected ...")
            self.print_assign_pcs_unit_id_help()

    def pcs_unit_set_power(self):
        # Only 3 user arguments, so ...
        if len(sys.argv) == 5:
            # Extract the id and curtail value. Both must be plain integers.
            pcs_unit_id_as_string = sys.argv[2]
            curtail_value_as_string = sys.argv[3]
            ramp_rate_as_float = float(sys.argv[4])
            if pcs_unit_id_as_string.isdigit() and curtail_value_as_string.isdigit():
                pcs_unit_id = int(pcs_unit_id_as_string)
                if pcs_unit_id > 0:
                    curtail_value = int(curtail_value_as_string)
                    sequence_number = SEQUENCE_NUMBER_INITIAL_STARTUP_VALUE + 1
                    if curtail_value <= 100:
                        # There are 5 user user TLVs encapsulated by START/END TLVs:
                        sub_group_data = ""
                        sub_group_data = gu.add_user_tlv_16bit_unsigned_short(GTW_TO_SG424_KEEP_ALIVE_SEQUENCE_NUMBER_U32_TLV_TYPE, sequence_number)
                        sub_group_data = gu.append_user_tlv_16bit_unsigned_short(sub_group_data, GTW_TO_SG424_REGULATION_U16_TLV_TYPE, curtail_value)
                        sub_group_data = gu.append_user_tlv_32bit_float(sub_group_data, GTW_TO_SG424_AIF_SETTINGS_NORMAL_RAMP_RATE_UP_PCT_RAM_ONLY_F32_TLV_TYPE, ramp_rate_as_float)
                        sub_group_length = len(sub_group_data)
                        number_of_user_tlv = 3
                        group_data_start = struct.pack('!HHH', GTW_TO_SG424_COMMAND_BY_PCS_UNIT_ID_TLV_TYPE, sub_group_length, int(self.pcs_unit_id))
                        number_of_user_tlv += 1
                        master_tlv = gu.add_master_tlv(number_of_user_tlv)
                        complete_packed_data = master_tlv + group_data_start + sub_group_data
                        multicast_ip = gu.construct_multicast_ip_gen2(INVERTER_MCAST_SG424_TYPE_BATTERY_ONLY, PCC_ALL_MULTICAST, FEED_ALL_MULTICAST)
                        if gu.is_send_tlv_packed_data(multicast_ip, complete_packed_data):
                            print("IP %s (MULTICAST BATTERY ONLY) -> SET ACTIVE POWER %sPCT/RR-UP=%s TO PCS UNIT ID %d SENT OK"
                                                           % (multicast_ip, curtail_value, ramp_rate_as_float, pcs_unit_id))
                        else:
                            print("IP %s (MULTICAST BATTERY ONLY) -> SET ACTIVE POWER ... * SEND FAILED*" % (multicast_ip))
                    else:
                        print("CURTAIL VALUE MUST BE 100 OR LESS ...")
                        self.print_pcs_unit_set_power_help()
                else:
                    print("PCS UNIT ID 0 NO LONGER ALLOWED")
                    self.print_pcs_unit_set_power_help()

            else:
                print("BOTH ARGUMENTS MUST BE DIGITS ...")
                self.print_pcs_unit_set_power_help()
        else:
            print("invalid number of arguments ...")
            self.print_pcs_unit_set_power_help()

        return

    def artesyn_cli_session(self):

        number_of_ess_units = gu.get_database_table_row_count(DB_ESS_UNITS_TABLE)
        if number_of_ess_units == 0:
            print("ARTESYN CLI: NO ESS UNITS IN DATABASE, NOTHING TO DO")
            return

        # -----------------------------------------------------------------------------------------
        # Note: ArtesynChassisRelayDaemon not included here; it is stopped/killed via os interaction.
        some_daemon_class = ArtesynControllerDaemon
        daemon_monitor = DaemonMonitor.find_or_create_all(some_daemon_class)
        daemon_monitor[0].save(enabled=True)
        self.daemon_monitor = daemon_monitor[0]
        # -----------------------------------------------------------------------------------------

        # The following daemons should not be running during the association process.
        # Kill the artesyn chassis relay daemons on a per-ESS basis. They will be immediately re-launched,
        # and a new queuing environment setup will be setup.
        print("PREPARING DAEMON ENVIRONMENT FOR ARTESYN CLI ...")
        for iggy in range(0, number_of_ess_units):
            # Kill the artesyn chassis relay daemons on a per-ESS basis. They will be immediately re-launched,
            # and a new queuing environment setup will be setup.
            artesyn_chassis_relay_name_by_ess = "%s-%s" % (ARTESYN_CHASSIS_RELAY_DAEMON_NAME, (iggy + 1))
            os_command_string = gu.kill_process_by_name(artesyn_chassis_relay_name_by_ess)
            if os_command_string:
                print("PROCESS NAME %s KILLED BY OS COMMAND: %s" % (artesyn_chassis_relay_name_by_ess, os_command_string))
            else:
                # Something not right here ...
                print("PROCESS NAME %s KILLED *FAILED* -> NO PROCESS BY THAT NAME" % (artesyn_chassis_relay_name_by_ess))

            # The following daemons should not be running during the association process.
            for index, this_daemon_name in enumerate(DAEMONS_FOR_ASSOCIATION_CONTROL):
                daemon_name_by_ess = "%s-%s" % (this_daemon_name, (iggy + 1))
                print("CLEAR ENABLED BIT FOR %s ..." % (daemon_name_by_ess))
                gu.update_daemon_control_enabled_by_name(daemon_name_by_ess, 0)

        # Launching the ArtesynControllerDaemon for charger interaction.
        print("START ArtesynControllerDaemon")
        gu.update_daemon_control_enabled_by_name("artesyncontrollerd", 1)
        d = self.daemon_monitor
        d.start()
        active_children()

        # Give a reasonable amount of time for both daemons to be fully launched and running.
        print("SHORT SLEEP TO ALLOW SETTLING DOWN ...")
        time.sleep(10)

        # Enter into a forever and ever loop, waiting for and analyzing user input.
        print("ENTER ARTESYN CLI COMMANDS - ENTER DONE BY ITSELF TO TERMINATE")
        print("ENTER h by itself for abbreviated artesyn CLI help ...")
        print("ENTER -h for expanded artesyn CLI help ...")
        print("TIMING THAT MAY BE REQUIRED AFTER A CLI IS UP TO YOU!!!")

        # -----------------------------------------------------------------------------------------
        wait_for_user_input = True
        while wait_for_user_input:
            user_input = raw_input("enter ARTESYN cli: ")

            if len(user_input) > 0:
                user_input_list = user_input.split(' ')
                check_restore = user_input_list[0]
                check_restore = check_restore.upper()
                if check_restore == "DONE":
                    wait_for_user_input = False
                    print("ARTESYN CLI DONE")
                else:
                    # Sleep a bit afterwards - the cli module itself does a print.
                    gu.send_artesyn_charger_cli_command(user_input, 0)
                    time.sleep(2)
            else:
                print("YOU REALLY OUGHT TO ENTER SOMETHING FOR ME TO DO ...")
        # -----------------------------------------------------------------------------------------

        print("RESTORING ARTESYN DAEMONS FOR NORMAL SYSTEM/BATTERY OPERATION")
        gu.update_daemon_control_enabled_by_name("artesyncontrollerd", 0)
        d = self.daemon_monitor
        d.stop()

        for iggy in range(0, number_of_ess_units):
            for index, this_daemon_name in enumerate(DAEMONS_FOR_ASSOCIATION_CONTROL):
                daemon_name_by_ess = "%s-%s" % (this_daemon_name, (iggy + 1))
                print("SET ENABLED BIT FOR %s ..." % (daemon_name_by_ess))
                gu.update_daemon_control_enabled_by_name(daemon_name_by_ess, 1)

            artesyn_chassis_relay_name_by_ess = "%s-%s" % (ARTESYN_CHASSIS_RELAY_DAEMON_NAME, (iggy + 1))
            os_command_string = gu.kill_process_by_name(artesyn_chassis_relay_name_by_ess)
            if os_command_string:
                print("PROCESS NAME %s KILLED BY OS COMMAND: %s" % (artesyn_chassis_relay_name_by_ess, os_command_string))
            else:
                # Something not right here ...
                print("PROCESS NAME %s KILLED *FAILED* -> NO PROCESS BY THAT NAME" % (artesyn_chassis_relay_name_by_ess))

        # Give a reasonable amount of time for both daemons to be fully launched and running.
        time.sleep(10)
        print("ARTESYN DAEMONS RESTORED - SYSTEM BACK TO NORMAL")

    def gimme_user_command_index(self, user_argument):
        gpts_command_index = INDEXOF_LAST_AND_INVALID_COMMAND
        gpts_command_handler = ""
        gpts_command_helper = ""

        try:
            gpts_tuple = gpts_command_lookup_table[user_argument]
            gpts_command_index = gpts_tuple.index
            gpts_command_handler = gpts_tuple.handler
            gpts_command_helper = gpts_tuple.helper
        except KeyError:
            # User command not in lookup table. The full help text will be provided.
            pass

        return (gpts_command_index, gpts_command_handler, gpts_command_helper)

    def is_second_arg_help(self):
        help_requested = False
        if len(sys.argv) == 3:
            # User entered something like:
            #  sudo python gpts.py some_command something_else
            #                [0]        [1]         [2]
            # Is the last argument a cry for help?
            second_arg = str(sys.argv[2])
            second_arg = second_arg.upper()
            if second_arg == "HELP" or second_arg == "H":
                # yes, it's a cry for help
                help_requested = True
        return help_requested

    def print_full_help_menu(self):

        length = len(gpts_command_lookup_table)
        sorted_commands = [None] * length

        for cmd in gpts_command_lookup_table.values():
            # Add to the index where the command says it wants to be in the list
            sorted_commands[cmd.index] = cmd

        for i in range(length):
            # DO NOT PRINT COMMANDS WITH INDEX 1 OR 2 (unless you like infinite loops!)
            if sorted_commands[i] != None and sorted_commands[i].index >= 2:
                sorted_commands[i].helper(self)


    def print_set_keep_alive_help(self):
        print("- set keepAlive time config:          gpts.py %s         <miliseconds> <ALL | SOLAR | BATTERY | comma-separated thingies>" % (GPTS_COMMAND_SET_KEEP_ALIVE))
        print("                                                                (ALL, SOLAR, BATTERY: multicasted)")

    def print_plain_reset_by_inv_type_help(self):
        print("- plain reset by inverter type:       gpts.py %s         <ALL | SOLAR | BATTERY>" % (GPTS_COMMAND_PLAIN_RESET_BY_INV_TYPE))
        print("                                                                (command is multicasted)")

    def print_upgrade_reset_by_inv_type_help(self):
        print("- upgrade reset by inverter tpe:      gpts.py %s         <ALL | SOLAR | BATTERY >" % (GPTS_COMMAND_UPGRADE_RESET_BY_INV_TYPE))
        print("                                                                (command is multicasted)")

    def print_plain_reset_by_ip_help(self):
        print("- plain reset by IP/string ID list:   gpts.py %s         <ALL | SOLAR | BATTERY | comma-separated thingies>" % (GPTS_COMMAND_PLAIN_RESET_BY_IP))
        print("                                                                (ALL, SOLAR, BATTERY: command is multicasted)")

    def print_upgrade_reset_by_ip_help(self):
        print("- upgrade reset by IP/string ID list: gpts.py %s         <ALL | SOLAR | BATTERY | comma-separated thingies>" % (GPTS_COMMAND_UPGRADE_RESET_BY_IP))
        print("                                                                (ALL, SOLAR, BATTERY: command is multicasted)")

    def print_plain_reset_by_mac_help(self):
        print("- plain reset by MAC list:            gpts.py %s         <comma-separated MAC address list>     <- command is multicasted to ALL" % (GPTS_COMMAND_PLAIN_RESET_BY_MAC))

    def print_upgrade_reset_by_mac_help(self):
        print("- upgrade reset by MAC list:          gpts.py %s         <comma-separated MAC address list>     <- command is multicasted to ALL" % (GPTS_COMMAND_UPGRADE_RESET_BY_MAC))

    def print_plain_reset_by_sn_help(self):
        print("- plain reset by SN list:             gpts.py %s         <comma-separated serial number list>   <- command is multicasted to ALL" % (GPTS_COMMAND_PLAIN_RESET_BY_SN))

    def print_upgrade_reset_by_sn_help(self):
        print("- upgrade reset by SN list:           gpts.py %s         <comma-separated serial number list>   <- command is multicasted to ALL" % (GPTS_COMMAND_UPGRADE_RESET_BY_SN))

    def print_upgrade_reset_by_http_help(self):
        print("- HTTP upgrade reset:                 gpts.py %s         <comma-separated thingies>" % (GPTS_COMMAND_UPGRADE_RESET_BY_HTTP))
        print("                                                                (ALL, SOLAR, BATTERY not allowed)")

    # ---------------------------------------------------------------------------------

    def print_send_cli_help(self):
        print("- send cli/tlv:                       gpts.py %s         <\"cli command of your dreams\">  <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_SEND_CLI))
        print("                                                                (ALL, SOLAR, BATTERY: command is multicasted)")

    def print_xet_info_by_nbns_help(self):
        print("- nbns get info:                      gpts.py nbns              <%s | %s | %s | %s> <comma-separated thingies>" % (XET_INFO_REQUEST_STRING_NODESTATUS,
                                                                                                                                  XET_INFO_REQUEST_STRING_XETINFO,
                                                                                                                                  XET_INFO_REQUEST_STRING_NAMESERVICE,
                                                                                                                                  XET_INFO_REQUEST_STRING_ALL))
        print("                                                                (ALL, SOLAR, BATTERY not allowed)")

    def print_query_help(self):
        print("- inverter QUERY:                     gpts.py query             <ALL|SOLAR|BATTERY|comma-separated thingies>")

    def print_send_keep_alive_test_help(self):
        print("- send keepAlive test:                gpts.py ska_test          <uni/multi ip> <timeout, msecs>")

    def print_full_upgrade_loop_test_help(self):
        print("- full upgrade looping:               gpts.py fut               <1st boot rev> <2nd boot rev> <number of upgrades>")

    def print_inverters_comm_help(self):
        print("- inverter comm/watts:                gpts.py comm              deprecated - replaced with psa")

    def print_dc_output_string_help(self):
        print("- dc output at frequency:             gpts.py %s                <0=once only | X secs (X=float or integer> <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_DC))

    def print_dc_gwa_output_string_help(self):
        print("- dc gwa output at frequency:         gpts.py %s                <0=once only | X secs (X=float or integer> <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_DC_GWA))

    def print_itca_help(self):
        print("- inverters table consistency audit:  gpts.py itca              <delete <- optional (rm /tmp/itca.txt)")

    def print_gidc_help(self):
        print("- get all inverters debug counters:   gpts.py gidc              <delete <- optional (rm /var/log/gidc>)")
        print("                                                                <comma-separated IP list> <- optional (if not set, get from all inverters in database)")
        print("                                                                <delay=x.y> <- optional delay x.y seconds between queries, else no delay")

    def print_dica_help(self):
        print("- db/inverters consistency audit:     gpts.py dica              optional -> devices_to_query; defaults to ALL")
        print("                                                                devices_to_query: ALL|SOLAR|BATTERY|comma-separated thingies")

    def print_psa_help(self):
        print("- inverters power status audit:      gpts.py psa                <delete <- optional (rm /var/log/psad.log)>")
        print("                                                                <comma-separated IP list> <- optional (if not set, get from all inverters in database)")
        print("                                                                <delay=x.y> <- optional delay x.y seconds between queries, else no delay")
        print("                                                                <decode> <- optional, if True and single IP Address input then response will be decoded ")
        print("                                                                <mcastAll> <- optional, if True then multicast all query is transmitted ")
        print("                                                                <mcastHos> <- optional, if True then multicast Hos query is transmitted ")

    def print_qip_help(self):
        print("- quick inverters ping:               gpts.py qip               <ALL | SOLAR | BATTERY> (<- will wait for responses forf 15sec period of time)")

    def print_iidc_help(self):
        print("- init inverter debug counters:       gpts.py iidc              <comma-separated IP list>")
        print("                                                                (if no IP, command is multicasted to ALL)")

    def print_set_clear_inv_ctrl_help(self):
        print("- SET or CLEAR SG424 InvCtrl:         gpts.py inv_ctrl          <%s | %s | %s | %s> <SET | CLEAR> <ALL|SOLAR|BATTERY|comma-separated thingies>" %
                                                                                  (INVERTER_CONTROL_BIT_GATEWAY,
                                                                                   INVERTER_CONTROL_BIT_LOW_POWER,
                                                                                   INVERTER_CONTROL_BIT_SLEEP,
                                                                                   INVERTER_CONTROL_BIT_ALL))

    def print_pdiu_help(self):
        print("- Prepare Deferred Inverter Upgrade:  gpts.py %s              <upgrade session file> <SG424 hex name/path> <IP/STRING ID comma-separated list>"
                                                                             % (GPTS_COMMAND_PDIU))

    def print_ldiu_help(self):
        print("- Launch Deferred Inverter Upgrade:   gpts.py %s              <upgrade session file>" % (GPTS_COMMAND_LDIU))

    def print_start_feed_detect_help(self):
        print("- Start Feed Detect:                  gpts.py sfd               ... no params")

    def print_assign_pcs_unit_id_help(self):
        print("- assign PCS UNIT ID:                 gpts.py %s  <PCS UNIT ID 0..65535> <ALL | SOLAR | BATTERY | comma-separated thingies>" % (GPTS_COMMAND_ASSIGN_PCS_UNIT_ID))
        print("                                      (ALL, SOLAR, BATTERY is multicasted, only BATTERY can have non-zero PCS UNIT ID)")

    def print_pcs_unit_set_power_help(self):
        print("- PCS UNIT SET ACTIVE POWER:          gpts.py %s  <PCS UNIT ID 1..65535> <curtail: 0..100> <RAMP RATE PERCENT AS FLOAT>" % (GPTS_COMMAND_PCS_UNIT_SET_POWER))
        print("                                      (multicast to ALL BATTERY INVERTERS WITH ENCAPSULATED CURTAIL VALUE)")

    def print_get_catch_up_reports_help(self):
        print("- get catch-up reports:               gpts.py %s <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_GET_CATCH_UP_REPORTS))

    def print_pcs_unit_id_good_tlv_test_help(self):
        print("- good encapsulated PCS UNIT ID TEST: gpts.py %s <1> <GROUP ID> <# OF TLVs> <IP>" % (GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST))
        print("                          or:         gpts.py %s <2> <GROUP ID #1> <# OF TLVs> <GROUP ID #2> <# OF TLVs> <IP>" % (GPTS_COMMAND_PCS_UNIT_ID_GOOD_TLV_TEST))
        print("                                      (if no IP, then multicasted to ALL)")
        print("  where:                              1 = simple START/END encapsulated TLV with user-specified number of TLVs")
        print("                                      2 = START/END encap'd TLV batch 1 + CLI TLV + START/END encap'd TLV batch 2 + NTP_URL TLV")

    def print_pcs_unit_id_bad_tlv_test_help(self):
        print("- bad encapsulated PCS UNIT ID TEST:  gpts.py %s <test #1 .. 5> <GROUP ID> <comma-separated IP> " % (GPTS_COMMAND_PCS_UNIT_ID_BAD_TLV_TEST))
        print("                                      (if no IP, then multicasted to ALL)")
        print("  where test #:                       1 = %s" % (PCS_UNIT_ID_BAD_TLV_TEST_1_TITLE))
        print("                                      2 = %s" % (PCS_UNIT_ID_BAD_TLV_TEST_2_TITLE))
        print("                                      3 = %s" % (PCS_UNIT_ID_BAD_TLV_TEST_3_TITLE))
        print("                                      4 = %s" % (PCS_UNIT_ID_BAD_TLV_TEST_4_TITLE))
        print("                                      5 = %s" % (PCS_UNIT_ID_BAD_TLV_TEST_5_TITLE))

    def print_mom_help(self):
        print("- mom output at specified frequency:  gpts.py mom               <0=once only | X secs (X=float or integer)> <uni ip>")

    def print_forced_inverter_upgrade_help(self):
        print("- forced inverter upgrade:            gpts.py %s <no paramers>" % (GPTS_COMMAND_FORCED_INVERTER_UPGRADE))
        print("                                           -> forced upgrade of all communicating inverters (SG424_APP, BOOT_LOADER, BOOT_UPDATER")
        print("                                              based on forced upgrade parameters in inverter_upgrade control table")

    def print_get_eerpt_info_help(self):
        print("- get eerpt info:                     gpts.py %s <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_GET_EERPT_INFO))
        return

    def print_set_eerpt_info_help(self):
        print("- set eerpt info:                     gpts.py %s <1..%s params with values> <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_SET_EERPT_INFO,
                                                                                                                                            NUMBER_OF_MANAGED_EERPT_PARAMETERS))
        print("                                                                <%s> <ENABLE: 0 | 1>"                             % (CMD_PARAM_SET_EERPT_SERVER_ENABLE))
        print("                                                                <%s> <RATE: 0 .. 65K (typically: 900>"            % (CMD_PARAM_SET_EERPT_SERVER_RATE))
        print("                                                                <%s> <URL: bleh.com (e.g.: www.xetinc.com)>"      % (CMD_PARAM_SET_EERPT_SERVER_URL))
        print("                                                                <%s> <PORT: any integer (typically: 8180 or 80)>" % (CMD_PARAM_SET_EERPT_SERVER_PORT))
        print("                                                                <%s> <not any dumb user>"                         % (CMD_PARAM_SET_EERPT_SERVER_USER_NAME))
        print("                                                                <%s> <not any dumb password>"                     % (CMD_PARAM_SET_EERPT_SERVER_PASSWORD))
        return

    def print_get_aif_settings_help(self):
        print("- get EEPROM aif settings data:       gpts.py get_aif_settings  <ALL|SOLAR|BATTERY|comma-separated thingies>")
        print("                                                                (if ALL, SOLAR, BATTERY: get from each inverter via unicast>")

    def print_get_aif_immed_help(self):
        print("- get EEPROM aif immed data:          gpts.py get_aif_immed     <ALL|SOLAR|BATTERY|comma-separated thingies>")
        print("                                                                (if ALL, SOLAR, BATTERY: get from each inverter via unicast>")

    def print_set_aif_settings_help(self):
        print("- set EEPROM aif settings group data: gpts.py set_aif_settings  <1..14 params with value> <ALL|SOLAR|BATTERY|comma-separated thingies>")
        print("                                           (ALL, SOLAR, BATTERY: command is unicasted to each IP in a constructed list")
        print("                                           params in any order: <%s> <-10.0 .. 10.0>"  % (CMD_PARAM_AIF_SETTINGS_AC_V_OFFSET))
        print("                                                                <%s> <26.0 .. 250.0>"  % (CMD_PARAM_AIF_SETTINGS_MAX_OUT_PWR))
        print("                                                                <%s> <0.1 .. 100>"     % (CMD_PARAM_AIF_SETTINGS_RAMP_RATE_UP))
        print("                                                                <%s> <0.1 .. 100>"     % (CMD_PARAM_AIF_SETTINGS_RAMP_RATE_DOWN))
        print("                                                                <%s> <0.1 .. 100>"     % (CMD_PARAM_AIF_SETTINGS_RECONN_RR_PCT))
        print("                                                                <%s> <0..600000>"      % (CMD_PARAM_AIF_SETTINGS_RTS_DEL_MSEC))
        print("                                                                <%s> <36.76 .. 325.0>" % (CMD_PARAM_AIF_SETTINGS_MAX_OUT_VA))
        print("                                                                <%s> <26.0 .. 230.0>"  % (CMD_PARAM_AIF_SETTINGS_MAX_OUT_VAR_Q1Q4))
        print("                                                                <%s> <0.707 .. 1.0>"   % (CMD_PARAM_AIF_SETTINGS_MIN_PF_Q1))
        print("                                                                <%s> <0.707 .. 1.0>"   % (CMD_PARAM_AIF_SETTINGS_MIN_PF_Q4))
        print("                                                                <%s> <1.0 .. 100.0>"   % (CMD_PARAM_AIF_SETTINGS_OUT_VAR_RR_PCT))
        print("                                                                <%s> <0.10 .. 2.0>"    % (CMD_PARAM_AIF_SETTINGS_V_WAVER))
        print("                                                                <%s> <0|1>"            % (CMD_PARAM_AIF_SETTINGS_V_WAVER_ENA))
        print("                                                                <%s> <0.0 .. 25.0>"    % (CMD_PARAM_AIF_SETTINGS_W_WAVER))
        print("                                                                <%s> <0|1> (0=REAL, 1=REACTIVE" % (CMD_PARAM_AIF_SETTINGS_REAL_REACT_PRIO))

    def print_set_aif_immed_help(self):
        print("- set EEPROM aif immed group data:    gpts.py set_aif_immed     <1..8 params with value> <ALL|SOLAR|BATTERY|comma-separated thingies>")
        print("                                           (ALL, SOLAR, BATTERY: command is unicasted to each IP in a constructed list)")
        print("                                           params in any order: <%s> < 0|1>"           % (CMD_PARAM_AIF_IMMED_CONNECT))
        print("                                                                <%s> < 10.0 .. 100.0>" % (CMD_PARAM_AIF_IMMED_PWR_LIMIT_PCT))
        print("                                                                <%s> < 0|1>"           % (CMD_PARAM_AIF_IMMED_PWR_LIMIT_ENABLE))
        print("                                                                <%s> <0.707 .. 1.0>"   % (CMD_PARAM_AIF_IMMED_POWER_FACTOR))
        print("                                                                <%s> < 0|1>"           % (CMD_PARAM_AIF_IMMED_POWER_FACTOR_C_LEAD))
        print("                                                                <%s> < 0|1>"           % (CMD_PARAM_AIF_IMMED_POWER_FACTOR_ENABLE))
        print("                                                                <%s> < 10.0 .. 100.0>" % (CMD_PARAM_AIF_IMMED_VAR_LIMIT_PCT))
        print("                                                                <%s> < 0|1>"           % (CMD_PARAM_AIF_IMMED_VAR_LIMIT_ENABLE))

    def print_dymanic_pf_help(self):
        print("- dynamic PF (1 feed):                gpts.py dynamic_pf        <A|B|C> <0.707 to 1.0> <0 or 1>")
        print("- dynamic PF (2 feeds):               gpts.py dynamic_pf        <A|B|C> <0.707 to 1.0> <0 or 1> <A|B|C> <0.707 to 1.0> <0 or 1>")
        print("- dynamic PF (3 feeds):               gpts.py dynamic_pf        <A|B|C> <0.707 to 1.0> <0 or 1> <A|B|C> <0.707 to 1.0> <0 or 1> <A|B|C> <0.707 to 1.0> <0 or 1>")
        print("- dynamic PF (all feeds):             gpts.py dynamic_pf        <ALL> <0.707 to 1.0> <0 or 1>")
        print("      examples: gpts.py dynamic_pf A 0.777 1                       <- feed A only")
        print("                gpts.py dynamic_pf B 0.777 1 C 0.888 0             <- feeds B and C (feed in any order)")
        print("                gpts.py dynamic_pf A 0.777 1 C 0.888 0 B 0.999 1   <- feeds A C B (feed in any order)")
        print("                gpts.py dynamic_pf ALL 0.777 1                     <- all feeds")
        return

    def print_universal_aif_help(self):
        print("- UNIVERSAL_AIF region name:          gpts.py universal_aif <CA120 | HI120 | IEEE | NA120 | MFG120>")

    def print_gateway_operation_mode_help(self):
        print("- Gateway operation mode:             gpts.py GOM               <%s | %s>" % (GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE,
                                                                                             GATEWAY_TABLE_GATEWAY_OPERATION_MODE_OBSERVATION_MODE))

    def print_start_pui_association_help(self):
        print("- launch inverter/PCS association:    gpts.py %s <ALL|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_START_PUI_ASSOC))
        print("                                                          (only battery inverters will be selected)")

    def print_start_charger_association_help(self):
        print("- launch charger/PCS association:     gpts.py %s <FULL | comma-separated list of charger ID>" % (GPTS_COMMAND_START_CHARGER_PCS_ASSOC))

    def print_show_bmu_soc_pairs_help(self):
        print("- show SOC for BMU pairs:             gpts.py %s  ***NO PARAMETERS***" % (GPTS_COMMAND_SHOW_BMU_SOC_PAIRS))


    def print_group_simple_tlv_command_test_help(self):
        print("- send SIMPLE TEST group tlv:  gpts.py %s <GROUP ID> <TEST_ID> <ALL|SOLAR|BATTERY|NON_GTW>" % (GPTS_COMMAND_GROUP_SIMPLE_TLV_TEST))
        print("                  where TEST_ID will send:")
        print("                      - TEST_ID 1: group ID X with 3 fixed sub-TLVs")
        print("                      - TEST_ID 2: group ID X with 4 fixed sub-TLVs")
        print("                      - TEST_ID 3: group ID X with 5 fixed sub-TLVs")

    def print_group_compound_tlv_command_test_help(self):
        print("- send COMPOUND TEST group tlv:  gpts.py %s <GROUP ID X> <GROUP ID Y> <TEST_ID> <ALL|SOLAR|BATTERY|NON_GTW>" % (GPTS_COMMAND_GROUP_COMPOUND_TLV_TEST))
        print("                  where TEST_ID ID will send:")
        print("                      - TEST_ID 1: group ID x(3 fixed sub-TLVs) + group ID y(6 fixed sub-TLVs)")
        print("                      - TEST_ID 2: group ID x(5 fixed sub-TLVs) + group ID y(4 fixed sub-TLVs)")
        print("                      - TEST_ID 3: group ID x(3 fixed sub-TLVs) + 1 generic TLV + group ID y(5 fixed sub-TLVs) + 1 more generic TLV")
        print("                      - TEST_ID 4: generic TLV + group ID x(5 fixed sub-TLVs) + generic TLV + generic TLV + group ID y(2 fixed sub-TLVs)")
        print("                      - TEST_ID 5: NOT DEFINED")
        print("                      - TEST_ID 6: NOT DEFINED")
        print("                      - TEST_ID 7: NOT DEFINED")
        print("                      - TEST_ID 8: NOT DEFINED")
        print("                      - TEST_ID 9: NOT DEFINED")

    def print_group_invalid_tlv_command_test_help(self):
        print("- send INVALID TEST group tlv:  gpts.py %s NOT IMPLEMENTED YET>" % (GPTS_COMMAND_GROUP_INVALID_TLV_TEST))

    def print_send_curtail_help(self):
        print("- send curtail command:               gpts.py %s <curtail value 0..100> <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_SEND_CURTAIL))
        print("                                           (ALL, SOLAR, BATTERY: curtail command is multicasted)")

    def print_pcc_feed_help(self):
        print("- feed setting:                       gpts.py %s <A | B | C | NULL> <ALL|SOLAR|BATTERY|comma-separated thingies>" % (GPTS_COMMAND_PCC_FEED))
        print("                                                                (NOTE: PCC=1 if feed=A/B/C else PCC=0)")

    def print_iqm_help(self):
        GptsTlvQuery.print_query_help()

    def print_packed_counters_help(self):
        GptsCounterQuery.print_query_help()


    def print_artesyn_cli_session_help(self):
        print("- artesyn CLI session:                gpts.py %s" % (GPTS_COMMAND_ARTESYN_CLI_SESSION))

    # -------------------------------------------------------------------------
    #
    def run_general_purpose_tlv_send_script(self):

        if len(sys.argv) <= 1:
            # No arguments beyond the command name itself. User doesn't know what he/she wants.
            command_index = INDEXOF_HELP_COMMAND
            handler = lookup_help
        else:
            # 1 or more arguments. Evaluate the 1st argument, converted to uppercase.
            first_user_arg = str(sys.argv[1])
            first_user_arg = first_user_arg.upper()
            (command_index, handler, helper) = self.gimme_user_command_index(first_user_arg)

        if command_index != INDEXOF_LAST_AND_INVALID_COMMAND:
            # User entered a recognized facility of the GPTS. Does the user want to invoke a
            # specific command, or asking for help for a particular command?
            if self.is_second_arg_help():
                # Help the poor bugger ...
                helper(self)
            else:
                # Invoke the handler for the desired command.
                handler(self)
        else:
            print("NO COMPRENDO SENOR, YOU NEED HELP ...")
            self.print_full_help_menu()

# CORE OF THE SCRIPT. THIS SCRIPT IS NOT A DAEMON.

# ----------------------------------
execute_general_purpose_tlv_send_script()   # <- IT'S ALL ABOUT ME, ME, ME!!!
# ----------------------------------
