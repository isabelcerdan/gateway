#!/usr/share/apparent/.py2-virtualenv/bin/python
"""
@module: CSV Logger

@description: Use CSV Logger to log data to a CSV file. Automatically
    handles writing to a new file after time duration has elapsed for
    the unit of time specified.

@copyright: Apparent Inc. 2019

@reference:

@author: patrice

@created: February 21, 2019
"""

import os
import datetime
import time
import collections


class MissingCsvHeader(Exception):
    pass

class ReadingsLogger(object):
    READINGS_RAW_PATH = "/var/lib/apparent/readings_raw"

    def __init__(self, **kwargs):
        """

        :param kwargs:
        basename: string used as basename of file.
        time_duration: time duration to write to current file (units defined by unit_of_time) (default=1).
        unit_of_time: minutes, hours, days, months, and years that defines time_duration units (default=days)
        dir_path: directory path where files should be written (default=/var/lib/apparent/readings_data)
        header: list of strings representing CSV header to be written to file.
            not required if rows are passed in as collections.OrderedDict or just dict.
        delimiter: cell delimiter in a row (default=',')

        """
        self.basename = kwargs.get('basename', '')
        self.time_duration = kwargs.get('time_duration', 1)
        self.unit_of_time = kwargs.get('unit_of_time', 'days')
        self.dir_path = kwargs.get('dir_path', self.READINGS_RAW_PATH)
        self.header = kwargs.get('header', [])
        self.delimiter = kwargs.get('delimiter', ',')
        self.dt = None  # current file datetime
        self.f = None   # file handle
        self.full_path = None
        self.dumpPath = kwargs.get('dumpPath', "/var/log/apparent/export_readings.log")


    def _log(self, logStr):
        try:
            self.dumpFile = open(self.dumpPath, 'a')
            now = time.time()
            nowStr = time.strftime(" %H:%M:%S.")
            msec = int(round(now * 1000)) % 1000
            self.dumpFile.write(str(os.getpid()) + nowStr + ("%03u " % msec) + logStr)

            self.dumpFile.write("\r\n")
            self.dumpFile.flush()
            self.dumpFile.close()

        except Exception:  # as err_string:
            print("FAILED TO OPEN file = " + str(self.dumpPath))

    def _duration(self):
        """
        Converter time_duration in unit_of_time to a timedelta object.
        :return:
        """
        kwargs = {self.unit_of_time: self.time_duration - 1}
        return datetime.timedelta(**kwargs)

    def _now(self):
        """
        Truncate datetime now to unit_of_time.

        For example:
        - if unit_of_time="minutes" then "Feb 27, 2019 14:37:43" returns "Feb 27, 2019 14:37:00"
        - if unit_of_time="hours" then "Feb 27, 2019 14:37:43" returns "Feb 27, 2019 14:00:00"
        - if unit_of_time="days" then "Feb 27, 2019 14:37:43" returns "Feb 27, 2019 00:00:00"
        :return:
        """
        now = datetime.datetime.now()
        if self.unit_of_time == 'minutes':
            return datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, 0)
        elif self.unit_of_time == 'hours':
            return datetime.datetime(now.year, now.month, now.day, now.hour, 0, 0)
        elif self.unit_of_time == 'days':
            return datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
        elif self.unit_of_time == 'months':
            return datetime.datetime(now.year, now.month, 0, 0, 0, 0)
        elif self.unit_of_time == 'years':
            return datetime.datetime(now.year, 0, 0, 0, 0, 0)

    def _file_exists(self):
        """
        Return true if file exists in file system and is size > 0
        :return:
        """
        retVal = True

        if self.full_path == None:
            retVal = False
        else:
            try:
                if os.stat(self.full_path).st_size == 0:
                    retVal = False
            except OSError:
                retVal = False

        return retVal

    def _create_header(self, row):
        """
        Create the header if it does not exist.
        Used for log file and parsing ordered pair data
        :param row: dict or OrderedDict where keys are header strings.
        :return: None
        """

        if len(self.header) == 0:
            if type(row) is dict or \
               type(row) is list or \
               type(row) is collections.OrderedDict:
                self.header = row.keys()
            elif type(row) is tuple:
                raise MissingCsvHeader("Missing Csv header defining columns. Data is a tuple")

            elif len(self.header) == 0:
                raise MissingCsvHeader("Missing Csv header defining columns.")

    def _write_header(self, row):
        """
        Write CSV header with comma separated header strings to current file.
        Add Timestamp to beginning of header
        :param row: dict or OrderedDict where keys are header strings.
        :return: None
        """
        self._create_header(row)
        header_str = "Timestamp,"
        header_str += ",".join(self.header) + "\n"
        self.f.write(header_str)
        self.f.flush()
        self._log((" Writing Header for %s" % self.full_path))

    unit_to_strftime = {
        'months': '%Y%m',
        'days': '%Y%m%d',
        'hours': '%Y%m%d%H',
        'minutes': '%Y%m%d%H%M'
    }

    def _file_path(self):
        """
        Derive full file path including basename, timestamp and .csv suffix
        from the current datetime in minutes.
        :return:
        """
        ftime = self.unit_to_strftime['minutes']
        now = datetime.datetime.now()
        filename = "-".join([self.basename, now.strftime(ftime)]) + ".csv"
        full_path = os.path.join(self.dir_path, filename)
        return full_path

    def _get_log_file_handle(self):
        """
        Close the current file if open and create a new file using self._file_path().
        :return:
        """
        if self.f is not None:
            self.f.close()

        self.full_path = self._file_path()
        if self._file_exists():
            needToWriteHeader = False
        else:
            needToWriteHeader = True

        self.f = open(self.full_path, 'a')

        self._log((" Opening file %s" % self.full_path +
                   " need to write header= " + str(needToWriteHeader)))

        return needToWriteHeader

    def _time_for_new_file(self):
        """
        Check if its time to create a new file based on time_duration,
        unit_of_time and current time.
        :return:
        """
        now = self._now()

        if self.dt is None or \
           (now > self.dt + self._duration()):
            self.dt = now
            retVal = True
        else:
            if not self._file_exists():
                retVal = True
            else:
                retVal = False

        return retVal

    def write_reading(self, reading):
        """
        Write a new row of reading data to CSV log. This method will automatically
        create a new file if the time has passed the time duration window.
        :param row: list of integer,floats,strings or dict of header and values
        :return:
        """

        if self._time_for_new_file():
            # If the process restarts make sure to created header
            # for ordered dictionary data processing.
            if len(self.header) == 0:
                self._create_header(reading)

            needToWriteHeader = self._get_log_file_handle()
            if needToWriteHeader:
                self._write_header(reading)

        if type(reading) is dict or type(reading) is collections.OrderedDict:
            values = [str(reading[h]) for h in self.header]
        else:
            values = [str(cell) for cell in reading]

        now = time.time()
        msec = int(round(now * 1000)) % 1000
        nowStr = time.strftime("%Y-%m-%d %H:%M:%S")
        self.f.write(nowStr + (".%03u," % msec))
        self.f.write(self.delimiter.join(values) + "\n")
        self.f.flush()


