import lib.db as db
from lib.helpers import merge_dicts


class ModelException(Exception):
    pass


class RecordNotFound(ModelException):
    pass


class MissingSqlTable(ModelException):
    pass


class UnidentifiableRecord(ModelException):
    pass


class Model(object):
    """
    Light-weight ORM (Object Relational Mapping) that provides the following
    ActiveRecord like interfaces:

        <DerivedClass>.find_by(**kwargs)
        <DerivedClass>.find_all_by(**kwargs)
        <DerivedClass>.find_all()
        <DerivedClass>.delete_by(**kwargs)
        <DerivedClass>.count(**kwargs)
        <DerivedClass>.save(**kwargs)

    Derived class must override sql_table class variable to name of the MySQL table.
    Derived class constructor should set an instance variable for each field initialized
    to values from kwargs dictionary.
    """
    sql_table = None
    id_field = None

    @staticmethod
    def table(cls):
        """
        Use sql_table for table name otherwise default to pluralized lower-case class name.
        :param cls:
        :return: MySQL table name this model is associated with.
        """
        if cls.sql_table is None:
            raise MissingSqlTable("Class %s must define sql_table class variable to use Model base class." % cls.__name__)

        return cls.sql_table

    @classmethod
    def find_by(cls, **kwargs):
        """
        Find first record matching criteria in kwargs.

            Examples:
            inverter = Inverter.find_by(serialnumber='10002302032')
            inverter = Inverter.find_by(ip_address='10.252.2.8')

        :param kwargs:
        :return: List of objects of type class.
        """
        extra = kwargs.pop('extra', None)
        sql = "SELECT * FROM %s" % Model.table(cls)
        where_clause = " AND ".join(["%s='%s'" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        record = db.prepared_act_on_database(db.FETCH_ONE, sql, [])
        if record:
            if extra is not None:
                record = merge_dicts(record, extra)
            return cls(**record)
        else:
            raise RecordNotFound("Record not found: sql=\"%s\"" % sql)

    @classmethod
    def find_or_create_by(cls, **kwargs):
        try:
            obj = cls.find_by(**kwargs)
        except RecordNotFound:
            columns = ",".join(kwargs.keys())
            values = ",".join(["'%s'" % str(v) for v in kwargs.values()])
            sql = "INSERT INTO %s (%s) VALUES (%s)" % (Model.table(cls), columns, values)
            db.prepared_act_on_database(db.EXECUTE, sql, [])
            obj = cls.find_by(**kwargs)
        return obj

    @classmethod
    def find_all_by(cls, **kwargs):
        """
        Find all records matching kwargs criteria.

            Examples:
            hos_inverters = Inverter.find_all_by(string_position=0)
            inverters_in_string = Inverter.find_all_by(stringId='03:4f:e8')

        :param kwargs:
        :return: Single instance of object of type class or None.
        """
        extra = kwargs.pop('extra', None)
        sql = "SELECT * FROM %s" % Model.table(cls)
        where_clause = " AND ".join(["%s='%s'" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        records = db.prepared_act_on_database(db.FETCH_ALL, sql, [])
        models = []
        if records is not False:
            for record in records:
                if extra is not None:
                    record = merge_dicts(record, extra)
                models.append(cls(**record))
        return models

    @classmethod
    def find_all(cls, **kwargs):
        """
        Find all records (no criteria). Alias for find_all_by() with no arguments.

            Example:
            inverters = Inverter.find_all()

        :return: List of objects of type class.
        """
        return cls.find_all_by(**kwargs)

    @classmethod
    def delete_by(cls, **kwargs):
        """
        Delete all records matching kwargs criteria.

            Example:
            inverters = Inverter.find_all()

        :return: List of objects of type class.
        """
        sql = "DELETE FROM %s" % Model.table(cls)
        where_clause = " AND ".join(["%s = %s" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        db.prepared_act_on_database(db.EXECUTE, sql, None)

    @classmethod
    def count(cls, **kwargs):
        """
        Count all records with matching kwargs criteria.

            Example:
            inverters = Inverter.count(ip_address='10.252.2.34')
            inverters = Inverter.count(stringId='43:ae:7f')

        :return: Number of records matching kwargs criteria (if any).
        """
        sql = "SELECT COUNT(*) FROM %s" % Model.table(cls)
        where_clause = " AND ".join(["%s='%s'" % (k, v) for k, v in kwargs.iteritems()])
        if len(where_clause) > 0:
            sql += " WHERE %s" % where_clause
        count_result = db.prepared_act_on_database(db.FETCH_ROW_COUNT, sql, [])
        return count_result

    def save(self, **kwargs):
        keys, args = [], []
        for k, v in kwargs.iteritems():
            keys.append(k)
            args.append(v)
        params = ", ".join(["%s=%s" % (k, "%s") for k in keys])
        sql = "UPDATE %s SET %s" % (Model.table(self.__class__), params)
        id_field = self.__class__.id_field
        if id_field:
            sql += " WHERE %s=%s" % (id_field, "%s")
            args.append(getattr(self, id_field))
        db.prepared_act_on_database(db.EXECUTE, sql, args)

    def identity(self):
        id_field = self.__class__.id_field
        id_value = None
        if id_field is not None:
            try:
                id_value = getattr(self, id_field)
            except Exception:
                raise UnidentifiableRecord("Model subclass must define cls.id_field.")
        return id_field, id_value

    def reload(self):
        args = []
        sql = "SELECT * FROM %s" % Model.table(self.__class__)
        id_field, id_value = self.identity()
        if id_field is not None:
            sql += " WHERE %s=%s" % (id_field, "%s")
            args.append(id_value)
        record = db.prepared_act_on_database(db.FETCH_ONE, sql, args)
        if not record:
            raise RecordNotFound("Record not found: sql=%s" % sql)
        record = merge_dicts(record, self.__dict__)
        self.__init__(**record)

    def destroy(self):
        sql = "DELETE FROM %s" % Model.table(self.__class__)
        id_field, id_value = self.identity()
        sql += " WHERE %s=%s" % (id_field, "%s")
        db.prepared_act_on_database(db.EXECUTE, sql, [id_value])
