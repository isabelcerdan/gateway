
class GatewayExceptions(Exception):
    def __init__(self, string):
        self.string = string

    def __str__(self):
        return "[Gateway Exception]: %s" % self.string


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class ConnectError(Error):
    """
    Exception raised when unable to connect to a remote device.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)

class CommunicationError(Error):
    """
    Exception raised when unable to communicate with remote device.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ReadError(Error):
    """
    Exception raised when unable to read holding register on a remote slave device.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class WriteError(Error):
    """
    Exception raised when unable to write to holding register on remote slave device.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class CommandError(Error):
    """
    Exception raised when unable to complete a command.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ParameterError(Error):
    """
    Exception raised when an invalid parameter is encountered.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ProcessingError(Error):
    """
    Exception raised when an internal error is encountered.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DatabaseError(Error):
    """
    Exception raised when database error maximum is exceeded.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class NotSupportedError(Error):
    """
    Exception raised when a function is not fully implemented.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ZmqError(Error):
    """
    Exception raised when a ZMQ error is detected.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class SignalExit(Exception):
    """
        Exception raised when a signal is caught

        Attributes:
            message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


