import logging.handlers

LOGGING_FILE = "/var/log/apparent/gateway.log"
LOGGING_MAX_BYTES = 2048*2048
LOGGING_BACKUP_COUNT = 10
LOGGING_LEVEL = logging.INFO
TRACEBACK_ENABLED = True

'''
    Below are the list of available fields to use in the logging format:

    Format	        Description
    ---------------------------
    %(name)s	    Name of the logger (logging channel).
    %(levelno)s	    Numeric logging level for the message (DEBUG, INFO, WARNING, ERROR, CRITICAL).
    %(levelname)s	Text logging level for the message ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL').
    %(pathname)s	Full pathname of the source file where the logging call was issued (if available).
    %(filename)s	Filename portion of pathname.
    %(module)s	    Module (name portion of filename).
    %(lineno)d	    Source line number where the logging call was issued (if available).
    %(created)f	    Time when the LogRecord was created (as returned by time.time()).
    %(asctime)s	    Human-readable time when the LogRecord was created. By default this is of the form ``2003-07-08 16:49:45,896'' (the numbers after the comma are millisecond portion of the time).
    %(msecs)d	    Millisecond portion of the time when the LogRecord was created.
    %(thread)d	    Thread ID (if available).
    %(threadName)s	Thread name (if available).
    %(process)d	    Process ID (if available).
    %(message)s	    The logged message, computed as msg % args.
'''
LOGGING_FORMAT = '%(asctime)s %(name)s [%(levelname)s]: %(message)s'


def setup_logger(name):
    logger = logging.getLogger('gat.' + name)
    return logger


from db import prepared_act_on_database, FETCH_ONE


def get_log_level():
    sql = "SELECT log_level FROM gateway"
    result = prepared_act_on_database(FETCH_ONE, sql, [])
    if result:
        log_level = result['log_level']
    else:
        log_level = LOGGING_LEVEL

    return log_level

#
# Set root logger to use the default logging settings.
# All other loggers inherit from these settings.
#
# logging.basicConfig()


gat_logger = logging.getLogger('gat')
gat_logger.setLevel(get_log_level())

# Updated per GI-283, uses logrotate
handler = logging.handlers.WatchedFileHandler(LOGGING_FILE)
handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
gat_logger.addHandler(handler)

# Only import event after the root logger has been defined; otherwise
# we see 'No handlers could be found for logger "lib.db"'.
import event
gat_logger.addHandler(event.EventHandler())

# Using logging.config requires python 2.7.12.
# Ubuntu 14.04 LTS debian packages only support up to python v2.7.6
#
# import logging.config
#
#  configdict = {
#     'version': 1,    # Configuration schema in use; must be 1 for now
#     'formatters': {
#         'standard': {
#             'format': ('%(asctime)s %(name)-15s '
#                        '%(levelname)-8s %(message)s')}},
#
#     'handlers': {'gatewaylog': {'backupCount': 10,
#                                 'class': 'logging.handlers.RotatingFileHandler',
#                                 'filename': '/var/log/apparent/gateway.log',
#                                 'formatter': 'standard',
#                                 'level': 'DEBUG',
#                                 'maxBytes': 2048*2048},
#                  'syslog': {'class': 'logging.handlers.SysLogHandler',
#                             'formatter': 'standard',
#                             'level': 'ERROR'}},
#
#     # Specify all the subordinate loggers
#     'loggers': {
#         'network': {
#             'handlers': ['netlog']
#         }
#     },
#     # Specify properties of the root logger
#     'root': {
#         'handlers': ['gatewaylog']
#     },
# }
#
# # Set up configuration
# logging.config.dictConfig(configdict)
#
# # As an example, log two error messages
# logger = logging.getLogger('/')
# logger.error('Database not found')
#
# netlogger = logging.getLogger('network')
# netlogger.error('Connection failed')
