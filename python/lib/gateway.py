from lib.db import EXECUTE
from lib.gateway_constants.DBConstants import GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE
from lib.logging_setup import *
from lib.model import Model


class Gateway(Model):
    sql_table = "gateway"

    OPERATION_MODE_POWER_CONTROL_MODE = "power_control_mode"
    OPERATION_MODE_OBSERVATION_MODE = "observation_mode"

    def __init__(self, **kwargs):
        self.id = kwargs.get('gateway_id', None)
        self.name = kwargs.get('gateway_name', None)
        self.enable = kwargs.get('system_enable', 0) == '1'
        self.update_config = kwargs.get('update_config', 0) == '1'
        self.system_enable = kwargs.get('system_enable', 0) == '1'
        self.system_restart = kwargs.get('system_restart', 0) == '1'
        self.siteId = kwargs.get('siteId', None)
        self.uptime = kwargs.get('uptime', None)
        self.string_sight = kwargs.get('stringSight', 0) == '1'
        self.log_level = kwargs.get('log_level', None)
        self.ramp_rate_knee = kwargs.get('ramp_rate_knee', 1.0)
        self.ramp_rate_slope = kwargs.get('ramp_rate_slope', 2.0)
        self.violation_response_factor = kwargs.get('violation_response_factor')
        self.settle_time = kwargs.get('settle_time', 2.0)
        self.perspective = kwargs.get('perspective', 'production')
        self.operation_mode = kwargs.get('operation_mode', GATEWAY_TABLE_GATEWAY_OPERATION_MODE_POWER_CONTROL_MODE)

    def save(self):
        sql = "UPDATE Gateway SET system_enable=%s, system_restart=%s, update_config=%s" % \
              (self.system_enable, self.system_restart, self.update_config)
        prepared_act_on_database(EXECUTE, sql, [])

    def in_power_control_mode(self):
        return self.operation_mode == self.OPERATION_MODE_POWER_CONTROL_MODE
