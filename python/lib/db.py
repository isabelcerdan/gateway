import MySQLdb
import MySQLdb.cursors
from contextlib import closing
import traceback

from gateway_constants.DBConstants import *

DUPLICATE_DATA_MESSAGE = "Data appears to be duplicate"


def get_default(_dict, key, default):
    """
    Return default if key doesn't exist or value at key is None.

    :param _dict:       Dictionary that may or may not contain key.
    :param key:         Key to use in look up on dictionary.
    :param default:     Default value if key doesn't exist in dictionary or key value is None.
    :return:            Return default if key doesn't exist or value at key is None.
    """
    if key in _dict:
        if not _dict[key] is None:
            return _dict[key]
    return default


def prepared_act_on_database(action, command, args=[]):
    """
    Execute a query on MySQL database.

    :param action:      FETCH_ONE, FETCH_ALL, FETCH_ROW_COUNT, or EXECUTE
    :param command:     String containing SQL statement with "%s" for prepared arguments
    :param args:        Array (or tuple) of prepared arguments
    :return:            List of dictionaries containing field name and value as key value pair.
    """
    try:
        from logging_setup import setup_logger
        logger = setup_logger(__name__)
    except Exception as ex:
        logger = logging.getLogger('db')
    result = False

    try:
        # Create a closing object for use with the database connection that
        # guarantees the connection will be opened and closed at the end of
        # the block.
        with closing(MySQLdb.connect(user=DB_U_NAME,
                                     passwd=DB_PASSWORD,
                                     db=DB_NAME,
                                     cursorclass=MySQLdb.cursors.DictCursor,
                                     autocommit=True)) as db:                                     
            cursor = db.cursor()
            cursor.execute(command, args)

            if action == FETCH_ONE:
                result = cursor.fetchone()
            elif action == FETCH_ALL:
                result = cursor.fetchall()
            elif action == FETCH_ROW_COUNT:
                fetch_response = cursor.fetchone()
                result = fetch_response['COUNT(*)']
            elif action == EXECUTE:
                result = cursor.lastrowid
            elif action == EXECUTE_ROW_COUNT:
                result = cursor.rowcount
            else:
                logger.error("prepared_act_on_database failed, unknown action %s" % action)

            #print(cursor._last_executed)

    except MySQLdb.OperationalError as ex:
        '''
        The meter readings tables (pcc_readings_1, solar_readings_1, and pcc_readigns_2) define 
        MySQL triggers that raise an exception when duplicate data is inserted. We can safely 
        ignore the exception since it is not technically an error condition.  
        '''
        if ex.args[1] != DUPLICATE_DATA_MESSAGE:
            err_str = repr(ex)
            logger.exception(err_str)
            raise ex
    except Exception as ex:
        logger.exception("prepared_act_on_database failed: command=%s, args=%s" % (command, args))

    return result

    # db = Database.get_instance()
    # return db.prepared_act_on_database(action, command, args)


class Database(object):

    instance = None

    @classmethod
    def get_instance(cls, logger=None):
        if cls.instance is None:
            if logger is None:
                from logging_setup import setup_logger
                logger = setup_logger(__name__)
            cls.instance = Database(user=DB_U_NAME, passwd=DB_PASSWORD, db_name=DB_NAME, logger=logger, autocommit=True)
        return cls.instance

    @classmethod
    def close(cls):
        if cls.instance is not None:
            cls.instance.close()

    def __init__(self, **kwargs):
        try:
            self.logger = kwargs.get('logger', None)
            self.user = kwargs['user']
            self.passwd = kwargs['passwd']
            self.database_name = kwargs['db_name']
        except KeyError:
            self.logger.exception("Error initializing DB.")

        try:
            self.logger.debug("Opening connection to database...")
            self.connection = MySQLdb.connect(user=self.user,
                                              passwd=self.passwd,
                                              db=self.database_name,
                                              cursorclass=MySQLdb.cursors.DictCursor)
            self.logger.debug("Successfully opened connection to database")
        except Exception as ex:
            self.logger.exception("Error connecting to database.")

    def prepared_act_on_database(self, action, command, args):
        """
        Execute a query on MySQL database.

        :param action:      FETCH_ONE, FETCH_ALL, FETCH_ROW_COUNT, or EXECUTE
        :param command:     String containing SQL statement with "%s" for prepared arguments
        :param args:        Array (or tuple) of prepared arguments
        :return:            List of dictionaries containing field name and value as key value pair.
        """

        result = False
        try:
            with self.instance.connection as cursor:
                cursor.execute(command, args)
                if action == FETCH_ONE:
                    result = cursor.fetchone()
                elif action == FETCH_ALL:
                    result = cursor.fetchall()
                elif action == FETCH_ROW_COUNT:
                    fetch_response = cursor.fetchone()
                    result = fetch_response['COUNT(*)']
                elif action == EXECUTE:
                    result = cursor.lastrowid
                elif action == EXECUTE_ROW_COUNT:
                    result = cursor.rowcount
                else:
                    self.logger.error("prepared_act_on_database failed, unknown action %s" % action)

                #print(cursor._last_executed)

        except MySQLdb.OperationalError as ex:
            """
            The meter readings tables (pcc_readings_1, solar_readings_1, and pcc_readigns_2) define 
            MySQL triggers that raise an exception when duplicate data is inserted. We can safely 
            ignore the exception since it is not technically an error condition.  
            """
            if ex.args[1] != DUPLICATE_DATA_MESSAGE:
                raise ex
        except Exception as ex:
            self.logger.exception("prepared_act_on_database failed.")

        return result
