from datetime import datetime


def get_float_seconds_since(start_time):
    interval_length = (datetime.now() - start_time).total_seconds()
    interval_length += (datetime.now() - start_time).microseconds / 1000000.0
    return interval_length


def try_float(value):
    try:
        return float(value)
    except TypeError as ex1:
        return 0.0
    except Exception as ex:
        return value

def to_datetime(obj):
    if isinstance(obj, datetime):
        return obj
    return datetime.strptime(obj, "%Y-%m-%d %H:%M:%S")


def merge_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def datetime_to_utc(dt):
    return (dt - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds()


def truncate(value, precision=1):
    scale_factor = 10.0 ** precision
    return int(value * scale_factor) / scale_factor


def minmax(lower_bound, value, upper_bound):
    return min(max(lower_bound, value), upper_bound)


def get_args(dict, parser):
    parser.add_argument("--ess-unit-id", action="store", default=1, help="ESS unit id")
    for command, articles in dict.iteritems():
        action = 'store_true' if articles[1] < 1 else 'store'
        parser.add_argument(command, action=action, help=articles[0])
    return parser.parse_args()


def extract_cmd(clargs=None):
    args_dict = vars(clargs)
    ess_unit_id = args_dict.pop('ess_unit_id')
    for command, param in args_dict.iteritems():
        if param is not None and param is not False:
            return ess_unit_id, command, param
    return ess_unit_id, None, None

