# -*- coding: utf-8 -*-

import subprocess
import time
import re

from alarm.alarm import Alarm
from alarm.alarm_constants import *
from event.event_constants import *

from lib.logging_setup import *
from lib.db import prepared_act_on_database
from lib.gateway_constants.DBConstants import *

SENSOR_READ_INTERVAL = 60  # every minute (in seconds)
SENSOR_MAX_READ_ERRORS = 3 

class Sensor(object):
    '''
        Description:
         Process the output of the `ipmitool sensor` command and check for motherboard sensor violations. 
         
        $ sudo ipmitool sensor
        CPU Temp         | 51.000     | degrees C  | ok    | 0.000     | 0.000     | 0.000     | 93.000    | 98.000    | 98.000
        System Temp      | 45.000     | degrees C  | ok    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000
        Peripheral Temp  | 50.000     | degrees C  | ok    | -9.000    | -7.000    | -5.000    | 80.000    | 85.000    | 90.000
        DIMMA1 Temp      | 45.000     | degrees C  | ok    | 1.000     | 2.000     | 4.000     | 80.000    | 85.000    | 90.000
        DIMMA2 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
        DIMMB1 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
        DIMMB2 Temp      | na         |            | na    | na        | na        | na        | na        | na        | na
        FAN1             | na         |            | na    | na        | na        | na        | na        | na        | na
        FAN2             | na         |            | na    | na        | na        | na        | na        | na        | na
        FAN3             | na         |            | na    | na        | na        | na        | na        | na        | na
        VCCP             | 0.756      | Volts      | ok    | 0.441     | 0.450     | 0.495     | 1.296     | 1.422     | 1.440
        VDIMM            | 1.353      | Volts      | ok    | 1.092     | 1.119     | 1.200     | 1.641     | 1.722     | 1.749
        12V              | 0.224      | Volts      | nr    | 10.144    | 10.272    | 10.784    | 12.960    | 13.280    | 13.408
        5VCC             | 4.870      | Volts      | ok    | 4.246     | 4.298     | 4.480     | 5.390     | 5.546     | 5.598
        3.3VCC           | 3.350      | Volts      | ok    | 2.789     | 2.823     | 2.959     | 3.554     | 3.656     | 3.690
        VBAT             | 3.225      | Volts      | ok    | 2.400     | 2.490     | 2.595     | 3.495     | 3.600     | 3.690
        5V Dual          | 4.865      | Volts      | ok    | 4.244     | 4.298     | 4.487     | 5.378     | 5.540     | 5.594
        3.3V AUX         | 3.316      | Volts      | ok    | 2.789     | 2.823     | 2.959     | 3.554     | 3.656     | 3.690
        Chassis Intru    | 0x0        | discrete   | 0x0000| na        | na        | na        | na        | na        | na
        
    '''

    # used to map the `ipmitool sensor` columns
    mapping = dict(
        name=0,
        value=1,
        units=2,
        status=3,
        lower_non_responsive=4,
        lower_critical=5,
        lower_non_critical=6,
        upper_non_critical=7,
        upper_critical=8,
        upper_non_responsive=9)

    # maps motherboard sensors to corresponding low and high alarms
    alarms = {
        'Core Temp':        {'low': ALARM_ID_LOW_CPU_CORE_TEMP,     'high': ALARM_ID_HIGH_CPU_CORE_TEMP},
        'CPU Temp':         {'low': ALARM_ID_LOW_CPU_TEMP,          'high': ALARM_ID_HIGH_CPU_TEMP},
        'System Temp':      {'low': ALARM_ID_LOW_SYSTEM_TEMP,       'high': ALARM_ID_HIGH_SYSTEM_TEMP},
        'Peripheral Temp':  {'low': ALARM_ID_LOW_PERIPHERAL_TEMP,   'high': ALARM_ID_HIGH_PERIPHERAL_TEMP},
        'DIMMA1 Temp':      {'low': ALARM_ID_LOW_DIMM_TEMP,         'high': ALARM_ID_HIGH_DIMM_TEMP},
        'DIMMA2 Temp':      {'low': ALARM_ID_LOW_DIMM_TEMP,         'high': ALARM_ID_HIGH_DIMM_TEMP},
        'DIMMB1 Temp':      {'low': ALARM_ID_LOW_DIMM_TEMP,         'high': ALARM_ID_HIGH_DIMM_TEMP},
        'DIMMB2 Temp':      {'low': ALARM_ID_LOW_DIMM_TEMP,         'high': ALARM_ID_HIGH_DIMM_TEMP},
        'FAN1':             {'low': ALARM_ID_LOW_FAN_STATUS,        'high': ALARM_ID_HIGH_FAN_STATUS},
        'FAN2':             {'low': ALARM_ID_LOW_FAN_STATUS,        'high': ALARM_ID_HIGH_FAN_STATUS},
        'FAN3':             {'low': ALARM_ID_LOW_FAN_STATUS,        'high': ALARM_ID_HIGH_FAN_STATUS},
        'VCCP':             {'low': ALARM_ID_LOW_VCCP_STATUS,       'high': ALARM_ID_HIGH_VCCP_STATUS},
        'VDIMM':            {'low': ALARM_ID_LOW_VDIMM_STATUS,      'high': ALARM_ID_HIGH_VDIMM_STATUS},
        '12V':              {'low': ALARM_ID_LOW_12V_STATUS,        'high': ALARM_ID_HIGH_12V_STATUS},
        '5VCC':             {'low': ALARM_ID_LOW_5VCC_STATUS,       'high': ALARM_ID_HIGH_5VCC_STATUS},
        '3.3VCC':           {'low': ALARM_ID_LOW_3_3VCC_STATUS,     'high': ALARM_ID_HIGH_3_3VCC_STATUS},
        'VBAT':             {'low': ALARM_ID_LOW_VBAT_STATUS,       'high': ALARM_ID_HIGH_VBAT_STATUS},
        '5V Dual':          {'low': ALARM_ID_HIGH_5V_DUAL_STATUS,   'high': ALARM_ID_LOW_5V_DUAL_STATUS},
        '3.3V AUX':         {'low': ALARM_ID_LOW_3_3V_AUX_STATUS,   'high': ALARM_ID_HIGH_3_3V_AUX_STATUS},
        'Chassis Intru':    {'low': ALARM_ID_CHASSIS_INTRU_STATUS,  'high': ALARM_ID_CHASSIS_INTRU_STATUS}
    }

    logger = setup_logger('SENSOR')

    #updated per GI-283, uses logrotate
    handler = logging.handlers.WatchedFileHandler('/var/log/apparent/sensor.log')
    handler.setFormatter(logging.Formatter(LOGGING_FORMAT))
    logger.addHandler(handler)
    logger.propagate = False
    SENSOR_READ_INTERVAL = SENSOR_READ_INTERVAL
    last_read_time = 0

    core_sensor_read_errors = 0
    ipmi_sensor_read_errors = 0
    disk_usage_read_errors = 0

    @staticmethod
    def read_all():
        if Sensor.time_since_last_read() > Sensor.SENSOR_READ_INTERVAL:
            Sensor.check_uptime()
            Sensor.check_disk_usage()
            Sensor.check_core_sensors()
            Sensor.check_ipmi_sensors()
            Sensor.last_read_time = int(time.time())

    @staticmethod
    def check_disk_usage():
        '''
            Scan output of "df -h" command:
            ---
            Filesystem      Size  Used Avail Use% Mounted on
            udev            360M  4.0K  360M   1% /dev
            tmpfs            75M  788K   74M   2% /run
            /dev/dm-0       6.8G  4.0G  2.5G  62% /
            none            4.0K     0  4.0K   0% /sys/fs/cgroup
            none            5.0M     0  5.0M   0% /run/lock
            none            371M  6.9M  364M   2% /run/shm
            none            100M  4.0K  100M   1% /run/user
            /dev/sda1       236M   68M  156M  31% /boot
            none            239G  185G   55G  78% /media/sf_code
            none            239G  185G   55G  78% /home/gateway/code
        '''
        p = subprocess.Popen(['df', '-h'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        if err:
            Sensor.disk_usage_read_errors += 1
            if Sensor.disk_usage_read_errors > SENSOR_MAX_READ_ERRORS:
                Alarm.occurrence(ALARM_ID_DISK_USAGE_READ_ERROR, message=err.rstrip(), event_tags=[EVENT_TAG_SENSORS])
        else:
            Sensor.disk_usage_read_errors = 0

        if out:
            Sensor.logger.info("\n" + out)
            for line in out.split('\n'):
                m = re.search("([\w/-]+)\s+([\w\.]+)\s+([\w\.]+)\s+([\w\.]+)\s+([\w\.]+)%\s+([\w/-]+)", line)
                if m:
                    mounted_on = m.group(6)
                    if mounted_on == '/':
                        percent_usage = float(m.group(5))
                        if percent_usage >= 99.0:
                            Alarm.occurrence(ALARM_ID_DISK_USAGE_99_CAPACITY)
                        elif percent_usage >= 90.0:
                            Alarm.occurrence(ALARM_ID_DISK_USAGE_90_CAPACITY)
                        elif percent_usage >= 50.0:
                            Alarm.occurrence(ALARM_ID_DISK_USAGE_50_CAPACITY)
                        break

    @staticmethod
    def check_uptime():

        # /proc/uptime contains two numbers:
        #   - uptime of the system (seconds)
        #   - amount of time spent in idle process (seconds)

        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])

        sql = "SELECT uptime FROM %s" % DB_GATEWAY_TABLE
        gateway = prepared_act_on_database(FETCH_ONE, sql, [])
        if not gateway:
            return False

        # uptime should only go up unless a reboot occurred
        if uptime_seconds < gateway['uptime']:
            Alarm.occurrence(ALARM_ID_GATEWAY_REBOOT)

        sql = "UPDATE %s SET uptime=%s" % (DB_GATEWAY_TABLE, "%s")
        prepared_act_on_database(EXECUTE, sql, [uptime_seconds])

    @staticmethod
    def time_since_last_read():
        return int(time.time()) - Sensor.last_read_time

    @staticmethod
    def read_core_sensors():
        p = subprocess.Popen(['sensors'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        if err:
            Sensor.core_sensor_read_errors += 1
            if Sensor.core_sensor_read_errors > SENSOR_MAX_READ_ERRORS:
                Alarm.occurrence(ALARM_ID_MOBO_SENSORS_READ_ERROR, message=err.rstrip(), event_tags=[EVENT_TAG_SENSORS])
        else:
            Sensor.core_sensor_read_errors = 0

        if out:
            Sensor.logger.info("\n" + out)

        return out

    @staticmethod
    def check_core_sensors():
        result = Sensor.read_core_sensors()
        if result:
            pattern = ur'^Core\s+(\d+):\s+([+-]\d+\.\d+)°C\s+\(high = (\+\d+\.\d+)°C, crit = (\+\d+\.\d+)°C\)'
            for line in result.split('\n'):
                match = re.match(pattern, line, re.UNICODE)
                if match:
                    reading = {
                        'name': 'Core Temp',
                        'core': int(match.group(1)),
                        'value': float(match.group(2)),
                        'units': 'degrees C',
                        'status': 'unknown',
                        'upper_non_responsive': float(match.group(4)),
                        'upper_critical': float(match.group(3)),
                        'upper_non_critical': 90.0,
                        'lower_non_critical': 5.0,
                        'lower_critical': 0.0,
                        'lower_non_responsive': 0.0
                    }
                    sensor = Sensor(**reading)
                    sensor.verify()

    @staticmethod
    def remap(reading):
        return dict(
            name=reading[Sensor.mapping['name']],
            value=reading[Sensor.mapping['value']],
            units=reading[Sensor.mapping['units']],
            status=reading[Sensor.mapping['status']],
            lower_non_responsive=reading[Sensor.mapping['lower_non_responsive']],
            lower_critical=reading[Sensor.mapping['lower_critical']],
            lower_non_critical=reading[Sensor.mapping['lower_non_critical']],
            upper_non_critical=reading[Sensor.mapping['upper_non_critical']],
            upper_critical=reading[Sensor.mapping['upper_critical']],
            upper_non_responsive=reading[Sensor.mapping['upper_non_responsive']])

    @staticmethod
    def check_ipmi_sensors():
        result = Sensor.read_ipmi_sensors()
        if result:
            for line in result.rstrip().split('\n'):
                reading = Sensor.remap(map(str.strip, line.split('|')))
                sensor = Sensor(**reading)
                sensor.verify()

    @staticmethod
    def read_ipmi_sensors():
        p = subprocess.Popen(['ipmitool', 'sensor'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        if err:
            Sensor.ipmi_sensor_read_errors += 1
            if Sensor.ipmi_sensor_read_errors > SENSOR_MAX_READ_ERRORS:
                Alarm.occurrence(ALARM_ID_MOBO_SENSORS_READ_ERROR, message=err.rstrip(), event_tags=[EVENT_TAG_SENSORS])
        else:
            Sensor.ipmi_sensor_read_errors = 0

        if out:
            Sensor.logger.info("\n" + out)

        return out

    def __init__(self, **kwargs):
        self.logger = Sensor.logger
        self.name = kwargs.get('name', None)
        self.value = self.try_float(kwargs.get('value', None))
        self.units = kwargs.get('units', None)
        self.status = kwargs.get('status', None)
        self.lower_non_responsive = self.try_float(kwargs.get('lower_non_responsive', None))
        self.lower_critical = self.try_float(kwargs.get('lower_critical', None))
        self.lower_non_critical = self.try_float(kwargs.get('lower_non_critical', None))
        self.upper_non_critical = self.try_float(kwargs.get('upper_non_critical', None))
        self.upper_critical = self.try_float(kwargs.get('upper_critical', None))
        self.upper_non_responsive = self.try_float(kwargs.get('upper_non_responsive', None))
        self.core = kwargs.get('core', None)

        try:
            self.low_alarm_id = Sensor.alarms[self.name]['low']
            self.high_alarm_id = Sensor.alarms[self.name]['high']
        except KeyError:
            self.low_alarm_id = None
            self.high_alarm_id = None
            self.logger.exception("Ignoring unrecognized motherboard sensor: %s" % self.name)

    def try_float(self, string_value):
        try:
            return float(string_value)
        except:
            return string_value

    def violation(self):
        return self.status not in ['ok', 'na'] and not (self.name == 'Chassis Intru' and self.status == '0x0000')

    def verify(self):

        alarm_id = None
        message = None

        # if sensor reading is in non-critical range, request to clear alarm
        if not self.violation() or (self.lower_non_critical < self.value < self.upper_non_critical):
            if self.low_alarm_id:
                Alarm.request_clear(self.low_alarm_id)
            if self.high_alarm_id:
                Alarm.request_clear(self.high_alarm_id)
            return

        # check if sensor reading is in violation of critical and non-responsive thresholds

        # special handling for chassis intrusion which has units of discrete
        if self.units == 'discrete':
            alarm_id = self.low_alarm_id
            message = "VIOLATION: %s %s" % (self.value, self.units)
        elif self.value >= self.upper_non_responsive:
            alarm_id = self.high_alarm_id
            message = "UPPER NON-RESPONSIVE VIOLATION: %s %s (limit=%s %s)" % \
                  (self.value, self.units, self.upper_non_responsive, self.units)
        elif self.value >= self.upper_critical:
            alarm_id = self.high_alarm_id
            message = "UPPER CRITICAL VIOLATION: %s %s (limit=%s %s)" % \
                      (self.value, self.units, self.upper_critical, self.units)
        elif self.value <= self.lower_non_responsive:
            alarm_id = self.low_alarm_id
            message = "LOWER NON-RESPONSIVE VIOLATION: %s %s (limit=%s %s)" % \
                      (self.value, self.units, self.lower_non_responsive, self.units)
        elif self.value <= self.lower_critical:
            alarm_id = self.low_alarm_id
            message = "LOWER CRITICAL VIOLATION: %s %s (limit=%s %s)" % \
                      (self.value, self.units, self.lower_critical, self.units)
        else:
            self.logger.critical("MOTHERBOARD SENSOR %s -- Unrecognized status: %s" % (self.name, self.status))

        if alarm_id:
            if self.core in range(10):
                message = ("CPU Core %s: " % self.core) + message
            Alarm.occurrence(alarm_id, message=message, event_tags=[EVENT_TAG_SENSORS])

if __name__ == '__main__':
    Sensor.read_all()
