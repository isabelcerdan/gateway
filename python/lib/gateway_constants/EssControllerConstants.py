'''
@module: EssControllerConstants.py

@description: Constants file for Ess Controller on the Gateway.

@copyright: Apparent Energy Inc.  2019

@created: February 4, 2019

@author: steve
'''

ess_controller_command_input_dict = \
    {
        '--stop': ['Set unit to stop state', 0],
        '--run': ['Set unit to run state', 0],
        '--fault': ['Set unit to fault state', 0],
        '--active-power': ['Set active power for the unit', 1],
        '--reactive-power': ['Set reactive power for the unit', 1],
        '--voltage-control': ['Set voltage level for the unit', 1],
        '--frequency-control': ['Set frequency for the unit', 1],
        '--set-battery-mode': ['Set battery mode for the unit', 1],
    }
