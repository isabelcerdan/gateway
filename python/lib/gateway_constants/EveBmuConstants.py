'''
@module: EveBmuConstants.py

@description: Constants file for EVE Battery Management Unit communications on the Gateway.

@copyright: Apparent Energy Inc.  2018

@created: May 30, 2018

@author: steve
'''

EVE_MAX_MODBUS_ERRORS = 1000
EVE_BMU_MAX_DB_ERRORS = 5
EVE_BMU_DEFAULT_POLLING_FREQ = 1
EVE_MAX_ZMQ_ERRORS = 10
EVE_MIN_MODBUS_ID = 1
EVE_MAX_MODBUS_ID = 11
EVE_CONNECT_WAIT_TIME = 10
EVE_BMU_NUM_PUB_THREADS = 2 #22
EVE_BMU_START_RACK_ID = 1
EVE_BMU_END_RACK_ID = 1 #11
EVE_BMU_START_MOD_ID = 1
EVE_BMU_END_MOD_ID = 1 #11
EVE_DATA_PUBLISHER_HWM = 100
EVE_DATA_PUBLISHER_LINGER_TIMEOUT = 100

'''
EVE BMU state definitions
'''
EVE_BMU_UNINITIALIZED_STATE = 'Uninitialized'
EVE_BMU_UNRESTRICTED_STATE = 'Unrestricted'
EVE_BMU_CHARGING_STATE = 'Charging'
EVE_BMU_CHARGED_COOLING_STATE = 'Charged cooling'
EVE_BMU_CHARGED_RTD_STATE = 'Charged rtd'
EVE_BMU_DISCHARGING_STATE = 'Discharging'
EVE_BMU_DISCHARGED_COOLING_STATE = 'Discharged cooling'
EVE_BMU_DISCHARGED_RTC_STATE = 'Discharged rtc'
EVE_BMU_MAINTENANCE_STATE = 'Maintenance'
EVE_BMU_FAULT_STATE = 'Fault'

'''
EVE BMU register definitions
'''
BATTERY_MODULE_CURRENT_REG_OFFSET = 0
BATTERY_MODULE_CURRENT_SCALING_FACTOR = 0.01

BATTERY_MODULE_VOLTAGE_REG_OFFSET = 1
BATTERY_MODULE_VOLTAGE_SCALING_FACTOR = 0.01

BATTERY_MODULE_SOC_REG_OFFSET = 2
BATTERY_MODULE_SOC_SCALING_FACTOR = 1.0

BATTERY_MODULE_SOH_REG_OFFSET = 3
BATTERY_MODULE_SOH_SCALING_FACTOR = 1.0

REMAINING_CAPACITY_REG_OFFSET = 4
REMAINING_CAPACITY_SCALING_FACTOR = 0.01

FULL_CHARGED_CAPACITY_REG_OFFSET = 5
FULL_CHARGED_CAPACITY_SCALING_FACTOR = 0.01

DESIGN_CAPACITY_REG_OFFSET = 6
DESIGN_CAPACITY_SCALING_FACTOR = 0.01

BATTERY_CYCLE_COUNT_REG_OFFSET = 7

RESERVED_8 = 8

WARNING_FLAG_REG_OFFSET = 9
PROTECTION_FLAG_REG_OFFSET = 10
FAULT_STATUS_REG_OFFSET = 11
BALANCE_STATUS_REG_OFFSET = 12

RESERVED_13 = 13
RESERVED_14 = 14

CELL_VOLTAGE_1_REG_OFFSET = 15
CELL_VOLTAGE_2_REG_OFFSET = 16
CELL_VOLTAGE_3_REG_OFFSET = 17
CELL_VOLTAGE_4_REG_OFFSET = 18
CELL_VOLTAGE_5_REG_OFFSET = 19
CELL_VOLTAGE_6_REG_OFFSET = 20
CELL_VOLTAGE_7_REG_OFFSET = 21
CELL_VOLTAGE_8_REG_OFFSET = 22
CELL_VOLTAGE_9_REG_OFFSET = 23
CELL_VOLTAGE_10_REG_OFFSET = 24
CELL_VOLTAGE_11_REG_OFFSET = 25
CELL_VOLTAGE_12_REG_OFFSET = 26
CELL_VOLTAGE_13_REG_OFFSET = 27
CELL_VOLTAGE_14_REG_OFFSET = 28
CELL_VOLTAGE_15_REG_OFFSET = 29
CELL_VOLTAGE_16_REG_OFFSET = 30
CELL_VOLTAGE_SCALING_FACTOR = 0.001
CELL_VOLTAGE_GROUP_OFFSET = 15
CELL_VOLTAGE_NUM_CELLS = 16

'''
4 cell temperature sensors
'''
CELL_TEMPERATURE_1_REG_OFFSET = 31
CELL_TEMPERATURE_2_REG_OFFSET = 32
CELL_TEMPERATURE_3_REG_OFFSET = 33
CELL_TEMPERATURE_4_REG_OFFSET = 34
CELL_TEMPERATURE_SCALING_FACTOR = 0.1
CELL_TEMPERATURE_GROUP_OFFSET = 31
CELL_TEMPERATURE_NUM_SENSORS = 4

MOSFET_TEMPERATURE_REG_OFFSET = 35
MOSFET_TEMPERATURE_SCALING_FACTOR = 0.1

ENVIRONMENT_TEMPERATURE_REG_OFFSET = 36
ENVIRONMENT_TEMPERATURE_SCALING_FACTOR = 0.1

RESERVED_37 = 37
RESERVED_38 = 38
RESERVED_39 = 39
RESERVED_40 = 40
RESERVED_41 = 41
RESERVED_42 = 42
RESERVED_43 = 43
RESERVED_44 = 44
RESERVED_45 = 45
RESERVED_46 = 46
RESERVED_47 = 47
RESERVED_48 = 48
RESERVED_49 = 49
RESERVED_50 = 50
RESERVED_51 = 51
RESERVED_52 = 52
RESERVED_53 = 53
RESERVED_54 = 54
RESERVED_55 = 55
RESERVED_56 = 56
RESERVED_57 = 57
RESERVED_58 = 58
RESERVED_59 = 59

PACK_OVERVOLTAGE_ALARM_REG_OFFSET = 60
PACK_OVERVOLTAGE_ALARM_SCALING_FACTOR = 0.001

PACK_OVERVOLTAGE_PROTECTION_REG_OFFSET = 61
PACK_OVERVOLTAGE_PROTECTION_SCALING_FACTOR = 0.001

PACK_OVERVOLTAGE_RELEASE_PROTECTION_REG_OFFSET = 62
PACK_OVERVOLTAGE_RELEASE_PROTECTION_SCALING_FACTOR = 0.001

PACK_OVERVOLTAGE_PROTECTION_DELAY_TIME_REG_OFFSET = 63
PACK_OVERVOLTAGE_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

CELL_OVERVOLTAGE_ALARM_REG_OFFSET = 64
CELL_OVERVOLTAGE_ALARM_SCALING_FACTOR = 0.001

CELL_OVERVOLTAGE_PROTECTION_REG_OFFSET = 65
CELL_OVERVOLTAGE_PROTECTION_SCALING_FACTOR = 0.001

CELL_OVERVOLTAGE_RELEASE_PROTECTION_REG_OFFSET = 66
CELL_OVERVOLTAGE_RELEASE_PROTECTION_SCALING_FACTOR = 0.001

CELL_OVERVOLTAGE_PROTECTION_DELAY_TIME_REG_OFFSET = 67
CELL_OVERVOLTAGE_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

PACK_UNDERVOLTAGE_ALARM_REG_OFFSET = 68
PACK_UNDERVOLTAGE_SCALING_FACTOR = 0.001

PACK_UNDERVOLTAGE_PROTECTION_REG_OFFSET = 69
PACK_UNDERVOLTAGE_PROTECTION_SCALING_FACTOR = 0.001

PACK_UNDERVOLTAGE_RELEASE_PROTECTION_REG_OFFSET = 70
PACK_UNDERVOLTAGE_RELEASE_PROTECTION_SCALING_FACTOR = 0.001

PACK_UNDERVOLTAGE_PROTECTION_DELAY_TIME_REG_OFFSET = 71
PACK_UNDERVOLTAGE_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

CELL_UNDERVOLTAGE_ALARM_REG_OFFSET = 72
CELL_UNDERVOLTAGE_ALARM_SCALING_FACTOR = 0.001

CELL_UNDERVOLTAGE_PROTECTION_REG_OFFSET = 73
CELL_UNDERVOLTAGE_PROTECTION_SCALING_FACTOR = 0.001

CELL_UNDERVOLTAGE_RELEASE_PROTECTION_REG_OFFSET = 74
CELL_UNDERVOLTAGE_RELEASE_PROTECTION_SCALING_FACTOR = 0.001

CELL_UNDERVOLTAGE_PROTECTION_DELAY_TIME_REG_OFFSET = 75
CELL_UNDERVOLTAGE_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

CHARGING_OVER_CURRENT_ALARM_REG_OFFSET = 76
CHARGING_OVER_CURRENT_PROTECTION_REG_OFFSET = 77
CHARGING_OVER_CURRENT_PROTECTION_DELAY_TIME_REG_OFFSET = 78
CHARGING_OVER_CURRENT_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

DISCHARGING_OVER_CURRENT_ALARM_REG_OFFSET = 79
DISCHARGING_OVER_CURRENT_PROTECTION_REG_OFFSET = 80
DISCHARGING_OVER_CURRENT_PROTECTION_DELAY_TIME_REG_OFFSET = 81
DISCHARGING_OVER_CURRENT_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.1

DISCHARGING_OVER_CURRENT_2_PROTECTION_REG_OFFSET = 82
DISCHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_REG_OFFSET = 83
DISCHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.025

CHARGING_OVER_TEMPERATURE_ALARM_REG_OFFSET = 84
CHARGING_OVER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

CHARGING_OVER_TEMPERATURE_PROTECTION_REG_OFFSET = 85
CHARGING_OVER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

CHARGING_OVER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 86
CHARGING_OVER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

DISCHARGING_OVER_TEMPERATURE_ALARM_REG_OFFSET = 87
DISCHARGING_OVER_TEMPERATURE_PROTECTION_REG_OFFSET = 88
DISCHARGING_OVER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 89

CHARGING_UNDER_TEMPERATURE_ALARM_REG_OFFSET = 90
CHARGING_UNDER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

CHARGING_UNDER_TEMPERATURE_PROTECTION_REG_OFFSET = 91
CHARGING_UNDER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

CHARGING_UNDER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 92
CHARGING_UNDER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

DISCHARGING_UNDER_TEMPERATURE_ALARM_REG_OFFSET = 93
DISCHARGING_UNDER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

DISCHARGING_UNDER_TEMPERATURE_PROTECTION_REG_OFFSET = 94
DISCHARGING_UNDER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

DISCHARGING_UNDER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 95
DISCHARGING_UNDER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

MOSFET_OVER_TEMPERATURE_ALARM_REG_OFFSET = 96
MOSFET_OVER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

MOSFET_OVER_TEMPERATURE_PROTECTION_REG_OFFSET = 97
MOSFET_OVER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

MOSFET_OVER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 98
MOSFET_OVER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

ENVIRONMENT_OVER_TEMPERATURE_ALARM_REG_OFFSET = 99
ENVIRONMENT_OVER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

ENVIRONMENT_OVER_TEMPERATURE_PROTECTION_REG_OFFSET = 100
ENVIRONMENT_OVER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

ENVIRONMENT_OVER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 101
ENVIRONMENT_OVER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

ENVIRONMENT_UNDER_TEMPERATURE_ALARM_REG_OFFSET = 102
ENVIRONMENT_UNDER_TEMPERATURE_ALARM_SCALING_FACTOR = 0.1

ENVIRONMENT_UNDER_TEMPERATURE_PROTECTION_REG_OFFSET = 103
ENVIRONMENT_UNDER_TEMPERATURE_PROTECTION_SCALING_FACTOR = 0.1

ENVIRONMENT_UNDER_TEMPERATURE_RELEASE_PROTECTION_REG_OFFSET = 104
ENVIRONMENT_UNDER_TEMPERATURE_RELEASE_PROTECTION_SCALING_FACTOR = 0.1

BALANCE_START_CELL_VOLTAGE = 105
BALANCE_START_CELL_VOLTAGE_SCALING_FACTOR = 0.001

BALANCE_START_DELTA_VOLTAGE_REG_OFFSET = 106
BALANCE_START_DELTA_VOLTAGE_SCALING_FACTOR = 0.001

PACK_FULL_CHARGE_VOLTAGE_REG_OFFSET = 107
PACK_FULL_CHARGE_VOLTAGE_SCALING_FACTOR = 0.001

PACK_FULL_CHARGE_CURRENT_REG_OFFSET = 108
PACK_FULL_CHARGE_CURRENT_SCALING_FACTOR = 0.001

CELL_SLEEP_VOLTAGE_REG_OFFSET = 109
CELL_SLEEP_VOLTAGE_SCALING_FACTOR = 0.001

CELL_SLEEP_DELAY_TIME_REG_OFFSET = 110

SHORT_CIRCUIT_PROTECT_DELAY_TIME_REG_OFFSET = 111
SHORT_CIRCUIT_PROTECT_DELAY_TIME_SCALING_FACTOR = 0.05 # each unit = 25us, max = 500um

SOC_ALARM_THRESHOLD_REG_OFFSET = 112
SOC_ALARM_THRESHOLD_SCALING_FACTOR = 0.1 # %

CHARGING_OVER_CURRENT_2_PROTECTION_REG_OFFSET = 113

CHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_REG_OFFFSET = 114
CHARGING_OVER_CURRENT_2_PROTECTION_DELAY_TIME_SCALING_FACTOR = 0.025

VERSION_INFORMATION_REG_OFFSET = 150
VERSION_INFORMATION_DATA_LEN = 20

MODEL_SN_REG_OFFSET = 160
MODEL_SN_DATA_LEN = 20

PACK_SN_REG_OFFSET = 170
PACK_SN_DATA_LEN = 20

warning_flag_dict = {
    0 : 'battery cell overvoltage',
    1 : 'battery cell low voltage',
    2 : 'battery pack overvoltage',
    3 : 'battery pack low voltage',
    4 : 'charging over current',
    5 : 'discharging over current',
    6 : 'reserved',
    7 : 'reserved',
    8 : 'charging high temperature',
    9 : 'discharging high temperature',
    10 : 'charging low temperature',
    11 : 'discharging low temperature',
    12 : 'environment high temperature',
    13 : 'environment low temperature',
    14 : 'MOSFET high temperature',
    15 : 'SOC low'
}
NUM_WARNING_BITS = 16

protection_flag_dict = {
    0 : 'battery cell overvoltage',
    1 : 'battery cell low voltage',
    2 : 'battery pack overvoltage',
    3 : 'battery pack low voltage',
    4 : 'charging over current',
    5 : 'discharging over current',
    6 : 'short circuit',
    7 : 'charger overvoltage',
    8 : 'charging high cell temperature',
    9 : 'discharging high cell temperature',
    10 : 'charging low cell temperature',
    11 : 'discharging low cell temperature',
    12 : 'MOSFET high temperature',
    13 : 'environment high temperature',
    14 : 'environment low temperature',
}
NUM_PROTECTION_BITS = 15

fault_status_flag_dict = {
    0 : 'charging MOSFET',
    1 : 'discharging MOSFET',
    2 : 'temperature sensor',
    3 : 'reserve',
    4 : 'battery cell',
    5 : 'front end sampling communication',
    6 : 'reserve',
    7 : 'reserve',
    8 : 'charging',
    9 : 'discharging',
    10 : 'charging MOSFET',
    11 : 'discharging MOSFET',
    12 : 'charging limiter is ',
    13 : 'reserve',
    14 : 'charger inversed',
    15 : 'heater'
}
NUM_FAULT_BITS = 8
NUM_STATUS_BITS = 8
NUM_FAULT_STATUS_BITS = 16

eve_bmu_controller_command_input_dict = \
    {
        '--set-alarm': ['Set alarm <pack_overvoltage | cell_overvoltage | pack_undervoltage | cell_undervoltage> <bmu_id> <value>', 3],
        '--set-protection': ['Set protection <pack_overvoltage | cell_overvoltage | pack_undervoltage | cell_undervoltage> <bmu_id> <value>', 3],
    }

'''
EVE BMU Exceptions
'''
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class BusyError(Error):
    """
    Exception raised when MODbus slave is too busy to comply with a request.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    
        
class ConnectError(Error):
    """
    Exception raised when unable to connect to MODbus slave.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    
    
class ReadError(Error):
    """
    Exception raised when unable to read holding register on MODbus slave.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class WriteError(Error):
    """
    Exception raised when unable to write to holding register on MODbus slave.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class CommandError(Error):
    """
    Exception raised when unable to complete a command on the CESS.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class ParameterError(Error):
    """
    Exception raised when an invalid parameter is encountered.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class ProcessingError(Error):
    """
    Exception raised when an internal error is encountered.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class DatabaseError(Error):
    """
    Exception raised when database error maximum is exceeded.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

class NotSupportedError(Error):
    """
    Exception raised when a function is not fully implemented.

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class SignalExit(Error):    
    """
    Exception raised when a signal is caught

    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class ZmqError(Error):
    """
    Exception raised when ZMQ error maximum is exceeded.
    
    Attributes:
        message -- explanation of the error
    """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
    

