'''
Created on Feb 17, 2017

Constants for the ICP DAS Pet 7202 network protector

Reference: "ET-7000/PET-7000/ET-7200/PET7200  Series Register Table",
            Copyright 2013 by ICP DAS CO., LTD.
            
@author: steve
'''

ICPCON_MODBUS_PORT = 502
ICPCON_NUM_DI_REGISTERS = 4
ICPCON_DISCRETE_INPUT_REGISTER_OFFSET = 0

ICPCON_ERROR_CYCLE = 3     # 3000 ms

ICPCON_STATE_NORMAL = 'normal'
ICPCON_STATE_TRIPPED = 'tripped'
ICPCON_STATE_ERROR = 'error'
