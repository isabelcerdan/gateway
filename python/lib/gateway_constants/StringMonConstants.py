#!/usr/bin/env python
# encoding: utf-8
'''
Constants for the String ID/Position monitor

Created on Jan 18, 2017

@author: steve
'''

'''
    IMPORTANT: 
    
    THIS FILE WILL BE OVERWRITTEN BY ANSIBLE DURING PROVISIONING!!!
    TO MAKE CHANGES TO THIS FILE, YOU NEED TO EDIT THE FOLLOWING 
    TEMPLATE:
    
        ./provision/roles/daemons/templates/StringMonConstants.py.j2

    YOU MUST THEN RE-RUN GATEWAY PROVISIOING TO RENDER THE TEMPLATE.
    
        $ cd ~/gateway/provision
        $ ./self-provision.sh daemons
'''
STRINGMON_HOS_DISCOVERY_PORT = 60997

STRINGMON_LAN_INTERFACE = 'eth1'

STRINGMON_HEAD_OF_STRING_POSITION = 1

HOS_QUERY_MULTICAST_ADDRESS = "239.192.0.0"
STRINGMON_AUDIT_CYCLE_TIME = 10    # The Monitor checks our heartbeat every 10 seconds.

STRINGMON_SUCCESS = 0
STRINGMON_FAIL = 1
STRINGMON_TRY_AGAIN_LATER = 2

STRINGMON_MAX_ON_DEMAND_FAILS = 3

STRINGMON_MAX_TRY_AGAIN_CNT = 3

