"""
@module: ZmqConstants.py

@description: Contains all ZMQ port-based definitions for the Gateway.

@copyright: Apparent Energy Inc.  2018

@created: June 19, 2018

@author: steve
"""

# Patrice de Muizon - 4/9/2018
#
# When using ZMQ publisher/subscriber protocol, the publisher needs to wait after
# connecting before sending a message. Otherwise, the subscriber never sees the
# message since ZMQ hasn't been given enough time to initialize the publisher
# connection. There may be a more elegant way to handle this but for now we
# simply wait 100ms using time.sleep(PUB_CONNECT_WAIT_TIME) after the publisher
# connects.

PUB_CONNECT_WAIT_TIME = 0.100  # wait 100ms after connect before sending

# DEPRECATED
ESS_ALARM_PORT = 60894
ESS_ALARM_URL = "tcp://127.0.0.1:%d" % ESS_ALARM_PORT

MSG_BROKER_XSUB_PORT = 60888
MSG_BROKER_XSUB_URL = "tcp://127.0.0.1:%d" % MSG_BROKER_XSUB_PORT

MSG_BROKER_XPUB_PORT = 60887
MSG_BROKER_XPUB_URL = "tcp://127.0.0.1:%d" % MSG_BROKER_XPUB_PORT

MSG_BROKER_CAPTURE_PORT = 60882
MSG_BROKER_CAPTURE_URL = "tcp://127.0.0.1:%d" % MSG_BROKER_CAPTURE_PORT

ARTESYN_CLI_PORT = 60881
ARTESYN_CLI_URL = "tcp://127.0.0.1:%d" % ARTESYN_CLI_PORT
