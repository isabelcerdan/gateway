#!/bin/bash
#
# Usage: AessSetThreshold.bash <threshold value>

input_value=$1
echo ${input_value}
let "threshold_value = ${input_value} * -1"
echo ${threshold_value}
/usr/share/apparent/python/DbUpdate.py --table meter_feeds \
                                       --column threshold \
                                       --column-value ${threshold_value} \
                                       --where-column-1 meter_role \
                                       --where-value-1 pcc \
                                       --where-column-2 feed_name \
                                       --where-value-2 Delta

