#!/usr/share/apparent/.py2-virtualenv/bin/python
import argparse

from ess.ess_unit import EssUnit
from power_regulator.meter_feed import MeterFeed
from power_regulator.power_regulator import PowerRegulator

parser = argparse.ArgumentParser()
parser.add_argument("--power-regulator-id", default=None, help="Specify the power regulator you want to modify.")
parser.add_argument("--threshold", default=None, help="Change the threshold for specified power regulator (--power-regulator-id).")
parser.add_argument("--meter-role", default='pcc', choices=["pcc", "pcc2", "pcc_combo"], help="Specify the meter role on which to modify the threshold.")
parser.add_argument("--feed-name", default='Delta', choices=["Delta", "A", "B", "C"], help="Specify the meter feed on which to modify the threshold.")
parser.add_argument("--battery-mode", default=None, choices=["manual", "auto"], help="Change the battery mode.")
parser.add_argument("--battery-strategy", default=None, choices=["variable_charge", "variable_discharge", "set_point"], help="Change the battery strategy for specified power regulator (--power-regulator-id).")
parser.add_argument("--unit-work-state", default=None, choices=["Running", "Stopped"], help="Specify the ESS unit work state.")
parser.add_argument("--ess-unit-id", default=1, help="Specify the ESS unit ID.")
args = parser.parse_args()

ess_unit = EssUnit.find_by(id=args.ess_unit_id)

if args.threshold is not None:
    if args.power_regulator_id is not None:
        meter_feed = MeterFeed.find_by(power_regulator_id=args.power_regulator_id, meter_role=args.meter_role, feed_name=args.feed_name)
        meter_feed.save(threshold=args.limit)
    else:
        raise Exception("Must specify --power-regulator-id when setting --threshold.")

if args.battery_mode is not None:
    ess_unit.save(mode=args.battery_mode)

if args.unit_work_state is not None:
    ess_unit.save(unit_work_state=args.unit_work_state)

if args.battery_strategy is not None:
    if args.power_regulator_id is not None:
        power_regulator = PowerRegulator.find_by(id=args.power_regulator_id)
        power_regulator.save(battery_strategy=args.battery_strategy)
    else:
        raise Exception("Must specify --power-regulator-id when setting --battery-strategy.")
