#!/usr/bin/env bash

dest=$1
rsync -avz --exclude-from=deploy.exclude --delete ./ ${dest}:/home/gateway/gateway.rsync/